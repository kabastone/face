I. Les profils Spring
======================

### 1/ Le profil prod
Le profil prod permet de déployer l'application Web sur un serveur Tomcat et d'exécuter les batchs __dans tout type d'environnement__.   

Les valeurs des paramètres de configuration des fichiers __application-prod.properties__ 
doivent être surchargées par des arguments donnés à la JVM ou par des variables d'environnement.

Il y a deux fichiers application-prod.properties :
* Le premier est dans le module faceWebApp (src/main/resources)
* Le second est dans le module faceBatch (src/main/resources)

### 2/ Le profil testpic

Le profil testpic permet d'exécuter les tests unitaires et d'intégration via Maven __dans tout type d'environnement__ (que ce soit sur la PIC du ministère ou la PIC Atos).
  
Les valeurs des paramètres de configuration des fichiers __application-testpic.properties__ 
doivent être surchargées par des arguments donnés à la JVM ou par des variables d'environnement.

Il y a deux fichiers application-testpic.properties :
* Le premier est dans le module faceWebApp (src/test/resources)
* Le second est dans le module faceBatch (src/test/resources)

### 3/ Le profil dev
Le profil dev permet de déployer l'application sur un serveur Tomcat et d'exécuter les batchs en __environnement dédié au développement__. 
  
Il s'agit du profil Spring par défaut pour l'exécution du WAR ou des batchs.

Il n'y a rien à configurer à part la datasource, les valeurs des paramètres de configuration dans le fichier __application.properties__ correspondent à l'environnement de développement standardisé.

Pour information, il y a deux fichiers application.properties :
* Le premier est dans le module faceWebApp (src/main/resources)
* Le second est dans le module faceBatch (src/main/resources)

### 4/ Le profil test
Le profil test permet d'exécuter les tests unitaires et d'intégration directement par l'IDE en __environnement dédié au développement__. 
  
Il s'agit du profil Spring par défaut pour l'exécution des tests jUnit.

Il n'y a rien à configurer, les valeurs des paramètres de configuration dans le fichier __application.properties__ correspondent à l'environnement de test standardisé.

Pour information, il y a deux fichiers application.properties :
* Le premier est dans le module faceWebApp (src/test/resources)
* Le second est dans le module faceBatch (src/test/resources)

### 5/ Le profil devang
Le profil devang permet une exécution de l'application WEB en __environnement dédié au développement Angular__, c'est à dire via "ng serve".

Il n'y a rien à configurer à part la datasource, les paramètres de configuration dans le fichier __application-devang.properties__ correspondent à l'environnement de développement standardisé.

Pour information, il n'y a qu'un seul fichier application-devang.properties, il se situe dans le module faceWebApp (src/main/resources).

II. Configuration de l'environnement
=====================================

### 1/ Configuration du profil spring
Il est inutile de configurer les profils dev et test qui sont des profils par défaut.   
Il est par contre __IMPERATIF__ de configurer le profil si on veut utiliser les profils prod, testpic ou devang.

Le profil Spring doit être fourni soit par argument donné à la JVM, soit par variable d'environnement. 

Exemple pour configurer un profil __prod__ par argument à la JVM :

	-Dspring.profiles.active="prod"

Exemple pour configurer un profil __prod__ sous Linux par variable d'environnement :

	export SPRING_PROFILES_ACTIVE=prod
	

### 2/ Configuration de la data source pour Tomcat

Pour un déploiement de l'application sur un serveur Tomcat, il faut donner les paramètres de configuration de la datasource JNDI.  

Pour ce faire, donner les arguments suivants à la JVM de Tomcat (les valeurs sont des exemples):

	-Dtomcat.datasource.jndi-name="jdbc/face"
	-Dtomcat.datasource.url="jdbc:postgresql://127.0.0.1:5432/face" 
	-Dtomcat.datasource.username="face"
	-Dtomcat.datasource.password="face" 
	 

### 3/ Valeur des paramètres de configuration des fichiers application-[profil].properties

Pour les profils Spring __prod__ et __testpic__, il faut donner un argument à la jvm  ou créer une variable d'environnement pour __chaque paramètre de configuration__  des fichiers idoines.

Par exemple, si on a configuré un environnement Spring __prod__,  il faut surcharger la valeur de chaque propriété du fichier __application-prod.properties__ en donnant un argument à la JVM ou en créant la variable d'environnement correspondante.

Pour surcharger une variable de configuration par argument donné à la JVM, l'argument doit avoir le même nom que la variable de configuration.

Pour surcharger une variable de configuration par variable d'environnement, il faut transformer le nom de cette variable en faisant 2 opérations :
* la mettre en majuscules
* remplacer les éventuels points et tirets par des underscores.

Exemple de propriété :

	jwt.token-secret-key=key
	
Pour configurer la clé secrète, on peut soit ajouter l'argument suivant à la VM :

	-Djwt.token-secret-key="maclesecrete"
 
soit créer la variable d'environnement correspondante : 

	export JWT_TOKEN_SECRET_KEY=maclesecrete
	


III. Le module faceData
======================

La build Maven du module faceData génère 3 archives zip.


### 1/ L'archive face-sql-complet.zip 

Cette archive contient les scripts SQL complets permettant de créer la base de données (from scratch).

### 2/ L'archive face-sql-incremental.zip 

Cette archive contient les scripts SQL incrementaux permettant de passer la base de données de la version N (la version en prod) à la version N+1 (la version livrée).

### 3/ L'archive face-fs.zip

Cette archive contient les ressources de type fichier nécessaires à l'application (File System). 

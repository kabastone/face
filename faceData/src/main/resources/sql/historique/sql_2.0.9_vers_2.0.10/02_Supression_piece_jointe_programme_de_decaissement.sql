ALTER TABLE DOCUMENT DROP CONSTRAINT FK_DOCUMENT_R_TYPE_DOCUMENT;

ALTER TABLE DOCUMENT ADD CONSTRAINT FK_DOCUMENT_R_TYPE_DOCUMENT
	FOREIGN KEY (DOC_TDO_ID_CODIFIE) REFERENCES R_TYPE_DOCUMENT (TDO_ID_CODIFIE) ON DELETE CASCADE ON UPDATE No Action
;

DELETE FROM R_TYPE_DOCUMENT CASCADE
WHERE TDO_ID_CODIFIE = '106';
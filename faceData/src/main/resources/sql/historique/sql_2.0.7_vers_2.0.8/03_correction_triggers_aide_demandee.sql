-- ################### TRIGGERS AIDE_DEMANDEE  #############################    
    
-- Trigger qui alimente le champ calculé dpa_aide_demandee de la table demande_paiement
DROP FUNCTION IF EXISTS calculer_aide_demandee_demande_pai()  CASCADE;
CREATE FUNCTION calculer_aide_demandee_demande_pai() RETURNS trigger AS $trg_aide_demandee_demande_pai$
    
	DECLARE
		version varchar(3);
		
	BEGIN
	    
	    version = (SELECT reg_version_algorithme FROM r_reglementaire WHERE reg_annee_mise_en_application = 
		    (SELECT COALESCE(max(reg_annee_mise_en_application), (SELECT min(reg_annee_mise_en_application) FROM r_reglementaire)) 
		    	FROM r_reglementaire WHERE reg_annee_mise_en_application <=
		    	(SELECT dpr_annee FROM dotation_programme 
		    	inner join dotation_sous_programme on dsp_dpr_id=dpr_id
		    	inner join dotation_departement on dde_dsp_id=dsp_id
		    	inner join dotation_collectivite on dco_dde_id=dde_id
		    	inner join dossier_subvention on dos_dco_id=dco_id
		    	WHERE dos_id=NEW.dpa_dos_id)));
		    
	    -- Alimente le champ calculé dpa_plafond_aide de la table demande_paiement
	    IF (version LIKE 'V1') THEN
	    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2);
	    ELSEIF (version LIKE 'V2') THEN
	    	IF NEW.dpa_type LIKE 'AVANCE' THEN
		    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2);
		    ELSE
		    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2) - 
		    		((SELECT COALESCE(sum(dpa_aide_demandee), 0) FROM demande_paiement inner join dossier_subvention on dpa_dos_id=dos_id 
		    			WHERE (dos_id=NEW.dpa_dos_id AND dpa_type LIKE 'AVANCE')) - (SELECT COALESCE(sum(round((dpa_montant_trx_realises * dpa_taux_aide) / 100, 2) - dpa_aide_demandee), 0) FROM demande_paiement inner join dossier_subvention on dpa_dos_id=dos_id 
		    			WHERE (dos_id=NEW.dpa_dos_id) AND (dpa_id <> NEW.dpa_id)));
    	   	END IF;
	   	END IF;
    	RETURN NEW;
    END;
$trg_aide_demandee_demande_pai$ LANGUAGE plpgsql;

CREATE TRIGGER trg_aide_demandee_demande_pai BEFORE INSERT OR UPDATE ON demande_paiement
    FOR EACH ROW EXECUTE PROCEDURE calculer_aide_demandee_demande_pai();
    
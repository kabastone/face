CREATE TABLE R_FONGIBILITE
(
	FON_SPR_DONNEUR_ID bigint NOT NULL,
	FON_SPR_RECEVEUR_ID bigint NOT NULL
)
;

ALTER TABLE R_FONGIBILITE
  ADD CONSTRAINT UQ_FONGIBILITE_DONNEUR_RECEVEUR UNIQUE (FON_SPR_DONNEUR_ID,FON_SPR_RECEVEUR_ID)
;

ALTER TABLE R_FONGIBILITE ADD CONSTRAINT CHK_FK_SPR_DONNEUR_RECEVEUR_DIFFERENT CHECK 
(FON_SPR_DONNEUR_ID <> FON_SPR_RECEVEUR_ID)
;

ALTER TABLE R_FONGIBILITE ADD CONSTRAINT FK_FONGIBILITE_DONNEUR
	FOREIGN KEY (FON_SPR_DONNEUR_ID) REFERENCES R_SOUS_PROGRAMME (SPR_ID) ON DELETE CASCADE ON UPDATE No Action
;

ALTER TABLE R_FONGIBILITE ADD CONSTRAINT FK_FONGIBILITE_RECEVEUR
	FOREIGN KEY (FON_SPR_RECEVEUR_ID) REFERENCES R_SOUS_PROGRAMME (SPR_ID) ON DELETE CASCADE ON UPDATE No Action
;

COMMENT ON COLUMN R_FONGIBILITE.FON_SPR_DONNEUR_ID
	IS 'Clé étrangère du sous-preogramme donneur'
; 

COMMENT ON COLUMN R_FONGIBILITE.FON_SPR_RECEVEUR_ID 
	IS 'Clé étrangère du sous-programme receveur'
;

INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-04'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-03'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-04'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-11'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-05'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-03'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-05'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-11'));
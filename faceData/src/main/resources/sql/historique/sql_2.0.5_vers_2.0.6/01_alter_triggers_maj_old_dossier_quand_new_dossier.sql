-- ################### TRIGGERS PLAFOND_AIDE  #############################

-- Trigger qui alimente le champ calculé dos_plafond_aide de la table dossier_subvention
DROP FUNCTION IF EXISTS calculer_plafond_aide_dossier()  CASCADE;
CREATE FUNCTION calculer_plafond_aide_dossier() RETURNS trigger AS $trg_plafond_aide_dossier$
	
	DECLARE
		id_dossier bigint [];
    BEGIN
	    -- Alimente le champ calculé dos_plafond_aide de la table dossier_subvention
	    IF (TG_OP = 'DELETE') THEN
	    	id_dossier = (array[OLD.dsu_dos_id]);
	   	ELSEIF (TG_OP='UPDATE' AND OLD.dsu_dos_id<>NEW.dsu_dos_id) THEN
			id_dossier =(array[OLD.dsu_dos_id, NEW.dsu_dos_id]);
		ELSE
	   		id_dossier = (array[NEW.dsu_dos_id]);
	   	END IF;
	   	
	   	update dossier_subvention as dos 
   		set dos_plafond_aide = (select COALESCE(SUM(dsu.dsu_plafond_aide), 0) from demande_subvention as dsu inner join r_etat_dem_subvention as eds on dsu.dsu_eds_id=eds.eds_id 
	    where (eds.eds_code='ATTRIBUEE' or eds.eds_code='CLOTUREE')  and dsu.dsu_dos_id = dos.dos_id) 
	    where dos.dos_id = any(id_dossier);
	    
	   	update dossier_subvention as dos 
   		set dos_plafond_aide_sans_refus = (select COALESCE(SUM(dsu.dsu_plafond_aide), 0) from demande_subvention as dsu inner join r_etat_dem_subvention as eds on dsu.dsu_eds_id=eds.eds_id 
	    where eds.eds_code<>'REFUSEE' and eds.eds_code<>'REJET_TAUX' and dsu.dsu_dos_id = dos.dos_id) 
	    where dos.dos_id = any(id_dossier);
		
	    RETURN NULL;
    END;
$trg_plafond_aide_dossier$ LANGUAGE plpgsql;

CREATE TRIGGER trg_plafond_aide_dossier AFTER INSERT OR UPDATE OR DELETE ON demande_subvention
    FOR EACH ROW EXECUTE PROCEDURE calculer_plafond_aide_dossier();
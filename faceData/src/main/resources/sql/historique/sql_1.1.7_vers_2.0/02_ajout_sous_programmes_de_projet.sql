insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet)
	values ('794-05','05', 'Transition énergétique', 'TE', (select pro_id from r_programme where pro_code_numerique = '794'), true );
	
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet)
	values ('794-06','06', 'Solution innovante', 'SO', (select pro_id from r_programme where pro_code_numerique = '794'), true );
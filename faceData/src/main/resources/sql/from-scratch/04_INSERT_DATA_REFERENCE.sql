-- Insertion des donnees des tables de reference

SET client_encoding = 'UTF8';

-- departement
insert into departement(dep_code, dep_nom) values ('01','Ain');
insert into departement(dep_code, dep_nom) values ('02','Aisne');
insert into departement(dep_code, dep_nom) values ('03','Allier');
insert into departement(dep_code, dep_nom) values ('04','Alpes-de-Haute-Provence');
insert into departement(dep_code, dep_nom) values ('05','Hautes-Alpes');
insert into departement(dep_code, dep_nom) values ('06','Alpes-Maritimes');
insert into departement(dep_code, dep_nom) values ('07','Ardèche');
insert into departement(dep_code, dep_nom) values ('08','Ardennes');
insert into departement(dep_code, dep_nom) values ('09','Ariège');
insert into departement(dep_code, dep_nom) values ('10','Aube');
insert into departement(dep_code, dep_nom) values ('11','Aude');
insert into departement(dep_code, dep_nom) values ('12','Aveyron');
insert into departement(dep_code, dep_nom) values ('13','Bouches-du-Rhône');
insert into departement(dep_code, dep_nom) values ('14','Calvados');
insert into departement(dep_code, dep_nom) values ('15','Cantal');
insert into departement(dep_code, dep_nom) values ('16','Charente');
insert into departement(dep_code, dep_nom) values ('17','Charente-Maritime');
insert into departement(dep_code, dep_nom) values ('18','Cher');
insert into departement(dep_code, dep_nom) values ('19','Corrèze');
insert into departement(dep_code, dep_nom) values ('21','Côte-d''Or');
insert into departement(dep_code, dep_nom) values ('22','Côtes-d''Armor');
insert into departement(dep_code, dep_nom) values ('23','Creuse');
insert into departement(dep_code, dep_nom) values ('24','Dordogne');
insert into departement(dep_code, dep_nom) values ('25','Doubs');
insert into departement(dep_code, dep_nom) values ('26','Drôme');
insert into departement(dep_code, dep_nom) values ('27','Eure');
insert into departement(dep_code, dep_nom) values ('28','Eure-et-Loir');
insert into departement(dep_code, dep_nom) values ('29','Finistère');
insert into departement(dep_code, dep_nom) values ('2A','Corse-du-Sud');
insert into departement(dep_code, dep_nom) values ('2B','Haute-Corse');
insert into departement(dep_code, dep_nom) values ('30','Gard');
insert into departement(dep_code, dep_nom) values ('31','Haute-Garonne');
insert into departement(dep_code, dep_nom) values ('32','Gers');
insert into departement(dep_code, dep_nom) values ('33','Gironde');
insert into departement(dep_code, dep_nom) values ('34','Hérault');
insert into departement(dep_code, dep_nom) values ('35','Ille-et-Vilaine');
insert into departement(dep_code, dep_nom) values ('36','Indre');
insert into departement(dep_code, dep_nom) values ('37','Indre-et-Loire');
insert into departement(dep_code, dep_nom) values ('38','Isère');
insert into departement(dep_code, dep_nom) values ('39','Jura');
insert into departement(dep_code, dep_nom) values ('40','Landes');
insert into departement(dep_code, dep_nom) values ('41','Loir-et-Cher');
insert into departement(dep_code, dep_nom) values ('42','Loire');
insert into departement(dep_code, dep_nom) values ('43','Haute-Loire');
insert into departement(dep_code, dep_nom) values ('44','Loire-Atlantique');
insert into departement(dep_code, dep_nom) values ('45','Loiret');
insert into departement(dep_code, dep_nom) values ('46','Lot');
insert into departement(dep_code, dep_nom) values ('47','Lot-et-Garonne');
insert into departement(dep_code, dep_nom) values ('48','Lozère');
insert into departement(dep_code, dep_nom) values ('49','Maine-et-Loire');
insert into departement(dep_code, dep_nom) values ('50','Manche');
insert into departement(dep_code, dep_nom) values ('51','Marne');
insert into departement(dep_code, dep_nom) values ('52','Haute-Marne');
insert into departement(dep_code, dep_nom) values ('53','Mayenne');
insert into departement(dep_code, dep_nom) values ('54','Meurthe-et-Moselle');
insert into departement(dep_code, dep_nom) values ('55','Meuse');
insert into departement(dep_code, dep_nom) values ('56','Morbihan');
insert into departement(dep_code, dep_nom) values ('57','Moselle');
insert into departement(dep_code, dep_nom) values ('58','Nièvre');
insert into departement(dep_code, dep_nom) values ('59','Nord');
insert into departement(dep_code, dep_nom) values ('60','Oise');
insert into departement(dep_code, dep_nom) values ('61','Orne');
insert into departement(dep_code, dep_nom) values ('62','Pas-de-Calais');
insert into departement(dep_code, dep_nom) values ('63','Puy-de-Dôme');
insert into departement(dep_code, dep_nom) values ('64','Pyrénées-Atlantiques');
insert into departement(dep_code, dep_nom) values ('65','Hautes-Pyrénées');
insert into departement(dep_code, dep_nom) values ('66','Pyrénées-Orientales');
insert into departement(dep_code, dep_nom) values ('67','Bas-Rhin');
insert into departement(dep_code, dep_nom) values ('68','Haut-Rhin');
insert into departement(dep_code, dep_nom) values ('69','Rhône');
insert into departement(dep_code, dep_nom) values ('70','Haute-Saône');
insert into departement(dep_code, dep_nom) values ('71','Saône-et-Loire');
insert into departement(dep_code, dep_nom) values ('72','Sarthe');
insert into departement(dep_code, dep_nom) values ('73','Savoie');
insert into departement(dep_code, dep_nom) values ('74','Haute-Savoie');
insert into departement(dep_code, dep_nom) values ('75','Paris');
insert into departement(dep_code, dep_nom) values ('76','Seine-Maritime');
insert into departement(dep_code, dep_nom) values ('77','Seine-et-Marne');
insert into departement(dep_code, dep_nom) values ('78','Yvelines');
insert into departement(dep_code, dep_nom) values ('79','Deux-Sèvres');
insert into departement(dep_code, dep_nom) values ('80','Somme');
insert into departement(dep_code, dep_nom) values ('81','Tarn');
insert into departement(dep_code, dep_nom) values ('82','Tarn-et-Garonne');
insert into departement(dep_code, dep_nom) values ('83','Var');
insert into departement(dep_code, dep_nom) values ('84','Vaucluse');
insert into departement(dep_code, dep_nom) values ('85','Vendée');
insert into departement(dep_code, dep_nom) values ('86','Vienne');
insert into departement(dep_code, dep_nom) values ('87','Haute-Vienne');
insert into departement(dep_code, dep_nom) values ('88','Vosges');
insert into departement(dep_code, dep_nom) values ('89','Yonne');
insert into departement(dep_code, dep_nom) values ('90','Territoire de Belfort');
insert into departement(dep_code, dep_nom) values ('91','Essonne');
insert into departement(dep_code, dep_nom) values ('92','Hauts-de-Seine');
insert into departement(dep_code, dep_nom) values ('93','Seine-Saint-Denis');
insert into departement(dep_code, dep_nom) values ('94','Val-de-Marne');
insert into departement(dep_code, dep_nom) values ('95','Val-d''Oise');
insert into departement(dep_code, dep_nom) values ('971','Guadeloupe');
insert into departement(dep_code, dep_nom) values ('972','Martinique');
insert into departement(dep_code, dep_nom) values ('973','Guyane');
insert into departement(dep_code, dep_nom) values ('974','La Réunion');
insert into departement(dep_code, dep_nom) values ('975','St Pierre et Miquelon');
insert into departement(dep_code, dep_nom) values ('976','Mayotte');
insert into departement(dep_code, dep_nom) values ('977','Saint Barthélemy');
insert into departement(dep_code, dep_nom) values ('978','Saint Martin');

-- R_FLUX_CHORUS
INSERT INTO r_flux_chorus(
	fch_centre_de_cout, fch_code_appli, fch_code_demandeur, fch_code_devise, fch_code_mvt, 
	fch_code_societe, fch_code_site, fch_code_tva, fch_condition_paiement, fch_localisation_inter, 
	fch_organisation_achats, fch_pr_uname, fch_domaine_activite, fch_groupe_acheteurs, 
	fch_service_fait_auto_flux_2, fch_schema_partenaire, fch_type_imputation, fch_type_ligne, 
	fch_type_operation, fch_type_option, fch_unite)
	VALUES (
	'ECLDENE092', 'ACE001', '6482', 'EUR', '103', 'ADCE', '316', 'T0', 'Z000', 'N', 'C003', 
	'AMMACEGESTSF', '9450', '01C', '0', 'BENM', 'Z', 'Z_LG_COND', 
	'ZSUB', '0', 'DEV');
	
-- CHORUS_SUIVI_BATCH
INSERT INTO chorus_suivi_batch(csb_fso0051a_ej, csb_fso0051a_sf, csb_fso0051a_dp)
	VALUES (0, 0, 0);

-- r_famille_document
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (100,'DOC_DEMANDE_SUBVENTION','Document de demande de subvention');
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (200,'DOC_DEMANDE_PROLONGATION','Document de demande de prolongation');
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (300,'DOC_DEMANDE_PAIEMENT','Document de demande de paiement');
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (400,'DOC_COURRIER_ANNUEL_DEPARTEMENT','Document des courriers annuels d''un département');
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (500,'DOC_LIGNE_DOTATION_DEPARTEMENT','Document de ligne de dotation de département');
insert into r_famille_document(fdo_id_codifie, fdo_code, fdo_libelle) values (600,'DOC_MODELE','Document modèle de tableur');


-- r_mission
insert into R_MISSION_FACE 
	(MIS_MINISTERE, MIS_DIRECTION, MIS_SOUS_DIRECTION, 
	MIS_MISSION, MIS_ADRESSE, MIS_TEL, MIS_FAX,
	MIS_SIGNATAIRE_FONCTION, MIS_SIGNATAIRE_NOM, MIS_SIGNATAIRE_DCB, 
	MIS_MINISTRE_FONCTION, MIS_MINISTRE_DELEGATION)
	values 
	('MINISTÈRE DE LA TRANSITION ÉCOLOGIE ET SOLIDAIRE', 'Direction de l''énergie', 'Sous-direction du système électrique et des énergies renouvelables',
	'Mission du financement de l''électrification rurale', 'Mission Face - Tour Séquia - 92 055 La Défense Cedex', '01.40.81.97.91', '01.40.81.98.70',
	'L''adjointe du chef de la mission du financement de l''électrification rurale', 'Corinne Lavigne', 'Pour le Contrôleur Budgétaire, et Comptable Ministériel,',
	'Le Ministre de la transition écologie et solidaire', 'Pour le ministre et par délégation');

	
-- r_programme
insert into r_programme(pro_code_numerique, pro_categorie, pro_code_centre_financier, pro_designation, pro_bop, pro_uo) values ('793','10.03.01', '0793-ELEC-RURA', 'Programme principal', 'BOP 0793-ELEC', 'UO 0793-ELEC-RURA');
insert into r_programme(pro_code_numerique, pro_categorie, pro_code_centre_financier, pro_designation, pro_bop, pro_uo) values ('794','11.01.01', '0794-ENER-PROD', 'Programme spécial', 'BOP 0794-ELEC', 'UO 0794-ENER-PRO');



-- r_reglementaire	
insert into R_REGLEMENTAIRE 
	(REG_ANNEE_MISE_EN_APPLICATION, REG_LOI_ORGANIQUE, REG_LOI_ORGANIQUE_MODIFIEE, REG_CODE_COL_TERRITORIALES_ARTICLE_1, REG_CODE_COL_TERRITORIALES_ARTICLE_2, REG_LOI_FINANCES, REG_DECRET_CREDITS_DECOUVERTS_AUTORISES, REG_DECRET_AIDES_ELECTRIFICATION_RURALE, REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE, REG_ARRETE, REG_CIRCULAIRE, REG_CATEGORIE, REG_NB_AN_DEBUT_TRX, REG_NB_AN_FIN_TRX_DEFAUT, REG_NB_AN_FIN_TRX_MAX, REG_TAUX_AIDE_SUBVENTION,
	REG_TAUX_AIDE_PAIEMENT_AVANCE, REG_TAUX_AIDE_PAIEMENT_ACCOMPTE, REG_MONTANT_VISA, REG_VERSION_ALGORITHME)
	values
	('2017', 'n° 2001-692 du 1er août 2001', 'n° 2005-779 du 12 juillet 2005', 'L-2224-31', 'L-224-33', 'n° 2017-1837 du 30 décembre 2017', 'n° 2018-46 du 14 mars 2018', 'n° 2013-46 du 14 janvier 2013', 'n° 2014-496 du 16 mai 2014', 'NON_RENSEIGNE', '3 mars 2017', '63', 1, 3, 4, 80, 10, 90, 1000000, 'V1')
;
	
insert into R_REGLEMENTAIRE 
	(REG_ANNEE_MISE_EN_APPLICATION, REG_LOI_ORGANIQUE, REG_LOI_ORGANIQUE_MODIFIEE, REG_CODE_COL_TERRITORIALES_ARTICLE_1, REG_CODE_COL_TERRITORIALES_ARTICLE_2, REG_LOI_FINANCES, REG_DECRET_CREDITS_DECOUVERTS_AUTORISES, REG_DECRET_AIDES_ELECTRIFICATION_RURALE, REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE, REG_ARRETE, REG_CIRCULAIRE, REG_CATEGORIE, REG_NB_AN_DEBUT_TRX, REG_NB_AN_FIN_TRX_DEFAUT, REG_NB_AN_FIN_TRX_MAX, REG_TAUX_AIDE_SUBVENTION,
	REG_TAUX_AIDE_PAIEMENT_AVANCE, REG_TAUX_AIDE_PAIEMENT_ACCOMPTE, REG_MONTANT_VISA, REG_VERSION_ALGORITHME)
	values
	('2021', 'n° 2001-692 du 1er août 2001', 'n° 2005-779 du 12 juillet 2005', 'L-2224-31', 'L-224-33', 'Vu la loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020', 'Vu le décret n° 2019-1493 du 28 décembre 2019 portant répartition des crédits et découverts autorisés par la loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020', 'n° 2013-46 du 14 janvier 2013', 'n° 2014-496 du 16 mai 2014', 'Vu l''arrêté du 27 mars 2020 relatif à la répartition des aides pour l''année 2020 au bénéfice des autorités organisatrices de réseau de distribution d''énergie pour le financement des travaux d''électrification visés à l''article L-322-6 du code de l''énergie', 'Vu la circulaire du 24 février 2020 relative à la répartition des aides à l''électificatio rurale pour 2020', '63', 1, 3, 4, 80, 20, 90, 1000000, 'V2')
;

	
-- r_sous_programme
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-03','03', 'Renforcement des réseaux', 'AP', (select pro_id from r_programme where pro_code_numerique = '793'), false );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-04','04', 'Extension des réseaux', 'AE', (select pro_id from r_programme where pro_code_numerique = '793'), false );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-05','05', 'Enfouissement et pose en façade', 'CE', (select pro_id from r_programme where pro_code_numerique = '793'), false );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-06','06', 'Sécurisation des fils nus', 'SS', (select pro_id from r_programme where pro_code_numerique = '793'), false );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-07','07', 'Sécurisation des fils nus de faible section', 'SF', (select pro_id from r_programme where pro_code_numerique = '793'), false );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet)
	values ('793-11','11', 'Sécurisation', 'SN', (select pro_id from r_programme where pro_code_numerique = '793'), false );

insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-09','09', 'Déclaration d''utilité publique (DUP)', 'AD', (select pro_id from r_programme where pro_code_numerique = '793'), true );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('793-10','10', 'Intempéries', 'AI', (select pro_id from r_programme where pro_code_numerique = '793'), true );
	
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('794-02','02', 'Sites isolés', 'AR', (select pro_id from r_programme where pro_code_numerique = '794'), true );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('794-03','03', 'Installations de proximité en zone non interconnectée', 'ZI', (select pro_id from r_programme where pro_code_numerique = '794'), true );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet) 
	values ('794-04','04', 'Maîtrise de la demande d''énergie', 'AM', (select pro_id from r_programme where pro_code_numerique = '794'), true );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet)
	values ('794-05','05', 'Transition énergétique', 'TE', (select pro_id from r_programme where pro_code_numerique = '794'), true );
insert into r_sous_programme(spr_code_domaine_fonctionnel, spr_num_sous_action, spr_description, spr_abreviation, spr_pro_id, spr_est_de_projet)
	values ('794-06','06', 'Solution innovante', 'SO', (select pro_id from r_programme where pro_code_numerique = '794'), true );
	
-- r_fongibilite
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-04'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-03'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-04'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-11'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-05'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-03'));
INSERT INTO r_fongibilite(fon_spr_donneur_id, fon_spr_receveur_id) values ((select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-05'),(select spr_id from r_sous_programme where spr_code_domaine_fonctionnel = '793-11'));

-- r_type_anomalie
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_CHO_SUB', 'Anomalie provenant de l''interface avec CHORUS', false);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_CHO_PAI', 'Anomalie provenant de l''interface avec CHORUS', true);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_SUB_0000', 'Autre', false);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_PAI_0000', 'Autre', true);

-- r_type_document
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (101,'ETAT_PREVISIONNEL_PDF','PDF de l''état prévisionnel d''une demande de subvention', 100);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (102,'ETAT_PREVISIONNEL_TABLEUR','Doc tableur de l''état prévisionnel d''une demande de subvention', 100);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (103,'DOC_COMPLEMENTAIRE_SUBVENTION','Document complémentaire d''une demande de subvention', 100);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (104,'DECISION_ATTRIBUTIVE_SUBV','Décision attributive signée d''une demande de subvention', 100);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (105,'FICHE_SIRET','Fiche Siret d''une demande de subvention', 100);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (201,'DEMANDE_PROLONGATION','Demande de prolongation', 200);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (301,'ETAT_MARCHES_PASSES_PDF','PDF de l''état des marchés passés d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (302,'ETAT_MARCHES_PASSES_TABLEUR','Doc tableur de l''état des marchés passés d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (303,'ETAT_REALISATION_TRAVAUX_PDF','PDF de l''état de la réalisation des travaux d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (304,'ETAT_REALISATION_TRAVAUX_TABLEUR','Doc tableur de l''état de la réalisation des travaux d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (305,'ETAT_ACHEVEMENT_TECH_PDF','PDF de l''état d''achèvement technique d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (306,'ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR','Doc tableur de l''état d''achèvement technique d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (307,'ETAT_ACHEVEMENT_FINA_PDF','PDF de l''état d''achèvement financier d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (308,'ETAT_ACHEVEMENT_FINANCIER_TABLEUR','Doc tableur de l''état d''achèvement financier d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (309,'DOC_COMPLEMENTAIRE_PAIEMENT','Document complémentaire d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (310,'DECISION_ATTRIBUTIVE_PAIE','Décision attributive signée d''une demande de paiement', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (311,'CERTIFICAT_PAIEMENT_PDF','Certificat de paiement en PDF (annexe 2)', 300);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (401,'COURRIER_REPARTITION','Courrier de répartition d''une dotation de département', 400);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (402,'COURRIER_DOTATION','Courrier de dotation des lignes de dotation de département', 400);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (501,'RENONCEMENT_MONTANT_DOTATION','Renoncement d''un montant de dotation d''une ligne de dotation de département', 500);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (601,'MODELE_ETAT_PREVISIONNEL','Modèle d''état prévisionnel', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (602,'MODELE_ETAT_MARCHES_PASSES','Modèle d''état des marchés passés', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (603,'MODELE_ETAT_REALISATION','Modèle d''état de réalisation des travaux', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (604,'MODELE_ETAT_ACHEVEMENT_FINANCIER','Modèle d''état d''achèvement financier', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (605,'MODELE_ETAT_ACHEVEMENT_TECHNIQUE_AP_AE','Modèle d''état d''achèvement technique AP/AE', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (606,'MODELE_ETAT_ACHEVEMENT_TECHNIQUE_CE','Modèle d''état d''achèvement technique CE', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (607,'MODELE_ETAT_ACHEVEMENT_TECHNIQUE_SS_SF','Modèle d''état d''achèvement technique SS/SF', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (608,'MODELE_ETAT_ACHEVEMENT_TECHNIQUE_ENR','Modèle d''état d''achèvement technique ENR', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (609,'MODELE_ETAT_ACHEVEMENT_TECHNIQUE_MDE','Modèle d''état d''achèvement technique MDE', 600);
insert into r_type_document(tdo_id_codifie, tdo_code, tdo_libelle, tdo_fdo_id_codifie) 
	values (610,'MODELE_CSV_IMPORT_DOTATION_DEPARTEMENT','Modèle csv du fichier d''import des dotations des départements', 600);	

-- r_etat_dem_subvention
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('DEMANDEE','Demandée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('QUALIFIEE','Qualifiée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('ACCORDEE','Accordée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('CONTROLEE','Contrôlée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('EN_ATTENTE_TRANSFERT_MAN','En attente de transfert manuel');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('EN_ATTENTE_TRANSFERT','En attente de transfert');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('EN_COURS_TRANSFERT','En cours de transfert');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('TRANSFEREE','Transférée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('APPROUVEE','Approuvée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('INCOHERENCE_CHORUS','Incohérence avec CHORUS');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('ANOMALIE_DETECTEE','Détectée en anomalie');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('ANOMALIE_SIGNALEE','Signalée en anomalie');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('CORRIGEE','Corrigée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('REFUSEE','Refusée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('ATTRIBUEE','Attribuée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('CLOTUREE','Clôturée');
insert into r_etat_dem_subvention(eds_code, eds_libelle) values ('REJET_TAUX','Rejetée pour modification de taux');

-- r_etat_dem_prolongation
insert into r_etat_dem_prolongation(edpr_code, edpr_libelle) values ('DEMANDEE','Demandée');
insert into r_etat_dem_prolongation(edpr_code, edpr_libelle) values ('ACCORDEE','Accordée');
insert into r_etat_dem_prolongation(edpr_code, edpr_libelle) values ('INCOHERENCE_CHORUS','Incohérence avec CHORUS');
insert into r_etat_dem_prolongation(edpr_code, edpr_libelle) values ('VALIDEE','Validée');
insert into r_etat_dem_prolongation(edpr_code, edpr_libelle) values ('REFUSEE','Refusée');

-- r_etat_dem_paiement :
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('DEMANDEE','Demandée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('QUALIFIEE','Qualifiée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('ACCORDEE','Accordée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('CONTROLEE','Contrôlée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('EN_ATTENTE_TRANSFERT','En attente de transfert');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('EN_COURS_TRANSFERT_1','En cours de transfert – étape 1');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('EN_COURS_TRANSFERT_2','En cours de transfert – étape 2');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('EN_COURS_TRANSFERT_FIN','En cours de transfert - fin');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('TRANSFEREE','Transférée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('ANOMALIE_DETECTEE','Détectée en anomalie');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('ANOMALIE_SIGNALEE','Signalée en anomalie');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('CORRIGEE','Corrigée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('REFUSEE','Refusée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('VERSEE','Versée');
insert into r_etat_dem_paiement(edpa_code, edpa_libelle) values ('CERTIFIEE','Certifiée');

-- utilisateur : user SYSTEM
insert into utilisateur(UTI_CERBERE_ID, UTI_NOM, UTI_PRENOM, UTI_EMAIL) values ('SYSTEM','System', 'Batch', 'nerepondezpas@developpementdurable.gouv.fr');

-- database metadata
insert into db_metadata(dmd_current_version, dmd_version_date) values ('1.0.0', current_timestamp(0));

-- Fin de transaction
COMMIT;

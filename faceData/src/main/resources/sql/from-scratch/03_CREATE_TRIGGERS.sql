
-- ################### TRIGGERS PLAFOND_AIDE  #############################

-- Trigger qui alimente le champ calculé dsu_plafond_aide de la table demande_subvention
CREATE FUNCTION calculer_plafond_aide_demande_sub() RETURNS trigger AS $trg_plafond_aide_demande_sub$
    BEGIN
	    -- Alimente le champ calculé dsu_plafond_aide de la table demande_subvention
    	NEW.dsu_plafond_aide = round((NEW.dsu_montant_trx_eligible * NEW.dsu_taux_aide) / 100, 2);
    	RETURN NEW;
    END;
$trg_plafond_aide_demande_sub$ LANGUAGE plpgsql;

CREATE TRIGGER trg_plafond_aide_demande_sub BEFORE INSERT OR UPDATE ON demande_subvention
    FOR EACH ROW EXECUTE PROCEDURE calculer_plafond_aide_demande_sub();
    
    
-- Trigger qui alimente le champ calculé dos_plafond_aide de la table dossier_subvention
CREATE FUNCTION calculer_plafond_aide_dossier() RETURNS trigger AS $trg_plafond_aide_dossier$
	DECLARE
		id_dossier bigint [];
    BEGIN
	    -- Alimente le champ calculé dos_plafond_aide de la table dossier_subvention
	    IF (TG_OP = 'DELETE') THEN
	    	id_dossier = (array[OLD.dsu_dos_id]);
	   	ELSEIF (TG_OP='UPDATE' AND OLD.dsu_dos_id<>NEW.dsu_dos_id) THEN
			id_dossier =(array[OLD.dsu_dos_id, NEW.dsu_dos_id]);
		ELSE
	   		id_dossier = (array[NEW.dsu_dos_id]);
	   	END IF;
	   	
	   	update dossier_subvention as dos 
   		set dos_plafond_aide = (select COALESCE(SUM(dsu.dsu_plafond_aide), 0) from demande_subvention as dsu inner join r_etat_dem_subvention as eds on dsu.dsu_eds_id=eds.eds_id 
	    where (eds.eds_code='ATTRIBUEE' or eds.eds_code='CLOTUREE')  and dsu.dsu_dos_id = dos.dos_id) 
	    where dos.dos_id = any(id_dossier);
	    
	   	update dossier_subvention as dos 
   		set dos_plafond_aide_sans_refus = (select COALESCE(SUM(dsu.dsu_plafond_aide), 0) from demande_subvention as dsu inner join r_etat_dem_subvention as eds on dsu.dsu_eds_id=eds.eds_id 
	    where eds.eds_code<>'REFUSEE' and eds.eds_code<>'REJET_TAUX' and dsu.dsu_dos_id = dos.dos_id) 
	    where dos.dos_id = any(id_dossier);
		
	    RETURN NULL;
    END;
$trg_plafond_aide_dossier$ LANGUAGE plpgsql;

CREATE TRIGGER trg_plafond_aide_dossier AFTER INSERT OR UPDATE OR DELETE ON demande_subvention
    FOR EACH ROW EXECUTE PROCEDURE calculer_plafond_aide_dossier();
    

-- ################### TRIGGERS AIDE_DEMANDEE  #############################    
    
-- Trigger qui alimente le champ calculé dpa_aide_demandee de la table demande_paiement
CREATE FUNCTION calculer_aide_demandee_demande_pai() RETURNS trigger AS $trg_aide_demandee_demande_pai$
    
	DECLARE
		version varchar(3);
		
	BEGIN
	    
	    version = (SELECT reg_version_algorithme FROM r_reglementaire WHERE reg_annee_mise_en_application = 
		    (SELECT COALESCE(max(reg_annee_mise_en_application), (SELECT min(reg_annee_mise_en_application) FROM r_reglementaire)) 
		    	FROM r_reglementaire WHERE reg_annee_mise_en_application <=
		    	(SELECT dpr_annee FROM dotation_programme 
		    	inner join dotation_sous_programme on dsp_dpr_id=dpr_id
		    	inner join dotation_departement on dde_dsp_id=dsp_id
		    	inner join dotation_collectivite on dco_dde_id=dde_id
		    	inner join dossier_subvention on dos_dco_id=dco_id
		    	WHERE dos_id=NEW.dpa_dos_id)));
		    
	    -- Alimente le champ calculé dpa_plafond_aide de la table demande_paiement
	    IF (version LIKE 'V1') THEN
	    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2);
	    ELSEIF (version LIKE 'V2') THEN
	    	IF NEW.dpa_type LIKE 'AVANCE' THEN
		    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2);
		    ELSE
		    	NEW.dpa_aide_demandee = round((NEW.dpa_montant_trx_realises * NEW.dpa_taux_aide) / 100, 2) - 
		    		((SELECT COALESCE(sum(dpa_aide_demandee), 0) FROM demande_paiement inner join dossier_subvention on dpa_dos_id=dos_id 
		    			WHERE (dos_id=NEW.dpa_dos_id AND dpa_type LIKE 'AVANCE')) - (SELECT COALESCE(sum(round((dpa_montant_trx_realises * dpa_taux_aide) / 100, 2) - dpa_aide_demandee), 0) FROM demande_paiement inner join dossier_subvention on dpa_dos_id=dos_id 
		    			WHERE (dos_id=NEW.dpa_dos_id) AND (dpa_id <> NEW.dpa_id)));
    	   	END IF;
	   	END IF;
    	RETURN NEW;
    END;
$trg_aide_demandee_demande_pai$ LANGUAGE plpgsql;

CREATE TRIGGER trg_aide_demandee_demande_pai BEFORE INSERT OR UPDATE ON demande_paiement
    FOR EACH ROW EXECUTE PROCEDURE calculer_aide_demandee_demande_pai();
    
    
-- Trigger qui alimente le champ calculé dos_aide_demandee de la table dossier_subvention
CREATE FUNCTION calculer_aide_demandee_dossier() RETURNS trigger AS $trg_aide_demandee_dossier$
	DECLARE
		id_dossier bigint;
    BEGIN
	    -- Alimente le champ calculé dos_aide_demandee de la table dossier_subvention
	    IF (TG_OP = 'DELETE') THEN
	    	id_dossier = OLD.dpa_dos_id;
	   	ELSE
	   		id_dossier = NEW.dpa_dos_id;
	   	END IF;
	   	
	   	update dossier_subvention as dos 
   		set dos_aide_demandee = (
   		select COALESCE(SUM(dpa.dpa_aide_demandee), 0) from demande_paiement as dpa 
   		inner join r_etat_dem_paiement as edp on dpa.dpa_epa_id=edp.edpa_id 
	    where edp.edpa_code<>'REFUSEE' and dpa.dpa_dos_id = dos.dos_id) 
	    where dos.dos_id = id_dossier;
	    
	    RETURN NULL;
    END;
$trg_aide_demandee_dossier$ LANGUAGE plpgsql;

CREATE TRIGGER trg_aide_demandee_dossier AFTER INSERT OR UPDATE OR DELETE ON demande_paiement
    FOR EACH ROW EXECUTE PROCEDURE calculer_aide_demandee_dossier();
    
-- Trigger qui alimente le champ calculé dco_aide_demandee de la table dotation_collectivite
CREATE FUNCTION calculer_aide_demandee_dotation() RETURNS trigger AS $trg_aide_demandee_dotation$
	DECLARE
		id_dotation bigint;
    BEGIN
	    -- Alimente le champ calculé dco_aide_demandee de la table dotation_collectivite
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation = OLD.dos_dco_id;
	   	ELSE
	   		id_dotation = NEW.dos_dco_id;
	   	END IF;
	   	
	   	update dotation_collectivite as dco 
   		set dco_aide_demandee = 
   			(select COALESCE(SUM(dos.dos_aide_demandee), 0) from dossier_subvention as dos where dos.dos_dco_id = dco.dco_id) 
	    where dco.dco_id = id_dotation;
	    
	    RETURN NULL;
    END;
$trg_aide_demandee_dotation$ LANGUAGE plpgsql;

CREATE TRIGGER trg_aide_demandee_dotation AFTER INSERT OR UPDATE OR DELETE ON dossier_subvention
    FOR EACH ROW EXECUTE PROCEDURE calculer_aide_demandee_dotation();
    
    
-- ################### TRIGGERS DOTATION_REPARTIE  #############################    
    
-- Trigger qui alimente le champ calculé dde_dotation_repartie de la table dotation_departement
CREATE FUNCTION calculer_dotation_repartie_departement() RETURNS trigger AS $trg_dotation_repartie_departement$
	DECLARE
		id_dotation_departement bigint;
    BEGIN
	    -- Alimente le champ calculé dde_dotation_repartie de la table dotation_departement
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation_departement = OLD.dco_dde_id;
	   	ELSE
	   		id_dotation_departement = NEW.dco_dde_id;
	   	END IF;
	   	
	   	update dotation_departement as dde 
   		set dde_dotation_repartie = (
   		select COALESCE(SUM(dco.dco_montant), 0) from dotation_collectivite as dco 
	    where dco.dco_dde_id = dde.dde_id) 
	    where dde.dde_id = id_dotation_departement;
	    
	    RETURN NULL;
    END;
$trg_dotation_repartie_departement$ LANGUAGE plpgsql;

CREATE TRIGGER trg_dotation_repartie_departement AFTER INSERT OR UPDATE OR DELETE ON dotation_collectivite
    FOR EACH ROW EXECUTE PROCEDURE calculer_dotation_repartie_departement();
  
    
-- Trigger qui alimente le champ calculé dsp_dotation_repartie de la table dotation_sous_programme
CREATE FUNCTION calculer_dotation_repartie_sous_programme() RETURNS trigger AS $trg_dotation_repartie_sous_programme$
	DECLARE
		id_dotation_sous_programme bigint;
    BEGIN
	    -- Alimente le champ calculé dsp_dotation_repartie de la table dotation_sous_programme
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation_sous_programme = OLD.dde_dsp_id;
	   	ELSE
	   		id_dotation_sous_programme = NEW.dde_dsp_id;
	   	END IF;
	   	
	   	update dotation_sous_programme as dsp 
   		set dsp_dotation_repartie = (
   		select COALESCE(SUM(dde.dde_montant_total), 0) from dotation_departement as dde 
	    where dde.dde_dsp_id = dsp.dsp_id) 
	    where dsp.dsp_id = id_dotation_sous_programme;
	    
	    RETURN NULL;
    END;
$trg_dotation_repartie_sous_programme$ LANGUAGE plpgsql;

CREATE TRIGGER trg_dotation_repartie_sous_programme AFTER INSERT OR UPDATE OR DELETE ON dotation_departement
    FOR EACH ROW EXECUTE PROCEDURE calculer_dotation_repartie_sous_programme();

  
-- Trigger qui alimente le champ calculé dpr_dotation_repartie de la table dotation_programme
CREATE FUNCTION calculer_dotation_repartie_programme() RETURNS trigger AS $trg_dotation_repartie_programme$
	DECLARE
		id_dotation_programme bigint;
    BEGIN
	    -- Alimente le champ calculé dpr_dotation_repartie de la table dotation_programme
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation_programme = OLD.dsp_dpr_id;
	   	ELSE
	   		id_dotation_programme = NEW.dsp_dpr_id;
	   	END IF;
	   	
	   	update dotation_programme as dpr 
   		set dpr_dotation_repartie = (
   		select COALESCE(SUM(dsp.dsp_montant), 0) from dotation_sous_programme as dsp 
	    where dsp.dsp_dpr_id = dpr.dpr_id) 
	    where dpr.dpr_id = id_dotation_programme;
	    
	    RETURN NULL;
    END;
$trg_dotation_repartie_programme$ LANGUAGE plpgsql;

CREATE TRIGGER trg_dotation_repartie_programme AFTER INSERT OR UPDATE OR DELETE ON dotation_sous_programme
    FOR EACH ROW EXECUTE PROCEDURE calculer_dotation_repartie_programme();
    
-- Trigger qui alimente le champ calculé dco_dotation_repartie de la table dotation_collectivite
CREATE FUNCTION calculer_dotation_repartie_collectivite() RETURNS trigger AS $trg_dotation_repartie_collectivite$
	DECLARE
		id_dossier bigint;
    BEGIN
	    -- Alimente le champ calculé dco_dotation_repartie de la table dotation_collectivite
	    IF (TG_OP = 'DELETE') THEN
	    	id_dossier = OLD.dsu_dos_id;
	   	ELSE
	   		id_dossier = NEW.dsu_dos_id;
	   	END IF;
	   	
	   	update dotation_collectivite as dco 
   		set dco_dotation_repartie = (select COALESCE(SUM(dsu.dsu_plafond_aide), 0) 
   		from demande_subvention as dsu inner join r_etat_dem_subvention as eds on dsu.dsu_eds_id=eds.eds_id 
	    inner join dossier_subvention as dos on dos.dos_id = dsu.dsu_dos_id where eds.eds_code!='REFUSEE' and eds.eds_code<>'REJET_TAUX' and dos.dos_dco_id = dco.dco_id) 
	    where exists(select 1 from dossier_subvention as dos2 where dos2.dos_dco_id = dco_id and dos2.dos_id = id_dossier);
	    
	    RETURN NULL;
    END;
$trg_dotation_repartie_collectivite$ LANGUAGE plpgsql;

CREATE TRIGGER trg_dotation_repartie_collectivite AFTER INSERT OR UPDATE OR DELETE ON demande_subvention
    FOR EACH ROW EXECUTE PROCEDURE calculer_dotation_repartie_collectivite();

    
-- ################### TRIGGER MONTANT_DEPARTEMENT  #############################    
  
-- Trigger qui alimente le champ calculé montant_total et montant_notifie de la table dotation_departement
CREATE FUNCTION calculer_montant_dotation_departement() RETURNS trigger AS $trg_montant_departement$
	DECLARE
		id_dotation_departement bigint;
    BEGIN
	    -- Alimente le champ calculé montant_total et montant_notifie de la table dotation_departement
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation_departement = OLD.ldd_dde_id;
	   	ELSE
	   		id_dotation_departement = NEW.ldd_dde_id;
	   	END IF;
	   	
	   	update dotation_departement as dde 
   		set dde_montant_total = (
   		(select COALESCE(SUM(ldd.ldd_montant), 0) from dotation_departement_ligne as ldd  
	    where ldd.ldd_dde_id = dde.dde_id)
			+ (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
				inner join dotation_collectivite as dco on trf_dco_id_destination=dco.dco_id
				where dco.dco_dde_id=dde.dde_id)
			- (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
			inner join dotation_collectivite as dco on trf_dco_id_origine=dco.dco_id
			where dco.dco_dde_id=dde.dde_id)
		)
	    where dde.dde_id = id_dotation_departement;
	    
	   	update dotation_departement as dde 
   		set dde_montant_notifie = ((
   		select COALESCE(SUM(ldd.ldd_montant), 0) from dotation_departement_ligne as ldd  
	    where ldd.ldd_dde_id = dde.dde_id
	    and ldd.ldd_date_envoi is not null)
			+ (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
				inner join dotation_collectivite as dco on trf_dco_id_destination=dco.dco_id
				where dco.dco_dde_id=dde.dde_id)
			- (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
			inner join dotation_collectivite as dco on trf_dco_id_origine=dco.dco_id
			where dco.dco_dde_id=dde.dde_id)
		)
	    where dde.dde_id = id_dotation_departement;
	    
	    RETURN NULL;
    END;
$trg_montant_departement$ LANGUAGE plpgsql;

CREATE TRIGGER trg_montant_departement AFTER INSERT OR UPDATE OR DELETE ON dotation_departement_ligne
    FOR EACH ROW EXECUTE PROCEDURE calculer_montant_dotation_departement();

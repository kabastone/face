-- Trigger qui alimente le champ calculé montant_total et montant_notifie de la table dotation_departement
DROP FUNCTION IF EXISTS calculer_montant_dotation_departement()  CASCADE;
CREATE FUNCTION calculer_montant_dotation_departement() RETURNS trigger AS $trg_montant_departement$
	DECLARE
		id_dotation_departement bigint;
    BEGIN
	    -- Alimente le champ calculé montant_total et montant_notifie de la table dotation_departement
	    IF (TG_OP = 'DELETE') THEN
	    	id_dotation_departement = OLD.ldd_dde_id;
	   	ELSE
	   		id_dotation_departement = NEW.ldd_dde_id;
	   	END IF;
	   	
	   	update dotation_departement as dde 
   		set dde_montant_total = (
   		(select COALESCE(SUM(ldd.ldd_montant), 0) from dotation_departement_ligne as ldd  
	    where ldd.ldd_dde_id = dde.dde_id)
			+ (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
				inner join dotation_collectivite as dco on trf_dco_id_destination=dco.dco_id
				where dco.dco_dde_id=dde.dde_id)
			- (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
			inner join dotation_collectivite as dco on trf_dco_id_origine=dco.dco_id
			where dco.dco_dde_id=dde.dde_id)
		)
	    where dde.dde_id = id_dotation_departement;
	    
	   	update dotation_departement as dde 
   		set dde_montant_notifie = ((
   		select COALESCE(SUM(ldd.ldd_montant), 0) from dotation_departement_ligne as ldd  
	    where ldd.ldd_dde_id = dde.dde_id
	    and ldd.ldd_date_envoi is not null)
			+ (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
				inner join dotation_collectivite as dco on trf_dco_id_destination=dco.dco_id
				where dco.dco_dde_id=dde.dde_id)
			- (select COALESCE(SUM(trf.trf_montant), 0) from transfert_fongibilite as trf
			inner join dotation_collectivite as dco on trf_dco_id_origine=dco.dco_id
			where dco.dco_dde_id=dde.dde_id)
		)
	    where dde.dde_id = id_dotation_departement;
	    
	    RETURN NULL;
    END;
$trg_montant_departement$ LANGUAGE plpgsql;

CREATE TRIGGER trg_montant_departement AFTER INSERT OR UPDATE OR DELETE ON dotation_departement_ligne
    FOR EACH ROW EXECUTE PROCEDURE calculer_montant_dotation_departement();
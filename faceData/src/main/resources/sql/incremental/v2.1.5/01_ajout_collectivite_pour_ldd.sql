ALTER TABLE dotation_departement_ligne ADD COLUMN ldd_col_id bigint;
ALTER TABLE dotation_departement_ligne ADD CONSTRAINT FK_LDD_COLLECTIVITE_ID
	FOREIGN KEY (LDD_COL_ID) REFERENCES collectivite (col_id) ON DELETE No Action ON UPDATE No Action
;
ALTER TABLE dotation_departement_ligne ADD CONSTRAINT CHK_COL_LDD_ID CHECK (ldd_col_id is not null OR ldd_type='EXCEPTIONNELLE' OR ldd_type='ANNUELLE')
;
ALTER TABLE r_reglementaire drop constraint if exists pk_reglementaire;
ALTER TABLE r_reglementaire drop constraint if exists chk_reglementaire_unique_enregistrement;
ALTER TABLE r_reglementaire drop column if exists reg_actif;
ALTER TABLE r_reglementaire add column reg_annee_mise_en_application integer default 2000;
ALTER TABLE r_reglementaire add constraint pk_reglementaire PRIMARY KEY (reg_annee_mise_en_application);
ALTER TABLE r_reglementaire add constraint uq_annee_application UNIQUE (reg_annee_mise_en_application);
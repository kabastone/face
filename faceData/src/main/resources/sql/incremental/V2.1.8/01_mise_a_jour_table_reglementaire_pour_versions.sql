DROP TABLE IF EXISTS R_REGLEMENTAIRE CASCADE;

CREATE TABLE R_REGLEMENTAIRE
(
	REG_ANNEE_MISE_EN_APPLICATION integer NOT NULL DEFAULT 2000,
	REG_LOI_ORGANIQUE varchar(150) NOT NULL,
	REG_LOI_ORGANIQUE_MODIFIEE varchar(150) NOT NULL,
	REG_CODE_COL_TERRITORIALES_ARTICLE_1 varchar(150) NOT NULL,
	REG_CODE_COL_TERRITORIALES_ARTICLE_2 varchar(150) NOT NULL,
	REG_LOI_FINANCES varchar(300) NOT NULL,
	REG_DECRET_CREDITS_DECOUVERTS_AUTORISES varchar(300) NOT NULL,
	REG_DECRET_AIDES_ELECTRIFICATION_RURALE varchar(150) NOT NULL,
	REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE varchar(150) NOT NULL,
	REG_ARRETE varchar(300) NOT NULL,
	REG_CIRCULAIRE varchar(300) NOT NULL,
	REG_CATEGORIE varchar(150) NOT NULL,
	REG_NB_AN_DEBUT_TRX integer NOT NULL,
	REG_NB_AN_FIN_TRX_DEFAUT integer NOT NULL,
	REG_NB_AN_FIN_TRX_MAX integer NOT NULL,
	REG_TAUX_AIDE_SUBVENTION integer NOT NULL,
	REG_TAUX_AIDE_PAIEMENT_AVANCE integer NOT NULL,
	REG_TAUX_AIDE_PAIEMENT_ACCOMPTE integer NOT NULL,
	REG_MONTANT_VISA integer NOT NULL,
	REG_VERSION_ALGORITHME varchar(3) NOT NULL
);

ALTER TABLE R_REGLEMENTAIRE ADD CONSTRAINT PK_REGLEMENTAIRE
	PRIMARY KEY (REG_ANNEE_MISE_EN_APPLICATION)
;

ALTER TABLE R_REGLEMENTAIRE ADD CONSTRAINT UQ_DATE_PUBLICATION
	UNIQUE (REG_ANNEE_MISE_EN_APPLICATION)
;

ALTER TABLE R_REGLEMENTAIRE ADD CONSTRAINT CHK_VERSION_ALGORITHME CHECK 
	(reg_version_algorithme::text = 'V1'::text 
	OR reg_version_algorithme::text = 'V2'::text)
;

/** COMMENT Table R_REGLEMENTAIRE */
COMMENT ON COLUMN R_REGLEMENTAIRE.REG_ANNEE_MISE_EN_APPLICATION
	IS 'Clé primaire basée sur l''année suivant la publication du décret'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_LOI_ORGANIQUE
	IS 'n° et date de la loi organique relative aux lois de finances'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_LOI_ORGANIQUE_MODIFIEE
	IS 'n° et date de la loi modifiant la loi organique relative aux lois de finances'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_CODE_COL_TERRITORIALES_ARTICLE_1
	IS '1er article du code générale des collectivités territoriales'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_CODE_COL_TERRITORIALES_ARTICLE_2
	IS '2ème article du code générale des collectivités territoriales'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_LOI_FINANCES
	IS 'n° et date de la loi de finances'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_DECRET_CREDITS_DECOUVERTS_AUTORISES
	IS 'n° et date du décret portant répartition des crédits et découverts autorisés par la loi de finances'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_DECRET_AIDES_ELECTRIFICATION_RURALE
	IS 'n° et date du décret relatif aux aides pour l''électrification rurale'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE
	IS 'n° et date du décret modificatif relatif aux aides pour l''électrification rurale'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_ARRETE
	IS 'Date de l''arrêté relatif à la répartition annuelle des aides'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_CIRCULAIRE
	IS 'Date de la circulaire relative à la répartition des aides à l''électrification rurale'
;

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_CATEGORIE 
	IS 'Catégorie des programmes du FACE'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_NB_AN_DEBUT_TRX 
	IS 'Nombre d''années pour lancer les premiers travaux; permet de calculer l''échéance de lancement d''une demande de subvention'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_NB_AN_FIN_TRX_DEFAUT 
	IS 'Nombre d''années pour achever les derniers travaux; permet de calculer l''échéance d''achevement d''une demande de subvention'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_NB_AN_FIN_TRX_MAX 
	IS 'Nombre d''années pour achever les derniers travaux au maximum dans le cas de prologation; permet de calculer l''échéance d''achevement demandée dans une demande de prolongation'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_TAUX_AIDE_SUBVENTION 
	IS 'Taux d''aide maximun pour une demande de subvention'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_TAUX_AIDE_PAIEMENT_AVANCE 
	IS 'Taux d''aide par rapport au montant de subvention, pour une demande de paiement de type avance'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_TAUX_AIDE_PAIEMENT_ACCOMPTE 
	IS 'Maximun du taux d''aide par rapport au montant de subvention, pour une demande de paiement de type accompte'
; 

COMMENT ON COLUMN R_REGLEMENTAIRE.REG_MONTANT_VISA 
	IS 'Montant maximun de la suvention qui necessite le visa de DCB'
;

insert into R_REGLEMENTAIRE 
	(REG_ANNEE_MISE_EN_APPLICATION, REG_LOI_ORGANIQUE, REG_LOI_ORGANIQUE_MODIFIEE, REG_CODE_COL_TERRITORIALES_ARTICLE_1, REG_CODE_COL_TERRITORIALES_ARTICLE_2, REG_LOI_FINANCES, REG_DECRET_CREDITS_DECOUVERTS_AUTORISES, REG_DECRET_AIDES_ELECTRIFICATION_RURALE, REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE, REG_ARRETE, REG_CIRCULAIRE, REG_CATEGORIE, REG_NB_AN_DEBUT_TRX, REG_NB_AN_FIN_TRX_DEFAUT, REG_NB_AN_FIN_TRX_MAX, REG_TAUX_AIDE_SUBVENTION,
	REG_TAUX_AIDE_PAIEMENT_AVANCE, REG_TAUX_AIDE_PAIEMENT_ACCOMPTE, REG_MONTANT_VISA, REG_VERSION_ALGORITHME)
	values
	('2017', 'n° 2001-692 du 1er août 2001', 'n° 2005-779 du 12 juillet 2005', 'L-2224-31', 'L-224-33', 'n° 2017-1837 du 30 décembre 2017', 'n° 2018-46 du 14 mars 2018', 'n° 2013-46 du 14 janvier 2013', 'n° 2014-496 du 16 mai 2014', 'NON_RENSEIGNE', '3 mars 2017', '63', 1, 3, 4, 80, 10, 90, 1000000, 'V1')
;
	
insert into R_REGLEMENTAIRE 
	(REG_ANNEE_MISE_EN_APPLICATION, REG_LOI_ORGANIQUE, REG_LOI_ORGANIQUE_MODIFIEE, REG_CODE_COL_TERRITORIALES_ARTICLE_1, REG_CODE_COL_TERRITORIALES_ARTICLE_2, REG_LOI_FINANCES, REG_DECRET_CREDITS_DECOUVERTS_AUTORISES, REG_DECRET_AIDES_ELECTRIFICATION_RURALE, REG_DECRET_MODIFICATIF_AIDES_ELECTRIFICATION_RURALE, REG_ARRETE, REG_CIRCULAIRE, REG_CATEGORIE, REG_NB_AN_DEBUT_TRX, REG_NB_AN_FIN_TRX_DEFAUT, REG_NB_AN_FIN_TRX_MAX, REG_TAUX_AIDE_SUBVENTION,
	REG_TAUX_AIDE_PAIEMENT_AVANCE, REG_TAUX_AIDE_PAIEMENT_ACCOMPTE, REG_MONTANT_VISA, REG_VERSION_ALGORITHME)
	values
	('2021', 'n° 2001-692 du 1er août 2001', 'n° 2005-779 du 12 juillet 2005', 'L-2224-31', 'L-224-33', 'Vu la loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020', 'Vu le décret n° 2019-1493 du 28 décembre 2019 portant répartition des crédits et découverts autorisés par la loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020', 'n° 2013-46 du 14 janvier 2013', 'n° 2014-496 du 16 mai 2014', 'Vu l''arrêté du 27 mars 2020 relatif à la répartition des aides pour l''année 2020 au bénéfice des autorités organisatrices de réseau de distribution d''énergie pour le financement des travaux d''électrification visés à l''article L-322-6 du code de l''énergie', 'Vu la circulaire du 24 février 2020 relative à la répartition des aides à l''électificatio rurale pour 2020', '63', 1, 3, 4, 80, 20, 90, 1000000, 'V2')
;
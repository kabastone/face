/**
 *
 */
package fr.gouv.sitde.face.application.dotation.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication;
import fr.gouv.sitde.face.application.dto.DotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleTableauPertesRenoncementDto;
import fr.gouv.sitde.face.application.mapping.DotationDepartementaleMapper;
import fr.gouv.sitde.face.application.mapping.LigneDotationDepartementaleMapper;
import fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService;
import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleBo;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Class GestionDotationDepartementApplicationImpl.
 */
@Service
@Transactional
public class GestionDotationDepartementApplicationImpl implements GestionDotationDepartementApplication {

    /** The donnees document service. */
    @Inject
    private DonneesDocumentService donneesDocumentService;

    /** The dotation departement service. */
    @Inject
    private DotationDepartementService dotationDepartementService;

    /** The ligne dotation departement service. */
    @Inject
    private LigneDotationDepartementService ligneDotationDepartementService;

    /** The dotation programme mapper. */
    @Inject
    private DotationDepartementaleMapper dotationDepartementaleMapper;

    /** The ligne dotation programme mapper. */
    @Inject
    private LigneDotationDepartementaleMapper ligneDotationDepartementaleMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.departement.GenerationAnnexeDotationApplication#genererAnnexeDotationPdf(java.lang.Integer)
     */
    @Override
    public FichierTransfert genererAnnexeDotationPdf(Integer idDepartement) {
        return this.donneesDocumentService.genererAnnexeDotationPdf(idDepartement);
    }

    /*
     *
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#genererAnnexeDotationPdf()
     */
    @Override
    public FichierTransfert genererAnnexeDotationPdf() {
        return this.donneesDocumentService.genererAnnexeDotationPdf();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#televerserEtNotifierDotations(java.lang.Integer,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public void televerserEtNotifierDotations(Integer idDepartement, FichierTransfert fichierCourrierDotation) {
        this.dotationDepartementService.televerserEtNotifierDotationDepartement(idDepartement, fichierCourrierDotation);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#rechercherDotationsDepartementalesParAnneeEnCours(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationDepartementaleDto> rechercherDotationsDepartementalesParAnneeEnCours(PageDemande pageDemande) {
        PageReponse<DotationDepartementaleBo> pageReponseDotations = this.dotationDepartementService
                .rechercherDotationsDepartementalesParAnneeEnCours(pageDemande);
        List<DotationDepartementaleDto> listeResultatsDto = this.dotationDepartementaleMapper
                .dotationDepartementaleBoToDotationDepartementaleDto(pageReponseDotations.getListeResultats());
        return new PageReponse<>(listeResultatsDto, pageReponseDotations.getNbTotalResultats());
    }

    /**
     * Lister les lignes de dotation départementale en préparation pour l'année en cours.
     *
     * @param idDepartement
     * @return La liste des lignes de dotation départementale en préparation pour l'année en cours
     */
    @Override
    public List<LigneDotationDepartementaleDto> rechercherLignesDotationDepartementaleEnPreparation(Integer idDepartement) {
        List<LigneDotationDepartement> liste = this.ligneDotationDepartementService
                .rechercherLignesDotationDepartementaleEnPreparation(idDepartement);
        return this.ligneDotationDepartementaleMapper.ligneDotationDepartementToLigneDotationDepartementaleDto(liste);
    }

    /**
     * Lister les lignes de dotation départementale notifiées pour l'année en cours.
     *
     * @param idDepartement
     * @return La liste des lignes de dotation départementale notifiées pour l'année en cours
     */
    @Override
    public List<LigneDotationDepartementaleDto> rechercherLignesDotationDepartementaleNotifiees(Integer idDepartement) {
        List<LigneDotationDepartement> liste = this.ligneDotationDepartementService.rechercherLignesDotationDepartementaleNotifiees(idDepartement);
        return this.ligneDotationDepartementaleMapper.ligneDotationDepartementToLigneDotationDepartementaleDto(liste);
    }

    /**
     * Supprimer une ligne de dotation départementale.
     *
     * @param idLigneDotationDepartementale
     */
    @Override
    public void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale) {
        this.ligneDotationDepartementService.detruireLigneDotationDepartementale(idLigneDotationDepartementale);
    }

    /**
     * Ajouter une ligne de dotation départementale.
     *
     * @param ligneDotationDepartementaleDto
     * @return la nouvelle ligne de dotation départementale
     */
    @Override
    public LigneDotationDepartementaleDto ajouterLigneDotationDepartementale(LigneDotationDepartementaleDto ligneDotationDepartementaleDto) {
        LigneDotationDepartement ligneDotationDepartement = this.ligneDotationDepartementaleMapper
                .ligneDotationDepartementaleDtoToLigneDotationDepartement(ligneDotationDepartementaleDto);
        ligneDotationDepartement = this.ligneDotationDepartementService.ajouterLigneDotationDepartement(ligneDotationDepartement,
                TypeDotationDepartementEnum.EXCEPTIONNELLE);
        return this.ligneDotationDepartementaleMapper.ligneDotationDepartementToLigneDotationDepartementaleDto(ligneDotationDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#rechercherDocumentNotification(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentNotification(Long idLigne) {
        return this.ligneDotationDepartementService.rechercherDocumentNotificationPourLigne(idLigne);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#listerLignesDotationsCollectivitePertesEtRenoncement(java.lang.
     * Long)
     */
    @Override
    public List<LigneDotationDepartementaleTableauPertesRenoncementDto> listerLignesDotationsCollectivitePertesEtRenoncement(Long idCollectivite) {
        List<LigneDotationDepartement> listeLignes = this.ligneDotationDepartementService
                .rechercherLignesPertesEtRenonciationsParIdCollectivite(idCollectivite);
        return this.ligneDotationDepartementaleMapper.ligneDotationDepartementsToLigneDotationDepartementaleTableauPertesRenoncementDtos(listeLignes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication#listerLignesPertesEtRenoncement(java.lang.String)
     */
    @Override
    public List<LigneDotationDepartementaleTableauPertesRenoncementDto> listerLignesPertesEtRenoncement(String codeProgramme) {
        List<LigneDotationDepartement> listeLignes = this.ligneDotationDepartementService.rechercherLignesPertesEtRenonciations(codeProgramme);
        return this.ligneDotationDepartementaleMapper.ligneDotationDepartementsToLigneDotationDepartementaleTableauPertesRenoncementDtos(listeLignes);
    }
}

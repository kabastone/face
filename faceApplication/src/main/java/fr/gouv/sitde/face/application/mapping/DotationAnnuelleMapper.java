package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import fr.gouv.sitde.face.application.dto.DotationAnnuelleDotationCollectiviteDto;
import fr.gouv.sitde.face.application.dto.DotationAnnuelleDto;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * Mapper entités/DTO dotation annuelle.
 *
 * @author a627888
 *
 */
@Mapper(uses = { DocumentMapper.class }, componentModel = "spring")
public interface DotationAnnuelleMapper {

    DotationAnnuelleDto paramsToDotationAnnuelleDto(Long idCollectivite, Integer annee, List<Document> documents,
            List<DotationCollectivite> dotationsCollectiviteProgrammePrincipal, List<DotationCollectivite> dotationsCollectiviteProgrammeSpecial);

    @Mapping(target = "idDotationCollectivite", source = "id")
    @Mapping(target = "numSousAction", source = "dotationDepartement.dotationSousProgramme.sousProgramme", qualifiedByName = "numeroSousProgramme")
    @Mapping(target = "abreviation", source = "dotationDepartement.dotationSousProgramme.sousProgramme.abreviation")
    @Mapping(target = "description", source = "dotationDepartement.dotationSousProgramme.sousProgramme.description")
    DotationAnnuelleDotationCollectiviteDto dotationCollectiviteToDotationAnnuelleDotationCollectiviteDto(DotationCollectivite dotationCollectivite);

    @Named("numeroSousProgramme")
    default String donnerNumeroSousProgramme(SousProgramme sp) {
        return sp.getAbreviation().equals(ConstantesFace.CODE_SP_SECURISATION) ? ConstantesFace.NUMERO_SP_SECURISATION : sp.getNumSousAction();
    }

    List<DotationAnnuelleDotationCollectiviteDto> dotationCollectiviteToDotationAnnuelleDotationCollectiviteDto(
            List<DotationCollectivite> dotationsCollectivites);

}

package fr.gouv.sitde.face.application.paiement;

import fr.gouv.sitde.face.application.dto.DemandePaiementDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface GestionDemandePaiementApplication.
 */
public interface GestionDemandePaiementApplication {

    /**
     * Pour réaliser une nouvelle demande de paiment : <br/>
     * on initialise juste le numéro de dossier de subvention <br/>
     * ainsi que le montant de l'aide disponible.
     *
     * @param idDossierSubvention the id dossier subvention
     * @return the demande paiement dto
     */
    DemandePaiementDto initNouvelleDemandePaiement(Long idDossierSubvention);

    /**
     * Effectue une demande de paiement sur un dossier de subvention <br/>
     * ou met à jour une demande existante.
     *
     * @param demandePaiementDto the demande paiement dto
     *
     * @return the demande paiement dto
     */
    DemandePaiementDto realiserDemandePaiement(DemandePaiementDto demandePaiementDto);

    /**
     * Recherche d'une demande de paiement.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto rechercheDemandePaiement(Long idDemandePaiement);

    /**
     * Génération de la décision attributive d'une demande de paiement.
     *
     * @param idDemandePaiement            the id demande paiement
     * @return the fichier transfert
     */
    FichierTransfert genererDecisionAttributive(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : ANOMALIE_DETECTEE.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementAnomalieDetectee(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : ANOMALIE_SIGNALEE.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementAnomalieSignalee(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : REFUSEE.
     *
     * @param idDemandePaiement the id demande paiement
     * @param motifRefus motif de refus
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementRefusee(Long idDemandePaiement, String motifRefus);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : QUALIFIEE.
     *
     * @param demandePaiementDto the demande paiement dto
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementQualifiee(DemandePaiementDto demandePaiementDto);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : ACCORDEE.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementAccordee(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : CONTROLEE.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementControlee(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : EN_ATTENTE_TRANSFERT.
     *
     * @param idDemandePaiement the id demande paiement
     * @return the demande paiement dto
     */
    DemandePaiementDto majDemandePaiementEnAttenteTransfert(Long idDemandePaiement);

    /**
     * Met à jour l'état de la demande de paiement avec l'état : CORRIGEE.
     * Met à jour tous les champs du formulaire + potentiellement tous les fichiers à uploader.
     *
     * @param demandePaiementDto the demande paiement dto
     * @return the demande paiement dto
     */
    DemandePaiementDto enregistrerDemandePaiementPourCorrection(DemandePaiementDto demandePaiementDto);
}

/**
 *
 */
package fr.gouv.sitde.face.application.jsonutils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class CustomLocalDateTimeSerializer.
 *
 * @author A754839
 */
public class CustomLocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5354911420674622415L;

    /**
     * Instantiates a new custom offset date time serializer.
     */
    public CustomLocalDateTimeSerializer() {
        this(null);
    }

    /**
     * Instantiates a new custom offset date time serializer.
     *
     * @param t
     *            the LocalDateTime
     */
    protected CustomLocalDateTimeSerializer(Class<LocalDateTime> t) {
        super(t);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.fasterxml.jackson.databind.ser.std.StdSerializer#serialize(java.lang.Object, com.fasterxml.jackson.core.JsonGenerator,
     * com.fasterxml.jackson.databind.SerializerProvider)
     */
    @Override
    public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider provider) {

        try {
            gen.writeString(formatter.format(value));
        } catch (IOException e) {
            throw new TechniqueException("Problème lors de la serialization de la LocalDateTime", e);
        }
    }

}

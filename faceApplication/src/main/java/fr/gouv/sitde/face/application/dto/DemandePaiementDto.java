package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Class DemandePaiementSimpleDto.
 */
public class DemandePaiementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8788990213515937533L;

    /** The num dossier. */
    private String numDossier;

    /** The reste consommer. */
    private BigDecimal resteConsommer;

    /** The id dossier subvention. */
    private Long idDossierSubvention;

    /** The id demande. */
    private Long idDemande;

    /** The nom long. */
    private String nomLongCollectivite;

    /** The code departement. */
    private String codeDepartement;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateDemande;

    /** The type demande. */
    private TypeDemandePaiementLovDto typeDemande;

    /** The montant travaux ht. */
    private BigDecimal montantTravauxHt;

    /** The taux aide face percent. */
    private BigDecimal tauxAideFacePercent;

    /** The taux avance reglementaire. */
    private BigDecimal tauxAvanceReglementaire;

    /** The aide demandee. */
    private BigDecimal aideDemandee;

    /** The montant restant avance. */
    private BigDecimal montantRestantAvance;

    /** The montant total ht travaux. */
    private BigDecimal montantTotalHtTravaux;

    /** The montant restant acompte. */
    private BigDecimal montantRestantAcompte;

    /** The montant subvention. */
    private BigDecimal montantSubvention;

    /** The etat marches passes. */
    private String etatMarchesPasses;

    /** The etat realisation travaux. */
    private String etatRealisationTravaux;

    /** The etat achevement financier. */
    private String etatAchevementFinancier;

    /** The etat achevenement tech. */
    private String etatAchevenementTech;

    /** The etat instruction. */
    private String etatInstruction;

    /** The motif refus. */
    private String motifRefus;

    /** The code etat demande. */
    private String codeEtatDemande;

    /** The libelle etat demande. */
    private String libelleEtatDemande;

    /** The version. */
    private int version;

    /** The is premiere demande. */
    private Boolean isPremiereDemande;

    /** The set has un acompte. */
    private Boolean hasUnAcompte;
    
    /** The message. */
    private String message;
    
    /** The liste fichiers envoyer. */
    private List<FichierTransfert> listeFichiersEnvoyer = new ArrayList<>();

    /** The liste documents. */
    private List<DocumentDto> documents = new ArrayList<>();

    /** The anomalies. */
    private Set<AnomalieDto> anomalies = new LinkedHashSet<>(0);

    /** The ids docs compl suppr. */
    private List<Long> idsDocsComplSuppr = new ArrayList<>();

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the reste consommer.
     *
     * @return the resteConsommer
     */
    public BigDecimal getResteConsommer() {
        return this.resteConsommer;
    }

    /**
     * Sets the reste consommer.
     *
     * @param resteConsommer
     *            the resteConsommer to set
     */
    public void setResteConsommer(BigDecimal resteConsommer) {
        this.resteConsommer = resteConsommer;
    }

    /**
     * Gets the id dossier subvention.
     *
     * @return the idDossierSubvention
     */
    public Long getIdDossierSubvention() {
        return this.idDossierSubvention;
    }

    /**
     * Sets the id dossier subvention.
     *
     * @param idDossierSubvention
     *            the idDossierSubvention to set
     */
    public void setIdDossierSubvention(Long idDossierSubvention) {
        this.idDossierSubvention = idDossierSubvention;
    }

    /**
     * Gets the id demande.
     *
     * @return the idDemande
     */
    public Long getIdDemande() {
        return this.idDemande;
    }

    /**
     * Sets the id demande.
     *
     * @param idDemande
     *            the idDemande to set
     */
    public void setIdDemande(Long idDemande) {
        this.idDemande = idDemande;
    }

    /**
     * Gets the LocalDateTime demande.
     *
     * @return the dateDemande
     */
    public LocalDateTime getDateDemande() {
        return this.dateDemande;
    }

    /**
     * Sets the LocalDateTime demande.
     *
     * @param dateDemande
     *            the dateDemande to set
     */
    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    /**
     * Gets the type demande.
     *
     * @return the typeDemande
     */
    public TypeDemandePaiementLovDto getTypeDemande() {
        return this.typeDemande;
    }

    /**
     * Sets the type demande.
     *
     * @param typeDemande
     *            the typeDemande to set
     */
    public void setTypeDemande(TypeDemandePaiementLovDto typeDemande) {
        this.typeDemande = typeDemande;
    }

    /**
     * Gets the montant travaux ht.
     *
     * @return the montantTravauxHt
     */
    public BigDecimal getMontantTravauxHt() {
        return this.montantTravauxHt;
    }

    /**
     * Sets the montant travaux ht.
     *
     * @param montantTravauxHt
     *            the montantTravauxHt to set
     */
    public void setMontantTravauxHt(BigDecimal montantTravauxHt) {
        this.montantTravauxHt = montantTravauxHt;
    }

    /**
     * Gets the taux aide face percent.
     *
     * @return the tauxAideFacePercent
     */
    public BigDecimal getTauxAideFacePercent() {
        return this.tauxAideFacePercent;
    }

    /**
     * Sets the taux aide face percent.
     *
     * @param bigDecimal
     *            the tauxAideFacePercent to set
     */
    public void setTauxAideFacePercent(BigDecimal bigDecimal) {
        this.tauxAideFacePercent = bigDecimal;
    }

    /**
     * Gets the taux avance reglementaire.
     *
     * @return the tauxAvanceReglementaire
     */
    public BigDecimal getTauxAvanceReglementaire() {
        return this.tauxAvanceReglementaire;
    }

    /**
     * Sets the taux avance reglementaire.
     *
     * @param tauxAvanceReglementaire
     *            the tauxAvanceReglementaire to set
     */
    public void setTauxAvanceReglementaire(BigDecimal tauxAvanceReglementaire) {
        this.tauxAvanceReglementaire = tauxAvanceReglementaire;
    }

    /**
     * Gets the aide demandee.
     *
     * @return the aideDemandee
     */
    public BigDecimal getAideDemandee() {
        return this.aideDemandee;
    }

    /**
     * Sets the aide demandee.
     *
     * @param aideDemandee
     *            the aideDemandee to set
     */
    public void setAideDemandee(BigDecimal aideDemandee) {
        this.aideDemandee = aideDemandee;
    }

    /**
     * Gets the etat marches passes.
     *
     * @return the etatMarchesPasses
     */
    public String getEtatMarchesPasses() {
        return this.etatMarchesPasses;
    }

    /**
     * Sets the etat marches passes.
     *
     * @param etatMarchesPasses
     *            the etatMarchesPasses to set
     */
    public void setEtatMarchesPasses(String etatMarchesPasses) {
        this.etatMarchesPasses = etatMarchesPasses;
    }

    /**
     * Gets the etat realisation travaux.
     *
     * @return the etatRealisationTravaux
     */
    public String getEtatRealisationTravaux() {
        return this.etatRealisationTravaux;
    }

    /**
     * Sets the etat realisation travaux.
     *
     * @param etatRealisationTravaux
     *            the etatRealisationTravaux to set
     */
    public void setEtatRealisationTravaux(String etatRealisationTravaux) {
        this.etatRealisationTravaux = etatRealisationTravaux;
    }

    /**
     * Gets the etat achevement financier.
     *
     * @return the etatAchevementFinancier
     */
    public String getEtatAchevementFinancier() {
        return this.etatAchevementFinancier;
    }

    /**
     * Sets the etat achevement financier.
     *
     * @param etatAchevementFinancier
     *            the etatAchevementFinancier to set
     */
    public void setEtatAchevementFinancier(String etatAchevementFinancier) {
        this.etatAchevementFinancier = etatAchevementFinancier;
    }

    /**
     * Gets the etat achevenement tech.
     *
     * @return the etatAchevenementTech
     */
    public String getEtatAchevenementTech() {
        return this.etatAchevenementTech;
    }

    /**
     * Sets the etat achevenement tech.
     *
     * @param etatAchevenementTech
     *            the etatAchevenementTech to set
     */
    public void setEtatAchevenementTech(String etatAchevenementTech) {
        this.etatAchevenementTech = etatAchevenementTech;
    }

    /**
     * Gets the etat instruction.
     *
     * @return the etatInstruction
     */
    public String getEtatInstruction() {
        return this.etatInstruction;
    }

    /**
     * Sets the etat instruction.
     *
     * @param etatInstruction
     *            the etatInstruction to set
     */
    public void setEtatInstruction(String etatInstruction) {
        this.etatInstruction = etatInstruction;
    }

    /**
     * Gets the motif refus.
     *
     * @return the motifRefus
     */
    public String getMotifRefus() {
        return this.motifRefus;
    }

    /**
     * Sets the motif refus.
     *
     * @param motifRefus
     *            the motifRefus to set
     */
    public void setMotifRefus(String motifRefus) {
        this.motifRefus = motifRefus;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the documents.
     *
     * @return the documents
     */
    public List<DocumentDto> getDocuments() {
        return this.documents;
    }

    /**
     * Sets the documents.
     *
     * @param documents
     *            the documents to set
     */
    public void setDocuments(List<DocumentDto> documents) {
        this.documents = documents;
    }

    /**
     * Gets the code etat demande.
     *
     * @return the codeEtatDemande
     */
    public String getCodeEtatDemande() {
        return this.codeEtatDemande;
    }

    /**
     * Sets the code etat demande.
     *
     * @param codeEtatDemande
     *            the codeEtatDemande to set
     */
    public void setCodeEtatDemande(String codeEtatDemande) {
        this.codeEtatDemande = codeEtatDemande;
    }

    /**
     * Gets the libelle etat demande.
     *
     * @return the libelleEtatDemande
     */
    public String getLibelleEtatDemande() {
        return this.libelleEtatDemande;
    }

    /**
     * Sets the libelle etat demande.
     *
     * @param libelleEtatDemande
     *            the libelleEtatDemande to set
     */
    public void setLibelleEtatDemande(String libelleEtatDemande) {
        this.libelleEtatDemande = libelleEtatDemande;
    }

    /**
     * Gets the liste fichiers envoyer.
     *
     * @return the listeFichiersEnvoyer
     */
    public List<FichierTransfert> getListeFichiersEnvoyer() {
        return this.listeFichiersEnvoyer;
    }

    /**
     * Sets the liste fichiers envoyer.
     *
     * @param listeFichiersEnvoyer
     *            the listeFichiersEnvoyer to set
     */
    public void setListeFichiersEnvoyer(List<FichierTransfert> listeFichiersEnvoyer) {
        this.listeFichiersEnvoyer = listeFichiersEnvoyer;
    }

    /**
     * Gets the anomalies.
     *
     * @return the anomalies
     */
    public Set<AnomalieDto> getAnomalies() {
        return this.anomalies;
    }

    /**
     * Sets the anomalies.
     *
     * @param anomalies
     *            the anomalies to set
     */
    public void setAnomalies(Set<AnomalieDto> anomalies) {
        this.anomalies = anomalies;
    }

    /**
     * Gets the checks if is premiere demande.
     *
     * @return the isPremiereDemande
     */
    public Boolean getIsPremiereDemande() {
        return this.isPremiereDemande;
    }

    /**
     * Sets the checks if is premiere demande.
     *
     * @param isPremiereDemande
     *            the isPremiereDemande to set
     */
    public void setIsPremiereDemande(Boolean isPremiereDemande) {
        this.isPremiereDemande = isPremiereDemande;
    }

    /**
     * Gets the montant restant avance.
     *
     * @return the montantRestantAvance
     */
    public BigDecimal getMontantRestantAvance() {
        return this.montantRestantAvance;
    }

    /**
     * Sets the montant restant avance.
     *
     * @param montantRestantAvance
     *            the montantRestantAvance to set
     */
    public void setMontantRestantAvance(BigDecimal montantRestantAvance) {
        this.montantRestantAvance = montantRestantAvance;
    }

    /**
     * Gets the montant restant acompte.
     *
     * @return the montantRestantAcompte
     */
    public BigDecimal getMontantRestantAcompte() {
        return this.montantRestantAcompte;
    }

    /**
     * Sets the montant restant acompte.
     *
     * @param montantRestantAcompte
     *            the montantRestantAcompte to set
     */
    public void setMontantRestantAcompte(BigDecimal montantRestantAcompte) {
        this.montantRestantAcompte = montantRestantAcompte;
    }

    /**
     * Gets the ids docs compl suppr.
     *
     * @return the idsDocsComplSuppr
     */
    public List<Long> getIdsDocsComplSuppr() {
        return this.idsDocsComplSuppr;
    }

    /**
     * Sets the ids docs compl suppr.
     *
     * @param idsDocsComplSuppr
     *            the idsDocsComplSuppr to set
     */
    public void setIdsDocsComplSuppr(List<Long> idsDocsComplSuppr) {
        this.idsDocsComplSuppr = idsDocsComplSuppr;
    }

    /**
     * Gets the nom long collectivite.
     *
     * @return the nomLongCollectivite
     */
    public String getNomLongCollectivite() {
        return this.nomLongCollectivite;
    }

    /**
     * Sets the nom long collectivite.
     *
     * @param nomLongCollectivite
     *            the nomLongCollectivite to set
     */
    public void setNomLongCollectivite(String nomLongCollectivite) {
        this.nomLongCollectivite = nomLongCollectivite;
    }

    /**
     * Gets the code departement.
     *
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Gets the montant subvention.
     *
     * @return the montantSubvention
     */
    public BigDecimal getMontantSubvention() {
        return this.montantSubvention;
    }

    /**
     * Sets the montant subvention.
     *
     * @param montantSubvention
     *            the montantSubvention to set
     */
    public void setMontantSubvention(BigDecimal montantSubvention) {
        this.montantSubvention = montantSubvention;
    }

    /**
     * Gets the montant total ht travaux.
     *
     * @return the montantTotalHtTravaux
     */
    public BigDecimal getMontantTotalHtTravaux() {
        return this.montantTotalHtTravaux;
    }

    /**
     * Sets the montant total ht travaux.
     *
     * @param montantTotalHtTravaux
     *            the montantTotalHtTravaux to set
     */
    public void setMontantTotalHtTravaux(BigDecimal montantTotalHtTravaux) {
        this.montantTotalHtTravaux = montantTotalHtTravaux;
    }

    /**
     * @return the setHasUnAcompte
     */
    public Boolean getHasUnAcompte() {
        return this.hasUnAcompte;
    }

    /**
     * @param setHasUnAcompte
     *            the setHasUnAcompte to set
     */
    public void setHasUnAcompte(Boolean hasUnAcompte) {
        this.hasUnAcompte = hasUnAcompte;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

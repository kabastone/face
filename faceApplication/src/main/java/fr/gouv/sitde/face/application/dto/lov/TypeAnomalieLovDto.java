/**
 *
 */
package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class TypeAnomalieLovDto.
 *
 * @author Atos
 */
public class TypeAnomalieLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1570727591720664383L;

    /** The code. */
    private Long id;

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

}

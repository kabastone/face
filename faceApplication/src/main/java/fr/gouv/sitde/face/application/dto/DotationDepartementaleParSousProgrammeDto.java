package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DotationDepartementaleParSousProgrammeDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1867294428474102722L;

    /** The abreviation. */
    private String sousProgrammeAbreviation;

    /** The description. */
    private String sousProgrammeDescription;

    /** The */
    private String sousProgrammeNumero;

    /** The */
    private BigDecimal dspMontant;

    public DotationDepartementaleParSousProgrammeDto() {
    }

    public DotationDepartementaleParSousProgrammeDto(String sousProgrammeAbreviation, String sousProgrammeDescription, String sousProgrammeNumero,
            BigDecimal dspMontant) {
        this.sousProgrammeAbreviation = sousProgrammeAbreviation;
        this.sousProgrammeDescription = sousProgrammeDescription;
        this.sousProgrammeNumero = sousProgrammeNumero;
        this.dspMontant = dspMontant;
    }

    public String getSousProgrammeAbreviation() {
        return this.sousProgrammeAbreviation;
    }

    public void setSousProgrammeAbreviation(String sousProgrammeAbreviation) {
        this.sousProgrammeAbreviation = sousProgrammeAbreviation;
    }

    public String getSousProgrammeDescription() {
        return this.sousProgrammeDescription;
    }

    public void setSousProgrammeDescription(String sousProgrammeDescription) {
        this.sousProgrammeDescription = sousProgrammeDescription;
    }

    public String getSousProgrammeNumero() {
        return this.sousProgrammeNumero;
    }

    public void setSousProgrammeNumero(String sousProgrammeNumero) {
        this.sousProgrammeNumero = sousProgrammeNumero;
    }

    public BigDecimal getDspMontant() {
        return this.dspMontant;
    }

    public void setDspMontant(BigDecimal dspMontant) {
        this.dspMontant = dspMontant;
    }

    @Override
    public String toString() {
        return "DotationDepartementaleParSousProgrammeDto{" + "sousProgrammeAbreviation='" + this.sousProgrammeAbreviation + '\''
                + ", sousProgrammeDescription='" + this.sousProgrammeDescription + '\'' + ", sousProgrammeNumero='" + this.sousProgrammeNumero + '\''
                + ", dspMontant=" + this.dspMontant + '}';
    }
}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationAnnuelleDto.
 */
public class DotationCollectiviteRepartitionDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5713260396099914860L;

    /** The code departement. */
    private String codeDepartement;

    /** The dotations departement. */
    private final List<DotationDepartementDto> dotationsDepartement = new ArrayList<>();

    /** The documents. */
    private final List<DocumentDto> documents = new ArrayList<>();

    /** The collectivites repartition. */
    private final List<CollectiviteRepartitionDto> collectivitesRepartition = new ArrayList<>();

    /**
     * Gets the code departement.
     *
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Gets the dotations departement.
     *
     * @return the dotationsDepartement
     */
    public List<DotationDepartementDto> getDotationsDepartement() {
        return this.dotationsDepartement;
    }

    /**
     * Gets the documents.
     *
     * @return the documents
     */
    public List<DocumentDto> getDocuments() {
        return this.documents;
    }

    /**
     * @return the collectivitesRepartition
     */
    public List<CollectiviteRepartitionDto> getCollectivitesRepartition() {
        return this.collectivitesRepartition;
    }

}

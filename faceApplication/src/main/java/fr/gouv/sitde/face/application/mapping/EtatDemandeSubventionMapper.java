package fr.gouv.sitde.face.application.mapping;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.EtatDemandeSubventionDto;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

@Mapper(componentModel = "spring")
public interface EtatDemandeSubventionMapper {

    EtatDemandeSubventionDto etatDemandeSubventionToEtatDemandeSubventionDto(EtatDemandeSubvention etatDemandeSubvention);
}

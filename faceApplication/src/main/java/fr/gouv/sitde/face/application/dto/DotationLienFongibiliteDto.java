/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * @author a768251
 *
 */
public class DotationLienFongibiliteDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 897272967699036411L;

    /** The id sous programme donneur. */
    private Integer idDonneur;

    /** The id sous programme receveur. */
    private Integer idReceveur;

    /**
     * @return the idDonneur
     */
    public Integer getIdDonneur() {
        return this.idDonneur;
    }

    /**
     * @param idDonneur
     *            the idDonneur to set
     */
    public void setIdDonneur(Integer idDonneur) {
        this.idDonneur = idDonneur;
    }

    /**
     * @return the idReceveur
     */
    public Integer getIdReceveur() {
        return this.idReceveur;
    }

    /**
     * @param idReceveur
     *            the idReceveur to set
     */
    public void setIdReceveur(Integer idReceveur) {
        this.idReceveur = idReceveur;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.idReceveur == null) ? 0 : this.idReceveur.hashCode());
        result = (prime * result) + ((this.idDonneur == null) ? 0 : this.idDonneur.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (this.getClass() != obj.getClass())) {
            return false;
        }
        DotationLienFongibiliteDto other = (DotationLienFongibiliteDto) obj;

        return (this.aucunNull(other) && this.idEgaux(other));
    }

    /**
     * @param other
     * @return
     */
    private boolean idEgaux(DotationLienFongibiliteDto other) {
        return this.idReceveur.equals(other.idReceveur) && this.idDonneur.equals(other.idDonneur);
    }

    /**
     * @param other
     * @return
     */
    private boolean aucunNull(DotationLienFongibiliteDto other) {
        return ((this.idReceveur != null) && (other.idReceveur != null)) && ((this.idDonneur != null) && (other.idDonneur != null));
    }

}

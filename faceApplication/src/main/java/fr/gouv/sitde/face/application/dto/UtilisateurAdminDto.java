package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Utilisateur UtilisateurAdminDto.
 */
public class UtilisateurAdminDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5629206738462240386L;

    /** The id. */
    private Long id;

    /** The nom. */
    private String nom;

    /** The prenom. */
    private String prenom;

    /** The telephone. */
    private String telephone;

    /** The email. */
    private String email;

    /** The cerbere id. */
    private String cerbereId;

    /** The version. */
    private int version;

    /** The droits agent collectivites. */
    private List<DroitAgentCollectiviteDto> droitsAgentCollectivites = new ArrayList<>(0);

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets the prenom.
     *
     * @param prenom
     *            the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets the telephone.
     *
     * @return the telephone
     */
    public String getTelephone() {
        return this.telephone;
    }

    /**
     * Sets the telephone.
     *
     * @param telephone
     *            the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the cerbere id.
     *
     * @return the cerbereId
     */
    public String getCerbereId() {
        return this.cerbereId;
    }

    /**
     * Sets the cerbere id.
     *
     * @param cerbereId
     *            the cerbereId to set
     */
    public void setCerbereId(String cerbereId) {
        this.cerbereId = cerbereId;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the droits agent collectivites.
     *
     * @return the droitsAgentCollectivites
     */
    public List<DroitAgentCollectiviteDto> getDroitsAgentCollectivites() {
        return this.droitsAgentCollectivites;
    }

    /**
     * Sets the droits agent collectivites.
     *
     * @param droitsAgentCollectivites
     *            the droitsAgentCollectivites to set
     */
    public void setDroitsAgentCollectivites(List<DroitAgentCollectiviteDto> droitsAgentCollectivites) {
        this.droitsAgentCollectivites = droitsAgentCollectivites;
    }
}

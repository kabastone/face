/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationListeLiensFongibiliteDto.
 *
 * @author a768251
 */
public class DotationListeLiensFongibiliteDto {

    /** The lien fongibilite. */
    private final List<DotationLienFongibiliteDto> lienFongibilite = new ArrayList<>();

    /** The dotation collectivite fongibilite dto. */
    private final List<DotationCollectiviteFongibiliteDto> dotationCollectiviteFongibiliteDto = new ArrayList<>();

    /**
     * Gets the lien fongibilite.
     *
     * @return the lienFongibilite
     */
    public List<DotationLienFongibiliteDto> getLienFongibilite() {
        return this.lienFongibilite;
    }

    /**
     * Gets the dotation collectivite fongibilite dto.
     *
     * @return the dotationCollectiviteFongibiliteDto
     */
    public List<DotationCollectiviteFongibiliteDto> getDotationCollectiviteFongibiliteDto() {
        return this.dotationCollectiviteFongibiliteDto;
    }
}

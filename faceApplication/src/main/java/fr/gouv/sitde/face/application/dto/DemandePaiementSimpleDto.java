package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DemandePaiementSimpleDto.
 */
public class DemandePaiementSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3316932124669534805L;

    /** The id. */
    private Long id;

    /** The code etat demande. */
    private String codeEtatDemande;

    /** The libelle etat demande. */
    private String libelleEtatDemande;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateDemande;

    /** The aide demandee. */
    private BigDecimal aideDemandee = BigDecimal.ZERO;

    /** The type demande paiement. */
    private String typeDemandePaiement;

    /** The montant total ht travaux. */
    private BigDecimal montantTotalHtTravaux;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code etat demande.
     *
     * @return the codeEtatDemande
     */
    public String getCodeEtatDemande() {
        return this.codeEtatDemande;
    }

    /**
     * Sets the code etat demande.
     *
     * @param codeEtatDemande
     *            the codeEtatDemande to set
     */
    public void setCodeEtatDemande(String codeEtatDemande) {
        this.codeEtatDemande = codeEtatDemande;
    }

    /**
     * Gets the libelle etat demande.
     *
     * @return the libelleEtatDemande
     */
    public String getLibelleEtatDemande() {
        return this.libelleEtatDemande;
    }

    /**
     * Sets the libelle etat demande.
     *
     * @param libelleEtatDemande
     *            the libelleEtatDemande to set
     */
    public void setLibelleEtatDemande(String libelleEtatDemande) {
        this.libelleEtatDemande = libelleEtatDemande;
    }

    /**
     * Gets the LocalDateTime demande.
     *
     * @return the dateDemande
     */
    public LocalDateTime getDateDemande() {
        return this.dateDemande;
    }

    /**
     * Sets the LocalDateTime demande.
     *
     * @param dateDemande
     *            the dateDemande to set
     */
    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    /**
     * Gets the aide demandee.
     *
     * @return the aideDemandee
     */
    public BigDecimal getAideDemandee() {
        return this.aideDemandee;
    }

    /**
     * Sets the aide demandee.
     *
     * @param aideDemandee
     *            the aideDemandee to set
     */
    public void setAideDemandee(BigDecimal aideDemandee) {
        this.aideDemandee = aideDemandee;
    }

    /**
     * Gets the type demande paiement.
     *
     * @return the typeDemandePaiement
     */
    public String getTypeDemandePaiement() {
        return this.typeDemandePaiement;
    }

    /**
     * Sets the type demande paiement.
     *
     * @param typeDemandePaiement
     *            the typeDemandePaiement to set
     */
    public void setTypeDemandePaiement(String typeDemandePaiement) {
        this.typeDemandePaiement = typeDemandePaiement;
    }

    /**
     * @return the montantTotalHtTravaux
     */
    public BigDecimal getMontantTotalHtTravaux() {
        return this.montantTotalHtTravaux;
    }

    /**
     * @param montantTotalHtTravaux
     *            the montantTotalHtTravaux to set
     */
    public void setMontantTotalHtTravaux(BigDecimal montantTotalHtTravaux) {
        this.montantTotalHtTravaux = montantTotalHtTravaux;
    }
}

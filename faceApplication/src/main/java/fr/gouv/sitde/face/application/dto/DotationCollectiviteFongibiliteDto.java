/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author a768251
 *
 */
public class DotationCollectiviteFongibiliteDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 38946331704256162L;

    /** The sousProgramme description. */
    private String description;

    /** The id sous programme. */
    private Integer idSousProgramme;

    /** The dotation disponible. */
    private BigDecimal dotationDisponible;

    /** The id collectivite. */
    private Long idCollectivite;

    /**
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the idSousProgramme
     */
    public Integer getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * @param idSousProgramme
     *            the idSousProgramme to set
     */
    public void setIdSousProgramme(Integer idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

    /**
     * @return the dotationDisponible
     */
    public BigDecimal getDotationDisponible() {
        return this.dotationDisponible;
    }

    /**
     * @param dotationDisponible
     *            the dotationDisponible to set
     */
    public void setDotationDisponible(BigDecimal dotationDisponible) {
        this.dotationDisponible = dotationDisponible;
    }

    /**
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

}

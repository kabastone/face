/**
 *
 */
package fr.gouv.sitde.face.application.commun.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.commun.GestionEmailContactApplication;
import fr.gouv.sitde.face.domaine.service.email.EmailContactService;
import fr.gouv.sitde.face.transverse.email.EmailContactDto;

/**
 * The Class GestionEmailContactApplicationImpl.
 */
@Service
public class GestionEmailContactApplicationImpl implements GestionEmailContactApplication {

    /** The email service. */
    @Inject
    private EmailContactService emailContactService;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.commun.GestionEmailContactApplication#envoyerEmailContact(fr.gouv.sitde.face.application.dto.EmailContactDto)
     */
    @Override
    public Boolean envoyerEmailContact(EmailContactDto emailContactDto) {
        return this.emailContactService.envoyerEmailContact(emailContactDto);
    }

}

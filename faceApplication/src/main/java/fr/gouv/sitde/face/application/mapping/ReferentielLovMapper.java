package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.lov.CollectiviteLovDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandePaiementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandeSubventionLovDto;
import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeAnomalieLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * The Interface ReferentielLovMapper.
 */
@Mapper(componentModel = "spring")
public interface ReferentielLovMapper {

    /**
     * Departement to departement lov dto.
     *
     * @param departement
     *            the departement
     * @return the departement lov dto
     */
    default DepartementLovDto departementToDepartementLovDto(Departement departement) {

        DepartementLovDto departementLovDto = new DepartementLovDto();

        departementLovDto.setId(departement.getId());
        departementLovDto.setCode(departement.getCode());
        departementLovDto.setNom(departement.getNom());
        departementLovDto.setCodeEtNom(departement.getCode() + " - " + departement.getNom());

        return departementLovDto;

    }

    /**
     * Departements to departements lov dto.
     *
     * @param departements
     *            the departements
     * @return the list
     */
    List<DepartementLovDto> departementsToDepartementsLovDtos(List<Departement> departements);

    /**
     * Collectivite to collectivite lov dto.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite lov dto
     */
    @Mapping(target = "etatCollectivite", source = "etatCollectivite")
    CollectiviteLovDto collectiviteToCollectiviteLovDto(Collectivite collectivite);

    /**
     * Collectivites to collectivite lov dtos.
     *
     * @param collectivites
     *            the collectivites
     * @return the list
     */
    List<CollectiviteLovDto> collectivitesToCollectiviteLovDtos(List<Collectivite> collectivites);

    /**
     * Sous programme to sous programme lov dto.
     *
     * @param sousProgramme
     *            the sous programme
     * @return the sous programme lov dto
     */
    SousProgrammeLovDto sousProgrammeToSousProgrammeLovDto(SousProgramme sousProgramme);

    /**
     * Sous programmes to sous programme lov dtos.
     *
     * @param sousProgrammes
     *            the sous programmes
     * @return the list
     */
    List<SousProgrammeLovDto> sousProgrammesToSousProgrammeLovDtos(List<SousProgramme> sousProgrammes);

    /**
     * Etat dossier enum to etat dossier lov dto.
     *
     * @param etatDemandeEnum
     *            the etat dossier enum
     * @return the etat dossier lov dto
     */
    default EtatDemandeSubventionLovDto etatDemandeSubventionEnumToEtatDemandeSubventionLovDto(EtatDemandeSubventionEnum etatDemandeEnum) {
        EtatDemandeSubventionLovDto etatDemandeSubventionLovDto = new EtatDemandeSubventionLovDto();
        etatDemandeSubventionLovDto.setCode(etatDemandeEnum.name());
        return etatDemandeSubventionLovDto;
    }

    /**
     * Etat dossier enum to etat dossier lov dto.
     *
     * @param etatDossierEnum
     *            the etat dossier enum
     * @return the etat dossier lov dto
     */
    default TypeDemandePaiementLovDto typeDemandePaiementEnumToTypeDemandePaiementLovDto(TypeDemandePaiementEnum typeDemandePaiementEnum) {
        TypeDemandePaiementLovDto typeDemandePaiementLovDto = new TypeDemandePaiementLovDto();
        typeDemandePaiementLovDto.setCode(typeDemandePaiementEnum.getCode());
        typeDemandePaiementLovDto.setLibelle(typeDemandePaiementEnum.getLibelle());
        return typeDemandePaiementLovDto;
    }

    /**
     * Type anomalie lov dto to type anomalie lov dtos.
     *
     * @param typeAnomalie
     *            the type anomalie
     * @return the list
     */
    List<TypeAnomalieLovDto> typeAnomalieToTypeAnomalieLovDtos(List<TypeAnomalie> listeTypesAnomalies);

    /**
     * @param typeDemande
     * @return
     */
    default EtatDemandePaiementLovDto etatDemandePaiementEnumToEtatDemandePaiementLovDto(EtatDemandePaiementEnum etatDemandePaiementEnum) {
        EtatDemandePaiementLovDto etatDemandePaiementLovDto = new EtatDemandePaiementLovDto();
        etatDemandePaiementLovDto.setCode(etatDemandePaiementEnum.name());
        return etatDemandePaiementLovDto;
    }
}

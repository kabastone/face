/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeProlongationTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DossierSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DotationCollectiviteTdbDto;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Interface TableauDeBordMapper.
 *
 * @author a453029
 */
@Mapper(componentModel = "spring")
public interface TableauDeBordMapper {

    /**
     * Dotation collectivite to dotation collectivite tdb dto.
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @return the dotation collectivite tdb dto
     */
    @Mapping(target = "descriptionSousProgramme", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "idSousProgramme", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id")
    DotationCollectiviteTdbDto dotationCollectiviteToDotationCollectiviteTdbDto(DotationCollectivite dotationCollectivite);

    /**
     * Demande subvention to demande suvention tdb dto.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention tdb dto
     */
    @Mapping(target = "dateCreationDossier", source = "demandeSubvention.dossierSubvention.dateCreationDossier")
    @Mapping(target = "nomCourtCollectivite", source = "demandeSubvention.dossierSubvention.dotationCollectivite.collectivite.nomCourt")
    @Mapping(target = "numDossier", source = "demandeSubvention.dossierSubvention.numDossier")
    @Mapping(target = "plafondAideDemande", source = "demandeSubvention.plafondAide")
    DemandeSubventionTdbDto demandeSubventionToDemandeSubventionTdbDto(DemandeSubvention demandeSubvention);

    /**
     * Demande paiement to demande paiement tdb dto.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement tdb dto
     */
    @Mapping(target = "dateCreationDossier", source = "demandePaiement.dossierSubvention.dateCreationDossier")
    @Mapping(target = "nomCourtCollectivite", source = "demandePaiement.dossierSubvention.dotationCollectivite.collectivite.nomCourt")
    @Mapping(target = "numDossier", source = "demandePaiement.dossierSubvention.numDossier")
    @Mapping(target = "aideDemandeeDemande", source = "demandePaiement.aideDemandee")
    DemandePaiementTdbDto demandeSubventionToDemandeSubventionTdbDto(DemandePaiement demandePaiement);

    /**
     * Demande prolongation to demande prolongation tdb dto.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation tdb dto
     */
    @Mapping(target = "dateCreationDossier", source = "demandeProlongation.dossierSubvention.dateCreationDossier")
    @Mapping(target = "nomCourtCollectivite", source = "demandeProlongation.dossierSubvention.dotationCollectivite.collectivite.nomCourt")
    @Mapping(target = "numDossier", source = "demandeProlongation.dossierSubvention.numDossier")
    @Mapping(target = "dateAchevementDemandee", source = "demandeProlongation.dateAchevementDemandee")
    DemandeProlongationTdbDto demandeProlongationToDemandeProlongationTdbDto(DemandeProlongation demandeProlongation);

    /**
     * Dossier subvention to dossier suvention tdb dto.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention tdb dto
     */
    @Mapping(target = "dateCreationDossier", source = "dossierSubvention.dateCreationDossier")
    @Mapping(target = "nomCourtCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.nomCourt")
    @Mapping(target = "numDossier", source = "dossierSubvention.numDossier")
    @Mapping(target = "plafondAideDossier", source = "dossierSubvention.plafondAide")
    @Mapping(target = "plafondAideSansRefus", source = "dossierSubvention.plafondAideSansRefus")
    DossierSubventionTdbDto dossierSubventionToDossierSubventionTdbDto(DossierSubvention dossierSubvention);

    /**
     * Dotations collectivite to dotations collectivite tdb dto.
     *
     * @param dotationsCollectivite
     *            the dotations collectivite
     * @return the list
     */
    List<DotationCollectiviteTdbDto> dotationsCollectiviteToDotationsCollectiviteTdbDto(List<DotationCollectivite> dotationsCollectivite);

    /**
     * Demandes subvention to demandes subvention tdb dto.
     *
     * @param demandesSubvention
     *            the demandes subvention
     * @return the list
     */
    List<DemandeSubventionTdbDto> demandesSubventionToDemandesSubventionTdbDto(List<DemandeSubvention> demandesSubvention);

    /**
     * Demandes paiement to demandes paiement tdb dto.
     *
     * @param demandesPaiement
     *            the demandes paiement
     * @return the list
     */
    List<DemandePaiementTdbDto> demandesPaiementToDemandesPaiementTdbDto(List<DemandePaiement> demandesPaiement);

    /**
     * Demandes prolongation to demandes prolongation tdb dto.
     *
     * @param demandesProlongation
     *            the demandes prolongation
     * @return the list
     */
    List<DemandeProlongationTdbDto> demandesProlongationToDemandesProlongationTdbDto(List<DemandeProlongation> demandesProlongation);

    /**
     * Dossiers subvention to dossiers suvention tdb dto.
     *
     * @param dossiersSubvention
     *            the dossiers subvention
     * @return the list
     */
    List<DossierSubventionTdbDto> dossiersSubventionToDossiersSubventionTdbDto(List<DossierSubvention> dossiersSubvention);

}

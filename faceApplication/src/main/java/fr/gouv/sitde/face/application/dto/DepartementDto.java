package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

public class DepartementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7744395061062866642L;

    /** The id. */
    private Integer id;

    /** The code. */
    private String code;

    /** The nom. */
    private String nom;

    private String nomCodique;

    /** The codique. */
    private String codique;

    /** The version. */
    private Integer version;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the version
     */
    public Integer getVersion() {
        return this.version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return the nomCodique
     */
    public String getNomCodique() {
        return this.nomCodique;
    }

    /**
     * @param nomCodique
     *            the nomCodique to set
     */
    public void setNomCodique(String nomCodique) {
        this.nomCodique = nomCodique;
    }

    /**
     * @return the codique
     */
    public String getCodique() {
        return this.codique;
    }

    /**
     * @param codique
     *            the codique to set
     */
    public void setCodique(String codique) {
        this.codique = codique;
    }
}

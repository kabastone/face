package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;

/**
 * The Class CollectiviteDetailDto.
 */
public class CollectiviteDetailDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7744395061062866642L;

    /** The id. */
    private Long id;

    /** The siret. */
    private String siret;

    /** The nom court. */
    private String nomCourt;

    /** The nom long. */
    private String nomLong;

    /** The adresse moe. */
    private AdresseDto adresseMoe;

    /** The adresse moa. */
    private AdresseDto adresseMoa;

    /** The departement. */
    private DepartementLovDto departement;

    /** The id chorus. */
    private String idChorus;

    /** The num collectivite. */
    private String numCollectivite;

    /** The receveur num codique. */
    private String receveurNumCodique;

    /** The receveur nom. */
    private String receveurNom;

    /** The avec moa moe identiques. */
    private boolean avecMoaMoeIdentiques;

    /** The avec plusieurs moe. */
    private boolean avecPlusieursMoe;

    /** The etat collectivite. */
    private EtatCollectiviteEnum etatCollectivite;

    /** The version. */
    private int version;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the siret.
     *
     * @return the siret
     */
    public String getSiret() {
        return this.siret;
    }

    /**
     * Sets the siret.
     *
     * @param siret
     *            the new siret
     */
    public void setSiret(String siret) {
        this.siret = siret;
    }

    /**
     * Gets the nom court.
     *
     * @return the nom court
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * Sets the nom court.
     *
     * @param nomCourt
     *            the new nom court
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * Gets the nom long.
     *
     * @return the nom long
     */
    public String getNomLong() {
        return this.nomLong;
    }

    /**
     * Sets the nom long.
     *
     * @param nomLong
     *            the new nom long
     */
    public void setNomLong(String nomLong) {
        this.nomLong = nomLong;
    }

    /**
     * Gets the etat collectivite.
     *
     * @return the etat collectivite
     */
    public EtatCollectiviteEnum getEtatCollectivite() {
        return this.etatCollectivite;
    }

    /**
     * Sets the etat collectivite.
     *
     * @param etatCollectivite
     *            the new etat collectivite
     */
    public void setEtatCollectivite(EtatCollectiviteEnum etatCollectivite) {
        this.etatCollectivite = etatCollectivite;
    }

    /**
     * @return the adresseMoe
     */
    public AdresseDto getAdresseMoe() {
        return this.adresseMoe;
    }

    /**
     * @param adresseMoe
     *            the adresseMoe to set
     */
    public void setAdresseMoe(AdresseDto adresseMoe) {
        this.adresseMoe = adresseMoe;
    }

    /**
     * @return the adresseMoa
     */
    public AdresseDto getAdresseMoa() {
        return this.adresseMoa;
    }

    /**
     * @param adresseMoa
     *            the adresseMoa to set
     */
    public void setAdresseMoa(AdresseDto adresseMoa) {
        this.adresseMoa = adresseMoa;
    }

    /**
     * @return the departement
     */
    public DepartementLovDto getDepartement() {
        return this.departement;
    }

    /**
     * @param departement
     *            the departement to set
     */
    public void setDepartement(DepartementLovDto departement) {
        this.departement = departement;
    }

    /**
     * @return the idChorus
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * @param idChorus
     *            the idChorus to set
     */
    public void setIdChorus(String idChorus) {
        this.idChorus = idChorus;
    }

    /**
     * @return the numCollectivite
     */
    public String getNumCollectivite() {
        return this.numCollectivite;
    }

    /**
     * @param numCollectivite
     *            the numCollectivite to set
     */
    public void setNumCollectivite(String numCollectivite) {
        this.numCollectivite = numCollectivite;
    }

    /**
     * @return the receveurNumCodique
     */
    public String getReceveurNumCodique() {
        return this.receveurNumCodique;
    }

    /**
     * @param receveurNumCodique
     *            the receveurNumCodique to set
     */
    public void setReceveurNumCodique(String receveurNumCodique) {
        this.receveurNumCodique = receveurNumCodique;
    }

    /**
     * @return the receveurNom
     */
    public String getReceveurNom() {
        return this.receveurNom;
    }

    /**
     * @param receveurNom
     *            the receveurNom to set
     */
    public void setReceveurNom(String receveurNom) {
        this.receveurNom = receveurNom;
    }

    /**
     * @return the avecMoaMoeIdentiques
     */
    public boolean isAvecMoaMoeIdentiques() {
        return this.avecMoaMoeIdentiques;
    }

    /**
     * @param avecMoaMoeIdentiques
     *            the avecMoaMoeIdentiques to set
     */
    public void setAvecMoaMoeIdentiques(boolean avecMoaMoeIdentiques) {
        this.avecMoaMoeIdentiques = avecMoaMoeIdentiques;
    }

    /**
     * @return the avecPlusieursMoe
     */
    public boolean isAvecPlusieursMoe() {
        return this.avecPlusieursMoe;
    }

    /**
     * @param avecPlusieursMoe
     *            the avecPlusieursMoe to set
     */
    public void setAvecPlusieursMoe(boolean avecPlusieursMoe) {
        this.avecPlusieursMoe = avecPlusieursMoe;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

}

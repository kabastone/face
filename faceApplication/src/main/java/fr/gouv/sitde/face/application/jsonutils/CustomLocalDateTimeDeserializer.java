/**
 *
 */
package fr.gouv.sitde.face.application.jsonutils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class CustomLocalDateTimeDeserializer.
 *
 * @author A754839
 */
public class CustomLocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5354911420674622415L;

    /**
     * Instantiates a new custom offset date time deserializer.
     */
    public CustomLocalDateTimeDeserializer() {
        this(null);
    }

    /**
     * Instantiates a new custom offset date time serializer.
     *
     * @param t
     *            the LocalDateTime
     */
    protected CustomLocalDateTimeDeserializer(Class<LocalDateTime> t) {
        super(t);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml.jackson.core.JsonParser,
     * com.fasterxml.jackson.databind.DeserializationContext)
     */
    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) {
        try {
            return LocalDateTime.of(LocalDate.parse(p.getText(), DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)), LocalTime.MIDNIGHT);
        } catch (IOException e) {
            throw new TechniqueException("Problème lors de la deserialization de la LocalDateTime", e);
        }

    }

}

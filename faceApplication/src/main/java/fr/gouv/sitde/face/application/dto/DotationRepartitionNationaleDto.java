/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationRepartitionNationaleDto.
 *
 * @author a453029
 */
public class DotationRepartitionNationaleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2484199320417329938L;

    /** The dotation programme. */
    private DotationProgrammeDto dotationProgramme;

    /** The liste dotations sous programmes. */
    private List<LigneDotationsSousProgrammeDto> listeLignesDotationsSousProgrammes = new ArrayList<>();

    /** The somme dotations sp annee moins 3. */
    private BigDecimal sommeDotationsSpAnneeMoins3 = BigDecimal.ZERO;

    /** The somme dotations sp annee moins 2. */
    private BigDecimal sommeDotationsSpAnneeMoins2 = BigDecimal.ZERO;

    /** The somme dotations sp annee moins 1. */
    private BigDecimal sommeDotationsSpAnneeMoins1 = BigDecimal.ZERO;

    /** The somme dotations sp annee N. */
    private BigDecimal sommeDotationsSpAnneeN = BigDecimal.ZERO;

    /** The somme dotations sp initial annee N. */
    private BigDecimal sommeDotationsSpInitialAnneeN = BigDecimal.ZERO;

    /**
     * Gets the dotation programme.
     *
     * @return the dotationProgramme
     */
    public DotationProgrammeDto getDotationProgramme() {
        return this.dotationProgramme;
    }

    /**
     * Sets the dotation programme.
     *
     * @param dotationProgramme
     *            the dotationProgramme to set
     */
    public void setDotationProgramme(DotationProgrammeDto dotationProgramme) {
        this.dotationProgramme = dotationProgramme;
    }

    /**
     * Gets the somme dotations sp annee moins 3.
     *
     * @return the sommeDotationsSpAnneeMoins3
     */
    public BigDecimal getSommeDotationsSpAnneeMoins3() {
        return this.sommeDotationsSpAnneeMoins3;
    }

    /**
     * Sets the somme dotations sp annee moins 3.
     *
     * @param sommeDotationsSpAnneeMoins3
     *            the sommeDotationsSpAnneeMoins3 to set
     */
    public void setSommeDotationsSpAnneeMoins3(BigDecimal sommeDotationsSpAnneeMoins3) {
        this.sommeDotationsSpAnneeMoins3 = sommeDotationsSpAnneeMoins3;
    }

    /**
     * Gets the somme dotations sp annee moins 2.
     *
     * @return the sommeDotationsSpAnneeMoins2
     */
    public BigDecimal getSommeDotationsSpAnneeMoins2() {
        return this.sommeDotationsSpAnneeMoins2;
    }

    /**
     * Sets the somme dotations sp annee moins 2.
     *
     * @param sommeDotationsSpAnneeMoins2
     *            the sommeDotationsSpAnneeMoins2 to set
     */
    public void setSommeDotationsSpAnneeMoins2(BigDecimal sommeDotationsSpAnneeMoins2) {
        this.sommeDotationsSpAnneeMoins2 = sommeDotationsSpAnneeMoins2;
    }

    /**
     * Gets the somme dotations sp annee moins 1.
     *
     * @return the sommeDotationsSpAnneeMoins1
     */
    public BigDecimal getSommeDotationsSpAnneeMoins1() {
        return this.sommeDotationsSpAnneeMoins1;
    }

    /**
     * Sets the somme dotations sp annee moins 1.
     *
     * @param sommeDotationsSpAnneeMoins1
     *            the sommeDotationsSpAnneeMoins1 to set
     */
    public void setSommeDotationsSpAnneeMoins1(BigDecimal sommeDotationsSpAnneeMoins1) {
        this.sommeDotationsSpAnneeMoins1 = sommeDotationsSpAnneeMoins1;
    }

    /**
     * Gets the somme dotations sp annee N.
     *
     * @return the somme dotations sp annee N
     */
    public BigDecimal getSommeDotationsSpAnneeN() {
        return this.sommeDotationsSpAnneeN;
    }

    /**
     * Sets the somme dotations sp annee N.
     *
     * @param sommeDotationsSpAnneeN
     *            the new somme dotations sp annee N
     */
    public void setSommeDotationsSpAnneeN(BigDecimal sommeDotationsSpAnneeN) {
        this.sommeDotationsSpAnneeN = sommeDotationsSpAnneeN;
    }

    /**
     * Gets the liste lignes dotations sous programmes.
     *
     * @return the liste lignes dotations sous programmes
     */
    public List<LigneDotationsSousProgrammeDto> getListeLignesDotationsSousProgrammes() {
        return this.listeLignesDotationsSousProgrammes;
    }

    /**
     * Sets the liste lignes dotations sous programmes.
     *
     * @param listeLignesDotationsSousProgrammes
     *            the new liste lignes dotations sous programmes
     */
    public void setListeLignesDotationsSousProgrammes(List<LigneDotationsSousProgrammeDto> listeLignesDotationsSousProgrammes) {
        this.listeLignesDotationsSousProgrammes = listeLignesDotationsSousProgrammes;
    }

    /**
     * @return the sommeDotationsSpInitialAnneeN
     */
    public BigDecimal getSommeDotationsSpInitialAnneeN() {
        return this.sommeDotationsSpInitialAnneeN;
    }

    /**
     * @param sommeDotationsSpInitialAnneeN
     *            the sommeDotationsSpInitialAnneeN to set
     */
    public void setSommeDotationsSpInitialAnneeN(BigDecimal sommeDotationsSpInitialAnneeN) {
        this.sommeDotationsSpInitialAnneeN = sommeDotationsSpInitialAnneeN;
    }

}

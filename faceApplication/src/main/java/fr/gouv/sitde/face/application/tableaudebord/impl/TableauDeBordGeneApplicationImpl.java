/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DossierSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DotationCollectiviteTdbDto;
import fr.gouv.sitde.face.application.mapping.TableauDeBordMapper;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;

/**
 * The Class TableauDeBordGeneApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class TableauDeBordGeneApplicationImpl implements TableauDeBordGeneApplication {

    /** The tableau de bord gene service. */
    @Inject
    private TableauDeBordGeneService tableauDeBordGeneService;

    /** The tableau de bord mapper. */
    @Inject
    private TableauDeBordMapper tableauDeBordMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherTableauDeBordGenerique(java.lang.Long)
     */
    @Override
    public TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite) {
        return this.tableauDeBordGeneService.rechercherTableauDeBordGenerique(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationCollectiviteTdbDto> rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(Long idCollectivite,
            PageDemande pageDemande) {
        // Recherche
        PageReponse<DotationCollectivite> pageReponseDotations = this.tableauDeBordGeneService
                .rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(idCollectivite, pageDemande);

        // Renvoi page reponse DotationCollectiviteTdbDto
        List<DotationCollectiviteTdbDto> listeDotationsDto = this.tableauDeBordMapper
                .dotationsCollectiviteToDotationsCollectiviteTdbDto(pageReponseDotations.getListeResultats());
        return new PageReponse<>(listeDotationsDto, pageReponseDotations.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDossiersSubventionTdbGeneAdemanderPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAdemanderPaiement(Long idCollectivite, PageDemande pageDemande) {
        // Recherche
        PageReponse<DossierSubvention> pageReponseDossierSubvention = this.tableauDeBordGeneService
                .rechercherDossiersSubventionTdbGeneAdemanderPaiement(idCollectivite, pageDemande);

        // Renvoi page reponse DossierSubventionTdbDto
        List<DossierSubventionTdbDto> listeDossierSubventionDto = this.tableauDeBordMapper
                .dossiersSubventionToDossiersSubventionTdbDto(pageReponseDossierSubvention.getListeResultats());
        return new PageReponse<>(listeDossierSubventionDto, pageReponseDossierSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDossiersSubventionTdbGeneACompleterSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override

    public PageReponse<DotationCollectiviteTdbDto> rechercherDotationCollectiviteTdbGeneACompleterSubvention(Long idCollectivite,
            PageDemande pageDemande) {
        // Recherche

        PageReponse<DotationCollectivite> pageReponseDossierSubvention = this.tableauDeBordGeneService
                .rechercherDotationCollectiviteTdbGeneAcompleterSubvention(idCollectivite, pageDemande);

        // Renvoi page reponse DossierSubventionTdbDto
        List<DotationCollectiviteTdbDto> listeDossierSubventionDto = this.tableauDeBordMapper
                .dotationsCollectiviteToDotationsCollectiviteTdbDto(pageReponseDossierSubvention.getListeResultats());
        return new PageReponse<>(listeDossierSubventionDto, pageReponseDossierSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDemandesSubventionTdbGeneAcorrigerSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbGeneAcorrigerSubvention(Long idCollectivite, PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordGeneService
                .rechercherDemandesSubventionTdbGeneAcorrigerSubvention(idCollectivite, pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDemandesPaiementTdbGeneAcorrigerPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbGeneAcorrigerPaiement(Long idCollectivite, PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordGeneService
                .rechercherDemandesPaiementTdbGeneAcorrigerPaiement(idCollectivite, pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDossiersSubventionTdbGeneAcommencerPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAcommencerPaiement(Long idCollectivite, PageDemande pageDemande) {
        // Recherche
        PageReponse<DossierSubvention> pageReponseDossierSubvention = this.tableauDeBordGeneService
                .rechercherDossiersSubventionTdbGeneAcommencerPaiement(idCollectivite, pageDemande);

        // Renvoi page reponse DossierSubventionTdbDto
        List<DossierSubventionTdbDto> listeDossierSubventionDto = this.tableauDeBordMapper
                .dossiersSubventionToDossiersSubventionTdbDto(pageReponseDossierSubvention.getListeResultats());
        return new PageReponse<>(listeDossierSubventionDto, pageReponseDossierSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#rechercherDossiersSubventionTdbGeneAsolderPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAsolderPaiement(Long idCollectivite, PageDemande pageDemande) {
        // Recherche
        PageReponse<DossierSubvention> pageReponseDossierSubvention = this.tableauDeBordGeneService
                .rechercherDossiersSubventionTdbGeneAsolderPaiement(idCollectivite, pageDemande);

        // Renvoi page reponse DossierSubventionTdbDto
        List<DossierSubventionTdbDto> listeDossierSubventionDto = this.tableauDeBordMapper
                .dossiersSubventionToDossiersSubventionTdbDto(pageReponseDossierSubvention.getListeResultats());
        return new PageReponse<>(listeDossierSubventionDto, pageReponseDossierSubvention.getNbTotalResultats());
    }

}

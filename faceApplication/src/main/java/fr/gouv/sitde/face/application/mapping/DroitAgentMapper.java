/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.application.dto.DroitAgentUtilisateurDto;
import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * Mapper entité/DTO des droits agents.
 *
 * @author Atos
 */
@Mapper(componentModel = "spring")
public interface DroitAgentMapper {

    /**
     * Droit agent to droit agent utilisateur dto.
     *
     * @param droitAgent
     *            the droit agent
     * @return the droit agent utilisateur dto
     */
    @Mapping(target = "idUtilisateur", source = "droitAgent.utilisateur.id")
    @Mapping(target = "email", source = "droitAgent.utilisateur.email")
    @Mapping(target = "nom", source = "droitAgent.utilisateur.nom")
    @Mapping(target = "prenom", source = "droitAgent.utilisateur.prenom")
    @Mapping(target = "telephone", source = "droitAgent.utilisateur.telephone")
    @Mapping(target = "idCollectivite", source = "droitAgent.collectivite.id")
    DroitAgentUtilisateurDto droitAgentToDroitAgentUtilisateurDto(DroitAgent droitAgent);

    /**
     * Droits agents to droit utilisateur dtos.
     *
     * @param droitsAgents
     *            the droits agents
     * @return the list
     */
    List<DroitAgentUtilisateurDto> droitsAgentsToDroitUtilisateurDtos(List<DroitAgent> droitsAgents);

    /**
     * Droit agent to droit agent collectivite dto.
     *
     * @param droitAgent
     *            the droit agent
     * @return the droit agent collectivite dto
     */
    @Mapping(target = "idCollectivite", source = "droitAgent.collectivite.id")
    @Mapping(target = "siret", source = "droitAgent.collectivite.siret")
    @Mapping(target = "nomCourt", source = "droitAgent.collectivite.nomCourt")
    @Mapping(target = "nomLong", source = "droitAgent.collectivite.nomLong")
    @Mapping(target = "etatCollectivite", source = "droitAgent.collectivite.etatCollectivite")
    @Mapping(target = "idUtilisateur", source = "droitAgent.utilisateur.id")
    DroitAgentCollectiviteDto droitAgentToDroitAgentCollectiviteDto(DroitAgent droitAgent);

    /**
     * Droit agent collectivite dto to droit agent.
     *
     * @param droitAgentCollectiviteDto
     *            the droit agent collectivite dto
     * @return the droit agent
     */
    @Mapping(target = "collectivite.id", source = "droitAgentCollectiviteDto.idCollectivite")
    @Mapping(target = "collectivite.siret", source = "droitAgentCollectiviteDto.siret")
    @Mapping(target = "collectivite.nomCourt", source = "droitAgentCollectiviteDto.nomCourt")
    @Mapping(target = "collectivite.nomLong", source = "droitAgentCollectiviteDto.nomLong")
    @Mapping(target = "collectivite.etatCollectivite", source = "droitAgentCollectiviteDto.etatCollectivite")
    @Mapping(target = "utilisateur.id", source = "droitAgentCollectiviteDto.idUtilisateur")
    DroitAgent droitAgentCollectiviteDtoToDroitAgent(DroitAgentCollectiviteDto droitAgentCollectiviteDto);

}

/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DemandeProlongationDto;
import fr.gouv.sitde.face.application.dto.DemandeProlongationSimpleDto;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;

/**
 * Mapper entité/DTO des demandes de prolongation.
 *
 * @author Atos
 *
 */
@Mapper(uses = { DocumentMapper.class, DossierSubventionMapper.class }, componentModel = "spring")
public interface DemandeProlongationMapper {

    /**
     * Demande prolongation to demande prolongation simple dto.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation simple dto
     */
    @Mapping(target = "codeEtatDemande", source = "etatDemandeProlongation.code")
    @Mapping(target = "libelleEtatDemande", source = "etatDemandeProlongation.libelle")
    DemandeProlongationSimpleDto demandeProlongationToDemandeProlongationSimpleDto(DemandeProlongation demandeProlongation);

    /**
     * Demandes prolongation to demandes prolongation simple dto.
     *
     * @param listeDemandesProlongation
     *            the liste demandes prolongation
     * @return the list
     */
    List<DemandeProlongationSimpleDto> demandesProlongationToDemandesProlongationSimpleDto(List<DemandeProlongation> listeDemandesProlongation);

    /**
     * Demande prolongation to demande prolongation dto.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation dto
     */
    @Mapping(target = "etat", source = "etatDemandeProlongation.code")
    @Mapping(target = "etatLibelle", source = "etatDemandeProlongation.libelle")
    @Mapping(target = "idDossierSubvention", source = "dossierSubvention.id")
    @Mapping(target = "numDossier", source = "dossierSubvention.numDossier")
    @Mapping(target = "dateEcheanceAchevement", source = "dossierSubvention.dateEcheanceAchevement")
    @Mapping(target = "dateEcheanceAchevementDemande", source = "dateAchevementDemandee")
    @Mapping(target = "nomLongCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.nomLong")
    @Mapping(target = "codeDepartement", source = "dossierSubvention.dotationCollectivite.dotationDepartement.departement.code")
    DemandeProlongationDto demandeProlongationToDemandeProlongationDto(DemandeProlongation demandeProlongation);

    /**
     * Demande prolongation dto to demande prolongation.
     *
     * @param demandeProlongationDto
     *            the demande prolongation dto
     * @return the demande prolongation
     */
    @Mapping(target = "etatDemandeProlongation.code", source = "etat")
    @Mapping(target = "dossierSubvention.id", source = "idDossierSubvention")
    @Mapping(target = "dossierSubvention.numDossier", source = "numDossier")
    @Mapping(target = "dossierSubvention.dateEcheanceAchevement", source = "dateEcheanceAchevement")
    @Mapping(target = "dateAchevementDemandee", source = "dateEcheanceAchevementDemande")
    @Mapping(target = "dossierSubvention.dotationCollectivite.collectivite.nomLong", source = "nomLongCollectivite")
    @Mapping(target = "dossierSubvention.dotationCollectivite.dotationDepartement.departement.code", source = "codeDepartement")
    DemandeProlongation demandeProlongationDtoToDemandeProlongation(DemandeProlongationDto demandeProlongationDto);
}

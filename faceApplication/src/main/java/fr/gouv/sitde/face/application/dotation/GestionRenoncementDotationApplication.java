package fr.gouv.sitde.face.application.dotation;

import fr.gouv.sitde.face.application.dto.DotationRenoncementDto;

/**
 * The Interface GestionRenoncementDotationApplication.
 */
public interface GestionRenoncementDotationApplication {

    /**
     * Rechercher renoncement dotation par id collectivite et id sous programme.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation renoncement dto
     */
    DotationRenoncementDto rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme(Long idCollectivite, Integer idSousProgramme);

    /**
     * Renoncer dotation.
     *
     * @param dotationRenoncementDto
     *            the dotation renoncement dto
     * @return the dotation renoncement dto
     */
    DotationRenoncementDto renoncerDotation(DotationRenoncementDto dotationRenoncementDto);
}

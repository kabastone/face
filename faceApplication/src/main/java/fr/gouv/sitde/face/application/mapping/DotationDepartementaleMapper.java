/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DotationDepartementaleDto;
import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleBo;

/**
 * Mapper entité/DTO des dotations departementales.
 *
 * @author a702709
 *
 */
@Mapper(uses = { DotationDepartementaleParSousProgrammeMapper.class }, componentModel = "spring")
public interface DotationDepartementaleMapper {

    /**
     * Dotation departementale Bo to dotation departementale dto.
     *
     * @param dotationDepartementaleBo
     *            the dotation departementale
     * @return the dotation departementale dto
     */

    @Mapping(target = "idDepartement", source = "departement.id")
    @Mapping(target = "codeDepartement", source = "departement.code")
    @Mapping(target = "libelleDepartement", source = "departement.nom")
    DotationDepartementaleDto dotationDepartementaleDTOToDotationDepartementaleDto(DotationDepartementaleBo dotationDepartementaleBo);

    /**
     * Dotation departementale bo to dotation departementale dto.
     *
     * @param dotationDepartementaleBo
     *            the dotation departementale bo
     * @return the list
     */
    List<DotationDepartementaleDto> dotationDepartementaleBoToDotationDepartementaleDto(List<DotationDepartementaleBo> dotationDepartementaleBo);

    /**
     * Dotation departementale dto to dotation departementale Bo.
     *
     * @param dotationDepartementaleDto
     *            the dotation departementale
     * @return the dotation departementale
     */
    @Mapping(target = "departement.id", source = "idDepartement")
    @Mapping(target = "departement.code", source = "codeDepartement")
    @Mapping(target = "departement.nom", source = "libelleDepartement")
    DotationDepartementaleBo dotationDepartementaleDtoToDotationDepartementaleBo(DotationDepartementaleDto dotationDepartementaleDto);

    /**
     * Dotation departementale dto to dotation departementale Bo.
     *
     * @param dotationDepartementaleDto
     *            the dotation departementale dto
     * @return the list
     */
    List<DotationDepartementaleBo> dotationDepartementaleDtoToDotationDepartementaleBo(List<DotationDepartementaleDto> dotationDepartementaleDto);

}

/**
 *
 */
package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class DossierSubventionLovDto.
 */
public class DossierSubventionLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2992221961265166062L;

    /** The id. */
    private String id;

    /** The num dossier. */
    private String numDossier;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the num dossier.
     *
     * @return the num dossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the new num dossier
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DemandeSubventionSimpleDto.
 */
public class DemandeSubventionSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1252795816599208493L;

    /** The id. */
    private Long id;

    /** The code etat demande. */
    private String codeEtatDemande;

    /** The libelle etat demande. */
    private String libelleEtatDemande;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateDemande;

    /** The plafond aide. */
    private BigDecimal plafondAide = BigDecimal.ZERO;

    /** The demande complementaire. */
    private boolean demandeComplementaire;

    /** The type demande. */
    private String typeDemande;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code etat demande.
     *
     * @return the code etat demande
     */
    public String getCodeEtatDemande() {
        return this.codeEtatDemande;
    }

    /**
     * Sets the code etat demande.
     *
     * @param codeEtatDemande
     *            the new code etat demande
     */
    public void setCodeEtatDemande(String codeEtatDemande) {
        this.codeEtatDemande = codeEtatDemande;
    }

    /**
     * Gets the libelle etat demande.
     *
     * @return the libelle etat demande
     */
    public String getLibelleEtatDemande() {
        return this.libelleEtatDemande;
    }

    /**
     * Sets the libelle etat demande.
     *
     * @param libelleEtatDemande
     *            the new libelle etat demande
     */
    public void setLibelleEtatDemande(String libelleEtatDemande) {
        this.libelleEtatDemande = libelleEtatDemande;
    }

    /**
     * Gets the LocalDateTime demande.
     *
     * @return the LocalDateTime demande
     */
    public LocalDateTime getDateDemande() {
        return this.dateDemande;
    }

    /**
     * Sets the LocalDateTime demande.
     *
     * @param dateDemande
     *            the new LocalDateTime demande
     */
    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    /**
     * Gets the plafond aide.
     *
     * @return the plafondAide
     */
    public BigDecimal getPlafondAide() {
        return this.plafondAide;
    }

    /**
     * Sets the plafond aide.
     *
     * @param plafondAide the plafondAide to set
     */
    public void setPlafondAide(BigDecimal plafondAide) {
        this.plafondAide = plafondAide;
    }

    /**
     * Checks if is demande complementaire.
     *
     * @return true, if is demande complementaire
     */
    public boolean isDemandeComplementaire() {
        return this.demandeComplementaire;
    }

    /**
     * Sets the demande complementaire.
     *
     * @param demandeComplementaire
     *            the new demande complementaire
     */
    public void setDemandeComplementaire(boolean demandeComplementaire) {
        this.demandeComplementaire = demandeComplementaire;
    }

    /**
     * Gets the type demande.
     *
     * @return the type demande
     */
    public String getTypeDemande() {
        return this.typeDemande;
    }

    /**
     * Sets the type demande.
     *
     * @param typeDemande
     *            the new type demande
     */
    public void setTypeDemande(String typeDemande) {
        this.typeDemande = typeDemande;
    }

}

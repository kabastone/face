package fr.gouv.sitde.face.application.subvention;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.application.dto.AnomalieSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionDetailsDto;
import fr.gouv.sitde.face.application.dto.lov.DossierSubventionLovDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface GestionDemandeSubventionApplication.
 */
public interface GestionDemandeSubventionApplication {

    /**
     * Recherche d'un dossier subvention par id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the utilisateur
     */
    DemandeSubventionDetailsDto rechercherDemandeSubventionParId(Long idDemandeSubvention);

    /**
     * Modifier anomalie.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie simple dto
     */
    AnomalieSimpleDto modifierAnomalieCorrigeePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee);

    /**
     * Inits the nouvelle demande subvention.
     *
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto initNouvelleDemandeSubvention();

    /**
     * Inits the nouvelle demande subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto initNouvelleDemandeSubvention(Long idDossierSubvention);

    /**
     * Creer demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto creerDemandeSubvention(DemandeSubventionDetailsDto demande);

    /**
     * Qualifier demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubventionDetailsDto qualifierDemandeSubvention(DemandeSubventionDetailsDto demande, List<FichierTransfert> fichiersTransferts);

    /**
     * Accorder demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubventionDetailsDto accorderDemandeSubvention(Long id);

    /**
     * Controler demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubventionDetailsDto controlerDemandeSubvention(Long id);

    /**
     * Transferer demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubventionDetailsDto transfererDemandeSubvention(Long id);

    /**
     * Refuser demande subvention.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto refuserDemandeSubvention(Long id, String motifRefus);

    /**
     * Signaler anomalie demande subvention.
     *
     * @param id
     *            the demande subvention
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto signalerAnomalieDemandeSubvention(Long id);

    /**
     * Detecter anomalie demande subvention.
     *
     * @param demandeDto
     *            the demande dto
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto detecterAnomalieDemandeSubvention(DemandeSubventionDetailsDto demandeDto);

    /**
     * Corriger demande subvention.
     *
     * @param demandeDto
     *            the demande dto
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto corrigerDemandeSubvention(DemandeSubventionDetailsDto demandeDto);

    /**
     * Attribuer demande subvention.
     *
     * @param demandeDto
     *            the demande dto
     * @return the demande subvention details dto
     */
    DemandeSubventionDetailsDto attribuerDemandeSubvention(Long idDemandeSubvention, List<FichierTransfert> fichiersTransferts);

    /**
     * Recuperer dotation disponible.
     *
     * @param id
     *            the id
     * @param idCollectivite
     * @return the big decimal
     */
    BigDecimal recupererDotationDisponible(Long id, Integer idCollectivite);

    /**
     * Recuperer dossiers par collectivite.
     *
     * @param id
     *            the id
     * @return the list
     */
    List<DossierSubventionLovDto> recupererDossiersParDotationCollectivite(Long id);

    /**
     * @param id
     * @param tauxDemande
     * @return
     */
    DemandeSubventionDetailsDto rejeterPourTauxDemandeSubvention(Long id, BigDecimal tauxDemande);
}

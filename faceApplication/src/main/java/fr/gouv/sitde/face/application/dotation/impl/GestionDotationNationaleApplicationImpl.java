/**
 *
 */
package fr.gouv.sitde.face.application.dotation.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionDotationNationaleApplication;
import fr.gouv.sitde.face.application.dto.DotationProgrammeDto;
import fr.gouv.sitde.face.application.dto.DotationRepartitionNationaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationsSousProgrammeDto;
import fr.gouv.sitde.face.application.mapping.DotationProgrammeMapper;
import fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService;
import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class GestionDotationNationaleApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class GestionDotationNationaleApplicationImpl implements GestionDotationNationaleApplication {

    /** The dotation programme service. */
    @Inject
    private DotationNationaleService dotationNationaleService;

    /** The dotation programme mapper. */
    @Inject
    private DotationProgrammeMapper dotationProgrammeMapper;

    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionDotationNationaleApplication#rechercherDonneesDotationRepartitionNationale(java.lang.String)
     */
    @Override
    public DotationRepartitionNationaleDto rechercherDonneesDotationRepartitionNationale(String codeProgramme) {

        // Initialisation des dotations programme et dotations sous-programmes inexistantes pour l'année en cours.
        this.dotationNationaleService.initialiserDotationsInexistantesAnneeEnCours();

        List<DotationSousProgramme> listeDotationsSp = this.dotationNationaleService.rechercherDotationsSousProgrammesSur4Ans(codeProgramme);
        return this.calculerDotationNationaleDtoFromListeDotationSousProgramme(listeDotationsSp);
    }

    /**
     * Calculer dotation nationale dto from liste dotation sous programme.
     *
     * @param listeDotationsSousProgramme
     *            the liste dotations sous programme
     * @return the dotation nationale dto
     */
    private DotationRepartitionNationaleDto calculerDotationNationaleDtoFromListeDotationSousProgramme(
            List<DotationSousProgramme> listeDotationsSousProgramme) {

        final int anneeCourante = this.timeOperationTransverse.getAnneeCourante();
        DotationRepartitionNationaleDto dotaNatDto = new DotationRepartitionNationaleDto();

        LigneDotationsSousProgrammeDto ligneDotas = null;
        Integer idSousProgrammeCourant = 0;

        for (DotationSousProgramme dotaSp : listeDotationsSousProgramme) {

            if (!idSousProgrammeCourant.equals(dotaSp.getSousProgramme().getId())) {
                SousProgramme sp = dotaSp.getSousProgramme();
                idSousProgrammeCourant = sp.getId();
                ligneDotas = new LigneDotationsSousProgrammeDto();
                dotaNatDto.getListeLignesDotationsSousProgrammes().add(ligneDotas);
                ligneDotas.setIdSousProgramme(sp.getId());
                ligneDotas.setAbreviationSp(sp.getAbreviation());
                ligneDotas.setDescriptionSp(sp.getDescription());
                ligneDotas.setNumSousActionSp(sp.getAbreviation().equals(ConstantesFace.CODE_SP_SECURISATION) ? ConstantesFace.NUMERO_SP_SECURISATION
                        : sp.getNumSousAction());
                if (sp.getDateFinValidite() != null) {
                    ligneDotas.setAnneeFinValidite(sp.getDateFinValidite().getYear());
                }
            }

            if (dotaSp.getDotationProgramme().getAnnee() == anneeCourante) {
                ligneDotas.setIdDotationSousProgramme(dotaSp.getId());
                ligneDotas.setVersionDotationSousProgramme(dotaSp.getVersion());
                ligneDotas.setMontantAnneeN(dotaSp.getMontant());
                ligneDotas.setMontantInitialAnneeN(dotaSp.getMontantInitial());
                dotaNatDto.setSommeDotationsSpAnneeN(dotaNatDto.getSommeDotationsSpAnneeN().add(dotaSp.getMontant()));
                dotaNatDto.setSommeDotationsSpInitialAnneeN(dotaNatDto.getSommeDotationsSpInitialAnneeN().add(dotaSp.getMontantInitial()));

                if (dotaNatDto.getDotationProgramme() == null) {
                    dotaNatDto.setDotationProgramme(
                            this.dotationProgrammeMapper.dotationProgrammeToDotationProgrammeDto(dotaSp.getDotationProgramme()));
                }

            } else if (dotaSp.getDotationProgramme().getAnnee() == (anneeCourante - 1)) {
                ligneDotas.setMontantAnneeMoins1(dotaSp.getMontant());
                dotaNatDto.setSommeDotationsSpAnneeMoins1(dotaNatDto.getSommeDotationsSpAnneeMoins1().add(dotaSp.getMontant()));

            } else if (dotaSp.getDotationProgramme().getAnnee() == (anneeCourante - 2)) {
                ligneDotas.setMontantAnneeMoins2(dotaSp.getMontant());
                dotaNatDto.setSommeDotationsSpAnneeMoins2(dotaNatDto.getSommeDotationsSpAnneeMoins2().add(dotaSp.getMontant()));

            } else if (dotaSp.getDotationProgramme().getAnnee() == (anneeCourante - 3)) {
                ligneDotas.setMontantAnneeMoins3(dotaSp.getMontant());
                dotaNatDto.setSommeDotationsSpAnneeMoins3(dotaNatDto.getSommeDotationsSpAnneeMoins3().add(dotaSp.getMontant()));
            }
        }

        dotaNatDto.getListeLignesDotationsSousProgrammes().sort((LigneDotationsSousProgrammeDto l1,
                LigneDotationsSousProgrammeDto l2) -> Integer.parseInt(l1.getNumSousActionSp()) - Integer.parseInt(l2.getNumSousActionSp()));

        return dotaNatDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionDotationNationaleApplication#enregistrerMontantDotationProgramme(fr.gouv.sitde.face.application.
     * dto.DotationProgrammeDto)
     */
    @Override
    public DotationProgrammeDto enregistrerMontantDotationProgramme(DotationProgrammeDto dotationProgrammeDto) {
        DotationProgramme dotationProgramme = this.dotationProgrammeMapper.dotationProgrammeDtoToDotationProgramme(dotationProgrammeDto);
        dotationProgramme = this.dotationNationaleService.enregistrerMontantDotationProgramme(dotationProgramme);
        return this.dotationProgrammeMapper.dotationProgrammeToDotationProgrammeDto(dotationProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationNationaleApplication#enregistrerMontantDotationSousProgramme(fr.gouv.sitde.face.
     * application.dto.LigneDotationsSousProgrammeDto)
     */
    @Override
    public DotationRepartitionNationaleDto enregistrerMontantDotationSousProgramme(DotationSousProgrammeMontantBo dotationSousProgrammeMontantBo) {
        DotationSousProgramme dotationSousProgramme = this.dotationNationaleService
                .enregistrerMontantDotationSousProgramme(dotationSousProgrammeMontantBo);

        String codeProgramme = dotationSousProgramme.getDotationProgramme().getProgramme().getCodeNumerique();
        List<DotationSousProgramme> listeDotationsSp = this.dotationNationaleService.rechercherDotationsSousProgrammesSur4Ans(codeProgramme);
        return this.calculerDotationNationaleDtoFromListeDotationSousProgramme(listeDotationsSp);
    }

}

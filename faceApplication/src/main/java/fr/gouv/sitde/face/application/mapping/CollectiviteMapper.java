/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.CollectiviteDetailDto;
import fr.gouv.sitde.face.application.dto.CollectiviteSimpleDto;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * Mapper entité/DTO des collectivites.
 *
 * @author a453029
 *
 */
@Mapper(uses = { AdresseMapper.class, ReferentielLovMapper.class }, componentModel = "spring")
public interface CollectiviteMapper {

    /**
     * Collectivite to collectivite simple dto.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite simple dto
     */
    CollectiviteSimpleDto collectiviteToCollectiviteSimpleDto(Collectivite collectivite);

    /**
     * Collectivite simple dto to collectivite.
     *
     * @param collectiviteSimpleDto
     *            the collectivite simple dto
     * @return the collectivite
     */
    Collectivite collectiviteSimpleDtoToCollectivite(CollectiviteSimpleDto collectiviteSimpleDto);

    /**
     * Collectivites to collectivite simple dtos.
     *
     * @param collectivites
     *            the collectivites
     * @return the list
     */
    List<CollectiviteSimpleDto> collectivitesToCollectiviteSimpleDtos(List<Collectivite> collectivites);

    /**
     * Collectivite to collectivite detail dto.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite detail dto
     */
    CollectiviteDetailDto collectiviteToCollectiviteDetailDto(Collectivite collectivite);

    /**
     * Collectivite details dto to collectivite.
     *
     * @param collectiviteDetailsDto
     *            the collectivite details dto
     * @return the collectivite
     */
    Collectivite collectiviteDetailsDtoToCollectivite(CollectiviteDetailDto collectiviteDetailsDto);

}

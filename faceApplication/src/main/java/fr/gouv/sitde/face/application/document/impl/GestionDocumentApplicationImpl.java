/**
 *
 */
package fr.gouv.sitde.face.application.document.impl;

import java.io.File;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.document.GestionDocumentApplication;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;

/**
 * The Class GestionDocumentApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionDocumentApplicationImpl implements GestionDocumentApplication {

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.document.GestionDocumentApplication#telechargerDocument(java.lang.Long)
     */
    @Override
    public File telechargerDocument(Long idDocument) {
        return this.documentService.chargerFichierDocument(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.document.GestionDocumentApplication#supprimerDocument(java.lang.Long)
     */
    @Override
    public void supprimerDocument(Long idDocument) {
        this.documentService.supprimerDocument(idDocument);
    }
}

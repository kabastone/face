/*
 *
 */
package fr.gouv.sitde.face.application.commun;

import java.util.List;

import fr.gouv.sitde.face.application.dto.AnomalieDto;

/**
 * The Class GestionAnomalieApplication.
 *
 * @author Atos
 */
public interface GestionAnomalieApplication {

    /**
     * Ajouter une nouvelle anomalie.
     *
     * @param anomalieDto
     *            the anomalie dto
     * @return the anomalie dto
     */
    AnomalieDto ajouterAnomalie(AnomalieDto anomalieDto);

    /**
     * Update anomalie.
     *
     * @param anomalieDto
     *            the anomalie dto
     * @return the anomalie dto
     */
    AnomalieDto updateAnomalie(AnomalieDto anomalieDto);

    /**
     * Pour une demande de paiement : mettre à jour la correction d'une anomalie.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie dto
     */
    AnomalieDto majCorrectionAnomaliePourPaiement(Long idDemandePaiement, Long idAnomalie, boolean corrigee);

    /**
     * Pour une demande de subvention : mettre à jour la correction d'une anomalie.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie dto
     */
    AnomalieDto majCorrectionAnomaliePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee);

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the list
     */
    List<AnomalieDto> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rechercher anomalies par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the list
     */
    List<AnomalieDto> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement);

    /**
     * Rechercher anomalies par id demande subvention pour AODE.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the list
     */
    List<AnomalieDto> rechercherAnomaliesParIdDemandeSubventionPourAODE(Long idDemandeSubvention);

    /**
     * Rechercher anomalies par id demande paiement pour AODE.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the list
     */
    List<AnomalieDto> rechercherAnomaliesParIdDemandePaiementPourAODE(Long idDemandePaiement);
}

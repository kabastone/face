/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DocumentDto;
import fr.gouv.sitde.face.transverse.entities.Document;

/**
 * Mapper entité/DTO des documents.
 *
 * @author Atos
 *
 */
@Mapper(componentModel = "spring")
public interface DocumentMapper {

    /**
     * Document to document dto.
     *
     * @param document the document
     * @return the document dto
     */

    @Mapping(target = "codeTypeDocument", source = "typeDocument.code")
    DocumentDto documentToDocumentDto(Document document);


    /**
     * Documents to documents dto.
     *
     * @param listeDocuments the liste documents
     * @return the list
     */
    List<DocumentDto> documentsToDocumentsDto(List<Document> listeDocuments);
}

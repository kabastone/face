/**
 *
 */
package fr.gouv.sitde.face.application.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.GestionListeDiffusionApplication;
import fr.gouv.sitde.face.application.dto.AdresseEmailDto;
import fr.gouv.sitde.face.application.mapping.ListeDiffusionMapper;
import fr.gouv.sitde.face.domaine.service.administration.ListeDiffusionService;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * The Class GestionListeDiffusionApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionListeDiffusionApplicationImpl implements GestionListeDiffusionApplication {

    /** The liste diffusion service. */
    @Inject
    private ListeDiffusionService listeDiffusionService;

    /** The liste diffusion mapper. */
    @Inject
    private ListeDiffusionMapper listeDiffusionMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionListeDiffusionApplication#rechercherAdressesEmail(java.lang.Long)
     */
    @Override
    public List<AdresseEmailDto> rechercherAdressesEmail(Long idCollectivite) {
        List<AdresseEmail> listeAdresses = this.listeDiffusionService.rechercherAdressesEmail(idCollectivite);
        return this.listeDiffusionMapper.adresseEmailsToListeDiffusionsDto(listeAdresses);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionListeDiffusionApplication#supprimerAdresseEmail(java.lang.Long)
     */
    @Override
    public void supprimerAdresseEmail(Long idAdresseEmail) {
        this.listeDiffusionService.supprimerAdresseEmail(idAdresseEmail);
    }

    @Override
    public AdresseEmailDto ajouterAdresseEmailCollectivite(String adresseEmail, Long idCollectivite) {
        AdresseEmail nouvelleAdresseEmail = this.listeDiffusionService.ajouterAdresseEmailCollectivite(adresseEmail, idCollectivite);
        return this.listeDiffusionMapper.adresseEmailToListeDiffusionDto(nouvelleAdresseEmail);
    }
}

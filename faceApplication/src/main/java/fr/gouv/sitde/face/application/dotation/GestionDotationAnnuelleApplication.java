package fr.gouv.sitde.face.application.dotation;

import fr.gouv.sitde.face.application.dto.DotationAnnuelleDto;

/**
 * The Interface GestionDotationAnnuelleApplication.
 */
public interface GestionDotationAnnuelleApplication {

    /**
     * Rechercher dotation annuelle.
     *
     * @param dotationAnnuelleDto
     *            the dotation annuelle dto
     * @return the dotation annuelle dto
     */
    DotationAnnuelleDto rechercherDotationAnnuelle(DotationAnnuelleDto dotationAnnuelleDto);

}

/**
 *
 */
package fr.gouv.sitde.face.application.dto.tdb;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class DotationCollectiviteTdbDto.
 *
 * @author a453029
 */
public class DotationCollectiviteTdbDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7060772889268768029L;

    /** The id. */
    private Long id;

    /** The description sous programme. */
    private String descriptionSousProgramme;

    /** The id sous programme. */
    private Integer idSousProgramme;

    /** The montant. */
    private BigDecimal montant = BigDecimal.ZERO;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the description sous programme.
     *
     * @return the descriptionSousProgramme
     */
    public String getDescriptionSousProgramme() {
        return this.descriptionSousProgramme;
    }

    /**
     * Sets the description sous programme.
     *
     * @param descriptionSousProgramme
     *            the descriptionSousProgramme to set
     */
    public void setDescriptionSousProgramme(String descriptionSousProgramme) {
        this.descriptionSousProgramme = descriptionSousProgramme;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * @return the idSousProgramme
     */
    public Integer getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * @param idSousProgramme
     *            the idSousProgramme to set
     */
    public void setIdSousProgramme(Integer idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

}

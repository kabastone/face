/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.mapping.TableauDeBordMapper;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Class TableauDeBordSd7ApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class TableauDeBordSd7ApplicationImpl implements TableauDeBordSd7Application {

    /** The tableau de bord sd 7 service. */
    @Inject
    private TableauDeBordSd7Service tableauDeBordSd7Service;

    /** The tableau de bord mapper. */
    @Inject
    private TableauDeBordMapper tableauDeBordMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application#rechercherTableauDeBordSd7()
     */
    @Override
    public TableauDeBordSd7Dto rechercherTableauDeBordSd7() {
        return this.tableauDeBordSd7Service.rechercherTableauDeBordSd7();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application#rechercherDemandesSubventionTdbSd7AcontrolerSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbSd7AcontrolerSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordSd7Service
                .rechercherDemandesSubventionTdbSd7AcontrolerSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application#rechercherDemandesPaiementTdbSd7AcontrolerPaiement(fr.gouv.sitde.face.
     * transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbSd7AcontrolerPaiement(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordSd7Service
                .rechercherDemandesPaiementTdbSd7AcontrolerPaiement(pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application#rechercherDemandesSubventionTdbSd7AtransfererSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbSd7AtransfererSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordSd7Service
                .rechercherDemandesSubventionTdbSd7AtransfererSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application#rechercherDemandesPaiementTdbSd7AtransfererPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbSd7AtransfererPaiement(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordSd7Service
                .rechercherDemandesPaiementTdbSd7AtransfererPaiement(pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

}

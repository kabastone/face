/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DossierSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DotationCollectiviteTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;

/**
 * The Interface TableauDeBordGeneApplication.
 *
 * @author a453029
 */
public interface TableauDeBordGeneApplication {

    /**
     * Rechercher tableau de bord generique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the tableau de bord gene dto
     */
    TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite);

    /**
     * Rechercher dotations collectivite tdb gene ademander subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DotationCollectiviteTdbDto> rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene ademander subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAdemanderPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a completer subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DotationCollectiviteTdbDto> rechercherDotationCollectiviteTdbGeneACompleterSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb gene a corriger subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbGeneAcorrigerSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb gene a corriger paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbGeneAcorrigerPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a commencer paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAcommencerPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a solder paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubventionTdbDto> rechercherDossiersSubventionTdbGeneAsolderPaiement(Long idCollectivite, PageDemande pageDemande);

}

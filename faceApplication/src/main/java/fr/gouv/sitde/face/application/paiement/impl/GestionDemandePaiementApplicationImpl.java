/**
 *
 */
package fr.gouv.sitde.face.application.paiement.impl;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.DemandePaiementDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.application.mapping.DemandePaiementMapper;
import fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication;
import fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class GestionDemandePaiementApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionDemandePaiementApplicationImpl implements GestionDemandePaiementApplication {

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The demande paiement mapper. */
    @Inject
    private DemandePaiementMapper demandePaiementMapper;

    /** The etat demande paiement service. */
    @Inject
    private EtatDemandePaiementService etatDemandePaiementService;

    /** The donnees document service. */
    @Inject
    private DonneesDocumentService donneesDocumentService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    @Inject
    private ReglementaireService reglementaireService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionPaiementApplication#initNouvelleDemandePaiement(java.lang.Long)
     */
    @Override
    public DemandePaiementDto initNouvelleDemandePaiement(Long idDossierSubvention) {
        // appel métier
        DossierSubvention dossierSubvention = this.dossierSubventionService.rechercherDossierSubventionParId(idDossierSubvention);

        /* mappings */
        DemandePaiementDto demandePaiementDto = new DemandePaiementDto();
        // numéro du dossier
        demandePaiementDto.setNumDossier(dossierSubvention.getNumDossier());
        // reste à consommer
        demandePaiementDto.setResteConsommer(dossierSubvention.getResteConsommerCalcule());
        // code département
        demandePaiementDto.setCodeDepartement(dossierSubvention.getDotationCollectivite().getDotationDepartement().getDepartement().getCode());
        // nom long collectivité
        demandePaiementDto.setNomLongCollectivite(dossierSubvention.getDotationCollectivite().getCollectivite().getNomLong());
        // date du jour
        demandePaiementDto.setDateDemande(this.timeOperationTransverse.now());
        // etat de la demande
        demandePaiementDto.setCodeEtatDemande(ConstantesFace.CODE_DEM_PAIEMENT_INITIAL);

        demandePaiementDto.setMontantTotalHtTravaux(this.demandePaiementMapper.calculMontantTotalDossier(dossierSubvention));
        // Montant restant avance
        demandePaiementDto.setMontantRestantAvance(this.demandePaiementService.calculerAvanceMaximumDemande(dossierSubvention));

        // Montant restant acompte
        demandePaiementDto.setMontantRestantAcompte(this.demandePaiementService.calculerAcompteMaximumDemande(dossierSubvention));

        demandePaiementDto.setMontantSubvention(dossierSubvention.getPlafondAide());

        demandePaiementDto.setIsPremiereDemande(dossierSubvention.getDemandesPaiement().isEmpty());

        demandePaiementDto.setHasUnAcompte(dossierSubvention.getDemandesPaiement().stream().map(DemandePaiement::getTypeDemandePaiement)
                .anyMatch(TypeDemandePaiementEnum.ACOMPTE::equals));

        // Taux réglementaires d'aide pour les subventions
        Reglementaire regle = this.reglementaireService.rechercherReglementaire(
                dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
        demandePaiementDto.setTauxAideFacePercent(new BigDecimal(regle.getTauxAideSubvention()));
        demandePaiementDto.setTauxAvanceReglementaire(new BigDecimal(regle.getTauxAidePaiementAvance()));

        // liste des types de demande
        TypeDemandePaiementLovDto typeDemandePaiementLovDto = new TypeDemandePaiementLovDto();
        typeDemandePaiementLovDto.setCode(TypeDemandePaiementEnum.ACOMPTE.toString());
        typeDemandePaiementLovDto.setLibelle(TypeDemandePaiementEnum.ACOMPTE.getLibelle());
        demandePaiementDto.setTypeDemande(typeDemandePaiementLovDto);

        return demandePaiementDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionPaiementApplication#realiserDemandePaiement(fr.gouv.sitde.face.application.dto.
     * DemandePaiementDto)
     */
    @Override
    public DemandePaiementDto realiserDemandePaiement(DemandePaiementDto demandePaiementDto) {
        // mapping vers entite
        DemandePaiement demandePaiement = this.demandePaiementMapper.demandePaiementDtoToDemandePaiement(demandePaiementDto);

        // si l'état est 'INITIAL' (ou non renseigné) : la demande va passer à l'état DEMANDEE
        if ((demandePaiement.getEtatDemandePaiement() == null)
                || (ConstantesFace.CODE_DEM_PAIEMENT_INITIAL.equals(demandePaiement.getEtatDemandePaiement().getCode()))) {
            EtatDemandePaiement etatDemandePaiement = this.etatDemandePaiementService.rechercherParCode(EtatDemandePaiementEnum.DEMANDEE.toString());
            demandePaiement.setEtatDemandePaiement(etatDemandePaiement);
            // mise en place de la date de demande
            demandePaiement.setDateDemande(this.timeOperationTransverse.now());
        } else {
            // le code d'état est alimenté OU il est différent de 'INITIAL' (= cas d'une modification d'une demande de paiement)
            EtatDemandePaiement etatDemandePaiement = this.etatDemandePaiementService
                    .rechercherParCode(demandePaiement.getEtatDemandePaiement().getCode());
            demandePaiement.setEtatDemandePaiement(etatDemandePaiement);
        }

        // appel métiers
        DemandePaiement demandeRealisee = this.demandePaiementService.realiserDemandePaiement(demandePaiement,
                demandePaiementDto.getListeFichiersEnvoyer());

        // retour
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandeRealisee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionPaiementApplication#rechercheDemandePaiement(java.lang.Long)
     */
    @Override
    public DemandePaiementDto rechercheDemandePaiement(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.rechercherDemandePaiementAvecDocuments(idDemandePaiement);

        // mapping
        DemandePaiementDto dto = this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);

        // isPremiereDemande
        dto.setIsPremiereDemande(demandePaiement.getNumOrdre() == 1);

        return dto;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionPaiementApplication#genererDecisionAttributive(java.lang.Long)
     */
    @Override
    public FichierTransfert genererDecisionAttributive(Long idDemandePaiement) {
        return this.donneesDocumentService.genererDecisionAttributive(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementAnomalieDetectee(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementAnomalieDetectee(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementAnomalieDetectee(idDemandePaiement);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementAnomalieSignalee(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementAnomalieSignalee(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementAnomalieSignalee(idDemandePaiement);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementRefusee(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementRefusee(Long idDemandePaiement, String motifRefus) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementRefusee(idDemandePaiement, motifRefus);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementQualifiee(fr.gouv.sitde.face.application.dto.
     * DemandePaiementDto)
     */
    @Override
    public DemandePaiementDto majDemandePaiementQualifiee(DemandePaiementDto demandePaiementDto) {
        // mapping vers entite
        DemandePaiement demandePaiement = this.demandePaiementMapper.demandePaiementDtoToDemandePaiement(demandePaiementDto);

        // appel métier
        DemandePaiement demandePaiementMaj = this.demandePaiementService.majDemandePaiementQualifiee(demandePaiement,
                demandePaiementDto.getListeFichiersEnvoyer());

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiementMaj);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementAccordee(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementAccordee(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementAccordee(idDemandePaiement);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementControlee(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementControlee(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementControlee(idDemandePaiement);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementEnAttenteTransfert(java.lang.Long)
     */
    @Override
    public DemandePaiementDto majDemandePaiementEnAttenteTransfert(Long idDemandePaiement) {
        // appel métier
        DemandePaiement demandePaiement = this.demandePaiementService.majDemandePaiementEnAttenteTransfert(idDemandePaiement);

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication#majDemandePaiementCorrigee(fr.gouv.sitde.face.application.dto.
     * DemandePaiementDto)
     */
    @Override
    public DemandePaiementDto enregistrerDemandePaiementPourCorrection(DemandePaiementDto demandePaiementDto) {
        // mapping vers entite
        DemandePaiement demandePaiement = this.demandePaiementService.rechercherDemandePaiement(demandePaiementDto.getIdDemande());

        this.demandePaiementMapper.updateDemandePaiementFromDemandePaiementDto(demandePaiementDto, demandePaiement);

        // appel métier
        DemandePaiement demandePaiementMaj = this.demandePaiementService.enregistrerDemandePaiementPourCorrection(demandePaiement,
                demandePaiementDto.getListeFichiersEnvoyer(), demandePaiementDto.getIdsDocsComplSuppr());

        // mapping
        return this.demandePaiementMapper.demandePaiementToDemandePaiementDto(demandePaiementMaj);
    }
}

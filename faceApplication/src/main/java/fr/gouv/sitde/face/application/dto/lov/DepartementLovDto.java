package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class DepartementLovDto.
 */
public class DepartementLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7834459428872345394L;

    /** The id. */
    private int id;

    /** The code. */
    private String code;

    /** The nom. */
    private String nom;

    /** The code et nom. */
    private String codeEtNom;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the code et nom.
     *
     * @return the codeEtNom
     */
    public String getCodeEtNom() {
        return this.codeEtNom;
    }

    /**
     * Sets the code et nom.
     *
     * @param codeEtNom
     *            the codeEtNom to set
     */
    public void setCodeEtNom(String codeEtNom) {
        this.codeEtNom = codeEtNom;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.application.administration;

import fr.gouv.sitde.face.transverse.document.OrigineDocumentDto;

/**
 * The Interface RechercheCollectiviteOrigineApplication.
 *
 * @author a453029
 */
public interface RechercheCollectiviteOrigineApplication {

    /**
     * Rechercher id collectivite par dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the long
     */
    Long rechercherIdCollectiviteParDotationCollectivite(Long idDotationCollectivite);

    /**
     * Rechercher id collectivite par dossier subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the long
     */
    Long rechercherIdCollectiviteParDossierSubvention(Long idDossierSubvention);

    /**
     * Rechercher id collectivite par demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rechercher id collectivite par demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandeProlongation(Long idDemandeProlongation);

    /**
     * Rechercher id collectivite par demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandePaiement(Long idDemandePaiement);

    /**
     * Rechercher collectivite origine par document.
     *
     * @param idDocument
     *            the id document
     * @return the origine document dto
     */
    OrigineDocumentDto rechercherCollectiviteOrigineParDocument(Long idDocument);
}

/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * Mapper enum/DTO du type de demande paiement.
 *
 * @author Atos
 */
@Mapper(componentModel = "spring")
public interface TypeDemandePaiementMapper {

    /**
     * Type demande paiement enum to type demande paiement lov dto.
     *
     * @param typeDemandePaiementEnum
     *            the type demande paiement enum
     * @return the type demande paiement lov dto
     */
    default TypeDemandePaiementLovDto typeDemandePaiementEnumToTypeDemandePaiementLovDto(TypeDemandePaiementEnum typeDemandePaiementEnum) {
        if (typeDemandePaiementEnum == null) {
            return null;
        }

        TypeDemandePaiementLovDto typeDemandePaiementLovDto = new TypeDemandePaiementLovDto();
        typeDemandePaiementLovDto.setCode(typeDemandePaiementEnum.getCode());
        typeDemandePaiementLovDto.setLibelle(typeDemandePaiementEnum.getLibelle());

        return typeDemandePaiementLovDto;
    }

    /**
     * Type demande paiement enum to type demande paiement lov dto.
     *
     * @param typeDemandePaiementLovDto the type demande paiement lov dto
     * @return the type demande paiement enum
     */
    default TypeDemandePaiementEnum typeDemandePaiementEnumToTypeDemandePaiementLovDto(TypeDemandePaiementLovDto typeDemandePaiementLovDto) {
        if ((typeDemandePaiementLovDto == null) || (typeDemandePaiementLovDto.getCode() == null)) {
            return null;
        }

        switch (typeDemandePaiementLovDto.getCode()) {
        case "ACOMPTE":
            return TypeDemandePaiementEnum.ACOMPTE;
        case "AVANCE":
            return TypeDemandePaiementEnum.AVANCE;
        case "SOLDE":
            return TypeDemandePaiementEnum.SOLDE;
        default:
            return null;
        }
    }
}

/**
 *
 */
package fr.gouv.sitde.face.application.dto.tdb;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DemandeProlongationTdbDto.
 *
 * @author a453029
 */
public class DemandeProlongationTdbDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -965692766134507457L;

    /** The id. */
    private Long id;

    /** The date creation dossier. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateCreationDossier;

    /** The nom court collectivite. */
    private String nomCourtCollectivite;

    /** The num dossier. */
    private String numDossier;

    /** The date achevement demandee. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateAchevementDemandee;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the date creation dossier.
     *
     * @return the dateCreationDossier
     */
    public LocalDateTime getDateCreationDossier() {
        return this.dateCreationDossier;
    }

    /**
     * Sets the date creation dossier.
     *
     * @param dateCreationDossier
     *            the dateCreationDossier to set
     */
    public void setDateCreationDossier(LocalDateTime dateCreationDossier) {
        this.dateCreationDossier = dateCreationDossier;
    }

    /**
     * Gets the nom court collectivite.
     *
     * @return the nomCourtCollectivite
     */
    public String getNomCourtCollectivite() {
        return this.nomCourtCollectivite;
    }

    /**
     * Sets the nom court collectivite.
     *
     * @param nomCourtCollectivite
     *            the nomCourtCollectivite to set
     */
    public void setNomCourtCollectivite(String nomCourtCollectivite) {
        this.nomCourtCollectivite = nomCourtCollectivite;
    }

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the date achevement demandee.
     *
     * @return the dateAchevementDemandee
     */
    public LocalDateTime getDateAchevementDemandee() {
        return this.dateAchevementDemandee;
    }

    /**
     * Sets the date achevement demandee.
     *
     * @param dateAchevementDemandee
     *            the dateAchevementDemandee to set
     */
    public void setDateAchevementDemandee(LocalDateTime dateAchevementDemandee) {
        this.dateAchevementDemandee = dateAchevementDemandee;
    }

}

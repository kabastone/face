package fr.gouv.sitde.face.application.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.application.jsonutils.CustomTypeDotationDepartementDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomTypeDotationDepartementSerializer;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * The Class LigneDotationDepartementaleDto.
 */
public class LigneDotationDepartementaleDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1867294428474102722L;

    /**
     * The instance id.
     */
    private Long idLigneDotationDepartementale;

    /**
     * The departement id.
     */
    private Long idDepartement;

    /** The num ordre. */
    private Short numOrdre;

    /** The montant. */
    private BigDecimal montant = BigDecimal.ZERO;

    /** The type dotation departement. */
    @JsonSerialize(using = CustomTypeDotationDepartementSerializer.class)
    @JsonDeserialize(using = CustomTypeDotationDepartementDeserializer.class)
    private TypeDotationDepartementEnum typeDotationDepartement;

    /** The LocalDateTime envoi. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEnvoi;

    /** The sous programme. */
    private SousProgrammeDto sousProgramme;

    public LigneDotationDepartementaleDto() {
    }

    public LigneDotationDepartementaleDto(Long idLigneDotationDepartementale, Long idDepartement, Short numOrdre, BigDecimal montant,
                                          TypeDotationDepartementEnum typeDotationDepartement, LocalDateTime dateEnvoi, SousProgrammeDto sousProgramme) {
        this.idLigneDotationDepartementale = idLigneDotationDepartementale;
        this.idDepartement = idDepartement;
        this.numOrdre = numOrdre;
        this.montant = montant;
        this.typeDotationDepartement = typeDotationDepartement;
        this.dateEnvoi = dateEnvoi;
        this.sousProgramme = sousProgramme;
    }

    public Long getIdLigneDotationDepartementale() {
        return idLigneDotationDepartementale;
    }

    public void setIdLigneDotationDepartementale(Long idLigneDotationDepartementale) {
        this.idLigneDotationDepartementale = idLigneDotationDepartementale;
    }

    public Long getIdDepartement() {
        return idDepartement;
    }

    public void setIdDepartement(Long idDepartement) {
        this.idDepartement = idDepartement;
    }

    public Short getNumOrdre() {
        return numOrdre;
    }

    public void setNumOrdre(Short numOrdre) {
        this.numOrdre = numOrdre;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public TypeDotationDepartementEnum getTypeDotationDepartement() {
        return typeDotationDepartement;
    }

    public void setTypeDotationDepartement(TypeDotationDepartementEnum typeDotationDepartement) {
        this.typeDotationDepartement = typeDotationDepartement;
    }

    public LocalDateTime getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(LocalDateTime dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public SousProgrammeDto getSousProgramme() {
        return sousProgramme;
    }

    public void setSousProgramme(SousProgrammeDto sousProgramme) {
        this.sousProgramme = sousProgramme;
    }
}


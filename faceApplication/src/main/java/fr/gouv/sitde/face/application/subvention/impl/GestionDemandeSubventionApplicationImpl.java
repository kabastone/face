/**
 *
 */
package fr.gouv.sitde.face.application.subvention.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.AnomalieSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionDetailsDto;
import fr.gouv.sitde.face.application.dto.lov.DossierSubventionLovDto;
import fr.gouv.sitde.face.application.mapping.AnomalieMapper;
import fr.gouv.sitde.face.application.mapping.CadencementMapper;
import fr.gouv.sitde.face.application.mapping.DemandeSubventionMapper;
import fr.gouv.sitde.face.application.mapping.DossierSubventionMapper;
import fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class GestionDossierSubventionApplicationImpl.
 *
 * @author Atos
 */
@Service
public class GestionDemandeSubventionApplicationImpl implements GestionDemandeSubventionApplication {

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande subvention simple mapper. */
    @Inject
    private DemandeSubventionMapper demandeSubventionMapper;

    /** The dossier subvention mapper. */
    @Inject
    private DossierSubventionMapper dossierSubventionMapper;

    /** The anomalie mapper. */
    @Inject
    private AnomalieMapper anomalieMapper;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    @Inject
    private CadencementMapper cadencementMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDossierSubventionApplication#rechercherDossierSubventionParId(java.lang.Long)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto rechercherDemandeSubventionParId(Long idDemandeSubvention) {
        DemandeSubvention demandeSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);
        DemandeSubventionDetailsDto dto = this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demandeSubvention);
        DotationCollectivite dotation = demandeSubvention.getDossierSubvention().getDotationCollectivite();
        if (dotation.getDotationDepartement().getDotationSousProgramme().getSousProgramme().isDeProjet()) {
            dto.setDotationDisponible(null);
        } else {
            dto.setDotationDisponible(this.dotationCollectiviteService.calculerDotationDisponible(dotation));
        }
        return dto;
    }

    /**
     * Modifier anomalie corrigee.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie simple dto
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#modifierAnomalie(java.lang.Long, java.lang.Long, boolean)
     */
    @Override
    @Transactional
    public AnomalieSimpleDto modifierAnomalieCorrigeePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee) {
        Anomalie anomalie = this.anomalieService.majCorrectionAnomaliePourSubvention(idDemandeSubvention, idAnomalie, corrigee);
        return this.anomalieMapper.anomalieToAnomalieSimpleDto(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#initNouvelleDemandeSubvention()
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto initNouvelleDemandeSubvention() {

        DemandeSubventionDetailsDto demande = this.demandeSubventionMapper
                .demandeSubventionToDemandeSubventionDetailsDto(this.demandeSubventionService.initNouvelleDemandeSubvention(null));
        demande.setEtat(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL);
        demande.setEtatLibelle(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL);
        return demande;
    }

    @Override
    @Transactional
    public DemandeSubventionDetailsDto initNouvelleDemandeSubvention(Long idDossierSubvention) {

        DemandeSubventionDetailsDto demande = this.demandeSubventionMapper
                .demandeSubventionToDemandeSubventionDetailsDto(this.demandeSubventionService.initNouvelleDemandeSubvention(idDossierSubvention));
        demande.setEtat(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL);
        demande.setEtatLibelle(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL);
        demande.setDemandeComplementaire(true);
        return demande;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#creerDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto creerDemandeSubvention(DemandeSubventionDetailsDto demandeSubventionDto) {
        DemandeSubvention demande = this.demandeSubventionMapper.demandeSubventionDetailsDtoToDemandeSubvention(demandeSubventionDto);

        demande = this.demandeSubventionService.creerDemandeSubvention(demande, demandeSubventionDto.getListeFichiersEnvoyer());

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#qualifierDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional(value = TxType.REQUIRES_NEW)
    public DemandeSubventionDetailsDto qualifierDemandeSubvention(DemandeSubventionDetailsDto demandeDto, List<FichierTransfert> fichiersTransferts) {

        DemandeSubvention demande = this.demandeSubventionMapper.demandeSubventionDetailsDtoToDemandeSubvention(demandeDto);
        demande = this.demandeSubventionService.qualifierDemandeSubvention(demande, fichiersTransferts);
        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#accorderDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto accorderDemandeSubvention(Long id) {
        DemandeSubvention demande = this.demandeSubventionService.accorderDemandeSubvention(id);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#controlerDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto controlerDemandeSubvention(Long id) {
        DemandeSubvention demande = this.demandeSubventionService.controlerDemandeSubvention(id);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#transfererDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto transfererDemandeSubvention(Long id) {
        DemandeSubvention demande = this.demandeSubventionService.transfererDemandeSubvention(id);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#refuserDemandeSubvention(java.lang.Long)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto refuserDemandeSubvention(Long idDemande, String motifRefus) {
       DemandeSubvention demande = this.demandeSubventionService.refuserDemandeSubvention(idDemande, motifRefus);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#signalerAnomalieDemandeSubvention(fr.gouv.sitde.face.application.
     * dto.DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto signalerAnomalieDemandeSubvention(Long idDemandeSubvention) {
        DemandeSubvention demande = this.demandeSubventionService.signalerAnomalieDemandeSubvention(idDemandeSubvention);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#detecterAnomalieDemandeSubvention(fr.gouv.sitde.face.application.
     * dto.DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto detecterAnomalieDemandeSubvention(DemandeSubventionDetailsDto demandeDto) {
        DemandeSubvention demande = this.demandeSubventionMapper.demandeSubventionDetailsDtoToDemandeSubvention(demandeDto);
        demande = this.demandeSubventionService.detecterAnomalieDemandeSubvention(demande);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#corrigerDemandeSubvention(fr.gouv.sitde.face.application.dto.
     * DemandeSubventionDetailsDto)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto corrigerDemandeSubvention(DemandeSubventionDetailsDto demandeDto) {

        DemandeSubvention demande = this.demandeSubventionService.rechercherDemandeSubventionParId(demandeDto.getId());

        this.demandeSubventionMapper.updateDemandeSubventionFromDemandeSubventionDetailsDto(demandeDto, demande);
        this.cadencementMapper.updateCadencementFromCadencementDto(demande.getCadencement(), demandeDto.getCadencementDto());

        demande = this.demandeSubventionService.enregistrerPourCorrectionDemandeSubvention(demande, demandeDto.getListeFichiersEnvoyer(),
                demandeDto.getIdsDocsComplSuppr());

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /**
     * Attribuer demande subvention.
     *
     * @param demandeDto
     *            the demande dto
     * @return the demande subvention details dto
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto attribuerDemandeSubvention(Long idDemandeSubvention, List<FichierTransfert> fichiersTransferts) {
        DemandeSubvention demande = this.demandeSubventionService.attribuerDemandeSubvention(idDemandeSubvention, fichiersTransferts);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#recupererDotationDisponible(java.lang.Long)
     */
    @Override
    @Transactional
    public BigDecimal recupererDotationDisponible(Long idCollectivite, Integer idSousProgramme) {
        DotationCollectivite dotation = this.dotationCollectiviteService
                .rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);

        if (dotation == null) {
            return null;
        }

        return this.dotationCollectiviteService.calculerDotationDisponible(dotation);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#recupererDossiersParCollectivite(java.lang.Long)
     */
    @Override
    @Transactional
    public List<DossierSubventionLovDto> recupererDossiersParDotationCollectivite(Long id) {
        List<DossierSubvention> listeDossiers = this.dotationCollectiviteService.recupererDossiersParDotationCollectivite(id);
        return this.dossierSubventionMapper.dossierSubventionsToDossierSubventionLovDtos(listeDossiers);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication#rejeterPourTauxDemandeSubvention(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    @Transactional
    public DemandeSubventionDetailsDto rejeterPourTauxDemandeSubvention(Long id, BigDecimal tauxDemande) {
        DemandeSubvention demande = this.demandeSubventionService.rejeterPourTauxDemandeSubvention(id, tauxDemande);

        return this.demandeSubventionMapper.demandeSubventionToDemandeSubventionDetailsDto(demande);
    }

}

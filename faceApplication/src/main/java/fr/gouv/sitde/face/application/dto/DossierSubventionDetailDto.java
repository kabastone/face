package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;

/**
 * The Class DossierSubventionDetailDto.
 */
public class DossierSubventionDetailDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -534514500689175061L;

    /** The num dossier. */
    private String numDossier;

    /** The etat dossier. */
    private EtatDossierEnum etatDossier;

    /** The annee. */
    private int annee;

    /** The description sous programme. */
    private String descriptionSousProgramme;

    /** The engagement juridique. */
    private String engagementJuridique;

    /** The LocalDateTime echeance lancement. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEcheanceLancement;

    /** The LocalDateTime echeance achevement. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEcheanceAchevement;

    /** The nom long. */
    private String nomLongCollectivite;

    /** The code departement. */
    private String codeDepartement;

    /** The reste disponible. */
    private BigDecimal resteDisponible;

    /** The plafond aide. */
    private BigDecimal plafondAide;

    /** The taux consommation calcule. */
    private BigDecimal tauxConsommationCalcule;

    /** The aide versee calcule. */
    private BigDecimal aideVerseeCalcule;

    /** The id dotation collectivite. */
    private Long idDotationCollectivite;

    /** The montant previsionel travaux. */
    private BigDecimal montantPrevisionelTravaux;

    /** The montant travaux verses. */
    private BigDecimal montantTravauxVerses;

    /** The est unique subvention par sous programme. */
    private Boolean estUniqueSubventionEnCoursParSousProgramme;

    /** The est unique paiement en cours. */
    private Boolean estUniquePaiementEnCours;

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the etat dossier.
     *
     * @return the etatDossier
     */
    public EtatDossierEnum getEtatDossier() {
        return this.etatDossier;
    }

    /**
     * Sets the etat dossier.
     *
     * @param etatDossier
     *            the etatDossier to set
     */
    public void setEtatDossier(EtatDossierEnum etatDossier) {
        this.etatDossier = etatDossier;
    }

    /**
     * Gets the annee.
     *
     * @return the annee
     */
    public int getAnnee() {
        return this.annee;
    }

    /**
     * Sets the annee.
     *
     * @param annee
     *            the annee to set
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * Gets the description sous programme.
     *
     * @return the descriptionSousProgramme
     */
    public String getDescriptionSousProgramme() {
        return this.descriptionSousProgramme;
    }

    /**
     * Sets the description sous programme.
     *
     * @param descriptionSousProgramme
     *            the descriptionSousProgramme to set
     */
    public void setDescriptionSousProgramme(String descriptionSousProgramme) {
        this.descriptionSousProgramme = descriptionSousProgramme;
    }

    /**
     * Gets the engagement juridique.
     *
     * @return the engagementJuridique
     */
    public String getEngagementJuridique() {
        return this.engagementJuridique;
    }

    /**
     * Sets the engagement juridique.
     *
     * @param engagementJuridique
     *            the engagementJuridique to set
     */
    public void setEngagementJuridique(String engagementJuridique) {
        this.engagementJuridique = engagementJuridique;
    }

    /**
     * Gets the LocalDateTime echeance lancement.
     *
     * @return the dateEcheanceLancement
     */
    public LocalDateTime getDateEcheanceLancement() {
        return this.dateEcheanceLancement;
    }

    /**
     * Sets the LocalDateTime echeance lancement.
     *
     * @param dateEcheanceLancement
     *            the dateEcheanceLancement to set
     */
    public void setDateEcheanceLancement(LocalDateTime dateEcheanceLancement) {
        this.dateEcheanceLancement = dateEcheanceLancement;
    }

    /**
     * Gets the LocalDateTime echeance achevement.
     *
     * @return the dateEcheanceAchevement
     */
    public LocalDateTime getDateEcheanceAchevement() {
        return this.dateEcheanceAchevement;
    }

    /**
     * Sets the LocalDateTime echeance achevement.
     *
     * @param dateEcheanceAchevement
     *            the dateEcheanceAchevement to set
     */
    public void setDateEcheanceAchevement(LocalDateTime dateEcheanceAchevement) {
        this.dateEcheanceAchevement = dateEcheanceAchevement;
    }

    /**
     * Gets the nom long collectivite.
     *
     * @return the nomLongCollectivite
     */
    public String getNomLongCollectivite() {
        return this.nomLongCollectivite;
    }

    /**
     * Sets the nom long collectivite.
     *
     * @param nomLongCollectivite
     *            the nomLongCollectivite to set
     */
    public void setNomLongCollectivite(String nomLongCollectivite) {
        this.nomLongCollectivite = nomLongCollectivite;
    }

    /**
     * Gets the reste disponible.
     *
     * @return the resteDisponible
     */
    public BigDecimal getResteDisponible() {
        return this.resteDisponible;
    }

    /**
     * Sets the reste disponible.
     *
     * @param resteDisponible
     *            the resteDisponible to set
     */
    public void setResteDisponible(BigDecimal resteDisponible) {
        this.resteDisponible = resteDisponible;
    }

    /**
     * Gets the plafond aide.
     *
     * @return the plafondAide
     */
    public BigDecimal getPlafondAide() {
        return this.plafondAide;
    }

    /**
     * Sets the plafond aide.
     *
     * @param plafondAide
     *            the plafondAide to set
     */
    public void setPlafondAide(BigDecimal plafondAide) {
        this.plafondAide = plafondAide;
    }

    /**
     * Gets the taux consommation calcule.
     *
     * @return the tauxConsommationCalcule
     */
    public BigDecimal getTauxConsommationCalcule() {
        return this.tauxConsommationCalcule;
    }

    /**
     * Sets the taux consommation calcule.
     *
     * @param tauxConsommationCalcule
     *            the tauxConsommationCalcule to set
     */
    public void setTauxConsommationCalcule(BigDecimal tauxConsommationCalcule) {
        this.tauxConsommationCalcule = tauxConsommationCalcule;
    }

    /**
     * Gets the aide versee calcule.
     *
     * @return the aideVerseeCalcule
     */
    public BigDecimal getAideVerseeCalcule() {
        return this.aideVerseeCalcule;
    }

    /**
     * Sets the aide versee calcule.
     *
     * @param aideVerseeCalcule
     *            the aideVerseeCalcule to set
     */
    public void setAideVerseeCalcule(BigDecimal aideVerseeCalcule) {
        this.aideVerseeCalcule = aideVerseeCalcule;
    }

    /**
     * Gets the id dotation collectivite.
     *
     * @return the idDotationCollectivite
     */
    public Long getIdDotationCollectivite() {
        return this.idDotationCollectivite;
    }

    /**
     * Sets the id dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the idDotationCollectivite to set
     */
    public void setIdDotationCollectivite(Long idDotationCollectivite) {
        this.idDotationCollectivite = idDotationCollectivite;
    }

    /**
     * Gets the code departement.
     *
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * @return the montantPrevisionelTravaux
     */
    public BigDecimal getMontantPrevisionelTravaux() {
        return this.montantPrevisionelTravaux;
    }

    /**
     * @param montantPrevisionelTravaux
     *            the montantPrevisionelTravaux to set
     */
    public void setMontantPrevisionelTravaux(BigDecimal montantPrevisionelTravaux) {
        this.montantPrevisionelTravaux = montantPrevisionelTravaux;
    }

    /**
     * @return the montantTravauxVerses
     */
    public BigDecimal getMontantTravauxVerses() {
        return this.montantTravauxVerses;
    }

    /**
     * @param montantTravauxVerses
     *            the montantTravauxVerses to set
     */
    public void setMontantTravauxVerses(BigDecimal montantTravauxVerses) {
        this.montantTravauxVerses = montantTravauxVerses;
    }

    /**
     * @return the estUniqueSubventionParSousProgramme
     */
    public Boolean getEstUniqueSubventionEnCoursParSousProgramme() {
        return this.estUniqueSubventionEnCoursParSousProgramme;
    }

    /**
     * @param estUniqueSubventionParSousProgramme
     *            the estUniqueSubventionParSousProgramme to set
     */
    public void setEstUniqueSubventionEnCoursParSousProgramme(Boolean estUniqueSubventionEnCoursParSousProgramme) {
        this.estUniqueSubventionEnCoursParSousProgramme = estUniqueSubventionEnCoursParSousProgramme;
    }

    /**
     * @return the estUniquePaiementEnCours
     */
    public Boolean getEstUniquePaiementEnCours() {
        return this.estUniquePaiementEnCours;
    }

    /**
     * @param estUniquePaiementEnCours
     *            the estUniquePaiementEnCours to set
     */
    public void setEstUniquePaiementEnCours(Boolean estUniquePaiementEnCours) {
        this.estUniquePaiementEnCours = estUniquePaiementEnCours;
    }
}

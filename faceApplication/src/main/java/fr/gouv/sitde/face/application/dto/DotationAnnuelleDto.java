package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationAnnuelleDto.
 */
public class DotationAnnuelleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3713833498303464599L;

    /** The id collectivite. */
    private Long idCollectivite;

    /** The annee. */
    private Integer annee;

    /** The documents. */
    private List<DocumentDto> documents = new ArrayList<>();

    /** The dotations collectivite programme principal. */
    private List<DotationAnnuelleDotationCollectiviteDto> dotationsCollectiviteProgrammePrincipal = new ArrayList<>();

    /** The dotations collectivite programme special. */
    private List<DotationAnnuelleDotationCollectiviteDto> dotationsCollectiviteProgrammeSpecial = new ArrayList<>();

    /** The somme programme principal. */
    private BigDecimal sommeProgrammePrincipal;

    /** The somme programme special. */
    private BigDecimal sommeProgrammeSpecial;

    /**
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

    /**
     * @return the annee
     */
    public Integer getAnnee() {
        return this.annee;
    }

    /**
     * @param annee
     *            the annee to set
     */
    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    /**
     * @return the documents
     */
    public List<DocumentDto> getDocuments() {
        return this.documents;
    }

    /**
     * @param documents
     *            the documents to set
     */
    public void setDocuments(List<DocumentDto> documents) {
        this.documents = documents;
    }

    /**
     * @return the dotationsCollectiviteProgrammePrincipal
     */
    public List<DotationAnnuelleDotationCollectiviteDto> getDotationsCollectiviteProgrammePrincipal() {
        return this.dotationsCollectiviteProgrammePrincipal;
    }

    /**
     * @param dotationsCollectiviteProgrammePrincipal
     *            the dotationsCollectiviteProgrammePrincipal to set
     */
    public void setDotationsCollectiviteProgrammePrincipal(List<DotationAnnuelleDotationCollectiviteDto> dotationsCollectiviteProgrammePrincipal) {
        this.dotationsCollectiviteProgrammePrincipal = dotationsCollectiviteProgrammePrincipal;
    }

    /**
     * @return the dotationsCollectiviteProgrammeSpecial
     */
    public List<DotationAnnuelleDotationCollectiviteDto> getDotationsCollectiviteProgrammeSpecial() {
        return this.dotationsCollectiviteProgrammeSpecial;
    }

    /**
     * @param dotationsCollectiviteProgrammeSpecial
     *            the dotationsCollectiviteProgrammeSpecial to set
     */
    public void setDotationsCollectiviteProgrammeSpecial(List<DotationAnnuelleDotationCollectiviteDto> dotationsCollectiviteProgrammeSpecial) {
        this.dotationsCollectiviteProgrammeSpecial = dotationsCollectiviteProgrammeSpecial;
    }

    /**
     * @return the sommeProgrammePrincipal
     */
    public BigDecimal getSommeProgrammePrincipal() {
        return this.sommeProgrammePrincipal;
    }

    /**
     * @param sommeProgrammePrincipal
     *            the sommeProgrammePrincipal to set
     */
    public void setSommeProgrammePrincipal(BigDecimal sommeProgrammePrincipal) {
        this.sommeProgrammePrincipal = sommeProgrammePrincipal;
    }

    /**
     * @return the sommeProgrammeSpecial
     */
    public BigDecimal getSommeProgrammeSpecial() {
        return this.sommeProgrammeSpecial;
    }

    /**
     * @param sommeProgrammeSpecial
     *            the sommeProgrammeSpecial to set
     */
    public void setSommeProgrammeSpecial(BigDecimal sommeProgrammeSpecial) {
        this.sommeProgrammeSpecial = sommeProgrammeSpecial;
    }

}

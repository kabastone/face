package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DotationTransfertFongibiliteDto;
import fr.gouv.sitde.face.application.dto.lov.TransfertFongibiliteLovDto;
import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

@Mapper(uses = { ReferentielLovMapper.class }, componentModel = "spring")
public interface TranfertFongibiliteMapper {
    @Mapping(target = "idSousProgrammeDebiter", source = "dotationCollectiviteOrigine.dotationDepartement.dotationSousProgramme.sousProgramme.id")
    @Mapping(target = "idSousProgrammeCrediter",
            source = "dotationCollectiviteDestination.dotationDepartement.dotationSousProgramme.sousProgramme.id")
    @Mapping(target = "montantTransfert", source = "montant")
    DotationTransfertFongibiliteDto transfertFongibiliteToDotationTransfertFongibiliteDto(TransfertFongibilite transfertFongibilite);

    @Mapping(target = "sousProgrammeCrediter", source = "dotationCollectiviteDestination.dotationDepartement.dotationSousProgramme.sousProgramme")
    @Mapping(target = "sousProgrammeDebiter", source = "dotationCollectiviteOrigine.dotationDepartement.dotationSousProgramme.sousProgramme")
    @Mapping(target = "departement", source = "dotationCollectiviteDestination.dotationDepartement.departement")
    @Mapping(target = "collectivite", source = "dotationCollectiviteDestination.collectivite")
    TransfertFongibiliteLovDto transfertFongibiliteToTransfertFongibiliteLovDto(TransfertFongibilite transfertFongibilite);

    /**
     * @param listeResultats
     * @return
     */
    List<TransfertFongibiliteLovDto> transfertFongibilitesToTransfertFongibiliteLovDtos(List<TransfertFongibilite> listeResultats);

}

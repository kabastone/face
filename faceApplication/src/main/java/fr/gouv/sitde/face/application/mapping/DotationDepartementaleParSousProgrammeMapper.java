/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import fr.gouv.sitde.face.application.dto.DotationDepartementaleParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleParSousProgrammeBo;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * Mapper entité/DTO des dotations departementales par sous-programme.
 *
 * @author a702709
 */
@Mapper(componentModel = "spring")
public interface DotationDepartementaleParSousProgrammeMapper {

    /**
     * Dotation departementale par sous programme Bo to dotation departementale par sous programme dto.
     *
     * @param dotationDepartementaleParSousProgrammeBo
     *            the dotation departementale par sous programme
     * @return the dotation departementale par sous programme dto
     */

    @Mapping(target = "sousProgrammeAbreviation", source = "sousProgramme.abreviation")
    @Mapping(target = "sousProgrammeDescription", source = "sousProgramme.description")
    @Mapping(target = "sousProgrammeNumero", source = "sousProgramme", qualifiedByName = "numeroSousProgramme")
    DotationDepartementaleParSousProgrammeDto dotationDepartementaleParSousProgrammeBoToDotationDepartementaleParSousProgrammeDto(
            DotationDepartementaleParSousProgrammeBo dotationDepartementaleParSousProgrammeBo);

    @Named("numeroSousProgramme")
    default String donnerNumeroSousProgramme(SousProgramme sp) {
        return sp.getAbreviation().equals(ConstantesFace.CODE_SP_SECURISATION) ? ConstantesFace.NUMERO_SP_SECURISATION : sp.getNumSousAction();
    }

    /**
     * Dotation departementale par sous programme bo to dotation departementale par sous programme dto.
     *
     * @param dotationDepartementaleParSousProgrammeBo
     *            the dotation departementale par sous programme bo
     * @return the list
     */
    List<DotationDepartementaleParSousProgrammeDto> dotationDepartementaleParSousProgrammeBoToDotationDepartementaleParSousProgrammeDto(
            List<DotationDepartementaleParSousProgrammeBo> dotationDepartementaleParSousProgrammeBo);

    /**
     * Dotation departementale par sous programme dto to dotation departementale par sous programme Bo.
     *
     * @param dotationDepartementaleParSousProgrammeDto
     *            the dotation departementale par sous programme
     * @return the dotation departementale par sous programme Bo
     */
    @Mapping(target = "sousProgramme.abreviation", source = "sousProgrammeAbreviation")
    @Mapping(target = "sousProgramme.description", source = "sousProgrammeDescription")
    @Mapping(target = "sousProgramme.numSousAction", source = "sousProgrammeNumero")
    DotationDepartementaleParSousProgrammeBo dotationDepartementaleParSousProgrammeDtoToDotationDepartementaleParSousProgrammeBo(
            DotationDepartementaleParSousProgrammeDto dotationDepartementaleParSousProgrammeDto);

    /**
     * Dotation departementale par sous programme dto to dotation departementale par sous programme bo.
     *
     * @param dotationDepartementaleParSousProgrammeDto
     *            the dotation departementale par sous programme dto
     * @return the list
     */
    List<DotationDepartementaleParSousProgrammeBo> dotationDepartementaleParSousProgrammeDtoToDotationDepartementaleParSousProgrammeBo(
            List<DotationDepartementaleParSousProgrammeDto> dotationDepartementaleParSousProgrammeDto);
}

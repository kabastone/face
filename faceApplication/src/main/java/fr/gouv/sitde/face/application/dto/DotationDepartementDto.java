package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class DotationAnnuelleDotationCollectiviteDto.
 */
public class DotationDepartementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5340987177999976L;

    /** The id. */
    private Long id;

    /** The abreviation sous programme. */
    private String abreviationSousProgramme;

    /** The description sous programme. */
    private String descriptionSousProgramme;

    /** The description sous programme. */
    private String numeroSousProgramme;

    /** The montant notifie. */
    private BigDecimal montantNotifie;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the abreviation sous programme.
     *
     * @return the abreviationSousProgramme
     */
    public String getAbreviationSousProgramme() {
        return this.abreviationSousProgramme;
    }

    /**
     * Sets the abreviation sous programme.
     *
     * @param abreviationSousProgramme
     *            the abreviationSousProgramme to set
     */
    public void setAbreviationSousProgramme(String abreviationSousProgramme) {
        this.abreviationSousProgramme = abreviationSousProgramme;
    }

    /**
     * Gets the description sous programme.
     *
     * @return the descriptionSousProgramme
     */
    public String getDescriptionSousProgramme() {
        return this.descriptionSousProgramme;
    }

    /**
     * Sets the description sous programme.
     *
     * @param descriptionSousProgramme
     *            the descriptionSousProgramme to set
     */
    public void setDescriptionSousProgramme(String descriptionSousProgramme) {
        this.descriptionSousProgramme = descriptionSousProgramme;
    }

    /**
     * Gets the numero sous programme.
     *
     * @return the numeroSousProgramme
     */
    public String getNumeroSousProgramme() {
        return this.numeroSousProgramme;
    }

    /**
     * Sets the numero sous programme.
     *
     * @param numeroSousProgramme
     *            the numeroSousProgramme to set
     */
    public void setNumeroSousProgramme(String numeroSousProgramme) {
        this.numeroSousProgramme = numeroSousProgramme;
    }

    /**
     * @return the montantNotifie
     */
    public BigDecimal getMontantNotifie() {
        return this.montantNotifie;
    }

    /**
     * @param montantNotifie
     *            the montantNotifie to set
     */
    public void setMontantNotifie(BigDecimal montantNotifie) {
        this.montantNotifie = montantNotifie;
    }

}

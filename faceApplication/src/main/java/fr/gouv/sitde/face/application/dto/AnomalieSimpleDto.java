package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class AnomalieSimpleDto.
 */
public class AnomalieSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3202653061398667654L;

    /** The id. */
    private Long id;

    /** The type anomalie. */
    private String libelleTypeAnomalie;

    /** The corrigee. */
    private boolean corrigee;

    /** The problematique. */
    private String problematique;

    /** The reponse. */
    private String reponse;

    /** The date. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateCreation;

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the libelleTypeAnomalie
     */
    public String getLibelleTypeAnomalie() {
        return this.libelleTypeAnomalie;
    }

    /**
     * @param libelleTypeAnomalie
     *            the libelleTypeAnomalie to set
     */
    public void setLibelleTypeAnomalie(String libelleTypeAnomalie) {
        this.libelleTypeAnomalie = libelleTypeAnomalie;
    }

    /**
     * @return the corrigee
     */
    public boolean isCorrigee() {
        return this.corrigee;
    }

    /**
     * @param corrigee
     *            the corrigee to set
     */
    public void setCorrigee(boolean corrigee) {
        this.corrigee = corrigee;
    }

    /**
     * @return the problematique
     */
    public String getProblematique() {
        return this.problematique;
    }

    /**
     * @param problematique
     *            the problematique to set
     */
    public void setProblematique(String problematique) {
        this.problematique = problematique;
    }

    /**
     * @return the reponse
     */
    public String getReponse() {
        return this.reponse;
    }

    /**
     * @param reponse
     *            the reponse to set
     */
    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    /**
     * @return the dateCreation
     */
    public LocalDateTime getDateCreation() {
        return this.dateCreation;
    }

    /**
     * @param dateCreation
     *            the dateCreation to set
     */
    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }
}

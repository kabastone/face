/*
 *
 */
package fr.gouv.sitde.face.application.prolongation.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.DemandeProlongationDto;
import fr.gouv.sitde.face.application.mapping.DemandeProlongationMapper;
import fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class GestionDemandeProlongationApplicationImpl.
 */
@Service
@Transactional
public class GestionDemandeProlongationApplicationImpl implements GestionDemandeProlongationApplication {
    /** The demande subvention service. */
    @Inject
    private DemandeProlongationService demandeProlongationService;

    /** The demande Prolongation simple mapper. */
    @Inject
    private DemandeProlongationMapper demandeProlongationMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication#rechercherDemandeProlongationParId(java.lang.Long)
     */
    @Override
    public DemandeProlongationDto rechercherDemandeProlongationParId(Long idDemandeProlongation) {
        DemandeProlongation demandeProlongation = this.demandeProlongationService.rechercherDemandeProlongation(idDemandeProlongation);
        return this.demandeProlongationMapper.demandeProlongationToDemandeProlongationDto(demandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication#initNouvelleDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongationDto initNouvelleDemandeProlongation(Long idDossier) {
        DemandeProlongationDto demande = this.demandeProlongationMapper
                .demandeProlongationToDemandeProlongationDto(this.demandeProlongationService.initNouvelleDemandeProlongation(idDossier));
        demande.setEtat(ConstantesFace.CODE_DEM_PROLONGATION_INITIAL);
        demande.setEtatLibelle(ConstantesFace.CODE_DEM_PROLONGATION_INITIAL);
        return demande;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication#creerDemandeProlongation(fr.gouv.sitde.face.application.dto.
     * DemandeProlongationDto)
     */
    @Override
    public DemandeProlongationDto creerDemandeProlongation(DemandeProlongationDto demandeProlongationDto) {
        DemandeProlongation demande = this.demandeProlongationMapper.demandeProlongationDtoToDemandeProlongation(demandeProlongationDto);
        demande = this.demandeProlongationService.creerDemandeProlongation(demande, demandeProlongationDto.getListeFichiersEnvoyer());

        return this.demandeProlongationMapper.demandeProlongationToDemandeProlongationDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication#accorderDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongationDto accorderDemandeProlongation(Long idDemandeProlongation) {
        DemandeProlongation demande = this.demandeProlongationService.accorderDemandeProlongation(idDemandeProlongation);

        return this.demandeProlongationMapper.demandeProlongationToDemandeProlongationDto(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication#refuserDemandeProlongation(fr.gouv.sitde.face.application.dto
     * .DemandeProlongationDto)
     */
    @Override
    public DemandeProlongationDto refuserDemandeProlongation(DemandeProlongationDto demandeDto) {
        DemandeProlongation demande = this.demandeProlongationMapper.demandeProlongationDtoToDemandeProlongation(demandeDto);
        demande = this.demandeProlongationService.refuserDemandeProlongation(demande);

        return this.demandeProlongationMapper.demandeProlongationToDemandeProlongationDto(demande);
    }

}

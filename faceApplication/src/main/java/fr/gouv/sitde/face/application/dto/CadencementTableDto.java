package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;

/**
 * The Class CadencementTableDto.
 */
public class CadencementTableDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8288749073462681616L;

    /** The sous programme. */
    private SousProgrammeLovDto sousProgramme;

    /** The annee programmation. */
    private Integer anneeProgrammation;

    /** The has prolongation. */
    private Boolean hasProlongation;

    /** The plafond aide subvention. */
    private BigDecimal plafondAideSubvention;

    /** The num dossier. */
    private String numDossier;

    /** The id dossier. */
    private Long idDossier;

    /** The cadencement. */
    private CadencementDto cadencement;

    /** The soldee. */
    private Boolean soldee;

    /**
     * @return the sousProgramme
     */
    public SousProgrammeLovDto getSousProgramme() {
        return this.sousProgramme;
    }

    /**
     * @param sousProgramme
     *            the sousProgramme to set
     */
    public void setSousProgramme(SousProgrammeLovDto sousProgramme) {
        this.sousProgramme = sousProgramme;
    }

    /**
     * @return the anneeProgrammation
     */
    public Integer getAnneeProgrammation() {
        return this.anneeProgrammation;
    }

    /**
     * @param anneeProgrammation
     *            the anneeProgrammation to set
     */
    public void setAnneeProgrammation(Integer anneeProgrammation) {
        this.anneeProgrammation = anneeProgrammation;
    }

    /**
     * @return the plafondAideSubvention
     */
    public BigDecimal getPlafondAideSubvention() {
        return this.plafondAideSubvention;
    }

    /**
     * @param plafondAideSubvention
     *            the plafondAideSubvention to set
     */
    public void setPlafondAideSubvention(BigDecimal plafondAideSubvention) {
        this.plafondAideSubvention = plafondAideSubvention;
    }

    /**
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * @return the idDossier
     */
    public Long getIdDossier() {
        return this.idDossier;
    }

    /**
     * @param idDossier
     *            the idDossier to set
     */
    public void setIdDossier(Long idDossier) {
        this.idDossier = idDossier;
    }

    /**
     * @return the cadencement
     */
    public CadencementDto getCadencement() {
        return this.cadencement;
    }

    /**
     * @param cadencement
     *            the cadencement to set
     */
    public void setCadencement(CadencementDto cadencement) {
        this.cadencement = cadencement;
    }

    /**
     * @return the hasProlongation
     */
    public Boolean getHasProlongation() {
        return this.hasProlongation;
    }

    /**
     * @param hasProlongation
     *            the hasProlongation to set
     */
    public void setHasProlongation(Boolean hasProlongation) {
        this.hasProlongation = hasProlongation;
    }

    /**
     * @return the soldee
     */
    public Boolean getSoldee() {
        return soldee;
    }

    /**
     * @param soldee the soldee to set
     */
    public void setSoldee(Boolean soldee) {
        this.soldee = soldee;
    }

}

package fr.gouv.sitde.face.application.commun.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.commun.GestionAnomalieApplication;
import fr.gouv.sitde.face.application.dto.AnomalieDto;
import fr.gouv.sitde.face.application.mapping.AnomalieMapper;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Class GestionAnomalieApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionAnomalieApplicationImpl implements GestionAnomalieApplication {

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /** The anomalie mapper. */
    @Inject
    private AnomalieMapper anomalieMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#ajouterAnomalie(fr.gouv.sitde.face.application.dto.AnomalieDto)
     */
    @Override
    public AnomalieDto ajouterAnomalie(AnomalieDto anomalieDto) {
        Anomalie anomalie = this.anomalieMapper.anomalieDtoToAnomalie(anomalieDto);
        Anomalie anomalieBdd = this.anomalieService.creerEtValiderAnomalie(anomalie);
        return this.anomalieMapper.anomalieToAnomalieDto(anomalieBdd);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#updateAnomalie(fr.gouv.sitde.face.application.dto.AnomalieDto)
     */
    @Override
    public AnomalieDto updateAnomalie(AnomalieDto anomalieDto) {
        Anomalie anomalie = this.anomalieMapper.anomalieDtoToAnomalie(anomalieDto);
        Anomalie anomalieBdd = this.anomalieService.modifierAnomalie(anomalie);
        return this.anomalieMapper.anomalieToAnomalieDto(anomalieBdd);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#majCorrectionAnomaliePourPaiement(java.lang.Long, java.lang.Long,
     * boolean)
     */
    @Override
    public AnomalieDto majCorrectionAnomaliePourPaiement(Long idDemandePaiement, Long idAnomalie, boolean corrigee) {
        Anomalie anomalie = this.anomalieService.majCorrectionAnomaliePourPaiement(idDemandePaiement, idAnomalie, corrigee);
        return this.anomalieMapper.anomalieToAnomalieDto(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#majCorrectionAnomaliePourSubvention(java.lang.Long, java.lang.Long,
     * boolean)
     */
    @Override
    public AnomalieDto majCorrectionAnomaliePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee) {
        Anomalie anomalie = this.anomalieService.majCorrectionAnomaliePourSubvention(idDemandeSubvention, idAnomalie, corrigee);
        return this.anomalieMapper.anomalieToAnomalieDto(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#rechercherAnomaliesParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public List<AnomalieDto> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention) {
        List<Anomalie> anomalies = this.anomalieService.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention);
        return this.anomalieMapper.anomaliesToAnomaliesDto(anomalies);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#rechercherAnomaliesParIdDemandePaiement(java.lang.Long)
     */
    @Override
    public List<AnomalieDto> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement) {
        List<Anomalie> anomalies = this.anomalieService.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement);
        return this.anomalieMapper.anomaliesToAnomaliesDto(anomalies);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#rechercherAnomaliesPourAODEParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public List<AnomalieDto> rechercherAnomaliesParIdDemandeSubventionPourAODE(Long idDemandeSubvention) {
        List<Anomalie> anomalies = this.anomalieService.rechercherAnomaliesParIdDemandeSubventionPourAODE(idDemandeSubvention);
        return this.anomalieMapper.anomaliesToAnomaliesDto(anomalies);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.application.commun.GestionAnomalieApplication#rechercherAnomaliesParIdDemandePaiementPourAODE(java.lang.Long)
     */
    @Override
    public List<AnomalieDto> rechercherAnomaliesParIdDemandePaiementPourAODE(Long idDemandePaiement) {
        List<Anomalie> anomalies = this.anomalieService.rechercherAnomaliesParIdDemandePaiementPourAODE(idDemandePaiement);
        return this.anomalieMapper.anomaliesToAnomaliesDto(anomalies);
    }
}

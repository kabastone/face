/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * Mapper entité/DTO des departements.
 *
 * @author a687370
 *
 */
@Mapper(componentModel = "spring")
public interface DepartementMapper {

    /**
     * Departement to departement dto.
     *
     *
     * @param departement
     *            the departement
     * @return the departement dto
     */
    @Mapping(target = "nomCodique", source = "tpgNomCodique")
    @Mapping(target = "codique", source = "tpgNumCodique")
    DepartementDto departementToDepartementDto(Departement departement);

    /**
     * @param departementDto
     * @return
     */
    @Mapping(target = "tpgNomCodique", source = "nomCodique")
    @Mapping(target = "tpgNumCodique", source = "codique")
    Departement departementDtoToDepartement(DepartementDto departementDto);

}

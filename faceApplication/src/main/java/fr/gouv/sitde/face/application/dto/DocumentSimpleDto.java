package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * The Class DocumentSimpleDto.
 */
public class DocumentSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5428274295329474311L;

    /** The id. */
    private Long id;

    /** The type document. */
    private String libelleTypeDocument;

    /** The nom fichier sans extension. */
    private String nomFichierSansExtension;

    /** The extension fichier. */
    private String extensionFichier;

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the nomFichierSansExtension
     */
    public String getNomFichierSansExtension() {
        return this.nomFichierSansExtension;
    }

    /**
     * @param nomFichierSansExtension
     *            the nomFichierSansExtension to set
     */
    public void setNomFichierSansExtension(String nomFichierSansExtension) {
        this.nomFichierSansExtension = nomFichierSansExtension;
    }

    /**
     * @return the extensionFichier
     */
    public String getExtensionFichier() {
        return this.extensionFichier;
    }

    /**
     * @param extensionFichier
     *            the extensionFichier to set
     */
    public void setExtensionFichier(String extensionFichier) {
        this.extensionFichier = extensionFichier;
    }

    /**
     * @return the libelleTypeDocument
     */
    public String getLibelleTypeDocument() {
        return this.libelleTypeDocument;
    }

    /**
     * @param libelleTypeDocument
     *            the libelleTypeDocument to set
     */
    public void setLibelleTypeDocument(String libelleTypeDocument) {
        this.libelleTypeDocument = libelleTypeDocument;
    }
}

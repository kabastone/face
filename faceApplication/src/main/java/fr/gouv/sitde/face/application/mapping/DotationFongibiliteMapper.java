/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import fr.gouv.sitde.face.application.dto.DotationCollectiviteFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationLienFongibiliteDto;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * @author a768251
 *
 */
@Mapper(componentModel = "spring")
public interface DotationFongibiliteMapper {

    @Mapping(target = "description", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "idSousProgramme", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id")
    @Mapping(target = "dotationDisponible", source = "dotationCollectivite", qualifiedByName = "dotationDisponible")
    @Mapping(target = "idCollectivite", source = "dotationCollectivite.collectivite.id")
    DotationCollectiviteFongibiliteDto dotationCollectiviteToDotationCollectiviteFongibiliteDto(DotationCollectivite dotationCollectivite);

    List<DotationCollectiviteFongibiliteDto> dotationCollectivitesToDotationCollectiviteFongibiliteDtos(
            List<DotationCollectivite> dotationCollectivite);

    @Named("dotationDisponible")
    default BigDecimal calculerDotationDisponible(DotationCollectivite dotationCollectivite) {

        if (dotationCollectivite == null) {
            return null;
        }

        return dotationCollectivite.getMontant().subtract(dotationCollectivite.getDotationRepartie());
    }

    default Set<DotationLienFongibiliteDto> calculerLiensFongibiliteSousProgrammes(List<SousProgramme> sousProgrammes) {

        Set<DotationLienFongibiliteDto> setDtos = new HashSet<>();
        for (SousProgramme sspr : sousProgrammes) {
            for (SousProgramme ssprDonneur : sspr.getSousProgrammeDonneur()) {
                DotationLienFongibiliteDto dto = new DotationLienFongibiliteDto();
                dto.setIdDonneur(ssprDonneur.getId());
                dto.setIdReceveur(sspr.getId());
                setDtos.add(dto);
            }
            for (SousProgramme ssprReceveur : sspr.getSousProgrammeReceveur()) {
                DotationLienFongibiliteDto dto = new DotationLienFongibiliteDto();
                dto.setIdDonneur(sspr.getId());
                dto.setIdReceveur(ssprReceveur.getId());
                setDtos.add(dto);
            }
        }
        return setDtos;
    }

}

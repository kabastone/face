/**
 *
 */
package fr.gouv.sitde.face.application.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.application.mapping.UtilisateurMapper;
import fr.gouv.sitde.face.domaine.service.administration.UtilisateurService;
import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Class GestionUtilisateurApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class GestionUtilisateurApplicationImpl implements GestionUtilisateurApplication {

    /** The utilisateur service. */
    @Inject
    private UtilisateurService utilisateurService;

    /** The utilisateur mapper. */
    @Inject
    private UtilisateurMapper utilisateurMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#rechercherUtilisateurParId(java.lang.Long)
     */
    @Override
    public UtilisateurAdminDto rechercherUtilisateurParId(Long idUtilisateur) {
        Utilisateur utilisateur = this.utilisateurService.rechercherUtilisateurParId(idUtilisateur);
        UtilisateurAdminDto utilisateurDto = this.utilisateurMapper.utilisateurToUtilisateurAdminDto(utilisateur);

        if (utilisateurDto != null) {
            // Tri des DroitAgentCollectiviteDto de l'utilisateur par ordre alphabétique
            utilisateurDto.getDroitsAgentCollectivites().sort((DroitAgentCollectiviteDto droitAgentCol1,
                    DroitAgentCollectiviteDto droitAgentCol2) -> droitAgentCol1.getNomCourt().compareTo(droitAgentCol2.getNomCourt()));
        }
        return utilisateurDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#rechercherUtilisateurParEmail(java.lang.String)
     */
    @Override
    public UtilisateurAdminDto rechercherUtilisateurParEmail(String email) {
        Utilisateur utilisateur = this.utilisateurService.rechercherUtilisateurParEmail(email);
        UtilisateurAdminDto utilisateurDto = this.utilisateurMapper.utilisateurToUtilisateurAdminDto(utilisateur);

        if (utilisateurDto != null) {
            // Tri des DroitAgentCollectiviteDto de l'utilisateur par ordre alphabétique
            utilisateurDto.getDroitsAgentCollectivites().sort((DroitAgentCollectiviteDto droitAgentCol1,
                    DroitAgentCollectiviteDto droitAgentCol2) -> droitAgentCol1.getNomCourt().compareTo(droitAgentCol2.getNomCourt()));
        }
        return utilisateurDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#synchroniserUtilisateurConnecte(fr.gouv.sitde.face.application.
     * administration.dto.UtilisateurAdminDto)
     */
    @Override
    public UtilisateurAdminDto synchroniserUtilisateurConnecte(UtilisateurAdminDto utilisateurDto) {
        Utilisateur utilisateur = this.utilisateurMapper.utilisateurAdminDtoToUtilisateur(utilisateurDto);
        utilisateur = this.utilisateurService.synchroniserUtilisateurConnecte(utilisateur);
        return this.utilisateurMapper.utilisateurToUtilisateurAdminDto(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#creerUtilisateur(fr.gouv.sitde.face.application.administration.dto.
     * UtilisateurDto)
     */
    @Override
    public UtilisateurAdminDto creerUtilisateurEnAttenteConnexion(UtilisateurAdminDto utilisateurDto) {

        Utilisateur utilisateur = this.utilisateurMapper.utilisateurAdminDtoToUtilisateur(utilisateurDto);
        utilisateur = this.utilisateurService.creerUtilisateurEnAttenteConnexion(utilisateur);
        return this.utilisateurMapper.utilisateurToUtilisateurAdminDto(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#modifierUtilisateur(fr.gouv.sitde.face.application.dto.
     * UtilisateurAdminDto)
     */
    @Override
    public UtilisateurAdminDto modifierUtilisateur(UtilisateurAdminDto utilisateurDto) {
        Utilisateur utilisateur = this.utilisateurMapper.utilisateurAdminDtoToUtilisateur(utilisateurDto);
        utilisateur = this.utilisateurService.modifierUtilisateur(utilisateur);
        return this.utilisateurMapper.utilisateurToUtilisateurAdminDto(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#supprimerUtilisateur(java.lang.Long)
     */
    @Override
    public void supprimerUtilisateur(Long idUtilisateur) {
        this.utilisateurService.supprimerUtilisateur(idUtilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication#rechercherTousUtilisateurs(fr.gouv.sitde.face.transverse.pagination
     * .PageDemande)
     */
    @Override
    public PageReponse<UtilisateurAdminDto> rechercherTousUtilisateurs(PageDemande pageDemande) {
        PageReponse<Utilisateur> pageUtilisateurs = this.utilisateurService.rechercherTousUtilisateurs(pageDemande);

        // Renvoi page reponse UtilisateurDto
        List<UtilisateurAdminDto> listeUtilisateursDto = this.utilisateurMapper
                .utilisateursToUtilisateurAdminDtos(pageUtilisateurs.getListeResultats());
        return new PageReponse<>(listeUtilisateursDto, pageUtilisateurs.getNbTotalResultats());
    }

}

package fr.gouv.sitde.face.application.document;

import java.io.File;

/**
 * The Interface GestionDocumentApplication.
 */
public interface GestionDocumentApplication {

    /**
     * Téléchargement d'un document.
     *
     * @param idDocument
     *            the id document
     * @return the file
     */
    File telechargerDocument(Long idDocument);

    /**
     * Supprimer document.
     *
     * @param idDocument
     *            the id document
     */
    void supprimerDocument(Long idDocument);

}

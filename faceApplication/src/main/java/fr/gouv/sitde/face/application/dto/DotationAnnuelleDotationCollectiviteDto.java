package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class DotationAnnuelleDotationCollectiviteDto.
 */
public class DotationAnnuelleDotationCollectiviteDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -9047015479731165815L;

    /** The id dotation collectivite. */
    private Long idDotationCollectivite;

    /** The num sous action. */
    private String numSousAction;

    /** The abreviation. */
    private String abreviation;

    /** The description. */
    private String description;

    /** The montant. */
    private BigDecimal montant;

    /**
     * @return the idDotationCollectivite
     */
    public Long getIdDotationCollectivite() {
        return this.idDotationCollectivite;
    }

    /**
     * @param idDotationCollectivite
     *            the idDotationCollectivite to set
     */
    public void setIdDotationCollectivite(Long idDotationCollectivite) {
        this.idDotationCollectivite = idDotationCollectivite;
    }

    /**
     * @return the numSousAction
     */
    public String getNumSousAction() {
        return this.numSousAction;
    }

    /**
     * @param numSousAction
     *            the numSousAction to set
     */
    public void setNumSousAction(String numSousAction) {
        this.numSousAction = numSousAction;
    }

    /**
     * @return the abreviation
     */
    public String getAbreviation() {
        return this.abreviation;
    }

    /**
     * @param abreviation
     *            the abreviation to set
     */
    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }
}

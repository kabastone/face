/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import fr.gouv.sitde.face.application.dto.DossierSubventionDetailDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionTableMferDto;
import fr.gouv.sitde.face.application.dto.ResultatRechercheDossierDto;
import fr.gouv.sitde.face.application.dto.lov.DossierSubventionLovDto;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * Mapper entité/DTO des dossiers de subvention (détails).
 *
 * @author Atos
 *
 */
@Mapper(uses = { AdresseMapper.class, ReferentielLovMapper.class }, componentModel = "spring")
public interface DossierSubventionMapper {

    /**
     * Dossier subvention to dossier subvention dto.
     *
     * @return the dossier subvention dto
     */
    @Mapping(target = "annee", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme.annee")
    @Mapping(target = "descriptionSousProgramme", source = "dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "engagementJuridique", source = "chorusNumEj")
    @Mapping(target = "nomLongCollectivite", source = "dotationCollectivite.collectivite.nomLong")
    @Mapping(target = "codeDepartement", source = "dotationCollectivite.dotationDepartement.departement.code")
    @Mapping(target = "resteDisponible", source = "resteConsommerCalcule")
    @Mapping(target = "idDotationCollectivite", source = "dotationCollectivite.id")
    @Mapping(target = "montantPrevisionelTravaux", source = "demandesSubvention", qualifiedByName = "sommeMontantTravaux")
    @Mapping(target = "montantTravauxVerses", source = "demandesPaiement", qualifiedByName = "sommeMontantTravauxPaiements")
    DossierSubventionDetailDto dossierSubventionToDossierSubventionDetailDto(DossierSubvention dossierSubvention);

    @Named("sommeMontantTravaux")
    default BigDecimal sommeMontantTravaux(Set<DemandeSubvention> demandes) {
        return demandes.stream().map(DemandeSubvention::getMontantTravauxEligible).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Named("sommeMontantTravauxPaiements")
    default BigDecimal sommeMontantPaiements(Set<DemandePaiement> demandes) {
        return demandes.stream().filter((DemandePaiement dem) -> !TypeDemandePaiementEnum.AVANCE.equals(dem.getTypeDemandePaiement()))
                .map(DemandePaiement::getMontantTravauxRealises).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Dossier subvention to dossier subvention lov dto.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention lov dto
     */
    @Mapping(target = "id", source = "dossierSubvention.id")
    @Mapping(target = "numDossier", source = "dossierSubvention.numDossier")
    DossierSubventionLovDto dossierSubventionToDossierSubventionLovDto(DossierSubvention dossierSubvention);

    /**
     * Dossier subventions to dossier subvention lov dtos.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the list
     */
    List<DossierSubventionLovDto> dossierSubventionsToDossierSubventionLovDtos(List<DossierSubvention> dossierSubvention);

    /**
     * Dossier subvention to resultat recherche dossier dto.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the resultat recherche dossier dto
     */
    @Mapping(target = "nomCourtCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.nomCourt")
    @Mapping(target = "idDossier", source = "dossierSubvention.id")
    @Mapping(target = "plafondAide", source = "dossierSubvention.plafondAide")
    @Mapping(target = "numEJ", source = "dossierSubvention.chorusNumEj")
    ResultatRechercheDossierDto dossierSubventionToResultatRechercheDossierDto(DossierSubvention dossierSubvention);

    /**
     * Dossiers subvention to resultat recherche dossier dtos.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the list
     */

    List<ResultatRechercheDossierDto> dossiersSubventionToResultatRechercheDossierDtos(List<DossierSubvention> dossierSubvention);

    /**
     * Dossier subvention to dossier subvention defaut dto.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention defaut dto
     */
    @Mapping(target = "idDossier", source = "id")
    @Mapping(target = "departementCode", source = "dotationCollectivite.dotationDepartement.departement.code")
    @Mapping(target = "departementNom", source = "dotationCollectivite.dotationDepartement.departement.nom")
    @Mapping(target = "numDossier", source = "numDossier")
    @Mapping(target = "dossierNumEJ", source = "chorusNumEj")
    @Mapping(target = "collectiviteNomCourt", source = "dotationCollectivite.collectivite.nomCourt")
    DossierSubventionTableMferDto dossierSubventionToDossierSubventionDefautDto(DossierSubvention dossierSubvention);

    List<DossierSubventionTableMferDto> dossierSubventionsToDossierSubventionDefautDtos(List<DossierSubvention> dossierSubvention);
}

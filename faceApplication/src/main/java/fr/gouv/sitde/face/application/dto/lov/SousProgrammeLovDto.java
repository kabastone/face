package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class SousProgrammeLovDto.
 */
public class SousProgrammeLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1475748522228933720L;

    /** The id. */
    private Integer id;

    /** The abreviation. */
    private String abreviation;

    /** The code domaine fonctionnel. */
    private String codeDomaineFonctionnel;

    /** The description. */
    private String description;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the abreviation.
     *
     * @return the abreviation
     */
    public String getAbreviation() {
        return this.abreviation;
    }

    /**
     * Sets the abreviation.
     *
     * @param abreviation the abreviation to set
     */
    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    /**
     * Gets the code domaine fonctionnel.
     *
     * @return the codeDomaineFonctionnel
     */
    public String getCodeDomaineFonctionnel() {
        return this.codeDomaineFonctionnel;
    }

    /**
     * Sets the code domaine fonctionnel.
     *
     * @param codeDomaineFonctionnel the codeDomaineFonctionnel to set
     */
    public void setCodeDomaineFonctionnel(String codeDomaineFonctionnel) {
        this.codeDomaineFonctionnel = codeDomaineFonctionnel;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;

public class CollectiviteSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7744395061062866642L;

    /** The id. */
    private Long id;

    /** The siret. */
    private String siret;

    /** The nom court. */
    private String nomCourt;

    /** The nom long. */
    private String nomLong;

    /** The etat collectivite. */
    private EtatCollectiviteEnum etatCollectivite;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the siret.
     *
     * @return the siret
     */
    public String getSiret() {
        return this.siret;
    }

    /**
     * Sets the siret.
     *
     * @param siret
     *            the new siret
     */
    public void setSiret(String siret) {
        this.siret = siret;
    }

    /**
     * Gets the nom court.
     *
     * @return the nom court
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * Sets the nom court.
     *
     * @param nomCourt
     *            the new nom court
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * Gets the nom long.
     *
     * @return the nom long
     */
    public String getNomLong() {
        return this.nomLong;
    }

    /**
     * Sets the nom long.
     *
     * @param nomLong
     *            the new nom long
     */
    public void setNomLong(String nomLong) {
        this.nomLong = nomLong;
    }

    /**
     * Gets the etat collectivite.
     *
     * @return the etat collectivite
     */
    public EtatCollectiviteEnum getEtatCollectivite() {
        return this.etatCollectivite;
    }

    /**
     * Sets the etat collectivite.
     *
     * @param etatCollectivite
     *            the new etat collectivite
     */
    public void setEtatCollectivite(EtatCollectiviteEnum etatCollectivite) {
        this.etatCollectivite = etatCollectivite;
    }

}

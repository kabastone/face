/**
 *
 */
package fr.gouv.sitde.face.application.subvention.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.ImportLigneDotationDto;
import fr.gouv.sitde.face.application.mapping.ImportLigneDotationMapper;
import fr.gouv.sitde.face.application.subvention.ImportDotationApplication;
import fr.gouv.sitde.face.domaine.service.subvention.ImportDotationService;
import fr.gouv.sitde.face.transverse.imports.ImportLigneDotation;

/**
 * The Class ImportDotationApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class ImportDotationApplicationImpl implements ImportDotationApplication {

    /** Import Dossier Dotation Service. */
    @Inject
    private ImportDotationService importDotationService;

    /** Mapper DTO / Entity. */
    @Inject
    private ImportLigneDotationMapper importLigneDossierDotationMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.ImportDotationApplication#ImporterDossierDodation(java.util.List, java.lang.Boolean)
     */
    @Override
    public Boolean importerDossierDotation(List<ImportLigneDotationDto> listeImportLigneDossierDotationDto) {

        List<ImportLigneDotation> listeImportLigneDossierDotation = null;

        listeImportLigneDossierDotation = this.importLigneDossierDotationMapper
                .importListeLigneDotationToImportListeLigneDotation(listeImportLigneDossierDotationDto);

        this.importDotationService.importerDotation(listeImportLigneDossierDotation);

        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.ImportDotationApplication#VerifierDotationAnnuelleNotifiee()
     */
    @Override
    public Boolean verifierDotationAnnuelleNotifiee() {
        return this.importDotationService.verifierDotationAnnuelleNotifiee();
    }

}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class AnomalieDto.
 */
public class AnomalieDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5358103677642044489L;

    /** The id. */
    private Long id;

    /** The id demande paiement. */
    private Long idDemandePaiement;

    /** The id demande subvention. */
    private Long idDemandeSubvention;

    /** The id type anomalie. */
    private String codeTypeAnomalie;

    /** The type anomalie. */
    private String libelleTypeAnomalie;

    /** The corrigee. */
    private boolean corrigee;

    /** Indique si l'anomalie est visible pour l'AODE. */
    private boolean visiblePourAODE;

    /** The problematique. */
    private String problematique;

    /** The reponse. */
    private String reponse;

    /** The date. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateCreation;

    /** The version. */
    private int version;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the libelle type anomalie.
     *
     * @return the libelleTypeAnomalie
     */
    public String getLibelleTypeAnomalie() {
        return this.libelleTypeAnomalie;
    }

    /**
     * Sets the libelle type anomalie.
     *
     * @param libelleTypeAnomalie
     *            the libelleTypeAnomalie to set
     */
    public void setLibelleTypeAnomalie(String libelleTypeAnomalie) {
        this.libelleTypeAnomalie = libelleTypeAnomalie;
    }

    /**
     * Checks if is corrigee.
     *
     * @return the corrigee
     */
    public boolean isCorrigee() {
        return this.corrigee;
    }

    /**
     * Sets the corrigee.
     *
     * @param corrigee
     *            the corrigee to set
     */
    public void setCorrigee(boolean corrigee) {
        this.corrigee = corrigee;
    }

    /**
     * Gets the problematique.
     *
     * @return the problematique
     */
    public String getProblematique() {
        return this.problematique;
    }

    /**
     * Sets the problematique.
     *
     * @param problematique
     *            the problematique to set
     */
    public void setProblematique(String problematique) {
        this.problematique = problematique;
    }

    /**
     * Gets the reponse.
     *
     * @return the reponse
     */
    public String getReponse() {
        return this.reponse;
    }

    /**
     * Sets the reponse.
     *
     * @param reponse
     *            the reponse to set
     */
    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    /**
     * Gets the date creation.
     *
     * @return the dateCreation
     */
    public LocalDateTime getDateCreation() {
        return this.dateCreation;
    }

    /**
     * Sets the date creation.
     *
     * @param dateCreation
     *            the dateCreation to set
     */
    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the id demande paiement.
     *
     * @return the idDemandePaiement
     */
    public Long getIdDemandePaiement() {
        return this.idDemandePaiement;
    }

    /**
     * Sets the id demande paiement.
     *
     * @param idDemandePaiement
     *            the idDemandePaiement to set
     */
    public void setIdDemandePaiement(Long idDemandePaiement) {
        this.idDemandePaiement = idDemandePaiement;
    }

    /**
     * Gets the id demande subvention.
     *
     * @return the idDemandeSubvention
     */
    public Long getIdDemandeSubvention() {
        return this.idDemandeSubvention;
    }

    /**
     * Sets the id demande subvention.
     *
     * @param idDemandeSubvention
     *            the idDemandeSubvention to set
     */
    public void setIdDemandeSubvention(Long idDemandeSubvention) {
        this.idDemandeSubvention = idDemandeSubvention;
    }

    /**
     * Gets the code type anomalie.
     *
     * @return the codeTypeAnomalie
     */
    public String getCodeTypeAnomalie() {
        return this.codeTypeAnomalie;
    }

    /**
     * Sets the code type anomalie.
     *
     * @param codeTypeAnomalie
     *            the codeTypeAnomalie to set
     */
    public void setCodeTypeAnomalie(String codeTypeAnomalie) {
        this.codeTypeAnomalie = codeTypeAnomalie;
    }

    /**
     * @return the visiblePourAODE
     */
    public boolean isVisiblePourAODE() {
        return this.visiblePourAODE;
    }

    /**
     * @param visiblePourAODE
     *            the visiblePourAODE to set
     */
    public void setVisiblePourAODE(boolean visiblePourAODE) {
        this.visiblePourAODE = visiblePourAODE;
    }

}

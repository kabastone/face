package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * The Class EtatDemandeSubventionDto.
 *
 * @author a608193
 */
public class EtatDemandeSubventionDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 259004305406355503L;

    /** The id. */
    private int id;

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.application.subvention;

import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * @author A754839
 *
 */
public interface GenerationDemandeSubventionApplication {

    /**
     * Génération d'un pdf de demande de subvention par id.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     */
    FichierTransfert genererDemandeSubventionPdf(Long idDemandeSubvention);
}

/**
 *
 */
package fr.gouv.sitde.face.application.administration;

import java.util.List;

import fr.gouv.sitde.face.application.dto.CollectiviteDetailDto;
import fr.gouv.sitde.face.application.dto.CollectiviteSimpleDto;

/**
 * The Interface GestionCollectiviteApplication.
 *
 * @author Atos
 */
public interface GestionCollectiviteApplication {

    /**
     * Liste toutes les collectivitées existantes.
     *
     * @return the list
     */
    List<CollectiviteSimpleDto> rechercherCollectivitesSimple();

    /**
     * Rechercher collectivite detail.
     *
     * @param id
     *            the id
     * @return the collectivite details dto
     */
    CollectiviteDetailDto rechercherCollectiviteDetail(Long id);

    /**
     * Modifier collectivite.
     *
     * @param collectiviteDto
     *            the collectivite dto
     * @return the collectivite details dto
     */
    CollectiviteDetailDto modifierCollectivite(CollectiviteDetailDto collectiviteDto);

    /**
     * Creer collectivite.
     *
     * @param collectiviteDto
     *            the collectivite dto
     * @return the collectivite details dto
     */
    CollectiviteDetailDto creerCollectivite(CollectiviteDetailDto collectiviteDto);

    /**
     * Synchroniser collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idDemandeSubvention
     *            the id demande subvention
     */
    void synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention);
}

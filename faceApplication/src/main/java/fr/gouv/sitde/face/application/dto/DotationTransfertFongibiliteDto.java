package fr.gouv.sitde.face.application.dto;

import java.math.BigDecimal;

public class DotationTransfertFongibiliteDto {
    /** id sous programme à débiter. */
    private Integer idSousProgrammeDebiter;

    /** id sous programme à créditer. */
    private Integer idSousProgrammeCrediter;

    /** la dotation disponible. */
    private BigDecimal montantTransfert;

    /** id de la collectivité. */
    private Long idCollectivite;

    public Integer getIdSousProgrammeCrediter() {
        return this.idSousProgrammeCrediter;
    }

    public void setIdSousProgrammeCrediter(Integer idSousProgrammeCrediter) {
        this.idSousProgrammeCrediter = idSousProgrammeCrediter;
    }

    public Integer getIdSousProgrammeDebiter() {
        return this.idSousProgrammeDebiter;
    }

    public void setIdSousProgrammeDebiter(Integer idSousProgrammeDebiter) {
        this.idSousProgrammeDebiter = idSousProgrammeDebiter;
    }

    public BigDecimal getMontantTransfert() {
        return this.montantTransfert;
    }

    public void setMontantTransfert(BigDecimal montantTransfert) {
        this.montantTransfert = montantTransfert;
    }

    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

}

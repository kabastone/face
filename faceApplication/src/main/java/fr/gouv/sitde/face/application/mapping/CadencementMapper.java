package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;

import fr.gouv.sitde.face.application.dto.CadencementDto;
import fr.gouv.sitde.face.application.dto.CadencementTableDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

@Mapper(uses = { ReferentielLovMapper.class }, componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CadencementMapper {

    @Mapping(target = "montantAnneeProlongation", source = "montantAnneeProlongation", defaultValue = "0")
    Cadencement cadencementDtoToCadencement(CadencementDto cadencementDto);

    Cadencement updateCadencementFromCadencementDto(@MappingTarget Cadencement cadencement, CadencementDto cadencementDto);

    CadencementDto cadencementToCadencementDto(Cadencement cadcadencement);

    @Mapping(target = "sousProgramme",
            source = "demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme")
    @Mapping(target = "anneeProgrammation",
            source = "demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme.annee")
    @Mapping(target = "hasProlongation", source = "demandeSubvention.dossierSubvention", qualifiedByName = "aDesProlongations")
    @Mapping(target = "plafondAideSubvention", source = "demandeSubvention.plafondAide")
    @Mapping(target = "numDossier", source = "demandeSubvention.dossierSubvention.numDossier")
    @Mapping(target = "idDossier", source = "demandeSubvention.dossierSubvention.id")
    @Mapping(target = "cadencement", source = "cadencement")
    @Mapping(target = "soldee", source = "demandeSubvention.dossierSubvention", qualifiedByName = "estSolde")
    CadencementTableDto cadencementToCadencementTableDto(Cadencement cadencement);

    /**
     * @param cadencements
     * @return
     */
    List<CadencementTableDto> cadencementsToCadencementTableDtos(List<Cadencement> cadencements);

    @Named("aDesProlongations")
    default Boolean aDesProlongations(DossierSubvention dossier) {
        return dossier.getDemandesProlongation().stream().anyMatch(CadencementMapper::prolongeSurAnneeSuivante);
    }

    @Named("estSolde")
    default Boolean estSolde(DossierSubvention dossier) {
        return dossier.getDemandesPaiement().stream().anyMatch(CadencementMapper::soldeEtNonRefusee);
    }

    static Boolean soldeEtNonRefusee(DemandePaiement demande) {
        return TypeDemandePaiementEnum.SOLDE.equals(demande.getTypeDemandePaiement())
                && !EtatDemandePaiementEnum.REFUSEE.name().equals(demande.getEtatDemandePaiement().getCode());
    }

    static Boolean prolongeSurAnneeSuivante(DemandeProlongation demande) {
        return EtatDemandeProlongationEnum.VALIDEE.name().equals(demande.getEtatDemandeProlongation().getCode())
                && (demande.getDateAchevementDemandee().equals(demande.getDossierSubvention().getDateEcheanceAchevement()))
                && (demande.getDateAchevementDemandee()
                        .getYear() == (demande.getDossierSubvention().getDateCreationDossier().getYear() + ConstantesFace.NOMBRE_ANNEE_DOSSIER));
    }

}

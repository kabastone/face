/**
 *
 */
package fr.gouv.sitde.face.application.administration.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService;
import fr.gouv.sitde.face.transverse.document.OrigineDocumentDto;

/**
 * The Class RechercheCollectiviteOrigineApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class RechercheCollectiviteOrigineApplicationImpl implements RechercheCollectiviteOrigineApplication {

    /** The collectivite origine service. */
    @Inject
    private CollectiviteOrigineService collectiviteOrigineService;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherIdCollectiviteParDotationCollectivite(java.lang
     * .Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDotationCollectivite(Long idDotationCollectivite) {
        return this.collectiviteOrigineService.rechercherIdCollectiviteParDotationCollectivite(idDotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherIdCollectiviteParDossierSubvention(java.lang.
     * Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDossierSubvention(Long idDossierSubvention) {
        return this.collectiviteOrigineService.rechercherIdCollectiviteParDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherIdCollectiviteParDemandeSubvention(java.lang.
     * Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeSubvention(Long idDemandeSubvention) {
        return this.collectiviteOrigineService.rechercherIdCollectiviteParDemandeSubvention(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherIdCollectiviteParDemandeProlongation(java.lang.
     * Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeProlongation(Long idDemandeProlongation) {
        return this.collectiviteOrigineService.rechercherIdCollectiviteParDemandeProlongation(idDemandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherIdCollectiviteParDemandePaiement(java.lang.
     * Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandePaiement(Long idDemandePaiement) {
        return this.collectiviteOrigineService.rechercherIdCollectiviteParDemandePaiement(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication#rechercherCollectiviteOrigineParDocument(java.lang.Long)
     */
    @Override
    public OrigineDocumentDto rechercherCollectiviteOrigineParDocument(Long idDocument) {
        return this.collectiviteOrigineService.rechercherCollectivitesOrigineParDocument(idDocument);
    }

}

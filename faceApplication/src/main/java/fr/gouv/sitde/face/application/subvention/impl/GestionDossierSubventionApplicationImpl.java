/**
 *
 */
package fr.gouv.sitde.face.application.subvention.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.DemandePaiementSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeProlongationSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionSimpleDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionDetailDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionTableMferDto;
import fr.gouv.sitde.face.application.dto.ResultatRechercheDossierDto;
import fr.gouv.sitde.face.application.mapping.DemandePaiementMapper;
import fr.gouv.sitde.face.application.mapping.DemandeProlongationMapper;
import fr.gouv.sitde.face.application.mapping.DemandeSubventionMapper;
import fr.gouv.sitde.face.application.mapping.DossierSubventionMapper;
import fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class GestionDossierSubventionApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionDossierSubventionApplicationImpl implements GestionDossierSubventionApplication {

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The dossier subvention mapper. */
    @Inject
    private DossierSubventionMapper dossierSubventionMapper;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande subvention simple mapper. */
    @Inject
    private DemandeSubventionMapper demandeSubventionMapper;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The demande paiement simple mapper. */
    @Inject
    private DemandePaiementMapper demandePaiementMapper;

    /** The demande prolongation service. */
    @Inject
    private DemandeProlongationService demandeProlongationService;

    /** The demande prolongation simple mapper. */
    @Inject
    private DemandeProlongationMapper demandeProlongationMapper;

    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDossierSubventionApplication#rechercherDossierSubventionParId(java.lang.Long)
     */
    @Override
    public DossierSubventionDetailDto rechercherDossierSubventionParId(Long idDossierSubvention) {
        // recherche
        DossierSubvention dossierSubvention = this.dossierSubventionService.rechercherDossierSubventionParId(idDossierSubvention);

        DossierSubventionDetailDto dossierSubventionDetailDto = this.dossierSubventionMapper
                .dossierSubventionToDossierSubventionDetailDto(dossierSubvention);

        boolean estUniqueEnCoursSubventionParSousProgramme = this.dotationCollectiviteService.isEstUniqueSubventionEnCoursParSousProgramme(null,
                dossierSubventionDetailDto.getIdDotationCollectivite(), dossierSubventionDetailDto.getDescriptionSousProgramme());

        dossierSubventionDetailDto.setEstUniqueSubventionEnCoursParSousProgramme(estUniqueEnCoursSubventionParSousProgramme);

        boolean estUniquePaiementEnCours = this.demandePaiementService.isEstUniqueDemandePaiementEnCours(dossierSubventionDetailDto.getNumDossier());

        dossierSubventionDetailDto.setEstUniquePaiementEnCours(estUniquePaiementEnCours);

        // mapping retour
        return dossierSubventionDetailDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication#rechercherDemandesSubvention(java.lang.Long)
     */
    @Override
    public List<DemandeSubventionSimpleDto> rechercherDemandesSubvention(Long idDossierSubvention) {
        List<DemandeSubvention> listeDemandeSubvention = this.demandeSubventionService.rechercherDemandesSubvention(idDossierSubvention);
        List<DemandeSubventionSimpleDto> listeDemandesSubventionDto = this.demandeSubventionMapper
                .demandesSubventionToDemandesSubventionSimpleDto(listeDemandeSubvention);

        // gestion du type de demande
        for (DemandeSubventionSimpleDto demandeSubventionDto : listeDemandesSubventionDto) {
            if (demandeSubventionDto.isDemandeComplementaire()) {
                demandeSubventionDto.setTypeDemande(MessageUtils.getMsgValidationMetier("subvention.demande.complementaire"));
            } else {
                demandeSubventionDto.setTypeDemande(MessageUtils.getMsgValidationMetier("subvention.demande.initiale"));
            }
        }

        return listeDemandesSubventionDto;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication#rechercherDemandesPaiement(java.lang.Long)
     */
    @Override
    public List<DemandePaiementSimpleDto> rechercherDemandesPaiement(Long idDossierSubvention) {
        List<DemandePaiement> listeDemandesPaiement = this.demandePaiementService.rechercherDemandesPaiement(idDossierSubvention);
        return this.demandePaiementMapper.demandesPaiementToDemandesPaiementSimpleDto(listeDemandesPaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication#rechercherDemandesProlongation(java.lang.Long)
     */
    @Override
    public List<DemandeProlongationSimpleDto> rechercherDemandesProlongation(Long idDossierSubvention) {
        List<DemandeProlongation> listeDemandesProlongation = this.demandeProlongationService.rechercherDemandesProlongation(idDossierSubvention);
        return this.demandeProlongationMapper.demandesProlongationToDemandesProlongationSimpleDto(listeDemandesProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication#rechercherDossiersParCriteres(fr.gouv.sitde.face.transverse.
     * queryobject.CritereRechercheDossierPourDemandeSubventionQo)
     */
    @Override
    public PageReponse<ResultatRechercheDossierDto> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche) {
        PageReponse<DossierSubvention> pageReponseDossiers = this.dossierSubventionService.rechercherDossiersParCriteres(criteresRecherche);
        return this.traiterPageReponsePourEnvoi(pageReponseDossiers);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication#rechercherDossiersParCriteresPaiement(fr.gouv.sitde.face.
     * transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo)
     */
    @Override
    public PageReponse<ResultatRechercheDossierDto> rechercherDossiersParCriteresPaiement(
            CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {
        PageReponse<DossierSubvention> pageReponseDossiers = this.dossierSubventionService.rechercherDossiersParCriteresPaiement(criteresRecherche);
        return this.traiterPageReponsePourEnvoi(pageReponseDossiers);
    }

    /**
     * @param pageReponseDossiers
     * @return
     */
    private PageReponse<ResultatRechercheDossierDto> traiterPageReponsePourEnvoi(PageReponse<DossierSubvention> pageReponseDossiers) {
        // Renvoi page reponse ResultatRechercheDossierDto
        List<ResultatRechercheDossierDto> listeResultatsDto = this.dossierSubventionMapper
                .dossiersSubventionToResultatRechercheDossierDtos(pageReponseDossiers.getListeResultats());
        return new PageReponse<>(listeResultatsDto, pageReponseDossiers.getNbTotalResultats());
    }

    @Override
    public List<DossierSubventionTableMferDto> rechercherDossierSubventionCadencementEnDefaut(String codeProgramme) {
        List<DossierSubvention> dossierSubventions = this.dossierSubventionService.rechercherDossierSubventionCadencementDefaut(codeProgramme);

        return this.dossierSubventionMapper.dossierSubventionsToDossierSubventionDefautDtos(dossierSubventions);
    }

    /**
     * Rechercher dossier subvention cadencement en retard.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    @Override
    public List<DossierSubventionTableMferDto> rechercherDossierSubventionTravauxEnRetard(String codeProgramme) {
        List<DossierSubvention> dossierSubventions = this.dossierSubventionService.rechercherDossierSubventionRetardTravaux(codeProgramme);

        return this.dossierSubventionMapper.dossierSubventionsToDossierSubventionDefautDtos(dossierSubventions);
    }
}

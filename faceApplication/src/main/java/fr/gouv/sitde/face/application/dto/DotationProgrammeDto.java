/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class DotationProgrammeDto.
 *
 * @author a453029
 */
public class DotationProgrammeDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2515605152960799267L;

    /** The id dotation programme. */
    private Long idDotationProgramme;

    /** The id programme. */
    private Long idProgramme;

    /** The annee. */
    private int annee;

    /** The code numerique. */
    private String codeNumerique;

    /** The montant. */
    private BigDecimal montant;

    /** The version. */
    private int version;

    /**
     * Gets the id dotation programme.
     *
     * @return the idDotationProgramme
     */
    public Long getIdDotationProgramme() {
        return this.idDotationProgramme;
    }

    /**
     * Sets the id dotation programme.
     *
     * @param idDotationProgramme
     *            the idDotationProgramme to set
     */
    public void setIdDotationProgramme(Long idDotationProgramme) {
        this.idDotationProgramme = idDotationProgramme;
    }

    /**
     * Gets the id programme.
     *
     * @return the idProgramme
     */
    public Long getIdProgramme() {
        return this.idProgramme;
    }

    /**
     * Sets the id programme.
     *
     * @param idProgramme
     *            the idProgramme to set
     */
    public void setIdProgramme(Long idProgramme) {
        this.idProgramme = idProgramme;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the annee.
     *
     * @return the annee
     */
    public int getAnnee() {
        return this.annee;
    }

    /**
     * Sets the annee.
     *
     * @param annee
     *            the annee to set
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * @return the codeNumerique
     */
    public String getCodeNumerique() {
        return this.codeNumerique;
    }

    /**
     * @param codeNumerique
     *            the codeNumerique to set
     */
    public void setCodeNumerique(String codeNumerique) {
        this.codeNumerique = codeNumerique;
    }
}

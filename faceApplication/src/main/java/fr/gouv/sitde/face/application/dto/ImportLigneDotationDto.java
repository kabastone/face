/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.bean.CsvBindByName;

/**
 * The Class ImportLigneDotationDto.
 *
 * @author Atos
 */
public class ImportLigneDotationDto {

    /** Numérique du departement sur 3 digit. */
    @CsvBindByName(required = false, column = "code")
    private String numeriqueDepartement;

    /**
     * Montant pour code domaine fonctionnel RR Renforcement des Reseaux.
     */
    @CsvBindByName(required = false, column = "AE")
    private BigDecimal renforcementReseaux;

    /** Montant pour code domaine fonctionnel ER Extension des Reseaux. */
    @CsvBindByName(required = false, column = "AP")
    private BigDecimal extensionReseaux;

    /** Montant pour code domaine fonctionnel EF Enfouissement et pose en facade. */
    @CsvBindByName(required = false, column = "CE")
    private BigDecimal enfouissementEtFacade;

    /** Montant pour code domaine fonctionnel SE Securisation */
    @CsvBindByName(required = false, column = "SN")
    private BigDecimal securisation;

    /** Somme des dotations. */
    @CsvBindByName(required = false, column = "total")
    private BigDecimal sommeDepartement;

    /** Penalite de non regroupement. */
    @CsvBindByName(required = false, column = "penalite non regroupement")
    private BigDecimal penaliteNonRegroupement;

    /** Penalite de penaliteGestionStock. */
    @CsvBindByName(required = false, column = "penalite stock")
    private BigDecimal penaliteGestionStock;

    /**
     * Gets the numerique departement.
     *
     * Get 3 first characters
     *
     * trim whitespace
     *
     * remove 00 or 0 before real number
     *
     * @return the numeriqueDepartement
     */
    public String getCodeDepartement() {
        return StringUtils.removeStart(StringUtils.removeStart(StringUtils.trim(StringUtils.substring(this.numeriqueDepartement, 0, 3)), "00"), "0");
    }

    /**
     * Gets the numerique departement.
     *
     * @return the numeriqueDepartement
     */
    public String getNumeriqueDepartement() {
        return this.numeriqueDepartement;
    }

    /**
     * Sets the numerique departement.
     *
     * @param numeriqueDepartement
     *            the numeriqueDepartement to set
     */
    public void setNumeriqueDepartement(String numeriqueDepartement) {
        this.numeriqueDepartement = numeriqueDepartement;
    }

    /**
     * Gets the dot renf.
     *
     * @return the renforcementReseaux
     */
    public BigDecimal getRenforcementReseaux() {
        return this.renforcementReseaux;
    }

    /**
     * Sets the 'dot_renf' column : renforcement reseaux.
     *
     * @param dotRenf
     *            the new renforcement reseaux
     */
    public void setRenforcementReseaux(BigDecimal dotRenf) {
        this.renforcementReseaux = dotRenf;
    }

    /**
     * Gets the dot ext.
     *
     * @return the extensionReseaux
     */
    public BigDecimal getExtensionReseaux() {
        return this.extensionReseaux;
    }

    /**
     * Sets the dot_ext column : extension reseau.
     *
     * @param dotExt
     *            the new extension reseaux
     */
    public void setExtensionReseaux(BigDecimal dotExt) {
        this.extensionReseaux = dotExt;
    }

    /**
     * Gets the dot c.
     *
     * @return the enfouissementEtFacade
     */
    public BigDecimal getEnfouissementEtFacade() {
        return this.enfouissementEtFacade;
    }

    /**
     * Sets the dot_c column : enfouissement et facade.
     *
     * @param dotC
     *            the new enfouissement et facade
     */
    public void setEnfouissementEtFacade(BigDecimal dotC) {
        this.enfouissementEtFacade = dotC;
    }

    /**
     * Gets the dot c.
     *
     * @return the securisation
     */
    public BigDecimal getSecurisation() {
        return this.securisation;
    }

    /**
     * Sets the dot_d column : securisation.
     *
     * @param dotD
     *            the new securisation
     */
    public void setSecurisation(BigDecimal dotD) {
        this.securisation = dotD;
    }

    /**
     * Gets the total dotation.
     *
     * @return the sommeDepartement
     */
    public BigDecimal getSommeDepartement() {
        return this.sommeDepartement;
    }

    /**
     * Sets the total dotation.
     *
     * @param totalDotation
     *            the new somme departement
     */
    public void setSommeDepartement(BigDecimal totalDotation) {
        this.sommeDepartement = totalDotation;
    }

    /**
     * Gets the non regroupement.
     *
     * @return the penaliteNonRegroupement
     */
    public BigDecimal getPenaliteNonRegroupement() {
        return this.penaliteNonRegroupement;
    }

    /**
     * Sets the non regroupement.
     *
     * @param nonRegroupement
     *            the new penalite non regroupement
     */
    public void setPenaliteNonRegroupement(BigDecimal nonRegroupement) {
        this.penaliteNonRegroupement = nonRegroupement;
    }

    /**
     * Gets the penaliteGestionStock.
     *
     * @return the penaliteGestionStock
     */
    public BigDecimal getPenaliteGestionStock() {
        return this.penaliteGestionStock;
    }

    /**
     * Sets the penaliteGestionStock.
     *
     * @param stock
     *            the new penalite gestion stock
     */
    public void setPenaliteGestionStock(BigDecimal stock) {
        this.penaliteGestionStock = stock;
    }

}

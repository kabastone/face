package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Class DotationRenoncementDto.
 */
public class DotationRenoncementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5069073725231687312L;

    /** The id sous programme. */
    private Integer idSousProgramme;

    /** The dotation disponible. */
    private BigDecimal dotationDisponible;

    /** The montant. */
    private BigDecimal montant;

    /** The id collectivite. */
    private Long idCollectivite;

    /** The fichier envoyer. */
    private FichierTransfert fichierEnvoyer;

    /**
     * Gets the id sous programme.
     *
     * @return the idSousProgramme
     */
    public Integer getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * Sets the id sous programme.
     *
     * @param idSousProgramme
     *            the idSousProgramme to set
     */
    public void setIdSousProgramme(Integer idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

    /**
     * Gets the dotation disponible.
     *
     * @return the dotationDisponible
     */
    public BigDecimal getDotationDisponible() {
        return this.dotationDisponible;
    }

    /**
     * Sets the dotation disponible.
     *
     * @param dotationDisponible
     *            the dotationDisponible to set
     */
    public void setDotationDisponible(BigDecimal dotationDisponible) {
        this.dotationDisponible = dotationDisponible;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

    /**
     * @return the fichierEnvoyer
     */
    public FichierTransfert getFichierEnvoyer() {
        return fichierEnvoyer;
    }

    /**
     * @param fichierEnvoyer the fichierEnvoyer to set
     */
    public void setFichierEnvoyer(FichierTransfert fichierEnvoyer) {
        this.fichierEnvoyer = fichierEnvoyer;
    }
}

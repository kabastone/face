/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleTableauPertesRenoncementDto;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;

/**
 * Mapper entité/DTO des lignes dotations departementales.
 *
 * @author a702709
 *
 */
@Mapper(uses = { SousProgrammeMapper.class }, componentModel = "spring")
public interface LigneDotationDepartementaleMapper {

    /**
     * Ligne dotation departementale to ligne dotation departementale dto.
     *
     * @param ligneDotationDepartement
     *            the ligne dotation departementale
     * @return the ligne dotation departementale dto
     */
    @Mapping(target = "idLigneDotationDepartementale", source = "id")
    @Mapping(target = "idDepartement", source = "dotationDepartement.departement.id")
    @Mapping(target = "sousProgramme", source = "dotationDepartement.dotationSousProgramme.sousProgramme")
    LigneDotationDepartementaleDto ligneDotationDepartementToLigneDotationDepartementaleDto(LigneDotationDepartement ligneDotationDepartement);

    List<LigneDotationDepartementaleDto> ligneDotationDepartementToLigneDotationDepartementaleDto(
            List<LigneDotationDepartement> ligneDotationDepartement);

    /**
     * Ligne dotation departementale dto to ligne dotation departementale
     *
     * @param ligneDotationDepartementaleDto
     *            the ligne dotation departementale
     * @return the ligne dotation departementale
     */
    @Mapping(target = "id", source = "idLigneDotationDepartementale")
    @Mapping(target = "dotationDepartement.departement.id", source = "idDepartement")
    @Mapping(target = "dotationDepartement.dotationSousProgramme.sousProgramme", source = "sousProgramme")
    LigneDotationDepartement ligneDotationDepartementaleDtoToLigneDotationDepartement(LigneDotationDepartementaleDto ligneDotationDepartementaleDto);

    List<LigneDotationDepartement> ligneDotationDepartementaleDtoToLigneDotationDepartement(
            List<LigneDotationDepartementaleDto> ligneDotationDepartementaleDto);

    @Mapping(target = "idLigneDotationDepartementale", source = "id")
    @Mapping(target = "departementCode", source = "dotationDepartement.departement.code")
    @Mapping(target = "departementNom", source = "dotationDepartement.departement.nom")
    @Mapping(target = "type", source = "typeDotationDepartement")
    @Mapping(target = "date", source = "dateEnvoi")
    @Mapping(target = "sousProgramme", source = "dotationDepartement.dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "collectivite", source = "collectivite.nomCourt")
    LigneDotationDepartementaleTableauPertesRenoncementDto ligneDotationDepartementaleToLigneDotationDepartementaleTableauPertesRenoncementDto(
            LigneDotationDepartement ligneDotationDepartementaleDto);

    /**
     * @param listeLignes
     * @return
     */
    List<LigneDotationDepartementaleTableauPertesRenoncementDto> ligneDotationDepartementsToLigneDotationDepartementaleTableauPertesRenoncementDtos(
            List<LigneDotationDepartement> listeLignes);

}

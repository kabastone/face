package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.application.jsonutils.CustomTypeDotationDepartementDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomTypeDotationDepartementSerializer;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Class LigneDotationDepartementaleDto.
 */
public class LigneDotationDepartementaleTableauPertesRenoncementDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1867294428474102722L;

    /**
     * The instance id.
     */
    private Long idLigneDotationDepartementale;

    /**
     * The departement code.
     */
    private String departementCode;

    /** The departement nom. */
    private String departementNom;

    /** The montant. */
    private BigDecimal montant = BigDecimal.ZERO;

    /** The type dotation departement. */
    @JsonSerialize(using = CustomTypeDotationDepartementSerializer.class)
    @JsonDeserialize(using = CustomTypeDotationDepartementDeserializer.class)
    private TypeDotationDepartementEnum type;

    /** The LocalDateTime envoi. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime date;

    /** The sous programme. */
    private String sousProgramme;

    /** The collectivite. */
    private String collectivite;

    public Long getIdLigneDotationDepartementale() {
        return this.idLigneDotationDepartementale;
    }

    public void setIdLigneDotationDepartementale(Long idLigneDotationDepartementale) {
        this.idLigneDotationDepartementale = idLigneDotationDepartementale;
    }

    public BigDecimal getMontant() {
        return this.montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public TypeDotationDepartementEnum getType() {
        return this.type;
    }

    public void setType(TypeDotationDepartementEnum typeDotationDepartement) {
        this.type = typeDotationDepartement;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime dateEnvoi) {
        this.date = dateEnvoi;
    }

    public String getSousProgramme() {
        return this.sousProgramme;
    }

    public void setSousProgramme(String sousProgramme) {
        this.sousProgramme = sousProgramme;
    }

    /**
     * @return the departementCode
     */
    public String getDepartementCode() {
        return this.departementCode;
    }

    /**
     * @param departementCode
     *            the departementCode to set
     */
    public void setDepartementCode(String departementCode) {
        this.departementCode = departementCode;
    }

    /**
     * @return the departementNom
     */
    public String getDepartementNom() {
        return this.departementNom;
    }

    /**
     * @param departementNom
     *            the departementNom to set
     */
    public void setDepartementNom(String departementNom) {
        this.departementNom = departementNom;
    }

    /**
     * @return the collectivite
     */
    public String getCollectivite() {
        return this.collectivite;
    }

    /**
     * @param collectivite
     *            the collectivite to set
     */
    public void setCollectivite(String collectivite) {
        this.collectivite = collectivite;
    }
}

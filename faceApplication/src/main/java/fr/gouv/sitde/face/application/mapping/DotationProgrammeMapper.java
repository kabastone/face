/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.DotationProgrammeDto;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;

/**
 * Mapper entité/DTO des dotations programme.
 *
 * @author a702709
 *
 */
@Mapper(componentModel = "spring")
public interface DotationProgrammeMapper {

    /**
     * Dotation programme to dotation programme dto.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme dto
     */
    @Mapping(target = "idDotationProgramme", source = "dotationProgramme.id")
    @Mapping(target = "idProgramme", source = "dotationProgramme.programme.id")
    @Mapping(target = "codeNumerique", source = "dotationProgramme.programme.codeNumerique")
    DotationProgrammeDto dotationProgrammeToDotationProgrammeDto(DotationProgramme dotationProgramme);

    /**
     * Dotation programme dto to dotation programme.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme
     */
    @Mapping(target = "id", source = "dotationProgramme.idDotationProgramme")
    @Mapping(target = "programme.id", source = "dotationProgramme.idProgramme")
    @Mapping(target = "programme.codeNumerique", source = "dotationProgramme.codeNumerique")
    DotationProgramme dotationProgrammeDtoToDotationProgramme(DotationProgrammeDto dotationProgramme);
}

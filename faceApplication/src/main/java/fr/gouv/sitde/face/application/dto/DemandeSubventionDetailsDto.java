/*
 * 
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Class DemandeSubventionDetailsDto.
 */
public class DemandeSubventionDetailsDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6292722605831412524L;

    /** The id. */
    private Long id;

    /** The etat demande subvention. */
    private String etat;

    /** The etat libelle. */
    private String etatLibelle;

    /** The num dossier. */
    private String numDossier;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateJour;

    /** The chorusnum ligne legacy. */
    private String chorusNumLigneLegacy;

    /** The montant travaux eligible. */
    private BigDecimal montantHTTravauxEligibles;

    /** The LocalDateTime etat. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEtat;

    /** The LocalDateTime etat. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateAccord;

    /** The taux aide. */
    private BigDecimal tauxAideFacePercent;

    /** The message complementaire. */
    private String message;

    /** The motif refus. */
    private String motifRefus;

    /** The demande complementaire. */
    private boolean demandeComplementaire;

    /** The documents. */
    private Set<DocumentDto> documents = new LinkedHashSet<>(0);

    /** The liste fichiers envoyer. */
    private List<FichierTransfert> listeFichiersEnvoyer = new ArrayList<>();

    /** The anomalies. */
    private Set<AnomalieDto> anomalies = new LinkedHashSet<>(0);

    /** The plafond aide. */
    private BigDecimal plafondAide = BigDecimal.ZERO;

    /** The id dossier dotation collectivite. */
    private Long idDotationCollectivite;

    /** The id collectivite. */
    private Long idCollectivite;

    /** The id dossier. */
    private Long idDossier;

    /** The id sous programme. */
    private Long idSousProgramme;

    /** The version. */
    private int version;

    /** The dotation disponible. */
    private BigDecimal dotationDisponible = BigDecimal.ZERO;

    /** The sous programme. */
    private String sousProgramme;

    /** The ids docs compl suppr. */
    private List<Long> idsDocsComplSuppr = new ArrayList<>();

    /** The nom long. */
    private String nomLongCollectivite;

    /**  The code département. */
    private String codeDepartement;
    
	 /** The cadencement dto. */
    private CadencementDto cadencementDto; 

    /**
     * Gets the cadencement dto.
     *
     * @return the cadencement dto
     */
    public CadencementDto getCadencementDto() {
		return cadencementDto;
	}

	/**
	 * Sets the cadencement dto.
	 *
	 * @param cadencementDto the new cadencement dto
	 */
	public void setCadencementDto(CadencementDto cadencementDto) {
		this.cadencementDto = cadencementDto;
	}

	/**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the chorus num ligne legacy.
     *
     * @return the chorusNumLigneLegacy
     */
    public String getChorusNumLigneLegacy() {
        return this.chorusNumLigneLegacy;
    }

    /**
     * Sets the chorus num ligne legacy.
     *
     * @param chorusNumLigneLegacy
     *            the chorusNumLigneLegacy to set
     */
    public void setChorusNumLigneLegacy(String chorusNumLigneLegacy) {
        this.chorusNumLigneLegacy = chorusNumLigneLegacy;
    }

    /**
     * Gets the date etat.
     *
     * @return the dateEtat
     */
    public LocalDateTime getDateEtat() {
        return this.dateEtat;
    }

    /**
     * Sets the date etat.
     *
     * @param dateEtat
     *            the dateEtat to set
     */
    public void setDateEtat(LocalDateTime dateEtat) {
        this.dateEtat = dateEtat;
    }

    /**
     * Gets the motif refus.
     *
     * @return the motifRefus
     */
    public String getMotifRefus() {
        return this.motifRefus;
    }

    /**
     * Sets the motif refus.
     *
     * @param motifRefus
     *            the motifRefus to set
     */
    public void setMotifRefus(String motifRefus) {
        this.motifRefus = motifRefus;
    }

    /**
     * Checks if is demande complementaire.
     *
     * @return the demandeComplementaire
     */
    public boolean isDemandeComplementaire() {
        return this.demandeComplementaire;
    }

    /**
     * Sets the demande complementaire.
     *
     * @param demandeComplementaire
     *            the demandeComplementaire to set
     */
    public void setDemandeComplementaire(boolean demandeComplementaire) {
        this.demandeComplementaire = demandeComplementaire;
    }

    /**
     * Gets the plafond aide.
     *
     * @return the plafondAide
     */
    public BigDecimal getPlafondAide() {
        return this.plafondAide;
    }

    /**
     * Sets the plafond aide.
     *
     * @param plafondAide
     *            the plafondAide to set
     */
    public void setPlafondAide(BigDecimal plafondAide) {
        this.plafondAide = plafondAide;
    }

    /**
     * Gets the documents.
     *
     * @return the documents
     */
    public Set<DocumentDto> getDocuments() {
        return this.documents;
    }

    /**
     * Gets the anomalies.
     *
     * @return the anomalies
     */
    public Set<AnomalieDto> getAnomalies() {
        return this.anomalies;
    }

    /**
     * Gets the etat.
     *
     * @return the etat
     */
    public String getEtat() {
        return this.etat;
    }

    /**
     * Sets the etat.
     *
     * @param etat
     *            the etat to set
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }

    /**
     * Gets the date jour.
     *
     * @return the dateJour
     */
    public LocalDateTime getDateJour() {
        return this.dateJour;
    }

    /**
     * Sets the date jour.
     *
     * @param dateJour
     *            the dateJour to set
     */
    public void setDateJour(LocalDateTime dateJour) {
        this.dateJour = dateJour;
    }

    /**
     * Gets the montant HT travaux eligibles.
     *
     * @return the montantHTTravauxEligibles
     */
    public BigDecimal getMontantHTTravauxEligibles() {
        return this.montantHTTravauxEligibles;
    }

    /**
     * Sets the montant HT travaux eligibles.
     *
     * @param montantHTTravauxEligibles
     *            the montantHTTravauxEligibles to set
     */
    public void setMontantHTTravauxEligibles(BigDecimal montantHTTravauxEligibles) {
        this.montantHTTravauxEligibles = montantHTTravauxEligibles;
    }

    /**
     * Gets the taux aide face percent.
     *
     * @return the tauxAideFacePercent
     */
    public BigDecimal getTauxAideFacePercent() {
        return this.tauxAideFacePercent;
    }

    /**
     * Sets the taux aide face percent.
     *
     * @param tauxAideFacePercent
     *            the tauxAideFacePercent to set
     */
    public void setTauxAideFacePercent(BigDecimal tauxAideFacePercent) {
        this.tauxAideFacePercent = tauxAideFacePercent;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Gets the id dossier.
     *
     * @return the idDossier
     */
    public Long getIdDossier() {
        return this.idDossier;
    }

    /**
     * Sets the id dossier.
     *
     * @param idDossier
     *            the idDossier to set
     */
    public void setIdDossier(Long idDossier) {
        this.idDossier = idDossier;
    }

    /**
     * Gets the liste fichiers envoyer.
     *
     * @return the listeFichiersEnvoyer
     */
    public List<FichierTransfert> getListeFichiersEnvoyer() {
        return this.listeFichiersEnvoyer;
    }

    /**
     * Sets the liste fichiers envoyer.
     *
     * @param listeFichiersEnvoyer
     *            the listeFichiersEnvoyer to set
     */
    public void setListeFichiersEnvoyer(List<FichierTransfert> listeFichiersEnvoyer) {
        this.listeFichiersEnvoyer = listeFichiersEnvoyer;
    }

    /**
     * Gets the etat libelle.
     *
     * @return the etatLibelle
     */
    public String getEtatLibelle() {
        return this.etatLibelle;
    }

    /**
     * Sets the etat libelle.
     *
     * @param etatLibelle
     *            the etatLibelle to set
     */
    public void setEtatLibelle(String etatLibelle) {
        this.etatLibelle = etatLibelle;
    }

    /**
     * Gets the sous programme.
     *
     * @return the sousProgramme
     */
    public String getSousProgramme() {
        return this.sousProgramme;
    }

    /**
     * Sets the sous programme.
     *
     * @param sousProgramme
     *            the sousProgramme to set
     */
    public void setSousProgramme(String sousProgramme) {
        this.sousProgramme = sousProgramme;
    }

    /**
     * Gets the dotation disponible.
     *
     * @return the dotationDisponible
     */
    public BigDecimal getDotationDisponible() {
        return this.dotationDisponible;
    }

    /**
     * Sets the dotation disponible.
     *
     * @param dotationDisponible
     *            the dotationDisponible to set
     */
    public void setDotationDisponible(BigDecimal dotationDisponible) {
        this.dotationDisponible = dotationDisponible;
    }

    /**
     * Gets the id dotation collectivite.
     *
     * @return the idDotationCollectivite
     */
    public Long getIdDotationCollectivite() {
        return this.idDotationCollectivite;
    }

    /**
     * Sets the id dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the idDotationCollectivite to set
     */
    public void setIdDotationCollectivite(Long idDotationCollectivite) {
        this.idDotationCollectivite = idDotationCollectivite;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

    /**
     * Gets the id sous programme.
     *
     * @return the idSousProgramme
     */
    public Long getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * Sets the id sous programme.
     *
     * @param idSousProgramme            the idSousProgramme to set
     */
    public void setIdSousProgramme(Long idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

    /**
     * Sets the documents.
     *
     * @param documents            the documents to set
     */
    public void setDocuments(Set<DocumentDto> documents) {
        this.documents = documents;
    }

    /**
     * Sets the anomalies.
     *
     * @param anomalies            the anomalies to set
     */
    public void setAnomalies(Set<AnomalieDto> anomalies) {
        this.anomalies = anomalies;
    }

    /**
     * Gets the date accord.
     *
     * @return the dateAccord
     */
    public LocalDateTime getDateAccord() {
        return this.dateAccord;
    }

    /**
     * Sets the date accord.
     *
     * @param dateAccord            the dateAccord to set
     */
    public void setDateAccord(LocalDateTime dateAccord) {
        this.dateAccord = dateAccord;
    }

    /**
     * Gets the ids docs compl suppr.
     *
     * @return the idsDocsComplSuppr
     */
    public List<Long> getIdsDocsComplSuppr() {
        return this.idsDocsComplSuppr;
    }

    /**
     * Sets the ids docs compl suppr.
     *
     * @param idsDocsComplSuppr            the idsDocsComplSuppr to set
     */
    public void setIdsDocsComplSuppr(List<Long> idsDocsComplSuppr) {
        this.idsDocsComplSuppr = idsDocsComplSuppr;
    }

    /**
     * Gets the nom long collectivite.
     *
     * @return the nomLongCollectivite
     */
    public String getNomLongCollectivite() {
        return this.nomLongCollectivite;
    }

    /**
     * Sets the nom long collectivite.
     *
     * @param nomLongCollectivite            the nomLongCollectivite to set
     */
    public void setNomLongCollectivite(String nomLongCollectivite) {
        this.nomLongCollectivite = nomLongCollectivite;
    }

    /**
     * Gets the code departement.
     *
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }
}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DocumentDto.
 */
public class DocumentDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3166795109764154612L;

    /** The id. */
    private Long id;

    /** The type document. */
    private String codeTypeDocument;

    /** The nom fichier sans extension. */
    private String nomFichierSansExtension;

    /** The extension fichier. */
    private String extensionFichier;

    /** The date televersement. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateTeleversement;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code type document.
     *
     * @return the codeTypeDocument
     */
    public String getCodeTypeDocument() {
        return this.codeTypeDocument;
    }

    /**
     * Sets the code type document.
     *
     * @param codeTypeDocument
     *            the codeTypeDocument to set
     */
    public void setCodeTypeDocument(String codeTypeDocument) {
        this.codeTypeDocument = codeTypeDocument;
    }

    /**
     * Gets the nom fichier sans extension.
     *
     * @return the nomFichierSansExtension
     */
    public String getNomFichierSansExtension() {
        return this.nomFichierSansExtension;
    }

    /**
     * Sets the nom fichier sans extension.
     *
     * @param nomFichierSansExtension
     *            the nomFichierSansExtension to set
     */
    public void setNomFichierSansExtension(String nomFichierSansExtension) {
        this.nomFichierSansExtension = nomFichierSansExtension;
    }

    /**
     * Gets the extension fichier.
     *
     * @return the extensionFichier
     */
    public String getExtensionFichier() {
        return this.extensionFichier;
    }

    /**
     * Sets the extension fichier.
     *
     * @param extensionFichier
     *            the extensionFichier to set
     */
    public void setExtensionFichier(String extensionFichier) {
        this.extensionFichier = extensionFichier;
    }

    /**
     * @return the dateTeleversement
     */
    public LocalDateTime getDateTeleversement() {
        return this.dateTeleversement;
    }

    /**
     * @param dateTeleversement
     *            the dateTeleversement to set
     */
    public void setDateTeleversement(LocalDateTime dateTeleversement) {
        this.dateTeleversement = dateTeleversement;
    }
}

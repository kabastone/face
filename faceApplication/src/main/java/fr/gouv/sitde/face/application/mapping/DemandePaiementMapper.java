/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.math.BigDecimal;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;

import fr.gouv.sitde.face.application.dto.DemandePaiementDto;
import fr.gouv.sitde.face.application.dto.DemandePaiementSimpleDto;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * Mapper entité/DTO des demandes de paiement.
 *
 * @author Atos
 *
 */
@Mapper(uses = { AnomalieMapper.class, TypeDemandePaiementMapper.class, DocumentMapper.class }, componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface DemandePaiementMapper {

    /**
     * Demande paiement to demande paiement simple dto.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement simple dto
     */
    @Mapping(target = "codeEtatDemande", source = "etatDemandePaiement.code")
    @Mapping(target = "libelleEtatDemande", source = "etatDemandePaiement.libelle")
    @Mapping(target = "montantTotalHtTravaux", source = "dossierSubvention", qualifiedByName = "calculMontantTotalDossier")
    DemandePaiementSimpleDto demandePaiementToDemandePaiementSimpleDto(DemandePaiement demandePaiement);

    /**
     * Demandes paiement to demandes paiement simple dto.
     *
     * @param listeDemandesPaiement
     *            the liste demandes paiement
     * @return the list
     */
    List<DemandePaiementSimpleDto> demandesPaiementToDemandesPaiementSimpleDto(List<DemandePaiement> listeDemandesPaiement);

    /**
     * Demande paiement dto to demande paiement.
     *
     * @param demandePaiementDto
     *            the demande paiement dto
     * @return the demande paiement
     */
    @Mapping(target = "dossierSubvention.id", source = "idDossierSubvention")
    @Mapping(target = "id", source = "idDemande")
    @Mapping(target = "typeDemandePaiement", source = "typeDemande")
    @Mapping(target = "montantTravauxRealises", source = "montantTravauxHt")
    @Mapping(target = "tauxAide", source = "tauxAideFacePercent")
    @Mapping(target = "messageComplementaire", source = "message")
    @Mapping(target = "dossierSubvention.etatDossier", source = "etatInstruction")
    @Mapping(target = "etatDemandePaiement.code", source = "codeEtatDemande")
    @Mapping(target = "etatDemandePaiement.libelle", source = "libelleEtatDemande")
    @Mapping(target = "dossierSubvention.numDossier", source = "numDossier")
    @Mapping(target = "dossierSubvention.resteConsommerCalcule", source = "resteConsommer")
    @Mapping(target = "dossierSubvention.dotationCollectivite.collectivite.nomLong", source = "nomLongCollectivite")
    @Mapping(target = "dossierSubvention.dotationCollectivite.dotationDepartement.departement.code", source = "codeDepartement")
    DemandePaiement demandePaiementDtoToDemandePaiement(DemandePaiementDto demandePaiementDto);

    /**
     * Demande paiement to demande paiement dto.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement dto
     */
    @Mapping(target = "idDossierSubvention", source = "dossierSubvention.id")
    @Mapping(target = "idDemande", source = "id")
    @Mapping(target = "typeDemande", source = "typeDemandePaiement")
    @Mapping(target = "montantTravauxHt", source = "montantTravauxRealises")
    @Mapping(target = "tauxAideFacePercent", source = "tauxAide")
    @Mapping(target = "message", source = "messageComplementaire")
    @Mapping(target = "etatInstruction", source = "dossierSubvention.etatDossier")
    @Mapping(target = "codeEtatDemande", source = "etatDemandePaiement.code")
    @Mapping(target = "libelleEtatDemande", source = "etatDemandePaiement.libelle")
    @Mapping(target = "numDossier", source = "dossierSubvention.numDossier")
    @Mapping(target = "resteConsommer", source = "dossierSubvention.resteConsommerCalcule")
    @Mapping(target = "nomLongCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.nomLong")
    @Mapping(target = "codeDepartement", source = "dossierSubvention.dotationCollectivite.dotationDepartement.departement.code")
    @Mapping(target = "montantTotalHtTravaux", source = "dossierSubvention", qualifiedByName = "calculMontantTotalDossier")
    DemandePaiementDto demandePaiementToDemandePaiementDto(DemandePaiement demandePaiement);

    /**
     * Update demande paiement from demande paiement dto.
     *
     * @param demandePaiementDto
     *            the demande paiement dto
     * @param demandePaiement
     *            the demande paiement
     */
    @Mapping(target = "dossierSubvention.id", source = "idDossierSubvention")
    @Mapping(target = "id", source = "idDemande")
    @Mapping(target = "typeDemandePaiement", source = "typeDemande")
    @Mapping(target = "montantTravauxRealises", source = "montantTravauxHt")
    @Mapping(target = "tauxAide", source = "tauxAideFacePercent")
    void updateDemandePaiementFromDemandePaiementDto(DemandePaiementDto demandePaiementDto, @MappingTarget DemandePaiement demandePaiement);

    @Named("calculMontantTotalDossier")
    default BigDecimal calculMontantTotalDossier(DossierSubvention dossier) {
        return dossier.getDemandesPaiement().stream()
                .filter(demande -> !TypeDemandePaiementEnum.AVANCE.getCode().equals(demande.getTypeDemandePaiement().getCode()))
                .map(DemandePaiement::getMontantTravauxRealises).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

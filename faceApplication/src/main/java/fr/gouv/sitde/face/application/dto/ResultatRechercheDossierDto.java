/**
 *
 */
package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class ResultatRechercheDossierDto.
 *
 * @author a453029
 */
public class ResultatRechercheDossierDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8357827085452562006L;

    /** The id dossier. */
    private Long idDossier;

    /** The LocalDateTime creation dossier. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateCreationDossier;

    /** The nom court collectivite. */
    private String nomCourtCollectivite;

    /** The num dossier. */
    private String numDossier;

    /** The num EJ. */
    private String numEJ;

    /** The plafond aide. */
    private BigDecimal plafondAide = BigDecimal.ZERO;

    /** The plafond aide sans refus. */
    private BigDecimal plafondAideSansRefus = BigDecimal.ZERO;

    /**
     * Gets the LocalDateTime creation dossier.
     *
     * @return the dateCreationDossier
     */
    public LocalDateTime getDateCreationDossier() {
        return this.dateCreationDossier;
    }

    /**
     * Sets the LocalDateTime creation dossier.
     *
     * @param dateCreationDossier
     *            the dateCreationDossier to set
     */
    public void setDateCreationDossier(LocalDateTime dateCreationDossier) {
        this.dateCreationDossier = dateCreationDossier;
    }

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the plafond aide.
     *
     * @return the plafondAide
     */
    public BigDecimal getPlafondAide() {
        return this.plafondAide;
    }

    /**
     * Sets the plafond aide.
     *
     * @param plafondAide
     *            the plafondAide to set
     */
    public void setPlafondAide(BigDecimal plafondAide) {
        this.plafondAide = plafondAide;
    }

    /**
     * Gets the nom court collectivite.
     *
     * @return the nomCourtCollectivite
     */
    public String getNomCourtCollectivite() {
        return this.nomCourtCollectivite;
    }

    /**
     * Sets the nom court collectivite.
     *
     * @param nomCourtCollectivite
     *            the nomCourtCollectivite to set
     */
    public void setNomCourtCollectivite(String nomCourtCollectivite) {
        this.nomCourtCollectivite = nomCourtCollectivite;
    }

    /**
     * Gets the id dossier.
     *
     * @return the idDossier
     */
    public Long getIdDossier() {
        return this.idDossier;
    }

    /**
     * Sets the id dossier.
     *
     * @param idDossier
     *            the idDossier to set
     */
    public void setIdDossier(Long idDossier) {
        this.idDossier = idDossier;
    }

    /**
     * @return the plafondAideSansRefus
     */
    public BigDecimal getPlafondAideSansRefus() {
        return this.plafondAideSansRefus;
    }

    /**
     * @param plafondAideSansRefus
     *            the plafondAideSansRefus to set
     */
    public void setPlafondAideSansRefus(BigDecimal plafondAideSansRefus) {
        this.plafondAideSansRefus = plafondAideSansRefus;
    }

    /**
     * @return the numEJ
     */
    public String getNumEJ() {
        return numEJ;
    }

    /**
     * @param numEJ the numEJ to set
     */
    public void setNumEJ(String numEJ) {
        this.numEJ = numEJ;
    }

}

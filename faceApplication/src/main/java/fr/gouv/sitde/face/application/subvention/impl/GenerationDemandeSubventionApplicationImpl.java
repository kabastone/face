/**
 *
 */
package fr.gouv.sitde.face.application.subvention.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.subvention.GenerationDemandeSubventionApplication;
import fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * @author A754839
 *
 *
 */
@Service
@Transactional
public class GenerationDemandeSubventionApplicationImpl implements GenerationDemandeSubventionApplication {

    @Inject
    private DonneesDocumentService donneesDocumentService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GenerationDemandeSubventionPdf#genererDemandeSubventionPdf(java.lang.Long)
     */
    @Override
    public FichierTransfert genererDemandeSubventionPdf(Long idDemandeSubvention) {
        return this.donneesDocumentService.genererDemandeSubventionPdf(idDemandeSubvention);
    }

}

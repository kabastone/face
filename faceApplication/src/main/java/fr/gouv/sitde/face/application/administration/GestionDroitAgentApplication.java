package fr.gouv.sitde.face.application.administration;

import java.util.List;

import fr.gouv.sitde.face.application.dto.DroitAgentUtilisateurDto;

/**
 * The Interface GestionDroitAgentApplication.
 *
 * @author Atos
 */
public interface GestionDroitAgentApplication {

    /**
     * Recherche tous les agents d'une collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<DroitAgentUtilisateurDto> rechercherAgentsParCollectivite(Long idCollectivite);

    /**
     * Ajoute un droit agent pour la collectivite et l'utilisateur specifies.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the droit agent collectivite dto
     */
    DroitAgentUtilisateurDto ajouterDroitAgent(Long idCollectivite, Long idUtilisateur);

    /**
     * Change l'état admin d'un droit agent (simple agent/admin) pour l'utilisateur et la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the droit agent collectivite dto
     */
    DroitAgentUtilisateurDto changerEtatAdminDroitAgent(Long idCollectivite, Long idUtilisateur);

    /**
     * Supprime un droit agent pour l'utilisateur et la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     */
    void supprimerDroitAgent(Long idCollectivite, Long idUtilisateur);
}

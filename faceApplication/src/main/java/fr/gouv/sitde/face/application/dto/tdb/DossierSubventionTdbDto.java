/**
 *
 */
package fr.gouv.sitde.face.application.dto.tdb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DossierSubventionTdbDto.
 *
 * @author a453029
 */
public class DossierSubventionTdbDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -965692766134507457L;

    /** The id. */
    private Long id;

    /** The date creation dossier. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateCreationDossier;

    /** The nom court collectivite. */
    private String nomCourtCollectivite;

    /** The num dossier. */
    private String numDossier;

    /** The plafond aide dossier. */
    private BigDecimal plafondAideDossier = BigDecimal.ZERO;

    /** The plafond aide sans refus. */
    private BigDecimal plafondAideSansRefus = BigDecimal.ZERO;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the date creation dossier.
     *
     * @return the dateCreationDossier
     */
    public LocalDateTime getDateCreationDossier() {
        return this.dateCreationDossier;
    }

    /**
     * Sets the date creation dossier.
     *
     * @param dateCreationDossier
     *            the dateCreationDossier to set
     */
    public void setDateCreationDossier(LocalDateTime dateCreationDossier) {
        this.dateCreationDossier = dateCreationDossier;
    }

    /**
     * Gets the nom court collectivite.
     *
     * @return the nomCourtCollectivite
     */
    public String getNomCourtCollectivite() {
        return this.nomCourtCollectivite;
    }

    /**
     * Sets the nom court collectivite.
     *
     * @param nomCourtCollectivite
     *            the nomCourtCollectivite to set
     */
    public void setNomCourtCollectivite(String nomCourtCollectivite) {
        this.nomCourtCollectivite = nomCourtCollectivite;
    }

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the plafond aide dossier.
     *
     * @return the plafondAideDossier
     */
    public BigDecimal getPlafondAideDossier() {
        return this.plafondAideDossier;
    }

    /**
     * Sets the plafond aide dossier.
     *
     * @param plafondAideDossier
     *            the plafondAideDossier to set
     */
    public void setPlafondAideDossier(BigDecimal plafondAideDossier) {
        this.plafondAideDossier = plafondAideDossier;
    }

    /**
     * @return the plafondAideSansRefus
     */
    public BigDecimal getPlafondAideSansRefus() {
        return this.plafondAideSansRefus;
    }

    /**
     * @param plafondAideSansRefus
     *            the plafondAideSansRefus to set
     */
    public void setPlafondAideSansRefus(BigDecimal plafondAideSansRefus) {
        this.plafondAideSansRefus = plafondAideSansRefus;
    }

}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * The Class AdresseDto.
 */
public class AdresseDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3521876031637017161L;

    /** The id. */
    private Long id;

    /** The civilite. */
    private String civilite;

    /** The nom. */
    private String nom;

    /** The prenom. */
    private String prenom;

    /** The qualite destinataire. */
    private String qualiteDestinataire;

    /** The numero voie. */
    private String numeroVoie;

    /** The nom voie. */
    private String nomVoie;

    /** The complement. */
    private String complement;

    /** The code postal. */
    private String codePostal;

    /** The commune. */
    private String commune;

    /** The version. */
    private Integer version;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the civilite.
     *
     * @return the civilite
     */
    public String getCivilite() {
        return this.civilite;
    }

    /**
     * Sets the civilite.
     *
     * @param civilite
     *            the civilite to set
     */
    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets the prenom.
     *
     * @param prenom
     *            the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets the qualite destinataire.
     *
     * @return the qualiteDestinataire
     */
    public String getQualiteDestinataire() {
        return this.qualiteDestinataire;
    }

    /**
     * Sets the qualite destinataire.
     *
     * @param qualiteDestinataire
     *            the qualiteDestinataire to set
     */
    public void setQualiteDestinataire(String qualiteDestinataire) {
        this.qualiteDestinataire = qualiteDestinataire;
    }

    /**
     * Gets the numero voie.
     *
     * @return the numeroVoie
     */
    public String getNumeroVoie() {
        return this.numeroVoie;
    }

    /**
     * Sets the numero voie.
     *
     * @param numeroVoie
     *            the numeroVoie to set
     */
    public void setNumeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
    }

    /**
     * Gets the nom voie.
     *
     * @return the nomVoie
     */
    public String getNomVoie() {
        return this.nomVoie;
    }

    /**
     * Sets the nom voie.
     *
     * @param nomVoie
     *            the nomVoie to set
     */
    public void setNomVoie(String nomVoie) {
        this.nomVoie = nomVoie;
    }

    /**
     * Gets the complement.
     *
     * @return the complement
     */
    public String getComplement() {
        return this.complement;
    }

    /**
     * Sets the complement.
     *
     * @param complement
     *            the complement to set
     */
    public void setComplement(String complement) {
        this.complement = complement;
    }

    /**
     * Gets the code postal.
     *
     * @return the codePostal
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * Sets the code postal.
     *
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Gets the commune.
     *
     * @return the commune
     */
    public String getCommune() {
        return this.commune;
    }

    /**
     * Sets the commune.
     *
     * @param commune
     *            the commune to set
     */
    public void setCommune(String commune) {
        this.commune = commune;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public Integer getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

}

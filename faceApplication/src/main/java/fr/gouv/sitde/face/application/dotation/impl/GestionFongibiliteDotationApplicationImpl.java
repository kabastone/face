/**
 *
 */
package fr.gouv.sitde.face.application.dotation.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionFongibiliteDotationApplication;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationListeLiensFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationTransfertFongibiliteDto;
import fr.gouv.sitde.face.application.dto.lov.TransfertFongibiliteLovDto;
import fr.gouv.sitde.face.application.mapping.DotationFongibiliteMapper;
import fr.gouv.sitde.face.application.mapping.TranfertFongibiliteMapper;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.TransfertFongibiliteService;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

/**
 * @author a768251
 *
 */
@Service
@Transactional
public class GestionFongibiliteDotationApplicationImpl implements GestionFongibiliteDotationApplication {

    /** The dotation collectivite service. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The dotation renoncement mapper. */
    @Inject
    private DotationFongibiliteMapper dotationFongibiliteMapper;

    @Inject
    private TransfertFongibiliteService transfertFongibiliteService;

    @Inject
    private TranfertFongibiliteMapper transfertFongibiliteMapper;

    /**
     * Rechercher renoncement dotation par id collectivite et id sous programme.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the dotation renoncement dto
     */

    @Override
    public DotationListeLiensFongibiliteDto rechercherFongibiliteDotationParIdCollectivite(Long idCollectivite) {
        DotationListeLiensFongibiliteDto dotationListeLiensFongibiliteDtoComplet = new DotationListeLiensFongibiliteDto();

        List<SousProgramme> sousProgramme = this.sousProgrammeService.rechercherTousSousProgrammes();
        List<DotationCollectivite> listeDotationCollectivite = this.dotationCollectiviteService
                .rechercherDotationCollectiviteParIdCollectivite(idCollectivite);
        dotationListeLiensFongibiliteDtoComplet.getLienFongibilite()
                .addAll(this.dotationFongibiliteMapper.calculerLiensFongibiliteSousProgrammes(sousProgramme));
        dotationListeLiensFongibiliteDtoComplet.getDotationCollectiviteFongibiliteDto()
                .addAll(this.dotationFongibiliteMapper.dotationCollectivitesToDotationCollectiviteFongibiliteDtos(listeDotationCollectivite));
        return dotationListeLiensFongibiliteDtoComplet;
    }

    @Override
    public DotationCollectiviteFongibiliteDto rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme(Long idCollectivite,
            Integer idSousProgramme) {

        DotationCollectivite dotationCollectivite = this.dotationCollectiviteService
                .rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);

        return this.dotationFongibiliteMapper.dotationCollectiviteToDotationCollectiviteFongibiliteDto(dotationCollectivite);
    }

    @Override
    public DotationTransfertFongibiliteDto enregistrerDotationTransfertFonbilite(DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto) {
        DotationCollectivite dcoDebit = this.dotationCollectiviteService.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(
                dotationTransfertFongibiliteDto.getIdCollectivite(), dotationTransfertFongibiliteDto.getIdSousProgrammeDebiter());

        DotationCollectivite dcoCredit = this.dotationCollectiviteService.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(
                dotationTransfertFongibiliteDto.getIdCollectivite(), dotationTransfertFongibiliteDto.getIdSousProgrammeCrediter());

        TransfertFongibilite transfertFongibilite = new TransfertFongibilite();

        transfertFongibilite.setDotationCollectiviteDestination(dcoCredit);
        transfertFongibilite.setDotationCollectiviteOrigine(dcoDebit);
        transfertFongibilite.setMontant(dotationTransfertFongibiliteDto.getMontantTransfert());
        transfertFongibilite.setDateTransfert(LocalDateTime.now());

        TransfertFongibilite transfertFongibilite2 = this.transfertFongibiliteService.enregistrerTransfert(transfertFongibilite);

        return this.transfertFongibiliteMapper.transfertFongibiliteToDotationTransfertFongibiliteDto(transfertFongibilite2);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionFongibiliteDotationApplication#rechercherTransfertsCollectiviteParAnnee(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<TransfertFongibiliteLovDto> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee) {
        List<TransfertFongibilite> transferts = this.transfertFongibiliteService.rechercherTransfertsCollectiviteParAnnee(idCollectivite, annee);
        return this.transfertFongibiliteMapper.transfertFongibilitesToTransfertFongibiliteLovDtos(transferts);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionFongibiliteDotationApplication#rechercherTransfertsParAnnee(java.lang.Integer)
     */
    @Override
    public List<TransfertFongibiliteLovDto> rechercherTransfertsParAnnee(Integer annee) {
        List<TransfertFongibilite> transferts = this.transfertFongibiliteService.rechercherTransfertsParAnnee(annee);
        return this.transfertFongibiliteMapper.transfertFongibilitesToTransfertFongibiliteLovDtos(transferts);
    }
}

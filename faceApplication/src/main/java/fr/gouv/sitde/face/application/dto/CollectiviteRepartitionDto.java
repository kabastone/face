package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CollectiviteRepartitionDto.
 */
public class CollectiviteRepartitionDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4340708382029184636L;

    /** The id. */
    private Long id;

    /** The nom court. */
    private String nomCourt;

    /** The code departement. */
    private String codeDepartement;

    /** The dotations collectivite. */
    private List<DotationCollectiviteRepartitionParSousProgrammeDto> dotationsCollectivite = new ArrayList<>();

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the nomCourt
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * @param nomCourt
     *            the nomCourt to set
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * @return the dotationsCollectivite
     */
    public List<DotationCollectiviteRepartitionParSousProgrammeDto> getDotationsCollectivite() {
        return this.dotationsCollectivite;
    }

    /**
     * @param dotationsCollectivite
     *            the dotationsCollectivite to set
     */
    public void setDotationsCollectivite(List<DotationCollectiviteRepartitionParSousProgrammeDto> dotationsCollectivite) {
        this.dotationsCollectivite = dotationsCollectivite;
    }

}

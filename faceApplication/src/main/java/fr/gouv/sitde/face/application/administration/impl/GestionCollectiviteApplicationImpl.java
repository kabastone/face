package fr.gouv.sitde.face.application.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CollectiviteDetailDto;
import fr.gouv.sitde.face.application.dto.CollectiviteSimpleDto;
import fr.gouv.sitde.face.application.mapping.CollectiviteMapper;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class GestionCollectiviteApplicationImpl.
 */
@Service
@Transactional
public class GestionCollectiviteApplicationImpl implements GestionCollectiviteApplication {
    /** The collectivite mapper. */
    @Inject
    private CollectiviteMapper collectiviteMapper;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication#rechercherCollectivitesSimple()
     */
    @Override
    public List<CollectiviteSimpleDto> rechercherCollectivitesSimple() {
        List<Collectivite> collectivites = this.collectiviteService.rechercherCollectivites();
        return this.collectiviteMapper.collectivitesToCollectiviteSimpleDtos(collectivites);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication#rechercherCollectiviteDetail(int)
     */
    @Override
    public CollectiviteDetailDto rechercherCollectiviteDetail(Long id) {

        Collectivite collectivite = this.collectiviteService.rechercherCollectiviteParId(id);
        return this.collectiviteMapper.collectiviteToCollectiviteDetailDto(collectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication#modifierCollectivite(fr.gouv.sitde.face.application.dto.
     * CollectiviteDetailDto)
     */
    @Override
    public CollectiviteDetailDto modifierCollectivite(CollectiviteDetailDto collectiviteDto) {

        Collectivite collectivite = this.collectiviteMapper.collectiviteDetailsDtoToCollectivite(collectiviteDto);

        return this.collectiviteMapper.collectiviteToCollectiviteDetailDto(this.collectiviteService.modifierCollectivite(collectivite));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication#creerCollectivite(fr.gouv.sitde.face.application.dto.
     * CollectiviteDetailDto)
     */
    @Override
    public CollectiviteDetailDto creerCollectivite(CollectiviteDetailDto collectiviteDto) {

        Collectivite collectivite = this.collectiviteMapper.collectiviteDetailsDtoToCollectivite(collectiviteDto);

        return this.collectiviteMapper.collectiviteToCollectiviteDetailDto(this.collectiviteService.creerCollectivite(collectivite));
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication#synchroniserCollectivite(java.lang.Long, java.lang.Long)
     */
    @Override
    public void synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention) {
        this.collectiviteService.synchroniserCollectivite(idCollectivite, idDemandeSubvention);

    }

}

/**
 *
 */
package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class TransfertFongibiliteLovDto.
 *
 * @author A754839
 */
public class TransfertFongibiliteLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8061757422881106838L;

    /** The montant. */
    private BigDecimal montant;

    /** The LocalDateTime date du transfert. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateTransfert;

    /** The sous programme crediter. */
    private SousProgrammeLovDto sousProgrammeCrediter;

    /** The sous programme debiter. */
    private SousProgrammeLovDto sousProgrammeDebiter;

    /** The departement. */
    private DepartementLovDto departement;

    /** The collectivite. */
    private CollectiviteLovDto collectivite;

    /**
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * @return the dateTransfert
     */
    public LocalDateTime getDateTransfert() {
        return this.dateTransfert;
    }

    /**
     * @param dateTransfert
     *            the dateTransfert to set
     */
    public void setDateTransfert(LocalDateTime dateTransfert) {
        this.dateTransfert = dateTransfert;
    }

    /**
     * @return the sousProgrammeDebiter
     */
    public SousProgrammeLovDto getSousProgrammeDebiter() {
        return this.sousProgrammeDebiter;
    }

    /**
     * @param sousProgrammeDebiter
     *            the sousProgrammeDebiter to set
     */
    public void setSousProgrammeDebiter(SousProgrammeLovDto sousProgrammeDebiter) {
        this.sousProgrammeDebiter = sousProgrammeDebiter;
    }

    /**
     * @return the departement
     */
    public DepartementLovDto getDepartement() {
        return this.departement;
    }

    /**
     * @param departement
     *            the departement to set
     */
    public void setDepartement(DepartementLovDto departement) {
        this.departement = departement;
    }

    /**
     * @return the collectivite
     */
    public CollectiviteLovDto getCollectivite() {
        return this.collectivite;
    }

    /**
     * @param collectivite
     *            the collectivite to set
     */
    public void setCollectivite(CollectiviteLovDto collectivite) {
        this.collectivite = collectivite;
    }

    /**
     * @return the sousProgrammeCrediter
     */
    public SousProgrammeLovDto getSousProgrammeCrediter() {
        return sousProgrammeCrediter;
    }

    /**
     * @param sousProgrammeCrediter the sousProgrammeCrediter to set
     */
    public void setSousProgrammeCrediter(SousProgrammeLovDto sousProgrammeCrediter) {
        this.sousProgrammeCrediter = sousProgrammeCrediter;
    }
}

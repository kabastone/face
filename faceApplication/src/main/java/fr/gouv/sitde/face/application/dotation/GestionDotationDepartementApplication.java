/**
 *
 */
package fr.gouv.sitde.face.application.dotation;

import java.util.List;

import fr.gouv.sitde.face.application.dto.DotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleTableauPertesRenoncementDto;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Interface GenerationAnnexeDotationApplication.
 *
 * @author A754839
 */
public interface GestionDotationDepartementApplication {

    /**
     * Generation de l'annexe dotation pour un département par pdf.
     *
     * @param idDepartement
     *            l'id du département concerné.
     * @return le FichierTransfert du pdf annexe dotation.
     */
    FichierTransfert genererAnnexeDotationPdf(Integer idDepartement);

    /**
     * Generation de l'annexe dotation pour tous les départements par pdf.
     *
     * @return le FichierTransfert du pdf annexe dotation.
     */
    FichierTransfert genererAnnexeDotationPdf();

    /**
     * Televerser et notifier dotations.
     *
     * @param idDepartement
     *            the id departement
     * @param fichierCourrierDotation
     *            the fichier courrier dotation
     */
    void televerserEtNotifierDotations(Integer idDepartement, FichierTransfert fichierCourrierDotation);

    /**
     * Lister les dotations départementales pour l'année en cours.
     *
     * @return the list
     */
    PageReponse<DotationDepartementaleDto> rechercherDotationsDepartementalesParAnneeEnCours(PageDemande pageDemande);

    /**
     * Lister les lignes de dotation départementale en préparation pour l'année en cours.
     *
     * @param idDepartement
     * @return La liste des lignes de dotation départementale en préparation pour l'année en cours
     */
    List<LigneDotationDepartementaleDto> rechercherLignesDotationDepartementaleEnPreparation(Integer idDepartement);

    /**
     * Lister les lignes de dotation départementale notifiées pour l'année en cours.
     *
     * @param idDepartement
     * @return La liste des lignes de dotation départementale notifiées pour l'année en cours
     */
    List<LigneDotationDepartementaleDto> rechercherLignesDotationDepartementaleNotifiees(Integer idDepartement);

    /**
     * Supprimer une ligne de dotation départementale.
     *
     * @param idLigneDotationDepartementale
     *            the id of ligne de dotation départementale
     */
    void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale);

    /**
     * Ajputer une ligne de dotation départementale.
     *
     * @param ligneDotationDepartementaleDto
     *            ligne de dotation départementale
     */
    LigneDotationDepartementaleDto ajouterLigneDotationDepartementale(LigneDotationDepartementaleDto ligneDotationDepartementaleDto);

    /**
     * Rechercher document notification.
     *
     * @param idLigne
     *            the id ligne
     * @return the document
     */
    Document rechercherDocumentNotification(Long idLigne);

    /**
     * Lister lignes dotations collectivite pertes et renoncement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<LigneDotationDepartementaleTableauPertesRenoncementDto> listerLignesDotationsCollectivitePertesEtRenoncement(Long idCollectivite);

    /**
     * Lister lignes pertes et renoncement.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    List<LigneDotationDepartementaleTableauPertesRenoncementDto> listerLignesPertesEtRenoncement(String codeProgramme);
}

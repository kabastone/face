package fr.gouv.sitde.face.application.commun;

import java.util.List;

import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.application.dto.lov.CollectiviteLovDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandePaiementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandeSubventionLovDto;
import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeAnomalieLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;

/**
 * The Interface GestionReferentielApplication.
 */
public interface GestionReferentielApplication {

    /**
     * Rechercher departements.
     *
     * @return the list
     */
    List<DepartementLovDto> rechercherDepartements();

    /**
     * Rechercher sous programmes.
     *
     * @return the list
     */
    List<SousProgrammeLovDto> rechercherSousProgrammes();

    /**
     * Rechercher departement.
     *
     * @param id
     *            the id
     * @return the collectivite detail dto
     */
    DepartementDto rechercherDepartement(Integer id);

    /**
     * Rechercher sous programmes pour renoncement dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgrammeLovDto> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite);

    /**
     * Rechercher sous programmes de projet.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgrammeLovDto> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite);

    /**
     * Rechercher sous programmes par collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgrammeLovDto> rechercherSousProgrammesParCollectivite(Long idCollectivite);

    /**
     * Rechercher etats demande de subvention.
     *
     * @return the list
     */
    List<EtatDemandeSubventionLovDto> rechercherEtatsDemandeSubvention();

    /**
     * Rechercher collectivites.
     *
     * @return the list
     */
    List<CollectiviteLovDto> rechercherCollectivites();

    /**
     * Rechercher les types de demande de paiement.
     *
     * @return the list
     */
    List<TypeDemandePaiementLovDto> rechercherTypesDemandePaiement();

    /**
     * Lister les types d'anomalie.
     *
     * @return the list
     */
    List<TypeAnomalieLovDto> rechercherTypesAnomaliePourDemandePaiement();

    /**
     * Lister les types d'anomalie.
     *
     * @return the list
     */
    List<TypeAnomalieLovDto> rechercherTypesAnomaliePourDemandeSubvention();

    /**
     * Rechercher collectivites par departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<CollectiviteLovDto> rechercherCollectivitesParDepartement(String codeDepartement);

    /**
     * Recherche sous programmes pour creation de ligne dotation departementale
     *
     * @return the list
     */
    List<SousProgrammeLovDto> rechercherSousProgrammesPourLigneDotationDepartementale();

    /**
     * Rechercher sous programme par id.
     *
     * @param idSousProgramme
     *            the id sous programme
     * @return the sous programme dto
     */
    SousProgrammeDto rechercherSousProgrammeParId(Integer idSousProgramme);

    /**
     * @return
     */
    List<SousProgrammeLovDto> rechercherSousProgrammesDeProjet();

    /**
     * Rechercher etats demande de paiement.
     *
     * @return
     */
    List<EtatDemandePaiementLovDto> rechercherEtatsDemandePaiement();
}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;

/**
 * The Class DemandeProlongationSimpleDto.
 */
public class DemandeProlongationSimpleDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2491156772311105308L;

    /** The id. */
    private Long id;

    /** The code etat demande. */
    private String codeEtatDemande;

    /** The libelle etat demande. */
    private String libelleEtatDemande;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateDemande;

    /** The LocalDateTime achevement demandee. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateAchevementDemandee;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code etat demande.
     *
     * @return the code etat demande
     */
    public String getCodeEtatDemande() {
        return this.codeEtatDemande;
    }

    /**
     * Sets the code etat demande.
     *
     * @param codeEtatDemande
     *            the new code etat demande
     */
    public void setCodeEtatDemande(String codeEtatDemande) {
        this.codeEtatDemande = codeEtatDemande;
    }

    /**
     * Gets the libelle etat demande.
     *
     * @return the libelle etat demande
     */
    public String getLibelleEtatDemande() {
        return this.libelleEtatDemande;
    }

    /**
     * Sets the libelle etat demande.
     *
     * @param libelleEtatDemande
     *            the new libelle etat demande
     */
    public void setLibelleEtatDemande(String libelleEtatDemande) {
        this.libelleEtatDemande = libelleEtatDemande;
    }

    /**
     * Gets the LocalDateTime demande.
     *
     * @return the LocalDateTime demande
     */
    public LocalDateTime getDateDemande() {
        return this.dateDemande;
    }

    /**
     * Sets the LocalDateTime demande.
     *
     * @param dateDemande
     *            the new LocalDateTime demande
     */
    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    /**
     * Gets the LocalDateTime achevement demandee.
     *
     * @return the LocalDateTime achevement demandee
     */
    public LocalDateTime getDateAchevementDemandee() {
        return this.dateAchevementDemandee;
    }

    /**
     * Sets the LocalDateTime achevement demandee.
     *
     * @param dateAchevementDemandee
     *            the new LocalDateTime achevement demandee
     */
    public void setDateAchevementDemandee(LocalDateTime dateAchevementDemandee) {
        this.dateAchevementDemandee = dateAchevementDemandee;
    }
}

package fr.gouv.sitde.face.application.subvention;

import java.util.List;

import fr.gouv.sitde.face.application.dto.DemandePaiementSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeProlongationSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionSimpleDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionDetailDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionTableMferDto;
import fr.gouv.sitde.face.application.dto.ResultatRechercheDossierDto;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;

/**
 * The Interface GestionUtilisateurApplication.
 */
public interface GestionDossierSubventionApplication {

    /**
     * Recherche d'un dossier subvention subvention par ID : avec toutes ses informations, <br/>
     * pour consultation du dossier avec calculs des montants nécessaires.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the utilisateur
     */
    DossierSubventionDetailDto rechercherDossierSubventionParId(Long idDossierSubvention);

    /**
     * Recherche des demandes de subvention via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandeSubventionSimpleDto> rechercherDemandesSubvention(Long idDossierSubvention);

    /**
     * Recherche des demandes de paiement via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandePaiementSimpleDto> rechercherDemandesPaiement(Long idDossierSubvention);

    /**
     * Recherche des demandes de prolongation via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandeProlongationSimpleDto> rechercherDemandesProlongation(Long idDossierSubvention);

    /**
     * Recherche paginée des dossiers de subvention selon les critères en paramètre.
     *
     * @param criteresRecherche
     *            critères de recherche
     * @return la page de réponse correspondante.
     */
    PageReponse<ResultatRechercheDossierDto> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche);

    /**
     * Recherche paginée des dossiers de subvention selon les critères en paramètre.
     *
     * @param criteresRecherche
     *            critères de recherche reçus
     * @return la page de réponse correspondante.
     */
    PageReponse<ResultatRechercheDossierDto> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche);

	List<DossierSubventionTableMferDto> rechercherDossierSubventionCadencementEnDefaut(String codeProgramme);

	List<DossierSubventionTableMferDto> rechercherDossierSubventionTravauxEnRetard(String codeProgramme); 

}

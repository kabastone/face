package fr.gouv.sitde.face.application.subvention;

import java.util.List;

import fr.gouv.sitde.face.application.dto.CadencementDto;
import fr.gouv.sitde.face.application.dto.CadencementTableDto;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;

/**
 * The Interface GestionUtilisateurApplication.
 */
public interface GestionCadencementApplication {

    List<CadencementTableDto> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet);

    /**
     * @param cadencementDto
     * @return
     */
    CadencementDto updateCadencement(CadencementDto cadencementDto);

    /**
     * @param idCollectivite
     * @return
     */
    Boolean estToutCadencementValidePourCollectivite(Long idCollectivite);

    /**
     * @param criteresRecherche
     * @return
     */
    List<ResultatRechercheCadencementDto> rechercherCadencementsParCriteres(CritereRechercheCadencementQo criteresRecherche, String codeProgramme);
}

package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class CollectiviteLovDto.
 */
public class CollectiviteLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5307355238237996139L;

    /** The id. */
    private Long id;

    /** The siret. */
    private String siret;

    /** The nom court. */
    private String nomCourt;

    /** The etat collectivite. */
    private String etatCollectivite;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the siret.
     *
     * @return the siret
     */
    public String getSiret() {
        return this.siret;
    }

    /**
     * Sets the siret.
     *
     * @param siret
     *            the new siret
     */
    public void setSiret(String siret) {
        this.siret = siret;
    }

    /**
     * Gets the nom court.
     *
     * @return the nom court
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * Sets the nom court.
     *
     * @param nomCourt
     *            the new nom court
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * @return the etatCollectivite
     */
    public String getEtatCollectivite() {
        return this.etatCollectivite;
    }

    /**
     * @param etatCollectivite
     *            the etatCollectivite to set
     */
    public void setEtatCollectivite(String etatCollectivite) {
        this.etatCollectivite = etatCollectivite;
    }

}

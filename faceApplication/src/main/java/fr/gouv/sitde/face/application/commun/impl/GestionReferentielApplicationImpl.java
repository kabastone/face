package fr.gouv.sitde.face.application.commun.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.commun.GestionReferentielApplication;
import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.application.dto.lov.CollectiviteLovDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandePaiementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandeSubventionLovDto;
import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeAnomalieLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.application.mapping.DepartementMapper;
import fr.gouv.sitde.face.application.mapping.ReferentielLovMapper;
import fr.gouv.sitde.face.application.mapping.SousProgrammeMapper;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.domaine.service.referentiel.DepartementService;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * The Class GestionReferentielApplicationImpl.
 */
@Service
@Transactional
public class GestionReferentielApplicationImpl implements GestionReferentielApplication {

    /** The referentiel lov mapper. */
    @Inject
    private ReferentielLovMapper referentielLovMapper;

    /** The departement mapper. */
    @Inject
    private DepartementMapper departementMapper;

    /** The sous programme mapper. */
    @Inject
    private SousProgrammeMapper sousProgrammeMapper;

    /** The departement service. */
    @Inject
    private DepartementService departementService;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /** The type anomalie service. */
    @Inject
    private TypeAnomalieService typeAnomalieService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.referentiel.GestionReferentielApplication#listerDepartements()
     */
    @Override
    public List<DepartementLovDto> rechercherDepartements() {

        List<Departement> departements = this.departementService.rechercherDepartements();
        return this.referentielLovMapper.departementsToDepartementsLovDtos(departements);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.referentiel.GestionReferentielApplication#rechercherSousProgrammes()
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammes() {

        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherTousSousProgrammes();
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherDepartement(java.lang.Integer)
     */
    @Override
    public DepartementDto rechercherDepartement(Integer id) {

        Departement departement = this.departementService.rechercherDepartementParId(id);
        return this.departementMapper.departementToDepartementDto(departement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherSousProgrammesPourRenoncementDotation(java.lang.Long)
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite) {

        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherSousProgrammesDeTravauxParCollectivite(idCollectivite);
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherSousProgrammesDeProjetParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite) {

        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherSousProgrammesDeProjetParCollectivite(idCollectivite);
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherSousProgrammesDeProjetParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammesDeProjet() {

        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherSousProgrammesDeProjet();
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherSousProgrammesParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammesParCollectivite(Long idCollectivite) {

        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherSousProgrammesParCollectivite(idCollectivite);
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.referentiel.GestionReferentielApplication#rechercherCollectivites()
     */
    @Override
    public List<CollectiviteLovDto> rechercherCollectivites() {
        List<Collectivite> collectivites = this.collectiviteService.rechercherCollectivites();
        return this.referentielLovMapper.collectivitesToCollectiviteLovDtos(collectivites);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.referentiel.GestionReferentielApplication#rechercherEtatsDossiers()
     */
    @Override
    public List<EtatDemandeSubventionLovDto> rechercherEtatsDemandeSubvention() {

        List<EtatDemandeSubventionLovDto> listeLov = new ArrayList<>(EtatDemandeSubventionEnum.values().length);

        for (final EtatDemandeSubventionEnum etatDemandeEnum : EtatDemandeSubventionEnum.values()) {
            listeLov.add(this.referentielLovMapper.etatDemandeSubventionEnumToEtatDemandeSubventionLovDto(etatDemandeEnum));
        }

        listeLov.sort((EtatDemandeSubventionLovDto etat1, EtatDemandeSubventionLovDto etat2) -> etat1.getCode().compareTo(etat2.getCode()));
        return listeLov;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.referentiel.GestionReferentielApplication#rechercherTypesDemandePaiement()
     */
    @Override
    public List<TypeDemandePaiementLovDto> rechercherTypesDemandePaiement() {
        List<TypeDemandePaiementLovDto> listeLov = new ArrayList<>(TypeDemandePaiementEnum.values().length);

        for (final TypeDemandePaiementEnum typeDemande : TypeDemandePaiementEnum.values()) {
            listeLov.add(this.referentielLovMapper.typeDemandePaiementEnumToTypeDemandePaiementLovDto(typeDemande));
        }

        // Tri des TypeDemandePaiementLovDto par ordre alphabétique
        listeLov.sort((TypeDemandePaiementLovDto etat1, TypeDemandePaiementLovDto etat2) -> etat1.getCode().compareTo(etat2.getCode()));
        return listeLov;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherTypesAnomaliePourDemandePaiement()
     */
    @Override
    public List<TypeAnomalieLovDto> rechercherTypesAnomaliePourDemandePaiement() {
        List<TypeAnomalie> listeTypesAnomalies = this.typeAnomalieService.rechercherTousPourDemandePaiement();
        return this.referentielLovMapper.typeAnomalieToTypeAnomalieLovDtos(listeTypesAnomalies);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherTypesAnomaliePourDemandeSubvention()
     */
    @Override
    public List<TypeAnomalieLovDto> rechercherTypesAnomaliePourDemandeSubvention() {
        List<TypeAnomalie> listeTypesAnomalies = this.typeAnomalieService.rechercherTousPourDemandeSubvention();
        return this.referentielLovMapper.typeAnomalieToTypeAnomalieLovDtos(listeTypesAnomalies);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherCollectivitesParDepartement(java.lang.Integer)
     */
    @Override
    public List<CollectiviteLovDto> rechercherCollectivitesParDepartement(String codeDepartement) {
        List<Collectivite> collectivites = this.collectiviteService.rechercherCollectivitesParCodeDepartement(codeDepartement);
        return this.referentielLovMapper.collectivitesToCollectiviteLovDtos(collectivites);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherSousProgrammesPourLigneDotationDepartementale()
     */
    @Override
    public List<SousProgrammeLovDto> rechercherSousProgrammesPourLigneDotationDepartementale() {
        List<SousProgramme> sousProgrammes = this.sousProgrammeService.rechercherSousProgrammesPourLigneDotationDepartementale();
        return this.referentielLovMapper.sousProgrammesToSousProgrammeLovDtos(sousProgrammes);
    }

    @Override
    public SousProgrammeDto rechercherSousProgrammeParId(Integer idSousProgramme) {
        SousProgramme sousProgramme = this.sousProgrammeService.rechercherSousProgrammeParId(idSousProgramme);
        return this.sousProgrammeMapper.sousProgrammeToSousProgrammeDto(sousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.commun.GestionReferentielApplication#rechercherEtatsDemandePaiement()
     */
    @Override
    public List<EtatDemandePaiementLovDto> rechercherEtatsDemandePaiement() {
        List<EtatDemandePaiementLovDto> listeLov = new ArrayList<>(EtatDemandePaiementEnum.values().length);

        for (final EtatDemandePaiementEnum typeDemande : EtatDemandePaiementEnum.values()) {
            listeLov.add(this.referentielLovMapper.etatDemandePaiementEnumToEtatDemandePaiementLovDto(typeDemande));
        }

        // Tri des EtatDemandeSubventionLovDto par ordre alphabétique
        listeLov.sort((EtatDemandePaiementLovDto etat1, EtatDemandePaiementLovDto etat2) -> etat1.getCode().compareTo(etat2.getCode()));
        return listeLov;
    }
}

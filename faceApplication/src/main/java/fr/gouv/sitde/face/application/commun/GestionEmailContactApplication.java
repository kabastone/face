/**
 *
 */
package fr.gouv.sitde.face.application.commun;

import fr.gouv.sitde.face.transverse.email.EmailContactDto;

/**
 * @author A754839
 *
 */
public interface GestionEmailContactApplication {

    Boolean envoyerEmailContact(EmailContactDto emailContactDto);
}

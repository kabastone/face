/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeProlongationTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.mapping.TableauDeBordMapper;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Class TableauDeBordMferApplicationImpl.
 *
 * @author a453029
 */
@Service
@Transactional
public class TableauDeBordMferApplicationImpl implements TableauDeBordMferApplication {

    /** The tableau de bord mfer service. */
    @Inject
    private TableauDeBordMferService tableauDeBordMferService;

    /** The tableau de bord mapper. */
    @Inject
    private TableauDeBordMapper tableauDeBordMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherTableauDeBordMfer()
     */
    @Override
    public TableauDeBordMferDto rechercherTableauDeBordMfer() {
        return this.tableauDeBordMferService.rechercherTableauDeBordMfer();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesSubventionTdbMferAqualifierSubvention(fr.gouv.sitde
     * .face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAqualifierSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordMferService
                .rechercherDemandesSubventionTdbMferAqualifierSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesPaiementTdbMferAqualifierPaiement(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAqualifierPaiement(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordMferService
                .rechercherDemandesPaiementTdbMferAqualifierPaiement(pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesSubventionTdbMferAaccorderSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAaccorderSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordMferService
                .rechercherDemandesSubventionTdbMferAaccorderSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesPaiementTdbMferAaccorderPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAaccorderPaiement(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordMferService
                .rechercherDemandesPaiementTdbMferAaccorderPaiement(pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesSubventionTdbMferAattribuerSubvention(fr.gouv.sitde
     * .face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAattribuerSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordMferService
                .rechercherDemandesSubventionTdbMferAattribuerSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesSubventionTdbMferAsignalerSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAsignalerSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeSubvention> pageReponseDemandeSubvention = this.tableauDeBordMferService
                .rechercherDemandesSubventionTdbMferAsignalerSubvention(pageDemande);

        // Renvoi page reponse DemandeSubventionTdbDto
        List<DemandeSubventionTdbDto> listeDemandeSubventionDto = this.tableauDeBordMapper
                .demandesSubventionToDemandesSubventionTdbDto(pageReponseDemandeSubvention.getListeResultats());
        return new PageReponse<>(listeDemandeSubventionDto, pageReponseDemandeSubvention.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesPaiementTdbMferAsignalerPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAsignalerPaiement(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandePaiement> pageReponseDemandePaiement = this.tableauDeBordMferService
                .rechercherDemandesPaiementTdbMferAsignalerPaiement(pageDemande);

        // Renvoi page reponse DemandePaiementTdbDto
        List<DemandePaiementTdbDto> listeDemandePaiementDto = this.tableauDeBordMapper
                .demandesPaiementToDemandesPaiementTdbDto(pageReponseDemandePaiement.getListeResultats());
        return new PageReponse<>(listeDemandePaiementDto, pageReponseDemandePaiement.getNbTotalResultats());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication#rechercherDemandesProlongationTdbMferAprolongerSubvention(fr.gouv.
     * sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeProlongationTdbDto> rechercherDemandesProlongationTdbMferAprolongerSubvention(PageDemande pageDemande) {
        // Recherche
        PageReponse<DemandeProlongation> pageReponseDemandeProlongation = this.tableauDeBordMferService
                .rechercherDemandesProlongationTdbMferAprolongerSubvention(pageDemande);

        // Renvoi page reponse DemandeProlongationTdbDto
        List<DemandeProlongationTdbDto> listeDemandeProlongationDto = this.tableauDeBordMapper
                .demandesProlongationToDemandesProlongationTdbDto(pageReponseDemandeProlongation.getListeResultats());
        return new PageReponse<>(listeDemandeProlongationDto, pageReponseDemandeProlongation.getNbTotalResultats());
    }

}

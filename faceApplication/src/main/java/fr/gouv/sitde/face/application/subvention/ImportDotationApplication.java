/**
 *
 */
package fr.gouv.sitde.face.application.subvention;

import java.util.List;

import fr.gouv.sitde.face.application.dto.ImportLigneDotationDto;

/**
 * The Interface ImportDotationApplication.
 *
 * @author Atos
 */
public interface ImportDotationApplication {

    /**
     * Importer dossier dodation.
     *
     * @param listeImportLigneDotationDto
     *            the liste import ligne dotation dto
     * @return the boolean
     */
    Boolean importerDossierDotation(List<ImportLigneDotationDto> listeImportLigneDotationDto);

    /**
     * RG_DOT_212-01.
     *
     * @return the boolean
     */
    Boolean verifierDotationAnnuelleNotifiee();

}

/**
 *
 */
package fr.gouv.sitde.face.application.administration;

import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.application.dto.DepartementDto;

/**
 * The Interface GestionDepartementApplication.
 */
public interface GestionDepartementApplication {

    /**
     * Rechercher adresse pour departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the adresse dto
     */
    AdresseDto rechercherAdressePourDepartement(Integer idDepartement);

    /**
     * Enregistrer adressepour departement.
     *
     * @param idDepartement
     *            the id departement
     * @param adresseDto
     *            the adresse dto
     * @return the adresse dto
     */
    AdresseDto enregistrerAdressepourDepartement(Integer idDepartement, AdresseDto adresseDto);

    /**
     * Enregistrer donnees fiscales departement.
     *
     * @param idDepartement
     *            the id departement
     * @param departement
     *            the departement
     * @return the departement dto
     */
    DepartementDto enregistrerDonneesFiscalesDepartement(Integer idDepartement, DepartementDto departement);
}

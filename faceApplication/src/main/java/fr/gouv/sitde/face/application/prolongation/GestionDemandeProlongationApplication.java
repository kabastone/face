/*
 *
 */
package fr.gouv.sitde.face.application.prolongation;

import fr.gouv.sitde.face.application.dto.DemandeProlongationDto;

/**
 * The Interface GestionDemandeProlongationApplication.
 */
public interface GestionDemandeProlongationApplication {

    /**
     * Rechercher demande prolongation par id.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation dto
     */
    DemandeProlongationDto rechercherDemandeProlongationParId(Long idDemandeProlongation);

    /**
     * Inits the nouvelle demande prolongation.
     *
     * @return the demande subvention details dto
     */
    DemandeProlongationDto initNouvelleDemandeProlongation(Long idDossier);

    /**
     * Creer demande prolongation.
     *
     * @param demandeProlongationDto
     *            the demande prolongation dto
     * @return the demande prolongation dto
     */
    DemandeProlongationDto creerDemandeProlongation(DemandeProlongationDto demandeProlongationDto);

    /**
     * Accorder demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation dto
     */
    DemandeProlongationDto accorderDemandeProlongation(Long idDemandeProlongation);

    /**
     * Refuser demande prolongation.
     *
     * @param demandeDto
     *            the demande dto
     * @return the demande prolongation dto
     */
    DemandeProlongationDto refuserDemandeProlongation(DemandeProlongationDto demandeDto);

}

package fr.gouv.sitde.face.application.dotation;

import fr.gouv.sitde.face.application.dto.DotationProgrammeDto;
import fr.gouv.sitde.face.application.dto.DotationRepartitionNationaleDto;
import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;

/**
 * The Interface GestionDotationNationaleApplication.
 */
public interface GestionDotationNationaleApplication {

    /**
     * Rechercher donnees dotation repartition nationale.
     *
     * @param codeProgramme
     *            the code programme
     * @return the dotation nationale dto
     */
    DotationRepartitionNationaleDto rechercherDonneesDotationRepartitionNationale(String codeProgramme);

    /**
     * Enregistrer montant dotation programme.
     *
     * @param dotationProgrammeDto
     *            the dotation programme dto
     * @return the dotation programme dto
     */
    DotationProgrammeDto enregistrerMontantDotationProgramme(DotationProgrammeDto dotationProgrammeDto);

    /**
     * Enregistrer montant dotation sous programme.
     *
     * @param dotationSousProgrammeMontantBo
     *            the ligne dotation sous programme dto
     * @return the dotation repartition nationale dto
     */
    DotationRepartitionNationaleDto enregistrerMontantDotationSousProgramme(DotationSousProgrammeMontantBo dotationSousProgrammeMontantBo);

}

package fr.gouv.sitde.face.application.dotation.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionDotationAnnuelleApplication;
import fr.gouv.sitde.face.application.dto.DotationAnnuelleDto;
import fr.gouv.sitde.face.application.mapping.DotationAnnuelleMapper;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Class GestionDotationAnnuelleApplicationImpl.
 */
@Service
@Transactional
public class GestionDotationAnnuelleApplicationImpl implements GestionDotationAnnuelleApplication {

    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    @Inject
    private DocumentService documentService;

    @Inject
    private DotationAnnuelleMapper dotationAnnuelleMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationAnnuelleApplication#rechercherDotationAnnuelle(fr.gouv.sitde.face.application.dto.
     * DotationAnnuelleDto)
     */
    @Override
    public DotationAnnuelleDto rechercherDotationAnnuelle(DotationAnnuelleDto dotationAnnuelleDto) {

        List<DotationCollectivite> dotationsCollectiviteProgrammePrincipal = this.dotationCollectiviteService
                .rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(dotationAnnuelleDto.getIdCollectivite(),
                        dotationAnnuelleDto.getAnnee(), "793");

        List<DotationCollectivite> dotationsCollectiviteProgrammeSpecial = this.dotationCollectiviteService
                .rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(dotationAnnuelleDto.getIdCollectivite(),
                        dotationAnnuelleDto.getAnnee(), "794");

        List<Document> documents = this.documentService.rechercherDocumentsDotationParIdCollectiviteEtAnnee(dotationAnnuelleDto.getIdCollectivite(),
                dotationAnnuelleDto.getAnnee());

        return this.dotationAnnuelleMapper.paramsToDotationAnnuelleDto(dotationAnnuelleDto.getIdCollectivite(), dotationAnnuelleDto.getAnnee(),
                documents, dotationsCollectiviteProgrammePrincipal, dotationsCollectiviteProgrammeSpecial);

    }
}

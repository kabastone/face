package fr.gouv.sitde.face.application.mapping;

import java.math.BigDecimal;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.DotationRenoncementDto;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * Mapper entité/DTO des renoncements dotation.
 *
 * @author a627888
 *
 */
@Mapper(componentModel = "spring")
public interface DotationRenoncementMapper {

    /**
     * Dotation collectivite to dotation renoncement dto.
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @return the dotation renoncement dto
     */
    default DotationRenoncementDto dotationCollectiviteToDotationRenoncementDto(DotationCollectivite dotationCollectivite) {

        if (dotationCollectivite == null) {
            return null;
        }

        DotationRenoncementDto dotationRenoncementDto = new DotationRenoncementDto();

        BigDecimal dotationDisponible = dotationCollectivite.getMontant().subtract(dotationCollectivite.getDotationRepartie());
        dotationRenoncementDto.setDotationDisponible(dotationDisponible);

        return dotationRenoncementDto;
    }
}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class CadencementDto.
 */
public class CadencementDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9150712707172609984L;
	
	/** The id. */
	private Long id;
	
	/** The montant annee 1. */
	private BigDecimal montantAnnee1;
	
	/** The montant annee 2. */
	private BigDecimal montantAnnee2;
	
	/** The montant annee 3. */
	private BigDecimal montantAnnee3;
	
	/** The montant annee 4. */
	private BigDecimal montantAnnee4;
	
	/** The montant annee prolongation. */
	private BigDecimal montantAnneeProlongation;
	
	/** The est valide. */
	private Boolean estValide;

	/**
	 * Gets the est valide.
	 *
	 * @return the est valide
	 */
	public Boolean getEstValide() {
		return estValide;
	}

	/**
	 * Sets the est valide.
	 *
	 * @param estValide the new est valide
	 */
	public void setEstValide(Boolean estValide) {
		this.estValide = estValide;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the montant annee 1.
	 *
	 * @return the montant annee 1
	 */
	public BigDecimal getMontantAnnee1() {
		return montantAnnee1;
	}

	/**
	 * Sets the montant annee 1.
	 *
	 * @param montantAnnee1 the new montant annee 1
	 */
	public void setMontantAnnee1(BigDecimal montantAnnee1) {
		this.montantAnnee1 = montantAnnee1;
	}

	/**
	 * Gets the montant annee 2.
	 *
	 * @return the montant annee 2
	 */
	public BigDecimal getMontantAnnee2() {
		return montantAnnee2;
	}

	/**
	 * Sets the montant annee 2.
	 *
	 * @param montantAnnee2 the new montant annee 2
	 */
	public void setMontantAnnee2(BigDecimal montantAnnee2) {
		this.montantAnnee2 = montantAnnee2;
	}

	/**
	 * Gets the montant annee 3.
	 *
	 * @return the montant annee 3
	 */
	public BigDecimal getMontantAnnee3() {
		return montantAnnee3;
	}

	/**
	 * Sets the montant annee 3.
	 *
	 * @param montantAnnee3 the new montant annee 3
	 */
	public void setMontantAnnee3(BigDecimal montantAnnee3) {
		this.montantAnnee3 = montantAnnee3;
	}

	/**
	 * Gets the montant annee 4.
	 *
	 * @return the montant annee 4
	 */
	public BigDecimal getMontantAnnee4() {
		return montantAnnee4;
	}

	/**
	 * Sets the montant annee 4.
	 *
	 * @param montantAnnee4 the new montant annee 4
	 */
	public void setMontantAnnee4(BigDecimal montantAnnee4) {
		this.montantAnnee4 = montantAnnee4;
	}
	
	/**
	 * Gets the montant annee prolongation.
	 *
	 * @return the montant annee prolongation
	 */
	public BigDecimal getMontantAnneeProlongation() {
		return montantAnneeProlongation;
	}

	/**
	 * Sets the montant annee prolongation.
	 *
	 * @param montantAnneeProlongation the new montant annee prolongation
	 */
	public void setMontantAnneeProlongation(BigDecimal montantAnneeProlongation) {
		this.montantAnneeProlongation = montantAnneeProlongation;
	}			

}

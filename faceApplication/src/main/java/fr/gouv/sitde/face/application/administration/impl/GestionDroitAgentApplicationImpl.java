/**
 *
 */
package fr.gouv.sitde.face.application.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication;
import fr.gouv.sitde.face.application.dto.DroitAgentUtilisateurDto;
import fr.gouv.sitde.face.application.mapping.DroitAgentMapper;
import fr.gouv.sitde.face.domaine.service.administration.DroitAgentService;
import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * The Class GestionDroitAgentApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionDroitAgentApplicationImpl implements GestionDroitAgentApplication {

    /** The droit agent service. */
    @Inject
    private DroitAgentService droitAgentService;

    /** The droit agent mapper. */
    @Inject
    private DroitAgentMapper droitAgentMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication#rechercherAgentsParCollectivite(java.lang.Long)
     */
    @Override
    public List<DroitAgentUtilisateurDto> rechercherAgentsParCollectivite(Long idCollectivite) {
        List<DroitAgent> listeDroitsAgents = this.droitAgentService.rechercherDroitsAgentParCollectivite(idCollectivite);
        return this.droitAgentMapper.droitsAgentsToDroitUtilisateurDtos(listeDroitsAgents);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication#ajouterDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public DroitAgentUtilisateurDto ajouterDroitAgent(Long idCollectivite, Long idUtilisateur) {
        DroitAgent droitAgent = this.droitAgentService.ajouterDroitAgent(idCollectivite, idUtilisateur);
        return this.droitAgentMapper.droitAgentToDroitAgentUtilisateurDto(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication#changerEtatAdminDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public DroitAgentUtilisateurDto changerEtatAdminDroitAgent(Long idCollectivite, Long idUtilisateur) {
        DroitAgent droitAgent = this.droitAgentService.modifierEtatAdminDroitAgent(idCollectivite, idUtilisateur);
        return this.droitAgentMapper.droitAgentToDroitAgentUtilisateurDto(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication#supprimerDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public void supprimerDroitAgent(Long idCollectivite, Long idUtilisateur) {
        this.droitAgentService.supprimerDroitAgent(idCollectivite, idUtilisateur);
    }

}

package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationDepartementaleDto.
 */
public class DotationDepartementaleDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1867294428474102722L;

    /**
     * The departement id.
     */
    private Integer idDepartement;

    /**
     * The code departement.
     */
    private String codeDepartement;

    /**
     * The libelle departement.
     */
    private String libelleDepartement;

    /** The en preparation flag. */
    private Boolean enPreparation;

    /** The notifiee flag. */
    private Boolean notifiee;

    /**
     * The somme.
     */
    private BigDecimal somme;

    /**
     * The nonRegroupement.
     */
    private BigDecimal nonRegroupement;

    /**
     * The stock.
     */
    private BigDecimal stock;

    /**
     * The liste dotation par sous-programme.
     */
    private List<DotationDepartementaleParSousProgrammeDto> listeDotationsDepartementalesParSousProgramme = new ArrayList<>();

    /**
     * Gets the id departement.
     *
     * @return the id departement
     */
    public Integer getIdDepartement() {
        return this.idDepartement;
    }

    /**
     * Sets the id departement.
     *
     * @param idDepartement
     *            the new id departement
     */
    public void setIdDepartement(Integer idDepartement) {
        this.idDepartement = idDepartement;
    }

    /**
     * Gets the code departement.
     *
     * @return the code departement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement
     *            the new code departement
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Gets the libelle departement.
     *
     * @return the libelle departement
     */
    public String getLibelleDepartement() {
        return this.libelleDepartement;
    }

    /**
     * Sets the libelle departement.
     *
     * @param libelleDepartement
     *            the new libelle departement
     */
    public void setLibelleDepartement(String libelleDepartement) {
        this.libelleDepartement = libelleDepartement;
    }

    /**
     * Gets the en preparation.
     *
     * @return the en preparation
     */
    public Boolean getEnPreparation() {
        return this.enPreparation;
    }

    /**
     * Sets the en preparation.
     *
     * @param enPreparation
     *            the new en preparation
     */
    public void setEnPreparation(Boolean enPreparation) {
        this.enPreparation = enPreparation;
    }

    /**
     * Gets the notifiee.
     *
     * @return the notifiee
     */
    public Boolean getNotifiee() {
        return this.notifiee;
    }

    /**
     * Sets the notifiee.
     *
     * @param notifiee
     *            the new notifiee
     */
    public void setNotifiee(Boolean notifiee) {
        this.notifiee = notifiee;
    }

    /**
     * Gets the somme.
     *
     * @return the somme
     */
    public BigDecimal getSomme() {
        return this.somme;
    }

    /**
     * Sets the somme.
     *
     * @param somme
     *            the new somme
     */
    public void setSomme(BigDecimal somme) {
        this.somme = somme;
    }

    /**
     * Gets the non regroupement.
     *
     * @return the non regroupement
     */
    public BigDecimal getNonRegroupement() {
        return this.nonRegroupement;
    }

    /**
     * Sets the non regroupement.
     *
     * @param nonRegroupement
     *            the new non regroupement
     */
    public void setNonRegroupement(BigDecimal nonRegroupement) {
        this.nonRegroupement = nonRegroupement;
    }

    /**
     * Gets the stock.
     *
     * @return the stock
     */
    public BigDecimal getStock() {
        return this.stock;
    }

    /**
     * Sets the stock.
     *
     * @param stock
     *            the new stock
     */
    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    /**
     * Gets the liste dotations departementales par sous programme.
     *
     * @return the liste dotations departementales par sous programme
     */
    public List<DotationDepartementaleParSousProgrammeDto> getListeDotationsDepartementalesParSousProgramme() {
        return this.listeDotationsDepartementalesParSousProgramme;
    }

    /**
     * Sets the liste dotations departementales par sous programme.
     *
     * @param listeDotationsDepartementalesParSousProgramme
     *            the new liste dotations departementales par sous programme
     */
    public void setListeDotationsDepartementalesParSousProgramme(
            List<DotationDepartementaleParSousProgrammeDto> listeDotationsDepartementalesParSousProgramme) {
        this.listeDotationsDepartementalesParSousProgramme.addAll(listeDotationsDepartementalesParSousProgramme);
    }
}

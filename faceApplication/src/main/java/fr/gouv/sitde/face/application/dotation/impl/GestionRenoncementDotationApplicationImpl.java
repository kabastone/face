package fr.gouv.sitde.face.application.dotation.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionRenoncementDotationApplication;
import fr.gouv.sitde.face.application.dto.DotationRenoncementDto;
import fr.gouv.sitde.face.application.mapping.DotationRenoncementMapper;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationRenoncementService;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Class GestionRenoncementDotationApplicationImpl.
 */
@Service
@Transactional
public class GestionRenoncementDotationApplicationImpl implements GestionRenoncementDotationApplication {

    /** The dotation collectivite service. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The dotation renoncement mapper. */
    @Inject
    private DotationRenoncementMapper dotationRenoncementMapper;

    /** The dotation renoncement service. */
    @Inject
    private DotationRenoncementService dotationRenoncementService;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionRenoncementDotationApplication#rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme(
     * java.lang.Long, java.lang.Integer)
     */
    @Override
    public DotationRenoncementDto rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme(Long idCollectivite, Integer idSousProgramme) {

        DotationCollectivite dotationCollectivite = this.dotationCollectiviteService
                .rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);

        return this.dotationRenoncementMapper.dotationCollectiviteToDotationRenoncementDto(dotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionRenoncementDotationApplication#renoncerDotation(
     * fr.gouv.sitde.face.application.dto.DotationRenoncementDto)
     */
    @Override
    public DotationRenoncementDto renoncerDotation(DotationRenoncementDto dotationRenoncementDto) {

        this.dotationRenoncementService.renoncerDotation(dotationRenoncementDto.getIdCollectivite(), dotationRenoncementDto.getIdSousProgramme(),
                dotationRenoncementDto.getMontant(), dotationRenoncementDto.getFichierEnvoyer());

        return dotationRenoncementDto;
    }
}

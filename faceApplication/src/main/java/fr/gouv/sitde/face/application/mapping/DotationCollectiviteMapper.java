package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import fr.gouv.sitde.face.application.dto.CollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionParSousProgrammeDto;
import fr.gouv.sitde.face.application.dto.DotationDepartementDto;
import fr.gouv.sitde.face.transverse.dotation.CollectiviteRepartitionBo;
import fr.gouv.sitde.face.transverse.dotation.DotationCollectiviteRepartitionParSousProgrammeBo;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

@Mapper(uses = { DocumentMapper.class }, componentModel = "spring")
public interface DotationCollectiviteMapper {

    DotationCollectiviteRepartitionDto paramsToDotationCollectiviteRepartitionDto(String codeDepartement,
            List<DotationDepartement> dotationsDepartement, List<Document> documents, List<CollectiviteRepartitionBo> collectivitesRepartition);

    @Mapping(target = "abreviationSousProgramme", source = "dotationSousProgramme.sousProgramme.abreviation")
    @Mapping(target = "descriptionSousProgramme", source = "dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "numeroSousProgramme", source = "dotationSousProgramme.sousProgramme", qualifiedByName = "numeroSousProgramme")
    DotationDepartementDto dotationDepartementToDotationDepartementaleParSousProgrammeDto(DotationDepartement dotationDepartement);

    @Named("numeroSousProgramme")
    default String donnerNumeroSousProgramme(SousProgramme sp) {
        return sp.getAbreviation().equals(ConstantesFace.CODE_SP_SECURISATION) ? ConstantesFace.NUMERO_SP_SECURISATION : sp.getNumSousAction();
    }

    List<DotationDepartementDto> dotationDepartementToDotationDepartementaleParSousProgrammeDto(List<DotationDepartement> dotationsDepartement);

    CollectiviteRepartitionDto collectiviteRepartitionBoToCollectiviteRepartitionDto(CollectiviteRepartitionBo collectiviteRepartitionBo);

    CollectiviteRepartitionBo collectiviteRepartitionDtoToCollectiviteRepartitionBo(CollectiviteRepartitionDto collectiviteRepartitionDto);

    List<CollectiviteRepartitionDto> collectiviteRepartitionBoToCollectiviteRepartitionDto(
            List<CollectiviteRepartitionBo> collectivitesRepartitionBo);

    DotationCollectiviteRepartitionParSousProgrammeDto dotationCollectiviteRepartitionParSousProgrammeBoToDotationCollectiviteRepartitionParSousProgrammeDto(
            DotationCollectiviteRepartitionParSousProgrammeDto dotationCollectiviteRepartitionParSousProgrammeBo);

    List<DotationCollectiviteRepartitionParSousProgrammeDto> dotationCollectiviteRepartitionParSousProgrammeDTOToDotationCollectiviteRepartitionParSousProgrammeDto(
            List<DotationCollectiviteRepartitionParSousProgrammeBo> dotationsCollectiviteRepartitionParSousProgrammeBo);

}

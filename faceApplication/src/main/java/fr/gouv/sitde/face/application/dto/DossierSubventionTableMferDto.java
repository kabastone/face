package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * The Class DossierSubventionDefautTableDto.
 */
public class DossierSubventionTableMferDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7012130526423662653L;

    /** The departement. */
    private String departementCode;

    /** The departement. */
    private String departementNom;

    /** The collectivite. */
    private String collectiviteNomCourt;

    /** The num dossier. */
    private String numDossier;

    /** The id dossier. */
    private Long idDossier;

    /** The dossier num EJ. */
    private String dossierNumEJ;

    /**
     * Gets the num dossier.
     *
     * @return the num dossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the new num dossier
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the id dossier.
     *
     * @return the id dossier
     */
    public Long getIdDossier() {
        return this.idDossier;
    }

    /**
     * Sets the id dossier.
     *
     * @param idDossier
     *            the new id dossier
     */
    public void setIdDossier(Long idDossier) {
        this.idDossier = idDossier;
    }

    /**
     * Gets the dossier num EJ.
     *
     * @return the dossier num EJ
     */
    public String getDossierNumEJ() {
        return this.dossierNumEJ;
    }

    /**
     * Sets the dossier num EJ.
     *
     * @param dossierNumEJ
     *            the new dossier num EJ
     */
    public void setDossierNumEJ(String dossierNumEJ) {
        this.dossierNumEJ = dossierNumEJ;
    }

    /**
     * Gets the departement code.
     *
     * @return the departement code
     */
    public String getDepartementCode() {
        return this.departementCode;
    }

    /**
     * Sets the departement code.
     *
     * @param departementCode
     *            the new departement code
     */
    public void setDepartementCode(String departementCode) {
        this.departementCode = departementCode;
    }

    /**
     * Gets the collectivite nom court.
     *
     * @return the collectivite nom court
     */
    public String getCollectiviteNomCourt() {
        return this.collectiviteNomCourt;
    }

    /**
     * Sets the collectivite nom court.
     *
     * @param collectiviteNomCourt
     *            the new collectivite nom court
     */
    public void setCollectiviteNomCourt(String collectiviteNomCourt) {
        this.collectiviteNomCourt = collectiviteNomCourt;
    }

    /**
     * Gets the departement nom.
     *
     * @return the departement nom
     */
    public String getDepartementNom() {
        return this.departementNom;
    }

    /**
     * Sets the departement nom.
     *
     * @param departementNom
     *            the new departement nom
     */
    public void setDepartementNom(String departementNom) {
        this.departementNom = departementNom;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.application.jsonutils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Class CustomTypeDotationDepartementDeserializer.
 *
 * @author A754839
 */
public class CustomTypeDotationDepartementDeserializer extends StdDeserializer<TypeDotationDepartementEnum> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5354911420674622415L;

    public CustomTypeDotationDepartementDeserializer() {
        super(TypeDotationDepartementEnum.class);
    }

    protected CustomTypeDotationDepartementDeserializer(Class<TypeDotationDepartementEnum> t) {
        super(t);
    }

    @Override
    public TypeDotationDepartementEnum deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {

        try {
            return TypeDotationDepartementEnum.rechercherEnumParCode(jsonParser.getText());
        } catch (IOException e) {
            throw new TechniqueException("Problème lors de la deserialization de TypeDotationDepartementEnum", e);
        }

    }
}

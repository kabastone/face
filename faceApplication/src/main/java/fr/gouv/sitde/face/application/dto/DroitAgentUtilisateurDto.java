package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * Classe des droits agent d'un utilisateur.
 */
public class DroitAgentUtilisateurDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3202951084028740375L;

    /** The id. */
    private Long idUtilisateur;

    /** The email. */
    private String email;

    /** The nom. */
    private String nom;

    /** The prenom. */
    private String prenom;

    /** The telephone. */
    private String telephone;

    /** The admin. */
    private boolean admin;

    /** The id collectivite. */
    private Long idCollectivite;

    /**
     * Gets the id utilisateur.
     *
     * @return the idUtilisateur
     */
    public Long getIdUtilisateur() {
        return this.idUtilisateur;
    }

    /**
     * Sets the id utilisateur.
     *
     * @param idUtilisateur
     *            the idUtilisateur to set
     */
    public void setIdUtilisateur(Long idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets the prenom.
     *
     * @param prenom
     *            the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets the telephone.
     *
     * @return the telephone
     */
    public String getTelephone() {
        return this.telephone;
    }

    /**
     * Sets the telephone.
     *
     * @param telephone
     *            the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Checks if is admin.
     *
     * @return the admin
     */
    public boolean isAdmin() {
        return this.admin;
    }

    /**
     * Sets the admin.
     *
     * @param admin
     *            the admin to set
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }
}

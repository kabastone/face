package fr.gouv.sitde.face.application.dotation;

import fr.gouv.sitde.face.application.dto.CollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface GestionDotationCollectiviteApplication.
 */
public interface GestionDotationCollectiviteApplication {

    /**
     * Rechercher dotation collectivite repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @return the dotation collectivite repartition dto
     */
    DotationCollectiviteRepartitionDto rechercherDotationCollectiviteRepartition(String codeDepartement);

    /**
     * Televerser document courrier repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @param fichierTransfert
     *            the fichier transfert
     * @return the dotation collectivite repartition dto
     */
    DotationCollectiviteRepartitionDto televerserDocumentCourrierRepartition(String codeDepartement, FichierTransfert fichierTransfert);

    /**
     * Repartir dotation collectivite.
     *
     * @param collectiviteRepartitionDto
     *            the collectivite repartition dto
     * @return the dotation collectivite repartition dto
     */
    DotationCollectiviteRepartitionDto repartirDotationCollectivite(CollectiviteRepartitionDto collectiviteRepartitionDto);

}

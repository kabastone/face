/**
 *
 */
package fr.gouv.sitde.face.application.dotation;

import java.util.List;

import fr.gouv.sitde.face.application.dto.DotationCollectiviteFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationListeLiensFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationTransfertFongibiliteDto;
import fr.gouv.sitde.face.application.dto.lov.TransfertFongibiliteLovDto;

/**
 * @author a768251
 *
 */
public interface GestionFongibiliteDotationApplication {

    DotationListeLiensFongibiliteDto rechercherFongibiliteDotationParIdCollectivite(Long idCollectivite);

    /**
     * @param idCollectivite
     * @param idSousProgramme
     * @return
     */
    DotationCollectiviteFongibiliteDto rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme(Long idCollectivite, Integer idSousProgramme);

    /**
     * @param DotationTransfertFongibiliteDto
     *
     * @return DotationTransfertFongibiliteDto
     */
    DotationTransfertFongibiliteDto enregistrerDotationTransfertFonbilite(DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto);

    /**
     * Rechercher transferts collectivite par annee.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the page reponse
     */
    List<TransfertFongibiliteLovDto> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee);

    /**
     * @param annee
     * @return
     */
    List<TransfertFongibiliteLovDto> rechercherTransfertsParAnnee(Integer annee);

}

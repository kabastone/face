/**
 *
 */
package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class TypeDemandePaiementLovDto.
 *
 * @author Atos
 */
public class TypeDemandePaiementLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6607611118108957784L;

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}

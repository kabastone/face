/**
 *
 */
package fr.gouv.sitde.face.application.administration.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.administration.GestionDepartementApplication;
import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.application.mapping.AdresseMapper;
import fr.gouv.sitde.face.application.mapping.DepartementMapper;
import fr.gouv.sitde.face.domaine.service.administration.AdresseService;
import fr.gouv.sitde.face.domaine.service.referentiel.DepartementService;
import fr.gouv.sitde.face.transverse.entities.Adresse;
import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * The Class GestionDepartementApplicationImpl.
 */
@Service
@Transactional
public class GestionDepartementApplicationImpl implements GestionDepartementApplication {

    /** The adresse mapper. */
    @Inject
    private AdresseMapper adresseMapper;

    @Inject
    private DepartementMapper departementMapper;

    @Inject
    private DepartementService departementService;

    /** The adresse service. */
    @Inject
    private AdresseService adresseService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDepartementApplication#rechercherAdressePourDepartement(java.lang.Integer)
     */
    @Override
    public AdresseDto rechercherAdressePourDepartement(Integer idDepartement) {
        Adresse adresse = this.adresseService.rechercherAdresseParDepartement(idDepartement);
        return this.adresseMapper.adresseToAdresseDto(adresse);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDepartementApplication#enregistrerAdressepourDepartement(java.lang.Integer,
     * fr.gouv.sitde.face.application.dto.AdresseDto)
     */
    @Override
    public AdresseDto enregistrerAdressepourDepartement(Integer idDepartement, AdresseDto adresseDto) {
        Adresse adresse = this.adresseMapper.adresseDtoToAdresse(adresseDto);
        adresse = this.adresseService.enregistrerAdressePourDepartement(idDepartement, adresse);
        return this.adresseMapper.adresseToAdresseDto(adresse);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.administration.GestionDepartementApplication#enregistrerDonneesFiscalesDepartement(java.lang.Integer,
     * fr.gouv.sitde.face.application.dto.DepartementDto)
     */
    @Override
    public DepartementDto enregistrerDonneesFiscalesDepartement(Integer idDepartement, DepartementDto departementDto) {
        Departement departement = this.departementMapper.departementDtoToDepartement(departementDto);
        departement = this.departementService.enregistrerDonneesFiscalesDepartement(departement);
        return this.departementMapper.departementToDepartementDto(departement);
    }

}

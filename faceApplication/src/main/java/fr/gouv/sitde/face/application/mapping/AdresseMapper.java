package fr.gouv.sitde.face.application.mapping;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.transverse.entities.Adresse;

/**
 * The Interface AdresseMapper.
 */
@Mapper(componentModel = "spring")
public interface AdresseMapper {

    /**
     * Adresse to adresse dto.
     *
     * @param adresse
     *            the adresse
     * @return the adresse dto
     */
    AdresseDto adresseToAdresseDto(Adresse adresse);

    /**
     * Adresse dto to adresse.
     *
     * @param adresseDto
     *            the adresse dto
     * @return the adresse
     */
    Adresse adresseDtoToAdresse(AdresseDto adresseDto);
}

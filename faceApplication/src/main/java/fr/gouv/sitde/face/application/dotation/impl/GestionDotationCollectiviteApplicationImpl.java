package fr.gouv.sitde.face.application.dotation.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dotation.GestionDotationCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.mapping.DotationCollectiviteMapper;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.transverse.dotation.CollectiviteRepartitionBo;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Class GestionDotationCollectiviteApplicationImpl.
 */
@Service
@Transactional
public class GestionDotationCollectiviteApplicationImpl implements GestionDotationCollectiviteApplication {

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The dotation departement service. */
    @Inject
    private DotationDepartementService dotationDepartementService;

    /** The dotation collectivite service. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The courrier annuel departement service. */
    @Inject
    private CourrierAnnuelDepartementService courrierAnnuelDepartementService;

    /** The dotation collectivite mapper. */
    @Inject
    private DotationCollectiviteMapper dotationCollectiviteMapper;

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationCollectiviteApplication#rechercherDotationCollectiviteRepartition(java.lang.String)
     */
    @Override
    public DotationCollectiviteRepartitionDto rechercherDotationCollectiviteRepartition(String codeDepartement) {

        List<DotationDepartement> dotationsDepartement = this.dotationDepartementService
                .rechercherDotationDepartementPourRepartition(codeDepartement);

        List<Document> documents = this.documentService.rechercherDocumentsDotationParCodeDepartement(codeDepartement);

        List<CollectiviteRepartitionBo> collectivitesRepartitionBo = this.dotationCollectiviteService
                .rechercherDotationsCollectivitesPourRepartition(codeDepartement);

        return this.dotationCollectiviteMapper.paramsToDotationCollectiviteRepartitionDto(codeDepartement, dotationsDepartement, documents,
                collectivitesRepartitionBo);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.dotation.GestionDotationCollectiviteApplication#televerserDocumentCourrierRepartition(java.lang.String,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public DotationCollectiviteRepartitionDto televerserDocumentCourrierRepartition(String codeDepartement, FichierTransfert fichierTransfert) {

        // Téleversement du fichier
        this.courrierAnnuelDepartementService.televerserEtCreerDocumentCourrierRepartition(codeDepartement, fichierTransfert);

        return this.rechercherDotationCollectiviteRepartition(codeDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.dotation.GestionDotationCollectiviteApplication#repartirDotationCollectivite(fr.gouv.sitde.face.application.dto.
     * CollectiviteRepartitionDto)
     */
    @Override
    public DotationCollectiviteRepartitionDto repartirDotationCollectivite(CollectiviteRepartitionDto collectiviteRepartitionDto) {

        CollectiviteRepartitionBo collectiviteRepartitionBo = this.dotationCollectiviteMapper
                .collectiviteRepartitionDtoToCollectiviteRepartitionBo(collectiviteRepartitionDto);

        // Répartition
        this.dotationCollectiviteService.repartirDotationCollectivite(collectiviteRepartitionBo);

        return this.rechercherDotationCollectiviteRepartition(collectiviteRepartitionDto.getCodeDepartement());
    }
}

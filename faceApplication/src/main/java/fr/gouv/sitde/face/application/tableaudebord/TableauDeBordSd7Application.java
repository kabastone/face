/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Interface TableauDeBordSd7Application.
 *
 * @author a453029
 */
public interface TableauDeBordSd7Application {

    /**
     * Rechercher tableau de bord sd 7.
     *
     * @return the tableau de bord sd 7 dto
     */
    TableauDeBordSd7Dto rechercherTableauDeBordSd7();

    /**
     * Rechercher demandes subvention tdb sd 7 acontroler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbSd7AcontrolerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb sd 7 acontroler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbSd7AcontrolerPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb sd 7 atransferer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbSd7AtransfererSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb sd 7 atransferer paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbSd7AtransfererPaiement(PageDemande pageDemande);

}

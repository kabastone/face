/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.AdresseEmailDto;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * Mapper entité/DTO de la liste de diffusion.
 *
 * @author Atos
 */
@Mapper(componentModel = "spring")
public interface ListeDiffusionMapper {

    /**
     * Adresse email to liste diffusion dto.
     *
     * @param adresseEmail
     *            the adresse email
     * @return the liste diffusion dto
     */
    @Mapping(target = "idCollectivite", source = "collectivite.id")
    @Mapping(target = "email", source = "adresse")
    AdresseEmailDto adresseEmailToListeDiffusionDto(AdresseEmail adresseEmail);

    /**
     * Adresse emails to liste diffusions dto.
     *
     * @param listeAdressesEmail
     *            the liste adresses email
     * @return the list
     */
    List<AdresseEmailDto> adresseEmailsToListeDiffusionsDto(List<AdresseEmail> listeAdressesEmail);
}

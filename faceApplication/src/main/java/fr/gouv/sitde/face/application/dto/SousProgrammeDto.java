package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * The Class SousProgrammeDto.
 */
public class SousProgrammeDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1867294428474102722L;

    /** The abreviation. */
    private String code;

    /** The description. */
    private String name;

    /** The code domaine fonctionnel. */
    private String codeDomaineFonctionnel;

    /** The de projet. */
    private boolean deProjet;

    /**
     * Instantiates a new sous programme dto.
     */
    public SousProgrammeDto() {
    }

    /**
     * Instantiates a new sous programme dto.
     *
     * @param code
     *            the code
     * @param name
     *            the name
     */
    public SousProgrammeDto(String code, String name, String codeDomaineFonctionnel) {
        this.code = code;
        this.name = name;
        this.codeDomaineFonctionnel = codeDomaineFonctionnel;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the code domaine fonctionnel.
     *
     * @return the codeDomaineFonctionnel
     */
    public String getCodeDomaineFonctionnel() {
        return this.codeDomaineFonctionnel;
    }

    /**
     * Sets the code domaine fonctionnel.
     *
     * @param codeDomaineFonctionnel
     *            the codeDomaineFonctionnel to set
     */
    public void setCodeDomaineFonctionnel(String codeDomaineFonctionnel) {
        this.codeDomaineFonctionnel = codeDomaineFonctionnel;
    }

    /**
     * @return the deProjet
     */
    public boolean isDeProjet() {
        return this.deProjet;
    }

    /**
     * @param deProjet
     *            the deProjet to set
     */
    public void setDeProjet(boolean deProjet) {
        this.deProjet = deProjet;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.application.jsonutils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

import java.io.IOException;

/**
 * The Class CustomTypeDotationDepartementSerializer.
 *
 * @author A754839
 */
public class CustomTypeDotationDepartementSerializer extends StdSerializer<TypeDotationDepartementEnum> {


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5354911420674622415L;

    public CustomTypeDotationDepartementSerializer() {
        super(TypeDotationDepartementEnum.class);
    }

    protected CustomTypeDotationDepartementSerializer(Class<TypeDotationDepartementEnum> t) {
        super(t);
    }

    @Override
    public void serialize(TypeDotationDepartementEnum typeDotationDepartementEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

                try {
                    jsonGenerator.writeString(typeDotationDepartementEnum.getCode());
        } catch (IOException e) {
            throw new TechniqueException("Problème lors de la serialization de TypeDotationDepartementEnum", e);
        }

    }

}

/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.entities.Utilisateur;

/**
 * Mapper entité/DTO des utilisateurs.
 *
 * @author a453029
 *
 */
@Mapper(uses = { DroitAgentMapper.class }, componentModel = "spring")
public interface UtilisateurMapper {

    /**
     * Utilisateur to utilisateur admin dto.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur admin dto
     */
    @Mapping(target = "droitsAgentCollectivites", source = "utilisateur.droitsAgent")
    UtilisateurAdminDto utilisateurToUtilisateurAdminDto(Utilisateur utilisateur);

    /**
     * Utilisateur admin dto to utilisateur.
     *
     * @param utilisateurAdministrationDto
     *            the utilisateur administration dto
     * @return the utilisateur
     */
    @Mapping(target = "droitsAgent", source = "utilisateurAdminDto.droitsAgentCollectivites")
    Utilisateur utilisateurAdminDtoToUtilisateur(UtilisateurAdminDto utilisateurAdminDto);

    /**
     * Utilisateurs to utilisateur admin dtos.
     *
     * @param utilisateurs
     *            the utilisateurs
     * @return the list
     */
    List<UtilisateurAdminDto> utilisateursToUtilisateurAdminDtos(List<Utilisateur> utilisateurs);

}

/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import fr.gouv.sitde.face.application.dto.DemandeSubventionDetailsDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionSimpleDto;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * Mapper entité/DTO des demandes de subvention.
 *
 * @author Atos
 *
 */
@Mapper(uses = { AnomalieMapper.class, DocumentMapper.class, DossierSubventionMapper.class, EtatDemandeSubventionMapper.class,
        CadencementMapper.class }, componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface DemandeSubventionMapper {

    /**
     * Demande subvention to demande subvention simple dto.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention simple dto
     */
    @Mapping(target = "codeEtatDemande", source = "etatDemandeSubvention.code")
    @Mapping(target = "libelleEtatDemande", source = "etatDemandeSubvention.libelle")
    DemandeSubventionSimpleDto demandeSubventionToDemandeSubventionSimpleDto(DemandeSubvention demandeSubvention);

    /**
     * Demandes subvention to demandes subvention simple dto.
     *
     * @param demandesSubvention
     *            the demandes subvention
     * @return the list
     */
    List<DemandeSubventionSimpleDto> demandesSubventionToDemandesSubventionSimpleDto(List<DemandeSubvention> listeDemandesSubvention);

    /**
     * Demande subvention to demande subvention details dto.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention details dto
     */
    @Mapping(target = "dateJour", source = "dateDemande")
    @Mapping(target = "montantHTTravauxEligibles", source = "montantTravauxEligible")
    @Mapping(target = "tauxAideFacePercent", source = "tauxAide")
    @Mapping(target = "message", source = "messageComplementaire")
    @Mapping(target = "etat", source = "etatDemandeSubvention.code")
    @Mapping(target = "etatLibelle", source = "etatDemandeSubvention.libelle")
    @Mapping(target = "numDossier", source = "dossierSubvention.numDossier")
    @Mapping(target = "motifRefus", source = "motifRefus")
    @Mapping(target = "anomalies", source = "anomalies")
    @Mapping(target = "idDotationCollectivite", source = "dossierSubvention.dotationCollectivite.id")
    @Mapping(target = "idCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.id")
    @Mapping(target = "idDossier", source = "dossierSubvention.id")
    @Mapping(target = "sousProgramme",
            source = "dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.description")
    @Mapping(target = "idSousProgramme", source = "dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id")
    @Mapping(target = "nomLongCollectivite", source = "dossierSubvention.dotationCollectivite.collectivite.nomLong")
    @Mapping(target = "codeDepartement", source = "dossierSubvention.dotationCollectivite.dotationDepartement.departement.code")
    @Mapping(target = "cadencementDto", source = "cadencement")
    DemandeSubventionDetailsDto demandeSubventionToDemandeSubventionDetailsDto(DemandeSubvention demandeSubvention);

    /**
     * Demande subvention details dto to demande subvention.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention
     */
    @Mapping(target = "dateDemande", source = "dateJour")
    @Mapping(target = "montantTravauxEligible", source = "montantHTTravauxEligibles")
    @Mapping(target = "tauxAide", source = "tauxAideFacePercent")
    @Mapping(target = "messageComplementaire", source = "message")
    @Mapping(target = "etatDemandeSubvention.code", source = "etat")
    @Mapping(target = "dossierSubvention.numDossier", source = "numDossier")
    @Mapping(target = "dossierSubvention.dotationCollectivite.id", source = "idDotationCollectivite")
    @Mapping(target = "dossierSubvention.dotationCollectivite.collectivite.id", source = "idCollectivite")
    @Mapping(target = "dossierSubvention.id", source = "idDossier")
    @Mapping(target = "dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id", source = "idSousProgramme")
    @Mapping(target = "cadencement", source = "cadencementDto")
    DemandeSubvention demandeSubventionDetailsDtoToDemandeSubvention(DemandeSubventionDetailsDto demandeSubventionDto);

    /**
     * Demande subvention details dto to demande subvention.
     *
     * @param demandeSubventionDto
     *            the demande subvention dto
     * @param demandeSubvention
     *            the demande subvention
     * @return the demande subvention
     */
    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "dateDemande", source = "dateJour")
    @Mapping(target = "montantTravauxEligible", source = "montantHTTravauxEligibles")
    @Mapping(target = "tauxAide", source = "tauxAideFacePercent")
    @Mapping(target = "messageComplementaire", source = "message")
    @Mapping(target = "etatDemandeSubvention.code", source = "etat")
    @Mapping(target = "etatDemandeSubvention.libelle", source = "etatLibelle")
    @Mapping(target = "dossierSubvention.numDossier", source = "numDossier")
    @Mapping(target = "motifRefus", source = "motifRefus")
    @Mapping(target = "anomalies", source = "anomalies")
    @Mapping(target = "dossierSubvention.dotationCollectivite.id", source = "idDotationCollectivite")
    @Mapping(target = "dossierSubvention.dotationCollectivite.collectivite.id", source = "idCollectivite")
    @Mapping(target = "dossierSubvention.id", source = "idDossier")
    @Mapping(target = "dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.description",
            source = "sousProgramme")
    @Mapping(target = "dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id", source = "idSousProgramme")
    @Mapping(target = "cadencement", source = "cadencementDto")
    DemandeSubvention updateDemandeSubventionFromDemandeSubventionDetailsDto(DemandeSubventionDetailsDto demandeSubventionDto,
            @MappingTarget DemandeSubvention demandeSubvention);

}

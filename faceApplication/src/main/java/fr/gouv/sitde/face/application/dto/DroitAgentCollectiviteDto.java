package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;

public class DroitAgentCollectiviteDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7744395061062866642L;

    /** The idCollectivite. */
    private Long idCollectivite;

    /** The siret. */
    private String siret;

    /** The nom court. */
    private String nomCourt;

    /** The nom long. */
    private String nomLong;

    /** The etat collectivite. */
    private EtatCollectiviteEnum etatCollectivite;

    /** The admin. */
    private boolean admin;

    /** The id. */
    private Long idUtilisateur;

    /**
     * Gets the idCollectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the idCollectivite.
     *
     * @param idCollectivite
     *            the new idCollectivite
     */
    public void setIdCollectivite(Long id) {
        this.idCollectivite = id;
    }

    /**
     * Gets the siret.
     *
     * @return the siret
     */
    public String getSiret() {
        return this.siret;
    }

    /**
     * Sets the siret.
     *
     * @param siret
     *            the new siret
     */
    public void setSiret(String siret) {
        this.siret = siret;
    }

    /**
     * Gets the nom court.
     *
     * @return the nom court
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * Sets the nom court.
     *
     * @param nomCourt
     *            the new nom court
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * Gets the nom long.
     *
     * @return the nom long
     */
    public String getNomLong() {
        return this.nomLong;
    }

    /**
     * Sets the nom long.
     *
     * @param nomLong
     *            the new nom long
     */
    public void setNomLong(String nomLong) {
        this.nomLong = nomLong;
    }

    /**
     * Gets the etat collectivite.
     *
     * @return the etat collectivite
     */
    public EtatCollectiviteEnum getEtatCollectivite() {
        return this.etatCollectivite;
    }

    /**
     * Sets the etat collectivite.
     *
     * @param etatCollectivite
     *            the new etat collectivite
     */
    public void setEtatCollectivite(EtatCollectiviteEnum etatCollectivite) {
        this.etatCollectivite = etatCollectivite;
    }

    /**
     * @return the admin
     */
    public boolean isAdmin() {
        return this.admin;
    }

    /**
     * @param admin
     *            the admin to set
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * @return the idUtilisateur
     */
    public Long getIdUtilisateur() {
        return this.idUtilisateur;
    }

    /**
     * @param idUtilisateur
     *            the idUtilisateur to set
     */
    public void setIdUtilisateur(Long idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

}

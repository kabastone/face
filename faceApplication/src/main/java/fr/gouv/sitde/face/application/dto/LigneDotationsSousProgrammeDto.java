package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class LigneDotationsSousProgrammeDto.
 *
 * @author a453029
 */
public class LigneDotationsSousProgrammeDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1867294428474102722L;

    /** The id dotation sous programme. */
    private Long idDotationSousProgramme;

    /** The versionDotationSousProgramme. */
    private int versionDotationSousProgramme;

    /** The id sous programme. */
    private Integer idSousProgramme;

    /** The num sous action. */
    private String numSousActionSp;

    /** The abreviationSp. */
    private String abreviationSp;

    /** The descriptionSp. */
    private String descriptionSp;

    /** The montant initial annee N. */
    private BigDecimal montantInitialAnneeN = BigDecimal.ZERO;

    /** The montant annee N. */
    private BigDecimal montantAnneeN = BigDecimal.ZERO;

    /** The montant annee moins 1. */
    private BigDecimal montantAnneeMoins1 = BigDecimal.ZERO;

    /** The montant annee moins 2. */
    private BigDecimal montantAnneeMoins2 = BigDecimal.ZERO;

    /** The montant annee moins 3. */
    private BigDecimal montantAnneeMoins3 = BigDecimal.ZERO;

    /** The annee fin validite. */
    private Integer anneeFinValidite;

    /**
     * @return the idSousProgramme
     */
    public Integer getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * @param idSousProgramme
     *            the idSousProgramme to set
     */
    public void setIdSousProgramme(Integer idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

    /**
     * @return the numSousActionSp
     */
    public String getNumSousActionSp() {
        return this.numSousActionSp;
    }

    /**
     * @param numSousActionSp
     *            the numSousActionSp to set
     */
    public void setNumSousActionSp(String numSousActionSp) {
        this.numSousActionSp = numSousActionSp;
    }

    /**
     * @return the abreviationSp
     */
    public String getAbreviationSp() {
        return this.abreviationSp;
    }

    /**
     * @param abreviationSp
     *            the abreviationSp to set
     */
    public void setAbreviationSp(String abreviationSp) {
        this.abreviationSp = abreviationSp;
    }

    /**
     * @return the descriptionSp
     */
    public String getDescriptionSp() {
        return this.descriptionSp;
    }

    /**
     * @param descriptionSp
     *            the descriptionSp to set
     */
    public void setDescriptionSp(String descriptionSp) {
        this.descriptionSp = descriptionSp;
    }

    /**
     * @return the montantAnneeN
     */
    public BigDecimal getMontantAnneeN() {
        return this.montantAnneeN;
    }

    /**
     * @param montantAnneeN
     *            the montantAnneeN to set
     */
    public void setMontantAnneeN(BigDecimal montantAnneeN) {
        this.montantAnneeN = montantAnneeN;
    }

    /**
     * @return the montantAnneeMoins1
     */
    public BigDecimal getMontantAnneeMoins1() {
        return this.montantAnneeMoins1;
    }

    /**
     * @param montantAnneeMoins1
     *            the montantAnneeMoins1 to set
     */
    public void setMontantAnneeMoins1(BigDecimal montantAnneeMoins1) {
        this.montantAnneeMoins1 = montantAnneeMoins1;
    }

    /**
     * @return the montantAnneeMoins2
     */
    public BigDecimal getMontantAnneeMoins2() {
        return this.montantAnneeMoins2;
    }

    /**
     * @param montantAnneeMoins2
     *            the montantAnneeMoins2 to set
     */
    public void setMontantAnneeMoins2(BigDecimal montantAnneeMoins2) {
        this.montantAnneeMoins2 = montantAnneeMoins2;
    }

    /**
     * @return the montantAnneeMoins3
     */
    public BigDecimal getMontantAnneeMoins3() {
        return this.montantAnneeMoins3;
    }

    /**
     * @param montantAnneeMoins3
     *            the montantAnneeMoins3 to set
     */
    public void setMontantAnneeMoins3(BigDecimal montantAnneeMoins3) {
        this.montantAnneeMoins3 = montantAnneeMoins3;
    }

    /**
     * @return the versionDotationSousProgramme
     */
    public int getVersionDotationSousProgramme() {
        return this.versionDotationSousProgramme;
    }

    /**
     * @param versionDotationSousProgramme
     *            the versionDotationSousProgramme to set
     */
    public void setVersionDotationSousProgramme(int versionDotationSousProgramme) {
        this.versionDotationSousProgramme = versionDotationSousProgramme;
    }

    /**
     * @return the idDotationSousProgramme
     */
    public Long getIdDotationSousProgramme() {
        return this.idDotationSousProgramme;
    }

    /**
     * @param idDotationSousProgramme
     *            the idDotationSousProgramme to set
     */
    public void setIdDotationSousProgramme(Long idDotationSousProgramme) {
        this.idDotationSousProgramme = idDotationSousProgramme;
    }

    /**
     * @return the anneeFinValidite
     */
    public Integer getAnneeFinValidite() {
        return this.anneeFinValidite;
    }

    /**
     * @param anneeFinValidite
     *            the anneeFinValidite to set
     */
    public void setAnneeFinValidite(Integer anneeFinValidite) {
        this.anneeFinValidite = anneeFinValidite;
    }

    /**
     * @return the montantInitialAnneeN
     */
    public BigDecimal getMontantInitialAnneeN() {
        return this.montantInitialAnneeN;
    }

    /**
     * @param montantInitialAnneeN
     *            the montantInitialAnneeN to set
     */
    public void setMontantInitialAnneeN(BigDecimal montantInitialAnneeN) {
        this.montantInitialAnneeN = montantInitialAnneeN;
    }

}

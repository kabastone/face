package fr.gouv.sitde.face.application.administration;

import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Interface GestionUtilisateurApplication.
 */
public interface GestionUtilisateurApplication {

    /**
     * Rechercher utilisateur par id.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @return the utilisateurAdminDto
     */
    UtilisateurAdminDto rechercherUtilisateurParId(Long idUtilisateur);

    /**
     * Rechercher utilisateur par email.
     *
     * @param email
     *            the email
     * @return the utilisateurAdminDto
     */
    UtilisateurAdminDto rechercherUtilisateurParEmail(String email);

    /**
     * Synchronise l'utilisateur connecté.<br/>
     * Crée l'utilisateur s'il n'existe pas en base, sinon met à jour le cerbereId de l'utilisateur existant s'il a été modifié.
     *
     * @param utilisateurAdminDto
     *            the utilisateur admin dto
     * @return the utilisateur admin dto
     */
    UtilisateurAdminDto synchroniserUtilisateurConnecte(UtilisateurAdminDto utilisateurAdminDto);

    /**
     * Creer un utilisateur en attente de connexion avec des droits d'agent sur une collectivite. <br/>
     * Son nom, son prénom et son id Cerbere sont des flags qui seront synchronisés avec Cerbere lors de sa premère connexion.
     *
     * @param utilisateurAdminDto
     *            the utilisateur admin dto
     * @return the utilisateurAdminDto
     */
    UtilisateurAdminDto creerUtilisateurEnAttenteConnexion(UtilisateurAdminDto utilisateurAdminDto);

    /**
     * Modifier utilisateur.
     *
     * @param utilisateurAdminDto
     *            the utilisateurAdminDto
     * @return the utilisateurAdminDto
     */
    UtilisateurAdminDto modifierUtilisateur(UtilisateurAdminDto utilisateurAdminDto);

    /**
     * Supprimer utilisateur.
     *
     * @param idUtilisateur
     *            the id utilisateur
     */
    void supprimerUtilisateur(Long idUtilisateur);

    /**
     * Rechercher tous utilisateurs admin dto.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<UtilisateurAdminDto> rechercherTousUtilisateurs(PageDemande pageDemande);
}

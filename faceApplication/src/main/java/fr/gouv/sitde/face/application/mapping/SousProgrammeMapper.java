/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * Mapper entité/DTO des sous programmes.
 *
 * @author a702709
 *
 */
@Mapper(componentModel = "spring")
public interface SousProgrammeMapper {

    /**
     * Sous programme to sous programme dto.
     *
     * @param sousProgramme
     *            the sous programme
     * @return the sous programme dto
     */
    @Mapping(target = "code", source = "abreviation")
    @Mapping(target = "name", source = "description")
    SousProgrammeDto sousProgrammeToSousProgrammeDto(SousProgramme sousProgramme);

    /**
     * Sous programme to sous programme dto.
     *
     * @param sousProgramme
     *            the sous programme
     * @return the list
     */
    List<SousProgrammeDto> sousProgrammeToSousProgrammeDto(List<SousProgramme> sousProgramme);

    /**
     * Sous programme dto to sous programme.
     *
     * @param sousProgrammeDto
     *            the sous programme dto
     * @return the sous programme
     */
    @Mapping(target = "abreviation", source = "code")
    @Mapping(target = "description", source = "name")
    SousProgramme sousProgrammeDtoToSousProgramme(SousProgrammeDto sousProgrammeDto);

    /**
     * Sous programme dto to sous programme.
     *
     * @param sousProgrammeDto
     *            the sous programme dto
     * @return the list
     */
    List<SousProgramme> sousProgrammeDtoToSousProgramme(List<SousProgrammeDto> sousProgrammeDto);

}

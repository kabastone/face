/**
 *
 */
package fr.gouv.sitde.face.application.tableaudebord;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeProlongationTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Interface TableauDeBordMferApplication.
 *
 * @author a453029
 */
public interface TableauDeBordMferApplication {

    /**
     * Rechercher tableau de bord mfer.
     *
     * @return the tableau de bord mfer dto
     */
    TableauDeBordMferDto rechercherTableauDeBordMfer();

    /**
     * Rechercher demandes subvention tdb mfer aqualifier subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAqualifierSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer aqualifier paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAqualifierPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer aaccorder subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAaccorderSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer aaccorder paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAaccorderPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer aattribuer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAattribuerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer asignaler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubventionTdbDto> rechercherDemandesSubventionTdbMferAsignalerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer asignaler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiementTdbDto> rechercherDemandesPaiementTdbMferAsignalerPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes prolongation tdb mfer aprolonger subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeProlongationTdbDto> rechercherDemandesProlongationTdbMferAprolongerSubvention(PageDemande pageDemande);

}

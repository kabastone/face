package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.gouv.sitde.face.application.dto.AnomalieDto;
import fr.gouv.sitde.face.application.dto.AnomalieSimpleDto;
import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Interface AnomalieMapper.
 */
@Mapper(componentModel = "spring")
public interface AnomalieMapper {

    /**
     * Anomalie to anomalie simple dto.
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie simple dto
     */
    @Mapping(target = "libelleTypeAnomalie", source = "typeAnomalie.libelle")
    AnomalieSimpleDto anomalieToAnomalieSimpleDto(Anomalie anomalie);

    /**
     * Anomalie dto to anomalie.
     *
     * @param anomalieDto
     *            the anomalie dto
     * @return the anomalie
     */
    @Mapping(target = "typeAnomalie.libelle", source = "libelleTypeAnomalie")
    @Mapping(target = "typeAnomalie.code", source = "codeTypeAnomalie")
    @Mapping(target = "demandePaiement.id", source = "idDemandePaiement")
    @Mapping(target = "demandeSubvention.id", source = "idDemandeSubvention")
    Anomalie anomalieDtoToAnomalie(AnomalieDto anomalieDto);

    /**
     * Anomalie to anomalie dto.
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie dto
     */
    @Mapping(target = "codeTypeAnomalie", source = "typeAnomalie.code")
    @Mapping(target = "libelleTypeAnomalie", source = "typeAnomalie.libelle")
    @Mapping(target = "idDemandePaiement", source = "demandePaiement.id")
    @Mapping(target = "idDemandeSubvention", source = "demandeSubvention.id")
    AnomalieDto anomalieToAnomalieDto(Anomalie anomalie);

    /**
     * Anomalies to anomalies dto.
     *
     * @param anomalie
     *            the anomalie
     * @return the list
     */
    List<AnomalieDto> anomaliesToAnomaliesDto(List<Anomalie> anomalies);
}

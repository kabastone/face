package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;

/**
 * Classe adresse email de la liste de diffusion.
 */
public class AdresseEmailDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8366919575728669492L;

    /** The id. */
    private Long id;

    /** The id collectivite. */
    private String idCollectivite;

    /** The email. */
    private String email;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public String getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite the idCollectivite to set
     */
    public void setIdCollectivite(String idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}

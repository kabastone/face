package fr.gouv.sitde.face.application.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeDeserializer;
import fr.gouv.sitde.face.application.jsonutils.CustomLocalDateTimeSerializer;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Class DemandeProlongationDto.
 */
public class DemandeProlongationDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7532278376462023127L;

    /** The id. */
    private Long id;

    /** The code etat demande. */
    private String etat;

    /** The libelle etat demande. */
    private String etatLibelle;

    /** The num dossier. */
    private String numDossier;

    /** The id dossier subvention. */
    private Long idDossierSubvention;

    /** The nom long. */
    private String nomLongCollectivite;

    /** The code département */
    private String codeDepartement;

    /** The LocalDateTime demande. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateDemande;

    /** The LocalDateTime achevement demandee. */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEcheanceAchevementDemande;

    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    private LocalDateTime dateEcheanceAchevement;

    /** The motif refus. */
    private String motifRefus;

    /** The documents. */
    private Set<DocumentDto> documents = new LinkedHashSet<>(0);

    /** The liste fichiers envoyer. */
    private final List<FichierTransfert> listeFichiersEnvoyer = new ArrayList<>();

    /** The version. */
    private int version;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the date demande.
     *
     * @return the dateDemande
     */
    public LocalDateTime getDateDemande() {
        return this.dateDemande;
    }

    /**
     * Sets the date demande.
     *
     * @param dateDemande
     *            the dateDemande to set
     */
    public void setDateDemande(LocalDateTime dateDemande) {
        this.dateDemande = dateDemande;
    }

    /**
     * Gets the motif refus.
     *
     * @return the motifRefus
     */
    public String getMotifRefus() {
        return this.motifRefus;
    }

    /**
     * Sets the motif refus.
     *
     * @param motifRefus
     *            the motifRefus to set
     */
    public void setMotifRefus(String motifRefus) {
        this.motifRefus = motifRefus;
    }

    /**
     * Gets the documents.
     *
     * @return the documents
     */
    public Set<DocumentDto> getDocuments() {
        return this.documents;
    }

    /**
     * Sets the documents.
     *
     * @param documents
     *            the documents to set
     */
    public void setDocuments(Set<DocumentDto> documents) {
        this.documents = documents;
    }

    /**
     * @return the etat
     */
    public String getEtat() {
        return this.etat;
    }

    /**
     * @param etat
     *            the etat to set
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }

    /**
     * @return the etatLibelle
     */
    public String getEtatLibelle() {
        return this.etatLibelle;
    }

    /**
     * @param etatLibelle
     *            the etatLibelle to set
     */
    public void setEtatLibelle(String etatLibelle) {
        this.etatLibelle = etatLibelle;
    }

    /**
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * @return the idDossierSubvention
     */
    public Long getIdDossierSubvention() {
        return this.idDossierSubvention;
    }

    /**
     * @param idDossierSubvention
     *            the idDossierSubvention to set
     */
    public void setIdDossierSubvention(Long idDossierSubvention) {
        this.idDossierSubvention = idDossierSubvention;
    }

    /**
     * @return the dateEcheanceAchevement
     */
    public LocalDateTime getDateEcheanceAchevement() {
        return this.dateEcheanceAchevement;
    }

    /**
     * @param dateEcheanceAchevement
     *            the dateEcheanceAchevement to set
     */
    public void setDateEcheanceAchevement(LocalDateTime dateEcheanceAchevement) {
        this.dateEcheanceAchevement = dateEcheanceAchevement;
    }

    /**
     * @return the listeFichiersEnvoyer
     */
    public List<FichierTransfert> getListeFichiersEnvoyer() {
        return this.listeFichiersEnvoyer;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the dateEcheanceAchevementDemande
     */
    public LocalDateTime getDateEcheanceAchevementDemande() {
        return this.dateEcheanceAchevementDemande;
    }

    /**
     * @param dateEcheanceAchevementDemande
     *            the dateEcheanceAchevementDemande to set
     */
    public void setDateEcheanceAchevementDemande(LocalDateTime dateEcheanceAchevementDemande) {
        this.dateEcheanceAchevementDemande = dateEcheanceAchevementDemande;
    }

    /**
     * @return the nomLongCollectivite
     */
    public String getNomLongCollectivite() {
        return this.nomLongCollectivite;
    }

    /**
     * @param nomLongCollectivite
     *            the nomLongCollectivite to set
     */
    public void setNomLongCollectivite(String nomLongCollectivite) {
        this.nomLongCollectivite = nomLongCollectivite;
    }

    /**
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

}

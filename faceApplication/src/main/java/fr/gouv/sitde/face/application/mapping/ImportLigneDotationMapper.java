/**
 *
 */
package fr.gouv.sitde.face.application.mapping;

import java.util.List;

import org.mapstruct.Mapper;

import fr.gouv.sitde.face.application.dto.ImportLigneDotationDto;
import fr.gouv.sitde.face.transverse.imports.ImportLigneDotation;

/**
 * Mapper DTO/Entite Import Dossier Dotation.
 *
 * @author Atos
 */
@Mapper(componentModel = "spring")
public interface ImportLigneDotationMapper {

    /**
     * Import ligne dotation to import liste ligne dotation.
     *
     * @param importLDDto
     *            the import LD dto
     * @return the import ligne dotation
     */
    ImportLigneDotation importLigneDotationToImportListeLigneDotation(ImportLigneDotationDto importLDDto);

    /**
     * Import liste ligne dotation to import liste ligne dotation.
     *
     * @param listeImportLDDto
     *            the liste import LD dto
     * @return the list
     */
    List<ImportLigneDotation> importListeLigneDotationToImportListeLigneDotation(List<ImportLigneDotationDto> listeImportLDDto);

}

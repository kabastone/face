/**
 *
 */
package fr.gouv.sitde.face.application.subvention.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.application.dto.CadencementDto;
import fr.gouv.sitde.face.application.dto.CadencementTableDto;
import fr.gouv.sitde.face.application.mapping.CadencementMapper;
import fr.gouv.sitde.face.application.subvention.GestionCadencementApplication;
import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class ImportDotationApplicationImpl.
 *
 * @author Atos
 */
@Service
@Transactional
public class GestionCadencementApplicationImpl implements GestionCadencementApplication {

    @Autowired
    private CadencementService cadencementService;

    @Autowired
    private CadencementMapper cadencementMapper;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionCadencementApplication#recupererCadencementsPourCollectivite(java.lang.Long)
     */
    @Override
    public List<CadencementTableDto> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet) {
        List<Cadencement> cadencements = this.cadencementService.recupererCadencementsPourCollectivite(idCollectivite, deProjet);
        return this.cadencementMapper.cadencementsToCadencementTableDtos(cadencements);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.application.subvention.GestionCadencementApplication#updateCadencement(fr.gouv.sitde.face.application.dto.CadencementDto)
     */
    @Override
    public CadencementDto updateCadencement(CadencementDto cadencementDto) {
        Cadencement cadencement = this.cadencementMapper.cadencementDtoToCadencement(cadencementDto);
        Cadencement cadencementBdd = this.cadencementService.modifierCadencement(cadencement);
        return this.cadencementMapper.cadencementToCadencementDto(cadencementBdd);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication#estCadencementValide(java.lang.Long)
     */
    @Override
    public Boolean estToutCadencementValidePourCollectivite(Long idCollectivite) {
        return this.cadencementService.estToutCadencementValidePourCollectivite(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.application.subvention.GestionCadencementApplication#rechercherCadencementsParCriteres(fr.gouv.sitde.face.transverse.
     * queryobject.CritereRechercheCadencementQo)
     */
    @Override
    public List<ResultatRechercheCadencementDto> rechercherCadencementsParCriteres(CritereRechercheCadencementQo criteresRecherche,
            String codeProgramme) {

        List<ResultatRechercheCadencementDto> listeCadencement = this.cadencementService.rechercherCadencementParCodeNumerique(criteresRecherche,
                codeProgramme);

        for (ResultatRechercheCadencementDto listeCadencements : listeCadencement) {
            if (ConstantesFace.CODE_SP_SECURISATION.equals(listeCadencements.getSousProgrammeAbreviation())) {
                listeCadencements.setSousProgrammeCode(ConstantesFace.NUMERO_SP_SECURISATION);
            }
        }

        listeCadencement
                .sort((ResultatRechercheCadencementDto res1, ResultatRechercheCadencementDto res2) -> Integer.parseInt(res1.getSousProgrammeCode())
                        - Integer.parseInt(res2.getSousProgrammeCode()));

        return listeCadencement;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.application.dto.lov;

import java.io.Serializable;

/**
 * The Class EtatDemandeSubventionLovDto.
 *
 * @author a754839
 */
public class EtatDemandePaiementLovDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4483560855438142576L;

    /** The code. */
    private String code;

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
}

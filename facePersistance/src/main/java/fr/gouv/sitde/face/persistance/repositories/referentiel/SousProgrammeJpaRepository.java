/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * The Interface SousProgrammeJpaRepository.
 *
 * @author a453029
 */
@Repository
public interface SousProgrammeJpaRepository extends JpaRepository<SousProgramme, Integer> {

    /**
     * Find all sous programmes.
     *
     * @return the list
     */
    @Query("select sp from SousProgramme sp inner join fetch sp.programme order by sp.codeDomaineFonctionnel")
    List<SousProgramme> findAllSousProgrammes();

    /**
     * Find sous programmes pour renoncement dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    @Query("select distinct spr from DotationCollectivite dco " + " join dco.dotationDepartement dde " + " join dde.dotationSousProgramme dsp "
            + " join dsp.dotationProgramme dpr " + " join dsp.sousProgramme spr " + " where dco.collectivite.id = :idCollectivite "
            + " and dpr.annee = :annee and spr.deProjet = false and year(spr.dateDebutValidite) <= :annee and (year(spr.dateFinValidite) >= :annee or spr.dateFinValidite = NULL) "
            + " and dco.montant - dco.dotationRepartie > 0" + " order by spr.codeDomaineFonctionnel")
    List<SousProgramme> findSousProgrammesDeTravauxParCollectivite(@Param("idCollectivite") Long idCollectivite, @Param("annee") Integer annee);

    /**
     * Find sous programmes par collectivite.
     *
     * @return the list
     */
    @Query("select distinct spr from DotationSousProgramme dsp " + " join dsp.dotationProgramme dpr " + " join dsp.sousProgramme spr "
            + " where dpr.annee = :annee and spr.deProjet = true" + " order by spr.codeDomaineFonctionnel")
    List<SousProgramme> findSousProgrammesDeProjet(@Param("annee") Integer annee);

    /**
     * Gets the sous programme par abreviation.
     *
     * @param abreviation
     *            the abreviation
     * @return the sous programme par abreviation
     */
    Optional<SousProgramme> findFirstByAbreviation(String abreviation);

    /**
     * Rechercher sous programmes par dotation programme.
     *
     * @param idDotationProgramme
     *            the id dotation programme
     * @return the list
     */
    @Query("select sp from SousProgramme sp inner join sp.programme p"
            + " where exists (select 1 from DotationProgramme dp where dp.programme = p and dp.id = :idDotationProgramme)"
            + " and year(sp.dateDebutValidite) <= :annee and (year(sp.dateFinValidite) >= :annee or sp.dateFinValidite = NULL) ")
    List<SousProgramme> rechercherSousProgrammesParDotationProgramme(@Param("idDotationProgramme") Long idDotationProgramme,
            @Param("annee") Integer annee);

    /**
     * Recherche sous programmes pour creation de ligne dotation departementale.
     *
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select distinct spr from DotationSousProgramme dotDep " + "join dotDep.sousProgramme spr join dotDep.dotationProgramme dpr "
            + "where dpr.annee = :annee and spr.deProjet = FALSE and (dotDep.montant - dotDep.dotationRepartie) > 0")
    List<SousProgramme> rechercherSousProgrammesPourLigneDotationDepartementale(@Param("annee") Integer annee);

    /**
     * Rechercher pour dotation departement.
     *
     * @param ap
     *            the ap
     * @param ae
     *            the ae
     * @param ce
     *            the ce
     * @param ss
     *            the ss
     * @param sf
     *            the sf
     * @return the list
     */
    @Query("select spr from SousProgramme spr where spr.deProjet = FALSE "
            + "and year(spr.dateDebutValidite) <= :annee and (year(spr.dateFinValidite) >= :annee or spr.dateFinValidite = NULL)")
    List<SousProgramme> rechercherPourDotationDepartement(@Param("annee") Integer annee);

    /**
     * @param codeDomaineFonctionnel
     * @return
     */
    @Query("select distinct spr from SousProgramme spr " + " where spr.codeDomaineFonctionnel like :codeDomaine")
    SousProgramme findSousProgrammesParCodeFonctionnel(@Param("codeDomaine") String codeDomaineFonctionnel);

    /**
     * Find sous programmes de projet par collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select distinct spr from DotationCollectivite dco " + " join dco.dotationDepartement dde " + " join dde.dotationSousProgramme dsp "
            + " join dsp.dotationProgramme dpr " + " join dsp.sousProgramme spr "
            + " where dco.collectivite.id = :idCollectivite and dpr.annee = :annee "
            + " and spr.deProjet = true and year(spr.dateDebutValidite) <= :annee and (year(spr.dateFinValidite) >= :annee or spr.dateFinValidite = NULL)"
            + " order by spr.codeDomaineFonctionnel")
    List<SousProgramme> findSousProgrammesDeProjetParCollectivite(@Param("idCollectivite") Long idCollectivite, @Param("annee") Integer annee);
}

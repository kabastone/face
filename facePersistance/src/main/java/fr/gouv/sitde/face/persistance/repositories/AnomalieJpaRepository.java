package fr.gouv.sitde.face.persistance.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Interface AnomalieJpaRepository.
 */
public interface AnomalieJpaRepository extends JpaRepository<Anomalie, Long> {

    /**
     * Rechercher anomalies par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the sets the
     */
    @Query("select anomalie from Anomalie anomalie where anomalie.demandePaiement.id=:id")
    Set<Anomalie> rechercherAnomaliesParIdDemandePaiement(@Param("id") Long idDemandePaiement);

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the sets the
     */
    @Query("select anomalie from Anomalie anomalie where anomalie.demandeSubvention.id=:id")
    Set<Anomalie> rechercherAnomaliesParIdDemandeSubvention(@Param("id") Long idDemandeSubvention);

    /**
     * @param idDemandeSubvention
     */
    @Modifying
    @Query("update Anomalie ano set ano.visiblePourAODE = true where ano.demandeSubvention.id=:id and ano.corrigee=false")
    void rendreAnomaliesDemandeSubventionVisiblesPourAODE(@Param("id") Long idDemandeSubvention);

    /**
     * @param idDemandeSubvention
     */
    @Modifying
    @Query("update Anomalie ano set ano.visiblePourAODE = true where ano.demandePaiement.id=:id and ano.corrigee=false")
    void rendreAnomaliesDemandePaiementVisiblesPourAODE(@Param("id") Long idDemandePaiement);

}

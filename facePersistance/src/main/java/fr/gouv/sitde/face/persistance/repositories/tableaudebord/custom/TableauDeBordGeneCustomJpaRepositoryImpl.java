/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import java.time.temporal.ChronoUnit;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class TableauDeBordGeneCustomJpaRepositoryImpl.
 *
 * @author a453029
 */
@Repository
public class TableauDeBordGeneCustomJpaRepositoryImpl implements TableauDeBordGeneCustomJpaRepository {

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** The Constant BRACKET_COMMA_BRACKET. */
    private static final String BRACKET_COMMA_BRACKET = "),(";

    /** The Constant REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION : RG_SUB_105-01. */
    public static final String REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION = "select dco from DotationCollectivite dco "
            + "inner join fetch dco.dotationDepartement dde inner join dco.collectivite col "
            + "inner join fetch dde.dotationSousProgramme dsp inner join fetch dsp.sousProgramme sp inner join dsp.dotationProgramme dpr "
            + "where not exists (select 1 from DossierSubvention dsu inner join dsu.dotationCollectivite dco2 where dco2.id=dco.id) "
            + "and dco.montant > 0" + "and dpr.annee = :anneeCourante and col.id = :idCollectivite " + "order by dco.id";

    /** The Constant REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION_COUNT : RG_SUB_105-01. */
    public static final String REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION_COUNT = "select count(dco.id) from DotationCollectivite dco "
            + "inner join dco.dotationDepartement dde inner join dco.collectivite col "
            + "inner join dde.dotationSousProgramme dsp inner join dsp.dotationProgramme dpr "
            + "where not exists (select 1 from DossierSubvention dsu inner join dsu.dotationCollectivite dco2 where dco2.id=dco.id) "
            + "and dco.montant > 0" + "and dpr.annee = :anneeCourante and col.id = :idCollectivite";

    /** The Constant REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT. RG_SUB_110-01. */
    public static final String REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT = "select dos from DossierSubvention dos "
            + "inner join fetch dos.dotationCollectivite dco inner join fetch dco.collectivite col "
            + "where (dos.plafondAide - dos.aideDemandee)> 0 and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite "
            + "and not exists (select 1 from DemandePaiement dpa where dpa.typeDemandePaiement = :typePaiementSolde and dpa.dossierSubvention = dos) "
            + "order by dos.id";

    /** The Constant REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT_COUNT. RG_SUB_110-01. */
    public static final String REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT_COUNT = "select count(dos.id) from DossierSubvention dos "
            + "inner join dos.dotationCollectivite dco inner join dco.collectivite col "
            + "where (dos.plafondAide - dos.aideDemandee)> 0 and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite "
            + "and not exists (select 1 from DemandePaiement dpa where dpa.typeDemandePaiement = :typePaiementSolde and dpa.dossierSubvention = dos)";

    /** The Constant REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION : RG_SUB_106-01. */
    public static final String REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION = "select distinct dco from DotationCollectivite dco "
            + "inner join dco.dossiersSubvention dos inner join dco.collectivite col "
            + "inner join dco.dotationDepartement dde inner join dco.collectivite col "
            + "inner join dde.dotationSousProgramme dsp inner join dsp.sousProgramme sp inner join dsp.dotationProgramme dpr "
            + "where (dco.montant - dco.dotationRepartie)> 0 and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite "
            + "AND dpr.annee = :anneeCourante "
            + "AND EXISTS (select 1 from DossierSubvention dos inner join dos.dotationCollectivite dco2 where dco2.id = dco.id) "
            + "AND NOT EXISTS (select 1 from DemandeSubvention dsu inner join dsu.dossierSubvention.dotationCollectivite dco3 "
            + "where dco3.id = dco.id AND dsu.etatDemandeSubvention.code not in ('REFUSEE', 'ATTRIBUEE', 'REJET_TAUX', 'CLOTUREE'))";

    /** The Constant REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION_COUNT : RG_SUB_106-01. */
    public static final String REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION_COUNT = "select count(distinct dco.id) from DotationCollectivite dco "
            + "inner join dco.dossiersSubvention dos inner join dco.collectivite col "
            + "inner join dco.dotationDepartement dde inner join dco.collectivite col "
            + "inner join dde.dotationSousProgramme dsp inner join dsp.sousProgramme sp inner join dsp.dotationProgramme dpr "
            + "where (dco.montant - dco.dotationRepartie)> 0 and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite "
            + "AND dpr.annee = :anneeCourante "
            + "AND EXISTS (select dos from DossierSubvention dos inner join dos.dotationCollectivite dco2 where dco2.id = dco.id) "
            + "AND NOT EXISTS (select 1 from DemandeSubvention dsu inner join dsu.dossierSubvention.dotationCollectivite dco3 "
            + "where dco3.id = dco.id AND dsu.etatDemandeSubvention.code not in ('REFUSEE', 'ATTRIBUEE', 'REJET_TAUX', 'CLOTUREE'))";

    /** The Constant REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION. RG_SUB_103-01 avec idCollectivite. */
    public static final String REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubAnomalieSignalee "
            + " and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite " + "order by dsu.id";

    /** The Constant REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION_COUNT. RG_SUB_103-01 avec idCollectivite. */
    public static final String REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubAnomalieSignalee "
            + " and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite";

    /** The Constant REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT. RG_SUB_104-01 avec idCollectivite */
    public static final String REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col where edp.code = :etatDemPaiAnomalieSignalee "
            + "and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite " + "order by dpa.id";

    /** The Constant REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT_COUNT. RG_SUB_104-01 avec idCollectivite */
    public static final String REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col where edp.code = :etatDemPaiAnomalieSignalee "
            + "and dos.etatDossier = :etatDossierOuvert and col.id = :idCollectivite";

    /** The Constant REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT. RG_SUB_107-01. */
    public static final String REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT = "select dos from DossierSubvention dos "
            + "inner join fetch dos.dotationCollectivite dco inner join fetch dco.collectivite col "
            + "where col.id = :idCollectivite and dos.dateEcheanceLancement <= :dateDuJourPlus3Mois "
            + "and dos.etatDossier = :etatDossierOuvert and dos.dateEcheanceLancement >= :dateDuJour and not exists "
            + "(select 1 from DemandePaiement dpa where dpa.dossierSubvention = dos) " + "order by dos.id";

    /** The Constant REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT_COUNT. RG_SUB_107-01. */
    public static final String REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT_COUNT = "select count(dos.id) from DossierSubvention dos "
            + "inner join dos.dotationCollectivite dco inner join dco.collectivite col "
            + "where col.id = :idCollectivite and dos.dateEcheanceLancement <= :dateDuJourPlus3Mois "
            + "and dos.etatDossier = :etatDossierOuvert and dos.dateEcheanceLancement >= :dateDuJour and not exists "
            + "(select 1 from DemandePaiement dpa where dpa.dossierSubvention = dos)";

    /** The Constant REQUETE_TDB_GENE_A_SOLDER_PAIEMENT. RG_SUB_108-01. */
    public static final String REQUETE_TDB_GENE_A_SOLDER_PAIEMENT = "select dos from DossierSubvention dos "
            + "inner join fetch dos.dotationCollectivite dco inner join fetch dco.collectivite col "
            + "where col.id = :idCollectivite and dos.dateEcheanceAchevement <= :dateDuJourPlus4Mois "
            + "and dos.etatDossier = :etatDossierOuvert and dos.dateEcheanceAchevement >= :dateDuJour and not exists "
            + "(select 1 from DemandePaiement dpa where dpa.typeDemandePaiement = :typePaiementSolde and dpa.dossierSubvention = dos) "
            + "order by dos.id";

    /** The Constant REQUETE_TDB_GENE_A_SOLDER_PAIEMENT_COUNT. RG_SUB_108-01. */
    public static final String REQUETE_TDB_GENE_A_SOLDER_PAIEMENT_COUNT = "select count(dos.id) from DossierSubvention dos "
            + "inner join dos.dotationCollectivite dco inner join dco.collectivite col "
            + "where col.id = :idCollectivite and dos.dateEcheanceAchevement <= :dateDuJourPlus4Mois "
            + "and dos.etatDossier = :etatDossierOuvert and dos.dateEcheanceAchevement >= :dateDuJour and not exists "
            + "(select 1 from DemandePaiement dpa where dpa.typeDemandePaiement = :typePaiementSolde and dpa.dossierSubvention = dos)";

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.persistance.repositories.custom.TableauDeBordJpaRepository#rechercherTableauDeBordGenerique()
     */
    @Override
    public TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite) {

        // Construction de la requete JPQL
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT NEW fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto((");
        jpql.append(REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_GENE_A_SOLDER_PAIEMENT_COUNT);
        jpql.append(")) FROM MissionFace ");
        String chaineRequete = jpql.toString();

        TypedQuery<TableauDeBordGeneDto> query = this.entityManager.createQuery(chaineRequete, TableauDeBordGeneDto.class)
                .setParameter("anneeCourante", this.timeOperationTransverse.getAnneeCourante()).setParameter("idCollectivite", idCollectivite)
                .setParameter("etatDossierOuvert", EtatDossierEnum.OUVERT)
                .setParameter("etatDemSubAnomalieSignalee", EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE.toString())
                .setParameter("etatDemPaiAnomalieSignalee", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString())
                .setParameter("dateDuJour", this.timeOperationTransverse.now())
                .setParameter("dateDuJourPlus3Mois", this.timeOperationTransverse.now().plusMonths(3).truncatedTo(ChronoUnit.DAYS))
                .setParameter("dateDuJourPlus4Mois", this.timeOperationTransverse.now().plusMonths(4).truncatedTo(ChronoUnit.DAYS))
                .setParameter("typePaiementSolde", TypeDemandePaiementEnum.SOLDE);

        return query.getSingleResult();
    }

}

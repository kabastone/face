package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.persistance.repositories.custom.DepartementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * The Interface DepartementJpaRepository.
 */
public interface DepartementJpaRepository extends JpaRepository<Departement, Long>, DepartementJpaRepositoryCustom {

    /**
     * Find all departements.
     *
     * @return the list
     */
    @Query("select d from Departement d order by replace(replace(d.code, 'A', '00'), 'B', '01')")
    List<Departement> findAllDepartements();

    /**
     * Compter collectivite.
     *
     * @param idDepartement
     *            the id departement
     * @return the integer
     */
    @Query("select count(col) from Departement d join d.collectivites col "
            + "where d.id = :idDepartement and (col.etatCollectivite = 'EN_CREATION' or col.etatCollectivite = 'CREEE')")
    Integer compterCollectivite(@Param("idDepartement") Integer idDepartement);

    /**
     * Gets the code par id.
     *
     * @param idDepartement
     *            the id departement
     * @return the code par id
     */
    @Query("select d.code from Departement d where d.id = :idDepartement")
    String getCodeParId(@Param("idDepartement") Integer idDepartement);

    /**
     * Gets the departement par id.
     *
     * @param idDepartement
     *            the id departement
     * @return the departement par id
     */
    @Query("select d from Departement d where d.id = :idDepartement")
    Optional<Departement> getDepartementParId(@Param("idDepartement") Integer idDepartement);

    /**
     * Gets the departement par code.
     *
     * @param code
     *            the code
     * @return the departement par code
     */
    @Query("select d from Departement d where d.code = :code")
    Optional<Departement> getDepartementParCode(@Param("code") String code);

    /**
     * Rechercher par id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the departement
     */
    @Query("select d from Departement d inner join d.collectivites cols where cols.id = :idCollectivite")
    Departement rechercherParIdCollectivite(@Param("idCollectivite") Long idCollectivite);
}

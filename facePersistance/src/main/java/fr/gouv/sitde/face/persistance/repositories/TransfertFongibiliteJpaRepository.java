package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

/**
 * Repository JPA de l'entité TransfertFongibilite.
 *
 * @author Atos
 */
@Repository
public interface TransfertFongibiliteJpaRepository extends JpaRepository<TransfertFongibilite, Long> {

    /**
     * @param idCollectivite
     * @param annee
     * @return
     */
    @Query("select trf from TransfertFongibilite trf where trf.dotationCollectiviteOrigine.dotationDepartement.dotationSousProgramme.dotationProgramme.annee=:annee"
            + " and trf.dotationCollectiviteOrigine.collectivite.id = :idCollectivite")
    List<TransfertFongibilite> rechercherTransfertsCollectiviteParAnnee(@Param("idCollectivite") Long idCollectivite, @Param("annee") Integer annee);

    /**
     * @param annee
     * @return
     */
    @Query("select trf from TransfertFongibilite trf where trf.dotationCollectiviteOrigine.dotationDepartement.dotationSousProgramme.dotationProgramme.annee=:annee")
    List<TransfertFongibilite> rechercherTransfertsParAnnee(@Param("annee") Integer annee);

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Email;

/**
 * Repository JPA de l'entité Email.
 *
 * @author a453029
 *
 */
@Repository
public interface EmailJpaRepository extends JpaRepository<Email, Long> {

}

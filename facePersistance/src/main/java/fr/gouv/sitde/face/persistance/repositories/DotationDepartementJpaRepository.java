/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;

/**
 * Repository JPA de l'entité DotationDepartement.
 *
 * @author Atos
 */
@Repository
public interface DotationDepartementJpaRepository extends JpaRepository<DotationDepartement, Long> {

    /**
     * Rechercher tous les éléments pour les départements donnés.
     *
     * @return the list
     */
    @Query("select distinct dd from DotationDepartement dd " + "inner join fetch dd.departement dep "
            + "inner join fetch dd.dotationSousProgramme dsp " + "inner join fetch dsp.sousProgramme sp " + "where dep in :listeDepartements "
            + "order by dep.code ")
    List<DotationDepartement> rechercherTousPourDepartements(@Param("listeDepartements") List<Departement> listeDepartements);

    /**
     * Pour l'import des dotation : recherche DD par année de DotationSousProgramme, type de sousprogramme et departement.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @param sprAbreviation
     *            the spr abreviation
     * @return the dotation departement
     */
    @Query("select distinct dd from DotationDepartement dd " + "inner join fetch dd.departement dep "
            + "inner join fetch dd.dotationSousProgramme dsp " + "inner join fetch dsp.dotationProgramme dp "
            + "inner join fetch dsp.sousProgramme spr "
            + "where dp.annee=:annee and dep.code = :departementCode and spr.codeDomaineFonctionnel = :codeDomaineFonctionnel "
            + "order by dd.departement.code")
    DotationDepartement rechercherDotationDepartementTroisCriteres(@Param("annee") Integer annee, @Param("departementCode") String departementCode,
            @Param("codeDomaineFonctionnel") String codeDomaineFonctionnel);

    /**
     * Ligne dotation departement en preparation existe.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @return the boolean
     */
    @Query("select case when count(ldp)> 0 then true else false end from LigneDotationDepartement ldp " + "inner join ldp.dotationDepartement dd "
            + "inner join dd.dotationSousProgramme dsp " + "inner join dsp.dotationProgramme dp " + "inner join dd.departement dep "
            + "where dp.annee=:annee and dep.code=:departementCode and ldp.dateEnvoi is null")
    Boolean ligneDotationDepartementEnPreparationExiste(@Param("annee") Integer annee, @Param("departementCode") String departementCode);

    /**
     * Ligne dotation departement notifiee existe.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @return the boolean
     */
    @Query("select case when count(ldp)> 0 then true else false end from LigneDotationDepartement ldp " + "inner join ldp.dotationDepartement dd "
            + "inner join dd.dotationSousProgramme dsp " + "inner join dsp.dotationProgramme dp " + "inner join dd.departement dep "
            + "where dp.annee=:annee and dep.code=:departementCode and ldp.dateEnvoi is not null")
    Boolean ligneDotationDepartementNotifieeExiste(@Param("annee") Integer annee, @Param("departementCode") String departementCode);

    /**
     * Rechercher par id.
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @return the optional
     */
    @Query("select distinct dde from DotationDepartement dde left join fetch dde.lignesDotationDepartement ldd inner join fetch dde.dotationSousProgramme dsp "
            + "inner join fetch dsp.dotationProgramme dp " + "inner join fetch dsp.sousProgramme spr " + "where dde.id = :dotationDepartementId")
    Optional<DotationDepartement> rechercherParId(@Param("dotationDepartementId") Long dotationDepartementId);

    /**
     * Rechercher dotation departement pour repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @param annee
     *            the annee
     * @param codeDomaineFonctionnel
     *            the code domaine fonctionnel
     * @return the dotation departement
     */
    @Query("select distinct dde from DotationDepartement dde " + "inner join fetch dde.departement dep "
            + "inner join fetch dde.dotationSousProgramme dsp " + "inner join fetch dsp.dotationProgramme dpr "
            + "inner join fetch dsp.sousProgramme spr "
            + "where dpr.annee=:annee and dep.code=:codeDepartement and spr.codeDomaineFonctionnel=:codeDomaineFonctionnel")
    DotationDepartement rechercherDotationDepartementPourRepartition(@Param("codeDepartement") String codeDepartement, @Param("annee") Integer annee,
            @Param("codeDomaineFonctionnel") String codeDomaineFonctionnel);

    /**
     * Rechercher dotation departement par id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation departement
     */
    @Query("select distinct dde from DotationDepartement dde " + "inner join fetch dde.departement dep "
            + "inner join fetch dde.dotationSousProgramme dsp " + "inner join fetch dep.collectivites cols "
            + "inner join fetch dsp.dotationProgramme dpr " + "inner join fetch dsp.sousProgramme spr "
            + "where cols.id = :idCollectivite and spr.id = :idSousProgramme and dpr.annee=:annee")
    DotationDepartement rechercherDotationDepartementParIdCollectivite(@Param("idCollectivite") Long idCollectivite,
            @Param("idSousProgramme") Integer idSousProgramme, @Param("annee") Integer annee);
}

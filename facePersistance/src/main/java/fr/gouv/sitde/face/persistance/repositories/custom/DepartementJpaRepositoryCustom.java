/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom;

import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Interface DepartementJpaRepositoryCustom.
 *
 * Atos
 */
public interface DepartementJpaRepositoryCustom {

    /**
     * Rechercher departements avec pagination & pénalités.
     *
     * @return the list
     */
    PageReponse<Departement> findAllDepartementsPagine(PageDemande pageDemande);

}

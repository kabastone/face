/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;

/**
 * Repository JPA de l'entité DotationProgramme.
 *
 * @author a702709
 *
 */
@Repository
public interface DotationSousProgrammeJpaRepository extends JpaRepository<DotationSousProgramme, Long> {

    /**
     * Find by current year.
     *
     * @param codeProgramme
     *            the code programme
     * @return the optional
     */
    @Query("select dsp from DotationSousProgramme dsp " + "inner join fetch dsp.dotationProgramme dp " + "inner join fetch dsp.sousProgramme sp "
            + "inner join fetch sp.programme p "
            + "where p.codeNumerique = :codeProgramme and dp.annee >= :anneeMinimale order by dsp.sousProgramme.numSousAction asc, dsp.dotationProgramme.annee desc")
    List<DotationSousProgramme> rechercherParAnneeMiniEtProgramme(@Param("codeProgramme") String codeProgramme,
            @Param("anneeMinimale") int anneeMinimale);

    /**
     * Rechercher par sous programme par annee en cours.
     *
     * @param sprAbreviation
     *            the spr abreviation
     * @param anneeEnCours
     *            the annee en cours
     * @return the dotation sous programme
     */
    @Query("select distinct dsp from DotationSousProgramme dsp " + "inner join dsp.dotationProgramme dp " + "inner join  dsp.sousProgramme sp "
            + "where dp.annee=:anneeEnCours and sp.codeDomaineFonctionnel=:sprCodeFonctionnel")
    Optional<DotationSousProgramme> rechercherParSousProgrammeParAnneeEnCours(@Param("sprCodeFonctionnel") String sprCodeFonctionnel,
            @Param("anneeEnCours") int anneeEnCours);

    /**
     * Rechercher par id.
     *
     * @param idDotationSousProgramme
     *            the id dotation sous programme
     * @return the optional
     */
    @Query("select dsp from DotationSousProgramme dsp " + "inner join fetch dsp.dotationProgramme dpr " + "inner join fetch dpr.programme p "
            + "inner join fetch dsp.sousProgramme spr " + "where dsp.id = :idDotationSousProgramme")
    Optional<DotationSousProgramme> rechercherParId(@Param("idDotationSousProgramme") Long idDotationSousProgramme);

    /**
     * Recuperer dotation sous programme par annee et sous programme.
     *
     * @param annee
     *            the annee
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation sous programme
     */
    @Query("select dsp from DotationSousProgramme dsp " + "inner join fetch dsp.dotationProgramme dpr " + "inner join fetch dsp.sousProgramme spr "
            + "where spr.id = :idSousProgramme and dpr.annee = :annee")
    DotationSousProgramme recupererDotationSousProgrammeParAnneeEtSousProgramme(@Param("annee") int annee,
            @Param("idSousProgramme") Integer idSousProgramme);
}

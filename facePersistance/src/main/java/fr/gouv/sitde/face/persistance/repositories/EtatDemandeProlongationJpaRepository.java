/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;

/**
 * The Interface EtatDemandeProlongationJpaRepository.
 *
 * @author Atos
 */
@Repository
public interface EtatDemandeProlongationJpaRepository extends JpaRepository<EtatDemandeProlongation, Long> {

    /**
     * Find by code.
     *
     * @param codeEtatDemandeProlongation
     *            the code etat demande prolongation
     * @return the etat demande prolongation
     */
    @Query("select ed from EtatDemandeProlongation ed where ed.code = :codeEtatDemandeProlongation")
    EtatDemandeProlongation findByCode(@Param("codeEtatDemandeProlongation") String codeEtatDemandeProlongation);
}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * Repository JPA de l'entité DemandeSubvention.
 *
 * @author Atos
 */
@Repository
public interface DemandeSubventionJpaRepository extends JpaRepository<DemandeSubvention, Long> {

    /**
     * Recherche des demandes de subvention via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @Query("select demande from DemandeSubvention demande inner join fetch demande.dossierSubvention dossier "
            + "where dossier.id = :idDossierSubvention order by demande.dateCreation asc")
    // + "where dossier.id = :idDossierSubvention order by demande.dateDemande DESC, demande.id DESC")
    List<DemandeSubvention> findByDossierSubvention(@Param("idDossierSubvention") Long idDossierSubvention);

    /**
     * Recherche d'une demande de subvention avec données nécessaires pour le pdf.
     *
     * @param idDemandeSubvention
     *            ID de la demande de subvention désirée.
     *
     * @return La demande de subvention.
     *
     */
    @Query("select demande from DemandeSubvention demande" + " join fetch demande.dossierSubvention ds" + " join fetch ds.dotationCollectivite dc"
            + " join fetch dc.collectivite co" + " join fetch dc.dotationDepartement dpe" + " join fetch dpe.dotationSousProgramme dsp"
            + " join fetch dsp.sousProgramme sp" + " join fetch sp.programme pr" + " where demande.id = :idDemandeSubvention")
    Optional<DemandeSubvention> rechercheDemandeSubventionPourPdfParId(@Param("idDemandeSubvention") Long idDemandeSubvention);

    /**
     * Recherche demande subvention par id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the optional
     */
    @Query("select demande from DemandeSubvention demande" + " join fetch demande.dossierSubvention ds" + " left join fetch demande.documents "
            + " left join fetch demande.anomalies " + " join fetch ds.dotationCollectivite dc" + " join fetch dc.dotationDepartement dd"
            + " join fetch dd.dotationSousProgramme dsp" + " join fetch dsp.sousProgramme sp" + " where demande.id = :idDemandeSubvention")
    Optional<DemandeSubvention> rechercheDemandeSubventionParId(@Param("idDemandeSubvention") Long idDemandeSubvention);

    /**
     * Find principale by dossier subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the optional
     */
    @Query("select demande from DemandeSubvention demande" + " join fetch demande.dossierSubvention ds"
            + " where (ds.chorusIdAe = :idDossierChorus and demande.demandeComplementaire = false and demande.etatDemandeSubvention.code not in ('REFUSEE', 'REJET_TAUX'))")
    Optional<DemandeSubvention> findPrincipaleByDossierChorus(@Param("idDossierChorus") Long idDossierChorus);

    @Query("select demande from DemandeSubvention demande" + " join fetch demande.dossierSubvention ds"
            + " where (ds.id = :idDossier and demande.demandeComplementaire = false and demande.etatDemandeSubvention.code not in ('REFUSEE', 'REJET_TAUX'))")
    Optional<DemandeSubvention> findPrincipaleByDossier(@Param("idDossier") Long idDossier);

    /**
     * Find max chorus num ligne legacy.
     *
     * @param idDossier
     *            the id dossier
     * @return the optional
     */
    @Query("select max(demande.chorusNumLigneLegacy) from DemandeSubvention demande" + " join demande.dossierSubvention ds"
            + " where ds.id = :idDossier")
    Optional<String> findMaxChorusNumLigneLegacy(@Param("idDossier") Long idDossier);

    /**
     * Rechercher par E jet poste.
     *
     * @param numeroEJ
     *            the numero EJ
     * @param numeroPoste
     *            the numero poste
     * @return the demande subvention
     */
    @Query("select distinct(dsu) from DemandeSubvention dsu inner join dsu.dossierSubvention dos " + "where dos.chorusNumEj = :numeroEJ "
            + "and dsu.chorusNumLigneLegacy = :numeroPoste")
    DemandeSubvention rechercherParEJetPoste(@Param("numeroEJ") String numeroEJ, @Param("numeroPoste") String numeroPoste);

    @Query("select distinct(dsu) from DemandeSubvention dsu inner join dsu.dossierSubvention dos " + "where dos.chorusNumEj = :numeroEJ "
            + "and abs(dsu.plafondAide - :plafond) < 0.0001 " + "and dsu.etatDemandeSubvention in " + "(select eds.id from EtatDemandeSubvention eds "
            + "where eds.code = 'EN_ATTENTE_TRANSFERT_MAN' or eds.code = 'INCOHERENCE_CHORUS')")
    Optional<DemandeSubvention> rechercherParEJQuantityEtat(@Param("numeroEJ") String numeroEJ, @Param("plafond") BigDecimal plafond);

    @Query("select distinct dem " + "from DemandeSubvention dem join fetch dem.etatDemandeSubvention eds "
            + "join fetch dem.dossierSubvention dos join fetch dem.documents "
            + "join fetch dos.dotationCollectivite dco join fetch dco.collectivite col "
            + "join fetch dco.dotationDepartement dde join fetch dde.dotationSousProgramme dsp join fetch dsp.sousProgramme spr "
            + "join fetch spr.programme pro " + "where dem.demandeComplementaire = FALSE and eds.code='EN_ATTENTE_TRANSFERT' "
            + "and EXISTS (select 1 from dem.documents doc3 join doc3.typeDocument tdo3 where tdo3.code='ETAT_PREVISIONNEL_PDF') "
            + "and EXISTS (select 1 from dem.documents doc4 join doc4.typeDocument tdo4 where tdo4.code='FICHE_SIRET') ")
    List<DemandeSubvention> obtenirDtoPourBatchFen0111A();

    /**
     * @param idDossier
     * @return
     */
    @Query("select demande from DemandeSubvention demande inner join fetch demande.dossierSubvention dossier "
            + "where dossier.id = :idDossierSubvention order by demande.dateDemande ASC, demande.id DESC")
    List<DemandeSubvention> findByDossierSubventionOrdonneeParDateDemande(@Param("idDossierSubvention") Long idDossier);

    /**
     * Compter nombre demande subvention par dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the integer
     */
    @Query("select count(dsu) from DemandeSubvention dsu inner join dsu.dossierSubvention dos where dos.id = :idDossier")
    Integer compterNombreDemandeSubventionParDossier(@Param("idDossier") Long idDossier);
}

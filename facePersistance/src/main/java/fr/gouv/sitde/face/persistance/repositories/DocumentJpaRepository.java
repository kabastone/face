/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;

/**
 * Repository JPA de l'entité Document.
 *
 * @author Atos
 */
@Repository
public interface DocumentJpaRepository extends JpaRepository<Document, Long> {

    /**
     * Find document demande subvention by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.demandeSubvention demSub " + "inner join fetch demSub.dossierSubvention "
            + "where doc.id=:id")
    Optional<Document> findDocumentDemandeSubventionById(@Param("id") Long id);

    /**
     * Find document demande prolongation by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.demandeProlongation demPro " + "inner join fetch demPro.dossierSubvention "
            + "inner join fetch doc.typeDocument td " + "inner join fetch td.familleDocument " + "where doc.id=:id")
    Optional<Document> findDocumentDemandeProlongationById(@Param("id") Long id);

    /**
     * Find document demande paiement by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.demandePaiement demPai " + "inner join fetch demPai.dossierSubvention "
            + "inner join fetch doc.typeDocument td " + "inner join fetch td.familleDocument " + "where doc.id=:id")
    Optional<Document> findDocumentDemandePaiementById(@Param("id") Long id);

    /**
     * Find document courrier departement by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.courrierAnnuelDepartement couDep " + "inner join fetch doc.typeDocument td "
            + "inner join fetch td.familleDocument " + "where doc.id=:id")
    Optional<Document> findDocumentCourrierAnnuelDepartementById(@Param("id") Long id);

    /**
     * Find document ligne dotation departement by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.ligneDotationDepartement liDotDep "
            + "inner join fetch liDotDep.dotationDepartement " + "inner join fetch doc.typeDocument td " + "inner join fetch td.familleDocument "
            + "where doc.id=:id")
    Optional<Document> findDocumentLigneDotationDepartementById(@Param("id") Long id);

    /**
     * Find document modele by id.
     *
     * @param id
     *            the id
     * @return the optional
     */
    @Query("select doc from Document doc " + "inner join fetch doc.typeDocument td " + "inner join fetch td.familleDocument fam "
            + "where fam.idCodifie=600 and doc.id=:id")
    Optional<Document> findDocumentModeleById(@Param("id") Long id);

    /**
     * Find famille document by id document.
     *
     * @param idDocument
     *            the id document
     * @return the optional
     */
    @Query("select fam from Document doc " + "inner join doc.typeDocument td " + "inner join td.familleDocument fam " + "where doc.id=:id")
    Optional<FamilleDocument> findFamilleDocumentByIdDocument(@Param("id") Long idDocument);

    /**
     * Rechercher documents dotation par id collectivite et annee.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @param courrierRepartition
     *            the courrier repartition
     * @param courrierDotation
     *            the courrier dotation
     * @param renoncementMontantDotation
     *            the renoncement montant dotation
     * @return the list
     */
    @Query("select doc from Document doc " + " inner join fetch doc.typeDocument td " + " inner join fetch td.familleDocument fam "
            + " left join fetch doc.courrierAnnuelDepartement dac1 " + " left join fetch dac1.departement dep1 "
            + " left join fetch doc.ligneDotationDepartement ldd2 " + " left join fetch ldd2.dotationDepartement dde2 "
            + " left join fetch dde2.dotationsCollectivite dco2 " + " left join dde2.dotationSousProgramme dsp2 "
            + " left join dsp2.dotationProgramme dpr2 " + " where (dac1.annee = :annee and td.code in (:courrierRepartition, :courrierDotation)"
            + " and exists (select 1 from Collectivite col1 where col1.id = :idCollectivite and col1.departement = dep1)) "
            + " or (dco2.collectivite.id = :idCollectivite and dpr2.annee = :annee and td.code in (:renoncementMontantDotation))")
    List<Document> rechercherDocumentsDotationParIdCollectiviteEtAnnee(@Param("idCollectivite") Long idCollectivite, @Param("annee") Integer annee,
            @Param("courrierRepartition") String courrierRepartition, @Param("courrierDotation") String courrierDotation,
            @Param("renoncementMontantDotation") String renoncementMontantDotation);

    /**
     * Rechercher documents dotation par code departement et annee.
     *
     * @param codeDepartement
     *            the code departement
     * @param annee
     *            the annee
     * @param courrierRepartition
     *            the courrier repartition
     * @param courrierDotation
     *            the courrier dotation
     * @param renoncementMontantDotation
     *            the renoncement montant dotation
     * @return the list
     */
    @Query("select doc from Document doc " + " inner join fetch doc.typeDocument td " + " inner join td.familleDocument fam "
            + " left join doc.courrierAnnuelDepartement dac1 " + " left join dac1.departement dep1 " + " left join doc.ligneDotationDepartement ldd2 "
            + " left join ldd2.dotationDepartement dde2 " + " left join dde2.departement dep2 " + " left join dde2.dotationSousProgramme dsp2 "
            + " left join dsp2.dotationProgramme dpr2 "
            + " where (dep1.code = :codeDepartement and dac1.annee = :annee and td.code in (:courrierRepartition, :courrierDotation)) "
            + " or (dep2.code = :codeDepartement and dpr2.annee = :annee and td.code in (:renoncementMontantDotation))")
    List<Document> rechercherDocumentsDotationParCodeDepartementEtAnnee(@Param("codeDepartement") String codeDepartement,
            @Param("annee") Integer annee, @Param("courrierRepartition") String courrierRepartition,
            @Param("courrierDotation") String courrierDotation, @Param("renoncementMontantDotation") String renoncementMontantDotation);
}

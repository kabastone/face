/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.custom.DossierSubventionJpaRepositoryCustom;
import fr.gouv.sitde.face.persistance.repositories.custom.RecherchePaiementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Interface DossierSubventionJpaRepository.
 */
@Repository
public interface DossierSubventionJpaRepository
        extends JpaRepository<DossierSubvention, Long>, DossierSubventionJpaRepositoryCustom, RecherchePaiementJpaRepositoryCustom {

    /**
     * Find dossier subvention by id.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the optional
     */
    @Query("select distinct ds from DossierSubvention ds " + " left join fetch ds.demandesPaiement dpa" + " left join fetch dpa.etatDemandePaiement "
            + "left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite c" + "left join fetch ds.demandesSubvention dsubs "
            + "left join fetch dsubs.etatDemandeSubvention " + "left join fetch dc.dotationDepartement dpe "
            + "left join fetch dpe.dotationSousProgramme dsp " + "left join fetch dsp.dotationProgramme dp " + "left join fetch dsp.sousProgramme sp "
            + "where ds.id = :idDossierSubvention")
    Optional<DossierSubvention> findDossierSubventionById(@Param("idDossierSubvention") Long idDossierSubvention);

    /**
     * Rechercher dossiers parnumero.
     *
     * @param numeroDossier
     *            the numero dossier
     * @return the dossier subvention
     */
    @Query("select distinct ds from DossierSubvention ds " + " left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite c"
            + "left join fetch dc.dotationDepartement dpe " + "left join fetch dpe.dotationSousProgramme dsp "
            + "left join fetch dsp.dotationProgramme dp " + "left join fetch dsp.sousProgramme sp " + "where ds.numDossier = :numeroDossier")
    Optional<DossierSubvention> rechercherDossierParNumero(@Param("numeroDossier") String numeroDossier);

    /**
     * Rechercher dossiers pour mail relance.
     *
     * @param dateRelanceMail
     *            the date relance mail
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds"
            + " join fetch ds.dotationCollectivite dc join fetch dc.collectivite c left join fetch c.adressesEmail em where"
            + " not exists (select 1 from Email em join em.dossierSubvention ds2 where em.typeEmail = 'DOS_RELANCE' and ds2.id = ds.id)"
            + " and  ds.dateEcheanceAchevement <= cast(:dateRelanceMail as timestamp)" + " and ds.etatDossier = 'OUVERT'")
    List<DossierSubvention> rechercherDossiersPourMailRelance(@Param("dateRelanceMail") LocalDateTime dateRelanceMail);

    /**
     * Rechercher dossiers pour mail expiration.
     *
     * @param dateExpirationMail
     *            the date expiration mail
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds left join fetch ds.demandesSubvention join fetch ds.dotationCollectivite dc"
            + " join fetch dc.collectivite c left join fetch c.adressesEmail em where"
            + " not exists (select 1 from Email em join em.dossierSubvention ds2 where em.typeEmail = 'DOS_EXPIRATION' and ds2.id = ds.id)"
            + " and  ds.dateEcheanceAchevement <= cast(:dateExpirationMail as timestamp)" + " and ds.etatDossier = 'OUVERT'")
    List<DossierSubvention> rechercherDossiersPourMailExpiration(@Param("dateExpirationMail") LocalDateTime dateExpirationMail);

    /**
     * Checks if is numero dossier unique.
     *
     * @param idDossier
     *            the id dossier
     * @param numDossier
     *            the num dossier
     * @return true, if is numero dossier unique
     */
    @Query("select count(ds) from DossierSubvention ds where ds.id <> :idDossier AND ds.numDossier = :numDossier")
    int countDossierPourNumeroDossier(@Param("idDossier") Long idDossier, @Param("numDossier") String numDossier);

    /**
     * Rechercher dossier par id chorus ae.
     *
     * @param idAe
     *            the id ae
     * @return the optional
     */
    @Query("select dos from DossierSubvention dos where dos.chorusIdAe = :idAe")
    Optional<DossierSubvention> rechercherDossierParIdChorusAe(@Param("idAe") Long idAe);

    /**
     * Supprimer dossier subvention par id.
     *
     * @param idDossier
     *            the id dossier
     */
    @Modifying
    @Query("delete from DossierSubvention dos where dos.id = :idDossier")
    void supprimerDossierSubventionParId(@Param("idDossier") Long idDossier);

    /**
     * Recherche un dossier selon criteres discriminants venant de la creation d'une demande de subvention.
     *
     * @param annee
     *            the annee
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds " + "left join fetch ds.demandesPaiement dpa " + "left join fetch dpa.etatDemandePaiement "
            + "left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite col " + "left join fetch ds.demandesSubvention dsubs "
            + "left join fetch dsubs.etatDemandeSubvention " + "left join fetch dc.dotationDepartement dpe "
            + "left join fetch dpe.dotationSousProgramme dsp " + "left join fetch dsp.dotationProgramme dp " + "left join fetch dsp.sousProgramme sp "
            + "where dp.annee = :annee and col.id = :idCollectivite and sp.id = :idSousProgramme")
    List<DossierSubvention> rechercherDossierParAnneeCollectiviteEtSousProgramme(@Param("annee") int annee,
            @Param("idCollectivite") Long idCollectivite, @Param("idSousProgramme") Integer idSousProgramme);

    /**
     * Recuperer tout numero dossier par dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the list
     */
    @Query("select ds.numDossier from DossierSubvention ds " + "left join ds.dotationCollectivite dc " + "where dc.id = :idDotationCollectivite")
    List<String> recupererToutNumeroDossierParDotationCollectivite(@Param("idDotationCollectivite") Long idDotationCollectivite);

    /**
     * Recher dossier subvention cadencement defaut.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds " + "left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite col "
            + "left join fetch dc.dotationDepartement dpe " + "left join fetch dpe.dotationSousProgramme dsp "
            + "left join fetch dsp.dotationProgramme dp " + "left join fetch dp.programme p "
            + "left join fetch col.departement left join fetch ds.demandesSubvention dsubs " + "left join fetch dsubs.cadencement cd "
            + "where cd.estValide = false and p.codeNumerique =:codeProgramme ")
    List<DossierSubvention> recherDossierSubventionCadencementDefaut(@Param("codeProgramme") String codeProgramme);

    /**
     * Rechercher dossier subvention en retard.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds " + "left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite col "
            + "left join fetch dc.dotationDepartement dpe " + "left join fetch dpe.dotationSousProgramme dsp "
            + "left join fetch dsp.dotationProgramme dp " + "left join fetch dp.programme p " + "left join fetch col.departement dp "
            + "where exists (select dsubs from DemandeSubvention dsubs join dsubs.etatDemandeSubvention eds "
            + "where dsubs.dossierSubvention.id = ds.id and eds.code = 'ATTRIBUEE') "
            + "and not exists (select dpa from DemandePaiement dpa join dpa.etatDemandePaiement edpa "
            + "where dpa.dossierSubvention.id = ds.id and edpa.code <> 'REFUSE' and (dpa.typeDemandePaiement in ('SOLDE', 'ACOMPTE'))) "
            + "and current_date > ds.dateEcheanceLancement and p.codeNumerique =:codeProgramme ")
    List<DossierSubvention> rechercherDossierSubventionEnRetard(@Param("codeProgramme") String codeProgramme);

    /**
     * Rechercher dossier subvention pour mail engagement travaux.
     *
     * @return the list
     */
    @Query("select distinct ds from DossierSubvention ds " + "left join fetch ds.dotationCollectivite dc " + "left join fetch dc.collectivite col " 
    		+ "join fetch col.adressesEmail aem where  exists (select dsubs from DemandeSubvention dsubs join dsubs.dossierSubvention dsubs_dos "
    		+ "join dsubs.etatDemandeSubvention eds where dsubs_dos.id = ds.id and eds.code = 'ATTRIBUEE') "
            + "and not exists (select dpa from DemandePaiement dpa join dpa.dossierSubvention dpa_dos where dpa_dos.id = ds.id and "
            + "dpa.typeDemandePaiement in ('SOLDE', 'ACOMPTE')) and date_part('year', ds.dateEcheanceLancement) = :annee")
    List<DossierSubvention> rechercherDossierSubventionPourMailEngagementTravaux(@Param("annee") int annee);

}

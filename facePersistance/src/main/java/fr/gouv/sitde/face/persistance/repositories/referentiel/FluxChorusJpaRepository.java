/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Interface FluxChorusJpaRepository.
 */
public interface FluxChorusJpaRepository extends JpaRepository<FluxChorus, Long> {

    /**
     * Rechercher flux chorus.
     *
     * @return the optional
     */
    @Query("select fch from FluxChorus fch where fch.actif = true")
    Optional<FluxChorus> rechercherFluxChorus();

}

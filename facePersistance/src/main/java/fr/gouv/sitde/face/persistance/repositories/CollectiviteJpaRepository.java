/*
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Interface CollectiviteJpaRepository.
 */
@Repository
public interface CollectiviteJpaRepository extends JpaRepository<Collectivite, Long> {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#findAll()
     */
    @Override
    @Query("select c from Collectivite c order by c.nomCourt")
    List<Collectivite> findAll();

    /**
     * Find collectivites with mail list by code departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    @Query("select distinct col from Collectivite col " + "left join fetch col.adressesEmail adr " + "left join fetch col.departement dep "
            + "where dep.code=:codeDepartement")
    List<Collectivite> findCollectivitesWithMailListByCodeDepartement(@Param("codeDepartement") String codeDepartement);

    /**
     * Find id collectivite by id dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the long
     */
    @Query("select col.id from DotationCollectivite dot " + "inner join dot.collectivite col " + "where dot.id=:idDotationCollectivite")
    Long findIdCollectiviteByIdDotationCollectivite(@Param("idDotationCollectivite") Long idDotationCollectivite);

    /**
     * Find id collectivite by id dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the long
     */
    @Query("select col.id from DossierSubvention dos " + "inner join dos.dotationCollectivite dot " + "inner join dot.collectivite col "
            + "where dos.id=:idDossier")
    Long findIdCollectiviteByIdDossier(@Param("idDossier") Long idDossier);

    /**
     * Find id collectivite by id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the long
     */
    @Query("select col.id from DemandeSubvention dem " + "inner join dem.dossierSubvention dos " + "inner join dos.dotationCollectivite dot "
            + "inner join dot.collectivite col " + "where dem.id=:idDemandeSubvention")
    Long findIdCollectiviteByIdDemandeSubvention(@Param("idDemandeSubvention") Long idDemandeSubvention);

    /**
     * Find id collectivite by id demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the long
     */
    @Query("select col.id from DemandeProlongation dem " + "inner join dem.dossierSubvention dos " + "inner join dos.dotationCollectivite dot "
            + "inner join dot.collectivite col " + "where dem.id=:idDemandeProlongation")
    Long findIdCollectiviteByIdDemandeProlongation(@Param("idDemandeProlongation") Long idDemandeProlongation);

    /**
     * Find id collectivite by id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the long
     */
    @Query("select col.id from DemandePaiement dem " + "inner join dem.dossierSubvention dos " + "inner join dos.dotationCollectivite dot "
            + "inner join dot.collectivite col " + "where dem.id=:idDemandePaiement")
    Long findIdCollectiviteByIdDemandePaiement(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Find id collectivite by id doc demande subvention.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    @Query("select col.id from Document doc " + "inner join doc.demandeSubvention dem " + "inner join dem.dossierSubvention dos "
            + "inner join dos.dotationCollectivite dot " + "inner join dot.collectivite col " + "where doc.id=:idDocument")
    Long findIdCollectiviteByIdDocDemandeSubvention(@Param("idDocument") Long idDocument);

    /**
     * Find id collectivite by id doc demande prolongation.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    @Query("select col.id from Document doc " + "inner join doc.demandeProlongation dem " + "inner join dem.dossierSubvention dos "
            + "inner join dos.dotationCollectivite dot " + "inner join dot.collectivite col " + "where doc.id=:idDocument")
    Long findIdCollectiviteByIdDocDemandeProlongation(@Param("idDocument") Long idDocument);

    /**
     * Find id collectivite by id doc demande paiement.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    @Query("select col.id from Document doc " + "inner join  doc.demandePaiement dem " + "inner join dem.dossierSubvention dos "
            + "inner join dos.dotationCollectivite dot " + "inner join dot.collectivite col " + "where doc.id=:idDocument")
    Long findIdCollectiviteByIdDocDemandePaiement(@Param("idDocument") Long idDocument);

    /**
     * Find id collectivites by id doc courrier annuel departement.
     *
     * @param idDocument
     *            the id document
     * @return the list
     */
    @Query("select col.id from Collectivite col " + "inner join col.departement depCol " + "where exists(select 1 from Document doc "
            + "inner join doc.courrierAnnuelDepartement cad " + "inner join cad.departement depDoc "
            + "where doc.id=:idDocument and depCol = depDoc)")
    List<Long> findIdCollectivitesByIdDocCourrierDepartement(@Param("idDocument") Long idDocument);

    /**
     * Find id collectivites by id doc ligne dotation departement.
     *
     * @param idDocument
     *            the id document
     * @return the list
     */
    @Query("select col.id from Collectivite col " + "inner join col.departement depCol " + "where exists(select 1 from Document doc "
            + "inner join doc.ligneDotationDepartement lidot " + "inner join lidot.dotationDepartement dot " + "inner join dot.departement depDoc "
            + "where doc.id=:idDocument and depCol.id=depDoc.id)")
    List<Long> findIdCollectivitesByIdDocLigneDotationDepartement(@Param("idDocument") Long idDocument);

    /**
     * Find all collectivite avec mail.
     *
     * @return the list
     */
    @Query("select distinct col from Collectivite col join fetch col.adressesEmail adr")
    List<Collectivite> findAllCollectiviteAvecMail();

    /**
     * @param idCollectivite
     * @return
     */
    @Query("select distinct col from Collectivite col join fetch col.departement dep left join fetch col.adresseMoa left join fetch col.adresseMoe where col.id=:idCol")
    Optional<Collectivite> rechercherById(@Param("idCol") Long idCollectivite);

    /**
     * @param siret
     * @return the boolean
     */
    @Query("select (count(*) > 0) from Collectivite col where col.siret=:siret")
    boolean existsBySiret(@Param("siret") String siret);

    /**
     * @param idCollectivite,
     *            siret
     * @return the boolean
     */
    @Query("select (count(*) > 0) from Collectivite col where col.id !=:idCol and col.siret=:siret")
    boolean existsByCollectiviteAndSiret(@Param("idCol") Long idCollectivite, @Param("siret") String siret);

    /**
     * @param i
     * @return
     */
    @Query("select distinct col from Collectivite col join fetch col.adressesEmail where"
            + " exists(select dos from DossierSubvention dos where dos.dotationCollectivite.collectivite.id=col.id"
            + " and year(dos.dateEcheanceAchevement) >= :annee)")
    List<Collectivite> rechercherCollectivitePourCadencementMaj(@Param("annee") int annee);

    /**
     * @return
     */
    @Query("select distinct col from Collectivite col join fetch col.adressesEmail where " + "exists(select cad from Cadencement cad where "
            + "cad.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite.id=col.id " + "and cad.estValide = false)")
    List<Collectivite> rechercherCollectivitePourMailCadencementNonValide();

    /**
     * Rechercher collectivite pour mail dotation non consomme.
     *
     * @return the list
     */
    @Query("select distinct col from Collectivite col left join fetch col.adressesEmail left join fetch col.dotationsCollectivite dcol left join fetch dcol.dotationDepartement ddp "
            + "left join fetch ddp.dotationSousProgramme dsp left join fetch dsp.sousProgramme spr where spr.deProjet = false and (dcol.montant - dcol.dotationRepartie) > 0 "
            + "and dsp.dotationProgramme.annee = :annee")
    List<Collectivite> rechercherCollectivitePourMailDotationNonConsomme(@Param("annee") int annee);
}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DotationProgramme;

/**
 * Repository JPA de l'entité DotationProgramme.
 *
 * @author a702709
 *
 */
@Repository
public interface DotationProgrammeJpaRepository extends JpaRepository<DotationProgramme, Long> {

    /**
     * Rechercher par id.
     *
     * @param idDotationProgramme
     *            the id dotation programme
     * @return the optional
     */
    @Query("select distinct dpr from DotationProgramme dpr left join fetch dpr.dotationsSousProgrammes inner join fetch dpr.programme pro "
            + "where dpr.id = :idDotationProgramme")
    Optional<DotationProgramme> rechercherParId(@Param("idDotationProgramme") Long idDotationProgramme);
}

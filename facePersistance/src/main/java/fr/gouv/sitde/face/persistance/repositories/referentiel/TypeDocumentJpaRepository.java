/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.TypeDocument;

/**
 * Repository JPA de l'entité TypeDocument.
 *
 * @author Atos
 */
@Repository
public interface TypeDocumentJpaRepository extends JpaRepository<TypeDocument, Long> {

    /**
     * Rechercher par idCodifie.
     *
     * @return the optional
     */
    @Query("select td from TypeDocument td " + "inner join fetch td.familleDocument " + "where td.idCodifie=:idCodifie")
    Optional<TypeDocument> findByIdCodifie(@Param("idCodifie") Integer idCodifie);
}

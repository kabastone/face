/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Reglementaire;

/**
 * @author A687370
 *
 */
@Repository
public interface ReglementaireJpaRepository extends JpaRepository<Reglementaire, Long> {

    /**
     * Find all reglementaire.
     *
     * @return the list
     */
    @Query("select reg from Reglementaire reg where reg.anneeMiseEnApplication = (SELECT max(r.anneeMiseEnApplication) from Reglementaire r where r.anneeMiseEnApplication <= :annee)")
    Optional<Reglementaire> findReglementaire(@Param("annee") Integer annee);

    /**
     * @return
     */
    @Query("select reg from Reglementaire reg where reg.anneeMiseEnApplication = (SELECT min(r.anneeMiseEnApplication) from Reglementaire r)")
    Optional<Reglementaire> findMinReglementaire();
}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * Repository de l'entite Chorus suivi batch
 *
 * @author Atos
 *
 */
@Repository
public interface ChorusSuiviBatchJpaRepository extends JpaRepository<ChorusSuiviBatch, Boolean> {

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordGeneCustomJpaRepository;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordGeneCustomJpaRepositoryImpl;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * Repository JPA du tableau de bord generique.
 *
 * @author a453029
 *
 */
@Repository
public interface TableauDeBordGeneJpaRepository extends PagingAndSortingRepository<DotationCollectivite, Long>, TableauDeBordGeneCustomJpaRepository {

    /**
     * Rechercher dotations collectivite tdb ademander subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param anneeCourante
     *            the annee courante
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_DEMANDER_SUBVENTION_COUNT)
    Page<DotationCollectivite> rechercherDotationsCollectiviteTdbAdemanderSubvention(@Param("idCollectivite") Long idCollectivite,
            @Param("anneeCourante") int anneeCourante, Pageable pageable);

    /**
     * Rechercher dossiers subvention tdb ademander paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_DEMANDER_PAIEMENT_COUNT)
    Page<DossierSubvention> rechercherDossiersSubventionTdbAdemanderPaiement(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("typePaiementSolde") TypeDemandePaiementEnum typePaiement,
            Pageable pageable);

    /**
     * Rechercher dossiers subvention tdb acompleter subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_COMPLETER_SUBVENTION_COUNT)
    Page<DotationCollectivite> rechercherDotationCollectiviteTdbAcompleterSubvention(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("anneeCourante") int annee, Pageable pageable);

    /**
     * Rechercher demandes subvention tdb acorriger subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubAnomalieSignalee
     *            the etat dem sub anomalie signalee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_CORRIGER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAcorrigerSubvention(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("etatDemSubAnomalieSignalee") String etatDemSubAnomalieSignalee,
            Pageable pageable);

    /**
     * Rechercher demandes paiement tdb acorriger subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiAnomalieSignalee
     *            the etat dem pai anomalie signalee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_CORRIGER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAcorrigerPaiement(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("etatDemPaiAnomalieSignalee") String etatDemPaiAnomalieSignalee,
            Pageable pageable);

    /**
     * Rechercher dossiers subvention tdb acommencer paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param dateDuJourPlus3Mois
     *            the date du jour moins 3 mois
     * @param dateDuJour
     *            the date du jour
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_COMMENCER_PAIEMENT_COUNT)
    Page<DossierSubvention> rechercherDossiersSubventionTdbAcommencerPaiement(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("dateDuJourPlus3Mois") LocalDateTime dateDuJourMoins3Mois,
            @Param("dateDuJour") LocalDateTime dateDuJour, Pageable pageable);

    /**
     * Rechercher dossiers subvention tdb asolder paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param dateDuJourPlus4Mois
     *            the date du jour plus 4 mois
     * @param dateDuJour
     *            the date du jour
     * @param typePaiementSolde
     *            the type paiement solde
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_SOLDER_PAIEMENT,
            countQuery = TableauDeBordGeneCustomJpaRepositoryImpl.REQUETE_TDB_GENE_A_SOLDER_PAIEMENT_COUNT)
    Page<DossierSubvention> rechercherDossiersSubventionTdbAsolderPaiement(@Param("idCollectivite") Long idCollectivite,
            @Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert, @Param("dateDuJourPlus4Mois") LocalDateTime dateDuJourMoins4Mois,
            @Param("dateDuJour") LocalDateTime dateDuJour, @Param("typePaiementSolde") TypeDemandePaiementEnum typePaiementSolde, Pageable pageable);
}

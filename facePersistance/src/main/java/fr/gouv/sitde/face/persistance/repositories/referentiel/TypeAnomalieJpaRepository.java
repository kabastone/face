/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;

/**
 * @author A754839
 *
 */
public interface TypeAnomalieJpaRepository extends JpaRepository<TypeAnomalie, Integer> {

    @Query("select ta from TypeAnomalie ta where ta.code = :codeAnomalie")
    Optional<TypeAnomalie> rechercherParCode(@Param("codeAnomalie") String codeAnomalie);

    @Query("select ta from TypeAnomalie ta where ta.pourDemandePaiement = true")
    List<TypeAnomalie> rechercherTousPourDemandePaiement();

    @Query("select ta from TypeAnomalie ta where ta.pourDemandePaiement = false")
    List<TypeAnomalie> rechercherTousPourDemandeSubvention();
}

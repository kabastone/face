/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * The Interface DroitAgentJpaRepository.
 *
 * @author a453029
 */
public interface DroitAgentJpaRepository extends JpaRepository<DroitAgent, Long> {

    /**
     * Find by utilisateur and collectivite.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @param idCollectivite
     *            the id collectivite
     * @return the optional
     */
    @Query("select da from DroitAgent da " + "inner join fetch da.utilisateur uti " + "inner join fetch da.collectivite col "
            + "where uti.id = :idUtilisateur and col.id = :idCollectivite")
    Optional<DroitAgent> findByUtilisateurAndCollectivite(@Param("idUtilisateur") Long idUtilisateur, @Param("idCollectivite") Long idCollectivite);

    /**
     * Find by collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    @Query("select da from DroitAgent da " + "inner join fetch da.utilisateur uti " + "inner join fetch da.collectivite col "
            + "where col.id = :idCollectivite")
    List<DroitAgent> findByCollectivite(@Param("idCollectivite") Long idCollectivite);

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordSd7CustomJpaRepository;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordSd7CustomJpaRepositoryImpl;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;

/**
 * Repository JPA du tableau de bord SD7.
 *
 * @author a453029
 *
 */
@Repository
public interface TableauDeBordSd7JpaRepository extends PagingAndSortingRepository<DossierSubvention, Long>, TableauDeBordSd7CustomJpaRepository {

    /**
     * Rechercher demandes subvention tdb acontroler subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubAccordee
     *            the etat dem sub accordee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION,
            countQuery = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAcontrolerSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubAccordee") String etatDemSubAccordee, Pageable pageable);

    /**
     * Rechercher demandes paiement tdb acontroler paiement.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiAccordee
     *            the etat dem pai accordee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT,
            countQuery = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAcontrolerPaiement(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemPaiAccordee") String etatDemPaiAccordee, Pageable pageable);

    /**
     * Rechercher demandes subvention tdb atransferer subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubControlee
     *            the etat dem sub controlee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION,
            countQuery = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAtransfererSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubControlee") String etatDemSubControlee, Pageable pageable);

    /**
     * Rechercher demandes paiement tdb atransferer paiement.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiControlee
     *            the etat dem pai controlee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT,
            countQuery = TableauDeBordSd7CustomJpaRepositoryImpl.REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAtransfererPaiement(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemPaiControlee") String etatDemPaiControlee, Pageable pageable);

}

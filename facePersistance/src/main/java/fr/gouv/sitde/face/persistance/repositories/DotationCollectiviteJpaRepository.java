/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Interface DotationCollectiviteJpaRepository.
 */
public interface DotationCollectiviteJpaRepository extends JpaRepository<DotationCollectivite, Long> {

    /**
     * aa Rechercher dotations collectivites par code departement et abreviation sous programme.
     *
     * @param codeDepartement
     *            the code departement
     * @param sprEnum
     *            the spr enum
     * @param year
     *            the year
     * @return the list
     */
    @Query("select distinct dco from DotationCollectivite dco join fetch dco.dotationDepartement dde"
            + " join fetch dde.departement dep join fetch dde.lignesDotationDepartement ldd"
            + " join fetch dde.dotationSousProgramme dsp join fetch dsp.dotationProgramme dpr" + " join fetch dsp.sousProgramme spr"
            + " where dep.code = :codeDepartement and dpr_annee = :anneeEnCours and spr.abreviation = :sprEnum")
    List<DotationCollectivite> rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(
            @Param("codeDepartement") String codeDepartement, @Param("sprEnum") String sprEnum, @Param("anneeEnCours") int year);

    /**
     * Rechercher dotation collectivite par id.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the optional
     */
    @Query("select distinct dco from DotationCollectivite dco join fetch dco.dotationDepartement dde "
            + "join fetch dde.departement dep join fetch dde.lignesDotationDepartement ldd "
            + "join fetch dde.dotationSousProgramme dsp join fetch dsp.dotationProgramme dpr "
            + "join fetch dsp.sousProgramme spr where dco.id = :idDotationCollectivite")
    Optional<DotationCollectivite> rechercherDotationCollectiviteParId(@Param("idDotationCollectivite") Long idDotationCollectivite);

    /**
     * Rechercher dotation collectivite simple par id.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the optional
     */
    @Query("select distinct dco from DotationCollectivite dco join fetch dco.dotationDepartement dde "
            + "join fetch dde.departement dep where dco.id = :idDotationCollectivite")
    Optional<DotationCollectivite> rechercherDotationCollectiviteSimpleParId(@Param("idDotationCollectivite") Long idDotationCollectivite);

    /**
     * Rechercher dotation collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @param annee
     *            the annee
     * @return the optional
     */
    @Query("select distinct dco from DotationCollectivite dco " + " join fetch dco.dotationDepartement dde "
            + " join fetch dde.dotationSousProgramme dsp " + " join fetch dsp.dotationProgramme dpr " + " join fetch dsp.sousProgramme spr "
            + " where dco.collectivite.id = :idCollectivite and dpr.annee = :annee and spr.id = :idSousProgramme")
    Optional<DotationCollectivite> rechercherDotationCollectivite(@Param("idCollectivite") Long idCollectivite,
            @Param("idSousProgramme") Integer idSousProgramme, @Param("annee") Integer annee);

    @Query("select distinct dco from DotationCollectivite dco " + " join fetch dco.dotationDepartement dde "
            + " join fetch dde.dotationSousProgramme dsp " + " join fetch dsp.dotationProgramme dpr " + " join fetch dsp.sousProgramme spr "
            + " where dco.collectivite.id = :idCollectivite and dpr.annee = :annee")
    List<DotationCollectivite> rechercherDotationCollectiviteParIdCollectivite(@Param("idCollectivite") Long idCollectivite,
            @Param("annee") Integer annee);

    /**
     * Rechercher dotations collectivites par id collectivite et annee et code numerique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @param codeNumerique
     *            the code numerique
     * @return the list
     */
    @Query("select distinct dco from DotationCollectivite dco " + " join fetch dco.dotationDepartement dde "
            + " join fetch dde.dotationSousProgramme dsp " + " join fetch dsp.dotationProgramme dpr " + " join fetch dsp.sousProgramme spr "
            + " join fetch spr.programme pro "
            + " where dco.collectivite.id = :idCollectivite and dpr.annee = :annee and pro.codeNumerique = :codeNumerique")
    List<DotationCollectivite> rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(@Param("idCollectivite") Long idCollectivite,
            @Param("annee") Integer annee, @Param("codeNumerique") String codeNumerique);

    /**
     * @param id
     * @return
     */
    @Query("select distinct dos from DossierSubvention dos join fetch dos.dotationCollectivite dco where dco.id = :id")
    List<DossierSubvention> recupererDossiersParDotationCollectivite(@Param("id") Long id);

    @Query("select (case when (count(*) > 0) then false else true end) " + "from DemandeSubvention dsu "
            + "inner join dsu.dossierSubvention.dotationCollectivite dco "
            + "inner join dco.dotationDepartement.dotationSousProgramme.sousProgramme spr "
            + "WHERE dco.id = :idDotationCollectivite AND spr.description = :descriptionSousProgramme " + "AND spr.deProjet = false "
            + "AND (dsu.etatDemandeSubvention.code not in ('REFUSEE', 'ATTRIBUEE', 'REJET_TAUX', 'CLOTUREE')) "
            + "AND dsu.id != :idDemandeSubvention")
    boolean isEstUniqueSubventionEnCoursParSousProgrammeAvecIdDemandeSubvention(@Param("idDemandeSubvention") Long idDemandeSubvention,
            @Param("idDotationCollectivite") Long idDotationCollectivite, @Param("descriptionSousProgramme") String descriptionSousProgramme);

    @Query("select (case when (count(*) > 0) then false else true end) " + "from DemandeSubvention dsu "
            + "inner join dsu.dossierSubvention.dotationCollectivite dco "
            + "inner join dco.dotationDepartement.dotationSousProgramme.sousProgramme spr "
            + "WHERE dco.id = :idDotationCollectivite AND spr.description = :descriptionSousProgramme " + "AND spr.deProjet = false "
            + "AND (dsu.etatDemandeSubvention.code not in ('REFUSEE', 'ATTRIBUEE', 'REJET_TAUX', 'CLOTUREE'))")
    boolean isEstUniqueSubventionEnCoursParSousProgramme(@Param("idDotationCollectivite") Long idDotationCollectivite,
            @Param("descriptionSousProgramme") String descriptionSousProgramme);

    @Query("select distinct dco from DotationCollectivite dco " + "join fetch dco.collectivite col " + "join fetch col.adressesEmail adem "
            + "join fetch dco.dotationDepartement dde "
            + "where  dde.dotationSousProgramme.sousProgramme.deProjet = false and (dco.montant - dco.dotationRepartie) > 0 and "
            + "dde.dotationSousProgramme.dotationProgramme.annee = :anneeEnCours ")
    List<DotationCollectivite> rechercherPerteDotationCollectivitePourBatch(@Param("anneeEnCours") Integer anneeEnCours);
}

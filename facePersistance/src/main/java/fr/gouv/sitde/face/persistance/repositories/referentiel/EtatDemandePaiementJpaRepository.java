/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;

/**
 * Repository JPA de l'entité DemandePaiement.
 *
 * @author Atos
 */
@Repository
public interface EtatDemandePaiementJpaRepository extends JpaRepository<DemandePaiement, Long> {

    /**
     * Recherche des demandes de paiement via l'id du dossier de subvention.
     *
     * @param idDossierSubvention the id dossier subention
     * @return the list
     */
    @Query("select ed from EtatDemandePaiement ed where ed.code = :codeEtatdemande")
    EtatDemandePaiement findByCode(@Param("codeEtatdemande") String codeEtatdemande);
}

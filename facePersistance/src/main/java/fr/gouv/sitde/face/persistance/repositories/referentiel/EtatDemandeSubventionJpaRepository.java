/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

/**
 * Repository JPA de l'entité EtatDemandeSubvention.
 *
 * @author Atos
 */
@Repository
public interface EtatDemandeSubventionJpaRepository extends JpaRepository<DemandePaiement, Long> {

    /**
     * Find by code.
     *
     * @param codeEtatdemande
     *            the code etatdemande
     * @return the etat demande subvention
     */
    @Query("select eds from EtatDemandeSubvention eds where eds.code = :codeEtatdemande")
    Optional<EtatDemandeSubvention> findByCode(@Param("codeEtatdemande") String codeEtatdemande);
}

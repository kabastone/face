package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.custom.RechercheCadencementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.entities.Cadencement;

@Repository
public interface CadencementJpaRepository extends JpaRepository<Cadencement, Long>, RechercheCadencementJpaRepositoryCustom {

    /**
     * @param idCollectivite
     * @param anneeCourante
     * @param deProjet
     * @return
     */
    @Query("select cad from Cadencement cad where" + " cad.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite.id = :idCollectivite"
            + " and cad.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme.annee <= :annee"
            + " and cad.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.deProjet = :deProjet"
            + " and year(cad.demandeSubvention.dossierSubvention.dateEcheanceAchevement) >= :annee")
    List<Cadencement> recupererCadencementsPourCollectivite(@Param("idCollectivite") Long idCollectivite, @Param("annee") int anneeCourante,
            @Param("deProjet") Boolean deProjet);

    /**
     * Rechercher cadencements pour maj.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select cad from Cadencement cad where" + " cad.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite.id = :idCollectivite"
            + " and year(cad.demandeSubvention.dossierSubvention.dateEcheanceAchevement) >= :annee")
    List<Cadencement> rechercherCadencementsPourMaj(@Param("idCollectivite") Long idCollectivite, @Param("annee") int annee);

    @Query("SELECT (true = ALL(select cad.estValide from Cadencement cad "
            + "where cad.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite.id = :idCollectivite)) from Cadencement")
    Boolean estToutCadencementValidePourCollectivite(@Param("idCollectivite") Long idCollectivite);

}

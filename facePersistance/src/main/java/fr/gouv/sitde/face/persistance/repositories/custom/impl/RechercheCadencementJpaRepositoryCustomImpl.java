/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.custom.RechercheCadencementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * @author a768251
 *
 */
@Repository
public class RechercheCadencementJpaRepositoryCustomImpl implements RechercheCadencementJpaRepositoryCustom {

    @Autowired
    private TimeOperationTransverse timeUtils;

    /** The Constant BRACKET_COMMA_BRACKET. */
    private static final String BRACKET_COMMA_BRACKET = "),(";

    private static final String CONDITION_COLLECTIVITE = "and col.id = :idCollectivite ";

    private static final String CONDITION_DEPARTEMENT = "and col.departement.id = :idDepartement ";

    private static final String REQUETE_CAD_SOUS_PROGRAMME_CODE = "sprGlobal.numSousAction ";

    private static final String REQUETE_CAD_SOUS_PROGRAMME_ABREVIATION = "sprGlobal.abreviation ";

    private static final String REQUETE_CAD_SOUS_PROGRAMME_DESCRIPTION = "sprGlobal.description ";

    private static final String REQUETE_CAD_MONTANT_SUBVENTIONS = "select sum(cad1.demandeSubvention.plafondAide) from Cadencement cad1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr1.id and dpr1.annee >= :annee-4 ";

    private static final String REQUETE_CAD_MONTANT_SUBVENTIONS_AVEC_ANNEE = "select sum(cad1.demandeSubvention.plafondAide) from Cadencement cad1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr1 "
            + "inner join cad1.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr1.id and dpr1.annee = :annee ";

    private static final String REQUETE_CAD_ANNEE_1 = "select (sum(case when dpr2.annee = :annee then cad2.montantAnnee1 else 0 end) "
            + "+ sum(case when dpr2.annee = :annee-1 then cad2.montantAnnee2 else 0 end) "
            + "+ sum(case when dpr2.annee = :annee-2 then cad2.montantAnnee3 else 0 end) "
            + "+ sum(case when dpr2.annee = :annee-3 then cad2.montantAnnee4 else 0 end) "
            + "+ sum(case when dpr2.annee = :annee-4 then cad2.montantAnneeProlongation else 0 end)) " + "from Cadencement cad2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col " + "where sprGlobal.id = spr2.id ";

    private static final String REQUETE_CAD_ANNEE_1_AVEC_ANNEE = "select sum(cad6.montantAnnee1) from Cadencement cad6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr6.id and dpr6.annee = :annee ";

    private static final String REQUETE_CAD_ANNEE_2 = "select (sum(case when dpr3.annee = :annee then cad3.montantAnnee2 else 0 end) "
            + "+ sum(case when dpr3.annee = :annee-1 then cad3.montantAnnee3 else 0 end) "
            + "+ sum(case when dpr3.annee = :annee-2 then cad3.montantAnnee4 else 0 end) "
            + "+ sum(case when dpr3.annee = :annee-3 then cad3.montantAnneeProlongation else 0 end)) " + "from Cadencement cad3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col " + "where sprGlobal.id = spr3.id ";

    private static final String REQUETE_CAD_ANNEE_2_AVEC_ANNEE = "select sum(cad2.montantAnnee2) from Cadencement cad2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr2 "
            + "inner join cad2.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr2.id and dpr2.annee = :annee ";

    private static final String REQUETE_CAD_ANNEE_3 = "select (sum(case when dpr4.annee = :annee then cad4.montantAnnee3 else 0 end) "
            + "+ sum(case when dpr4.annee = :annee-1 then cad4.montantAnnee4 else 0 end) "
            + "+ sum(case when dpr4.annee = :annee-2 then cad4.montantAnneeProlongation else 0 end)) " + "from Cadencement cad4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col " + "where sprGlobal.id = spr4.id ";

    private static final String REQUETE_CAD_ANNEE_3_AVEC_ANNEE = "select sum(cad3.montantAnnee3) from Cadencement cad3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr3 "
            + "inner join cad3.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr3.id and dpr3.annee = :annee ";

    private static final String REQUETE_CAD_ANNEE_4 = "select (sum(case when dpr5.annee = :annee then cad5.montantAnnee4 else 0 end) "
            + "+ sum(case when dpr5.annee = :annee-1 then cad5.montantAnneeProlongation else 0 end)) " + "from Cadencement cad5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col " + "where sprGlobal.id = spr5.id ";

    private static final String REQUETE_CAD_ANNEE_4_AVEC_ANNEE = "select sum(cad4.montantAnnee4) from Cadencement cad4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr4 "
            + "inner join cad4.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr4.id and dpr4.annee = :annee ";

    private static final String REQUETE_CAD_ANNEE_PROLONGATION = "select (sum(case when dpr6.annee = :annee then cad6.montantAnneeProlongation else 0 end)) "
            + "from Cadencement cad6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr6 "
            + "inner join cad6.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col " + "where sprGlobal.id = spr6.id ";

    private static final String REQUETE_CAD_ANNEE_PROLONGATION_AVEC_ANNEE = "select sum(cad5.montantAnneeProlongation) from Cadencement cad5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr5 "
            + "inner join cad5.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr5.id and dpr5.annee = :annee ";

    private static final String REQUETE_CAD_EST_VALIDE = "select distinct true = all(select cad7.estValide from Cadencement cad7 "
            + "inner join cad7.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr7 "
            + "inner join cad7.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr7 "
            + "inner join cad7.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr7.id and dpr7.annee >= :annee-4 ";

    private static final String REQUETE_CAD_EST_VALIDE_AVEC_ANNEE = "select distinct true = all(select cad8.estValide from Cadencement cad8 "
            + "inner join cad8.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.dotationProgramme dpr8 "
            + "inner join cad8.demandeSubvention.dossierSubvention.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme spr8 "
            + "inner join cad8.demandeSubvention.dossierSubvention.dotationCollectivite.collectivite col "
            + "where sprGlobal.id = spr8.id and dpr8.annee = :annee ";

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche,
            String codeNumerique) {

        // Construction de la requete JPQL
        StringBuilder jpqlQuery = new StringBuilder();

        String ajoutConditions = RechercheCadencementJpaRepositoryCustomImpl.ajoutConditionDepartementOuCollectivite(criteresRecherche);

        if (criteresRecherche.getAnneeProgrammation() == null) {
            RechercheCadencementJpaRepositoryCustomImpl.constructionRequetePourCumul(jpqlQuery, ajoutConditions);
        } else {
            RechercheCadencementJpaRepositoryCustomImpl.constructionRequetePourAnnee(jpqlQuery, ajoutConditions);
        }
        jpqlQuery.append(")) FROM SousProgramme sprGlobal ");
        jpqlQuery.append("where sprGlobal.programme.codeNumerique = :codeNumerique " + "and year(sprGlobal.dateDebutValidite) <= :annee "
                + "and (year(sprGlobal.dateFinValidite) >= :annee or sprGlobal.dateFinValidite = NULL) ");

        TypedQuery<ResultatRechercheCadencementDto> query = this.entityManager
                .createQuery(jpqlQuery.toString(), ResultatRechercheCadencementDto.class).setParameter("codeNumerique", codeNumerique)
                .setParameter("annee", criteresRecherche.getAnneeProgrammation() != null ? criteresRecherche.getAnneeProgrammation()
                        : this.timeUtils.getAnneeCourante());
        if (criteresRecherche.getIdCollectivite() != null) {
            query.setParameter("idCollectivite", criteresRecherche.getIdCollectivite());
        }
        if (criteresRecherche.getIdDepartement() != null) {
            query.setParameter("idDepartement", criteresRecherche.getIdDepartement());
        }

        return query.getResultList();

    }

    /**
     * @param jpqlQuery
     * @param ajoutConditions
     */
    private static void constructionRequetePourCumul(StringBuilder jpqlQuery, String ajoutConditions) {
        jpqlQuery.append("SELECT NEW fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto((");
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_CODE);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_ABREVIATION);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_DESCRIPTION);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_MONTANT_SUBVENTIONS);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_1);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_2);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_3);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_4);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_PROLONGATION);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_EST_VALIDE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(") from Cadencement ");
    }

    /**
     * @param jpqlQuery
     * @param ajoutConditions
     */
    private static void constructionRequetePourAnnee(StringBuilder jpqlQuery, String ajoutConditions) {
        jpqlQuery.append("SELECT NEW fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto((");
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_CODE);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_ABREVIATION);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_SOUS_PROGRAMME_DESCRIPTION);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_MONTANT_SUBVENTIONS_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_1_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_2_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_3_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_4_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_ANNEE_PROLONGATION_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(BRACKET_COMMA_BRACKET);
        jpqlQuery.append(REQUETE_CAD_EST_VALIDE_AVEC_ANNEE);
        jpqlQuery.append(ajoutConditions);
        jpqlQuery.append(") from Cadencement ");
    }

    /**
     * @param criteresRecherche
     * @param jpqlQuery
     */
    private static String ajoutConditionDepartementOuCollectivite(CritereRechercheCadencementQo criteresRecherche) {
        StringBuilder conditions = new StringBuilder();
        if (criteresRecherche.getIdCollectivite() != null) {
            conditions.append(CONDITION_COLLECTIVITE);
        }
        if (criteresRecherche.getIdDepartement() != null) {
            conditions.append(CONDITION_DEPARTEMENT);
        }
        return conditions.toString();
    }

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom;

import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;

/**
 * The Interface DossierSubventionJpaRepositoryCustom.
 *
 * @author a453029
 */
public interface DossierSubventionJpaRepositoryCustom {

    /**
     * Recherche paginée des dossiers de subvention selon les critères en paramètre.
     *
     * @param criteresRecherche
     *            critères de recherche
     * @return la page de réponse correspondante.
     */
    PageReponse<DossierSubvention> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche);

}

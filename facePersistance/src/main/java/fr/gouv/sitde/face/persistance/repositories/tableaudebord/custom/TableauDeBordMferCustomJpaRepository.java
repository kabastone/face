/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Interface TableauDeBordMferCustomJpaRepository.
 *
 * @author a453029
 */
public interface TableauDeBordMferCustomJpaRepository {

    /**
     * Rechercher tableau de bord mfer.
     *
     * @return the tableau de bord mfer dto
     */
    TableauDeBordMferDto rechercherTableauDeBordMfer();

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Penalite;

/**
 * The Interface PenaliteJpaRepository.
 *
 * @author Atos
 */
@Repository
public interface PenaliteJpaRepository extends JpaRepository<Penalite, Long> {

    /**
     * Supprimer penalite par annee.
     *
     * @param annee
     *            the annee
     */
    @Modifying
    @Query("delete from Penalite pen where pen.annee=:annee")
    void supprimerPenaliteParAnnee(@Param("annee") int annee);

}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.gouv.sitde.face.persistance.repositories.custom.DepartementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The class DepartementJpaRepositoryCustomImpl.
 *
 * @author Atos
 */
public class DepartementJpaRepositoryCustomImpl implements DepartementJpaRepositoryCustom {

    /**
     * entityManager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.persistance.repositories.custom.DepartementJpaRepositoryCustom#findAllDepartementsPagine(fr.gouv.sitde.face.transverse.
     * pagination.PageDemande)
     */
    @Override
    public PageReponse<Departement> findAllDepartementsPagine(PageDemande pageDemande) {
        // Construction de la requete
        StringBuilder jpqlQuery = new StringBuilder();
        jpqlQuery.append("select d from Departement d left join fetch d.penalites p order by replace(replace(d.code, 'A', '00'), 'B', '01')");
        Query query = this.entityManager.createQuery(jpqlQuery.toString(), Departement.class);

        // pagination
        query.setFirstResult(pageDemande.getIndexPremierResultat());
        query.setMaxResults(pageDemande.getTaillePage());

        // Execution de la requete de resultats
        @SuppressWarnings("unchecked")
        List<Departement> listeDepartements = query.getResultList();

        // Construction de la pageReponse
        PageReponse<Departement> pageReponse = new PageReponse<>();
        pageReponse.setListeResultats(listeDepartements);

        // Recherche du nombre total de résultats
        Long countResult = this.getCountDepartementsPagine();
        pageReponse.setNbTotalResultats(countResult);

        return pageReponse;
    }

    /**
     * Gets the count departements pagine.
     *
     * @param pageDemande
     *            the page demande
     * @return the count departements pagine
     */
    private Long getCountDepartementsPagine() {

        // Construction de la requete
        StringBuilder jpqlCountQuery = new StringBuilder("select count(d) from Departement d");
        Query queryTotal = this.entityManager.createQuery(jpqlCountQuery.toString(), Long.class);

        // Renseignenment des parametres de la query à partir des criteres de recherche
        return (Long) queryTotal.getSingleResult();
    }
}

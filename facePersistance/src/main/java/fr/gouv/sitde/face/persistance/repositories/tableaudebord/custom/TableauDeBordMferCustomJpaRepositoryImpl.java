/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Class TableauDeBordMferCustomJpaRepositoryImpl.
 *
 * @author a453029
 */
@Repository
public class TableauDeBordMferCustomJpaRepositoryImpl implements TableauDeBordMferCustomJpaRepository {

    /** The Constant BRACKET_COMMA_BRACKET. */
    private static final String BRACKET_COMMA_BRACKET = "),(";

    /** The Constant ETAT_DOSSIER_OUVERT. */
    private static final String ETAT_DOSSIER_OUVERT = "etatDossierOuvert";

    /**
     * The Constant REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION. Variante de RG_SUB_103-01 avec deux états de demande.
     */
    public static final String REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where (eds.code = :etatDemSubDemandee or eds.code = :etatDemSubCorrigee) "
            + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT + " order by dsu.id";

    /**
     * The Constant REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION_COUNT. Variante de RG_SUB_103-01 avec deux états de demande.
     */
    public static final String REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where (eds.code = :etatDemSubDemandee or eds.code = :etatDemSubCorrigee) "
            + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /**
     * The Constant REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT. Variante de RG_SUB_104-01 avec deux états de demande.
     */
    public static final String REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where (edp.code = :etatDemPaiDemandee or edp.code = :etatDemPaiCorrigee) "
            + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT + " order by dpa.id";

    /**
     * The Constant REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT_COUNT. Variante de RG_SUB_104-01 avec deux états de demande.
     */
    public static final String REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where (edp.code = :etatDemPaiDemandee or edp.code = :etatDemPaiCorrigee) "
            + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubQualifiee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dsu.id";

    /** The Constant REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubQualifiee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where edp.code = :etatDemPaiQualifiee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dpa.id";

    /** The Constant REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT_COUNT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where edp.code = :etatDemPaiQualifiee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubApprouvee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dsu.id";

    /** The Constant REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubApprouvee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubAnomalieDetectee " + "and dos.etatDossier = :"
            + ETAT_DOSSIER_OUVERT + " order by dsu.id";

    /** The Constant REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubAnomalieDetectee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where edp.code = :etatDemPaiAnomalieDetectee " + "and dos.etatDossier = :"
            + ETAT_DOSSIER_OUVERT + " order by dpa.id";

    /** The Constant REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT_COUNT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where edp.code = :etatDemPaiAnomalieDetectee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION. */
    public static final String REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION = "select dpr from DemandeProlongation dpr "
            + "inner join dpr.etatDemandeProlongation edp inner join fetch dpr.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where edp.code = :etatDemProDemandee" + " order by dpr.id";

    /** The Constant REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION_COUNT. */
    public static final String REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION_COUNT = "select count(dpr.id) from DemandeProlongation dpr "
            + "inner join dpr.etatDemandeProlongation edp inner join dpr.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where edp.code = :etatDemProDemandee";

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.persistance.repositories.custom.TableauDeBordJpaRepository#rechercherTableauDeBordMfer()
     */
    @Override
    public TableauDeBordMferDto rechercherTableauDeBordMfer() {
        // Construction de la requete JPQL
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT NEW fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto((");
        jpql.append(REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION_COUNT);
        jpql.append(")) FROM MissionFace ");
        String chaineRequete = jpql.toString();

        TypedQuery<TableauDeBordMferDto> query = this.entityManager.createQuery(chaineRequete, TableauDeBordMferDto.class)
                .setParameter(ETAT_DOSSIER_OUVERT, EtatDossierEnum.OUVERT)
                .setParameter("etatDemSubDemandee", EtatDemandeSubventionEnum.DEMANDEE.toString())
                .setParameter("etatDemSubCorrigee", EtatDemandeSubventionEnum.CORRIGEE.toString())
                .setParameter("etatDemPaiDemandee", EtatDemandePaiementEnum.DEMANDEE.toString())
                .setParameter("etatDemPaiCorrigee", EtatDemandePaiementEnum.CORRIGEE.toString())
                .setParameter("etatDemSubQualifiee", EtatDemandeSubventionEnum.QUALIFIEE.toString())
                .setParameter("etatDemPaiQualifiee", EtatDemandePaiementEnum.QUALIFIEE.toString())
                .setParameter("etatDemSubApprouvee", EtatDemandeSubventionEnum.APPROUVEE.toString())
                .setParameter("etatDemSubAnomalieDetectee", EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.toString())
                .setParameter("etatDemPaiAnomalieDetectee", EtatDemandePaiementEnum.ANOMALIE_DETECTEE.toString())
                .setParameter("etatDemProDemandee", EtatDemandeProlongationEnum.DEMANDEE.toString());

        return query.getSingleResult();
    }
}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;

/**
 * Repository JPA de l'entité DemandeProlongation.
 *
 * @author Atos
 */
@Repository
public interface DemandeProlongationJpaRepository extends JpaRepository<DemandeProlongation, Long> {

    /**
     * Recherche des demandes de prolongation via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @Query("select demande from DemandeProlongation demande inner join fetch demande.dossierSubvention dossier "
            + "where dossier.id = :idDossierSubvention order by demande.dateDemande DESC, demande.id DESC")
    List<DemandeProlongation> findByDossierSubvention(@Param("idDossierSubvention") Long idDossierSubvention);

    /**
     * Rechercher demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation
     */
    @Query("select dem from DemandeProlongation dem inner join fetch dem.dossierSubvention dos "
            + "join fetch dos.dotationCollectivite dco join fetch dco.collectivite col " + "where dem.id = :idDemande")
    Optional<DemandeProlongation> rechercherDemandeProlongation(@Param("idDemande") Long idDemandeProlongation);

    /**
     * Recherche par date achevement et dossier subvention.
     *
     * @param dateLivraisonDuPoste
     *            the date livraison du poste
     * @param numeroEJ
     *            the numero EJ
     * @return the optional
     */
    @Query("select dpr from DemandeProlongation dpr inner join dpr.dossierSubvention dos "
            + " where dos.chorusNumEj = :numeroEJ and date_trunc('day',dpr.dateAchevementDemandee) = date_trunc('day',cast(:dateLivraisonDuPoste AS timestamp))")
    Optional<DemandeProlongation> rechercheParDateAchevementEtDossierSubvention(@Param("dateLivraisonDuPoste") LocalDateTime dateLivraisonDuPoste,
            @Param("numeroEJ") String numeroEJ);
}

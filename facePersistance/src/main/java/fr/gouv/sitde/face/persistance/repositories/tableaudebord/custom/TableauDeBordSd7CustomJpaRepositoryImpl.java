/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Class TableauDeBordSd7CustomJpaRepositoryImpl.
 *
 * @author a453029
 */
@Repository
public class TableauDeBordSd7CustomJpaRepositoryImpl implements TableauDeBordSd7CustomJpaRepository {

    /** The Constant BRACKET_COMMA_BRACKET. */
    private static final String BRACKET_COMMA_BRACKET = "),(";

    /** The Constant ETAT_DOSSIER_OUVERT. */
    private static final String ETAT_DOSSIER_OUVERT = "etatDossierOuvert";

    /** The Constant REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubAccordee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dsu.id";

    /** The Constant REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubAccordee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where edp.code = :etatDemPaiAccordee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dpa.id";

    /** The Constant REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT_COUNT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where edp.code = :etatDemPaiAccordee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION = "select dsu from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join fetch dsu.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where eds.code = :etatDemSubControlee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dsu.id";

    /** The Constant REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION_COUNT. RG_SUB_103-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION_COUNT = "select count(dsu.id) from DemandeSubvention dsu "
            + "inner join dsu.etatDemandeSubvention eds inner join dsu.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where eds.code = :etatDemSubControlee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /** The Constant REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT = "select dpa from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join fetch dpa.dossierSubvention dos inner join fetch dos.dotationCollectivite dco "
            + "inner join fetch dco.collectivite col " + "where edp.code = :etatDemPaiControlee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT
            + " order by dpa.id";

    /** The Constant REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT_COUNT. RG_SUB_104-01 sans idCollectivite. */
    public static final String REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT_COUNT = "select count(dpa.id) from DemandePaiement dpa "
            + "inner join dpa.etatDemandePaiement edp inner join dpa.dossierSubvention dos inner join dos.dotationCollectivite dco "
            + "inner join dco.collectivite col " + "where edp.code = :etatDemPaiControlee " + "and dos.etatDossier = :" + ETAT_DOSSIER_OUVERT;

    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.persistance.repositories.custom.TableauDeBordSd7CustomJpaRepository#rechercherTableauDeBordSd7()
     */
    @Override
    public TableauDeBordSd7Dto rechercherTableauDeBordSd7() {
        // Construction de la requete JPQL
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT NEW fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto((");
        jpql.append(REQUETE_TDB_SD7_A_CONTROLER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_SD7_A_CONTROLER_PAIEMENT_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_SD7_A_TRANSFERER_SUBVENTION_COUNT);
        jpql.append(BRACKET_COMMA_BRACKET);
        jpql.append(REQUETE_TDB_SD7_A_TRANSFERER_PAIEMENT_COUNT);
        jpql.append(")) FROM MissionFace ");
        String chaineRequete = jpql.toString();

        TypedQuery<TableauDeBordSd7Dto> query = this.entityManager.createQuery(chaineRequete, TableauDeBordSd7Dto.class)
                .setParameter(ETAT_DOSSIER_OUVERT, EtatDossierEnum.OUVERT)
                .setParameter("etatDemSubAccordee", EtatDemandeSubventionEnum.ACCORDEE.toString())
                .setParameter("etatDemPaiAccordee", EtatDemandePaiementEnum.ACCORDEE.toString())
                .setParameter("etatDemSubControlee", EtatDemandeSubventionEnum.CONTROLEE.toString())
                .setParameter("etatDemPaiControlee", EtatDemandePaiementEnum.CONTROLEE.toString());

        return query.getSingleResult();
    }
}

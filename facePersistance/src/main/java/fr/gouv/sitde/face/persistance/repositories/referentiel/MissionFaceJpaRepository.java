/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.gouv.sitde.face.transverse.entities.MissionFace;

/**
 * @author A687370
 *
 */
public interface MissionFaceJpaRepository extends JpaRepository<MissionFace, Long> {

    /**
     * Find all mission face.
     *
     * @return the list
     */
    @Query("select mis from MissionFace mis where mis.actif = true")
    Optional<MissionFace> findMissionFace();
}

/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * Repository JPA de l'entité DemandePaiement.
 *
 * @author Atos
 */
@Repository
public interface DemandePaiementJpaRepository extends JpaRepository<DemandePaiement, Long> {

    /**
     * Recherche de la demande de paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the optional
     */
    @Override
    @Query("select demande from DemandePaiement demande inner join fetch demande.dossierSubvention dossier "
            + " where demande.id = :idDemandePaiement")
    Optional<DemandePaiement> findById(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Recherche de la demande de paiement avec documents.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the optional
     */
    @Query("select demande from DemandePaiement demande inner join fetch demande.dossierSubvention dossier "
            + "left join fetch demande.documents docs where demande.id = :idDemandePaiement")
    Optional<DemandePaiement> findByIdAvecDocs(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Recherche des demandes de paiement via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @Query("select demande from DemandePaiement demande inner join fetch demande.dossierSubvention dossier "
            + "where dossier.id = :idDossierSubvention order by demande.dateDemande DESC, demande.id DESC")
    List<DemandePaiement> findByDossierSubvention(@Param("idDossierSubvention") Long idDossierSubvention);

    /**
     * Recherche d'une demande de paiement avec données nécessaires pour le pdf.
     *
     * @param idDemandePaiement
     *            ID de la demande de paiement désirée.
     *
     * @return La demande de paiement.
     *
     */
    @Query("select demande from DemandePaiement demande" + " join fetch demande.dossierSubvention ds" + " join fetch ds.dotationCollectivite dc"
            + " join fetch dc.collectivite co" + " join fetch dc.dotationDepartement dpe" + " join fetch dpe.dotationSousProgramme dsp"
            + " join fetch dsp.sousProgramme sp" + " join fetch sp.programme pr" + " where demande.id = :idDemandePaiement")
    Optional<DemandePaiement> rechercheDemandePaiementPourPdfParId(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Compte des demandes en accomptes pour dossier par id demande.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the integer
     */
    @Query("select count(demande) from DemandePaiement demande " + "where demande.dossierSubvention.id = ("
            + "select demande.dossierSubvention.id from DemandePaiement demande " + "where demande.id = :idDemandePaiement"
            + ") and demande.typeDemandePaiement = 'ACOMPTE'")
    Integer compteAccomptesPourDossierParIdDemande(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Rechercher par id chorus.
     *
     * @param idChorus
     *            the id chorus
     * @return the demande paiement
     */
    @Query("select dpa from DemandePaiement dpa where dpa.chorusIdAe = :idChorus")
    Optional<DemandePaiement> rechercherParIdChorus(@Param("idChorus") Long idChorus);

    /**
     * Find all pour batch.
     *
     * @return the list
     */
    @Query("select distinct dpa from DemandePaiement dpa " + "join fetch dpa.transfertsPaiement trp " + "join fetch dpa.dossierSubvention dos "
            + "join fetch trp.demandeSubvention dsu " + "join fetch dpa.etatDemandePaiement edpa " + "where edpa.code = 'EN_ATTENTE_TRANSFERT' "
            + "order by dpa.chorusIdAe, trp.chorusNumLigneLegacy")
    List<DemandePaiement> findAllPourBatch();

    /**
     * Find all pour repartition.
     *
     * @return the list
     */
    @Query("select distinct dpa from DemandePaiement dpa " + "join fetch dpa.dossierSubvention dos " + "join fetch dos.demandesSubvention dsu "
            + "join fetch dpa.etatDemandePaiement edpa " + "where edpa.code = 'EN_ATTENTE_TRANSFERT' " + "order by dpa.chorusIdAe")
    List<DemandePaiement> findAllPourRepartition();

    /**
     * Gets the idfrom id ae.
     *
     * @param idAe
     *            the id ae
     * @return the idfrom id ae
     */
    @Query("select dpa.id from DemandePaiement dpa where dpa.chorusIdAe = :idAe")
    Optional<Long> getIdfromIdAe(@Param("idAe") Long idAe);

    /**
     * Find all pour fen 0159 a.
     *
     * @return the list
     */
    @Query("select distinct dpa from DemandePaiement dpa " + "join fetch dpa.etatDemandePaiement edpa " + "join fetch dpa.documents doc "
            + " join fetch doc.typeDocument td " + "where edpa.code = 'EN_COURS_TRANSFERT_2' " + "and td.idCodifie in (301, 303, 305, 307, 310, 311)")
    List<DemandePaiement> findAllPourFen0159a();

    /**
     * Rechercher certif non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    @Query("select distinct(dpa) from DemandePaiement dpa " + "where not exists(" + "select 1 from TransfertPaiement trp "
            + "left join trp.demandePaiement dpa2" + " where dpa2 = dpa and trp.dateCertification is null) " + "and dpa.chorusNumSf = :chorusNumSf")
    List<DemandePaiement> rechercherCertifNonNulParService(@Param("chorusNumSf") String chorusNumSf);

    /**
     * Rechercher versement non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    @Query("select distinct(dpa) from DemandePaiement dpa " + "where not exists(" + "select 1 from TransfertPaiement trp "
            + "left join trp.demandePaiement dpa2" + " where dpa2 = dpa and trp.dateVersement is null) " + "and dpa.chorusNumSf = :chorusNumSf")
    List<DemandePaiement> rechercherVersementNonNulParService(@Param("chorusNumSf") String chorusNumSf);

    /**
     * Rechercher montant avance total demandes paiement dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the big decimal
     */
    @Query("select sum(dem.aideDemandee) from DemandePaiement dem " + "join dem.etatDemandePaiement eta "
            + "where dem.dossierSubvention.id = :idDossier and dem.typeDemandePaiement = 'AVANCE' " + " and eta.code != 'REFUSEE'")
    BigDecimal rechercherMontantAvanceTotalDemandesPaiementDossier(@Param("idDossier") Long idDossier);

    /**
     * rechercher numero ordre suivant pour les demandes de paiement du dossier voulu.
     *
     * @param idDossier
     *            the id dossier
     * @return the short
     */
    @Query("select coalesce(max(dem.numOrdre + 1), 1) from DemandePaiement dem join dem.dossierSubvention dos " + " where dos.id = :idDossier")
    short rechercherNumeroOrdreSuivant(@Param("idDossier") Long idDossier);

    /**
     * rechercher numero ordre d'une demande de paiement.
     *
     * @param idDossier
     *            the id dossier
     * @return the short
     */
    @Query("select dem.numOrdre from DemandePaiement dem where dem.id = :idDemandePaiement")
    short rechercherNumeroOrdre(@Param("idDemandePaiement") Long idDemandePaiement);

    /**
     * Checks if is est unique demande paiement en cours.
     *
     * @param numDossier
     *            the num dossier
     * @return true, if is est unique demande paiement en cours
     */
    @Query("select (case when (count(*) > 0) then false else true end) " + "from DemandePaiement dpa " + "inner join dpa.dossierSubvention dos "
            + "where (dos.numDossier = :numDossier) and (dpa.etatDemandePaiement.code not in ('REFUSEE', 'VERSEE'))")
    boolean isEstUniqueDemandePaiementEnCours(@Param("numDossier") String numDossier);
}

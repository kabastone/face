/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Interface TableauDeBordSd7CustomJpaRepository.
 *
 * @author a453029
 */
public interface TableauDeBordSd7CustomJpaRepository {

    /**
     * Rechercher tableau de bord sd 7.
     *
     * @return the tableau de bord sd 7 dto
     */
    TableauDeBordSd7Dto rechercherTableauDeBordSd7();

}

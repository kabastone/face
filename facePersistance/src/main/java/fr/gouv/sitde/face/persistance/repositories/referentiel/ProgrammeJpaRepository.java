/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.referentiel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Programme;

/**
 * The Interface SousProgrammeJpaRepository.
 *
 * @author a453029
 */
@Repository
public interface ProgrammeJpaRepository extends JpaRepository<Programme, Integer> {

    /**
     * Rechercher programmes sans dotation.
     *
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select p from Programme p where not exists (select 1 from DotationProgramme dp where dp.programme = p and dp.annee= :annee)")
    List<Programme> rechercherProgrammesSansDotation(@Param("annee") Integer annee);

}

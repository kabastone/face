/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom;

import java.util.List;

import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto;

/**
 * The Interface DotationCollectiviteEmailDtoJpaRepository.
 */
public interface DotationCollectiviteEmailDtoJpaRepository {

    /**
     * Rechercher donnees pour email dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<DotationCollectiviteParSousProgrammeDto> rechercherDonneesPourEmailDotation(Long idCollectivite, Integer annee);

    /**
     * Verifier existence demande subvention.
     *
     * @param id
     *            the id
     * @param anneeEnCours
     *            the annee en cours
     * @return true, if successful
     */
    Boolean verifierExistenceDemandeSubvention(@Param("id") Long id, @Param("anneeEnCours") Integer anneeEnCours);
}

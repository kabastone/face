/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.custom.DotationCollectiviteEmailDtoJpaRepository;
import fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.email.SousProgrammeNotificationEnum;

/**
 * The Class DotationCollectiviteEmailDTOJpaRepositoryImpl.
 */
@Repository
public class DotationCollectiviteEmailDTOJpaRepositoryImpl implements DotationCollectiviteEmailDtoJpaRepository {

    /** The entity manager. */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.persistance.repositories.custom.DotationCollectiviteEmailDtoJpaRepository#rechercherDonneesPourEmailDotation(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<DotationCollectiviteParSousProgrammeDto> rechercherDonneesPourEmailDotation(Long idCollectivite, Integer annee) {

        List<String> listAbreviation = new ArrayList<>(SousProgrammeNotificationEnum.values().length);
        List<SousProgrammeNotificationEnum> list = Arrays.asList(SousProgrammeNotificationEnum.values());
        for (SousProgrammeNotificationEnum spr : list) {
            listAbreviation.add(spr.getAbreviation());
        }
        StringBuilder jpql = new StringBuilder("select");
        jpql.append(" distinct new fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto(");
        jpql.append(" dco.dotationRepartie, dco.aideDemandee, dpr.annee, spr.abreviation) ");
        jpql.append("from DotationCollectivite dco ");
        jpql.append("join dco.collectivite col ");
        jpql.append("join col.adressesEmail adr ");
        jpql.append("join dco.dotationDepartement dde ");
        jpql.append("join dde.dotationSousProgramme dsp ");
        jpql.append("join dsp.sousProgramme spr ");
        jpql.append("join dsp.dotationProgramme dpr ");
        jpql.append("where (dco.dotationRepartie > 0) and col.id = :idCollectivite ");
        jpql.append("and (dpr.annee >= :annee - 4) and spr.abreviation in :sprEnum ");
        jpql.append("order by spr.abreviation, dpr.annee");
        String chaineRequete = jpql.toString();

        TypedQuery<DotationCollectiviteParSousProgrammeDto> query = this.entityManager
                .createQuery(chaineRequete, DotationCollectiviteParSousProgrammeDto.class).setParameter("idCollectivite", idCollectivite)
                .setParameter("annee", annee).setParameter("sprEnum", listAbreviation);

        return query.getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.persistance.repositories.custom.DotationCollectiviteEmailDtoJpaRepository#verifierExistenceDemandeSubvention(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public Boolean verifierExistenceDemandeSubvention(Long id, Integer anneeEnCours) {
        StringBuilder jpql = new StringBuilder("select exists (");
        jpql.append("select 1 from DemandeSubvention dem ");
        jpql.append("join dem.dossierSubvention dos ");
        jpql.append("join dos.dotationCollectivite dco ");
        jpql.append("join dco.collectivite col ");
        jpql.append("join dco.dotationDepartement dde ");
        jpql.append("join dde.dotationSousProgramme dsp ");
        jpql.append("join dsp.dotationProgramme dpr ");
        jpql.append("where dpr.annee = :anneeEnCours and col.id = :id) from DemandeSubvention demande");
        String chaineRequete = jpql.toString();

        TypedQuery<Boolean> query = this.entityManager.createQuery(chaineRequete, Boolean.class).setParameter("anneeEnCours", anneeEnCours)
                .setParameter("id", id);
        return query.getSingleResult();
    }

}

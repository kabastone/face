/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Utilisateur;

/**
 * Repository JPA de l'entité Utilisateur.
 *
 * @author a453029
 *
 */
@Repository
public interface UtilisateurJpaRepository extends JpaRepository<Utilisateur, Long> {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#findById(java.lang.Object)
     */
    @Query("select distinct u from Utilisateur u " + "left join fetch u.droitsAgent " + "where u.id=:id")
    @Override
    Optional<Utilisateur> findById(@Param("id") Long id);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)
     */
    @Override
    Page<Utilisateur> findAll(Pageable pageable);

    /**
     * Recherche d'un utilisateur par e-mail.
     *
     * @param email
     *            l'email a chercher
     * @return the optional
     */
    @Query("select distinct u from Utilisateur u " + "left join fetch u.droitsAgent " + "where u.email=:email")
    Optional<Utilisateur> findByEmail(@Param("email") String email);
}

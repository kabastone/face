/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom;

import java.util.List;

import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;

/**
 * @author a768251
 *
 */
public interface RechercheCadencementJpaRepositoryCustom {

    /**
     * Rechercher cadencement par code numerique.
     *
     * @param criteresRecherche
     *            the criteres recherche
     * @param codeNumerique
     *            the code numerique
     * @return the list resultat recherche cadencement dto
     */
    List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche,
            String codeNumerique);

}

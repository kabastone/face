/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * Repository JPA de l'entité AdresseEmail.
 *
 * @author Atos
 */
@Repository
public interface AdresseEmailJpaRepository extends JpaRepository<AdresseEmail, Long> {
    /**
     * Pour la liste de diffusion : recherche toutes les adresses e-mail d'une collectivité.
     *
     * @param idCollectivite the id collectivite
     * @return the list
     */
    @Query("select a from AdresseEmail a left join fetch a.collectivite c where c.id=:idCollectivite order by c.dateCreation asc")
    List<AdresseEmail> rechercherAdressesEmail(@Param("idCollectivite") Long idCollectivite);
}

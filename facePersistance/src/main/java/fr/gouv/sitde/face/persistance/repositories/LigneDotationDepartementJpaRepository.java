/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Interface LigneDotationDepartementJpaRepository.
 *
 * @author Atos
 *
 *         The Interface DotationDepartementLigneJpaRepository.
 */
@Repository
public interface LigneDotationDepartementJpaRepository extends JpaRepository<LigneDotationDepartement, Long> {

    /**
     * Compte dotation annuelle notifiee annee en cours.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @return the integer
     */
    @Query("select count(ldd) from LigneDotationDepartement ldd " + "inner join ldd.dotationDepartement dd "
            + "inner join dd.dotationSousProgramme dsp " + "inner join dsp.dotationProgramme dp "
            + "where ldd.typeDotationDepartement='ANNUELLE' and dp.annee=:anneeEnCours and ldd.dateEnvoi is not null")
    Integer compteDotationAnnuelleNotifieeAnneeEnCours(@Param("anneeEnCours") Integer anneeEnCours);

    /**
     * Calculer dotation sans annuelle.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @param sprAbreviation
     *            the spr abreviation
     * @return the big decimal
     */
    @Query("select sum(ldd.montant) from LigneDotationDepartement ldd " + "join ldd.dotationDepartement dd " + "join dd.dotationSousProgramme dsp "
            + "join dsp.dotationProgramme dp " + "join dsp.sousProgramme sp "
            + "where ldd.typeDotationDepartement!='ANNUELLE' and dp.annee=:anneeEnCours and sp.abreviation=:sprAbreviation")
    BigDecimal calculerDotationSansAnnuelle(@Param("anneeEnCours") Integer anneeEnCours, @Param("sprAbreviation") String sprAbreviation);

    /**
     * Compter dotation sous programme.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @param sprAbreviation
     *            the spr abreviation
     * @return the big decimal
     */
    @Query("select sum(dsp.montant) from DotationSousProgramme dsp " + "left join dsp.sousProgramme sp " + "left join dsp.dotationProgramme dp "
            + "where dp.annee=:anneeEnCours and sp.abreviation=:sprAbreviation")
    BigDecimal compterDotationSousProgramme(@Param("anneeEnCours") Integer anneeEnCours, @Param("sprAbreviation") String sprAbreviation);

    /**
     * Effacer dotations annuelles.
     *
     * @param anneeEnCours
     *            the annee en cours
     */
    @Modifying
    @Query("delete from LigneDotationDepartement ldd where " + "exists (select 1 from LigneDotationDepartement ldd1 "
            + "join ldd1.dotationDepartement dd " + "join dd.dotationSousProgramme dsp " + "join dsp.dotationProgramme dp "
            + "where ldd.id = ldd1.id " + "and dp.annee=:anneeEnCours " + "and ldd1.typeDotationDepartement='ANNUELLE')")
    void effacerDotationsAnnuelles(@Param("anneeEnCours") Integer anneeEnCours);

    /**
     * Recherche de la liste des lignes dotation departement pour un département donné.
     *
     * @param idDepartement
     *            the id departement
     * @return the list //
     */
    @Query("select ligneDotDep from LigneDotationDepartement ligneDotDep join fetch ligneDotDep.dotationDepartement dotDep"
            + " join fetch dotDep.departement dep" + " join fetch dotDep.dotationSousProgramme dsp" + " join fetch dsp.sousProgramme spr"
            + " join fetch dsp.dotationProgramme dpr" + " where dep.id = :idDepartement" + " and ligneDotDep.dateEnvoi is null")
    List<LigneDotationDepartement> rechercherParDepartementPourAnnexe(@Param("idDepartement") Integer idDepartement);

    /**
     * Recherche de la liste des lignes dotation departement pour un département donné selon le type voulu.
     *
     * @param idDepartement
     *            the id departement
     * @param typeDotation
     *            the type dotation
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select distinct ligneDotDep from LigneDotationDepartement ligneDotDep join fetch ligneDotDep.dotationDepartement dotDep"
            + " join fetch dotDep.departement dep" + " join fetch dotDep.dotationSousProgramme dsp" + " left join fetch dep.penalites pen"
            + " join fetch dsp.sousProgramme spr" + " join fetch dsp.dotationProgramme dpr"
            + " where dep.id = :idDepartement and ligneDotDep.typeDotationDepartement = :typeDotation" + " and ligneDotDep.dateEnvoi is null"
            + " and dpr.annee = :annee")
    List<LigneDotationDepartement> rechercherParDepartementEtTypePourAnnexe(@Param("idDepartement") Integer idDepartement,
            @Param("typeDotation") TypeDotationDepartementEnum typeDotation, @Param("annee") Integer annee);

    /**
     * Recherche de la liste des lignes dotation departement selon le type voulu.
     *
     * @param typeDotation
     *            the type dotation
     * @param annee
     *            the annee
     * @return the list
     */
    @Query("select distinct ligneDotDep from LigneDotationDepartement ligneDotDep join fetch ligneDotDep.dotationDepartement dotDep"
            + " join fetch dotDep.departement dep" + " join fetch dotDep.dotationSousProgramme dsp" + " left join fetch dep.penalites pen"
            + " join fetch dsp.sousProgramme spr" + " join fetch dsp.dotationProgramme dpr"
            + " where ligneDotDep.typeDotationDepartement = :typeDotation" + " and ligneDotDep.dateEnvoi = null" + " and dpr.annee = :annee")
    List<LigneDotationDepartement> rechercherParTypePourAnnexe(@Param("typeDotation") TypeDotationDepartementEnum typeDotation,
            @Param("annee") Integer annee);

    /**
     * Maj date envoi ligne dotation departement.
     *
     * @param idDepartement
     *            the id departement
     * @param LocalDateTime
     *            the offset date time
     */
    @Modifying(clearAutomatically = true)
    @Query("update LigneDotationDepartement ldd set ldd.dateEnvoi = :now where ldd.id in (select ldd.id"
            + " from LigneDotationDepartement ldd join ldd.dotationDepartement dotDep" + " join dotDep.departement dep"
            + " where dep.id = :idDepartement and ldd.dateEnvoi is null)")
    void majDateEnvoiLigneDotationDepartement(@Param("idDepartement") Integer idDepartement, @Param("now") LocalDateTime LocalDateTime);

    /**
     * rechercher numero ordre suivant.
     *
     * @param idDepartement
     *            the id departement
     * @return the short
     */
    @Query("select coalesce(max(ldd.numOrdre + 1), 1) from LigneDotationDepartement ldd join ldd.dotationDepartement dde "
            + " join dde.departement dep " + " where dep.id = :idDepartement")
    short rechercherNumeroOrdreSuivant(@Param("idDepartement") Integer idDepartement);

    /**
     * Rechercher lignes dotation departementale en preparation.
     *
     * @param annee
     *            the annee
     * @param idDepartement
     *            the id departement
     * @return the list
     */
    @Query("select ligneDotDep from LigneDotationDepartement ligneDotDep " + "inner join fetch ligneDotDep.dotationDepartement dotDep "
            + "inner join fetch dotDep.departement dep " + "inner join fetch dotDep.dotationSousProgramme dsp "
            + "inner join fetch dsp.sousProgramme spr " + "inner join fetch dsp.dotationProgramme dpr " + "where dep.id = :idDepartement "
            + "and dpr.annee = :annee " + "and ligneDotDep.dateEnvoi is null " + "order by ligneDotDep.numOrdre")
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleEnPreparation(@Param("annee") Integer annee,
            @Param("idDepartement") Integer idDepartement);

    /**
     * Rechercher lignes dotation departementale notifiees.
     *
     * @param annee
     *            the annee
     * @param idDepartement
     *            the id departement
     * @return the list
     */
    @Query("select ligneDotDep from LigneDotationDepartement ligneDotDep " + "inner join fetch ligneDotDep.dotationDepartement dotDep "
            + "inner join fetch dotDep.departement dep " + "inner join fetch dotDep.dotationSousProgramme dsp "
            + "inner join fetch dsp.sousProgramme spr " + "inner join fetch dsp.dotationProgramme dpr " + "where dep.id = :idDepartement "
            + "and dpr.annee = :annee " + "and ligneDotDep.dateEnvoi is not null " + "order by ligneDotDep.numOrdre")
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleNotifiees(@Param("annee") Integer annee,
            @Param("idDepartement") Integer idDepartement);

    /**
     * @param idLigne
     * @return
     */
    @Query("select ldd from LigneDotationDepartement ldd join fetch ldd.dotationDepartement dde join fetch dde.departement where ldd.id = :id")
    Optional<LigneDotationDepartement> rechercherLigneParId(@Param("id") Long idLigne);

    /**
     * Recupere document notification par departement et date envoi.
     *
     * @param id
     *            the id
     * @param dateEnvoi
     *            the date envoi
     * @return the document
     */
    @Query("select doc from Document doc join fetch doc.courrierAnnuelDepartement cad join fetch cad.departement dep"
            + " where dep.id = :id and cast(doc.dateTeleversement as date) = cast(:dateEnvoi as date)" + " order by doc.dateTeleversement desc")
    List<Document> recupereDocumentNotificationParDepartementEtDateEnvoi(@Param("id") Integer id, @Param("dateEnvoi") LocalDateTime dateEnvoi);

    /**
     * Rechercher lignes dotation departementale.
     *
     * @param dateAccord
     *            the date accord
     * @return the list
     */
    @Query("select ldd from LigneDotationDepartement ldd inner join ldd.dotationDepartement dd inner join dd.dotationSousProgramme dsp "
            + " where ldd.dateEnvoi <= cast(:dateAccord as timestamp)")
    List<Long> rechercherLignesDotationDepartementale(@Param("dateAccord") LocalDateTime dateAccord);

    /**
     * Rechercher lignes pertes et renonciations.
     *
     * @param anneeCourante
     *            the annee courante
     * @return the list
     */
    @Query("select ldd from LigneDotationDepartement ldd where ldd.typeDotationDepartement in ('PERTE_DE_DOTATION', 'RENONCEMENT') "
            + "and ldd.dotationDepartement.dotationSousProgramme.dotationProgramme.annee = :annee "
            + "and ldd.dotationDepartement.dotationSousProgramme.dotationProgramme.programme.codeNumerique = :codePro")
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciations(@Param("codePro") String codeProgramme,
            @Param("annee") Integer anneeCourante);

    /**
     * Rechercher lignes pertes et renonciations par id collectivite.
     *
     * @param anneeCourante
     *            the annee courante
     * @return the list
     */
    @Query("select ldd from LigneDotationDepartement ldd where ldd.typeDotationDepartement in ('PERTE_DE_DOTATION', 'RENONCEMENT') "
            + "and ldd.dotationDepartement.dotationSousProgramme.dotationProgramme.annee = :annee " + "and ldd.collectivite.id = :idCollectivite")
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciationsParIdCollectivite(@Param("idCollectivite") Long idCollectivite,
            @Param("annee") Integer anneeCourante);

}

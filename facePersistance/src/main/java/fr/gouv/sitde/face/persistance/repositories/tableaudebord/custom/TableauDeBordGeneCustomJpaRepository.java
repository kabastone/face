/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom;

import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;

/**
 * The Interface TableauDeBordGeneCustomJpaRepository.
 *
 * @author a453029
 */
public interface TableauDeBordGeneCustomJpaRepository {

    /**
     * Rechercher tableau de bord generique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the tableau de bord gene dto
     */
    TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite);

}

package fr.gouv.sitde.face.persistance.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;

/**
 * The Interface CourrierAnnuelDepartementJpaRepository.
 */
public interface CourrierAnnuelDepartementJpaRepository extends JpaRepository<CourrierAnnuelDepartement, Long> {

    @Query("select cad from CourrierAnnuelDepartement cad " + "inner join fetch cad.departement dep "
            + "where cad.annee=:annee and dep.code=:codeDepartement")
    Optional<CourrierAnnuelDepartement> findCourrierAnnuelDepartementByCodeDepartementEtAnnee(@Param("codeDepartement") String codeDepartement,
            @Param("annee") Integer annee);
}

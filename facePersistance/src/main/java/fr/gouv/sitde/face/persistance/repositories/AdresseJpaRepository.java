/*
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.Adresse;

/**
 * The Interface AdresseJpaRepository.
 */
@Repository
public interface AdresseJpaRepository extends JpaRepository<Adresse, Long> {

    /**
     * Rechercher par departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the adresse
     */
    @Query("select adr from Departement dep left join dep.adresse adr where dep.id = :idDepartement")
    Optional<Adresse> rechercherParDepartement(@Param("idDepartement") Integer idDepartement);

}

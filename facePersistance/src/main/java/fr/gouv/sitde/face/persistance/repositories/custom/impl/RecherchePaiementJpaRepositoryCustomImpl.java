/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.custom.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.persistance.repositories.custom.RecherchePaiementJpaRepositoryCustom;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * @author A754839
 *
 */
public class RecherchePaiementJpaRepositoryCustomImpl implements RecherchePaiementJpaRepositoryCustom {

    /** The Constant AGE_MAX_DOSSIER_EN_ANNEES. */
    private static final int AGE_MAX_DOSSIER_EN_ANNEES = 4;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /**
     * entityManager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.persistance.repositories.custom.RecherchePaiementJpaRepositoryCustom#rechercherDossiersParCriteresPaiement(fr.gouv.sitde.
     * face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {

        // Construction de la requete
        StringBuilder jpqlQuery = new StringBuilder();
        jpqlQuery.append("select distinct dos from DossierSubvention dos inner join fetch dos.dotationCollectivite doco ");
        jpqlQuery.append("inner join doco.dotationDepartement.dotationSousProgramme.dotationProgramme dpr ");
        jpqlQuery.append("inner join fetch doco.collectivite col inner join fetch dos.demandesPaiement dpa ");
        jpqlQuery.append("left join fetch dpa.etatDemandePaiement edpa ");
        jpqlQuery.append("where dos.dateCreationDossier > :dateCreationMinimale ");

        // Calcul des conditions à partir des criteres de recherche
        jpqlQuery.append(getConditionsFromCritereRechercheDossierQo(criteresRecherche));

        // Ajout d'un tri antichronologique par date de creation.
        jpqlQuery.append("order by dos.dateCreationDossier desc , dos.id DESC");

        Query query = this.entityManager.createQuery(jpqlQuery.toString(), DossierSubvention.class);

        // Mise en place de la pagination
        if (criteresRecherche.getPageDemande() != null) {
            query.setFirstResult(criteresRecherche.getPageDemande().getIndexPremierResultat());
            query.setMaxResults(criteresRecherche.getPageDemande().getTaillePage());
        }

        // Renseignenment des parametres de la query à partir des criteres de recherche
        this.renseignerParametresRequeteRechercheDossiers(query, criteresRecherche);

        // Execution de la requete de resultats
        @SuppressWarnings("unchecked")
        List<DossierSubvention> listeDossiers = query.getResultList();

        // Construction de la pageReponse
        PageReponse<DossierSubvention> pageReponse = new PageReponse<>();
        pageReponse.setListeResultats(listeDossiers);

        // Recherche du nombre total de résultats
        Long countResult = this.getCountResultatRechercheDossiers(criteresRecherche);
        pageReponse.setNbTotalResultats(countResult);

        return pageReponse;
    }

    /**
     * Gets the conditions from critere recherche dossier qo.
     *
     * @param criteresRecherche
     *            the criteres recherche
     * @return the conditions from critere recherche dossier qo
     */
    private static String getConditionsFromCritereRechercheDossierQo(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {
        StringBuilder jpqlQuery = new StringBuilder();

        if (criteresRecherche.getAnneePaiement() != null) {
            jpqlQuery.append("and dpr.annee = :anneePaiement ");
        }

        if (!StringUtils.isEmpty(criteresRecherche.getCodeEtatDemande())) {
            jpqlQuery.append("and edpa.code like :etatDemande ");
        }

        if (criteresRecherche.getIdCollectivite() != null) {
            jpqlQuery.append("and dos.dotationCollectivite.collectivite.id = :idCollectivite ");
        }

        if (criteresRecherche.getIdDepartement() != null) {
            jpqlQuery.append("and dos.dotationCollectivite.collectivite.departement.id = :idDepartement ");
        }

        if (criteresRecherche.getIdSousProgramme() != null) {
            jpqlQuery.append("and dos.dotationCollectivite.dotationDepartement.dotationSousProgramme.sousProgramme.id = :idSousProgramme ");
        }

        if (!StringUtils.isBlank(criteresRecherche.getNumDossier())) {
            jpqlQuery.append("and lower(dos.numDossier) like lower(:numDossier) ");
        }

        return jpqlQuery.toString();
    }

    /**
     * Renseigner parametres requete recherche dossiers.
     *
     * @param query
     *            the query
     * @param criteresRecherche
     *            the criteres recherche
     */
    private void renseignerParametresRequeteRechercheDossiers(Query query, CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {

        // RG_Sub_101 La liste est uniquement alimentée par les dossiers des quatre dernières années
        LocalDateTime dateCreationMinimale = this.timeOperationTransverse.now().minusYears(AGE_MAX_DOSSIER_EN_ANNEES);
        query.setParameter("dateCreationMinimale", dateCreationMinimale);

        // Criteres de recherche
        if (criteresRecherche.getAnneePaiement() != null) {
            query.setParameter("anneePaiement", criteresRecherche.getAnneePaiement());
        }

        if (!StringUtils.isEmpty(criteresRecherche.getCodeEtatDemande())) {
            query.setParameter("etatDemande", criteresRecherche.getCodeEtatDemande());
        }

        if (criteresRecherche.getIdCollectivite() != null) {
            query.setParameter("idCollectivite", criteresRecherche.getIdCollectivite());
        }

        if (criteresRecherche.getIdDepartement() != null) {
            query.setParameter("idDepartement", criteresRecherche.getIdDepartement());
        }

        if (criteresRecherche.getIdSousProgramme() != null) {
            query.setParameter("idSousProgramme", criteresRecherche.getIdSousProgramme());
        }

        if (!StringUtils.isBlank(criteresRecherche.getNumDossier())) {
            query.setParameter("numDossier", StringUtils.trim(criteresRecherche.getNumDossier()));
        }
    }

    /**
     * Gets the count resultat recherche dossiers.
     *
     * @param criteresRecherche
     *            the criteres recherche
     * @return the count resultat recherche dossiers
     */
    private Long getCountResultatRechercheDossiers(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {

        // Construction de la requete
        StringBuilder jpqlCountQuery = new StringBuilder();
        jpqlCountQuery.append("select count(distinct dos) from DossierSubvention dos inner join dos.dotationCollectivite doco ");
        jpqlCountQuery.append("inner join doco.dotationDepartement.dotationSousProgramme.dotationProgramme dpr ");
        jpqlCountQuery.append("inner join doco.collectivite col inner join dos.demandesPaiement dpa ");
        jpqlCountQuery.append("left join dpa.etatDemandePaiement edpa ");
        jpqlCountQuery.append("where dos.dateCreationDossier > :dateCreationMinimale ");

        // Calcul des conditions à partir des criteres de recherche
        jpqlCountQuery.append(getConditionsFromCritereRechercheDossierQo(criteresRecherche));

        Query queryTotal = this.entityManager.createQuery(jpqlCountQuery.toString(), Long.class);

        // Renseignenment des parametres de la query à partir des criteres de recherche
        this.renseignerParametresRequeteRechercheDossiers(queryTotal, criteresRecherche);
        return (Long) queryTotal.getSingleResult();
    }
}

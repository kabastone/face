/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * Repository JPA de l'entité TransfertPaiement.
 *
 * @author Atos
 */
@Repository
public interface TransfertPaiementJpaRepository extends JpaRepository<TransfertPaiement, Long> {

    /**
     * Recherche tous les transferts de paiements a l'état 'VERSE'<br/>
     * ou 'DEMANDE' liés à une demande de paiement.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    @Query("select tp from TransfertPaiement tp " + "inner join fetch tp.demandePaiement dp " + "inner join fetch dp.dossierSubvention ds "
            + "where ds.id = :idDossierSubvention")
    List<TransfertPaiement> findTransfertPaiementByDossierSubvention(@Param("idDossierSubvention") Long idDossierSubvention);

    /**
     * Rechercher par num S fet legacyet num EJ.
     *
     * @param chorusNumSF
     *            the chorus num SF
     * @param chorusNumLigneLegacy
     *            the chorus num ligne legacy
     * @param chorusNumEJ
     *            the chorus num EJ
     * @return the transfert paiement
     */
    @Query("select distinct tp from TransfertPaiement tp " + "inner join tp.demandeSubvention dsu " + "inner join tp.demandePaiement dpa "
            + "inner join dpa.dossierSubvention dos " + "where dpa.chorusNumSf = :chorusNumSF "
            + "and dsu.chorusNumLigneLegacy = :chorusNumLigneLegacy " + "and dos.chorusNumEj = :chorusNumEJ")
    List<TransfertPaiement> rechercherParNumSFetLegacyetNumEJ(@Param("chorusNumSF") String chorusNumSF,
            @Param("chorusNumLigneLegacy") String chorusNumLigneLegacy, @Param("chorusNumEJ") String chorusNumEJ);
}

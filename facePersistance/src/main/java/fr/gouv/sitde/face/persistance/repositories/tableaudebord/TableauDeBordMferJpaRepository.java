/**
 *
 */
package fr.gouv.sitde.face.persistance.repositories.tableaudebord;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordMferCustomJpaRepository;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.custom.TableauDeBordMferCustomJpaRepositoryImpl;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;

/**
 * Repository JPA du tableau de bord MFER.
 *
 * @author a453029
 *
 */
@Repository
public interface TableauDeBordMferJpaRepository extends PagingAndSortingRepository<DossierSubvention, Long>, TableauDeBordMferCustomJpaRepository {

    /**
     * Rechercher demandes subvention tdb aqualifier subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubDemandee
     *            the etat dem sub demandee
     * @param etatDemSubCorrigee
     *            the etat dem sub corrigee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_QUALIFIER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAqualifierSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubDemandee") String etatDemSubDemandee, @Param("etatDemSubCorrigee") String etatDemSubCorrigee, Pageable pageable);

    /**
     * Rechercher demandes paiement tdb aqualifier paiement.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiDemandee
     *            the etat dem pai demandee
     * @param etatDemPaiCorrigee
     *            the etat dem pai corrigee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_QUALIFIER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAqualifierPaiement(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemPaiDemandee") String etatDemPaiDemandee, @Param("etatDemPaiCorrigee") String etatDemPaiCorrigee, Pageable pageable);

    /**
     * Rechercher demandes subvention tdb aaccorder subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubQualifiee
     *            the etat dem sub qualifiee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ACCORDER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAaccorderSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubQualifiee") String etatDemSubQualifiee, Pageable pageable);

    /**
     * Rechercher demandes paiement tdb aaccorder paiement.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiQualifiee
     *            the etat dem pai qualifiee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ACCORDER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAaccorderPaiement(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemPaiQualifiee") String etatDemPaiQualifiee, Pageable pageable);

    /**
     * Rechercher demandes subvention tdb aattribuer subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubApprouvee
     *            the etat dem sub approuvee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_ATTRIBUER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAattribuerSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubApprouvee") String etatDemSubApprouvee, Pageable pageable);

    /**
     * Rechercher demandes subvention tdb asignaler subvention.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemSubAnomalieDetectee
     *            the etat dem sub anomalie detectee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_SIGNALER_SUBVENTION_COUNT)
    Page<DemandeSubvention> rechercherDemandesSubventionTdbAsignalerSubvention(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemSubAnomalieDetectee") String etatDemSubAnomalieDetectee, Pageable pageable);

    /**
     * Rechercher demandes paiement tdb asignaler paiement.
     *
     * @param etatDossierOuvert
     *            the etat dossier ouvert
     * @param etatDemPaiAnomalieDetectee
     *            the etat dem pai anomalie detectee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_SIGNALER_PAIEMENT_COUNT)
    Page<DemandePaiement> rechercherDemandesPaiementTdbAsignalerPaiement(@Param("etatDossierOuvert") EtatDossierEnum etatDossierOuvert,
            @Param("etatDemPaiAnomalieDetectee") String etatDemPaiAnomalieDetectee, Pageable pageable);

    /**
     * Rechercher demandes prolongation tdb aprolonger subvention.
     *
     * @param etatDemProDemandee
     *            the etat dem pro demandee
     * @param pageable
     *            the pageable
     * @return the page
     */
    @Query(value = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION,
            countQuery = TableauDeBordMferCustomJpaRepositoryImpl.REQUETE_TDB_MFER_A_PROLONGER_SUBVENTION_COUNT)
    Page<DemandeProlongation> rechercherDemandesProlongationTdbAprolongerSubvention(@Param("etatDemProDemandee") String etatDemProDemandee,
            Pageable pageable);

}

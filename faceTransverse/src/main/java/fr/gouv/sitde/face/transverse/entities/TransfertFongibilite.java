package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The Class TransfertFongibilite.
 */
@Entity
@Table(name = "transfert_fongibilite")
@SequenceGenerator(name = "generatorTransfertFongibilite", sequenceName = "trf_id_seq", allocationSize = 1)
public class TransfertFongibilite implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1405257770026013418L;

    /** The id. */
    private Long id;

    /** The dotation collectivite origine. */
    private DotationCollectivite dotationCollectiviteOrigine;

    /** The dotation collectivite destination. */
    private DotationCollectivite dotationCollectiviteDestination;

    /** The montant. */
    private BigDecimal montant;

    /** The date transfert. */
    private LocalDateTime dateTransfert;

    /**
     * Gets the date transfert.
     *
     * @return the date transfert
     */
    @Column(name = "trf_date_transfert", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    public LocalDateTime getDateTransfert() {
        return this.dateTransfert;
    }

    /**
     * Sets the date transfert.
     *
     * @param dateTransfert
     *            the new date transfert
     */
    public void setDateTransfert(LocalDateTime dateTransfert) {
        this.dateTransfert = dateTransfert;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generatorTransfertFongibilite")
    @Column(name = "trf_id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the dotation collectivite.
     *
     * @return the dotation collectivite
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trf_dco_id_destination", nullable = false)
    public DotationCollectivite getDotationCollectiviteDestination() {
        return this.dotationCollectiviteDestination;
    }

    /**
     * Gets the dotation collectivite.
     *
     * @return the dotation collectivite
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trf_dco_id_origine", nullable = false)
    public DotationCollectivite getDotationCollectiviteOrigine() {
        return this.dotationCollectiviteOrigine;
    }

    /**
     * Sets the dotation collectivite origine.
     *
     * @param dotationCollectiviteOrigine
     *            the new dotation collectivite origine
     */
    public void setDotationCollectiviteOrigine(DotationCollectivite dotationCollectiviteOrigine) {
        this.dotationCollectiviteOrigine = dotationCollectiviteOrigine;
    }

    /**
     * Sets the dotation collectivite destination.
     *
     * @param dotationCollectiviteDestination
     *            the new dotation collectivite destination
     */
    public void setDotationCollectiviteDestination(DotationCollectivite dotationCollectiviteDestination) {
        this.dotationCollectiviteDestination = dotationCollectiviteDestination;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    @Column(name = "trf_montant", nullable = false)
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the new montant
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

}

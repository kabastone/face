/**
 *
 */
package fr.gouv.sitde.face.transverse.dotation;

import java.io.Serializable;
import java.math.BigDecimal;

import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * The Class DotationDepartementaleParSousProgrammeBo.
 *
 * @author
 */
public class DotationDepartementaleParSousProgrammeBo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -782758870610871373L;

    /** The abreviation. */
    private SousProgramme sousProgramme;

    /** The montant */
    private BigDecimal dspMontant;

    public DotationDepartementaleParSousProgrammeBo() {
    }

    public DotationDepartementaleParSousProgrammeBo(SousProgramme sousProgramme, BigDecimal dspMontant) {
        this.sousProgramme = sousProgramme;
        this.dspMontant = dspMontant;
    }

    public DotationDepartementaleParSousProgrammeBo(SousProgramme sousProgramme) {
        this.sousProgramme = sousProgramme;
        this.dspMontant = BigDecimal.ZERO;
    }

    public SousProgramme getSousProgramme() {
        return this.sousProgramme;
    }

    public void setSousProgramme(SousProgramme sousProgramme) {
        this.sousProgramme = sousProgramme;
    }

    public BigDecimal getDspMontant() {
        return this.dspMontant;
    }

    public void setDspMontant(BigDecimal dspMontant) {
        this.dspMontant = dspMontant;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.transverse.tableaudebord;

import java.io.Serializable;

/**
 * The Class TableauDeBordGeneDto.
 *
 * @author a453029
 */
public class TableauDeBordGeneDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -840720855909823015L;

    /** The nb ademander subvention. */
    private long nbAdemanderSubvention;

    /** The nb ademander paiement. */
    private long nbAdemanderPaiement;

    /** The nb acompleter subvention. */
    private long nbAcompleterSubvention;

    /** The nb acorriger subvention. */
    private long nbAcorrigerSubvention;

    /** The nb acorriger paiement. */
    private long nbAcorrigerPaiement;

    /** The nb acommencer paiement. */
    private long nbAcommencerPaiement;

    /** The nb asolder paiement. */
    private long nbAsolderPaiement;

    /**
     * Instantiates a new tableau de bord gene dto.
     */
    public TableauDeBordGeneDto() {
        // Constructeur non paramétré.
    }

    /**
     * Instantiates a new tableau de bord gene dto.
     *
     * @param nbAdemanderSubvention
     *            the nb ademander subvention
     * @param nbAdemanderPaiement
     *            the nb ademander paiement
     * @param nbAcompleterSubvention
     *            the nb acompleter subvention
     * @param nbAcorrigerSubvention
     *            the nb acorriger subvention
     * @param nbAcorrigerPaiement
     *            the nb acorriger paiement
     * @param nbAcommencerPaiement
     *            the nb acommencer paiement
     * @param nbAsolderPaiement
     *            the nb asolder paiement
     */
    public TableauDeBordGeneDto(long nbAdemanderSubvention, long nbAdemanderPaiement, long nbAcompleterSubvention, long nbAcorrigerSubvention,
            long nbAcorrigerPaiement, long nbAcommencerPaiement, long nbAsolderPaiement) {
        super();
        this.nbAdemanderSubvention = nbAdemanderSubvention;
        this.nbAdemanderPaiement = nbAdemanderPaiement;
        this.nbAcompleterSubvention = nbAcompleterSubvention;
        this.nbAcorrigerSubvention = nbAcorrigerSubvention;
        this.nbAcorrigerPaiement = nbAcorrigerPaiement;
        this.nbAcommencerPaiement = nbAcommencerPaiement;
        this.nbAsolderPaiement = nbAsolderPaiement;
    }

    /**
     * Gets the nb ademander subvention.
     *
     * @return the nbAdemanderSubvention
     */
    public long getNbAdemanderSubvention() {
        return this.nbAdemanderSubvention;
    }

    /**
     * Sets the nb ademander subvention.
     *
     * @param nbAdemanderSubvention
     *            the nbAdemanderSubvention to set
     */
    public void setNbAdemanderSubvention(long nbAdemanderSubvention) {
        this.nbAdemanderSubvention = nbAdemanderSubvention;
    }

    /**
     * Gets the nb ademander paiement.
     *
     * @return the nbAdemanderPaiement
     */
    public long getNbAdemanderPaiement() {
        return this.nbAdemanderPaiement;
    }

    /**
     * Sets the nb ademander paiement.
     *
     * @param nbAdemanderPaiement
     *            the nbAdemanderPaiement to set
     */
    public void setNbAdemanderPaiement(long nbAdemanderPaiement) {
        this.nbAdemanderPaiement = nbAdemanderPaiement;
    }

    /**
     * Gets the nb acompleter subvention.
     *
     * @return the nbAcompleterSubvention
     */
    public long getNbAcompleterSubvention() {
        return this.nbAcompleterSubvention;
    }

    /**
     * Sets the nb acompleter subvention.
     *
     * @param nbAcompleterSubvention
     *            the nbAcompleterSubvention to set
     */
    public void setNbAcompleterSubvention(long nbAcompleterSubvention) {
        this.nbAcompleterSubvention = nbAcompleterSubvention;
    }

    /**
     * Gets the nb acorriger subvention.
     *
     * @return the nbAcorrigerSubvention
     */
    public long getNbAcorrigerSubvention() {
        return this.nbAcorrigerSubvention;
    }

    /**
     * Sets the nb acorriger subvention.
     *
     * @param nbAcorrigerSubvention
     *            the nbAcorrigerSubvention to set
     */
    public void setNbAcorrigerSubvention(long nbAcorrigerSubvention) {
        this.nbAcorrigerSubvention = nbAcorrigerSubvention;
    }

    /**
     * Gets the nb acorriger paiement.
     *
     * @return the nbAcorrigerPaiement
     */
    public long getNbAcorrigerPaiement() {
        return this.nbAcorrigerPaiement;
    }

    /**
     * Sets the nb acorriger paiement.
     *
     * @param nbAcorrigerPaiement
     *            the nbAcorrigerPaiement to set
     */
    public void setNbAcorrigerPaiement(long nbAcorrigerPaiement) {
        this.nbAcorrigerPaiement = nbAcorrigerPaiement;
    }

    /**
     * Gets the nb acommencer paiement.
     *
     * @return the nbAcommencerPaiement
     */
    public long getNbAcommencerPaiement() {
        return this.nbAcommencerPaiement;
    }

    /**
     * Sets the nb acommencer paiement.
     *
     * @param nbAcommencerPaiement
     *            the nbAcommencerPaiement to set
     */
    public void setNbAcommencerPaiement(long nbAcommencerPaiement) {
        this.nbAcommencerPaiement = nbAcommencerPaiement;
    }

    /**
     * Gets the nb asolder paiement.
     *
     * @return the nbAsolderPaiement
     */
    public long getNbAsolderPaiement() {
        return this.nbAsolderPaiement;
    }

    /**
     * Sets the nb asolder paiement.
     *
     * @param nbAsolderPaiement
     *            the nbAsolderPaiement to set
     */
    public void setNbAsolderPaiement(long nbAsolderPaiement) {
        this.nbAsolderPaiement = nbAsolderPaiement;
    }

}

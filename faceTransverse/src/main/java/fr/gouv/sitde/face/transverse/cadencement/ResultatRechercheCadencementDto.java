/**
 *
 */
package fr.gouv.sitde.face.transverse.cadencement;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class ResultatRechercheCadencementDto.
 *
 * @author a768251
 */
public class ResultatRechercheCadencementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8054100381169393184L;

    /** The sous programme code. */
    private String sousProgrammeCode;

    /** The sous programme abreviation. */
    private String sousProgrammeAbreviation;

    /** The sous programme description. */
    private String sousProgrammeDescription;

    /** The montant subventions. */
    private BigDecimal montantSubventions = BigDecimal.ZERO;

    /** The cadencement annee 1. */
    private BigDecimal cadencementAnnee1 = BigDecimal.ZERO;

    /** The cadencement annee 2. */
    private BigDecimal cadencementAnnee2 = BigDecimal.ZERO;

    /** The cadencement annee 3. */
    private BigDecimal cadencementAnnee3 = BigDecimal.ZERO;

    /** The cadencement annee 4. */
    private BigDecimal cadencementAnnee4 = BigDecimal.ZERO;

    /** The cadencement annee prolongation. */
    private BigDecimal cadencementAnneeProlongation = BigDecimal.ZERO;

    /** The cadencement est valide. */
    private Boolean cadencementEstValide;

    public ResultatRechercheCadencementDto() {

    }

    public ResultatRechercheCadencementDto(String sousProgrammeCode, String sousProgrammeAbreviation, String sousProgrammeDescription,
            BigDecimal montantSubventions, BigDecimal cadencementAnnee1, BigDecimal cadencementAnnee2, BigDecimal cadencementAnnee3,
            BigDecimal cadencementAnnee4, BigDecimal cadencementAnneeProlongation, Boolean cadencementEstValide) {
        super();
        this.sousProgrammeCode = sousProgrammeCode;
        this.sousProgrammeAbreviation = sousProgrammeAbreviation;
        this.sousProgrammeDescription = sousProgrammeDescription;
        this.montantSubventions = montantSubventions;
        this.cadencementAnnee1 = cadencementAnnee1;
        this.cadencementAnnee2 = cadencementAnnee2;
        this.cadencementAnnee3 = cadencementAnnee3;
        this.cadencementAnnee4 = cadencementAnnee4;
        this.cadencementAnneeProlongation = cadencementAnneeProlongation;
        this.cadencementEstValide = cadencementEstValide;
    }

    /**
     * Gets the sous programme code.
     *
     * @return the sousProgrammeCode
     */
    public String getSousProgrammeCode() {
        return this.sousProgrammeCode;
    }

    /**
     * Sets the sous programme code.
     *
     * @param sousProgrammeCode
     *            the sousProgrammeCode to set
     */
    public void setSousProgrammeCode(String sousProgrammeCode) {
        this.sousProgrammeCode = sousProgrammeCode;
    }

    /**
     * Gets the sous programme abreviation.
     *
     * @return the sousProgrammeAbreviation
     */
    public String getSousProgrammeAbreviation() {
        return this.sousProgrammeAbreviation;
    }

    /**
     * Sets the sous programme abreviation.
     *
     * @param sousProgrammeAbreviation
     *            the sousProgrammeAbreviation to set
     */
    public void setSousProgrammeAbreviation(String sousProgrammeAbreviation) {
        this.sousProgrammeAbreviation = sousProgrammeAbreviation;
    }

    /**
     * Gets the sous programme description.
     *
     * @return the sousProgrammeDescription
     */
    public String getSousProgrammeDescription() {
        return this.sousProgrammeDescription;
    }

    /**
     * Sets the sous programme description.
     *
     * @param sousProgrammeDescription
     *            the sousProgrammeDescription to set
     */
    public void setSousProgrammeDescription(String sousProgrammeDescription) {
        this.sousProgrammeDescription = sousProgrammeDescription;
    }

    /**
     * Gets the montant subventions.
     *
     * @return the montantSubventions
     */
    public BigDecimal getMontantSubventions() {
        return this.montantSubventions;
    }

    /**
     * Sets the montant subventions.
     *
     * @param montantSubventions
     *            the montantSubventions to set
     */
    public void setMontantSubventions(BigDecimal montantSubventions) {
        this.montantSubventions = montantSubventions;
    }

    /**
     * Gets the cadencement annee 1.
     *
     * @return the cadencementAnnee1
     */
    public BigDecimal getCadencementAnnee1() {
        return this.cadencementAnnee1;
    }

    /**
     * Sets the cadencement annee 1.
     *
     * @param cadencementAnnee1
     *            the cadencementAnnee1 to set
     */
    public void setCadencementAnnee1(BigDecimal cadencementAnnee1) {
        this.cadencementAnnee1 = cadencementAnnee1;
    }

    /**
     * Gets the cadencement annee 2.
     *
     * @return the cadencementAnnee2
     */
    public BigDecimal getCadencementAnnee2() {
        return this.cadencementAnnee2;
    }

    /**
     * Sets the cadencement annee 2.
     *
     * @param cadencementAnnee2
     *            the cadencementAnnee2 to set
     */
    public void setCadencementAnnee2(BigDecimal cadencementAnnee2) {
        this.cadencementAnnee2 = cadencementAnnee2;
    }

    /**
     * Gets the cadencement annee 3.
     *
     * @return the cadencementAnnee3
     */
    public BigDecimal getCadencementAnnee3() {
        return this.cadencementAnnee3;
    }

    /**
     * Sets the cadencement annee 3.
     *
     * @param cadencementAnnee3
     *            the cadencementAnnee3 to set
     */
    public void setCadencementAnnee3(BigDecimal cadencementAnnee3) {
        this.cadencementAnnee3 = cadencementAnnee3;
    }

    /**
     * Gets the cadencement annee 4.
     *
     * @return the cadencementAnnee4
     */
    public BigDecimal getCadencementAnnee4() {
        return this.cadencementAnnee4;
    }

    /**
     * Sets the cadencement annee 4.
     *
     * @param cadencementAnnee4
     *            the cadencementAnnee4 to set
     */
    public void setCadencementAnnee4(BigDecimal cadencementAnnee4) {
        this.cadencementAnnee4 = cadencementAnnee4;
    }

    /**
     * Gets the cadencement annee prolongation.
     *
     * @return the cadencementAnneeProlongation
     */
    public BigDecimal getCadencementAnneeProlongation() {
        return this.cadencementAnneeProlongation;
    }

    /**
     * Sets the cadencement annee prolongation.
     *
     * @param cadencementAnneeProlongation
     *            the cadencementAnneeProlongation to set
     */
    public void setCadencementAnneeProlongation(BigDecimal cadencementAnneeProlongation) {
        this.cadencementAnneeProlongation = cadencementAnneeProlongation;
    }

    /**
     * Gets the cadencement est valide.
     *
     * @return the cadencementEstValide
     */
    public Boolean getCadencementEstValide() {
        return this.cadencementEstValide;
    }

    /**
     * Sets the cadencement est valide.
     *
     * @param cadencementEstValide
     *            the cadencementEstValide to set
     */
    public void setCadencementEstValide(Boolean cadencementEstValide) {
        this.cadencementEstValide = cadencementEstValide;
    }

}

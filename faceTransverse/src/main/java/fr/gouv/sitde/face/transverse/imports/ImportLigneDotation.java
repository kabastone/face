/**
 *
 */
package fr.gouv.sitde.face.transverse.imports;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import javax.persistence.Entity;

/**
 * The Class ImportLigneDotation.
 *
 * @author Atos
 */
@Entity
public class ImportLigneDotation implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5791061101605088381L;

    /** Numérique du departement sans les zeros. */
    private String codeDepartement;

    /** The map montant. */
    private Map<SousProgrammeTypeEnum, BigDecimal> mapMontant = new EnumMap<>(SousProgrammeTypeEnum.class);

    /** Somme des dotations. */
    private BigDecimal sommeDepartement;

    /** Penalite de non regroupement. */
    private BigDecimal penaliteNonRegroupement;

    /** Penalite de penaliteGestionStock. */
    private BigDecimal penaliteGestionStock;

    /**
     * Gets the code departement.
     *
     * @return the code departement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Sets the code departement.
     *
     * @param codeDepartement
     *            the new code departement
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Gets the renforcement reseaux.
     *
     * @return the renforcementReseaux
     */
    public BigDecimal getRenforcementReseaux() {
        return this.mapMontant.get(SousProgrammeTypeEnum.RENFORCEMENT_RESEAUX);
    }

    /**
     * Sets the renforcement reseaux.
     *
     * @param renforcementReseaux
     *            the renforcementReseaux to set
     */
    public void setRenforcementReseaux(BigDecimal renforcementReseaux) {
        this.mapMontant.put(SousProgrammeTypeEnum.RENFORCEMENT_RESEAUX, renforcementReseaux);
    }

    /**
     * Gets the extension reseaux.
     *
     * @return the extensionReseaux
     */
    public BigDecimal getExtensionReseaux() {
        return this.mapMontant.get(SousProgrammeTypeEnum.EXTENSION_RESEAUX);
    }

    /**
     * Sets the extension reseaux.
     *
     * @param extensionReseaux
     *            the extensionReseaux to set
     */
    public void setExtensionReseaux(BigDecimal extensionReseaux) {
        this.mapMontant.put(SousProgrammeTypeEnum.EXTENSION_RESEAUX, extensionReseaux);
    }

    /**
     * Gets the enfouissement et facade.
     *
     * @return the enfouissementEtFacade
     */
    public BigDecimal getEnfouissementEtFacade() {
        return this.mapMontant.get(SousProgrammeTypeEnum.ENFOUISSEMENT_POSE_FACADE);
    }

    /**
     * Sets the enfouissement et facade.
     *
     * @param enfouissementEtFacade
     *            the enfouissementEtFacade to set
     */
    public void setEnfouissementEtFacade(BigDecimal enfouissementEtFacade) {
        this.mapMontant.put(SousProgrammeTypeEnum.ENFOUISSEMENT_POSE_FACADE, enfouissementEtFacade);
    }

    /**
     * Gets the securisation.
     *
     * @return the securisation
     */
    public BigDecimal getSecurisation() {
        return this.mapMontant.get(SousProgrammeTypeEnum.SECURISATION);
    }

    /**
     * Sets the securisation.
     *
     * @param securisation
     *            the securisation to set
     */
    public void setSecurisation(BigDecimal securisation) {
        this.mapMontant.put(SousProgrammeTypeEnum.SECURISATION, securisation);
    }

    /**
     * Gets the somme departement.
     *
     * @return the sommeDepartement
     */
    public BigDecimal getSommeDepartement() {
        return this.sommeDepartement;
    }

    /**
     * Sets the somme departement.
     *
     * @param sommeDepartement
     *            the sommeDepartement to set
     */
    public void setSommeDepartement(BigDecimal sommeDepartement) {
        this.sommeDepartement = sommeDepartement;
    }

    /**
     * Gets the penalite non regroupement.
     *
     * @return the penaliteNonRegroupement
     */
    public BigDecimal getPenaliteNonRegroupement() {
        return this.penaliteNonRegroupement;
    }

    /**
     * Sets the penalite non regroupement.
     *
     * @param penaliteNonRegroupement
     *            the penaliteNonRegroupement to set
     */
    public void setPenaliteNonRegroupement(BigDecimal penaliteNonRegroupement) {
        this.penaliteNonRegroupement = penaliteNonRegroupement;
    }

    /**
     * Gets the penalite gestion stock.
     *
     * @return the penaliteGestionStock
     */
    public BigDecimal getPenaliteGestionStock() {
        return this.penaliteGestionStock;
    }

    /**
     * Sets the penalite gestion stock.
     *
     * @param penaliteGestionStock
     *            the penaliteGestionStock to set
     */
    public void setPenaliteGestionStock(BigDecimal penaliteGestionStock) {
        this.penaliteGestionStock = penaliteGestionStock;
    }

    /**
     * Gets the montant par sous programme.
     *
     * @param sousProgrammeType
     *
     *
     * @return the montant par sous programme
     */
    public BigDecimal getMontantParSousProgramme(SousProgrammeTypeEnum sousProgrammeType) {
        return this.mapMontant.get(sousProgrammeType);
    }
}

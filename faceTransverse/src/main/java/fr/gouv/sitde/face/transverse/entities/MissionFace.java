/**
 *
 */
package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class MissionFace.
 *
 * @author A687370
 */
@Entity
@Table(name = "r_mission_face")
public class MissionFace implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5460499761737011648L;

    /** Clé primaire de la table. */
    private Boolean actif;

    /** Nom du ministère. */
    private String ministere;

    /** Nom de la direction. */
    private String direction;

    /** Nom de la sous-direction. */
    private String sousDirection;

    /** Nom de la mission. */
    private String mission;

    /** Adresse de la mission. */
    private String adresse;

    /** Téléphone de la mission. */
    private String tel;

    /** Fax de la mission. */
    private String fax;

    /** Fonction du signataire. */
    private String signataireFonction;

    /** Nom du signataire. */
    private String signataireNom;

    /** Fonction du signataire DCB. */
    private String signataireDCB;

    /** Intitulé de la fonction du ministre. */
    private String ministreFonction;

    /** Nom de la délégation du ministre. */
    private String ministreDelegation;

    /**
     * Checks if is actif.
     *
     * @return the actif
     */
    @Id
    @Column(name = "mis_actif", unique = true, nullable = false)
    public Boolean isActif() {
        return this.actif;
    }

    /**
     * Sets the actif.
     *
     * @param actif
     *            the actif to set
     */
    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    /**
     * Gets the ministere.
     *
     * @return the ministere
     */
    @Column(name = "mis_ministere", unique = true, nullable = false, length = 150)
    public String getMinistere() {
        return this.ministere;
    }

    /**
     * Sets the ministere.
     *
     * @param ministere
     *            the ministere to set
     */
    public void setMinistere(String ministere) {
        this.ministere = ministere;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    @Column(name = "mis_direction", unique = true, nullable = false, length = 150)
    public String getDirection() {
        return this.direction;
    }

    /**
     * Sets the direction.
     *
     * @param direction
     *            the direction to set
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * Gets the sous direction.
     *
     * @return the sousDirection
     */
    @Column(name = "mis_sous_direction", unique = true, nullable = false, length = 150)
    public String getSousDirection() {
        return this.sousDirection;
    }

    /**
     * Sets the sous direction.
     *
     * @param sousDirection
     *            the sousDirection to set
     */
    public void setSousDirection(String sousDirection) {
        this.sousDirection = sousDirection;
    }

    /**
     * Gets the mission.
     *
     * @return the mission
     */
    @Column(name = "mis_mission", unique = true, nullable = false, length = 150)
    public String getMission() {
        return this.mission;
    }

    /**
     * Sets the mission.
     *
     * @param mission
     *            the mission to set
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     * Gets the adresse.
     *
     * @return the adresse
     */
    @Column(name = "mis_adresse", unique = true, nullable = false, length = 150)
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Sets the adresse.
     *
     * @param adresse
     *            the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Gets the tel.
     *
     * @return the tel
     */
    @Column(name = "mis_tel", unique = true, nullable = false, length = 30)
    public String getTel() {
        return this.tel;
    }

    /**
     * Sets the tel.
     *
     * @param tel
     *            the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * Gets the fax.
     *
     * @return the fax
     */
    @Column(name = "mis_fax", unique = true, nullable = false, length = 30)
    public String getFax() {
        return this.fax;
    }

    /**
     * Sets the fax.
     *
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Gets the signataire fonction.
     *
     * @return the signataireFonction
     */
    @Column(name = "mis_signataire_fonction", unique = true, nullable = false, length = 150)
    public String getSignataireFonction() {
        return this.signataireFonction;
    }

    /**
     * Sets the signataire fonction.
     *
     * @param signataireFonction
     *            the signataireFonction to set
     */
    public void setSignataireFonction(String signataireFonction) {
        this.signataireFonction = signataireFonction;
    }

    /**
     * Gets the signataire nom.
     *
     * @return the signataireNom
     */
    @Column(name = "mis_signataire_nom", unique = true, nullable = false, length = 150)
    public String getSignataireNom() {
        return this.signataireNom;
    }

    /**
     * Sets the signataire nom.
     *
     * @param signataireNom
     *            the signataireNom to set
     */
    public void setSignataireNom(String signataireNom) {
        this.signataireNom = signataireNom;
    }

    /**
     * @return the signataireDCB
     */
    @Column(name = "mis_signataire_dcb", unique = true, nullable = false, length = 150)
    public String getSignataireDCB() {
        return this.signataireDCB;
    }

    /**
     * @param signataireDCB
     *            the signataireDCB to set
     */
    public void setSignataireDCB(String signataireDCB) {
        this.signataireDCB = signataireDCB;
    }

    /**
     * Gets the ministre fonction.
     *
     * @return the ministreFonction
     */
    @Column(name = "mis_ministre_fonction", unique = true, nullable = false, length = 150)
    public String getMinistreFonction() {
        return this.ministreFonction;
    }

    /**
     * Sets the ministre fonction.
     *
     * @param ministreFonction
     *            the ministreFonction to set
     */
    public void setMinistreFonction(String ministreFonction) {
        this.ministreFonction = ministreFonction;
    }

    /**
     * Gets the ministre delegation.
     *
     * @return the ministreDelegation
     */
    @Column(name = "mis_ministre_delegation", unique = true, nullable = false, length = 150)
    public String getMinistreDelegation() {
        return this.ministreDelegation;
    }

    /**
     * Sets the ministre delegation.
     *
     * @param ministreDelegation
     *            the ministreDelegation to set
     */
    public void setMinistreDelegation(String ministreDelegation) {
        this.ministreDelegation = ministreDelegation;
    }

}

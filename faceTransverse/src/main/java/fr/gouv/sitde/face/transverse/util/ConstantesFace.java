/**
 *
 */
package fr.gouv.sitde.face.transverse.util;

/**
 * Constantes utilisées par l'application.
 *
 * @author Atos
 *
 */
public final class ConstantesFace {

    /** Base name du bundle des messages de validation metier (fichiers properties pour les RegleGestionException). */
    public static final String BASE_NAME_BUNDLE_MSG_VALIDATION_METIER = "ValidationMessages";

    /** Base name du bundle des libellés de référentiel (utilisé par les enums). */
    public static final String BASE_NAME_BUNDLE_LIBELLE_REFERENTIEL = "libelleReferentiel";

    /** Pour retour avec succès d'un controller REST (idéalement pour une suppression). */
    public static final String STATUS_OK = "{\"status\": \"OK\"}";

    /** The Constant MAX_TYPES_DOCUMENT_PAR_FAMILLE. */
    public static final int MAX_TYPES_DOCUMENT_PAR_FAMILLE = 99;

    /** The Constant MAX_DOCUMENTS_CHORUS. */
    public static final int MAX_DOCUMENTS_CHORUS = 99_999;

    /** The Constant FORMAT_DATE. */
    public static final String FORMAT_DATE = "dd/MM/yyyy";

    /** The Constant FORMAT_DATE_DOCUMENTS. */
    public static final String FORMAT_DATE_DOCUMENTS = "dd MMMM yyyy";

    /** Code de demande de paiement initial. */
    public static final String CODE_DEM_PAIEMENT_INITIAL = "INITIAL";

    /** Code de demande de subvention initial. */
    public static final String CODE_DEM_SUBVENTION_INITIAL = "INITIAL";

    /** The Constant CODE_DEM_PROLONGATION_INITIAL. */
    public static final String CODE_DEM_PROLONGATION_INITIAL = "INITIAL";

    /**
     * Constante pour l'entête HTTP 'Content-Disposition' : permet d'indiquer si le contenu doit être affiché en ligne dans le navigateur ou
     * téléchargé localement.
     */
    public static final String ENTETE_CONTENT_DISPOSITION = "Content-Disposition";

    /** Constante pour l'entête HTTP 'Content-Type' : permet d'indiquer le contenu envoyé. */
    public static final String ENTETE_CONTENT_TYPE = "Content-Type";

    /** The Constant VIRGULE. */
    public static final char VIRGULE = ',';

    /** The Constant FORMAT_DATE_CHORUS. */
    public static final String FORMAT_DATE_CHORUS = "yyyyMMdd";

    /** The Constant FORMAT_DATE_FEN0111A. */
    public static final String FORMAT_DATE_FEN0111A = "yyyy-MM-dd";

    /** The Constant ISO_8859_15. */
    public static final String ISO_8859_15 = "ISO-8859-15";

    /** The Constant CODE_SP_SECURISATION */
    public static final String CODE_SP_SECURISATION = "SN";

    /** The Constant NUMERO_SP_SECURISATION */
    public static final String NUMERO_SP_SECURISATION = "06";

    /** The Constant MAX_CHIFFRES_CHORUS_LIGNE. */
    public static final int MAX_CHIFFRES_CHORUS_LIGNE = 6;

    public static final int NOMBRE_ANNEE_DOSSIER = 4;

    /**
     * La classe ConstantesFaceCommun est une classe utilitaire qui ne contient que des variables statiques. Elle n'a pas à être instanciée.
     */
    private ConstantesFace() {
        // Constructeur privé
    }
}

package fr.gouv.sitde.face.transverse.exceptions;

/**
 * Exception technique (étend runtime exception).
 *
 * @author Atos
 *
 */
public class TechniqueException extends RuntimeException {

    /**
     * Attribut de version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur.
     *
     * @param message
     *            le message d'erreur
     */
    public TechniqueException(final String message) {
        super(message);
    }

    /**
     * Constructeur paramétré cause.
     *
     * @param cause
     *            l'exception source
     */
    public TechniqueException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructeur paramétré message.
     *
     * @param message
     *            le message d'erreur
     * @param cause
     *            l'exception source
     */
    public TechniqueException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

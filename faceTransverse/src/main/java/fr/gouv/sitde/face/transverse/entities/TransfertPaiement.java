package fr.gouv.sitde.face.transverse.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The Class TransfertPaiement.
 */
@Entity
@Table(name = "transfert_paiement")
@SequenceGenerator(name = "generatorTransfertPaiement", sequenceName = "transfert_paiement_trp_id_seq", allocationSize = 1)
public class TransfertPaiement extends TrackedEntity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1052623185365156187L;

    /** The id. */
    private Long id;

    /** The chorusnum ligne legacy. */
    private String chorusNumLigneLegacy;

    /** The montant. */
    private BigDecimal montant = BigDecimal.ZERO;

    /** The LocalDateTime Versement. */
    private LocalDateTime dateVersement;

    /** The LocalDateTime certification. */
    private LocalDateTime dateCertification;

    /** The demande paiement. */
    private DemandePaiement demandePaiement;

    /** The demande subvention. */
    private DemandeSubvention demandeSubvention;

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generatorTransfertPaiement")
    @Column(name = "trp_id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    @Column(name = "trp_montant", nullable = false, precision = 15)
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * Gets the chorus num ligne legacy.
     *
     * @return the chorus num ligne legacy
     */
    @Column(name = "trp_chorus_num_ligne_legacy")
    public String getChorusNumLigneLegacy() {
        return this.chorusNumLigneLegacy;
    }

    /**
     * Sets the chorus num ligne legacy.
     *
     * @param chorusNumLigneLegacy
     *            the new chorus num ligne legacy
     */
    public void setChorusNumLigneLegacy(String chorusNumLigneLegacy) {
        this.chorusNumLigneLegacy = chorusNumLigneLegacy;
    }

    /**
     * Gets the date versement.
     *
     * @return the date versement
     */
    @Column(name = "trp_date_versement", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    public LocalDateTime getDateVersement() {
        return this.dateVersement;
    }

    /**
     * Sets the date versement.
     *
     * @param dateVersement
     *            the new date versement
     */
    public void setDateVersement(LocalDateTime dateVersement) {
        this.dateVersement = dateVersement;
    }

    /**
     * Gets the demande paiement.
     *
     * @return the demande paiement
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trp_dpa_id", nullable = false)
    public DemandePaiement getDemandePaiement() {
        return this.demandePaiement;
    }

    /**
     * Sets the demande paiement.
     *
     * @param demandePaiement
     *            the new demande paiement
     */
    public void setDemandePaiement(DemandePaiement demandePaiement) {
        this.demandePaiement = demandePaiement;
    }

    /**
     * Gets the demande subvention.
     *
     * @return the demande subvention
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trp_dsu_id", nullable = false)
    public DemandeSubvention getDemandeSubvention() {
        return this.demandeSubvention;
    }

    /**
     * Sets the demande subvention.
     *
     * @param demandeSubvention
     *            the new demande subvention
     */
    public void setDemandeSubvention(DemandeSubvention demandeSubvention) {
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * Gets the LocalDateTime certification.
     *
     * @return the LocalDateTime certification
     */

    @Column(name = "trp_date_certification", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    public LocalDateTime getDateCertification() {
        return this.dateCertification;
    }

    /**
     * Sets the LocalDateTime certification.
     *
     * @param dateCertification
     *            the dateCertification to set
     */
    public void setDateCertification(LocalDateTime dateCertification) {
        this.dateCertification = dateCertification;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

/**
 * The Class TrackedEntity.
 *
 * @author a453029
 */
@MappedSuperclass
public class TrackedEntity implements java.io.Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3603757744077080901L;

    /** The LocalDateTime creation. */
    protected LocalDateTime dateCreation;

    /** The LocalDateTime derniere modif. */
    protected LocalDateTime dateDerniereModif;

    /** The version. */
    protected int version;

    /**
     * On create.
     */
    @PrePersist
    protected void onCreate() {
        this.dateCreation = LocalDateTime.now();
        this.dateDerniereModif = LocalDateTime.now();
    }

    /**
     * On update.
     */
    @PreUpdate
    protected void onUpdate() {
        this.dateDerniereModif = LocalDateTime.now();
    }

    /**
     * Gets the LocalDateTime creation.
     *
     * @return the dateCreation
     */

    @Column(name = "date_creation", nullable = false, updatable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    public LocalDateTime getDateCreation() {
        return this.dateCreation;
    }

    /**
     * Sets the LocalDateTime creation.
     *
     * @param dateCreation
     *            the dateCreation to set
     */
    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Gets the LocalDateTime derniere modif.
     *
     * @return the dateDerniereModif
     */

    @Column(name = "date_derniere_modif", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    public LocalDateTime getDateDerniereModif() {
        return this.dateDerniereModif;
    }

    /**
     * Sets the LocalDateTime derniere modif.
     *
     * @param dateDerniereModif
     *            the dateDerniereModif to set
     */
    public void setDateDerniereModif(LocalDateTime dateDerniereModif) {
        this.dateDerniereModif = dateDerniereModif;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    @Version
    @Column(name = "version", nullable = false)
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }
}

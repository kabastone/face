package fr.gouv.sitde.face.transverse.exceptions;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;

/**
 * Regle gestion Exception .
 *
 * @author Atos
 *
 */
public class RegleGestionException extends RuntimeException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -6963457512110388605L;

    /**
     * Tableau des paramètres de la propriété "message" (qui est une clé d'un fichier properties).
     */
    private final String[] params;

    /**
     * Liste des messages d'erreur (cas d'erreurs multiples).
     */
    private final List<MessageProperty> erreursMultiples = new ArrayList<>();

    /**
     * Constructeur non paramétré.
     */
    public RegleGestionException() {
        super();
        this.params = null;
    }

    /**
     * Constructeur paramétré.
     *
     * @param message
     *            the message
     */
    public RegleGestionException(final String message) {
        super(message);
        this.params = null;

        MessageProperty msgProperty = new MessageProperty(message);
        this.addMessageError(msgProperty);
    }

    /**
     * Constructeur paramétré.
     *
     * @param message
     *            le message d'erreur (clé d'un fichier properties)
     * @param params
     *            les parametres supplémentaires (paramètres de la clé)
     */
    public RegleGestionException(final String message, final String... params) {
        super(message);
        this.params = params;

        MessageProperty msgProperty = new MessageProperty(message, params);
        this.addMessageError(msgProperty);
    }

    /**
     * Constructeur paramétré.
     *
     * @param erreursMultiples
     *            la liste d'erreurs multiples
     * @param message
     *            le message de l'exception
     * @param params
     *            the params
     */
    public RegleGestionException(final List<MessageProperty> erreursMultiples, final String message, final String... params) {
        super(message);
        this.params = params;
        this.erreursMultiples.addAll(erreursMultiples);
    }

    /**
     * Constructeur paramétré.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public RegleGestionException(final String message, final Throwable cause) {
        super(message, cause);
        this.params = null;

        MessageProperty msgProperty = new MessageProperty(message);
        this.addMessageError(msgProperty);
    }

    /**
     * Ajoute un message d'erreur à la liste des messages d'erreur de l'exception.
     *
     * @param messageErreur
     *            Le message d'erreur a ajouter
     */
    public final void addMessageError(final MessageProperty messageErreur) {
        if (messageErreur != null) {
            this.erreursMultiples.add(messageErreur);
        }
    }

    /**
     * <b>Retourne un clone</b> du tableau contenant les paramètres de la propriété "message" (qui est une clé de fichier properties).
     *
     * @return un tableau clone de la property params de l'exception.
     */
    public String[] getParams() {
        return this.params == null ? null : this.params.clone();
    }

    /**
     * Renvoie la liste des messages d'erreur (cas d'erreurs multiples).
     * <p>
     * la liste ne peut pas être null mais elle peut être vide
     * </p>
     *
     * @return the erreursMultiples
     */
    public List<MessageProperty> getErreursMultiples() {
        return this.erreursMultiples;
    }

}

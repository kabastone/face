/**
 *
 */
package fr.gouv.sitde.face.transverse.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class OrigineDocumentDto.
 *
 * @author a453029
 */
public class OrigineDocumentDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4718869892691976485L;

    /** The liste id collectivites origine. */
    private List<Long> listeIdCollectivitesOrigine = new ArrayList<>(0);

    /** The doc sans lien collectivite. */
    private boolean docSansLienCollectivite;

    /**
     * @return the listeIdCollectivitesOrigine
     */
    public List<Long> getListeIdCollectivitesOrigine() {
        return this.listeIdCollectivitesOrigine;
    }

    /**
     * @param listeIdCollectivitesOrigine
     *            the listeIdCollectivitesOrigine to set
     */
    public void setListeIdCollectivitesOrigine(List<Long> listeIdCollectivitesOrigine) {
        this.listeIdCollectivitesOrigine = listeIdCollectivitesOrigine;
    }

    /**
     * @return the docSansLienCollectivite
     */
    public boolean isDocSansLienCollectivite() {
        return this.docSansLienCollectivite;
    }

    /**
     * @param docSansLienCollectivite
     *            the docSansLienCollectivite to set
     */
    public void setDocSansLienCollectivite(boolean docSansLienCollectivite) {
        this.docSansLienCollectivite = docSansLienCollectivite;
    }

}

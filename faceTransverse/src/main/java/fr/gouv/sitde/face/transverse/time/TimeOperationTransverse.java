/**
 *
 */
package fr.gouv.sitde.face.transverse.time;

import java.time.LocalDateTime;

/**
 * The Interface TimeOperationTransverse.
 *
 * @author A754839
 */
public interface TimeOperationTransverse {

    /**
     * Gets the 31 december of current year.
     *
     * @return the 31 december of current year
     */
    public LocalDateTime get31DecemberOfCurrentYear();

    /**
     * Gets the local date time of.
     *
     * @param year
     *            the year
     * @param month
     *            the month
     * @param dayOfMonth
     *            the day of month
     * @param hour
     *            the hour
     * @param minute
     *            the minute
     * @param second
     *            the second
     * @return the local date time of
     */
    public LocalDateTime getLocalDateTimeOf(int year, int month, int dayOfMonth, int hour, int minute, int second);

    /**
     * Gets the annee courante.
     *
     * @return the annee courante
     */
    public int getAnneeCourante();

    /**
     * Compare egalite dates.
     *
     * @param first
     *            the first
     * @param second
     *            the second
     * @return true, if successful
     */
    public boolean compareEgaliteDates(LocalDateTime first, LocalDateTime second);

    /**
     * @param anneeCreationDossier
     * @return
     */
    public LocalDateTime getDebutAnnee(Integer anneeCreationDossier);

    /**
     * @param anneeCreationDossier
     * @return
     */
    public LocalDateTime getFinAnnee(Integer anneeCreationDossier);

    /**
     * Now.
     *
     * @return the local date time
     */
    public LocalDateTime now();
}

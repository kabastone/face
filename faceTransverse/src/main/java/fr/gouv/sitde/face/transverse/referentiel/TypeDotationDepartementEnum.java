/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import fr.gouv.sitde.face.transverse.util.MessageUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Enum des types de dotation de département.
 *
 * @author a453029
 *
 */
public enum TypeDotationDepartementEnum {

    /** Annuelle. */
    ANNUELLE("Annuelle", "typeDotationDepartement.annuelle"),

    /** Exceptionnelle. */
    EXCEPTIONNELLE("Exceptionnelle", "typeDotationDepartement.exceptionnelle"),

    /** Renoncement. */
    RENONCEMENT("Renoncement", "typeDotationDepartement.renoncement"),
	
	/** The perte de dotation. */
	PERTE_DE_DOTATION("Perte_de_dotation", "typeDotationDepartement.perte_de_dotation");

    /**
     * The cleLibelle.
     */
    private final String cleLibelle;

    /**
     * The code.
     */
    private final String code;

    /**
     * Constructeur privé.
     *
     * @param code       the code
     * @param cleLibelle the cle libelle
     */
    TypeDotationDepartementEnum(final String code, final String cleLibelle) {
        this.code = code;
        this.cleLibelle = cleLibelle;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Texte courrier of code.
     *
     * @return the string
     */
    public String getLibelle() {
        return MessageUtils.getLibelleReferentiel(this.cleLibelle);
    }

    /**
     * Renvoie l'enum correspondant au code passé en parametre.<br/>
     * remarque : la recherche du code est insensible à la casse.
     *
     * @param code le code voulu
     * @return l'enum correspondante ou null
     */
    public static TypeDotationDepartementEnum rechercherEnumParCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        for (TypeDotationDepartementEnum typeDotationDepartementEnum : TypeDotationDepartementEnum.values()) {
            if (code.equalsIgnoreCase(typeDotationDepartementEnum.getCode())) {
                return typeDotationDepartementEnum;
            }
        }
        return null;
    }
}

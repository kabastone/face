/**
 *
 */
package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.gouv.sitde.face.transverse.referentiel.VersionAlgorithmeEnum;

/**
 * The Class Reglementaire.
 *
 * @author A687370
 */
@Entity
@Table(name = "r_reglementaire")
public class Reglementaire implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6449671484788957090L;

    /** Clé primaire de la table. */
    private Integer anneeMiseEnApplication;

    /** Numéro et LocalDateTime de la loi organique. */
    private String loiOrganique;

    /** Numéro et LocalDateTime de la loi organique modifiée. */
    private String loiOrganiqueModifiee;

    /** Code général des collectivités territoriales article 1. */
    private String codeColTerritorialesArticle1;

    /** Code général des collectivités territoriales article 2. */
    private String codeColTerritorialesArticle2;

    /** Numéro et LocalDateTime de la loi de finances. */
    private String loiFinances;

    /** Numéro et LocalDateTime du décret portant répartition des crédits et découverts autorisés par la loi de finances. */
    private String decretCreditsDecouvertsAutorises;

    /** Numéro et LocalDateTime du décret relatif aux aides pour l'électrification rurale. */
    private String decretAidesElectrificationRurale;

    /** Numéro et LocalDateTime du décret modificatif relatif aux aides pour l'électrification rurale. */
    private String decretModificatifAidesElectrificationRurale;

    /** Date de l'arrêté relatif à la répartition annuelle des aides. */
    private String arrete;

    /** Date de la circulaire relative à la répartition des aides à l'électrification rurale. */
    private String circulaire;

    /** Catégorie des programmes du FACE. */
    private String categorie;

    /** The version algorithme. */
    private VersionAlgorithmeEnum versionAlgorithme;

    /** Nombre années avant lancement des travaux. */
    private int nbAnDebutTrx;

    /** Nombre années par défaut pour finir les travaux. */
    private int nbAnFinTrxDefaut;

    /** Nombre années pour finir les travaux en cas de prolongation. */
    private int nbAnFinTrxMax;

    /** Taux d'aide maximum pour une demande. */
    private int tauxAideSubvention;

    /** Taux d'aide rapport au montant de subvention pour paiement de type avance. */
    private int tauxAidePaiementAvance;

    /** Taux d'aide maximum rapport au montant subvention pour paiement de type accompte. */
    private int tauxAidePaiementAccompte;

    /** Montant maximum de la subvention necessitant le visa de DCB. */
    private int montantVisa;

    /**
     * Checks if is actif.
     *
     * @return the actif
     */
    @Id
    @Column(name = "reg_annee_mise_en_application", nullable = false)
    public Integer getAnneeMiseEnApplication() {
        return this.anneeMiseEnApplication;
    }

    /**
     * Sets the actif.
     *
     * @param anneeMiseEnApplication
     *            the new annee mise en application
     */
    public void setAnneeMiseEnApplication(Integer anneeMiseEnApplication) {
        this.anneeMiseEnApplication = anneeMiseEnApplication;
    }

    /**
     * Gets the categorie.
     *
     * @return the categorie
     */
    @Column(name = "reg_categorie", nullable = false, length = 150)
    public String getCategorie() {
        return this.categorie;
    }

    /**
     * Sets the categorie.
     *
     * @param categorie
     *            the categorie to set
     */
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Gets the nb an debut trx.
     *
     * @return the nbAnDebutTrx
     */
    @Column(name = "reg_nb_an_debut_trx", nullable = false)
    public int getNbAnDebutTrx() {
        return this.nbAnDebutTrx;
    }

    /**
     * Sets the nb an debut trx.
     *
     * @param nbAnDebutTrx
     *            the nbAnDebutTrx to set
     */
    public void setNbAnDebutTrx(int nbAnDebutTrx) {
        this.nbAnDebutTrx = nbAnDebutTrx;
    }

    /**
     * Gets the nb an fin trx defaut.
     *
     * @return the nbAnFinTrxDefaut
     */
    @Column(name = "reg_nb_an_fin_trx_defaut", nullable = false)
    public int getNbAnFinTrxDefaut() {
        return this.nbAnFinTrxDefaut;
    }

    /**
     * Sets the nb an fin trx defaut.
     *
     * @param nbAnFinTrxDefaut
     *            the nbAnFinTrxDefaut to set
     */
    public void setNbAnFinTrxDefaut(int nbAnFinTrxDefaut) {
        this.nbAnFinTrxDefaut = nbAnFinTrxDefaut;
    }

    /**
     * Gets the nb an fin trx max.
     *
     * @return the nbAnFinTrxMax
     */
    @Column(name = "reg_nb_an_fin_trx_max", nullable = false)
    public int getNbAnFinTrxMax() {
        return this.nbAnFinTrxMax;
    }

    /**
     * Sets the nb an fin trx max.
     *
     * @param nbAnFinTrxMax
     *            the nbAnFinTrxMax to set
     */
    public void setNbAnFinTrxMax(int nbAnFinTrxMax) {
        this.nbAnFinTrxMax = nbAnFinTrxMax;
    }

    /**
     * Gets the taux aide subvention.
     *
     * @return the tauxAideSubvention
     */
    @Column(name = "reg_taux_aide_subvention", nullable = false)
    public int getTauxAideSubvention() {
        return this.tauxAideSubvention;
    }

    /**
     * Sets the taux aide subvention.
     *
     * @param tauxAideSubvention
     *            the tauxAideSubvention to set
     */
    public void setTauxAideSubvention(int tauxAideSubvention) {
        this.tauxAideSubvention = tauxAideSubvention;
    }

    /**
     * Gets the taux aide paiement avance.
     *
     * @return the tauxAidePaiementAvance
     */
    @Column(name = "reg_taux_aide_paiement_avance", nullable = false)
    public int getTauxAidePaiementAvance() {
        return this.tauxAidePaiementAvance;
    }

    /**
     * Sets the taux aide paiement avance.
     *
     * @param tauxAidePaiementAvance
     *            the tauxAidePaiementAvance to set
     */
    public void setTauxAidePaiementAvance(int tauxAidePaiementAvance) {
        this.tauxAidePaiementAvance = tauxAidePaiementAvance;
    }

    /**
     * Gets the taux aide paiement accompte.
     *
     * @return the tauxAidePaiementAccompte
     */
    @Column(name = "reg_taux_aide_paiement_accompte", nullable = false)
    public int getTauxAidePaiementAccompte() {
        return this.tauxAidePaiementAccompte;
    }

    /**
     * Sets the taux aide paiement accompte.
     *
     * @param tauxAidePaiementAccompte
     *            the tauxAidePaiementAccompte to set
     */
    public void setTauxAidePaiementAccompte(int tauxAidePaiementAccompte) {
        this.tauxAidePaiementAccompte = tauxAidePaiementAccompte;
    }

    /**
     * Gets the montant visa.
     *
     * @return the montantVisa
     */
    @Column(name = "reg_montant_visa", nullable = false)
    public int getMontantVisa() {
        return this.montantVisa;
    }

    /**
     * Sets the montant visa.
     *
     * @param montantVisa
     *            the montantVisa to set
     */
    public void setMontantVisa(int montantVisa) {
        this.montantVisa = montantVisa;
    }

    /**
     * Gets the version algorithme.
     *
     * @return the versionAlgorithme
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "reg_version_algorithme", nullable = false, length = 3)
    public VersionAlgorithmeEnum getVersionAlgorithme() {
        return this.versionAlgorithme;
    }

    /**
     * Sets the version algorithme.
     *
     * @param versionAlgorithme
     *            the versionAlgorithme to set
     */
    public void setVersionAlgorithme(VersionAlgorithmeEnum versionAlgorithme) {
        this.versionAlgorithme = versionAlgorithme;
    }

    /**
     * @return the loiOrganique
     */
    @Column(name = "reg_loi_organique", nullable = false)
    public String getLoiOrganique() {
        return this.loiOrganique;
    }

    /**
     * @param loiOrganique
     *            the loiOrganique to set
     */
    public void setLoiOrganique(String loiOrganique) {
        this.loiOrganique = loiOrganique;
    }

    /**
     * @return the loiOrganiqueModifiee
     */
    @Column(name = "reg_loi_organique_modifiee", nullable = false)
    public String getLoiOrganiqueModifiee() {
        return this.loiOrganiqueModifiee;
    }

    /**
     * @param loiOrganiqueModifiee
     *            the loiOrganiqueModifiee to set
     */
    public void setLoiOrganiqueModifiee(String loiOrganiqueModifiee) {
        this.loiOrganiqueModifiee = loiOrganiqueModifiee;
    }

    /**
     * @return the codeColTerritorialesArticle1
     */
    @Column(name = "reg_code_col_territoriales_article_1", nullable = false)
    public String getCodeColTerritorialesArticle1() {
        return this.codeColTerritorialesArticle1;
    }

    /**
     * @param codeColTerritorialesArticle1
     *            the codeColTerritorialesArticle1 to set
     */
    public void setCodeColTerritorialesArticle1(String codeColTerritorialesArticle1) {
        this.codeColTerritorialesArticle1 = codeColTerritorialesArticle1;
    }

    /**
     * @return the codeColTerritorialesArticle2
     */
    @Column(name = "reg_code_col_territoriales_article_2", nullable = false)
    public String getCodeColTerritorialesArticle2() {
        return this.codeColTerritorialesArticle2;
    }

    /**
     * @param codeColTerritorialesArticle2
     *            the codeColTerritorialesArticle2 to set
     */
    public void setCodeColTerritorialesArticle2(String codeColTerritorialesArticle2) {
        this.codeColTerritorialesArticle2 = codeColTerritorialesArticle2;
    }

    /**
     * @return the loiFinances
     */
    @Column(name = "reg_loi_finances", nullable = false)
    public String getLoiFinances() {
        return this.loiFinances;
    }

    /**
     * @param loiFinances
     *            the loiFinances to set
     */
    public void setLoiFinances(String loiFinances) {
        this.loiFinances = loiFinances;
    }

    /**
     * @return the decretCreditsDecouvertsAutorises
     */
    @Column(name = "reg_decret_credits_decouverts_autorises", nullable = false)
    public String getDecretCreditsDecouvertsAutorises() {
        return this.decretCreditsDecouvertsAutorises;
    }

    /**
     * @param decretCreditsDecouvertsAutorises
     *            the decretCreditsDecouvertsAutorises to set
     */
    public void setDecretCreditsDecouvertsAutorises(String decretCreditsDecouvertsAutorises) {
        this.decretCreditsDecouvertsAutorises = decretCreditsDecouvertsAutorises;
    }

    /**
     * @return the decretAidesElectrificationRurale
     */
    @Column(name = "reg_decret_aides_electrification_rurale", nullable = false)
    public String getDecretAidesElectrificationRurale() {
        return this.decretAidesElectrificationRurale;
    }

    /**
     * @param decretAidesElectrificationRurale
     *            the decretAidesElectrificationRurale to set
     */
    public void setDecretAidesElectrificationRurale(String decretAidesElectrificationRurale) {
        this.decretAidesElectrificationRurale = decretAidesElectrificationRurale;
    }

    /**
     * @return the decretModificatifAidesElectrificationRurale
     */
    @Column(name = "reg_decret_modificatif_aides_electrification_rurale", nullable = false)
    public String getDecretModificatifAidesElectrificationRurale() {
        return this.decretModificatifAidesElectrificationRurale;
    }

    /**
     * @param decretModificatifAidesElectrificationRurale
     *            the decretModificatifAidesElectrificationRurale to set
     */
    public void setDecretModificatifAidesElectrificationRurale(String decretModificatifAidesElectrificationRurale) {
        this.decretModificatifAidesElectrificationRurale = decretModificatifAidesElectrificationRurale;
    }

    /**
     * @return the arrete
     */
    @Column(name = "reg_arrete", nullable = false)
    public String getArrete() {
        return this.arrete;
    }

    /**
     * @param arrete
     *            the arrete to set
     */
    public void setArrete(String arrete) {
        this.arrete = arrete;
    }

    /**
     * @return the circulaire
     */
    @Column(name = "reg_circulaire", nullable = false)
    public String getCirculaire() {
        return this.circulaire;
    }

    /**
     * @param circulaire
     *            the circulaire to set
     */
    public void setCirculaire(String circulaire) {
        this.circulaire = circulaire;
    }

}

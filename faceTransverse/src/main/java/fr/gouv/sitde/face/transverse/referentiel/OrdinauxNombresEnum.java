/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * @author A754839
 *
 */
public enum OrdinauxNombresEnum {

    UN(1, "ordinaux.un"),

    DEUX(2, "ordinaux.deux"),

    TROIS(3, "ordinaux.trois"),

    QUATRE(4, "ordinaux.quatre"),

    CINQ(5, "ordinaux.cinq"),

    SIX(6, "ordinaux.six"),

    SEPT(7, "ordinaux.sept"),

    HUIT(8, "ordinaux.huit"),

    NEUF(9, "ordinaux.neuf"),

    DIX(10, "ordinaux.dix"),

    ONZE(11, "ordinaux.onze"),

    DOUZE(12, "ordinaux.douze"),

    TREIZE(13, "ordinaux.treize"),

    QUATORZE(14, "ordinaux.quatorze"),

    QUINZE(15, "ordinaux.quinze"),

    SEIZE(16, "ordinaux.seize"),

    DIXSEPT(17, "ordinaux.dixsept"),

    DIXHUIT(18, "ordinaux.dixhuit"),

    DIXNEUF(19, "ordinaux.dixneuf"),

    VINGT(20, "ordinaux.vingt");

    /** The cleLibelle. */
    private final String cleLibelle;

    /** The code. */
    private final Integer code;

    /**
     * Constructeur privé.
     *
     * @param code
     *            the code
     * @param cleLibelle
     *            the cle libelle
     */
    OrdinauxNombresEnum(final Integer code, final String cleLibelle) {
        this.code = code;
        this.cleLibelle = cleLibelle;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public Integer getCode() {
        return this.code;
    }

    /**
     * Texte courrier of code.
     *
     * @param code
     *            the code
     * @return the string
     */
    public String getLibelle() {
        return MessageUtils.getLibelleReferentiel(this.cleLibelle);
    }

    /**
     * Renvoie l'enum correspondant au code passé en parametre.<br/>
     * remarque : la recherche du code est insensible à la casse.
     *
     * @param code
     *            le code voulu
     * @return l'enum correspondante ou null
     */
    public static OrdinauxNombresEnum rechercherEnumParCode(final Integer code) {
        if (code == null) {
            return null;
        }
        for (OrdinauxNombresEnum ordinauxNombresEnum : OrdinauxNombresEnum.values()) {
            if (code.equals(ordinauxNombresEnum.getCode())) {
                return ordinauxNombresEnum;
            }
        }
        return null;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum EtatDemandeSubventionEnum.
 *
 * @author a453029
 */
public enum EtatDemandeSubventionEnum {

    /** The demandee. */
    DEMANDEE,

    /** The qualifiee. */
    QUALIFIEE,

    /** The accordee. */
    ACCORDEE,

    /** The controlee. */
    CONTROLEE,

    /** The en attente transfert manuel. */
    EN_ATTENTE_TRANSFERT_MAN,

    /** The en attente transfert. */
    EN_ATTENTE_TRANSFERT,

    /** The en cours transfert. */
    EN_COURS_TRANSFERT,

    /** The transferee. */
    TRANSFEREE,

    /** The approuvee. */
    APPROUVEE,

    /** The incoherence chorus. */
    INCOHERENCE_CHORUS,

    /** The anomalie detectee. */
    ANOMALIE_DETECTEE,

    /** The anomalie signalee. */
    ANOMALIE_SIGNALEE,

    /** The corrigee. */
    CORRIGEE,

    /** The refusee. */
    REFUSEE,

    /** The attribuee. */
    ATTRIBUEE,

    /** The cloturee. */
    CLOTUREE,

    /** The cloturee. */
    REJET_TAUX;
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum TemplateDocumentEnum.
 *
 * @author a453029
 */
public enum TemplateDocumentEnum {

    /** Le template des demandes de subvention. */
    DOC_DEMANDE_SUBVENTION("docDemandeSubvention"),

    /** Le template des demandes de paiement. */
    DOC_DEMANDE_PAIEMENT("docDemandePaiement"),

    /** Le template des annexes dotation par département. */
    DOC_ANNEXE_DOTATION("docAnnexeDotation"),

    /** Le template des certificats de paiement. */
    DOC_CERTIFICAT_PAIEMENT("docCertificatPaiementAides");

    /** Nom du template du document. */
    private final String nomTemplate;

    /**
     * Constructeur privé.
     *
     * @param nomTemplate
     *            the nom template
     */
    TemplateDocumentEnum(final String nomTemplate) {
        this.nomTemplate = nomTemplate;
    }

    /**
     * Renvoie le nom du template.
     *
     * @return Le nom du template.
     */
    public String getNomTemplate() {
        return this.nomTemplate;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Enum TypeDocumentEnum.
 *
 * @author a453029
 */
public enum TypeDocumentEnum {

    /** The etat previsionnel pdf. */
    ETAT_PREVISIONNEL_PDF(101, "ETAT_PREVISIONNEL_PDF"),

    /** The etat previsionnel tableur. */
    ETAT_PREVISIONNEL_TABLEUR(102, "ETAT_PREVISIONNEL_TABLEUR"),

    /** The doc complementaire subvention. */
    DOC_COMPLEMENTAIRE_SUBVENTION(103, "DOC_COMPLEMENTAIRE_SUBVENTION"),

    /** The decision attributive subv (document signé). */
    DECISION_ATTRIBUTIVE_SUBV(104, "DECISION_ATTRIBUTIVE_SUBV"),

    /** The fiche siret. */
    FICHE_SIRET(105, "FICHE_SIRET"),

    /** The demande prolongation. */
    DEMANDE_PROLONGATION(201, "DEMANDE_PROLONGATION"),

    /** The etat marches passes pdf. */
    ETAT_MARCHES_PASSES_PDF(301, "ETAT_MARCHES_PASSES_PDF"),

    /** The etat marches passes tableur. */
    ETAT_MARCHES_PASSES_TABLEUR(302, "ETAT_MARCHES_PASSES_TABLEUR"),

    /** The etat realisation travaux pdf. */
    ETAT_REALISATION_TRAVAUX_PDF(303, "ETAT_REALISATION_TRAVAUX_PDF"),

    /** The etat realisation travaux tableur. */
    ETAT_REALISATION_TRAVAUX_TABLEUR(304, "ETAT_REALISATION_TRAVAUX_TABLEUR"),

    /** The etat achevement tech pdf. */
    ETAT_ACHEVEMENT_TECH_PDF(305, "ETAT_ACHEVEMENT_TECH_PDF"),

    /** The etat achevement technique tableur. */
    ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR(306, "ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR"),

    /** The etat achevement fina pdf. */
    ETAT_ACHEVEMENT_FINA_PDF(307, "ETAT_ACHEVEMENT_FINA_PDF"),

    /** The etat achevement financier tableur. */
    ETAT_ACHEVEMENT_FINANCIER_TABLEUR(308, "ETAT_ACHEVEMENT_FINANCIER_TABLEUR"),

    /** The doc complementaire paiement. */
    DOC_COMPLEMENTAIRE_PAIEMENT(309, "DOC_COMPLEMENTAIRE_PAIEMENT"),

    /** The decision attributive paie (document signé). */
    DECISION_ATTRIBUTIVE_PAIE(310, "DECISION_ATTRIBUTIVE_PAIE"),

    /** The certificat paiement pdf. */
    CERTIFICAT_PAIEMENT_PDF(311, "CERTIFICAT_PAIEMENT_PDF"),

    /** The courrier repartition. */
    COURRIER_REPARTITION(401, "COURRIER_REPARTITION"),

    /** The courrier dotation. */
    COURRIER_DOTATION(402, "COURRIER_DOTATION"),

    /** The renoncement montant dotation. */
    RENONCEMENT_MONTANT_DOTATION(501, "RENONCEMENT_MONTANT_DOTATION"),

    /** The modele etat previsionnel. */
    MODELE_ETAT_PREVISIONNEL(601, "MODELE_ETAT_PREVISIONNEL"),

    /** The modele etat marches passes. */
    MODELE_ETAT_MARCHES_PASSES(602, "MODELE_ETAT_MARCHES_PASSES"),

    /** The modele etat realisation. */
    MODELE_ETAT_REALISATION(603, "MODELE_ETAT_REALISATION"),

    /** The modele etat achevement financier. */
    MODELE_ETAT_ACHEVEMENT_FINANCIER(604, "MODELE_ETAT_ACHEVEMENT_FINANCIER"),

    /** The modele etat achevement technique ap ae. */
    MODELE_ETAT_ACHEVEMENT_TECHNIQUE_AP_AE(605, "MODELE_ETAT_ACHEVEMENT_TECHNIQUE_AP_AE"),

    /** The modele etat achevement technique ce. */
    MODELE_ETAT_ACHEVEMENT_TECHNIQUE_CE(606, "MODELE_ETAT_ACHEVEMENT_TECHNIQUE_CE"),

    /** The modele etat achevement technique ss sf. */
    MODELE_ETAT_ACHEVEMENT_TECHNIQUE_SS_SF(607, "MODELE_ETAT_ACHEVEMENT_TECHNIQUE_SS_SF"),

    /** The modele etat achevement technique enr. */
    MODELE_ETAT_ACHEVEMENT_TECHNIQUE_ENR(608, "MODELE_ETAT_ACHEVEMENT_TECHNIQUE_ENR"),

    /** The modele etat achevement technique mde. */
    MODELE_ETAT_ACHEVEMENT_TECHNIQUE_MDE(609, "MODELE_ETAT_ACHEVEMENT_TECHNIQUE_MDE");

    /** The id codifie. */
    private final Integer idCodifie;

    /** The code. */
    private final String code;

    /**
     * Instantiates a new famille document enum.
     *
     * @param idCodifie
     *            the id codifie
     * @param code
     *            the code
     */
    TypeDocumentEnum(Integer idCodifie, String code) {
        this.idCodifie = idCodifie;
        this.code = code;
    }

    /**
     * Gets the id codifie.
     *
     * @return the id codifie
     */
    public Integer getIdCodifie() {
        return this.idCodifie;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Renvoie l'enum correspondant à l'idCodifie passé en paramètre.
     *
     * @param idCodifie
     *            the id codifie
     * @return l'enum correspondante ou null
     */
    public static TypeDocumentEnum rechercherEnumParIdCodifie(final Integer idCodifie) {

        for (TypeDocumentEnum typeDocEnum : TypeDocumentEnum.values()) {
            if (typeDocEnum.getIdCodifie().equals(idCodifie)) {
                return typeDocEnum;
            }
        }
        return null;
    }

    /**
     * Renvoie l'enum correspondant au code passé en paramètre.
     *
     * @param code
     *            the code
     * @return the type document enum
     */
    public static TypeDocumentEnum rechercherEnumParCode(final String code) {

        for (TypeDocumentEnum typeDocEnum : TypeDocumentEnum.values()) {
            if (typeDocEnum.getCode().equals(code)) {
                return typeDocEnum;
            }
        }
        return null;
    }

    /**
     * Renvoie le familleDocumentEnum correspondant au typeDocumentEnum passé en paramètre.
     *
     * @param typeDoc
     *            the type doc
     * @return the famille from type doc
     */
    public static FamilleDocumentEnum getFamilleFromTypeDoc(TypeDocumentEnum typeDoc) {

        FamilleDocumentEnum familleDocEnum = null;

        if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION)) {
            familleDocEnum = FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION;

        } else if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_DEMANDE_PROLONGATION)) {
            familleDocEnum = FamilleDocumentEnum.DOC_DEMANDE_PROLONGATION;

        } else if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_DEMANDE_PAIEMENT)) {
            familleDocEnum = FamilleDocumentEnum.DOC_DEMANDE_PAIEMENT;

        } else if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_COURRIER_ANNUEL_DEPARTEMENT)) {
            familleDocEnum = FamilleDocumentEnum.DOC_COURRIER_ANNUEL_DEPARTEMENT;

        } else if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_LIGNE_DOTATION_DEPARTEMENT)) {
            familleDocEnum = FamilleDocumentEnum.DOC_LIGNE_DOTATION_DEPARTEMENT;

        } else if (isIdCodifieFamilleDoc(typeDoc.getIdCodifie(), FamilleDocumentEnum.DOC_MODELE)) {
            familleDocEnum = FamilleDocumentEnum.DOC_MODELE;

        }

        return familleDocEnum;
    }

    /**
     * Checks if is id codifie famille doc.
     *
     * @param idCodifieTypeDoc
     *            the id codifie type doc
     * @param familleDocumentEnum
     *            the famille document enum
     * @return true, if is id codifie famille doc
     */
    private static boolean isIdCodifieFamilleDoc(Integer idCodifieTypeDoc, FamilleDocumentEnum familleDocumentEnum) {

        return (idCodifieTypeDoc >= (familleDocumentEnum.getIdCodifie() + 1))
                && (idCodifieTypeDoc <= (familleDocumentEnum.getIdCodifie() + ConstantesFace.MAX_TYPES_DOCUMENT_PAR_FAMILLE));
    }

    /**
     * Renvoie la liste des types de documents associés à la famille de documents passée en paramètre.
     *
     * @param familleDocumentEnum
     *            the famille document enum
     * @return the liste documents par famille
     */
    public static List<TypeDocumentEnum> getListeTypeDocumentsPourFamille(FamilleDocumentEnum familleDocumentEnum) {

        // init.
        List<TypeDocumentEnum> listeTypesDocuments = new ArrayList<>();

        // si famille vide
        if (familleDocumentEnum == null) {
            return listeTypesDocuments;
        }

        // alimentation de la liste de type de documents
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.values()) {
            // si la famille correspond, on ajoute à la liste
            if (familleDocumentEnum.equals(getFamilleFromTypeDoc(typeDocument))) {
                listeTypesDocuments.add(typeDocument);
            }
        }

        // retour
        return listeTypesDocuments;
    }

}

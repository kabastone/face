/**
 *
 */
package fr.gouv.sitde.face.transverse.fichier;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * The Class FichierTransfert.
 *
 * @author a453029
 */
public class FichierTransfert implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -319296398852546156L;

    /** The nom fichier. */
    private String nomFichier;

    /** The content type theorique. */
    private String contentTypeTheorique;

    /** The bytes. */
    private byte[] bytes;

    /** The type document. */
    private TypeDocumentEnum typeDocument;

    /**
     * Gets the nom fichier.
     *
     * @return the nom fichier
     */
    public String getNomFichier() {
        return this.nomFichier;
    }

    /**
     * Sets the nom fichier.
     *
     * @param nomFichier
     *            the new nom fichier
     */
    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    /**
     * Gets the content type theorique.
     *
     * @return the contentTypeTheorique
     */
    public String getContentTypeTheorique() {
        return this.contentTypeTheorique;
    }

    /**
     * Sets the content type theorique.
     *
     * @param contentTypeTheorique
     *            the contentTypeTheorique to set
     */
    public void setContentTypeTheorique(String contentTypeTheorique) {
        this.contentTypeTheorique = contentTypeTheorique;
    }

    /**
     * Gets the bytes.
     *
     * @return the bytes
     */
    public byte[] getBytes() {
        return this.bytes;
    }

    /**
     * Sets the bytes.
     *
     * @param bytes
     *            the bytes to set
     */
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    /**
     * Gets the type document.
     *
     * @return the typeDocument
     */
    public TypeDocumentEnum getTypeDocument() {
        return this.typeDocument;
    }

    /**
     * Sets the type document.
     *
     * @param typeDocument the typeDocument to set
     */
    public void setTypeDocument(TypeDocumentEnum typeDocument) {
        this.typeDocument = typeDocument;
    }
}

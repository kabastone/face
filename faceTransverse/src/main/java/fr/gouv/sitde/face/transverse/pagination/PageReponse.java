/**
 *
 */
package fr.gouv.sitde.face.transverse.pagination;

import java.io.Serializable;
import java.util.List;

/**
 * Page de résultats d'une pagination.
 *
 * @author Atos
 *
 * @param <T>
 *            Type parametré de la liste de résultats de la page reponse.
 */
public class PageReponse<T extends Serializable> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** Liste de résultats. */
    private List<T> listeResultats;

    /** nb total de résultats. */
    private long nbTotalResultats;

    /**
     * Instantiates a new page reponse.
     *
     * @param listeResultats
     *            the liste resultats
     * @param nbTotalResultats
     *            the nb total resultats
     */
    public PageReponse(final List<T> listeResultats, final long nbTotalResultats) {
        super();
        this.listeResultats = listeResultats;
        this.nbTotalResultats = nbTotalResultats;
    }

    /**
     * Constructeur non-paramétré.
     */
    public PageReponse() {
        // Constructeur non-parammétré
    }

    /**
     * @return the listeResultats
     */
    public List<T> getListeResultats() {
        return this.listeResultats;
    }

    /**
     * @param listeResultats
     *            the listeResultats to set
     */
    public void setListeResultats(final List<T> listeResultats) {
        this.listeResultats = listeResultats;
    }

    /**
     * @return the nbTotalResultats
     */
    public long getNbTotalResultats() {
        return this.nbTotalResultats;
    }

    /**
     * @param nbTotalResultats
     *            the nbTotalResultats to set
     */
    public void setNbTotalResultats(final long nbTotalResultats) {
        this.nbTotalResultats = nbTotalResultats;
    }

}

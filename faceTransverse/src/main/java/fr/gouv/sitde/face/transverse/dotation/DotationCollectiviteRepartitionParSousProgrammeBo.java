package fr.gouv.sitde.face.transverse.dotation;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class DotationCollectiviteParSousProgrammeBo.
 */
public class DotationCollectiviteRepartitionParSousProgrammeBo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8712789892305481471L;

    /** The id. */
    private Long id;

    /** The id dotation departement. */
    private Long idDotationDepartement;

    /** The montant. */
    private BigDecimal montant;

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the idDotationDepartement
     */
    public Long getIdDotationDepartement() {
        return this.idDotationDepartement;
    }

    /**
     * @param idDotationDepartement
     *            the idDotationDepartement to set
     */
    public void setIdDotationDepartement(Long idDotationDepartement) {
        this.idDotationDepartement = idDotationDepartement;
    }

    /**
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }
}

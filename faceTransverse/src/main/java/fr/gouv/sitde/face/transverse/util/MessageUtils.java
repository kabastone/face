package fr.gouv.sitde.face.transverse.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;

/**
 * Classe de methodes utilitaires pour les messages.
 *
 * @author Atos
 */
public final class MessageUtils {

    private static final DecimalFormat df = new DecimalFormat("#,##0.00");

    /**
     * Constructeur privé.
     */
    private MessageUtils() {
    }

    /**
     * Renvoie le message correspondant à l'exception métier en paramètre.
     *
     * (Remarque : le message original d'une exception metier est la clé d'une donnée dans un fichier properties)
     *
     * @param rgException
     *            rgException
     * @return le message voulu
     */
    public static String getMessageMetier(final RegleGestionException rgException) {

        if ((rgException == null) || (rgException.getMessage() == null)) {
            return null;
        }

        return getMsgValidationMetier(rgException.getMessage(), rgException.getParams());

    }

    /**
     * Format des montants .
     *
     * @param montant
     *            the montant
     * @return the string
     */
    public static String formatMontant(BigDecimal montant) {
        return MessageUtils.df.format(montant);
    }

    /**
     * Renvoie les messages multiples d'une RegleGestionException.
     *
     *
     * @param rgException
     *            rgException
     * @return la liste de messages correspondant aux messages multiples de l'exception.
     */
    public static List<String> getListeMessagesMetier(final RegleGestionException rgException) {

        if ((rgException == null)) {
            return new ArrayList<>(0);
        }

        List<String> listeMessages = new ArrayList<>(rgException.getErreursMultiples().size());

        for (MessageProperty msgProperty : rgException.getErreursMultiples()) {
            listeMessages.add(getMsgValidationMetier(msgProperty.getCleMessage(), msgProperty.getParams()));
        }

        return listeMessages;
    }

    /**
     * Renvoie une valeur du fichier properties de messages de validation metier pour le code donné (et les arguments si présents). <br/>
     * Si le code n'est pas trouvé en tant que clé dans le fichier properties, le code est renvoyé.
     *
     *
     * @param code
     *            code
     * @param args
     *            args
     * @return msgMetier
     */
    public static String getMsgValidationMetier(final String code, final String... args) {

        ResourceBundle bundleMsgMetier = ResourceBundle.getBundle(ConstantesFace.BASE_NAME_BUNDLE_MSG_VALIDATION_METIER, Locale.FRANCE);
        return getMessage(bundleMsgMetier, code, args);
    }

    /**
     * Renvoie le libellé de référentiel correspondant aux paramètres. <br/>
     * Si le code n'est pas trouvé en tant que clé dans le fichier properties, le code est renvoyé.
     *
     * @param code
     *            the code
     * @return the libelle referentiel
     */
    public static String getLibelleReferentiel(final String code, final String... args) {
        ResourceBundle bundleLibelleReferentiel = ResourceBundle.getBundle(ConstantesFace.BASE_NAME_BUNDLE_LIBELLE_REFERENTIEL, Locale.FRANCE);
        return getMessage(bundleLibelleReferentiel, code, args);
    }

    /**
     * Renvoie la valeur du fichier properties correspondant aux paramètres.<br/>
     * Si le code n'est pas trouvé en tant que clé dans le fichier properties, le code est renvoyé.
     *
     * @param resourceBundle
     *            the resource bundle
     * @param code
     *            the code
     * @param args
     *            the args
     * @return the message
     */
    private static String getMessage(ResourceBundle resourceBundle, final String code, final String... args) {

        Object[] parametres = null;

        if ((args != null) && (args.length > 0)) {
            parametres = args.clone();
        }

        if (resourceBundle.containsKey(code)) {
            final String valeurBrute = resourceBundle.getString(code);
            // S'il y a des paramètres, on formate le message pour avoir le libellé d'erreur final.
            // Sinon, le libellé est formaté uniquement pour échapper les doubles quotes.
            return MessageFormat.format(valeurBrute, parametres);
        } else {
            // Le code n'a pas été trouvé dans le fichier property, on renvoie le code tel quel.
            return code;
        }
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.queryobject;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.transverse.pagination.PageDemande;

/**
 * The Class CritereRechercheDossierPourDemandeSubventionQo.
 *
 * @author a453029
 */
public class CritereRechercheDossierPourDemandeSubventionQo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5199701789539372L;

    /** The num dossier. */
    private String numDossier;

    /** The annee creation dossier. */
    private Integer anneeProgrammationDossier;

    /** The id sous programme. */
    private Integer idSousProgramme;

    /** The code etat demande. */
    private String codeEtatDemande;

    /** The id collectivite. */
    private Long idCollectivite;

    /** The id departement. */
    private Integer idDepartement;

    /** The page demande. */
    private PageDemande pageDemande;

    /**
     * Gets the num dossier.
     *
     * @return the numDossier
     */
    public String getNumDossier() {
        return this.numDossier;
    }

    /**
     * Sets the num dossier.
     *
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * Gets the annee programmation dossier.
     *
     * @return the anneeProgrammationDossier
     */
    public Integer getAnneeProgrammationDossier() {
        return this.anneeProgrammationDossier;
    }

    /**
     * Sets the annee programmation dossier.
     *
     * @param anneeProgrammationDossier
     *            the anneeProgrammationDossier to set
     */
    public void setAnneeProgrammationDossier(Integer anneeProgrammationDossier) {
        this.anneeProgrammationDossier = anneeProgrammationDossier;
    }

    /**
     * Gets the id sous programme.
     *
     * @return the idSousProgramme
     */
    public Integer getIdSousProgramme() {
        return this.idSousProgramme;
    }

    /**
     * Sets the id sous programme.
     *
     * @param idSousProgramme
     *            the idSousProgramme to set
     */
    public void setIdSousProgramme(Integer idSousProgramme) {
        this.idSousProgramme = idSousProgramme;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

    /**
     * Gets the id departement.
     *
     * @return the idDepartement
     */
    public Integer getIdDepartement() {
        return this.idDepartement;
    }

    /**
     * Sets the id departement.
     *
     * @param idDepartement
     *            the idDepartement to set
     */
    public void setIdDepartement(Integer idDepartement) {
        this.idDepartement = idDepartement;
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    public boolean isEmpty() {
        boolean idNulles = (this.idCollectivite == null) && (this.idDepartement == null) && (this.idSousProgramme == null);
        return idNulles && (this.anneeProgrammationDossier == null) && (StringUtils.isBlank(this.codeEtatDemande))
                && (StringUtils.isBlank(this.numDossier));
    }

    /**
     * Gets the page demande.
     *
     * @return the pageDemande
     */
    public PageDemande getPageDemande() {
        return this.pageDemande;
    }

    /**
     * Sets the page demande.
     *
     * @param pageDemande
     *            the pageDemande to set
     */
    public void setPageDemande(PageDemande pageDemande) {
        this.pageDemande = pageDemande;
    }

    /**
     * @return the codeEtatDemande
     */
    public String getCodeEtatDemande() {
        return this.codeEtatDemande;
    }

    /**
     * @param codeEtatDemande
     *            the codeEtatDemande to set
     */
    public void setCodeEtatDemande(String codeEtatDemande) {
        this.codeEtatDemande = codeEtatDemande;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum TemplateEmailEnum.
 */
public enum TemplateEmailEnum {

    /** The email relance. */
    EMAIL_RELANCE("emailRelanceTemplate"),

    /** The email expiration. */
    EMAIL_EXPIRATION("emailExpirationTemplate"),

    /** The email prolongation acceptee. */
    EMAIL_PROLONGATION_ACCEPTEE("emailProlongationAcceptee"),

    /** The email prolongation refusee. */
    EMAIL_PROLONGATION_REFUSEE("emailProlongationRefusee"),

    /** The email col consommation. */
    EMAIL_COL_CONSOMMATION("emailCollectiviteConsommation"),

    /** The email contact. */
    EMAIL_CONTACT("emailContact"),

    /**  The email attibution subvention. */
    EMAIL_DOS_ATTRIBUTION_SUBVENTION("emailAttributionSubvention"),

    /** The sub rejet taux. */
    EMAIL_SUB_REJET_TAUX("emailSubventionRejetTaux"),

    /** The cad maj. */
    CAD_MAJ("emailMajCadencement"),
	
	/** The email rappel consommation. */
	EMAIL_RAPPEL_CONSOMMATION("emailRappelConsommation"),
	
	/** The email perte dotation. */
	EMAIL_PERTE_DOTATION("emailPerteDotation"), 
	
	/** The email engagement travaux. */
	EMAIL_ENGAGEMENT_TRAVAUX("emailEngagementTravaux"),
	
	/** The email dos rejet subvention paiement. */
	EMAIL_DOS_REJET_SUBVENTION_PAIEMENT("emailRejetSubventionPaiement");

    /** Nom du template de l'e-mail. */
    private final String nomTemplate;

    /**
     * Constructeur privé.
     *
     * @param nomTemplate
     *            the nom template
     */
    TemplateEmailEnum(final String nomTemplate) {
        this.nomTemplate = nomTemplate;
    }

    /**
     * Renvoie le nom du template.
     *
     * @return Le nom du template.
     */
    public String getNomTemplate() {
        return this.nomTemplate;
    }
}

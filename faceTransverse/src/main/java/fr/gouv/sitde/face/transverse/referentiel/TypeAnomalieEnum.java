/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import org.apache.commons.lang3.StringUtils;

/**
 * @author A754839
 *
 */
public enum TypeAnomalieEnum {

    /** The anomalie chorus subvention. */
    ANOMALIE_CHORUS_SUBVENTION("ANO_CHO_SUB"),

    /** The anomalie chorus paiement. */
    ANOMALIE_CHORUS_PAIEMENT("ANO_CHO_PAI"),

    /** The anomalie subvention autre. */
    ANOMALIE_SUBVENTION_AUTRE("ANO_SUB_0000"),

    /** The anomalie paiement autre. */
    ANOMALIE_PAIEMENT_AUTRE("ANO_PAI_0000");

    /** The code. */
    private final String code;

    /**
     * Instantiates a new type anomalie enum.
     *
     * @param code
     *            the code
     */
    TypeAnomalieEnum(final String code) {
        this.code = code;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Renvoie l'enum correspondant au code passé en parametre.<br/>
     * remarque : la recherche du code est insensible à la casse.
     *
     * @param code
     *            le code voulu
     * @return l'enum correspondante ou null
     */
    public static TypeAnomalieEnum rechercherEnumParCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        for (TypeAnomalieEnum typeAnomalieEnum : TypeAnomalieEnum.values()) {
            if (code.equalsIgnoreCase(typeAnomalieEnum.getCode())) {
                return typeAnomalieEnum;
            }
        }
        return null;
    }
}

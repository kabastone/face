/**
 *
 */
package fr.gouv.sitde.face.transverse.imports;

/**
 * The Enum SousProgrammeTypeEnum.
 *
 * @author Atos
 */
public enum SousProgrammeTypeEnum {

    /** The renforcement reseaux. */
    RENFORCEMENT_RESEAUX(0, "Renforcement des réseaux", "AE"),

    /** The extension reseaux. */
    EXTENSION_RESEAUX(1, "Extension des réseaux", "AP"),

    /** The enfouissement pose facade. */
    ENFOUISSEMENT_POSE_FACADE(2, "Enfouissement et pose en façade", "CE"),

    /** The securisation. */
    SECURISATION(3, "Sécurisation", "SN");

    /** The description. */
    private final String description;

    /** The abreviation. */
    private final String abreviation;

    /** The index. */
    private final int index;

    /**
     * Instantiates a new sous programme type enum.
     *
     * @param index
     *            the index
     * @param description
     *            the description
     * @param abreviation
     *            the abreviation
     * @param nomColonne
     *            the nom colonne
     */
    SousProgrammeTypeEnum(int index, String description, String abreviation) {
        this.index = index;
        this.description = description;
        this.abreviation = abreviation;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the abreviation.
     *
     * @return the abreviation
     */
    public String getAbreviation() {
        return this.abreviation;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * Rechercher de l'enum par le code d'abreviation.
     *
     * @param abreviationRechercher
     *            the abreviation rechercher
     * @return the sous programme type enum
     */
    public static SousProgrammeTypeEnum rechercherParAbreviation(String abreviationRechercher) {
        for (SousProgrammeTypeEnum sousProgrammeTypeEnum : SousProgrammeTypeEnum.values()) {
            if (sousProgrammeTypeEnum.abreviation.equals(abreviationRechercher)) {
                return sousProgrammeTypeEnum;
            }
        }
        return null;
    }
}

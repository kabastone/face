package fr.gouv.sitde.face.transverse.exceptions.messages;

import java.io.Serializable;

/**
 * Message dont la valeur est définie dans un fichier properties.
 *
 * @author Atos
 *
 */
public class MessageProperty implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5200883899273034009L;

    /**
     * Clé du message.
     */
    private String cleMessage;

    /**
     * Tableau des paramètres du message.
     */
    private String[] params;

    /**
     * Instantiates a new message property.
     */
    public MessageProperty() {
        super();
    }

    /**
     * Instantiates a new message property.
     *
     * @param cleMessage
     *            the cle message
     */
    public MessageProperty(String cleMessage) {
        super();
        this.cleMessage = cleMessage;
    }

    /**
     * Instantiates a new message property.
     *
     * @param cleMessage
     *            the cle message
     * @param params
     *            the params
     */
    public MessageProperty(String cleMessage, String[] params) {
        super();
        this.cleMessage = cleMessage;
        this.params = params == null ? null : params.clone();
    }

    /**
     * @return la clé du message.
     */
    public String getCleMessage() {
        return this.cleMessage;
    }

    /**
     * Sets the cle message.
     *
     * @param cleMessage
     *            the new cle message
     */
    public void setCleMessage(final String cleMessage) {
        this.cleMessage = cleMessage;
    }

    /**
     * <b>Retourne un clone</b> du tableau contenant les paramètres du message.
     *
     * @return clone de la propriété params
     */
    public String[] getParams() {
        return this.params == null ? null : this.params.clone();
    }

    /**
     * Affecte à la propriété <i>params</i> un clone du tableau passé en argument.
     *
     * @param params
     *            the params to set
     */
    public void setParams(final String[] params) {
        this.params = params == null ? null : params.clone();
    }

}

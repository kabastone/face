/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * Enum des types demande paiement.
 *
 * @author Atos
 *
 */
public enum TypeDemandePaiementEnum {

    /** Etat acompte. */
    ACOMPTE("ACOMPTE", "typeDemande.acompte"),

    /** Etat avance. */
    AVANCE("AVANCE", "typeDemande.avance"),

    /** Etat soldé. */
    SOLDE("SOLDE", "typeDemande.solde");

    /** The cleLibelle. */
    private final String cleLibelle;

    /** The code. */
    private final String code;

    /**
     * Constructeur privé.
     *
     * @param code
     *            the code
     * @param cleLibelle
     *            the cle libelle
     */
    TypeDemandePaiementEnum(final String code, final String cleLibelle) {
        this.code = code;
        this.cleLibelle = cleLibelle;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Texte courrier of code.
     *
     * @param code
     *            the code
     * @return the string
     */
    public String getLibelle() {
        return MessageUtils.getLibelleReferentiel(this.cleLibelle);
    }

    /**
     * Renvoie l'enum correspondant au code passé en parametre.<br/>
     * remarque : la recherche du code est insensible à la casse.
     *
     * @param code
     *            le code voulu
     * @return l'enum correspondante ou null
     */
    public static TypeDemandePaiementEnum rechercherEnumParCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        for (TypeDemandePaiementEnum typeDemandePaiementEnum : TypeDemandePaiementEnum.values()) {
            if (code.equalsIgnoreCase(typeDemandePaiementEnum.getCode())) {
                return typeDemandePaiementEnum;
            }
        }
        return null;
    }

}

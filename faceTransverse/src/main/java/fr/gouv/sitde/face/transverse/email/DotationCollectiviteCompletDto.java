/**
 *
 */
package fr.gouv.sitde.face.transverse.email;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DotationCollectiviteCompletDto.
 *
 * @author A754839
 */
public class DotationCollectiviteCompletDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3959915736129992402L;

    /** The Constant NB_ANNEE_MAX. */
    private static final int NB_ANNEE_MAX = 5;

    private static final int NB_SOUS_PROGRAMMES = SousProgrammeNotificationEnum.values().length;

    /** The liste dotation collectivite annee DTO. */
    private List<DotationCollectiviteParSousProgrammeDto> listeDotationCollectiviteParSousProgrammeDTO = new ArrayList<>(
            NB_ANNEE_MAX * NB_SOUS_PROGRAMMES);

    public DotationCollectiviteCompletDto(List<DotationCollectiviteParSousProgrammeDto> listeDotationCollectiviteParSousProgrammeDTO) {
        this.listeDotationCollectiviteParSousProgrammeDTO = listeDotationCollectiviteParSousProgrammeDTO;

    }

    public DotationCollectiviteParSousProgrammeDto getDTOParAnneeEtSousProgramme(Integer annee, SousProgrammeNotificationEnum sprEnum) {
        for (DotationCollectiviteParSousProgrammeDto dto : this.listeDotationCollectiviteParSousProgrammeDTO) {
            if ((dto.getAnnee().equals(annee)) && (dto.getAbreviation().equals(sprEnum))) {
                return dto;
            }
        }
        return new DotationCollectiviteParSousProgrammeDto(annee, sprEnum);
    }

    public DotationCollectiviteParSousProgrammeDto getTotalParSousProgramme(SousProgrammeNotificationEnum sprEnum) {

        BigDecimal dotationRepartie = new BigDecimal("0");
        BigDecimal aideConsommee = new BigDecimal("0");

        for (DotationCollectiviteParSousProgrammeDto dto : this.listeDotationCollectiviteParSousProgrammeDTO) {
            if (dto.getAbreviation().equals(sprEnum)) {
                dotationRepartie = dotationRepartie.add(dto.getDotationRepartie());
                aideConsommee = aideConsommee.add(dto.getAideConsommee());
            }
        }
        return new DotationCollectiviteParSousProgrammeDto(dotationRepartie, aideConsommee, null, sprEnum.getAbreviation());

    }
}

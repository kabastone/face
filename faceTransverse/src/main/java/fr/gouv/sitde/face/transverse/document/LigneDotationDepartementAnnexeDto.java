/**
 *
 */
package fr.gouv.sitde.face.transverse.document;

import java.math.BigDecimal;

/**
 * The Class LigneDotationDepartementAnnexeDto.
 *
 * @author A754839
 */
public class LigneDotationDepartementAnnexeDto implements java.io.Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1916047507658035309L;

    /** Le nom du sous-programme lié à cette ligne dotation département. */
    private String nom;

    /** Le montant associé à ce sous-programme. */
    private BigDecimal montant;

    /**
     * Instantiates a new ligne dotation departement annexe DTO.
     *
     * @param nom
     *            the nom
     * @param montant
     *            the montant
     */
    public LigneDotationDepartementAnnexeDto(String nom, BigDecimal montant) {
        this.nom = nom;
        this.montant = montant;
    }

    /**
     * Gets the montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * Sets the montant.
     *
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}

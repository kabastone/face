/**
 *
 */
package fr.gouv.sitde.face.transverse.email;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * The Class EmailContactDto.
 */
public class EmailContactDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3377315607640908980L;

    /** The nom. */
    @NotBlank(message = "{contact.prenomNom.obligatoire}")
    @Size(message = "{contact.prenomNom.tailleMax}", max = 300)
    private String nom;

    /** The email. */
    @NotBlank(message = "{contact.adresseEmail.obligatoire}")
    @Email(message = "{listeDiffusion.email.pattern}")
    private String email;

    @NotBlank(message = "{contact.objet.obligatoire}")
    @Size(message = "{contact.objet.tailleMax}", max = 300)
    private String objet;

    /** The message. */
    @NotBlank(message = "{contact.message.obligatoire}")
    @Size(message = "{contact.message.tailleMax}", max = 5000)
    private String message;

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param email
     *            the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the objet
     */
    public String getObjet() {
        return this.objet;
    }

    /**
     * @param objet
     *            the objet to set
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }
}

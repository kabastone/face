package fr.gouv.sitde.face.transverse.chorus;

/**
 * The Enum WsChorusCodesErreursEnum.
 *
 * @author Atos
 *         <p>
 *         Detaille les codes d'erreurs possibles de Chorus
 */
public enum WsChorusCodesErreursEnum {

    /** Le code erreur ECF_02. */
    ECF_02("ECF_02", "L'identifiant recherche n'existe pas dans le referentiel des tiers CHORUS."),

    /** Le code erreur ECF_03. */
    ECF_03("ECF_03", "Le Code Organisation fourni n'existe pas dans CHORUS."),

    /** Le code erreur ECF_04. */
    ECF_04("ECF_04", "Le Domaine Commercial fourni n'existe pas dans CHORUS."),

    /** Le code erreur ECF_06. */
    ECF_06("ECF_06", "La requête fournit plus de 50 occurrences."),

    /** Le code erreur ECF_08. */
    ECF_08("ECF_08", "Seul l'un ou l'autre des champs entre l'organisation d'achat et le domaine commercial, peut être renseigné."),

    /** Le code erreur ECF_09. */
    ECF_09("ECF_09", "Au moins un des champs entre l'identifiant technique et l'identifiant fonctionnel, doit être renseigné."),

    /** Le code erreur ECF_11. */
    ECF_11("ECF_11", "Le code société n'existe pas dans CHORUS."),

    /** Le code erreur ECF_12. */
    ECF_12("ECF_12", "Le tiers donne existe. En revanche, l'extension fournie pour ce tiers (OA ou OC et/ou Societe) n'existe pas."),

    /** Le code erreur WEBS2_00. */
    WEBS2_00("WEBS2_00", "Le service web de chorus pour la synchronisation de la collectivité n'est pas accessible."),
    /** Le code erreur WEBS2_01. */
    WEBS2_01("WEBS2_01", "Le service web de chorus a retourné plusieurs collectivités avec les paramètres fournis."),

    /** Le code erreur ECT_01. */
    ECT_01("ECT_01", "Le traitement interne du service Web est en erreur."),

    /** Le code erreur ECT_02. */
    ECT_02("ECT_02", "Le format de la requête est non conforme."),

    /** Le code erreur ECT_03. */
    ECT_03("ECT_03", "La base Tiers de correspondance est indisponible."),

    /** Le code erreur ECT_04. */
    ECT_04("ECT_04", "Restriction du « Motif de complétude « * » sur les champs autorisés : au moins les 2 premiers caractères sont présents."),

    ;

    /**
     * Texte associé aux Enum.
     */
    private final String code;

    /**
     * Texte associé aux Enum.
     */
    private final String description;

    /**
     * Constructeur privé.
     *
     * @param code
     *            le code associé aux enum
     * @param description
     *            la description associé aux enum
     */
    WsChorusCodesErreursEnum(final String code, final String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Renvoie la description de l'enum.
     *
     * @return La description
     */
    public String getDescription() {
        return this.description;
    }
}

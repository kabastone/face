/**
 *
 */
package fr.gouv.sitde.face.transverse.document;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;

/**
 * The Class FEN0111AReadingDto.
 */
public class FEN0111AReadingDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4832689394075045933L;

    private DemandeSubvention demandeSubvention;

    private Document ficheSiret;

    private Document etatPrevisionnelPdf;

    public FEN0111AReadingDto(DemandeSubvention demandeSubvention, Document ficheSiret, Document etatPrevisionnelPdf) {
        this.demandeSubvention = demandeSubvention;
        this.ficheSiret = ficheSiret;
        this.etatPrevisionnelPdf = etatPrevisionnelPdf;
    }

    /**
     * @return the demandeSubvention
     */
    public DemandeSubvention getDemandeSubvention() {
        return this.demandeSubvention;
    }

    /**
     * @param demandeSubvention
     *            the demandeSubvention to set
     */
    public void setDemandeSubvention(DemandeSubvention demandeSubvention) {
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * @return the ficheSiret
     */
    public Document getFicheSiret() {
        return this.ficheSiret;
    }

    /**
     * @param ficheSiret
     *            the ficheSiret to set
     */
    public void setFicheSiret(Document ficheSiret) {
        this.ficheSiret = ficheSiret;
    }

    /**
     * @return the etatPrevisionnelPdf
     */
    public Document getEtatPrevisionnelPdf() {
        return this.etatPrevisionnelPdf;
    }

    /**
     * @param etatPrevisionnelPdf
     *            the etatPrevisionnelPdf to set
     */
    public void setEtatPrevisionnelPdf(Document etatPrevisionnelPdf) {
        this.etatPrevisionnelPdf = etatPrevisionnelPdf;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.transverse.email;

import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Enum SousProgrammeAbreviationEnum.
 *
 */
public enum SousProgrammeNotificationEnum {

    /** The renforcement reseaux. */
    RENFORCEMENT_RESEAUX("RR", "spr.rr"),

    /** The extension reseaux. */
    EXTENSION_RESEAUX("ER", "spr.er"),

    /** The enfouissement. */
    ENFOUISSEMENT("EF", "spr.ef"),

    /** The securisation fils nus. */
    SECURISATION_FILS_NUS("SS", "spr.ss"),

    /** The securisation fils faibles section. */
    SECURISATION_FILS_FAIBLES_SECTION("SF", "spr.sf");

    /** The abreviation. */
    private final String abreviation;

    /** The nom. */
    private final String nom;

    /**
     * Instantiates a new sous programme abreviation enum.
     *
     * @param abreviation
     *            the abreviation
     */
    SousProgrammeNotificationEnum(final String abreviation, final String nom) {
        this.abreviation = abreviation;
        this.nom = nom;
    }

    /**
     * Gets the abreviation.
     *
     * @return the abreviation
     */
    public String getAbreviation() {
        return this.abreviation;
    }

    public static SousProgrammeNotificationEnum getEnumFromAbreviation(String abreviation) {
        if (abreviation == null) {
            return null;
        }
        for (SousProgrammeNotificationEnum sousProgrammeNotificationEnum : SousProgrammeNotificationEnum.values()) {
            if (abreviation.equals(sousProgrammeNotificationEnum.getAbreviation())) {
                return sousProgrammeNotificationEnum;
            }
        }
        return null;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return MessageUtils.getLibelleReferentiel(this.nom);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.tableaudebord;

import java.io.Serializable;

/**
 * The Class TableauDeBordSd7Dto.
 *
 * @author a453029
 */
public class TableauDeBordSd7Dto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6598712096136752443L;

    /** The nb acontroler subvention. */
    private long nbAcontrolerSubvention;

    /** The nb acontroler paiement. */
    private long nbAcontrolerPaiement;

    /** The nb atransferer subvention. */
    private long nbAtransfererSubvention;

    /** The nb atransferer paiement. */
    private long nbAtransfererPaiement;

    /**
     * Instantiates a new tableau de bord sd 7 dto.
     */
    public TableauDeBordSd7Dto() {
        // Constructeur non paramétré.
    }

    /**
     * Instantiates a new tableau de bord sd 7 dto.
     *
     * @param nbAcontrolerSubvention
     *            the nb acontroler subvention
     * @param nbAcontrolerPaiement
     *            the nb acontroler paiement
     * @param nbAtransfererSubvention
     *            the nb atransferer subvention
     * @param nbAtransfererPaiement
     *            the nb atransferer paiement
     */
    public TableauDeBordSd7Dto(long nbAcontrolerSubvention, long nbAcontrolerPaiement, long nbAtransfererSubvention, long nbAtransfererPaiement) {
        super();
        this.nbAcontrolerSubvention = nbAcontrolerSubvention;
        this.nbAcontrolerPaiement = nbAcontrolerPaiement;
        this.nbAtransfererSubvention = nbAtransfererSubvention;
        this.nbAtransfererPaiement = nbAtransfererPaiement;
    }

    /**
     * Gets the nb acontroler subvention.
     *
     * @return the nbAcontrolerSubvention
     */
    public long getNbAcontrolerSubvention() {
        return this.nbAcontrolerSubvention;
    }

    /**
     * Sets the nb acontroler subvention.
     *
     * @param nbAcontrolerSubvention
     *            the nbAcontrolerSubvention to set
     */
    public void setNbAcontrolerSubvention(long nbAcontrolerSubvention) {
        this.nbAcontrolerSubvention = nbAcontrolerSubvention;
    }

    /**
     * Gets the nb acontroler paiement.
     *
     * @return the nbAcontrolerPaiement
     */
    public long getNbAcontrolerPaiement() {
        return this.nbAcontrolerPaiement;
    }

    /**
     * Sets the nb acontroler paiement.
     *
     * @param nbAcontrolerPaiement
     *            the nbAcontrolerPaiement to set
     */
    public void setNbAcontrolerPaiement(long nbAcontrolerPaiement) {
        this.nbAcontrolerPaiement = nbAcontrolerPaiement;
    }

    /**
     * Gets the nb atransferer subvention.
     *
     * @return the nbAtransfererSubvention
     */
    public long getNbAtransfererSubvention() {
        return this.nbAtransfererSubvention;
    }

    /**
     * Sets the nb atransferer subvention.
     *
     * @param nbAtransfererSubvention
     *            the nbAtransfererSubvention to set
     */
    public void setNbAtransfererSubvention(long nbAtransfererSubvention) {
        this.nbAtransfererSubvention = nbAtransfererSubvention;
    }

    /**
     * Gets the nb atransferer paiement.
     *
     * @return the nbAtransfererPaiement
     */
    public long getNbAtransfererPaiement() {
        return this.nbAtransfererPaiement;
    }

    /**
     * Sets the nb atransferer paiement.
     *
     * @param nbAtransfererPaiement
     *            the nbAtransfererPaiement to set
     */
    public void setNbAtransfererPaiement(long nbAtransfererPaiement) {
        this.nbAtransfererPaiement = nbAtransfererPaiement;
    }

}

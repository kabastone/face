/**
 *
 */
package fr.gouv.sitde.face.transverse.fichier;

/**
 * The Enum ExtensionFichierEnum.
 *
 * @author Atos
 */
public enum ExtensionFichierEnum {

    /** Fichier de type PDF. */
    PDF("application/pdf", "pdf", "application/pdf"),

    /** Fichier Excel. */
    XLS("application/vnd.ms-excel", "xls", "application/vnd.ms-excel"),

    /** Fichier Word. */
    DOC("application/msword", "doc", "application/msword"),

    /** Fichier Word > 2003. */
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),

    /** Fichier Excel > 2003. */
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),

    /** Fichier de type Writer. */
    ODT("application/vnd.oasis.opendocument.text", "odt", "application/vnd.oasis.opendocument.text"),

    /** Fichier de type Calc. */
    ODS("application/vnd.oasis.opendocument.spreadsheet", "ods", "application/vnd.oasis.opendocument.spreadsheet"),

    /** Fichier de type JPG. */
    JPG(Constantes.CONTENT_TYPE_JPEG, "jpg", Constantes.CONTENT_TYPE_JPEG),

    /** Fichier de type JPEG. */
    JPEG(Constantes.CONTENT_TYPE_JPEG, "jpeg", Constantes.CONTENT_TYPE_JPEG),

    /** Fichier de type PNG. */
    PNG("image/png", "png", "image/png"),

    /** Fichier de type XML. */
    XML(Constantes.CONTENT_TYPE_XML, "xml", Constantes.CONTENT_TYPE_XML);

    /**
     * le content type.
     */
    private String contentType;
    /**
     * Extension.
     */
    private String extension;

    /**
     * Extension.
     */
    private String tikaType;

    /**
     * Instantiates a new ExtensionFichierEnum.
     *
     * @param contentType
     *            le content type
     * @param extension
     *            l'extension du fichier
     * @param tikaType
     *            String contentType retourné par Tyka
     */
    ExtensionFichierEnum(final String contentType, final String extension, final String tikaType) {
        this.contentType = contentType;
        this.extension = extension;
        this.tikaType = tikaType;
    }

    /**
     * Gets the content type.
     *
     * @return the contentType
     */
    public String getContentType() {
        return this.contentType;
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    public String getExtension() {
        return this.extension;
    }

    /**
     * Gets the tika content type.
     *
     * @return the extension
     */
    public String getTikaType() {
        return this.tikaType;
    }

    /**
     * Renvoie l'enum correspondant à l'extension passée en parametre.<br/>
     * remarque : la recherche de l'extension est insensible à la casse.
     *
     * @param extension
     *            l'extension voulue
     * @return l'enum correspondante ou null
     */
    public static ExtensionFichierEnum rechercherEnumParExtension(final String extension) {
        if ((extension == null) || extension.isEmpty()) {
            return null;
        }
        for (ExtensionFichierEnum extensionFichierEnum : ExtensionFichierEnum.values()) {
            if (extension.equalsIgnoreCase(extensionFichierEnum.getExtension())) {
                return extensionFichierEnum;
            }
        }
        return null;
    }

    /**
     * Recherche de l'énumération lié au Content Type passé en paramètre.
     *
     * @param contentType
     *            String ContentType à tester.
     * @return ExtensionFichierEnum lié au contentType
     */
    public static ExtensionFichierEnum rechercherEnumParContentType(final String contentType) {
        if ((contentType == null) || contentType.isEmpty()) {
            return null;
        }

        for (ExtensionFichierEnum extensionFichierEnum : ExtensionFichierEnum.values()) {
            if (contentType.equalsIgnoreCase(extensionFichierEnum.getContentType())) {
                return extensionFichierEnum;
            }
        }
        return null;

    }

    /**
     * Recherche de l'énumération lié au ContenType Tika passé en paramètre.
     *
     * @param tikaType
     *            String ContentType Tika à tester
     * @return ExtensionFichierEnum lié au ContentType Tika
     */
    public static ExtensionFichierEnum rechercherEnumParTikaType(final String tikaType) {
        if ((tikaType == null) || tikaType.isEmpty()) {
            return null;
        }
        for (ExtensionFichierEnum extensionFichierEnum : ExtensionFichierEnum.values()) {
            if (tikaType.equalsIgnoreCase(extensionFichierEnum.getTikaType())) {
                return extensionFichierEnum;
            }
        }
        return null;

    }

    /**
     * The Class Constantes.
     */
    private static final class Constantes {

        public static final String CONTENT_TYPE_XML = "text/xml";

        /** The Constant CONTENT_TYPE_JPEG. */
        public static final String CONTENT_TYPE_JPEG = "image/jpeg";

        /**
         * constructeur privé.
         */
        private Constantes() {
        }
    }
}

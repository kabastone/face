/**
 *
 */
package fr.gouv.sitde.face.transverse.chorus;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Transaction de paiement Payzen.
 *
 * @author Atos
 *
 */
public class CollectiviteChorus implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 9127807146171807764L;

    /** The typeTiers. */
    private String typeTiers;

    /** The groupeDeComptes. */
    private String groupeDeComptes;

    /** The idTechnique. */
    private String idTechnique;

    /** The nom. */
    private String nom;

    /** The codePostal. */
    private String codePostal;

    /** The ville. */
    private String ville;

    /** The numeroRue. */
    private String numeroRue;

    /** The nomRue. */
    private String nomRue;

    /** The complementAdresse. */
    private String complementAdresse;

    /** The codePays. */
    private String codePays;

    /** The dateSynchro. */
    private LocalDateTime dateSynchro;

    /**
     * @return the typeTiers
     */
    public String getTypeTiers() {
        return this.typeTiers;
    }

    /**
     * @param typeTiers
     *            the typeTiers to set
     */
    public void setTypeTiers(String typeTiers) {
        this.typeTiers = typeTiers;
    }

    /**
     * @return the groupeDeComptes
     */
    public String getGroupeDeComptes() {
        return this.groupeDeComptes;
    }

    /**
     * @param groupeDeComptes
     *            the groupeDeComptes to set
     */
    public void setGroupeDeComptes(String groupeDeComptes) {
        this.groupeDeComptes = groupeDeComptes;
    }

    /**
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * @param ville
     *            the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the numeroRue
     */
    public String getNumeroRue() {
        return this.numeroRue;
    }

    /**
     * @param numeroRue
     *            the numeroRue to set
     */
    public void setNumeroRue(String numeroRue) {
        this.numeroRue = numeroRue;
    }

    /**
     * @return the nomRue
     */
    public String getNomRue() {
        return this.nomRue;
    }

    /**
     * @param nomRue
     *            the nomRue to set
     */
    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    /**
     * @return the complementAdresse
     */
    public String getComplementAdresse() {
        return this.complementAdresse;
    }

    /**
     * @param complementAdresse
     *            the complementAdresse to set
     */
    public void setComplementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
    }

    /**
     * @return the codePays
     */
    public String getCodePays() {
        return this.codePays;
    }

    /**
     * @param codePays
     *            the codePays to set
     */
    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    /**
     * @return the dateSynchro
     */
    public LocalDateTime getDateSynchro() {
        return this.dateSynchro;
    }

    /**
     * @param dateSynchro
     *            the dateSynchro to set
     */
    public void setDateSynchro(LocalDateTime dateSynchro) {
        this.dateSynchro = dateSynchro;
    }

}

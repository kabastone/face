/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * Enum des états de collectivité.
 *
 * @author a453029
 *
 */
public enum EtatCollectiviteEnum {

    /** The creee. */
    CREEE,

    /** The fermee. */
    FERMEE;

}

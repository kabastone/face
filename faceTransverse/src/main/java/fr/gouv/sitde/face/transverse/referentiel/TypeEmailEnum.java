/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * Enum des types de dotation de département.
 *
 * @author a453029
 *
 */
public enum TypeEmailEnum {

    /** DOS_RELANCE. */
    DOS_RELANCE,
    /** DOS_EXPIRATION. */
    DOS_EXPIRATION,
    /** DOS_PROLONGATION_ACCEPTEE. */
    DOS_PROLONGATION_ACCEPTEE,
    /** DOS_PROLONGATION_REFUSEE. */
    DOS_PROLONGATION_REFUSEE,
    /** COL_CONSOMMATION. */
    COL_CONSOMMATION,
    /** The contact. */
    CONTACT,
    /** The contact. */
    DOS_ATTRIBUTION_SUBVENTION,
    /** The sub rejet taux. */
    SUB_REJET_TAUX,
    /** The cad maj. */
    CAD_MAJ,
    
    /** The rappel consommation. */
    RAPPEL_CONSOMMATION,
    
    /** The perte dotation. */
    PERTE_DOTATION, 
    
    /** The engagement travaux. */
    ENGAGEMENT_TRAVAUX, 
    
    /** The dos refus subvention. */
    DOS_REJET_SUBVENTION_PAIEMENT;
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum EtatDemandeProlongationEnum.
 *
 * @author a453029
 */
public enum EtatDemandeProlongationEnum {

    /** The demandee. */
    DEMANDEE,

    /** The accordee. */
    ACCORDEE,

    /** The incoherence chorus. */
    INCOHERENCE_CHORUS,

    /** The valideee. */
    VALIDEE,

    /** The refusee. */
    REFUSEE
}

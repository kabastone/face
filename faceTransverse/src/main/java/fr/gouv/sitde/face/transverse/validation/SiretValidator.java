package fr.gouv.sitde.face.transverse.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SiretValidator.
 *
 * @author a453029
 *
 */
public class SiretValidator implements ConstraintValidator<Siret, String> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SiretValidator.class);

    /** The Constant LONGUEUR_SIRET. */
    private static final int LONGUEUR_SIRET = 14;

    /** The Constant NEUF. */
    private static final int NEUF = 9;

    /*
     * (non-Javadoc)
     *
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final Siret constraintAnnotation) {
        // Aucun traitement d'initialisation n'est nécessaire ici.
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String siret, final ConstraintValidatorContext constraintContext) {

        if (StringUtils.isEmpty(siret)) {
            return true;
        }
        if (siret.length() != LONGUEUR_SIRET) {
            // Si le SIRET est différent de 14 caractères, il ne peut pas être correct. Empèche que l'algo suivant valide les SIRET
            return false;
        }
        // invalide si ne contient que des 0
        if (StringUtils.isEmpty(siret.replace("0", ""))) {
            return false;
        }
        // valide si passe l'algorithme de Luhn
        return validerAlgorithmeLuhn(siret);
    }

    /**
     * Valider siret via l'algorithme de Luhn.
     *
     * @param siret
     *            the siret
     * @return true, if successful
     */
    private static boolean validerAlgorithmeLuhn(final String siret) {
        try {
            int sum = 0;
            boolean alternate = false;
            for (int i = siret.length() - 1; i >= 0; i--) {
                int n = Integer.parseInt(siret.substring(i, i + 1));
                if (alternate) {
                    n *= 2;
                    if (n > NEUF) {
                        n = (n % 10) + 1;
                    }
                }
                sum += n;
                alternate = !alternate;
            }
            return (sum % 10) == 0;
        } catch (RuntimeException ex) {
            LOGGER.trace("Erreur durant l'execution de l'algo de Luhn", ex);
            return false;
        }
    }

}

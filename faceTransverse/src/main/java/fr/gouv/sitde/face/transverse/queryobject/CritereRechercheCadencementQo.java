/**
 *
 */
package fr.gouv.sitde.face.transverse.queryobject;

import java.io.Serializable;

/**
 * @author a768251
 *
 */
public class CritereRechercheCadencementQo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7470707020553723707L;

    /** The annee programmation. */
    private Integer anneeProgrammation;

    /** The id collectivite. */
    private Integer idDepartement;

    /** The id departement. */
    private Long idCollectivite;

    /**
     * @return the anneeProgrammation
     */
    public Integer getAnneeProgrammation() {
        return this.anneeProgrammation;
    }

    /**
     * @param anneeProgrammation
     *            the anneeProgrammation to set
     */
    public void setAnneeProgrammation(Integer anneeProgrammation) {
        this.anneeProgrammation = anneeProgrammation;
    }

    /**
     * @return the idDepartement
     */
    public Integer getIdDepartement() {
        return this.idDepartement;
    }

    /**
     * @param idDepartement
     *            the idDepartement to set
     */
    public void setIdDepartement(Integer idDepartement) {
        this.idDepartement = idDepartement;
    }

    /**
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum EtatDemandePaiementEnum.
 *
 * @author a453029
 */
public enum EtatDemandePaiementEnum {

    /** The demandee. */
    DEMANDEE,

    /** The qualifiee. */
    QUALIFIEE,

    /** The accordee. */
    ACCORDEE,

    /** The controlee. */
    CONTROLEE,

    /** The en attente transfert. */
    EN_ATTENTE_TRANSFERT,

    /** The en cours transfert - etape 1. */
    EN_COURS_TRANSFERT_1,

    /** The en cours transfert - etape 2. */
    EN_COURS_TRANSFERT_2,

    /** The en cours transfert - fin. */
    EN_COURS_TRANSFERT_FIN,

    /** The transferee. */
    TRANSFEREE,

    /** The anomalie detectee. */
    ANOMALIE_DETECTEE,

    /** The anomalie signalee. */
    ANOMALIE_SIGNALEE,

    /** The corrigee. */
    CORRIGEE,

    /** The refusee. */
    REFUSEE,

    /** The versee. */
    VERSEE,

    /** The certifiee. */
    CERTIFIEE
}

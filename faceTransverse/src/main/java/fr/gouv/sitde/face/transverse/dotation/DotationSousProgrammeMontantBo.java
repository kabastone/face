package fr.gouv.sitde.face.transverse.dotation;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class CollectiviteRepartitionBo.
 */
public class DotationSousProgrammeMontantBo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4340708382029184636L;

    /** The id. */
    private Long idDotationSousProgramme;

    /** The nom court. */
    private BigDecimal montant;

    /** The code departement. */
    private Boolean concerneInitial;

    /**
     * @return the idDotationSousProgramme
     */
    public Long getIdDotationSousProgramme() {
        return this.idDotationSousProgramme;
    }

    /**
     * @param idDotationSousProgramme
     *            the idDotationSousProgramme to set
     */
    public void setIdDotationSousProgramme(Long idDotationSousProgramme) {
        this.idDotationSousProgramme = idDotationSousProgramme;
    }

    /**
     * @return the montant
     */
    public BigDecimal getMontant() {
        return this.montant;
    }

    /**
     * @param montant
     *            the montant to set
     */
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    /**
     * @return the concerneInitial
     */
    public Boolean isConcerneInitial() {
        return this.concerneInitial;
    }

    /**
     * @param concerneInitial
     *            the concerneInitial to set
     */
    public void setConcerneInitial(Boolean concerneInitial) {
        this.concerneInitial = concerneInitial;
    }

}

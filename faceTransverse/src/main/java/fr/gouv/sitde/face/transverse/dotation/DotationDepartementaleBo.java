/**
 *
 */
package fr.gouv.sitde.face.transverse.dotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * The Class DotationDepartementaleBo.
 *
 * @author
 */
public class DotationDepartementaleBo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -782758870610871373L;

    /** The annee. */
    private Integer annee;

    /** The departement. */
    private Departement departement;

    /** The en preparation flag. */
    private Boolean enPreparation;

    /** The notifiee flag. */
    private Boolean notifiee;

    /** The somme. */
    private BigDecimal somme;

    /** The non regroupement. */
    private BigDecimal nonRegroupement;

    /** The stock. */
    private BigDecimal stock;

    /** The liste dotation par sous-programme. */
    private List<DotationDepartementaleParSousProgrammeBo> listeDotationsDepartementalesParSousProgramme = new ArrayList<>();

    /**
     * Instantiates a new dotation departementale bo.
     */
    public DotationDepartementaleBo() {
    }

    /**
     * Instantiates a new dotation departementale bo.
     *
     * @param annee
     *            the annee
     * @param departement
     *            the departement
     */
    public DotationDepartementaleBo(Integer annee, Departement departement) {
        this.annee = annee;
        this.departement = departement;
    }

    /**
     * Gets the annee.
     *
     * @return the annee
     */
    public Integer getAnnee() {
        return this.annee;
    }

    /**
     * Sets the annee.
     *
     * @param annee
     *            the new annee
     */
    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    /**
     * Gets the departement.
     *
     * @return the departement
     */
    public Departement getDepartement() {
        return this.departement;
    }

    /**
     * Sets the departement.
     *
     * @param departement
     *            the new departement
     */
    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    /**
     * Gets the en preparation.
     *
     * @return the en preparation
     */
    public Boolean getEnPreparation() {
        return this.enPreparation;
    }

    /**
     * Sets the en preparation.
     *
     * @param enPreparation
     *            the new en preparation
     */
    public void setEnPreparation(Boolean enPreparation) {
        this.enPreparation = enPreparation;
    }

    /**
     * Gets the notifiee.
     *
     * @return the notifiee
     */
    public Boolean getNotifiee() {
        return this.notifiee;
    }

    /**
     * Sets the notifiee.
     *
     * @param notifiee
     *            the new notifiee
     */
    public void setNotifiee(Boolean notifiee) {
        this.notifiee = notifiee;
    }

    /**
     * Gets the somme.
     *
     * @return the somme
     */
    public BigDecimal getSomme() {
        return this.somme;
    }

    /**
     * Sets the somme.
     *
     * @param somme
     *            the new somme
     */
    public void setSomme(BigDecimal somme) {
        this.somme = somme;
    }

    /**
     * Gets the non regroupement.
     *
     * @return the non regroupement
     */
    public BigDecimal getNonRegroupement() {
        return this.nonRegroupement;
    }

    /**
     * Sets the non regroupement.
     *
     * @param nonRegroupement
     *            the new non regroupement
     */
    public void setNonRegroupement(BigDecimal nonRegroupement) {
        this.nonRegroupement = nonRegroupement;
    }

    /**
     * Gets the stock.
     *
     * @return the stock
     */
    public BigDecimal getStock() {
        return this.stock;
    }

    /**
     * Sets the stock.
     *
     * @param stock
     *            the new stock
     */
    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    /**
     * Gets the liste dotations departementales par sous programme.
     *
     * @return the liste dotations departementales par sous programme
     */
    public List<DotationDepartementaleParSousProgrammeBo> getListeDotationsDepartementalesParSousProgramme() {
        return this.listeDotationsDepartementalesParSousProgramme;
    }

    /**
     * Sets the liste dotations departementales par sous programme.
     *
     * @param listeDotationsDepartementalesParSousProgramme
     *            the new liste dotations departementales par sous programme
     */
    public void setListeDotationsDepartementalesParSousProgramme(
            List<DotationDepartementaleParSousProgrammeBo> listeDotationsDepartementalesParSousProgramme) {
        this.listeDotationsDepartementalesParSousProgramme.addAll(listeDotationsDepartementalesParSousProgramme);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DotationDepartementaleBo{" + "annee=" + this.annee + ", departement=" + this.departement + ", somme=" + this.somme
                + ", nonRegroupement=" + this.nonRegroupement + ", stock=" + this.stock + ", listeDotationsDepartementalesParSousProgramme="
                + this.listeDotationsDepartementalesParSousProgramme + '}';
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * @author A754839
 *
 */
public enum VersionAlgorithmeEnum {

    V1, V2;
}

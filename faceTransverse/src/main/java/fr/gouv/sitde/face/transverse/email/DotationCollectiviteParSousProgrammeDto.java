/**
 *
 */
package fr.gouv.sitde.face.transverse.email;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The Class DotationCollectiviteParSousProgrammeDto.
 *
 * @author A754839
 */
public class DotationCollectiviteParSousProgrammeDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -782758870610871373L;

    /** The annee. */
    private Integer annee;

    /** The abreviation. */
    private SousProgrammeNotificationEnum abreviation;

    /** The dotation repartie. */
    private BigDecimal dotationRepartie;

    /** The aide consommee. */
    private BigDecimal aideConsommee;

    /** The taux. */
    private BigDecimal taux;

    /**
     * Instantiates a new dotation collectivite par sous programme DTO.
     *
     * @param dotationRepartie
     *            the dotation repartie
     * @param aideConsommee
     *            the aide consommee
     * @param annee
     *            the annee
     * @param abreviation
     *            the abreviation
     */
    public DotationCollectiviteParSousProgrammeDto(BigDecimal dotationRepartie, BigDecimal aideConsommee, Integer annee, String abreviation) {

        this.dotationRepartie = dotationRepartie;
        this.aideConsommee = aideConsommee;
        this.annee = annee;
        this.abreviation = SousProgrammeNotificationEnum.getEnumFromAbreviation(abreviation);
        if (dotationRepartie.compareTo(BigDecimal.ZERO) > 0) {
            this.taux = aideConsommee.setScale(2, RoundingMode.HALF_UP)
                    .divide(dotationRepartie.setScale(2, RoundingMode.HALF_UP), 4, RoundingMode.HALF_UP).multiply(new BigDecimal("100.00"))
                    .setScale(2, RoundingMode.HALF_UP);
        } else {
            this.taux = BigDecimal.ZERO;
        }
    }

    /**
     * Instantiates a new dotation collectivite par sous programme DTO.
     *
     * @param annee
     *            the annee
     * @param sprEnum
     *            the spr enum
     */
    public DotationCollectiviteParSousProgrammeDto(Integer annee, SousProgrammeNotificationEnum sprEnum) {

        this.annee = annee;
        this.abreviation = sprEnum;
        this.dotationRepartie = BigDecimal.ZERO;
        this.aideConsommee = BigDecimal.ZERO;
        this.taux = BigDecimal.ZERO;
    }

    /**
     * Gets the dotation repartie.
     *
     * @return the dotationRepartie
     */
    public BigDecimal getDotationRepartie() {
        return this.dotationRepartie;
    }

    /**
     * Sets the dotation repartie.
     *
     * @param dotationRepartie
     *            the dotationRepartie to set
     */
    public void setDotationRepartie(BigDecimal dotationRepartie) {
        this.dotationRepartie = dotationRepartie;
    }

    /**
     * Gets the aide consommee.
     *
     * @return the aideConsommee
     */
    public BigDecimal getAideConsommee() {
        return this.aideConsommee;
    }

    /**
     * Sets the aide consommee.
     *
     * @param aideConsommee
     *            the aideConsommee to set
     */
    public void setAideConsommee(BigDecimal aideConsommee) {
        this.aideConsommee = aideConsommee;
    }

    /**
     * Gets the taux.
     *
     * @return the taux
     */
    public BigDecimal getTaux() {
        return this.taux;
    }

    /**
     * Sets the taux.
     *
     * @param taux
     *            the taux to set
     */
    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    /**
     * Gets the annee.
     *
     * @return the annee
     */
    public Integer getAnnee() {
        return this.annee;
    }

    /**
     * Sets the annee.
     *
     * @param annee
     *            the annee to set
     */
    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    /**
     * Gets the abreviation.
     *
     * @return the abreviationSousProgramme
     */
    public SousProgrammeNotificationEnum getAbreviation() {
        return this.abreviation;
    }

    /**
     * Sets the abreviation.
     *
     * @param abreviationSousProgramme
     *            the abreviationSousProgramme to set
     */
    public void setAbreviation(SousProgrammeNotificationEnum abreviationSousProgramme) {
        this.abreviation = abreviationSousProgramme;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.getAnnee() + " | " + this.getAbreviation() + " | Consommé : " + this.getAideConsommee() + " sur " + this.getDotationRepartie()
                + " || " + this.getTaux();
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum StatutEjChorusEnum.
 *
 * @author a453029
 */
public enum StatutEjChorusEnum {

    /** Le statut commandé est pour un EJ validé */
    COMMANDE("commandé"),

    /** Le statut sauvegardé est pour l'envoi de l'EJ. */
    SAUVEGARDE("sauvegardé"),

    /** Le statut clôturé est pour un EJ clôturé. Non traité par l'application FACE. */
    CLOTURE("clôturé"),

    /** Le statut Supprimé est pour un EJ supprimé. Non traité par l'application FACE. */
    SUPPRIME("supprimé"),

    /** Le statut Basculé est pour l'EJ reporté sur l'exercice suivant. Non traité par l'application FACE. */
    BASCULE("basculé"),

    /** Le statut En cours d'approbation est pour ... Non traité par l'application FACE. */
    EN_COURS_APPROBATION("en cours d'approbation");

    /** The code. */
    private final String code;

    /**
     * Instantiates a new statut ej chorus enum.
     *
     * @param code
     *            the code
     */
    StatutEjChorusEnum(final String code) {
        this.code = code;
    }

    /**
     * Renvoie le nom du template.
     *
     * @return Le nom du template.
     */
    public String getCode() {
        return this.code;
    }
}

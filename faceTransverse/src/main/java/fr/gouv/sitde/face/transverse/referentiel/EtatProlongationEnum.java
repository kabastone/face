/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum TypeProlongationEnum.
 *
 * @author A754839
 */
public enum EtatProlongationEnum {

    /** The demandee. */
    DEMANDEE("DEMANDEE"),

    /** The accordee. */
    ACCORDEE("ACCORDEE"),

    /** The incoherence avec chorus. */
    INCOHERENCE_AVEC_CHORUS("INCOHERENCE_AVEC_CHORUS"),

    /** The validee. */
    VALIDEE("VALIDEE"),

    /** The refusee. */
    REFUSEE("REFUSEE");

    /** The type. */
    private String type;

    /**
     * Instantiates a new type prolongation enum.
     *
     * @param type
     *            the type
     */
    EtatProlongationEnum(String type) {
        this.type = type;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }
}

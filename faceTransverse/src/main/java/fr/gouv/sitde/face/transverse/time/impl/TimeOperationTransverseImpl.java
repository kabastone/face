package fr.gouv.sitde.face.transverse.time.impl;

import java.time.LocalDateTime;

import javax.inject.Named;

import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class TimeUtils.
 */
@Named
public class TimeOperationTransverseImpl implements TimeOperationTransverse {

    /** The Constant nanoseconds. */
    private static final int MIN_NANO_SECOND = 0;

    /** The Constant MAX_NANO_SECOND. */
    private static final int MAX_NANO_SECOND = 999_999_999;

    /** The Constant MAX_HOUR. */
    public static final int MAX_HOUR = 23;

    /** The Constant MIN_HOUR. */
    public static final int MIN_HOUR = 0;

    /** The Constant MAX_MINUTE. */
    public static final int MAX_MINUTE = 59;

    /** The Constant MIN_MINUTE. */
    public static final int MIN_MINUTE = 0;

    /** The Constant MAX_SECOND. */
    public static final int MAX_SECOND = 59;

    /** The Constant MIN_SECOND. */
    public static final int MIN_SECOND = 0;

    /** The Constant MAX_DAY_OF_MONTH. */
    public static final int MAX_DAY_OF_MONTH = 31;

    /** The Constant MIN_DAY_OF_MONTH. */
    public static final int MIN_DAY_OF_MONTH = 1;

    /** The Constant MAX_MONTH. */
    public static final int MAX_MONTH = 12;

    /** The Constant MIN_MONTH. */
    public static final int MIN_MONTH = 1;

    /**
     * Obtenir une instance de LocalDateTime avec la date et l'heure passées en paramètre avec le fuseau horaire système.
     *
     * @param year
     *            the year
     * @param month
     *            the month
     * @param dayOfMonth
     *            the day of month
     * @param hour
     *            the hour
     * @param minute
     *            the minute
     * @param second
     *            the second
     * @return the offset date time
     */
    @Override
    public LocalDateTime getLocalDateTimeOf(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, MIN_NANO_SECOND);
    }

    /**
     * Gets the 31 december of current year.
     *
     * @return the 31 december of current year
     */
    @Override
    public LocalDateTime get31DecemberOfCurrentYear() {
        return LocalDateTime.of(this.getAnneeCourante(), MAX_MONTH, MAX_DAY_OF_MONTH, MAX_HOUR, MAX_MINUTE, MAX_SECOND, MIN_NANO_SECOND);
    }

    /**
     * Gets the annee courante.
     *
     * @return the annee courante
     */
    @Override
    public int getAnneeCourante() {
        return LocalDateTime.now().getYear();
    }

    /**
     * Compare egalite dates.
     *
     * @param first
     *            the first
     * @param second
     *            the second
     * @return true, if successful
     */
    @Override
    public boolean compareEgaliteDates(LocalDateTime first, LocalDateTime second) {
        if (first == null) {
            return false;
        }

        if (second == null) {
            return false;
        }

        return (first.getYear() == second.getYear()) && (first.getMonthValue() == second.getMonthValue())
                && (first.getDayOfMonth() == second.getDayOfMonth());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.transverse.time.TimeOperationTransverse#getDebutAnnee(java.lang.Integer)
     */
    @Override
    public LocalDateTime getDebutAnnee(Integer anneeCreationDossier) {
        return this.getLocalDateTimeOf(anneeCreationDossier, MIN_MONTH, MIN_DAY_OF_MONTH, MIN_HOUR, MIN_MINUTE, MIN_SECOND);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.transverse.time.TimeOperationTransverse#getFinAnnee(java.lang.Integer)
     */
    @Override
    public LocalDateTime getFinAnnee(Integer anneeCreationDossier) {
        return LocalDateTime.of(anneeCreationDossier, MAX_MONTH, MAX_DAY_OF_MONTH, MAX_HOUR, MAX_MINUTE, MAX_SECOND, MAX_NANO_SECOND);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.transverse.time.TimeOperationTransverse#now()
     */
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}

/**
 *
 */
package fr.gouv.sitde.face.transverse.pagination;

import java.io.Serializable;

/**
 * Classe permettant de définir une demande de page.
 *
 * @author Atos
 *
 */
public class PageDemande implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -1093838308757274792L;

    /** Index de la page voulue (attention, l'index commence à zero). */
    private int indexPage;

    /** Taille de la page voulue (nombre de résultats). */
    private int taillePage;

    /** Champ de tri de la clause orderBy. */
    private String champTri;

    /** Le tri doit être dans le sens descendant ou pas. */
    private boolean desc;

    /** The valeur filtre. */
    private String valeurFiltre;

    /**
     * Constructeur paramétré.
     *
     * @param indexPage
     *            Index de la page voulue (attention, l'index commence à zero).
     * @param taillePage
     *            Taille de la page voulue (nombre de résultats par page).
     * @param champTri
     *            champTri
     * @param desc
     *            Tri par sens descandant ou pas.
     * @param valeurFiltre
     *            the valeur filtre
     */
    public PageDemande(final int indexPage, final int taillePage, final String champTri, final boolean desc, String valeurFiltre) {
        super();
        this.indexPage = indexPage;
        this.taillePage = taillePage;
        this.champTri = champTri;
        this.desc = desc;
        this.valeurFiltre = valeurFiltre;
    }

    /**
     * Constructeur paramétré.
     *
     * @param indexPage
     *            Index de la page voulue (attention, l'index commence à zero).
     * @param taillePage
     *            Taille de la page voulue (nombre de résultats par page).
     * @param champTri
     *            champTri
     * @param desc
     *            Tri par sens descandant ou pas.
     */
    public PageDemande(final int indexPage, final int taillePage, final String champTri, final boolean desc) {
        super();
        this.indexPage = indexPage;
        this.taillePage = taillePage;
        this.champTri = champTri;
        this.desc = desc;
    }

    /**
     * Constructeur paramétré (sans tri).
     *
     * @param indexPage
     *            Index de la page voulue (attention, l'index commence à zero).
     * @param taillePage
     *            Taille de la page voulue (nombre de résultats par page).
     */
    public PageDemande(final int indexPage, final int taillePage) {
        super();
        this.indexPage = indexPage;
        this.taillePage = taillePage;
    }

    /**
     * Constructeur non-paramétré.
     */
    public PageDemande() {
        // Constructeur non-parammétré
    }

    /**
     * Gets the index page.
     *
     * @return the indexPage
     */
    public int getIndexPage() {
        return this.indexPage;
    }

    /**
     * Sets the index page.
     *
     * @param indexPage
     *            the indexPage to set
     */
    public void setIndexPage(final int indexPage) {
        this.indexPage = indexPage;
    }

    /**
     * Gets the taille page.
     *
     * @return the taillePage
     */
    public int getTaillePage() {
        return this.taillePage;
    }

    /**
     * Sets the taille page.
     *
     * @param taillePage
     *            the taillePage to set
     */
    public void setTaillePage(final int taillePage) {
        this.taillePage = taillePage;
    }

    /**
     * Renvoi l'index du premier résultat voulu (utilisé par l'API typedQuery JPA).
     *
     * @return l'index du premier résultat (commence à zero)
     */
    public int getIndexPremierResultat() {
        return this.indexPage * this.taillePage;
    }

    /**
     * Gets the champ tri.
     *
     * @return the champTri
     */
    public String getChampTri() {
        return this.champTri;
    }

    /**
     * Sets the champ tri.
     *
     * @param champTri
     *            the champTri to set
     */
    public void setChampTri(String champTri) {
        this.champTri = champTri;
    }

    /**
     * Checks if is desc.
     *
     * @return the desc
     */
    public boolean isDesc() {
        return this.desc;
    }

    /**
     * Sets the desc.
     *
     * @param desc
     *            the desc to set
     */
    public void setDesc(boolean desc) {
        this.desc = desc;
    }

    /**
     * @return the valeurFiltre
     */
    public String getValeurFiltre() {
        return this.valeurFiltre;
    }

    /**
     * @param valeurFiltre
     *            the valeurFiltre to set
     */
    public void setValeurFiltre(String valeurFiltre) {
        this.valeurFiltre = valeurFiltre;
    }
}

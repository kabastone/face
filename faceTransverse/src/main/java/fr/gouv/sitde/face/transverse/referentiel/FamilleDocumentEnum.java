/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * The Enum FamilleDocumentEnum.
 *
 * @author a453029
 */
public enum FamilleDocumentEnum {

    /** The doc demande subvention. */
    DOC_DEMANDE_SUBVENTION(100, "DOC_DEMANDE_SUBVENTION"),

    /** The doc demande prolongation. */
    DOC_DEMANDE_PROLONGATION(200, "DOC_DEMANDE_PROLONGATION"),

    /** The doc demande paiement. */
    DOC_DEMANDE_PAIEMENT(300, "DOC_DEMANDE_PAIEMENT"),

    /** The doc courrier annuel departement. */
    DOC_COURRIER_ANNUEL_DEPARTEMENT(400, "DOC_COURRIER_ANNUEL_DEPARTEMENT"),

    /** The doc ligne dotation departement. */
    DOC_LIGNE_DOTATION_DEPARTEMENT(500, "DOC_LIGNE_DOTATION_DEPARTEMENT"),

    /** The doc modele. */
    DOC_MODELE(600, "DOC_MODELE");

    /** The id codifie. */
    private final Integer idCodifie;

    /** The code. */
    private final String code;

    /**
     * Instantiates a new famille document enum.
     *
     * @param idCodifie
     *            the id codifie
     * @param code
     *            the code
     */
    FamilleDocumentEnum(Integer idCodifie, String code) {
        this.idCodifie = idCodifie;
        this.code = code;
    }

    /**
     * Gets the id codifie.
     *
     * @return the id codifie
     */
    public Integer getIdCodifie() {
        return this.idCodifie;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Renvoie l'enum correspondant à l'idCodifie passée en parametre.<br/>
     *
     * @param idCodifie
     *            the id codifie
     * @return l'enum correspondante ou null
     */
    public static FamilleDocumentEnum rechercherEnumParIdCodifie(final Integer idCodifie) {

        for (FamilleDocumentEnum familleDocEnum : FamilleDocumentEnum.values()) {
            if (familleDocEnum.getIdCodifie().equals(idCodifie)) {
                return familleDocEnum;
            }
        }
        return null;
    }

}

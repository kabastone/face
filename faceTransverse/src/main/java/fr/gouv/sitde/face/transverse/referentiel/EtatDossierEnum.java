/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * Enum des états d'un dossier de subvention.
 *
 * @author a453029
 *
 */
public enum EtatDossierEnum {

    /** Etat ouvert. */
    OUVERT("OUVERT", "etatDossier.ouvert", 1),

    /** Etat expiré. */
    EXPIRE("EXPIRE", "etatDossier.expire", 3),

    /** Etat soldé. */
    SOLDE("SOLDE", "etatDossier.solde", 2),

    /** Etat clôturé. */
    CLOTURE("CLOTURE", "etatDossier.cloture", 4);

    /** The cleLibelle. */
    private final String cleLibelle;

    /** The code. */
    private final String code;

    /** The order. */
    private final Integer order;

    /**
     * Constructeur privé.
     *
     * @param code
     *            the code
     * @param cleLibelle
     *            the cle libelle
     */
    EtatDossierEnum(final String code, final String cleLibelle, final Integer order) {
        this.code = code;
        this.cleLibelle = cleLibelle;
        this.order = order;
    }

    /**
     * Renvoie le code de l'enum.
     *
     * @return Le code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Texte courrier of code.
     *
     * @param code
     *            the code
     * @return the string
     */
    public String getLibelle() {
        return MessageUtils.getLibelleReferentiel(this.cleLibelle);
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * Renvoie l'enum correspondant au code passé en parametre.<br/>
     * remarque : la recherche du code est insensible à la casse.
     *
     * @param code
     *            le code voulu
     * @return l'enum correspondante ou null
     */
    public static EtatDossierEnum rechercherEnumParCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        for (EtatDossierEnum etatDossierEnum : EtatDossierEnum.values()) {
            if (code.equalsIgnoreCase(etatDossierEnum.getCode())) {
                return etatDossierEnum;
            }
        }
        return null;
    }

}

package fr.gouv.sitde.face.transverse.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The Interface Siret.
 *
 * @author a453029
 *
 */
@Target({ FIELD, METHOD })
@Retention(RUNTIME)
@Constraint(validatedBy = SiretValidator.class)
@Documented
public @interface Siret {

    /**
     * Message.
     *
     * @return the string
     */
    String message() default "collectivite.siret.invalide";

    /**
     * Groups.
     *
     * @return the class[]
     */
    Class<?>[] groups() default {};

    /**
     * Payload.
     *
     * @return the class<? extends payload>[]
     */
    Class<? extends Payload>[] payload() default {};

}

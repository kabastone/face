/**
 *
 */
package fr.gouv.sitde.face.transverse.referentiel;

/**
 * Enum des états de transfert de paiement.
 *
 * @author Atos
 *
 */
public enum EtatTransfertPaiementEnum {

    /** DEMANDE. */
    DEMANDE,

    /** REFUSE. */
    REFUSE,

    /** VERSE. */
    VERSE;

}

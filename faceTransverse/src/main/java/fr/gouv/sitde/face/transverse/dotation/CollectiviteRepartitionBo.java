package fr.gouv.sitde.face.transverse.dotation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CollectiviteRepartitionBo.
 */
public class CollectiviteRepartitionBo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4340708382029184636L;

    /** The id. */
    private Long id;

    /** The nom court. */
    private String nomCourt;

    /** The code departement. */
    private String codeDepartement;

    /** The dotations collectivite. */
    private List<DotationCollectiviteRepartitionParSousProgrammeBo> dotationsCollectivite = new ArrayList<>();

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the nom court.
     *
     * @return the nomCourt
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

    /**
     * Sets the nom court.
     *
     * @param nomCourt
     *            the nomCourt to set
     */
    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    /**
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Gets the dotations collectivite.
     *
     * @return the dotationsCollectivite
     */
    public List<DotationCollectiviteRepartitionParSousProgrammeBo> getDotationsCollectivite() {
        return this.dotationsCollectivite;
    }

    /**
     * Sets the dotations collectivite.
     *
     * @param dotationsCollectivite
     *            the dotationsCollectivite to set
     */
    public void setDotationsCollectivite(List<DotationCollectiviteRepartitionParSousProgrammeBo> dotationsCollectivite) {
        this.dotationsCollectivite = dotationsCollectivite;
    }

}

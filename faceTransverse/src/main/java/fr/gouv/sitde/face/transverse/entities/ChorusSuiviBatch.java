/**
 *
 */
package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class ChorusSuiviBatch.
 *
 * @author A452836
 */
@Entity
@Table(name = "chorus_suivi_batch")
public class ChorusSuiviBatch implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3516468931728339365L;

    /** Clé primaire de la table. */
    private Boolean actif = Boolean.FALSE;

    /** Dernier numéro d'intégration du fichier FSO0051A - EJ . */
    private int fso0051aEj;

    /** Dernier numéro d'intégration du fichier FSO0051A - SF . */
    private int fso0051aSf;

    /** Dernier numéro d'intégration du fichier FSO0051A - DP . */
    private int fso0051aDp;

    /** Dernier numéro généré des fichiers fso0028a. */
    private int fso0028a;

    /** Dernier numéro généré des fichiers fen0111a. */
    private int fen0111a;

    /** Dernier numéro généré des fichiers fen0072a. */
    private int fen0072a;

    /** Dernier numéro généré des fichiers fen0159a. */
    private int fen0159a;

    /**
     * Checks if is actif.
     *
     * @return the actif
     */
    @Id
    @Column(name = "csb_actif", unique = true, nullable = false)
    public Boolean isActif() {
        return this.actif;
    }

    /**
     * Sets the actif.
     *
     * @param actif            the actif to set
     */
    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    /**
     * Gets the fso 0051 a ej.
     *
     * @return the fso0051aEj
     */
    @Column(name = "csb_fso0051a_ej", nullable = false)
    public int getFso0051aEj() {
        return this.fso0051aEj;
    }

    /**
     * Sets the fso 0051 a ej.
     *
     * @param fso0051aEj            the fso0051aEj to set
     */
    public void setFso0051aEj(int fso0051aEj) {
        this.fso0051aEj = fso0051aEj;
    }

    /**
     * Gets the fso 0051 a sf.
     *
     * @return the fso0051aSf
     */
    @Column(name = "csb_fso0051a_sf", nullable = false)
    public int getFso0051aSf() {
        return this.fso0051aSf;
    }

    /**
     * Sets the fso 0051 a sf.
     *
     * @param fso0051aSf            the fso0051aSf to set
     */
    public void setFso0051aSf(int fso0051aSf) {
        this.fso0051aSf = fso0051aSf;
    }

    /**
     * Gets the fso 0051 a dp.
     *
     * @return the fso0051aDp
     */
    @Column(name = "csb_fso0051a_dp", nullable = false)
    public int getFso0051aDp() {
        return this.fso0051aDp;
    }

    /**
     * Sets the fso 0051 a dp.
     *
     * @param fso0051aDp            the fso0051aDp to set
     */
    public void setFso0051aDp(int fso0051aDp) {
        this.fso0051aDp = fso0051aDp;
    }

    /**
     * Gets the fso 0028 a.
     *
     * @return the fso0028a
     */
    @Column(name = "csb_fso0028a", nullable = false)
    public int getFso0028a() {
        return this.fso0028a;
    }

    /**
     * Sets the fso 0028 a.
     *
     * @param fso0028a            the fso0028a to set
     */
    public void setFso0028a(int fso0028a) {
        this.fso0028a = fso0028a;
    }

    /**
     * Gets the fen 0111 a.
     *
     * @return the fen0111a
     */
    @Column(name = "csb_fen0111a", nullable = false)
    public int getFen0111a() {
        return this.fen0111a;
    }

    /**
     * Sets the fen 0111 a.
     *
     * @param fen0111a            the fen0111a to set
     */
    public void setFen0111a(int fen0111a) {
        this.fen0111a = fen0111a;
    }

    /**
     * Gets the fen 0072 a.
     *
     * @return the fen0072a
     */
    @Column(name = "csb_fen0072a", nullable = false)
    public int getFen0072a() {
        return this.fen0072a;
    }

    /**
     * Sets the fen 0072 a.
     *
     * @param fen0072a            the fen0072a to set
     */
    public void setFen0072a(int fen0072a) {
        this.fen0072a = fen0072a;
    }

    /**
     * Gets the fen 0159 a.
     *
     * @return the fen0159a
     */
    @Column(name = "csb_fen0159a", nullable = false)
    public int getFen0159a() {
        return this.fen0159a;
    }

    /**
     * Sets the fen 0159 a.
     *
     * @param fen0159a            the fen0159a to set
     */
    public void setFen0159a(int fen0159a) {
        this.fen0159a = fen0159a;
    }

}

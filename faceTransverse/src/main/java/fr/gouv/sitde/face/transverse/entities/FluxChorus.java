/**
 *
 */
package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class FluxChorus.
 *
 * @author A452836
 */
@Entity
@Table(name = "r_flux_chorus")
public class FluxChorus implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3516468931728339365L;

    /** Clé primaire de la table. */
    private Boolean actif;

    /** Centre de coût . */
    private String centreDeCout;

    /** Code application . */
    private String codeApplication;

    /** The code demandeur. */
    private String codeDemandeur;

    /** Code devise . */
    private String codeDevise;

    /** Code mouvement . */
    private String codeMvt;

    /** Code sociéte . */
    private String codeSociete;

    /** The code site. */
    private String codeSite;

    /** Code TVA . */
    private String codeTVA;

    /** Condition de paiement . */
    private String conditionPaiement;

    /** Localisation interministerielle . */
    private String localisationInter;

    /** Organisation d'achat . */
    private String organisationDachat;

    /** Utilisateur CHORUS . */
    private String prUname;

    /** Domaine d'activité . */
    private String domaineActivite;

    /** Groupe d'acheteurs . */
    private String groupeAcheteurs;

    /** Service fait automatique flux 2 . */
    private String serviceFaitAutoFlux;

    /** The schema partenaire. */
    private String schemaPartenaire;

    /** Type d'imputation . */
    private String typeImputation;

    /** Type de ligne . */
    private String typeLigne;

    /** Type d'opération . */
    private String typeOperation;

    /** Type d'option . */
    private String typeOption;

    /** The unite. */
    private String unite;

    /**
     * @return the actif
     */
    @Id
    @Column(name = "fch_actif", unique = true, nullable = false)
    public Boolean isActif() {
        return this.actif;
    }

    /**
     * @param actif
     *            the actif to set
     */
    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    /**
     * @return the centreDeCout
     */
    @Column(name = "fch_centre_de_cout", unique = true, nullable = false, length = 10)
    public String getCentreDeCout() {
        return this.centreDeCout;
    }

    /**
     * @param centreDeCout
     *            the centreDeCout to set
     */
    public void setCentreDeCout(String centreDeCout) {
        this.centreDeCout = centreDeCout;
    }

    /**
     * @return the codeApplication
     */
    @Column(name = "fch_code_appli", unique = true, nullable = false, length = 10)
    public String getCodeApplication() {
        return this.codeApplication;
    }

    /**
     * @param codeApplication
     *            the codeApplication to set
     */
    public void setCodeApplication(String codeApplication) {
        this.codeApplication = codeApplication;
    }

    /**
     * @return the codeDemandeur
     */
    @Column(name = "fch_code_demandeur", unique = true, nullable = false, length = 10)
    public String getCodeDemandeur() {
        return this.codeDemandeur;
    }

    /**
     * @param codeDemandeur
     *            the codeDemandeur to set
     */
    public void setCodeDemandeur(String codeDemandeur) {
        this.codeDemandeur = codeDemandeur;
    }

    /**
     * @return the codeDevise
     */
    @Column(name = "fch_code_devise", unique = true, nullable = false, length = 10)
    public String getCodeDevise() {
        return this.codeDevise;
    }

    /**
     * @param codeDevise
     *            the codeDevise to set
     */
    public void setCodeDevise(String codeDevise) {
        this.codeDevise = codeDevise;
    }

    /**
     * @return the codeMvt
     */
    @Column(name = "fch_code_mvt", unique = true, nullable = false, length = 10)
    public String getCodeMvt() {
        return this.codeMvt;
    }

    /**
     * @param codeMvt
     *            the codeMvt to set
     */
    public void setCodeMvt(String codeMvt) {
        this.codeMvt = codeMvt;
    }

    /**
     * @return the codeSociete
     */
    @Column(name = "fch_code_societe", unique = true, nullable = false, length = 10)
    public String getCodeSociete() {
        return this.codeSociete;
    }

    /**
     * @param codeSociete
     *            the codeSociete to set
     */
    public void setCodeSociete(String codeSociete) {
        this.codeSociete = codeSociete;
    }

    /**
     * @return the codeSite
     */
    @Column(name = "fch_code_site", unique = true, nullable = false, length = 10)
    public String getCodeSite() {
        return this.codeSite;
    }

    /**
     * @param codeSite
     *            the codeSite to set
     */
    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    /**
     * @return the codeTVA
     */
    @Column(name = "fch_code_tva", unique = true, nullable = false, length = 10)
    public String getCodeTVA() {
        return this.codeTVA;
    }

    /**
     * @param codeTVA
     *            the codeTVA to set
     */
    public void setCodeTVA(String codeTVA) {
        this.codeTVA = codeTVA;
    }

    /**
     * @return the conditionPaiement
     */
    @Column(name = "fch_condition_paiement", unique = true, nullable = false, length = 10)
    public String getConditionPaiement() {
        return this.conditionPaiement;
    }

    /**
     * @param conditionPaiement
     *            the conditionPaiement to set
     */
    public void setConditionPaiement(String conditionPaiement) {
        this.conditionPaiement = conditionPaiement;
    }

    /**
     * @return the localisationInter
     */
    @Column(name = "fch_localisation_inter", unique = true, nullable = false, length = 10)
    public String getLocalisationInter() {
        return this.localisationInter;
    }

    /**
     * @param localisationInter
     *            the localisationInter to set
     */
    public void setLocalisationInter(String localisationInter) {
        this.localisationInter = localisationInter;
    }

    /**
     * @return the organisationDachat
     */
    @Column(name = "fch_organisation_achats", unique = true, nullable = false, length = 10)
    public String getOrganisationDachat() {
        return this.organisationDachat;
    }

    /**
     * @param organisationDachat
     *            the organisationDachat to set
     */
    public void setOrganisationDachat(String organisationDachat) {
        this.organisationDachat = organisationDachat;
    }

    /**
     * @return the prUname
     */
    @Column(name = "fch_pr_uname", unique = true, nullable = false, length = 16)
    public String getPrUname() {
        return this.prUname;
    }

    /**
     * @param prUname
     *            the prUname to set
     */
    public void setPrUname(String prUname) {
        this.prUname = prUname;
    }

    /**
     * @return the domaineActivite
     */
    @Column(name = "fch_domaine_activite", unique = true, nullable = false, length = 10)
    public String getDomaineActivite() {
        return this.domaineActivite;
    }

    /**
     * @param domaineActivite
     *            the domaineActivite to set
     */
    public void setDomaineActivite(String domaineActivite) {
        this.domaineActivite = domaineActivite;
    }

    /**
     * @return the groupeAcheteurs
     */
    @Column(name = "fch_groupe_acheteurs", unique = true, nullable = false, length = 10)
    public String getGroupeAcheteurs() {
        return this.groupeAcheteurs;
    }

    /**
     * @param groupeAcheteurs
     *            the groupeAcheteurs to set
     */
    public void setGroupeAcheteurs(String groupeAcheteurs) {
        this.groupeAcheteurs = groupeAcheteurs;
    }

    /**
     * @return the serviceFaitAutoFlux
     */
    @Column(name = "fch_service_fait_auto_flux_2", unique = true, nullable = false, length = 10)
    public String getServiceFaitAutoFlux() {
        return this.serviceFaitAutoFlux;
    }

    /**
     * @param serviceFaitAutoFlux
     *            the serviceFaitAutoFlux to set
     */
    public void setServiceFaitAutoFlux(String serviceFaitAutoFlux) {
        this.serviceFaitAutoFlux = serviceFaitAutoFlux;
    }

    /**
     * @return the schemaPartenaire
     */
    @Column(name = "fch_schema_partenaire", unique = true, nullable = false, length = 10)
    public String getSchemaPartenaire() {
        return this.schemaPartenaire;
    }

    /**
     * @param schemaPartenaire
     *            the schemaPartenaire to set
     */
    public void setSchemaPartenaire(String schemaPartenaire) {
        this.schemaPartenaire = schemaPartenaire;
    }

    /**
     * @return the typeImputation
     */
    @Column(name = "fch_type_imputation", unique = true, nullable = false, length = 10)
    public String getTypeImputation() {
        return this.typeImputation;
    }

    /**
     * @param typeImputation
     *            the typeImputation to set
     */
    public void setTypeImputation(String typeImputation) {
        this.typeImputation = typeImputation;
    }

    /**
     * @return the typeLigne
     */
    @Column(name = "fch_type_ligne", unique = true, nullable = false, length = 10)
    public String getTypeLigne() {
        return this.typeLigne;
    }

    /**
     * @param typeLigne
     *            the typeLigne to set
     */
    public void setTypeLigne(String typeLigne) {
        this.typeLigne = typeLigne;
    }

    /**
     * @return the typeOperation
     */
    @Column(name = "fch_type_operation", unique = true, nullable = false, length = 10)
    public String getTypeOperation() {
        return this.typeOperation;
    }

    /**
     * @param typeOperation
     *            the typeOperation to set
     */
    public void setTypeOperation(String typeOperation) {
        this.typeOperation = typeOperation;
    }

    /**
     * @return the typeOption
     */
    @Column(name = "fch_type_option", unique = true, nullable = false, length = 10)
    public String getTypeOption() {
        return this.typeOption;
    }

    /**
     * @param typeOption
     *            the typeOption to set
     */
    public void setTypeOption(String typeOption) {
        this.typeOption = typeOption;
    }

    /**
     * @return the unite
     */
    @Column(name = "fch_unite", unique = true, nullable = false, length = 10)
    public String getUnite() {
        return this.unite;
    }

    /**
     * @param unite
     *            the unite to set
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

}

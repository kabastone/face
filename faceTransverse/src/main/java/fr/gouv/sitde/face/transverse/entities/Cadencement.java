package fr.gouv.sitde.face.transverse.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * The Class Cadencement.
 */
@Entity
@Table(name = "cadencement")
public class Cadencement implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8426814306992236232L;

    /** The id. */
    private Long id;

    /** The demande subvention. */
    private DemandeSubvention demandeSubvention;

    /** The montant annee 1. */
    private BigDecimal montantAnnee1;

    /** The montant annee 2. */
    private BigDecimal montantAnnee2;

    /** The montant annee 3. */
    private BigDecimal montantAnnee3;

    /** The montant annee 4. */
    private BigDecimal montantAnnee4;

    /** The montant annee prolongation. */
    private BigDecimal montantAnneeProlongation;

    /** The est valide. */
    private Boolean estValide;

    /**
     * Gets the est valide.
     *
     * @return the est valide
     */
    @Column(name = "cad_est_valide")
    public Boolean getEstValide() {
        return this.estValide;
    }

    /**
     * Sets the est valide.
     *
     * @param estValide
     *            the new est valide
     */
    public void setEstValide(Boolean estValide) {
        this.estValide = estValide;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Id
    @Column(name = "cad_dsu_id")
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the demande subvention.
     *
     * @return the demande subvention
     */
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "cad_dsu_id")
    public DemandeSubvention getDemandeSubvention() {
        return this.demandeSubvention;
    }

    /**
     * Sets the demande subvention.
     *
     * @param demandeSubvention
     *            the new demande subvention
     */
    public void setDemandeSubvention(DemandeSubvention demandeSubvention) {
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * Gets the montant annee 1.
     *
     * @return the montant annee 1
     */
    @Column(name = "cad_montant_annee_1", nullable = false)
    @NotNull(message = "cadencement.montant.annee.1.non.renseigne")
    @DecimalMin(message = "cadencement.montant.negatif", value = "0.0")
    public BigDecimal getMontantAnnee1() {
        return this.montantAnnee1;
    }

    /**
     * Sets the montant annee 1.
     *
     * @param montantAnnee1
     *            the new montant annee 1
     */
    public void setMontantAnnee1(BigDecimal montantAnnee1) {
        this.montantAnnee1 = montantAnnee1;
    }

    /**
     * Gets the montant annee 2.
     *
     * @return the montant annee 2
     */
    @Column(name = "cad_montant_annee_2", nullable = false)
    @NotNull(message = "cadencement.montant.annee.2.non.renseigne")
    @DecimalMin(message = "cadencement.montant.negatif", value = "0.0")
    public BigDecimal getMontantAnnee2() {
        return this.montantAnnee2;
    }

    /**
     * Sets the montant annee 2.
     *
     * @param montantAnnee2
     *            the new montant annee 2
     */
    public void setMontantAnnee2(BigDecimal montantAnnee2) {
        this.montantAnnee2 = montantAnnee2;
    }

    /**
     * Gets the montant annee 3.
     *
     * @return the montant annee 3
     */
    @Column(name = "cad_montant_annee_3", nullable = false)
    @NotNull(message = "cadencement.montant.annee.3.non.renseigne")
    @DecimalMin(message = "cadencement.montant.negatif", value = "0.0")
    public BigDecimal getMontantAnnee3() {
        return this.montantAnnee3;
    }

    /**
     * Sets the montant annee 3.
     *
     * @param montantAnnee3
     *            the new montant annee 3
     */
    public void setMontantAnnee3(BigDecimal montantAnnee3) {
        this.montantAnnee3 = montantAnnee3;
    }

    /**
     * Gets the montant annee 4.
     *
     * @return the montant annee 4
     */
    @Column(name = "cad_montant_annee_4", nullable = false)
    @NotNull(message = "cadencement.montant.annee.4.non.renseigne")
    @DecimalMin(message = "cadencement.montant.negatif", value = "0.0")
    public BigDecimal getMontantAnnee4() {
        return this.montantAnnee4;
    }

    /**
     * Sets the montant annee 4.
     *
     * @param montantAnnee4
     *            the new montant annee 4
     */
    public void setMontantAnnee4(BigDecimal montantAnnee4) {
        this.montantAnnee4 = montantAnnee4;
    }

    /**
     * Gets the montant annee prolongation.
     *
     * @return the montant annee prolongation
     */
    @Column(name = "cad_montant_annee_prol", nullable = true)
    @DecimalMin(message = "cadencement.montant.negatif", value = "0.0")
    public BigDecimal getMontantAnneeProlongation() {
        return this.montantAnneeProlongation;
    }

    /**
     * Sets the montant annee prolongation.
     *
     * @param montantAnneeProlongation
     *            the new montant annee prolongation
     */
    public void setMontantAnneeProlongation(BigDecimal montantAnneeProlongation) {
        this.montantAnneeProlongation = montantAnneeProlongation;
    }

}

package fr.gouv.sitde.face.transverse.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * The Class FamilleDocument.
 */
@Entity
@Table(name = "r_famille_document", uniqueConstraints = @UniqueConstraint(columnNames = "fdo_code"))
public class FamilleDocument implements java.io.Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2390900655645922770L;

    /** The idCodifie. */
    private Integer idCodifie;

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /**
     * Gets the idCodifie.
     *
     * @return the idCodifie
     */
    @Id
    @Column(name = "fdo_id_codifie", unique = true, nullable = false)
    public Integer getIdCodifie() {
        return this.idCodifie;
    }

    /**
     * Sets the idCodifie.
     *
     * @param idCodifie
     *            the new idCodifie
     */
    public void setIdCodifie(Integer idCodifie) {
        this.idCodifie = idCodifie;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    @Column(name = "fdo_code", unique = true, nullable = false, length = 30)
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    @Column(name = "fdo_libelle", nullable = false, length = 100)
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the new libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}

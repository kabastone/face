package fr.gouv.sitde.face.transverse.entities;
// Generated 28 janv. 2019 18:43:54 by Hibernate Tools 5.2.11.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * The Class DroitAgent.
 */
@Entity
@Table(name = "droit_agent", uniqueConstraints = @UniqueConstraint(columnNames = { "dag_uti_id", "dag_col_id" }))
@SequenceGenerator(name = "generatorDroitAgent", sequenceName = "droit_agent_dag_id_seq", allocationSize = 1)
public class DroitAgent extends TrackedEntity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6196635084107267629L;

    /** The id. */
    private Long id;

    /** The utilisateur. */
    private Utilisateur utilisateur;

    /** The admin. */
    private boolean admin;

    /** The collectivite. */
    private Collectivite collectivite;

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generatorDroitAgent")
    @Column(name = "dag_id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Checks if is admin.
     *
     * @return the admin
     */
    @Column(name = "dag_est_admin", nullable = false)
    public boolean isAdmin() {
        return this.admin;
    }

    /**
     * Sets the admin.
     *
     * @param admin
     *            the admin to set
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Gets the utilisateur.
     *
     * @return the utilisateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dag_uti_id", nullable = false)
    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    /**
     * Sets the utilisateur.
     *
     * @param utilisateur
     *            the utilisateur to set
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     * Gets the collectivite.
     *
     * @return the collectivite
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dag_col_id", nullable = false)
    public Collectivite getCollectivite() {
        return this.collectivite;
    }

    /**
     * Sets the collectivite.
     *
     * @param collectivite
     *            the collectivite to set
     */
    public void setCollectivite(Collectivite collectivite) {
        this.collectivite = collectivite;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.transverse.tableaudebord;

import java.io.Serializable;

/**
 * The Class TableauDeBordMferDto.
 *
 * @author a453029
 */
public class TableauDeBordMferDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3555683078141963341L;

    /** The nb aqualifier subvention. */
    private long nbAqualifierSubvention;

    /** The nb aqualifier paiement. */
    private long nbAqualifierPaiement;

    /** The nb aaccorder subvention. */
    private long nbAaccorderSubvention;

    /** The nb aaccorder paiement. */
    private long nbAaccorderPaiement;

    /** The nb aattribuer subvention. */
    private long nbAattribuerSubvention;

    /** The nb asignaler subvention. */
    private long nbAsignalerSubvention;

    /** The nb asignaler paiement. */
    private long nbAsignalerPaiement;

    /** The nb aprolonger subvention. */
    private long nbAprolongerSubvention;

    /**
     * Instantiates a new tableau de bord mfer dto.
     */
    public TableauDeBordMferDto() {
        // Constructeur non paramétré.
    }

    /**
     * Instantiates a new tableau de bord mfer dto.
     *
     * @param nbAqualifierSubvention
     *            the nb aqualifier subvention
     * @param nbAqualifierPaiement
     *            the nb aqualifier paiement
     * @param nbAaccorderSubvention
     *            the nb aaccorder subvention
     * @param nbAaccorderPaiement
     *            the nb aaccorder paiement
     * @param nbAattribuerSubvention
     *            the nb aattribuer subvention
     * @param nbAsignalerSubvention
     *            the nb asignaler subvention
     * @param nbAsignalerPaiement
     *            the nb asignaler paiement
     * @param nbAprolongerSubvention
     *            the nb aprolonger subvention
     */
    public TableauDeBordMferDto(long nbAqualifierSubvention, long nbAqualifierPaiement, long nbAaccorderSubvention, long nbAaccorderPaiement,
            long nbAattribuerSubvention, long nbAsignalerSubvention, long nbAsignalerPaiement, long nbAprolongerSubvention) {
        super();
        this.nbAqualifierSubvention = nbAqualifierSubvention;
        this.nbAqualifierPaiement = nbAqualifierPaiement;
        this.nbAaccorderSubvention = nbAaccorderSubvention;
        this.nbAaccorderPaiement = nbAaccorderPaiement;
        this.nbAattribuerSubvention = nbAattribuerSubvention;
        this.nbAsignalerSubvention = nbAsignalerSubvention;
        this.nbAsignalerPaiement = nbAsignalerPaiement;
        this.nbAprolongerSubvention = nbAprolongerSubvention;
    }

    /**
     * Gets the nb aqualifier subvention.
     *
     * @return the nbAqualifierSubvention
     */
    public long getNbAqualifierSubvention() {
        return this.nbAqualifierSubvention;
    }

    /**
     * Sets the nb aqualifier subvention.
     *
     * @param nbAqualifierSubvention
     *            the nbAqualifierSubvention to set
     */
    public void setNbAqualifierSubvention(long nbAqualifierSubvention) {
        this.nbAqualifierSubvention = nbAqualifierSubvention;
    }

    /**
     * Gets the nb aqualifier paiement.
     *
     * @return the nbAqualifierPaiement
     */
    public long getNbAqualifierPaiement() {
        return this.nbAqualifierPaiement;
    }

    /**
     * Sets the nb aqualifier paiement.
     *
     * @param nbAqualifierPaiement
     *            the nbAqualifierPaiement to set
     */
    public void setNbAqualifierPaiement(long nbAqualifierPaiement) {
        this.nbAqualifierPaiement = nbAqualifierPaiement;
    }

    /**
     * Gets the nb aaccorder subvention.
     *
     * @return the nbAaccorderSubvention
     */
    public long getNbAaccorderSubvention() {
        return this.nbAaccorderSubvention;
    }

    /**
     * Sets the nb aaccorder subvention.
     *
     * @param nbAaccorderSubvention
     *            the nbAaccorderSubvention to set
     */
    public void setNbAaccorderSubvention(long nbAaccorderSubvention) {
        this.nbAaccorderSubvention = nbAaccorderSubvention;
    }

    /**
     * Gets the nb aaccorder paiement.
     *
     * @return the nbAaccorderPaiement
     */
    public long getNbAaccorderPaiement() {
        return this.nbAaccorderPaiement;
    }

    /**
     * Sets the nb aaccorder paiement.
     *
     * @param nbAaccorderPaiement
     *            the nbAaccorderPaiement to set
     */
    public void setNbAaccorderPaiement(long nbAaccorderPaiement) {
        this.nbAaccorderPaiement = nbAaccorderPaiement;
    }

    /**
     * Gets the nb aattribuer subvention.
     *
     * @return the nbAattribuerSubvention
     */
    public long getNbAattribuerSubvention() {
        return this.nbAattribuerSubvention;
    }

    /**
     * Sets the nb aattribuer subvention.
     *
     * @param nbAattribuerSubvention
     *            the nbAattribuerSubvention to set
     */
    public void setNbAattribuerSubvention(long nbAattribuerSubvention) {
        this.nbAattribuerSubvention = nbAattribuerSubvention;
    }

    /**
     * Gets the nb asignaler subvention.
     *
     * @return the nbAsignalerSubvention
     */
    public long getNbAsignalerSubvention() {
        return this.nbAsignalerSubvention;
    }

    /**
     * Sets the nb asignaler subvention.
     *
     * @param nbAsignalerSubvention
     *            the nbAsignalerSubvention to set
     */
    public void setNbAsignalerSubvention(long nbAsignalerSubvention) {
        this.nbAsignalerSubvention = nbAsignalerSubvention;
    }

    /**
     * Gets the nb asignaler paiement.
     *
     * @return the nbAsignalerPaiement
     */
    public long getNbAsignalerPaiement() {
        return this.nbAsignalerPaiement;
    }

    /**
     * Sets the nb asignaler paiement.
     *
     * @param nbAsignalerPaiement
     *            the nbAsignalerPaiement to set
     */
    public void setNbAsignalerPaiement(long nbAsignalerPaiement) {
        this.nbAsignalerPaiement = nbAsignalerPaiement;
    }

    /**
     * Gets the nb aprolonger subvention.
     *
     * @return the nbAprolongerSubvention
     */
    public long getNbAprolongerSubvention() {
        return this.nbAprolongerSubvention;
    }

    /**
     * Sets the nb aprolonger subvention.
     *
     * @param nbAprolongerSubvention
     *            the nbAprolongerSubvention to set
     */
    public void setNbAprolongerSubvention(long nbAprolongerSubvention) {
        this.nbAprolongerSubvention = nbAprolongerSubvention;
    }

}

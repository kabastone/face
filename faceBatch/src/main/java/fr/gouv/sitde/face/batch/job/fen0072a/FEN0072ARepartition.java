/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0072a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.paiement.DirecteurRepartitionPaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Class FEN0072ARepartition.
 */
public class FEN0072ARepartition implements ItemWriter<DemandePaiement> {

    @Inject
    private DirecteurRepartitionPaiementService directeurRepartitionPaiement;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends DemandePaiement> items) throws Exception {
        for (DemandePaiement demande : items) {
            this.directeurRepartitionPaiement.realiserRepartition(demande.getId());
        }
    }

}

package fr.gouv.sitde.face.batch.job.pertedotation;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class PerteDotationBatchConfiguration {

	/** The chunk size. */
	private static final int CHUNK_SIZE = 1;

	/** The step builder factory. */
	@Inject
	private StepBuilderFactory stepBuilderFactory;

	/** The job builder factory. */
	@Inject
	private JobBuilderFactory jobBuilderFactor;

	/** The dotation collectivite service. */
	@Inject
	private DotationCollectiviteService dotationCollectiviteService;

	/**
	 * Perte dotation reader.
	 *
	 * @return the list item reader
	 */
	@Bean
	@StepScope
	ListItemReader<DotationCollectivite> perteDotationReader() {
		List<DotationCollectivite> listeDotationsCollectivite = dotationCollectiviteService
				.rechercherPerteDotationCollectivitePourBatch();

		return new ListItemReader<>(listeDotationsCollectivite);

	}

	/**
	 * Processor.
	 *
	 * @return the perte dotation processor
	 */
	@Bean
	@StepScope
	PerteDotationProcessor perteDotationProcessor() {
		return new PerteDotationProcessor();
	}

	/**
	 * Perte dotation writer.
	 *
	 * @return the perte dotation writer
	 */
	@Bean
	@StepScope
	PerteDotationWriter perteDotationWriter() {
		return new PerteDotationWriter();
	}

	/**
	 * Step perte dotation.
	 *
	 * @return the step
	 */
	@Bean
	@JobScope
	public Step stepPerteDotation() {
		return this.stepBuilderFactory.get("step_perte_dotation")
				.<DotationCollectivite, PerteDotationMajDto>chunk(PerteDotationBatchConfiguration.CHUNK_SIZE)
				.reader(this.perteDotationReader()).processor(this.perteDotationProcessor())
				.writer(this.perteDotationWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
	}

	/**
	 * Job.
	 *
	 * @return the job
	 */
	@Bean(name = JobEnum.Constantes.PERTE_DOTATION_JOB)
	public Job job() {
		return this.jobBuilderFactor.get(JobEnum.PERTE_DOTATION_JOB.getNom()).start(this.stepPerteDotation()).build();

	}
}

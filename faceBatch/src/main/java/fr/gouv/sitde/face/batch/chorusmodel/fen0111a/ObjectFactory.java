
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.sitde.face.batch.job.fen0111a.model package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.sitde.face.batch.job.fen0111a.model
     *
     */
    public ObjectFactory() {
        // Methode générée et inutilisée.
    }

    /**
     * Create an instance of {@link EngagementsJuridiques }
     *
     */
    public EngagementsJuridiques createEngagementsJuridiques() {
        return new EngagementsJuridiques();
    }

    /**
     * Create an instance of {@link EngagementJuridiqueType }
     *
     */
    public EngagementJuridiqueType createEngagementJuridiqueType() {
        return new EngagementJuridiqueType();
    }

    /**
     * Create an instance of {@link EnteteType }
     *
     */
    public EnteteType createEnteteType() {
        return new EnteteType();
    }

    /**
     * Create an instance of {@link PartenaireType }
     *
     */
    public PartenaireType createPartenaireType() {
        return new PartenaireType();
    }

    /**
     * Create an instance of {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete createPartenaireTypeEntete() {
        return new PartenaireTypeEntete();
    }

    /**
     * Create an instance of {@link EJPartenaireEnteteType }
     *
     */
    public EJPartenaireEnteteType createEJPartenaireEnteteType() {
        return new EJPartenaireEnteteType();
    }

    /**
     * Create an instance of {@link ImputationType }
     *
     */
    public ImputationType createImputationType() {
        return new ImputationType();
    }

    /**
     * Create an instance of {@link LignePosteType }
     *
     */
    public LignePosteType createLignePosteType() {
        return new LignePosteType();
    }

    /**
     * Create an instance of {@link FicheRecensementType }
     *
     */
    public FicheRecensementType createFicheRecensementType() {
        return new FicheRecensementType();
    }

    /**
     * Create an instance of {@link PJType }
     *
     */
    public PJType createPJType() {
        return new PJType();
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0111a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * The Class FEN0111ABatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class FEN0111ABatchConfiguration {

    /** The chunk size. */
    private int chunkSize = 1;

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /**
     * Fen 0111 a reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<DemandeSubvention> fen0111aReader() {
        List<DemandeSubvention> listeDemandes = this.demandeSubventionService.obtenirDtoPourBatchFen0111A();
        return new ListItemReader<>(listeDemandes);
    }

    /**
     * Fen 0111 a processor.
     *
     * @return the FEN 0111 A processor
     */
    @Bean
    @StepScope
    public FEN0111AProcessor fen0111aProcessor() {
        return new FEN0111AProcessor();
    }

    /**
     * Fen 0111 a writer.
     *
     * @return the FEN 0111 A writer
     */
    @Bean
    @StepScope
    public FEN0111AWriter fen0111aWriter() {
        return new FEN0111AWriter(this.batchCheminRepertoireProperties.getOutput());
    }

    /**
     * Step FEN 0159 A output.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepFEN0111AOutput() {
        return this.stepBuilderFactory.get("step_fen_0111a").<DemandeSubvention, FEN0111ADto> chunk(this.chunkSize).reader(this.fen0111aReader())
                .processor(this.fen0111aProcessor()).writer(this.fen0111aWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FEN_0111A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FEN_0111A_JOB.getNom()).start(this.stepFEN0111AOutput()).build();
    }
}

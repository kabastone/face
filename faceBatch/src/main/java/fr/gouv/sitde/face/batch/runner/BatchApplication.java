/**
 *
 */
package fr.gouv.sitde.face.batch.runner;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class BatchApplication.
 */
@SpringBootApplication
@EnableJpaRepositories("fr.gouv.sitde.face.persistance")
@EnableAspectJAutoProxy
@EntityScan("fr.gouv.sitde.face.transverse.entities")
@ComponentScan(basePackages = "fr.gouv.sitde.face")
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
@Profile({ "prod", "dev" })
public class BatchApplication implements CommandLineRunner {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchApplication.class);

    /** The Constant MDC_CLE_JOB. */
    public static final String MDC_CLE_JOB = "job";

    /** The job launcher. */
    @Inject
    private JobLauncher jobLauncher;

    /** The context. */
    @Inject
    private ApplicationContext context;

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(BatchApplication.class, args);

        // Le context doit être fermé dans tous les cas (arrêt du batch)
        context.close();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
     */
    @Override
    public void run(String... args) throws Exception {

        String strArgs = Arrays.stream(args).collect(Collectors.joining("|"));
        LOGGER.info("Application demarrée avec les arguments : {}", strArgs);

        // Vérification du nom du job qui doit être passé en argument.
        String nomJob = args[0];

        if (StringUtils.isEmpty(nomJob)) {
            String msg = "Il est obligatoire de fournir le nom du job en argument";
            LOGGER.error(msg);
            throw new TechniqueException(msg);
        }

        if (!JobEnum.jobExists(nomJob)) {
            String msg = "Le nom du job " + nomJob + " n'existe pas";
            LOGGER.error(msg);
            throw new TechniqueException(msg);
        }

        // On ajoute le nom du job dans le MDC
        MDC.put(BatchApplication.MDC_CLE_JOB, nomJob);
        Job job = this.context.getBean(nomJob, Job.class);

        // Lancement du job
        JobParameters params = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis())).toJobParameters();
        this.jobLauncher.run(job, params);
        LOGGER.info("Fin du job {}", nomJob);
    }
}

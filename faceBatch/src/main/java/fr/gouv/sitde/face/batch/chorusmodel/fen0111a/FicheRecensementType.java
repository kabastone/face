
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.gouv.sitde.face.batch.chorusmodel.adapter.CustomLocalDateTimeAdapter;

/**
 * <p>
 * Classe Java pour FicheRecensementType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="FicheRecensementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DateNotification" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="DateFinMarche" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="DateDebPresta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="DateFinPresta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="SIRETAcheteur" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="14"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TypeMarche" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}TypeMarcheType" minOccurs="0"/&gt;
 *         &lt;element name="Procedure" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}ProcedureType" minOccurs="0"/&gt;
 *         &lt;element name="SousTraitance" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CodeCCAG" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}CCAGType" minOccurs="0"/&gt;
 *         &lt;element name="RefAccordCadre" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CPVPrincipal" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{8}-[0-9]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CPVSecond1" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{8}-[0-9]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CPVSecond2" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{8}-[0-9]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CPVSecond3" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{8}-[0-9]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NbEntrCotrait" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;maxInclusive value="999"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CarteAchat" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ClauseSociale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="NbTotalProp" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;maxInclusive value="999"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NbTotalPropDemat" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;maxInclusive value="999"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ClauseEnvironnementale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="FormePrix" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reconduction" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateReconduction" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="PenaliteRetard" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PourcentageRetenue" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;totalDigits value="7"/&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *               &lt;minExclusive value="0"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CodeRetenue" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FicheRecensementType",
        propOrder = { "dateNotification", "dateFinMarche", "dateDebPresta", "dateFinPresta", "siretAcheteur", "typeMarche", "procedure",
                "sousTraitance", "codeCCAG", "refAccordCadre", "cpvPrincipal", "cpvSecond1", "cpvSecond2", "cpvSecond3", "nbEntrCotrait",
                "carteAchat", "clauseSociale", "nbTotalProp", "nbTotalPropDemat", "clauseEnvironnementale", "formePrix", "reconduction",
                "dateReconduction", "penaliteRetard", "pourcentageRetenue", "codeRetenue" })
public class FicheRecensementType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "DateNotification", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateNotification;
    @XmlElement(name = "DateFinMarche", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateFinMarche;
    @XmlElement(name = "DateDebPresta", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateDebPresta;
    @XmlElement(name = "DateFinPresta", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateFinPresta;
    @XmlElement(name = "SIRETAcheteur")
    private String siretAcheteur;
    @XmlElement(name = "TypeMarche")
    private String typeMarche;
    @XmlElement(name = "Procedure")
    private String procedure;
    @XmlElement(name = "SousTraitance")
    private String sousTraitance;
    @XmlElement(name = "CodeCCAG")
    private String codeCCAG;
    @XmlElement(name = "RefAccordCadre")
    private String refAccordCadre;
    @XmlElement(name = "CPVPrincipal")
    private String cpvPrincipal;
    @XmlElement(name = "CPVSecond1")
    private String cpvSecond1;
    @XmlElement(name = "CPVSecond2")
    private String cpvSecond2;
    @XmlElement(name = "CPVSecond3")
    private String cpvSecond3;
    @XmlElement(name = "NbEntrCotrait")
    private Integer nbEntrCotrait;
    @XmlElement(name = "CarteAchat")
    private Boolean carteAchat;
    @XmlElement(name = "ClauseSociale")
    private Boolean clauseSociale;
    @XmlElement(name = "NbTotalProp")
    private Integer nbTotalProp;
    @XmlElement(name = "NbTotalPropDemat")
    private Integer nbTotalPropDemat;
    @XmlElement(name = "ClauseEnvironnementale")
    private Boolean clauseEnvironnementale;
    @XmlElement(name = "FormePrix")
    private String formePrix;
    @XmlElement(name = "Reconduction")
    private String reconduction;
    @XmlElement(name = "DateReconduction", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateReconduction;
    @XmlElement(name = "PenaliteRetard")
    private String penaliteRetard;
    @XmlElement(name = "PourcentageRetenue")
    private BigDecimal pourcentageRetenue;
    @XmlElement(name = "CodeRetenue")
    private String codeRetenue;

    /**
     * Obtient la valeur de la propriété dateNotification.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateNotification() {
        return this.dateNotification;
    }

    /**
     * Définit la valeur de la propriété dateNotification.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateNotification(LocalDateTime value) {
        this.dateNotification = value;
    }

    /**
     * Obtient la valeur de la propriété dateFinMarche.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateFinMarche() {
        return this.dateFinMarche;
    }

    /**
     * Définit la valeur de la propriété dateFinMarche.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateFinMarche(LocalDateTime value) {
        this.dateFinMarche = value;
    }

    /**
     * Obtient la valeur de la propriété dateDebPresta.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateDebPresta() {
        return this.dateDebPresta;
    }

    /**
     * Définit la valeur de la propriété dateDebPresta.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateDebPresta(LocalDateTime value) {
        this.dateDebPresta = value;
    }

    /**
     * Obtient la valeur de la propriété dateFinPresta.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateFinPresta() {
        return this.dateFinPresta;
    }

    /**
     * Définit la valeur de la propriété dateFinPresta.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateFinPresta(LocalDateTime value) {
        this.dateFinPresta = value;
    }

    /**
     * Obtient la valeur de la propriété siretAcheteur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSIRETAcheteur() {
        return this.siretAcheteur;
    }

    /**
     * Définit la valeur de la propriété siretAcheteur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setSIRETAcheteur(String value) {
        this.siretAcheteur = value;
    }

    /**
     * Obtient la valeur de la propriété typeMarche.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTypeMarche() {
        return this.typeMarche;
    }

    /**
     * Définit la valeur de la propriété typeMarche.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setTypeMarche(String value) {
        this.typeMarche = value;
    }

    /**
     * Obtient la valeur de la propriété procedure.
     *
     * @return possible object is {@link String }
     *
     */
    public String getProcedure() {
        return this.procedure;
    }

    /**
     * Définit la valeur de la propriété procedure.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setProcedure(String value) {
        this.procedure = value;
    }

    /**
     * Obtient la valeur de la propriété sousTraitance.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSousTraitance() {
        return this.sousTraitance;
    }

    /**
     * Définit la valeur de la propriété sousTraitance.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setSousTraitance(String value) {
        this.sousTraitance = value;
    }

    /**
     * Obtient la valeur de la propriété codeCCAG.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeCCAG() {
        return this.codeCCAG;
    }

    /**
     * Définit la valeur de la propriété codeCCAG.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeCCAG(String value) {
        this.codeCCAG = value;
    }

    /**
     * Obtient la valeur de la propriété refAccordCadre.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRefAccordCadre() {
        return this.refAccordCadre;
    }

    /**
     * Définit la valeur de la propriété refAccordCadre.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setRefAccordCadre(String value) {
        this.refAccordCadre = value;
    }

    /**
     * Obtient la valeur de la propriété cpvPrincipal.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCPVPrincipal() {
        return this.cpvPrincipal;
    }

    /**
     * Définit la valeur de la propriété cpvPrincipal.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCPVPrincipal(String value) {
        this.cpvPrincipal = value;
    }

    /**
     * Obtient la valeur de la propriété cpvSecond1.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCPVSecond1() {
        return this.cpvSecond1;
    }

    /**
     * Définit la valeur de la propriété cpvSecond1.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCPVSecond1(String value) {
        this.cpvSecond1 = value;
    }

    /**
     * Obtient la valeur de la propriété cpvSecond2.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCPVSecond2() {
        return this.cpvSecond2;
    }

    /**
     * Définit la valeur de la propriété cpvSecond2.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCPVSecond2(String value) {
        this.cpvSecond2 = value;
    }

    /**
     * Obtient la valeur de la propriété cpvSecond3.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCPVSecond3() {
        return this.cpvSecond3;
    }

    /**
     * Définit la valeur de la propriété cpvSecond3.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCPVSecond3(String value) {
        this.cpvSecond3 = value;
    }

    /**
     * Obtient la valeur de la propriété nbEntrCotrait.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getNbEntrCotrait() {
        return this.nbEntrCotrait;
    }

    /**
     * Définit la valeur de la propriété nbEntrCotrait.
     *
     * @param value
     *            allowed object is {@link Integer }
     *
     */
    public void setNbEntrCotrait(Integer value) {
        this.nbEntrCotrait = value;
    }

    /**
     * Obtient la valeur de la propriété carteAchat.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isCarteAchat() {
        return this.carteAchat;
    }

    /**
     * Définit la valeur de la propriété carteAchat.
     *
     * @param value
     *            allowed object is {@link Boolean }
     *
     */
    public void setCarteAchat(Boolean value) {
        this.carteAchat = value;
    }

    /**
     * Obtient la valeur de la propriété clauseSociale.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isClauseSociale() {
        return this.clauseSociale;
    }

    /**
     * Définit la valeur de la propriété clauseSociale.
     *
     * @param value
     *            allowed object is {@link Boolean }
     *
     */
    public void setClauseSociale(Boolean value) {
        this.clauseSociale = value;
    }

    /**
     * Obtient la valeur de la propriété nbTotalProp.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getNbTotalProp() {
        return this.nbTotalProp;
    }

    /**
     * Définit la valeur de la propriété nbTotalProp.
     *
     * @param value
     *            allowed object is {@link Integer }
     *
     */
    public void setNbTotalProp(Integer value) {
        this.nbTotalProp = value;
    }

    /**
     * Obtient la valeur de la propriété nbTotalPropDemat.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getNbTotalPropDemat() {
        return this.nbTotalPropDemat;
    }

    /**
     * Définit la valeur de la propriété nbTotalPropDemat.
     *
     * @param value
     *            allowed object is {@link Integer }
     *
     */
    public void setNbTotalPropDemat(Integer value) {
        this.nbTotalPropDemat = value;
    }

    /**
     * Obtient la valeur de la propriété clauseEnvironnementale.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isClauseEnvironnementale() {
        return this.clauseEnvironnementale;
    }

    /**
     * Définit la valeur de la propriété clauseEnvironnementale.
     *
     * @param value
     *            allowed object is {@link Boolean }
     *
     */
    public void setClauseEnvironnementale(Boolean value) {
        this.clauseEnvironnementale = value;
    }

    /**
     * Obtient la valeur de la propriété formePrix.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFormePrix() {
        return this.formePrix;
    }

    /**
     * Définit la valeur de la propriété formePrix.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setFormePrix(String value) {
        this.formePrix = value;
    }

    /**
     * Obtient la valeur de la propriété reconduction.
     *
     * @return possible object is {@link String }
     *
     */
    public String getReconduction() {
        return this.reconduction;
    }

    /**
     * Définit la valeur de la propriété reconduction.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setReconduction(String value) {
        this.reconduction = value;
    }

    /**
     * Obtient la valeur de la propriété dateReconduction.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateReconduction() {
        return this.dateReconduction;
    }

    /**
     * Définit la valeur de la propriété dateReconduction.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateReconduction(LocalDateTime value) {
        this.dateReconduction = value;
    }

    /**
     * Obtient la valeur de la propriété penaliteRetard.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPenaliteRetard() {
        return this.penaliteRetard;
    }

    /**
     * Définit la valeur de la propriété penaliteRetard.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setPenaliteRetard(String value) {
        this.penaliteRetard = value;
    }

    /**
     * Obtient la valeur de la propriété pourcentageRetenue.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getPourcentageRetenue() {
        return this.pourcentageRetenue;
    }

    /**
     * Définit la valeur de la propriété pourcentageRetenue.
     *
     * @param value
     *            allowed object is {@link BigDecimal }
     *
     */
    public void setPourcentageRetenue(BigDecimal value) {
        this.pourcentageRetenue = value;
    }

    /**
     * Obtient la valeur de la propriété codeRetenue.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeRetenue() {
        return this.codeRetenue;
    }

    /**
     * Définit la valeur de la propriété codeRetenue.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeRetenue(String value) {
        this.codeRetenue = value;
    }

}

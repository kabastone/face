/**
 *
 */
package fr.gouv.sitde.face.batch.commun;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Suppression de dossier et des fichiers contenus.
 *
 * @author Atos
 */
public class SupressionDossierTasklet implements Tasklet {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SupressionDossierTasklet.class);

    /** The resources. */
    private Path directoryPath;

    /**
     * Execute.
     *
     * @param contribution
     *            the contribution
     * @param chunkContext
     *            the chunk context
     * @return the repeat status
     * @throws Exception
     *             the exception
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        try {
            Files.delete(this.directoryPath);
        } catch (IOException e) {
            throw new UnexpectedJobExecutionException("Impossible de supprimer le dossier " + this.directoryPath, e);
        }

        LOGGER.debug("Dossier {} supprimé", this.directoryPath);
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the directory path.
     *
     * @param directoryPath
     *            the new directory path
     */
    public void setDirectoryPath(Path directoryPath) {
        this.directoryPath = directoryPath;
    }

}

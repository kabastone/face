/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0072a;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.Dossier;
import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.Entete;
import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.Fen0072A;
import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.LignePoste;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class FEN0072AProcessor.
 */
public class FEN0072AProcessor implements ItemProcessor<List<DemandePaiement>, Fen0072A> {

    /** The formatter. */
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE_CHORUS);

    @Inject
    private DemandePaiementService demandePaiementService;

    /** The transfert paiement service. */
    @Inject
    private TransfertPaiementService transfertPaiementService;

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public Fen0072A process(List<DemandePaiement> readItem) throws Exception {

        if (readItem.isEmpty()) {
            return null;
        }

        // Récupération du référentiel.
        FluxChorus referentiel = this.fluxChorusService.rechercherFluxChorus();

        // Initialisation de la liste des dossiers.
        List<Dossier> listeDossier = new ArrayList<>(readItem.size());

        // Itération sur chaque demande de paiement obtenues par le reader.
        for (DemandePaiement demandePaiement : readItem) {
            // Mise à jour de la demande de paiement.
            this.demandePaiementService.majDemandePaiementEnCoursTransfertEtapeUne(demandePaiement.getId());

            // Mise en forme de l'entete du fichier.
            Entete entete = new Entete();
            entete.setCodeApplication(referentiel.getCodeApplication());
            entete.setCodeMouvement(referentiel.getCodeMvt());
            entete.setDateComptable(demandePaiement.getDateDemande().format(this.formatter));
            entete.setDateDocument(demandePaiement.getDateDemande().format(this.formatter));
            entete.setIdAE(demandePaiement.getChorusIdAe().toString());
            entete.setPrUname(referentiel.getPrUname());
            List<LignePoste> listeLigne;

            // Mise en forme des lignes poste du fichier.
            if (demandePaiement.getTransfertsPaiement() == null) {
                listeLigne = null;
            } else {
                listeLigne = new ArrayList<>(demandePaiement.getTransfertsPaiement().size());

                demandePaiement.getTransfertsPaiement().stream().sorted(Comparator.comparing(TransfertPaiement::getChorusNumLigneLegacy))
                        .forEach(trp -> {
                            LignePoste lignePoste = new LignePoste();
                            lignePoste.setMontant(this.transfertPaiementService.formatageChorusMontant(trp.getMontant()));
                            lignePoste.setIdAE(trp.getDemandePaiement().getChorusIdAe().toString());
                            lignePoste.setIdLigne(trp.getChorusNumLigneLegacy());
                            lignePoste.setNumCourtEJ(trp.getDemandePaiement().getDossierSubvention().getChorusNumEj());
                            // Le numéro legacy fait 10 caractères en base, on prend les 5 derniers caractères (les 5 premiers étant des 0)
                            lignePoste.setNumPosteChorus(StringUtils.truncate(trp.getDemandeSubvention().getChorusNumLigneLegacy(), 5, 5));

                            listeLigne.add(lignePoste);
                        });
            }
            // Création du dossier.
            Dossier dossier = new Dossier();
            dossier.setEntete(entete);
            dossier.setLignePoste(listeLigne);

            // Ajout du dossier à la liste.
            listeDossier.add(dossier);
        }

        // Création du FEN0072A et remplissage de la liste de dossier.
        Fen0072A fen = new Fen0072A();
        fen.setDossier(listeDossier);

        return fen;
    }

}


package fr.gouv.sitde.face.batch.chorusmodel.fen0072a;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.sitde.face.batch.job.fen0072a.model package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.sitde.face.batch.job.fen0072a.model
     *
     */
    public ObjectFactory() {
        // Source générée et non utilisée.
    }

    /**
     * Create an instance of {@link Fen0072A }
     *
     */
    public Fen0072A createFen0072A() {
        return new Fen0072A();
    }

    /**
     * Create an instance of {@link Dossier }
     *
     */
    public Dossier createDossier() {
        return new Dossier();
    }

    /**
     * Create an instance of {@link Entete }
     *
     */
    public Entete createEntete() {
        return new Entete();
    }

    /**
     * Create an instance of {@link LignePoste }
     *
     */
    public LignePoste createLignePoste() {
        return new LignePoste();
    }

}

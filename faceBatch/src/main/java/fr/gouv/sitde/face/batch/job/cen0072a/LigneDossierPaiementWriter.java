/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0072a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;

/**
 * The Class LigneDossierPaiementWriter.
 */
public class LigneDossierPaiementWriter implements ItemWriter<LigneDossierPaiementTraitementDto> {

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends LigneDossierPaiementTraitementDto> items) {

        for (LigneDossierPaiementTraitementDto ligneDossier : items) {
            if (ligneDossier.hasAnomalie()) {
                this.anomalieService.creerAnomalie(ligneDossier.getAnomalie());
            }

            this.demandePaiementService.majDemandePaiementBdd(ligneDossier.getDemandePaiement());
        }
    }

}

package fr.gouv.sitde.face.batch.job.pertedotation;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;

/**
 * The Class PerteDotationMajDto.
 */
public class PerteDotationMajDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8640804170601683753L;

    /** The dotation collectivite. */
    private DotationCollectivite dotationCollectivite;

    /** The ligne dotation departement. */
    private LigneDotationDepartement ligneDotationDepartement;

    /**
     * Gets the dotation collectivite.
     *
     * @return the dotation collectivite
     */
    public DotationCollectivite getDotationCollectivite() {
        return this.dotationCollectivite;
    }

    /**
     * Sets the dotation collectivite.
     *
     * @param dotationCollectivite
     *            the new dotation collectivite
     */
    public void setDotationCollectivite(DotationCollectivite dotationCollectivite) {
        this.dotationCollectivite = dotationCollectivite;
    }

    /**
     * Gets the ligne dotation departement.
     *
     * @return the ligne dotation departement
     */
    public LigneDotationDepartement getLigneDotationDepartement() {
        return this.ligneDotationDepartement;
    }

    /**
     * Sets the ligne dotation departement.
     *
     * @param ligneDotationDepartement
     *            the new ligne dotation departement
     */
    public void setLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement) {
        this.ligneDotationDepartement = ligneDotationDepartement;
    }

}

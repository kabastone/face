/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0159a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;

/**
 * The Class LigneDossierPaiementTransfertWriter.
 */
public class LigneDossierPaiementTransfertWriter implements ItemWriter<LigneDossierPaiementTransfertTraitementDto> {

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends LigneDossierPaiementTransfertTraitementDto> items) {

        for (LigneDossierPaiementTransfertTraitementDto ligneDossier : items) {
            if (ligneDossier.hasAnomalie()) {
                this.anomalieService.creerAnomalie(ligneDossier.getAnomalie());
            }

            this.demandePaiementService.majDemandePaiementBdd(ligneDossier.getDemandePaiement());
        }
    }

}


package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour PartenaireType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="PartenaireType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FSO0028A}Char12Type"/&gt;
 *         &lt;element name="PARTNER_FCT" type="{FSO0028A}Char8Type" minOccurs="0"/&gt;
 *         &lt;element name="PARTNER" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_FONC_TIERS" type="{FSO0028A}Char80Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_POSTE" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartenaireType", propOrder = { "idAe", "partnerFct", "partner", "idFoncTiers", "idPoste" })
public class PartenaireFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The id ae. */
    @XmlElement(name = "ID_AE", required = true)
    private String idAe;

    /** The partner fct. */
    @XmlElement(name = "PARTNER_FCT")
    private String partnerFct;

    /** The partner. */
    @XmlElement(name = "PARTNER")
    private String partner;

    /** The id fonc tiers. */
    @XmlElement(name = "ID_FONC_TIERS")
    private String idFoncTiers;

    /** The id poste. */
    @XmlElement(name = "ID_POSTE")
    private String idPoste;

    /**
     * Gets the id ae.
     *
     * @return the idAe
     */
    public String getIdAe() {
        return this.idAe;
    }

    /**
     * Sets the id ae.
     *
     * @param idAe
     *            the idAe to set
     */
    public void setIdAe(String idAe) {
        this.idAe = idAe;
    }

    /**
     * Gets the partner fct.
     *
     * @return the partnerFct
     */
    public String getPartnerFct() {
        return this.partnerFct;
    }

    /**
     * Sets the partner fct.
     *
     * @param partnerFct
     *            the partnerFct to set
     */
    public void setPartnerFct(String partnerFct) {
        this.partnerFct = partnerFct;
    }

    /**
     * Gets the partner.
     *
     * @return the partner
     */
    public String getPartner() {
        return this.partner;
    }

    /**
     * Sets the partner.
     *
     * @param partner
     *            the partner to set
     */
    public void setPartner(String partner) {
        this.partner = partner;
    }

    /**
     * Gets the id fonc tiers.
     *
     * @return the idFoncTiers
     */
    public String getIdFoncTiers() {
        return this.idFoncTiers;
    }

    /**
     * Sets the id fonc tiers.
     *
     * @param idFoncTiers
     *            the idFoncTiers to set
     */
    public void setIdFoncTiers(String idFoncTiers) {
        this.idFoncTiers = idFoncTiers;
    }

    /**
     * Gets the id poste.
     *
     * @return the idPoste
     */
    public String getIdPoste() {
        return this.idPoste;
    }

    /**
     * Sets the id poste.
     *
     * @param idPoste
     *            the idPoste to set
     */
    public void setIdPoste(String idPoste) {
        this.idPoste = idPoste;
    }

}

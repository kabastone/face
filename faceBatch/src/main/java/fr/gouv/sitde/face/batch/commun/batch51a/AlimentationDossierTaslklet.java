/**
 *
 */
package fr.gouv.sitde.face.batch.commun.batch51a;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.gouv.sitde.face.batch.commun.batch51a.model.SuiviBatch;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * Sert a deplacer les fichiers a traiter pour les batch 51A.
 *
 * @author Atos
 */
public class AlimentationDossierTaslklet implements Tasklet {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AlimentationDossierTaslklet.class);

    /** The source path. */
    private File inputDirectory;

    /** The travail path. */
    private File travailDirectory;

    /** The chorus suivi batch. */
    private SuiviBatch suiviBatch;

    /**
     * Execute.
     *
     * @param contribution
     *            the contribution
     * @param chunkContext
     *            the chunk context
     * @return the repeat status
     * @throws Exception
     *             the exception
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        // si le repertoire d'input du batch n'existe pas : exception
        if (!this.inputDirectory.exists()) {
            throw new TechniqueException("Problème de configuration : le répertoire d'input " + this.inputDirectory.getName() + " n'existe pas.");
        }

        // recupere la liste des fichiers en input
        File[] tableauFichiers = this.inputDirectory.listFiles();
        if (tableauFichiers == null) {
            throw new TechniqueException(
                    "Problème de configuration : le répertoire d'input " + this.inputDirectory.getName() + " n'est pas accessible.");
        }

        // on trie les fichiers sur les 5 derniers caracteres pour determiner quels sont ceux integrables
        Arrays.sort(tableauFichiers, (File file1, File file2) -> file1.getName().substring(file1.getName().length() - 5, file1.getName().length())
                .compareTo(file2.getName().substring(file2.getName().length() - 5, file2.getName().length())));

        // creation repertoire de travail
        try {
            if (!this.travailDirectory.toPath().toFile().exists()) {
                Files.createDirectory(this.travailDirectory.toPath());
            }
        } catch (IOException e) {
            throw new TechniqueException(
                    "Problème de configuration : impossible de créer le repertoire de travail " + this.travailDirectory.getName(), e);
        }

        int fileNumber;
        String fileName;
        Integer numeroAttendu = this.suiviBatch.getNumeroIntegration();

        for (File currentFile : tableauFichiers) {
            fileName = currentFile.getName();

            // on ne recupère que les 5 derniers charactères qui contiennent le numero
            try {
                fileNumber = Integer.parseInt(fileName.substring(fileName.length() - 5, fileName.length()));
            } catch (NumberFormatException e) {
                LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.fichier.mauvais.format", fileName));
                fileNumber = 0;
            }

            // si numero du fichier est celui attendu ( dernier intégré +1 )
            if ((this.suiviBatch.getNumeroIntegration() + 1) == fileNumber) {
                try {
                    // deplace le fichier dans le dossier de travail, on le renomme : on ne garde que le numero
                    Files.move(currentFile.toPath(), Paths.get(this.travailDirectory.getPath() + File.separator + fileNumber));

                    // on incremente pour traiter le suivant si il existe
                    this.suiviBatch.setNumeroIntegration(fileNumber);
                } catch (IOException e) {
                    LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.fichier.erreur.deplacement", fileName), e);
                }
            }
            // sinon on ne fait rien on passe au suivant(s)
        }

        // Si on n'a pas trouvé de fichier a traiter :erreur et on continue (on fait rien)
        if (numeroAttendu.equals(this.suiviBatch.getNumeroIntegration())) {
            Integer num = numeroAttendu + 1;
            LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.fichier.non.trouve", num.toString()));
        }

        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the input directory.
     *
     * @param inputDirectory
     *            the inputDirectory to set
     */
    public void setInputDirectory(File inputDirectory) {
        this.inputDirectory = inputDirectory;
    }

    /**
     * Sets the travail directory.
     *
     * @param travailDirectory
     *            the travailDirectory to set
     */
    public void setTravailDirectory(File travailDirectory) {
        this.travailDirectory = travailDirectory;
    }

    /**
     * Sets the suivi batch.
     *
     * @param suiviBatch
     *            the new suivi batch
     */
    public void setSuiviBatch(SuiviBatch suiviBatch) {
        this.suiviBatch = suiviBatch;
    }

}

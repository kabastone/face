/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051adp;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * @author Atos
 *
 */
public class DemandePaiementChorus {

    private TransfertPaiement transfertPaiement;

    private List<DemandePaiement> listDemandePaiement = new ArrayList<>(0);

    /**
     * @return the transfertPaiement
     */
    public TransfertPaiement getTransfertPaiement() {
        return this.transfertPaiement;
    }

    /**
     * @param transfertPaiement
     *            the transfertPaiement to set
     */
    public void setTransfertPaiement(TransfertPaiement transfertPaiement) {
        this.transfertPaiement = transfertPaiement;
    }

    /**
     * @return the listDemandePaiement
     */
    public List<DemandePaiement> getListDemandePaiement() {
        return this.listDemandePaiement;
    }

    public void addDemandePaiement(DemandePaiement demandePaiement) {
        this.listDemandePaiement.add(demandePaiement);
    }

}

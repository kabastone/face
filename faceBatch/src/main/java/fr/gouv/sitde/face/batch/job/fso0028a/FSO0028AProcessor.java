/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0028a;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.fso0028a.EngagementJuridiqueFSO0028A;
import fr.gouv.sitde.face.batch.chorusmodel.fso0028a.FSO0028A;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class FSO0028AProcessor.
 */
public class FSO0028AProcessor implements ItemProcessor<FSO0028A, FSO0028ADto> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FSO0028AProcessor.class);

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public FSO0028ADto process(FSO0028A item) throws Exception {
        List<DossierSubvention> listeDossier = new ArrayList<>(item.getEngagementJuridiqueFSO0028A().size());
        // On boucle sur la liste pour obtenir tout les DossierSubvention.
        for (EngagementJuridiqueFSO0028A engagement : item.getEngagementJuridiqueFSO0028A()) {

            // Récupération du dossier de subvention correspondant au FSO0028A courant.
            DossierSubvention dossier = this.dossierSubventionService
                    .rechercherDossierSubventionParIdChorusAe(Long.valueOf(engagement.getEntete().getIdAe()));

            // RG_CHO_111-01
            // Si le dossier est null on log.
            if (dossier == null) {
                LOGGER.error(MessageUtils.getMsgValidationMetier("batch.fso0028a.non.trouve", engagement.getEntete().getIdEj()));
            } else {
                // RG_CHO_111-02
                // Si le dossier est non null et que le numéro ej est null on update, sinon on ne change rien.
                if (dossier.getChorusNumEj() == null) {
                    dossier.setChorusNumEj(engagement.getEntete().getIdEj());
                    listeDossier.add(dossier);
                } else {
                    LOGGER.error(MessageUtils.getMsgValidationMetier("batch.fso0028a.aucun.impact", engagement.getEntete().getIdEj()));
                }
            }

        }
        return new FSO0028ADto(listeDossier);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0111a;

import java.io.Serializable;

import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EngagementsJuridiques;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * The Class FEN011ADTO.
 */
public class FEN0111ADto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The demande. */
    private DemandeSubvention demande;

    /** The engagements. */
    private EngagementsJuridiques engagements;

    /**
     * Instantiates a new fen0111adto.
     *
     * @param demande
     *            the demande
     * @param engagements
     *            the engagements
     */
    public FEN0111ADto(DemandeSubvention demande, EngagementsJuridiques engagements) {
        super();
        this.demande = demande;
        this.engagements = engagements;
    }

    /**
     * Gets the demande.
     *
     * @return the demande
     */
    public DemandeSubvention getDemande() {
        return this.demande;
    }

    /**
     * Sets the demande.
     *
     * @param demande
     *            the demande to set
     */
    public void setDemande(DemandeSubvention demande) {
        this.demande = demande;
    }

    /**
     * Gets the engagements.
     *
     * @return the engagements
     */
    public EngagementsJuridiques getEngagements() {
        return this.engagements;
    }

    /**
     * Sets the engagements.
     *
     * @param engagements
     *            the engagements to set
     */
    public void setEngagements(EngagementsJuridiques engagements) {
        this.engagements = engagements;
    }
}

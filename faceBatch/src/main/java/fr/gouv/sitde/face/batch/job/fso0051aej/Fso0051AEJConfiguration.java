/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051aej;

import java.io.File;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

import fr.gouv.sitde.face.batch.commun.AiguillageTasklet;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.commun.ResourcesUtils;
import fr.gouv.sitde.face.batch.commun.SuppressionFichiersTasklet;
import fr.gouv.sitde.face.batch.commun.SupressionDossierTasklet;
import fr.gouv.sitde.face.batch.commun.batch51a.AlimentationDossierTaslklet;
import fr.gouv.sitde.face.batch.commun.batch51a.MiseAJourCompteurTasklet;
import fr.gouv.sitde.face.batch.commun.batch51a.model.Batch51AEnum;
import fr.gouv.sitde.face.batch.commun.batch51a.model.SuiviBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Class Fso0051AEJConfiguration.
 *
 * @author Atos
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class Fso0051AEJConfiguration {

    /** The Constant FSO0051A_EJ_REGEXP. */
    private static final String FSO0051A_EJ_REGEXP = "^FSO0051A_[a-zA-Z0-9_]+EEJ\\w+";

    /** The Constant SOUS_REP_INPUT_FSO0051AEJ. */
    public static final String SOUS_REP_INPUT_FSO0051AEJ = "/fso0051aej";

    /** The sous dossier travail. */
    private static final String SOUS_DOSSIER_TRAVAIL = File.separator + "jobBatch51aEJ";

    /** The Constant UN. */
    public static final int UN = 1;

    /** The Constant ENO_SEQ_INDEX. */
    public static final int ENO_SEQ_INDEX = 2 - UN;

    /** The Constant BBP_PO_ID_INDEX. */
    public static final int BBP_PO_ID_INDEX = 4 - UN;

    /** The Constant BBP_PO_ITEM_INDEX. */
    public static final int BBP_PO_ITEM_INDEX = 5 - UN;

    /** The Constant QUANTITY_INDEX. */
    public static final int QUANTITY_INDEX = 41 - UN;

    /** The Constant EDP_DELDA_INDEX. */
    public static final int EDP_DELDA_INDEX = 70 - UN;

    /** The Constant PO_STATUS_INDEX. */
    public static final int PO_STATUS_INDEX = 71 - UN;

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The chorus suivi batch service. */
    @Inject
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /** The input global. */
    private File inputGlobal;

    /** The input file. */
    private File inputFile;

    /** The work file. */
    private File workFile;

    /** The chorus suivi batch. */
    private SuiviBatch suiviBatch;

    public File getInputGlobal() {
        if (this.inputGlobal == null) {
            this.inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        }
        return this.inputGlobal;
    }

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile() {
        if (this.inputFile == null) {
            this.inputFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0051AEJ);
        }
        return this.inputFile;
    }

    /**
     * Gets the work file.
     *
     * @return the work file
     */
    public File getWorkFile() {
        if (this.workFile == null) {
            this.workFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0051AEJ + SOUS_DOSSIER_TRAVAIL);
        }
        return this.workFile;
    }

    /**
     * Gets the ressources from work directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromInputDirectory() {
        File repertoireWorkJob = this.getInputFile();
        return ResourcesUtils.getRessourcesFromDirectory(repertoireWorkJob);
    }

    /**
     * Gets the ressources from input directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromWorkDirectory() {
        File repertoireWorkJob = this.getWorkFile();
        return ResourcesUtils.getRessourcesFromDirectory(repertoireWorkJob);
    }

    /**
     * Creates the line tokenizer.
     *
     * @return the line tokenizer
     */
    private static LineTokenizer createLineTokenizer() {
        DelimitedLineTokenizer dossierLineTokenizer = new DelimitedLineTokenizer();
        dossierLineTokenizer.setDelimiter("|");
        // ENO_SEQ | BBP_PO_ID | BBP_PO_ITEM | QUANTITY | EDP_DELDA | PO_STATUS
        // numc(20) | char(10) | numc(10) | QUAN(17) 13.3 | DATS(ddMMyyyy) | CHAR(1)
        dossierLineTokenizer.setNames("numeroSequence", "numeroEngagementJuridique", "posteDeDocument", "quantiteCommandee", "dateDeLivraisonDuPoste",
                "statutCloturePosteEJ");
        dossierLineTokenizer.setIncludedFields(ENO_SEQ_INDEX, BBP_PO_ID_INDEX, BBP_PO_ITEM_INDEX, QUANTITY_INDEX, EDP_DELDA_INDEX, PO_STATUS_INDEX);
        return dossierLineTokenizer;
    }

    /**
     * Creates the eengagment juridique dto line mapper.
     *
     * @return the field set mapper
     */
    private static FieldSetMapper<EngagementJuridiqueDto> createEengagmentJuridiqueDtoLineMapper() {
        BeanWrapperFieldSetMapper<EngagementJuridiqueDto> engagementJuridiqueMapper = new BeanWrapperFieldSetMapper<>();
        engagementJuridiqueMapper.setTargetType(EngagementJuridiqueDto.class);
        return engagementJuridiqueMapper;
    }

    /**
     * Creates the engagement juridique dto line mapper.
     *
     * @return the line mapper
     */
    private static LineMapper<EngagementJuridiqueDto> createEngagementJuridiqueDtoLineMapper() {
        DefaultLineMapper<EngagementJuridiqueDto> engagmentJuridiqueLineMapper = new DefaultLineMapper<>();

        LineTokenizer dossierLineTokenizer = createLineTokenizer();
        engagmentJuridiqueLineMapper.setLineTokenizer(dossierLineTokenizer);

        FieldSetMapper<EngagementJuridiqueDto> enagementJuridiqueMapper = createEengagmentJuridiqueDtoLineMapper();
        engagmentJuridiqueLineMapper.setFieldSetMapper(enagementJuridiqueMapper);

        return engagmentJuridiqueLineMapper;
    }

    /**
     * Engagement juridique FSO 0051 AEJ reader.
     *
     * @return the flat file item reader
     */
    @Bean
    @StepScope
    public FlatFileItemReader<EngagementJuridiqueDto> engagementJuridiqueFSO0051AEJReader() {
        FlatFileItemReader<EngagementJuridiqueDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setLinesToSkip(0);

        LineMapper<EngagementJuridiqueDto> engagmentJuridiqueLineMapper = createEngagementJuridiqueDtoLineMapper();
        flatFileItemReader.setLineMapper(engagmentJuridiqueLineMapper);
        return flatFileItemReader;
    }

    /**
     * Multi resource FSO 0051 AEJ reader.
     *
     * @return the multi resource item reader
     */
    @Bean
    @StepScope
    public MultiResourceItemReader<EngagementJuridiqueDto> multiResourceFSO0051AEJReader() {
        Resource[] tableauResources = this.getRessourcesFromWorkDirectory();
        MultiResourceItemReader<EngagementJuridiqueDto> resourceItemReader = new MultiResourceItemReader<>();
        resourceItemReader.setResources(tableauResources);
        // Pour chaque fichier, on délègue la lecture au dossierFlatFileReader
        resourceItemReader.setDelegate(this.engagementJuridiqueFSO0051AEJReader());
        return resourceItemReader;
    }

    /**
     * Step aiguillage FSO 0051 AEJ.
     *
     * Step 0 : on va chercher les fichiers propres au batch dans le repertoire global
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAiguillageFSO0051AEJ() {
        AiguillageTasklet tasklet = new AiguillageTasklet();
        tasklet.setInputDir(this.getInputGlobal());
        tasklet.setOutputDir(this.getInputFile());
        tasklet.setRegexp(FSO0051A_EJ_REGEXP);

        return this.stepBuilderFactory.get("step_aiguillage").tasklet(tasklet).build();
    }

    /**
     * Step alimentation dossier.
     *
     * Step 1 : on recherche les dossiers intégrables on les déplace dans le dossier de travail Le compteur ChorusSuiviBatch est mis a jour
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAlimentationDossierFSO0051AEJ() {
        this.suiviBatch = new SuiviBatch();
        this.suiviBatch.setInstance(Batch51AEnum.BATCH_EJ);

        AlimentationDossierTaslklet tasklet = new AlimentationDossierTaslklet();

        tasklet.setInputDirectory(this.getInputFile());
        tasklet.setTravailDirectory(this.getWorkFile());
        ChorusSuiviBatch chorusSuiviBatch = this.chorusSuiviBatchService.lireSuiviBatch();

        this.suiviBatch.setNumeroIntegration(chorusSuiviBatch.getFso0051aEj());

        tasklet.setSuiviBatch(this.suiviBatch);

        return this.stepBuilderFactory.get("step_alimentation_dossier").tasklet(tasklet).build();
    }

    /**
     * Step Engagement Juridique.
     *
     * Step 2 : lecture et traitement des lignes : read, process, write
     *
     * <p>
     * <b>Ce step définit des chunks de 1</b> (commit/rollback pour chaque item traité par le writer).<br/>
     * <br/>
     * <b>Une exception n'est pas bloquante :</b>
     * <ul>
     * <li>Un rollback est effectué si l'exception a été lancée par le writer</li>
     * <li>Le traitement continue pour le prochain item.</li>
     * </ul>
     * </p>
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEngagementJuridiqueFSO0051AEJ() {
        return this.stepBuilderFactory.get("step_engagement_juridique").<EngagementJuridiqueDto, EngagementJuridiqueChorus> chunk(1)
                .reader(this.multiResourceFSO0051AEJReader()).processor(this.engagementJuridiquetProcessorFSO0051AEJ())
                .writer(this.engagementJuridiqueWriterFSO0051AEJ()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Step maj compteur.
     *
     * Step 3 : une fois les fichiers traités on met a jour ChorusSuiviBatch dans la base
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepMajCompteurFSO0051AEJ() {
        MiseAJourCompteurTasklet tasklet = new MiseAJourCompteurTasklet();
        tasklet.setSuiviBatch(this.suiviBatch);
        tasklet.setChorusSuiviBatchService(this.chorusSuiviBatchService);
        return this.stepBuilderFactory.get("step_maj_compteur").tasklet(tasklet).build();
    }

    /**
     * Engagement juridiquet processor FSO 0051 AEJ.
     *
     * @return the engagement juridique processor
     */
    @Bean
    @StepScope
    public EngagementJuridiqueProcessor engagementJuridiquetProcessorFSO0051AEJ() {
        return new EngagementJuridiqueProcessor();
    }

    /**
     * Engagement juridique writer FSO 0051 AEJ.
     *
     * @return the engagement juridique writer
     */
    @Bean
    @StepScope
    public EngagementJuridiqueWriter engagementJuridiqueWriterFSO0051AEJ() {
        return new EngagementJuridiqueWriter();
    }

    /**
     * Step supression dossier working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSupressionDossierWorkFSO0051AEJ() {
        SupressionDossierTasklet tasklet = new SupressionDossierTasklet();
        tasklet.setDirectoryPath(this.getWorkFile().toPath());
        return this.stepBuilderFactory.get("step_supression_dossier_work").tasklet(tasklet).build();
    }

    /**
     * Step suppression fichiers working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersWorkFSO0051AEJ() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromWorkDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers_work").tasklet(tasklet).build();
    }

    /**
     * Step suppression fichiers working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersInputFSO0051AEJ() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromInputDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers_input").tasklet(tasklet).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FSO0051A_EJ_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FSO0051A_EJ_JOB.getNom()).start(this.stepAiguillageFSO0051AEJ())
                .next(this.stepAlimentationDossierFSO0051AEJ()).next(this.stepEngagementJuridiqueFSO0051AEJ()).next(this.stepMajCompteurFSO0051AEJ())
                .next(this.stepSuppressionFichiersWorkFSO0051AEJ()).next(this.stepSupressionDossierWorkFSO0051AEJ())
                .next(this.stepSuppressionFichiersInputFSO0051AEJ()).build();
    }
}

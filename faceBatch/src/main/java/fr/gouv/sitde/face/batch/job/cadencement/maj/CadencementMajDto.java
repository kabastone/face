/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.maj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * @author A754839
 *
 */
public class CadencementMajDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3100932727436021769L;

    private List<Cadencement> cadencements = new ArrayList<>();
    private Collectivite collectivite;

    /**
     * @return the cadencements
     */
    public List<Cadencement> getCadencements() {
        return this.cadencements;
    }

    /**
     * @param cadencements
     *            the cadencements to set
     */
    public void setCadencements(List<Cadencement> cadencements) {
        this.cadencements = cadencements;
    }

    /**
     * @return the collectivite
     */
    public Collectivite getCollectivite() {
        return this.collectivite;
    }

    /**
     * @param collectivite
     *            the collectivite to set
     */
    public void setCollectivite(Collectivite collectivite) {
        this.collectivite = collectivite;
    }

}

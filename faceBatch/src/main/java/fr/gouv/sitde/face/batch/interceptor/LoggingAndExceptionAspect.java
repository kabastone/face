package fr.gouv.sitde.face.batch.interceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class LoggingAndExceptionsAspect.
 *
 * @author a453029
 */
@Aspect
@Configuration
public class LoggingAndExceptionAspect {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAndExceptionAspect.class);

    /**
     * Log en DEBUG l'entrée dans toutes les méthodes publiques de l'application.
     *
     * @param joinPoint
     *            the join point
     */
    @Before("execution(public * fr.gouv.sitde.face..*.*(..))")
    public void logBeforeAllMethods(JoinPoint joinPoint) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entrée dans {}", joinPoint.toShortString());
        }
    }

    /**
     * Log en DEBUG la sortie de toutes les méthodes publiques de l'application (si aucune exception n'est envoyée).
     *
     * @param joinPoint
     *            the join point
     * @param result
     *            the result
     */
    @AfterReturning(value = "execution(public * fr.gouv.sitde.face..*.*(..))", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Sortie de {} avec le valeur de retour {}", joinPoint.toShortString(), result);
        }
    }

    /**
     * Logue les erreurs et transforme les exceptions non prévues en TechniqueException.
     *
     * @param joinPoint
     *            the join point
     * @param ex
     *            the ex
     */
    @AfterThrowing(pointcut = "execution(public * fr.gouv.sitde.face.batch..*.*(..))", throwing = "ex")
    public void treatThrownExceptions(JoinPoint joinPoint, Throwable ex) {

        if (ex instanceof RegleGestionException) {
            // Exception metier : on la logue en debug
            RegleGestionException rgException = (RegleGestionException) ex;

            LOGGER.error(MessageUtils.getMessageMetier(rgException));
            throw rgException;

        } else if (ex instanceof TechniqueException) {
            // Exception technique, on la logue en erreur
            LOGGER.error(ex.getMessage(), ex);
            throw (TechniqueException) ex;

        } else {
            // Exception non prévue, on la transforme en TechniqueException et on la logue
            TechniqueException techException = new TechniqueException(ex.getMessage(), ex);
            LOGGER.error(techException.getMessage(), techException);
            throw techException;
        }
    }
}

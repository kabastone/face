
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour EJType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EJType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ENTETE" type="{FSO0028A}EnteteType"/&gt;
 *         &lt;element name="IMPUTATION" type="{FSO0028A}ImputationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="POSTE" type="{FSO0028A}PosteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PARTENAIRE" type="{FSO0028A}PartenaireType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TEXTE" type="{FSO0028A}TexteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EJType", propOrder = { "entete", "imputation", "lignePoste", "partenaire", "texte" })
public class EngagementJuridiqueFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The entete. */
    @XmlElement(name = "ENTETE", required = true)
    private EnteteFSO0028A entete;

    /** The imputation. */
    @XmlElement(name = "IMPUTATION")
    private List<ImputationFSO0028A> imputation;

    /** The ligne poste. */
    @XmlElement(name = "POSTE")
    private List<LignePosteFSO0028A> lignePoste;

    /** The partenaire. */
    @XmlElement(name = "PARTENAIRE")
    private List<PartenaireFSO0028A> partenaire;

    /** The texte. */
    @XmlElement(name = "TEXTE")
    private List<TexteFSO0028A> texte;

    /**
     * Obtient la valeur de la propriété entete.
     *
     * @return possible object is {@link EnteteFSO0028A }
     *
     */
    public EnteteFSO0028A getEntete() {
        return this.entete;
    }

    /**
     * Définit la valeur de la propriété entete.
     *
     * @param value
     *            allowed object is {@link EnteteFSO0028A }
     *
     */
    public void setEntete(EnteteFSO0028A value) {
        this.entete = value;
    }

    /**
     * Gets the value of the imputation property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the imputation property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getImputation().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ImputationFSO0028A }
     *
     * @return the imputation
     */
    public List<ImputationFSO0028A> getImputation() {
        if (this.imputation == null) {
            this.imputation = new ArrayList<>();
        }
        return this.imputation;
    }

    /**
     * Gets the value of the lignePoste property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the lignePoste property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getLignePoste().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link LignePosteFSO0028A }
     *
     * @return the ligne poste
     */
    public List<LignePosteFSO0028A> getLignePoste() {
        if (this.lignePoste == null) {
            this.lignePoste = new ArrayList<>();
        }
        return this.lignePoste;
    }

    /**
     * Gets the value of the partenaire property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the partenaire property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPartenaire().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link PartenaireFSO0028A }
     *
     * @return the partenaire
     */
    public List<PartenaireFSO0028A> getPartenaire() {
        if (this.partenaire == null) {
            this.partenaire = new ArrayList<>();
        }
        return this.partenaire;
    }

    /**
     * Gets the value of the texte property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the texte property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getTexte().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link TexteFSO0028A }
     *
     * @return the texte
     */
    public List<TexteFSO0028A> getTexte() {
        if (this.texte == null) {
            this.texte = new ArrayList<>();
        }
        return this.texte;
    }

    /**
     * Sets the imputation.
     *
     * @param imputation
     *            the imputation to set
     */
    public void setImputation(List<ImputationFSO0028A> imputation) {
        this.imputation = imputation;
    }

    /**
     * Sets the ligne poste.
     *
     * @param lignePoste
     *            the lignePoste to set
     */
    public void setLignePoste(List<LignePosteFSO0028A> lignePoste) {
        this.lignePoste = lignePoste;
    }

    /**
     * Sets the partenaire.
     *
     * @param partenaire
     *            the partenaire to set
     */
    public void setPartenaire(List<PartenaireFSO0028A> partenaire) {
        this.partenaire = partenaire;
    }

    /**
     * Sets the texte.
     *
     * @param texte
     *            the texte to set
     */
    public void setTexte(List<TexteFSO0028A> texte) {
        this.texte = texte;
    }

}

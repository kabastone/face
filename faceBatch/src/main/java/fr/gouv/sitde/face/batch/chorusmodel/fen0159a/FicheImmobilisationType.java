
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'une fiche d'immobilisation dans CHORUS
 *
 * <p>
 * Classe Java pour FicheImmobilisationType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="FicheImmobilisationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="COMP_CODE" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *         &lt;element name="SUB_NUMBER" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FicheImmobilisationType", propOrder = { "idChorus", "compCode", "subNumber" })
public class FicheImmobilisationType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "COMP_CODE", required = true)
    private String compCode;
    @XmlElement(name = "SUB_NUMBER", required = true)
    private String subNumber;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété compCode.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCompCode() {
        return this.compCode;
    }

    /**
     * Définit la valeur de la propriété compCode.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCompCode(String value) {
        this.compCode = value;
    }

    /**
     * Obtient la valeur de la propriété subNumber.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSubNumber() {
        return this.subNumber;
    }

    /**
     * Définit la valeur de la propriété subNumber.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setSubNumber(String value) {
        this.subNumber = value;
    }

}

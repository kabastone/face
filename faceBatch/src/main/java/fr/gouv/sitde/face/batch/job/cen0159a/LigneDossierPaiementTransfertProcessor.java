/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0159a;

import javax.inject.Inject;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.cen.LigneDossier;
import fr.gouv.sitde.face.batch.chorusmodel.cen.TypeLigneDtlEnum;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService;
import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeAnomalieEnum;

/**
 * The Class LigneDossierPaiementTransfertProcessor.
 */
public class LigneDossierPaiementTransfertProcessor implements ItemProcessor<LigneDossier, LigneDossierPaiementTransfertTraitementDto> {

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The etat demande paiement service. */
    @Inject
    private EtatDemandePaiementService etatDemandePaiementService;

    /** The type anomalie service. */
    @Inject
    private TypeAnomalieService typeAnomalieService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public LigneDossierPaiementTransfertTraitementDto process(LigneDossier ligneDossier) throws Exception {
        DemandePaiement demande = this.demandePaiementService.rechercherDemandePaiementParIdChorus(Long.valueOf(ligneDossier.getIdDae()));
        LigneDossierPaiementTransfertTraitementDto ligneDossierTraitementDTO;
        if (ligneDossier.getTypeLigne().equals(TypeLigneDtlEnum.ACQ)) {
            demande.setEtatDemandePaiement(this.etatDemandePaiementService.rechercherParCode(EtatDemandePaiementEnum.TRANSFEREE.toString()));
            demande.setChorusNumSf(ligneDossier.getIdDest());

            ligneDossierTraitementDTO = new LigneDossierPaiementTransfertTraitementDto(demande);
        } else {
            // Mise à jour de la demande.
            demande.setEtatDemandePaiement(this.etatDemandePaiementService.rechercherParCode(EtatDemandePaiementEnum.ANOMALIE_DETECTEE.toString()));
            demande.setChorusNumSf(ligneDossier.getIdDest());

            // Création d'une anomalie.
            Anomalie anomalie = new Anomalie();
            anomalie.setCorrigee(false);
            anomalie.setDemandePaiement(demande);

            // Création de la string explicative de la problématique.
            StringBuilder builder = new StringBuilder(ligneDossier.getCodeErreur());
            builder.append(" | ");
            builder.append(ligneDossier.getLibelleErreur());
            anomalie.setProblematique(builder.toString());

            // Récupération du type anomalie.
            TypeAnomalie typeAnomalie = this.typeAnomalieService.rechercherParCode(TypeAnomalieEnum.ANOMALIE_CHORUS_PAIEMENT.getCode());
            anomalie.setTypeAnomalie(typeAnomalie);

            // Sauvegarde de l'anomalie.
            ligneDossierTraitementDTO = new LigneDossierPaiementTransfertTraitementDto(demande, anomalie);
        }
        return ligneDossierTraitementDTO;
    }

}

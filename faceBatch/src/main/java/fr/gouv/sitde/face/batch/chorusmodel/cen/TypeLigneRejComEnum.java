
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TypeLigneRejComType.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="TypeLigneRejComType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="3"/&gt;
 *     &lt;enumeration value="REJ"/&gt;
 *     &lt;enumeration value="COM"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "TypeLigneRejComType")
@XmlEnum
public enum TypeLigneRejComEnum {

    /** The rej. */
    REJ,

    /** The com. */
    COM;

    /**
     * Value.
     *
     * @return the string
     */
    public String value() {
        return this.name();
    }

    /**
     * From value.
     *
     * @param v
     *            the v
     * @return the type ligne rej com enum
     */
    public static TypeLigneRejComEnum fromValue(String v) {
        return valueOf(v);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.expiration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class CadencementMajBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailCadencementNonValideBatchConfiguration {

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The dossier subvention service. */
    @Inject
    private CollectiviteService collectiviteService;

    /**
     * Dossier subvention expiration reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<Collectivite> collectiviteExpirationReader() {
        List<Collectivite> listeCollectivite = this.collectiviteService.rechercherCollectivitePourMailCadencementNonValide();
        return new ListItemReader<>(listeCollectivite);
    }

    /**
     * Email expiration writer.
     *
     * @return the email expiration writer
     */
    @Bean
    @StepScope
    public EmailCadencementNonValideWriter emailExpirationCadencementWriter() {
        return new EmailCadencementNonValideWriter();
    }

    /**
     * Step email expiration.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEmailCadencementNonValide() {
        return this.stepBuilderFactory.get("step_email_cadencement_non_valide").<Collectivite, Collectivite> chunk(1)
                .reader(this.collectiviteExpirationReader()).writer(this.emailExpirationCadencementWriter()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.EMAIL_CADENCEMENT_NON_VALIDE_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.EMAIL_EXPIRATION_JOB.getNom()).start(this.stepEmailCadencementNonValide()).build();
    }
}

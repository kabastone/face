
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.gouv.sitde.face.batch.chorusmodel.adapter.CustomLocalDateTimeAdapter;

/**
 * <p>
 * Classe Java pour EnteteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EnteteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TypeEJ" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}TypeEJType"/&gt;
 *         &lt;element name="OrganisationAchat"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="4"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="GroupeAcheteur"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="3"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="IdEJChorus" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="IdEJAppliExt" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Initiateur" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="12"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Devise" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CondPaiement" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Statut" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="30"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CodeSociete" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SchemaPartenaire" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CodeAcompte" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PourcentageAcompte" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;totalDigits value="7"/&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *               &lt;minExclusive value="0"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateAcompte" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="CodeInteret" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnteteType", propOrder = { "typeEJ", "organisationAchat", "groupeAcheteur", "idEJChorus", "idEJAppliExt", "initiateur", "devise",
        "condPaiement", "statut", "codeSociete", "schemaPartenaire", "codeAcompte", "pourcentageAcompte", "dateAcompte", "codeInteret" })
public class EnteteType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "TypeEJ", required = true)
    @XmlSchemaType(name = "string")
    private TypeEJType typeEJ;
    @XmlElement(name = "OrganisationAchat", required = true)
    private String organisationAchat;
    @XmlElement(name = "GroupeAcheteur", required = true)
    private String groupeAcheteur;
    @XmlElement(name = "IdEJChorus")
    private String idEJChorus;
    @XmlElement(name = "IdEJAppliExt")
    private String idEJAppliExt;
    @XmlElement(name = "Initiateur")
    private String initiateur;
    @XmlElement(name = "Devise")
    private String devise;
    @XmlElement(name = "CondPaiement")
    private String condPaiement;
    @XmlElement(name = "Statut")
    private String statut;
    @XmlElement(name = "CodeSociete")
    private String codeSociete;
    @XmlElement(name = "SchemaPartenaire")
    private String schemaPartenaire;
    @XmlElement(name = "CodeAcompte")
    private String codeAcompte;
    @XmlElement(name = "PourcentageAcompte")
    private BigDecimal pourcentageAcompte;
    @XmlElement(name = "DateAcompte", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateAcompte;
    @XmlElement(name = "CodeInteret")
    private String codeInteret;

    /**
     * Obtient la valeur de la propriété typeEJ.
     *
     * @return possible object is {@link TypeEJType }
     *
     */
    public TypeEJType getTypeEJ() {
        return this.typeEJ;
    }

    /**
     * Définit la valeur de la propriété typeEJ.
     *
     * @param value
     *            allowed object is {@link TypeEJType }
     *
     */
    public void setTypeEJ(TypeEJType value) {
        this.typeEJ = value;
    }

    /**
     * Obtient la valeur de la propriété organisationAchat.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOrganisationAchat() {
        return this.organisationAchat;
    }

    /**
     * Définit la valeur de la propriété organisationAchat.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setOrganisationAchat(String value) {
        this.organisationAchat = value;
    }

    /**
     * Obtient la valeur de la propriété groupeAcheteur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGroupeAcheteur() {
        return this.groupeAcheteur;
    }

    /**
     * Définit la valeur de la propriété groupeAcheteur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setGroupeAcheteur(String value) {
        this.groupeAcheteur = value;
    }

    /**
     * Obtient la valeur de la propriété idEJChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdEJChorus() {
        return this.idEJChorus;
    }

    /**
     * Définit la valeur de la propriété idEJChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdEJChorus(String value) {
        this.idEJChorus = value;
    }

    /**
     * Obtient la valeur de la propriété idEJAppliExt.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdEJAppliExt() {
        return this.idEJAppliExt;
    }

    /**
     * Définit la valeur de la propriété idEJAppliExt.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdEJAppliExt(String value) {
        this.idEJAppliExt = value;
    }

    /**
     * Obtient la valeur de la propriété initiateur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getInitiateur() {
        return this.initiateur;
    }

    /**
     * Définit la valeur de la propriété initiateur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setInitiateur(String value) {
        this.initiateur = value;
    }

    /**
     * Obtient la valeur de la propriété devise.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDevise() {
        return this.devise;
    }

    /**
     * Définit la valeur de la propriété devise.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDevise(String value) {
        this.devise = value;
    }

    /**
     * Obtient la valeur de la propriété condPaiement.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCondPaiement() {
        return this.condPaiement;
    }

    /**
     * Définit la valeur de la propriété condPaiement.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCondPaiement(String value) {
        this.condPaiement = value;
    }

    /**
     * Obtient la valeur de la propriété statut.
     *
     * @return possible object is {@link String }
     *
     */
    public String getStatut() {
        return this.statut;
    }

    /**
     * Définit la valeur de la propriété statut.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setStatut(String value) {
        this.statut = value;
    }

    /**
     * Obtient la valeur de la propriété codeSociete.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeSociete() {
        return this.codeSociete;
    }

    /**
     * Définit la valeur de la propriété codeSociete.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeSociete(String value) {
        this.codeSociete = value;
    }

    /**
     * Obtient la valeur de la propriété schemaPartenaire.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSchemaPartenaire() {
        return this.schemaPartenaire;
    }

    /**
     * Définit la valeur de la propriété schemaPartenaire.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setSchemaPartenaire(String value) {
        this.schemaPartenaire = value;
    }

    /**
     * Obtient la valeur de la propriété codeAcompte.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeAcompte() {
        return this.codeAcompte;
    }

    /**
     * Définit la valeur de la propriété codeAcompte.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeAcompte(String value) {
        this.codeAcompte = value;
    }

    /**
     * Obtient la valeur de la propriété pourcentageAcompte.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getPourcentageAcompte() {
        return this.pourcentageAcompte;
    }

    /**
     * Définit la valeur de la propriété pourcentageAcompte.
     *
     * @param value
     *            allowed object is {@link BigDecimal }
     *
     */
    public void setPourcentageAcompte(BigDecimal value) {
        this.pourcentageAcompte = value;
    }

    /**
     * Obtient la valeur de la propriété dateAcompte.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateAcompte() {
        return this.dateAcompte;
    }

    /**
     * Définit la valeur de la propriété dateAcompte.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateAcompte(LocalDateTime value) {
        this.dateAcompte = value;
    }

    /**
     * Obtient la valeur de la propriété codeInteret.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeInteret() {
        return this.codeInteret;
    }

    /**
     * Définit la valeur de la propriété codeInteret.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeInteret(String value) {
        this.codeInteret = value;
    }

}

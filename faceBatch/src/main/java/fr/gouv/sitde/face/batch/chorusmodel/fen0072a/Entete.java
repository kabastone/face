
package fr.gouv.sitde.face.batch.chorusmodel.fen0072a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>
 * Classe Java pour EnteteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EnteteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CODE_APPLI" type="{FEN0072A}Char6Type"/&gt;
 *         &lt;element name="ID_AE" type="{FEN0072A}Char16Type"/&gt;
 *         &lt;element name="DATE_CPT" type="{FEN0072A}Date8Type"/&gt;
 *         &lt;element name="DATE_DOC" type="{FEN0072A}Date8Type"/&gt;
 *         &lt;element name="BILL_OF_LADING" type="{FEN0072A}Char16Type" minOccurs="0"/&gt;
 *         &lt;element name="CODE_MVT" type="{FEN0072A}Char3Type"/&gt;
 *         &lt;element name="MOTIF_MVT" type="{FEN0072A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="GR_GI_SLIP_NO" type="{FEN0072A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="PR_UNAME" type="{FEN0072A}Char12Type"/&gt;
 *         &lt;element name="TXT_ENTETE" type="{FEN0072A}Char25Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ENTETE")
@XmlType(name = "EnteteType", propOrder = { "codeApplication", "idAE", "dateComptable", "dateDocument", "billOfLading", "codeMouvement",
        "motifMouvement", "grGiSlipNo", "prUname", "texteEntete" })
public class Entete implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "CODE_APPLI", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String codeApplication;
    @XmlElement(name = "ID_AE", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String idAE;
    @XmlElement(name = "DATE_CPT", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String dateComptable;
    @XmlElement(name = "DATE_DOC", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String dateDocument;
    @XmlElement(name = "BILL_OF_LADING")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String billOfLading;
    @XmlElement(name = "CODE_MVT", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String codeMouvement;
    @XmlElement(name = "MOTIF_MVT")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String motifMouvement;
    @XmlElement(name = "GR_GI_SLIP_NO")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String grGiSlipNo;
    @XmlElement(name = "PR_UNAME", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String prUname;
    @XmlElement(name = "TXT_ENTETE")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String texteEntete;

    /**
     * Obtient la valeur de la propriété codeApplication.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeApplication() {
        return this.codeApplication;
    }

    /**
     * Définit la valeur de la propriété codeApplication.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeApplication(String value) {
        this.codeApplication = value;
    }

    /**
     * Obtient la valeur de la propriété idAE.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdAE() {
        return this.idAE;
    }

    /**
     * Définit la valeur de la propriété idAE.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdAE(String value) {
        this.idAE = value;
    }

    /**
     * Obtient la valeur de la propriété dateComptable.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDateComptable() {
        return this.dateComptable;
    }

    /**
     * Définit la valeur de la propriété dateComptable.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateComptable(String value) {
        this.dateComptable = value;
    }

    /**
     * Obtient la valeur de la propriété dateDocument.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDateDocument() {
        return this.dateDocument;
    }

    /**
     * Définit la valeur de la propriété dateDocument.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateDocument(String value) {
        this.dateDocument = value;
    }

    /**
     * Obtient la valeur de la propriété billOfLading.
     *
     * @return possible object is {@link String }
     *
     */
    public String getBillOfLading() {
        return this.billOfLading;
    }

    /**
     * Définit la valeur de la propriété billOfLading.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setBillOfLading(String value) {
        this.billOfLading = value;
    }

    /**
     * Obtient la valeur de la propriété codeMouvement.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeMouvement() {
        return this.codeMouvement;
    }

    /**
     * Définit la valeur de la propriété codeMouvement.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeMouvement(String value) {
        this.codeMouvement = value;
    }

    /**
     * Obtient la valeur de la propriété motifMouvement.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMotifMouvement() {
        return this.motifMouvement;
    }

    /**
     * Définit la valeur de la propriété motifMouvement.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setMotifMouvement(String value) {
        this.motifMouvement = value;
    }

    /**
     * Obtient la valeur de la propriété grGiSlipNo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGrGiSlipNo() {
        return this.grGiSlipNo;
    }

    /**
     * Définit la valeur de la propriété grGiSlipNo.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setGrGiSlipNo(String value) {
        this.grGiSlipNo = value;
    }

    /**
     * Obtient la valeur de la propriété prUname.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPrUname() {
        return this.prUname;
    }

    /**
     * Définit la valeur de la propriété prUname.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setPrUname(String value) {
        this.prUname = value;
    }

    /**
     * Obtient la valeur de la propriété texteEntete.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTexteEntete() {
        return this.texteEntete;
    }

    /**
     * Définit la valeur de la propriété texteEntete.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setTexteEntete(String value) {
        this.texteEntete = value;
    }

}

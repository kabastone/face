/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051asf;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class ServiceFaitProcessor.
 *
 * @author Atos
 */
public class ServiceFaitProcessor implements ItemProcessor<ServiceFaitDto, ServiceFait> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceFaitProcessor.class);

    /** The transfert paiement service. */
    @Inject
    private TransfertPaiementService transfertPaiementService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The etat demande paiement service. */
    @Inject
    private EtatDemandePaiementService etatDemandePaiementService;

    /** The batch 51 date format. */
    private static String batch51DateFormat = "ddMMyyyy";

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public ServiceFait process(ServiceFaitDto item) throws Exception {

        ServiceFait serviceFait = new ServiceFait();
        DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(batch51DateFormat);

        LocalDateTime dateCertification = null;
        TransfertPaiement transfertPaiement = null;

        try {

            dateCertification = LocalDateTime.of(LocalDate.parse(item.getDateCertification(), dateTimerFormatter), LocalTime.NOON);

        } catch (DateTimeParseException e) {
            LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.erreur.date.certification", item.getDateCertification(),
                    item.getNumeroServiceFait(), item.getNumeroEJ()), e);
        }
        // Step 1 de RG_SUB_211-02_Trouver la correspondance du service fait

        List<TransfertPaiement> listeTransfert = this.transfertPaiementService.rechercherParNumSFetLegacyetNumEJ(item.getNumeroServiceFait(),
                item.getNumLigneLegacy(), item.getNumeroEJ());

        if (listeTransfert.size() == 1) {
            transfertPaiement = listeTransfert.get(0);
            if (transfertPaiement != null) {
                transfertPaiement.setDateCertification(dateCertification);
            }
        }

        serviceFait.setTransfertPaiement(transfertPaiement);

        // Etape 2 : certifier le paiement
        // recherche des demandes de paiement
        List<DemandePaiement> listDemandePaiement = this.demandePaiementService.rechercherCertifNonNulParService(item.getNumeroServiceFait());

        for (DemandePaiement demandePaiement : listDemandePaiement) {
            EtatDemandePaiement etatCertifie = this.etatDemandePaiementService.rechercherParCode(EtatDemandePaiementEnum.CERTIFIEE.toString());
            demandePaiement.setEtatDemandePaiement(etatCertifie);

            serviceFait.addDemandePaiement(demandePaiement);
        }

        return serviceFait;
    }

}

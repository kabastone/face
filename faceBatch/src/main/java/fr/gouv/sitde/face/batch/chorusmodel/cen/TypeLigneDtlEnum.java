
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TypeLigneDtlType.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 *
 * <pre>
 * &lt;simpleType name="TypeLigneDtlType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="3"/&gt;
 *     &lt;enumeration value="ACQ"/&gt;
 *     &lt;enumeration value="ERR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "TypeLigneDtlType")
@XmlEnum
public enum TypeLigneDtlEnum {

    /** The acq. */
    ACQ,

    /** The err. */
    ERR;

    /**
     * Value.
     *
     * @return the string
     */
    public String value() {
        return this.name();
    }

    /**
     * From value.
     *
     * @param v
     *            the v
     * @return the type ligne dtl enum
     */
    public static TypeLigneDtlEnum fromValue(String v) {
        return valueOf(v);
    }

}

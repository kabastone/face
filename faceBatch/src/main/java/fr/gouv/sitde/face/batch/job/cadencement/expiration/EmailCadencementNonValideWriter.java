/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.expiration;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * @author A754839
 *
 */
public class EmailCadencementNonValideWriter implements ItemWriter<Collectivite> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailCadencementNonValideWriter.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

    @Override
    public void write(List<? extends Collectivite> items) throws Exception {
        for (Collectivite collectivite : items) {
            this.emailService.creerEtEnvoyerEmail(collectivite, TypeEmailEnum.CAD_MAJ, TemplateEmailEnum.CAD_MAJ, null);

            LOGGER.debug("Envoi de l'email pour la collectivite  {}", collectivite.getNomCourt());
        }
    }

}


package fr.gouv.sitde.face.batch.chorusmodel.fen0072a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour FEN0072AType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="FEN0072AType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SERVICE_FAIT" type="{FEN0072A}DossierType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "FEN0072A")
@XmlType(name = "FEN0072AType", propOrder = { "dossier" })
public class Fen0072A implements Serializable {

    /** The xml namespace. */
    @XmlAttribute(name = "xmlns")
    private final String XML_NAMESPACE = "FEN0072A";

    /** The xsi schema location. */
    @XmlAttribute(name = "xsi:schemaLocation")
    private final String XSI_SCHEMA_LOCATION = "FEN0072A XSD_SE_FEN0072A_v2.1.xsd";

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "SERVICE_FAIT", required = true)
    private List<Dossier> dossier;

    /**
     * Gets the value of the dossier property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the dossier property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getDossier().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Dossier }
     *
     *
     */
    public List<Dossier> getDossier() {
        if (this.dossier == null) {
            this.dossier = new ArrayList<>();
        }
        return this.dossier;
    }

    /**
     * @param dossier
     *            the dossier to set
     */
    public void setDossier(List<Dossier> dossier) {
        this.dossier = dossier;
    }

}

package fr.gouv.sitde.face.batch.job.pertedotation;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

public class PerteDotationWriter implements ItemWriter<PerteDotationMajDto> {
	
	/** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PerteDotationWriter.class);

	@Inject
	private DotationCollectiviteService dotationCollectiviteService;

	@Inject
	private LigneDotationDepartementService ligneDotationDepartementService;

	@Autowired
	private EmailService emailService;

	@Override
	public void write(List<? extends PerteDotationMajDto> items) throws Exception {
		if (!items.isEmpty()) {
			for (PerteDotationMajDto dto : items) {
				this.ligneDotationDepartementService.ajouterLigneDotationDepartement(dto.getLigneDotationDepartement(),
						TypeDotationDepartementEnum.PERTE_DE_DOTATION);

				this.dotationCollectiviteService.majDotationCollectivite(dto.getDotationCollectivite());
				Collectivite collectivite = dto.getDotationCollectivite().getCollectivite();
				if (!collectivite.getAdressesEmail().isEmpty()) {
					this.emailService.creerEtEnvoyerEmail(collectivite, TypeEmailEnum.PERTE_DOTATION,
							TemplateEmailEnum.EMAIL_PERTE_DOTATION, null);
					 LOGGER.debug("Envoi de l'email pour la collectivite {}", collectivite);
				}

			}
		}
	}

}

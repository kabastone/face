/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.maj;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class CadencementMajBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class CadencementMajBatchConfiguration {

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The dossier subvention service. */
    @Inject
    private CollectiviteService collectiviteService;

    /**
     * Dossier subvention expiration reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<Collectivite> retrouverCollectivitePourCadencementMajReader() {
        List<Collectivite> listeCollectivite = this.collectiviteService.rechercherCollectivitePourCadencementMaj();
        return new ListItemReader<>(listeCollectivite);
    }

    /**
     * Email expiration writer.
     *
     * @return the email expiration writer
     */
    @Bean
    @StepScope
    public CadencementMajWriter cadencementMajWriter() {
        return new CadencementMajWriter();
    }

    @Bean
    @StepScope
    public CadencementMajProcessor cadencementMajProcessor() {
        return new CadencementMajProcessor();
    }

    /**
     * Step email expiration.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepCadencementMaj() {
        return this.stepBuilderFactory.get("step_cadencement_maj").<Collectivite, CadencementMajDto> chunk(1)
                .reader(this.retrouverCollectivitePourCadencementMajReader()).processor(this.cadencementMajProcessor())
                .writer(this.cadencementMajWriter()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.CADENCEMENT_MAJ_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.CADENCEMENT_MAJ_JOB.getNom()).start(this.stepCadencementMaj()).build();
    }
}

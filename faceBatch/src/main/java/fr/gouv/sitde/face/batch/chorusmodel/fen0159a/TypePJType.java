
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour TypePJType.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TypePJType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;maxLength value="5"/&gt;
 *     &lt;enumeration value="BMP"/&gt;
 *     &lt;enumeration value="BZ2"/&gt;
 *     &lt;enumeration value="DOC"/&gt;
 *     &lt;enumeration value="DOCX"/&gt;
 *     &lt;enumeration value="CSV"/&gt;
 *     &lt;enumeration value="FAX"/&gt;
 *     &lt;enumeration value="GIF"/&gt;
 *     &lt;enumeration value="GZ"/&gt;
 *     &lt;enumeration value="GZIP"/&gt;
 *     &lt;enumeration value="HTM"/&gt;
 *     &lt;enumeration value="HTML"/&gt;
 *     &lt;enumeration value="JPEG"/&gt;
 *     &lt;enumeration value="JPG"/&gt;
 *     &lt;enumeration value="ODP"/&gt;
 *     &lt;enumeration value="ODS"/&gt;
 *     &lt;enumeration value="ODT"/&gt;
 *     &lt;enumeration value="P7S"/&gt;
 *     &lt;enumeration value="PDF"/&gt;
 *     &lt;enumeration value="PNG"/&gt;
 *     &lt;enumeration value="PPS"/&gt;
 *     &lt;enumeration value="PPT"/&gt;
 *     &lt;enumeration value="PPTX"/&gt;
 *     &lt;enumeration value="RTF"/&gt;
 *     &lt;enumeration value="SVG"/&gt;
 *     &lt;enumeration value="TGZ"/&gt;
 *     &lt;enumeration value="TIF"/&gt;
 *     &lt;enumeration value="TXT"/&gt;
 *     &lt;enumeration value="XHTML"/&gt;
 *     &lt;enumeration value="XLC"/&gt;
 *     &lt;enumeration value="XLM"/&gt;
 *     &lt;enumeration value="XLS"/&gt;
 *     &lt;enumeration value="XLSX"/&gt;
 *     &lt;enumeration value="XML"/&gt;
 *     &lt;enumeration value="XSD"/&gt;
 *     &lt;enumeration value="XSL"/&gt;
 *     &lt;enumeration value="ZIP"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypePJType")
@XmlEnum
public enum TypePJType {

    BMP("BMP"),
    @XmlEnumValue("BZ2")
    BZ_2("BZ2"),
    DOC("DOC"),
    DOCX("DOCX"),
    CSV("CSV"),
    FAX("FAX"),
    GIF("GIF"),
    GZ("GZ"),
    GZIP("GZIP"),
    HTM("HTM"),
    HTML("HTML"),
    JPEG("JPEG"),
    JPG("JPG"),
    ODP("ODP"),
    ODS("ODS"),
    ODT("ODT"),
    @XmlEnumValue("P7S")
    P_7_S("P7S"),
    PDF("PDF"),
    PNG("PNG"),
    PPS("PPS"),
    PPT("PPT"),
    PPTX("PPTX"),
    RTF("RTF"),
    SVG("SVG"),
    TGZ("TGZ"),
    TIF("TIF"),
    TXT("TXT"),
    XHTML("XHTML"),
    XLC("XLC"),
    XLM("XLM"),
    XLS("XLS"),
    XLSX("XLSX"),
    XML("XML"),
    XSD("XSD"),
    XSL("XSL"),
    ZIP("ZIP");
    private final String value;

    TypePJType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypePJType fromValue(String v) {
        for (TypePJType c: TypePJType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

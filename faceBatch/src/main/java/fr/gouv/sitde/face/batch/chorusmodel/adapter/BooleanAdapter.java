/**
 *
 */
package fr.gouv.sitde.face.batch.chorusmodel.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The Class BooleanAdapter.
 */
public class BooleanAdapter extends XmlAdapter<Integer, Boolean> {

    /*
     * (non-Javadoc)
     *
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public Boolean unmarshal(Integer s) {
        return s == null ? null : (s == 1);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public Integer marshal(Boolean c) {
        if (c == null) {
            return null;
        } else {
            return (c ? 1 : 0);
        }
    }
}

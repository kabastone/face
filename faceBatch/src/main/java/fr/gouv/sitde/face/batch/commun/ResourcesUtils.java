/**
 *
 */
package fr.gouv.sitde.face.batch.commun;

import java.io.File;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class ResourcesUtils.
 *
 * @author a453029
 */
public final class ResourcesUtils {

    /**
     * Constructeur privé.
     */
    private ResourcesUtils() {
    }

    /**
     * Renvoie le tableau de ressources correspondant aux fichiers présents dans le répertoire passé en paramètre.
     *
     * @param inputDirectory
     *            the input directory
     * @return the ressources from directory
     */
    public static Resource[] getRessourcesFromDirectory(File inputDirectory) {

        // On envoie une technique exception si le fichier n'existe pas ou s'il n'est pas reconnu en tant que répertoire.
        if (!inputDirectory.exists()) {
            throw new TechniqueException("Problème de configuration : le répertoire d'input " + inputDirectory.getName() + " n'existe pas.");
        }

        File[] tableauFichiers = inputDirectory.listFiles();
        if (!inputDirectory.isDirectory() || (tableauFichiers == null)) {
            throw new TechniqueException("Problème de configuration : " + inputDirectory.getName() + " n'est pas un répertoire.");
        }

        // Parcours des fichiers du répertoire pour renvoyer les resources correspondantes
        Resource[] tableauResources = new Resource[tableauFichiers.length];

        for (int i = 0; i < tableauFichiers.length; i++) {
            tableauResources[i] = new FileSystemResource(tableauFichiers[i].getPath());
        }

        return tableauResources;
    }

}

package fr.gouv.sitde.face.batch.job.pertedotation;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class PerteDotationProcessor.
 */
public class PerteDotationProcessor implements ItemProcessor<DotationCollectivite, PerteDotationMajDto> {

	/** The time operation transverse. */
	@Inject
	private TimeOperationTransverse timeOperationTransverse;

	@Inject
	private DotationDepartementService dotationDepartementService;

	@Inject
	private DotationSousProgrammeRepository dotationSpRepo;
	
	@Inject
	private CollectiviteRepository collectiviteRepository;

	/**
	 * Process.
	 *
	 * @param dotationCollectivite the dotation collectivite
	 * @return the sets the
	 * @throws Exception the exception
	 */
	@Override
	public PerteDotationMajDto process(DotationCollectivite dotationCollectivite) throws Exception {
		// récuperer la perte au niveau du département
		BigDecimal lddMontant = dotationCollectivite.getMontant().subtract(dotationCollectivite.getDotationRepartie());
		LigneDotationDepartement ligneDotationDepartement = new LigneDotationDepartement();
		DotationDepartement dotationDepartement = this.dotationDepartementService
				.rechercherParId(dotationCollectivite.getDotationDepartement().getId());
		ligneDotationDepartement.setMontant(new BigDecimal(lddMontant.negate().intValue()));
		ligneDotationDepartement.setDotationDepartement(dotationDepartement);
		ligneDotationDepartement.setDateEnvoi(this.timeOperationTransverse.now());
		Collectivite collectivite = this.collectiviteRepository.rechercherParId(dotationCollectivite.getCollectivite().getId());
		ligneDotationDepartement.setCollectivite(collectivite);

		// Mise à jour de la perte au niveau de la collectivité
		dotationCollectivite.setMontant(dotationCollectivite.getDotationRepartie());
		DotationSousProgramme dotationSp = this.dotationSpRepo.rechercherDotationSousProgrammeParId(
				dotationCollectivite.getDotationDepartement().getDotationSousProgramme().getId());
		BigDecimal dotSpMontant = dotationSp.getMontant();
		dotationSp.setMontant(dotSpMontant.subtract(lddMontant));
		dotationDepartement.setDotationSousProgramme(dotationSp);
		dotationCollectivite.setDotationDepartement(dotationDepartement);
		PerteDotationMajDto dto = new PerteDotationMajDto();
		dto.setDotationCollectivite(dotationCollectivite);
		dto.setLigneDotationDepartement(ligneDotationDepartement);
		return dto;
	}

}

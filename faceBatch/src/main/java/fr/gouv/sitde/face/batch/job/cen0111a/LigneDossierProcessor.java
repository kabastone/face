/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0111a;

import javax.inject.Inject;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.cen.LigneDossier;
import fr.gouv.sitde.face.batch.chorusmodel.cen.TypeLigneDtlEnum;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeAnomalieEnum;

/**
 * The Class LigneDossierProcessor.
 */
public class LigneDossierProcessor implements ItemProcessor<LigneDossier, LigneDossierTraitementDto> {

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The etat demande subvention service. */
    @Inject
    private EtatDemandeSubventionService etatDemandeSubventionService;

    /** The type anomalie service. */
    @Inject
    private TypeAnomalieService typeAnomalieService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public LigneDossierTraitementDto process(LigneDossier ligneDossier) throws Exception {
        DemandeSubvention demande = this.demandeSubventionService
                .rechercherDemandeSubventionPrincipaleParIdChorus(Long.valueOf(ligneDossier.getIdDae()));
        LigneDossierTraitementDto ligneDossierTraitementDTO;
        if (ligneDossier.getTypeLigne().equals(TypeLigneDtlEnum.ACQ)) {
            demande.setEtatDemandeSubvention(
                    this.etatDemandeSubventionService.rechercherEtatDemandeSubventionParCode(EtatDemandeSubventionEnum.TRANSFEREE.toString()));
            demande.getDossierSubvention().setChorusNumEj(ligneDossier.getIdDest());
            ligneDossierTraitementDTO = new LigneDossierTraitementDto(demande);
        } else {
            // Mise à jour de la demande.
            demande.setEtatDemandeSubvention(
                    this.etatDemandeSubventionService.rechercherEtatDemandeSubventionParCode(EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.toString()));
            demande.getDossierSubvention().setChorusNumEj(ligneDossier.getIdDest());

            // Création d'une anomalie.
            Anomalie anomalie = new Anomalie();
            anomalie.setCorrigee(false);
            anomalie.setDemandeSubvention(demande);

            // Création de la string explicative de la problématique.
            StringBuilder builder = new StringBuilder(ligneDossier.getCodeErreur());
            builder.append(" | ");
            builder.append(ligneDossier.getLibelleErreur());
            anomalie.setProblematique(builder.toString());

            // Récupération du type anomalie.
            TypeAnomalie typeAnomalie = this.typeAnomalieService.rechercherParCode(TypeAnomalieEnum.ANOMALIE_CHORUS_SUBVENTION.getCode());
            anomalie.setTypeAnomalie(typeAnomalie);

            // Sauvegarde de l'anomalie.
            ligneDossierTraitementDTO = new LigneDossierTraitementDto(demande, anomalie);
        }
        return ligneDossierTraitementDTO;
    }

}

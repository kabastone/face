
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour ImputationType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ImputationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FSO0028A}Char12Type"/&gt;
 *         &lt;element name="ID_POSTE" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="PERC" type="{FSO0028A}Num3.2Type" minOccurs="0"/&gt;
 *         &lt;element name="GL_ACCT" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="BUS_AREA" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="COST_CTR" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="ASSET_NO" type="{FSO0028A}Char12Type" minOccurs="0"/&gt;
 *         &lt;element name="CO_AREA" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="PROFIT_CTR" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="WBS_ELEM_E" type="{FSO0028A}Char24Type" minOccurs="0"/&gt;
 *         &lt;element name="CMMT_ITEM" type="{FSO0028A}Char24Type" minOccurs="0"/&gt;
 *         &lt;element name="FUNDS_CTR" type="{FSO0028A}Char16Type" minOccurs="0"/&gt;
 *         &lt;element name="FUND" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="FUNC_AREA" type="{FSO0028A}Char16Type" minOccurs="0"/&gt;
 *         &lt;element name="ACC_CAT" type="{FSO0028A}Char5Type" minOccurs="0"/&gt;
 *         &lt;element name="ACTIVITY" type="{FSO0028A}Char24Type" minOccurs="0"/&gt;
 *         &lt;element name="TRANCHE" type="{FSO0028A}Char24Type" minOccurs="0"/&gt;
 *         &lt;element name="LOC_INT" type="{FSO0028A}Char8Type" minOccurs="0"/&gt;
 *         &lt;element name="PROJ_ANA_MIN" type="{FSO0028A}Char22Type" minOccurs="0"/&gt;
 *         &lt;element name="LOC_MIN" type="{FSO0028A}Char22Type" minOccurs="0"/&gt;
 *         &lt;element name="NAT_MIN" type="{FSO0028A}Char11Type" minOccurs="0"/&gt;
 *         &lt;element name="CPER" type="{FSO0028A}Char15Type" minOccurs="0"/&gt;
 *         &lt;element name="PRESAGE" type="{FSO0028A}Char15Type" minOccurs="0"/&gt;
 *         &lt;element name="AXE_MIN1" type="{FSO0028A}Char22Type" minOccurs="0"/&gt;
 *         &lt;element name="AXE_MIN2" type="{FSO0028A}Char22Type" minOccurs="0"/&gt;
 *         &lt;element name="SUB_NUMBER" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImputationType",
        propOrder = { "idAe", "idPoste", "perc", "glAcct", "busArea", "costCtr", "assetNo", "coArea", "profitCtr", "wbsElemE", "cmmtItem", "fundsCtr",
                "fund", "funcArea", "accCat", "activity", "tranche", "locInt", "projAnaMin", "locMin", "natMin", "cper", "presage", "axeMin1",
                "axeMin2", "subNumber" })
public class ImputationFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The id ae. */
    @XmlElement(name = "ID_AE", required = true)
    private String idAe;

    /** The id poste. */
    @XmlElement(name = "ID_POSTE")
    private String idPoste;

    /** The perc. */
    @XmlElement(name = "PERC")
    private String perc;

    /** The gl acct. */
    @XmlElement(name = "GL_ACCT")
    private String glAcct;

    /** The bus area. */
    @XmlElement(name = "BUS_AREA")
    private String busArea;

    /** The cost ctr. */
    @XmlElement(name = "COST_CTR")
    private String costCtr;

    /** The asset no. */
    @XmlElement(name = "ASSET_NO")
    private String assetNo;

    /** The co area. */
    @XmlElement(name = "CO_AREA")
    private String coArea;

    /** The profit ctr. */
    @XmlElement(name = "PROFIT_CTR")
    private String profitCtr;

    /** The wbs elem E. */
    @XmlElement(name = "WBS_ELEM_E")
    private String wbsElemE;

    /** The cmmt item. */
    @XmlElement(name = "CMMT_ITEM")
    private String cmmtItem;

    /** The funds ctr. */
    @XmlElement(name = "FUNDS_CTR")
    private String fundsCtr;

    /** The fund. */
    @XmlElement(name = "FUND")
    private String fund;

    /** The func area. */
    @XmlElement(name = "FUNC_AREA")
    private String funcArea;

    /** The acc cat. */
    @XmlElement(name = "ACC_CAT")
    private String accCat;

    /** The activity. */
    @XmlElement(name = "ACTIVITY")
    private String activity;

    /** The tranche. */
    @XmlElement(name = "TRANCHE")
    private String tranche;

    /** The loc int. */
    @XmlElement(name = "LOC_INT")
    private String locInt;

    /** The proj ana min. */
    @XmlElement(name = "PROJ_ANA_MIN")
    private String projAnaMin;

    /** The loc min. */
    @XmlElement(name = "LOC_MIN")
    private String locMin;

    /** The nat min. */
    @XmlElement(name = "NAT_MIN")
    private String natMin;

    /** The cper. */
    @XmlElement(name = "CPER")
    private String cper;

    /** The presage. */
    @XmlElement(name = "PRESAGE")
    private String presage;

    /** The axe min 1. */
    @XmlElement(name = "AXE_MIN1")
    private String axeMin1;

    /** The axe min 2. */
    @XmlElement(name = "AXE_MIN2")
    private String axeMin2;

    /** The sub number. */
    @XmlElement(name = "SUB_NUMBER")
    private String subNumber;

    /**
     * Gets the id ae.
     *
     * @return the idAe
     */
    public String getIdAe() {
        return this.idAe;
    }

    /**
     * Sets the id ae.
     *
     * @param idAe
     *            the idAe to set
     */
    public void setIdAe(String idAe) {
        this.idAe = idAe;
    }

    /**
     * Gets the id poste.
     *
     * @return the idPoste
     */
    public String getIdPoste() {
        return this.idPoste;
    }

    /**
     * Sets the id poste.
     *
     * @param idPoste
     *            the idPoste to set
     */
    public void setIdPoste(String idPoste) {
        this.idPoste = idPoste;
    }

    /**
     * Gets the perc.
     *
     * @return the perc
     */
    public String getPerc() {
        return this.perc;
    }

    /**
     * Sets the perc.
     *
     * @param perc
     *            the perc to set
     */
    public void setPerc(String perc) {
        this.perc = perc;
    }

    /**
     * Gets the gl acct.
     *
     * @return the glAcct
     */
    public String getGlAcct() {
        return this.glAcct;
    }

    /**
     * Sets the gl acct.
     *
     * @param glAcct
     *            the glAcct to set
     */
    public void setGlAcct(String glAcct) {
        this.glAcct = glAcct;
    }

    /**
     * Gets the bus area.
     *
     * @return the busArea
     */
    public String getBusArea() {
        return this.busArea;
    }

    /**
     * Sets the bus area.
     *
     * @param busArea
     *            the busArea to set
     */
    public void setBusArea(String busArea) {
        this.busArea = busArea;
    }

    /**
     * Gets the cost ctr.
     *
     * @return the costCtr
     */
    public String getCostCtr() {
        return this.costCtr;
    }

    /**
     * Sets the cost ctr.
     *
     * @param costCtr
     *            the costCtr to set
     */
    public void setCostCtr(String costCtr) {
        this.costCtr = costCtr;
    }

    /**
     * Gets the asset no.
     *
     * @return the assetNo
     */
    public String getAssetNo() {
        return this.assetNo;
    }

    /**
     * Sets the asset no.
     *
     * @param assetNo
     *            the assetNo to set
     */
    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    /**
     * Gets the co area.
     *
     * @return the coArea
     */
    public String getCoArea() {
        return this.coArea;
    }

    /**
     * Sets the co area.
     *
     * @param coArea
     *            the coArea to set
     */
    public void setCoArea(String coArea) {
        this.coArea = coArea;
    }

    /**
     * Gets the profit ctr.
     *
     * @return the profitCtr
     */
    public String getProfitCtr() {
        return this.profitCtr;
    }

    /**
     * Sets the profit ctr.
     *
     * @param profitCtr
     *            the profitCtr to set
     */
    public void setProfitCtr(String profitCtr) {
        this.profitCtr = profitCtr;
    }

    /**
     * Gets the wbs elem E.
     *
     * @return the wbsElemE
     */
    public String getWbsElemE() {
        return this.wbsElemE;
    }

    /**
     * Sets the wbs elem E.
     *
     * @param wbsElemE
     *            the wbsElemE to set
     */
    public void setWbsElemE(String wbsElemE) {
        this.wbsElemE = wbsElemE;
    }

    /**
     * Gets the cmmt item.
     *
     * @return the cmmtItem
     */
    public String getCmmtItem() {
        return this.cmmtItem;
    }

    /**
     * Sets the cmmt item.
     *
     * @param cmmtItem
     *            the cmmtItem to set
     */
    public void setCmmtItem(String cmmtItem) {
        this.cmmtItem = cmmtItem;
    }

    /**
     * Gets the funds ctr.
     *
     * @return the fundsCtr
     */
    public String getFundsCtr() {
        return this.fundsCtr;
    }

    /**
     * Sets the funds ctr.
     *
     * @param fundsCtr
     *            the fundsCtr to set
     */
    public void setFundsCtr(String fundsCtr) {
        this.fundsCtr = fundsCtr;
    }

    /**
     * Gets the fund.
     *
     * @return the fund
     */
    public String getFund() {
        return this.fund;
    }

    /**
     * Sets the fund.
     *
     * @param fund
     *            the fund to set
     */
    public void setFund(String fund) {
        this.fund = fund;
    }

    /**
     * Gets the func area.
     *
     * @return the funcArea
     */
    public String getFuncArea() {
        return this.funcArea;
    }

    /**
     * Sets the func area.
     *
     * @param funcArea
     *            the funcArea to set
     */
    public void setFuncArea(String funcArea) {
        this.funcArea = funcArea;
    }

    /**
     * Gets the acc cat.
     *
     * @return the accCat
     */
    public String getAccCat() {
        return this.accCat;
    }

    /**
     * Sets the acc cat.
     *
     * @param accCat
     *            the accCat to set
     */
    public void setAccCat(String accCat) {
        this.accCat = accCat;
    }

    /**
     * Gets the activity.
     *
     * @return the activity
     */
    public String getActivity() {
        return this.activity;
    }

    /**
     * Sets the activity.
     *
     * @param activity
     *            the activity to set
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    /**
     * Gets the tranche.
     *
     * @return the tranche
     */
    public String getTranche() {
        return this.tranche;
    }

    /**
     * Sets the tranche.
     *
     * @param tranche
     *            the tranche to set
     */
    public void setTranche(String tranche) {
        this.tranche = tranche;
    }

    /**
     * Gets the loc int.
     *
     * @return the locInt
     */
    public String getLocInt() {
        return this.locInt;
    }

    /**
     * Sets the loc int.
     *
     * @param locInt
     *            the locInt to set
     */
    public void setLocInt(String locInt) {
        this.locInt = locInt;
    }

    /**
     * Gets the proj ana min.
     *
     * @return the projAnaMin
     */
    public String getProjAnaMin() {
        return this.projAnaMin;
    }

    /**
     * Sets the proj ana min.
     *
     * @param projAnaMin
     *            the projAnaMin to set
     */
    public void setProjAnaMin(String projAnaMin) {
        this.projAnaMin = projAnaMin;
    }

    /**
     * Gets the loc min.
     *
     * @return the locMin
     */
    public String getLocMin() {
        return this.locMin;
    }

    /**
     * Sets the loc min.
     *
     * @param locMin
     *            the locMin to set
     */
    public void setLocMin(String locMin) {
        this.locMin = locMin;
    }

    /**
     * Gets the nat min.
     *
     * @return the natMin
     */
    public String getNatMin() {
        return this.natMin;
    }

    /**
     * Sets the nat min.
     *
     * @param natMin
     *            the natMin to set
     */
    public void setNatMin(String natMin) {
        this.natMin = natMin;
    }

    /**
     * Gets the cper.
     *
     * @return the cper
     */
    public String getCper() {
        return this.cper;
    }

    /**
     * Sets the cper.
     *
     * @param cper
     *            the cper to set
     */
    public void setCper(String cper) {
        this.cper = cper;
    }

    /**
     * Gets the presage.
     *
     * @return the presage
     */
    public String getPresage() {
        return this.presage;
    }

    /**
     * Sets the presage.
     *
     * @param presage
     *            the presage to set
     */
    public void setPresage(String presage) {
        this.presage = presage;
    }

    /**
     * Gets the axe min 1.
     *
     * @return the axeMin1
     */
    public String getAxeMin1() {
        return this.axeMin1;
    }

    /**
     * Sets the axe min 1.
     *
     * @param axeMin1
     *            the axeMin1 to set
     */
    public void setAxeMin1(String axeMin1) {
        this.axeMin1 = axeMin1;
    }

    /**
     * Gets the axe min 2.
     *
     * @return the axeMin2
     */
    public String getAxeMin2() {
        return this.axeMin2;
    }

    /**
     * Sets the axe min 2.
     *
     * @param axeMin2
     *            the axeMin2 to set
     */
    public void setAxeMin2(String axeMin2) {
        this.axeMin2 = axeMin2;
    }

    /**
     * Gets the sub number.
     *
     * @return the subNumber
     */
    public String getSubNumber() {
        return this.subNumber;
    }

    /**
     * Sets the sub number.
     *
     * @param subNumber
     *            the subNumber to set
     */
    public void setSubNumber(String subNumber) {
        this.subNumber = subNumber;
    }

}

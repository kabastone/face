/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0028a;

import java.io.Serializable;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Class FSO0028ADto.
 */
public class FSO0028ADto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1336285751592448515L;

    private List<DossierSubvention> listeDossier;

    public FSO0028ADto(List<DossierSubvention> listeDossier) {
        super();
        this.listeDossier = listeDossier;
    }

    /**
     * @return the listeDossier
     */
    public List<DossierSubvention> getListeDossier() {
        return this.listeDossier;
    }
}

package fr.gouv.sitde.face.batch.job.emailrappelconsommation;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * The Class EmailRappelConsommationWriter.
 */
public class EmailRappelConsommationWriter implements ItemWriter<Collectivite> {
	
	/** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailRappelConsommationWriter.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

	/**
	 * Write.
	 *
	 * @param items the items
	 * @throws Exception the exception
	 */
	@Override
	public void write(List<? extends Collectivite> items) throws Exception {
		if(!items.isEmpty()) {
			for (Collectivite collectivite : items) {
	            this.emailService.creerEtEnvoyerEmail(collectivite, TypeEmailEnum.RAPPEL_CONSOMMATION, TemplateEmailEnum.EMAIL_RAPPEL_CONSOMMATION, null);

	            LOGGER.debug("Envoi de l'email pour la collectivite  {}", collectivite.getNomCourt());
	        }
		}
		
	}

}

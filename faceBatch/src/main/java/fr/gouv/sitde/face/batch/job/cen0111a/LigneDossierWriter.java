/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0111a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;

/**
 * The Class LigneDossierWriter.
 */
public class LigneDossierWriter implements ItemWriter<LigneDossierTraitementDto> {

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends LigneDossierTraitementDto> items) {

        for (LigneDossierTraitementDto ligneDossier : items) {
            if (ligneDossier.hasAnomalie()) {
                this.anomalieService.creerAnomalie(ligneDossier.getAnomalie());
                this.demandeSubventionService.mettreAJourDemandeSubvention(ligneDossier.getDemandeSubvention());
            }

            this.demandeSubventionService.mettreAJourDemandeSubvention(ligneDossier.getDemandeSubvention());
        }
    }

}

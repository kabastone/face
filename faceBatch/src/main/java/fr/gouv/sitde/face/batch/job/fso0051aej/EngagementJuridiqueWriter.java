/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051aej;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * @author Atos
 *
 */
public class EngagementJuridiqueWriter implements ItemWriter<EngagementJuridiqueChorus> {

    @Inject
    private DemandeSubventionService demandeSubventionService;

    @Inject
    private DossierSubventionService dossierSubventionService;

    @Inject
    private DemandeProlongationService demandeProlongationService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends EngagementJuridiqueChorus> items) throws Exception {
        for (EngagementJuridiqueChorus engagementJuridiqueChorus : items) {
            DemandeSubvention demandeSubvention = engagementJuridiqueChorus.getDemandeSubventionPoste();
            if (demandeSubvention != null) {
                this.demandeSubventionService.mettreAJourDemandeSubvention(demandeSubvention);
            }

            DossierSubvention dossierSubvention = engagementJuridiqueChorus.getDossierSubvention();
            if (dossierSubvention != null) {
                this.dossierSubventionService.mettreAJourDossierSubvention(dossierSubvention);
            }

            DemandeProlongation demandeProlongation = engagementJuridiqueChorus.getDemandeProlongation();
            if (demandeProlongation != null) {
                this.demandeProlongationService.mettreAJourDemandeProlongation(demandeProlongation);
            }
        }
    }

}

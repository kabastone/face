/**
 *
 */
package fr.gouv.sitde.face.batch.job.emaildotationconsommation;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class EmailConsommationWriter.
 */
public class EmailConsommationWriter implements ItemWriter<Collectivite> {

    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends Collectivite> items) throws Exception {
        for (Collectivite col : items) {
            this.dotationCollectiviteService.emailInformationConsommation(col);
        }
    }

}

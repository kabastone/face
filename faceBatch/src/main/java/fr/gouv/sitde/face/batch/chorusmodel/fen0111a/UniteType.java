
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour UniteType.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="UniteType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DEV"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "UniteType")
@XmlEnum
public enum UniteType {


    /**
     * Devise
     * 
     */
    DEV;

    public String value() {
        return name();
    }

    public static UniteType fromValue(String v) {
        return valueOf(v);
    }

}

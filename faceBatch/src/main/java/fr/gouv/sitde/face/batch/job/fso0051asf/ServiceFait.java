/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051asf;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * The Class ServiceFait.
 *
 * Données entre le Processor et le Writter
 *
 * @author Atos
 */
public class ServiceFait {

    /** The list demande paiement. */
    private List<DemandePaiement> listDemandePaiement = new ArrayList<>(0);

    /** The transfert paiement. */
    private TransfertPaiement transfertPaiement;

    /**
     * Gets the transfert paiement.
     *
     * @return the transfertPaiement
     */
    public TransfertPaiement getTransfertPaiement() {
        return this.transfertPaiement;
    }

    /**
     * Sets the transfert paiement.
     *
     * @param transfertPaiement            the transfertPaiement to set
     */
    public void setTransfertPaiement(TransfertPaiement transfertPaiement) {
        this.transfertPaiement = transfertPaiement;
    }

    /**
     * Gets the list demande paiement.
     *
     * @return the listDemandePaiement
     */
    public List<DemandePaiement> getListDemandePaiement() {
        return this.listDemandePaiement;
    }

    /**
     * Adds the demande paiement.
     *
     * @param demandePaiement the demande paiement
     */
    public void addDemandePaiement(DemandePaiement demandePaiement) {
        this.listDemandePaiement.add(demandePaiement);
    }

}

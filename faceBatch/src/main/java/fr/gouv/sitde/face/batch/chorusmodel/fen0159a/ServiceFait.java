
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'un service fait dans CHORUS
 *
 * <p>
 * Classe Java pour ServiceFaitType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ServiceFaitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="COMP_CODE" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *         &lt;element name="FISC_YEAR" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceFaitType", propOrder = { "idChorus", "codeSociete", "anneeFiscale" })
public class ServiceFait implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "COMP_CODE", required = true)
    private String codeSociete;
    @XmlElement(name = "FISC_YEAR", required = true)
    private String anneeFiscale;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété codeSociete.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getCodeSociete() {
        return this.codeSociete;
    }

    /**
     * Définit la valeur de la propriété codeSociete.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCodeSociete(String value) {
        this.codeSociete = value;
    }

    /**
     * Obtient la valeur de la propriété anneeFiscale.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getAnneeFiscale() {
        return this.anneeFiscale;
    }

    /**
     * Définit la valeur de la propriété anneeFiscale.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAnneeFiscale(String value) {
        this.anneeFiscale = value;
    }

}

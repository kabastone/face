
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour TypeLigneType.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeLigneType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Z_LG_SANSF"/&gt;
 *     &lt;enumeration value="Z_LG_SFAUT"/&gt;
 *     &lt;enumeration value="Z_LG_COND"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeLigneType")
@XmlEnum
public enum TypeLigneType {


    /**
     * Ligne de gestion sans service fait (flux 3)
     * 
     */
    Z_LG_SANSF,

    /**
     * Ligne de gestion service fait auto (flux 2)
     * 
     */
    Z_LG_SFAUT,

    /**
     * Ligne de gestion avec condition de réalisation (flux 1)
     * 
     */
    Z_LG_COND;

    public String value() {
        return name();
    }

    public static TypeLigneType fromValue(String v) {
        return valueOf(v);
    }

}

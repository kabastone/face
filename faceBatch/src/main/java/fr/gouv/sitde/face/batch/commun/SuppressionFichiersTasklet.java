/**
 *
 */
package fr.gouv.sitde.face.batch.commun;

import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

/**
 * The Class SuppressionFichiersTasklet.
 *
 * @author a453029
 */
public class SuppressionFichiersTasklet implements Tasklet {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SuppressionFichiersTasklet.class);

    /** The resources. */
    private Resource[] resources;

    /**
     * Execute.
     *
     * @param contribution
     *            the contribution
     * @param chunkContext
     *            the chunk context
     * @return the repeat status
     * @throws Exception
     *             the exception
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        int nbFichiersSupprimes = 0;

        for (Resource resource : this.resources) {
            try {
                Files.delete(resource.getFile().toPath());
                nbFichiersSupprimes++;
            } catch (IOException e) {
                throw new UnexpectedJobExecutionException("Impossible de supprimer le fichier " + resource.getFilename(), e);
            }
        }
        LOGGER.debug("{} fichiers ont été supprimés", nbFichiersSupprimes);
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the resources.
     *
     * @param resources
     *            the new resources
     */
    public void setResources(Resource[] resources) {
        this.resources = resources;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.emailexpiration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Class CadencementMajBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailExpirationBatchConfiguration {

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /**
     * Dossier subvention expiration reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<DossierSubvention> dossierSubventionExpirationReader() {
        List<DossierSubvention> listeDossierSubvention = this.dossierSubventionService.rechercherDossiersPourMailExpiration();
        return new ListItemReader<>(listeDossierSubvention);
    }

    /**
     * Email expiration writer.
     *
     * @return the email expiration writer
     */
    @Bean
    @StepScope
    public EmailExpirationWriter emailExpirationWriter() {
        return new EmailExpirationWriter();
    }

    @Bean
    @StepScope
    public ExpirationMajProcessor expirationMajProcessor() {
        return new ExpirationMajProcessor();
    }

    /**
     * Step email expiration.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEmailExpiration() {
        return this.stepBuilderFactory.get("step_email_expiration").<DossierSubvention, DossierSubvention> chunk(1)
                .reader(this.dossierSubventionExpirationReader()).processor(this.expirationMajProcessor()).writer(this.emailExpirationWriter())
                .build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.EMAIL_EXPIRATION_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.EMAIL_EXPIRATION_JOB.getNom()).start(this.stepEmailExpiration()).build();
    }
}

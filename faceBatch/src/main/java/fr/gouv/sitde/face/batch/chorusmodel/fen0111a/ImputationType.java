
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour ImputationType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ImputationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TypeImputation" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}TypeImputationType" minOccurs="0"/&gt;
 *         &lt;element name="CentreCouts" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NumPrincImmo" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="12"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NumPieceReserCredits" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PostePieceReserCredits" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OTP" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CentreFinancier" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="14"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Fonds" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DomaineFonctionnel" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Activite" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TrancheFonctionnelle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LocalisationInterministerielle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProjetAnalytiqueMinisteriel" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="22"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LocalisationMinisterielle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="22"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NatureDetailleeMinisterielle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="11"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CPER" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="15"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NumPRESAGE" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="15"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AxeMinisteriel1" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="22"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AxeMinisteriel2" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="22"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImputationType",
        propOrder = { "typeImputation", "centreCouts", "numPrincImmo", "numPieceReserCredits", "postePieceReserCredits", "otp", "centreFinancier",
                "fonds", "domaineFonctionnel", "activite", "trancheFonctionnelle", "localisationInterministerielle", "projetAnalytiqueMinisteriel",
                "localisationMinisterielle", "natureDetailleeMinisterielle", "cper", "numPRESAGE", "axeMinisteriel1", "axeMinisteriel2" })
public class ImputationType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "TypeImputation")
    @XmlSchemaType(name = "string")
    private TypeImputationType typeImputation;
    @XmlElement(name = "CentreCouts")
    private String centreCouts;
    @XmlElement(name = "NumPrincImmo")
    private String numPrincImmo;
    @XmlElement(name = "NumPieceReserCredits")
    private String numPieceReserCredits;
    @XmlElement(name = "PostePieceReserCredits")
    private String postePieceReserCredits;
    @XmlElement(name = "OTP")
    private String otp;
    @XmlElement(name = "CentreFinancier")
    private String centreFinancier;
    @XmlElement(name = "Fonds")
    private String fonds;
    @XmlElement(name = "DomaineFonctionnel")
    private String domaineFonctionnel;
    @XmlElement(name = "Activite")
    private String activite;
    @XmlElement(name = "TrancheFonctionnelle")
    private String trancheFonctionnelle;
    @XmlElement(name = "LocalisationInterministerielle")
    private String localisationInterministerielle;
    @XmlElement(name = "ProjetAnalytiqueMinisteriel")
    private String projetAnalytiqueMinisteriel;
    @XmlElement(name = "LocalisationMinisterielle")
    private String localisationMinisterielle;
    @XmlElement(name = "NatureDetailleeMinisterielle")
    private String natureDetailleeMinisterielle;
    @XmlElement(name = "CPER")
    private String cper;
    @XmlElement(name = "NumPRESAGE")
    private String numPRESAGE;
    @XmlElement(name = "AxeMinisteriel1")
    private String axeMinisteriel1;
    @XmlElement(name = "AxeMinisteriel2")
    private String axeMinisteriel2;

    /**
     * Obtient la valeur de la propriété typeImputation.
     *
     * @return possible object is {@link TypeImputationType }
     *
     */
    public TypeImputationType getTypeImputation() {
        return this.typeImputation;
    }

    /**
     * Définit la valeur de la propriété typeImputation.
     *
     * @param value
     *            allowed object is {@link TypeImputationType }
     *
     */
    public void setTypeImputation(TypeImputationType value) {
        this.typeImputation = value;
    }

    /**
     * Obtient la valeur de la propriété centreCouts.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCentreCouts() {
        return this.centreCouts;
    }

    /**
     * Définit la valeur de la propriété centreCouts.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCentreCouts(String value) {
        this.centreCouts = value;
    }

    /**
     * Obtient la valeur de la propriété numPrincImmo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumPrincImmo() {
        return this.numPrincImmo;
    }

    /**
     * Définit la valeur de la propriété numPrincImmo.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumPrincImmo(String value) {
        this.numPrincImmo = value;
    }

    /**
     * Obtient la valeur de la propriété numPieceReserCredits.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumPieceReserCredits() {
        return this.numPieceReserCredits;
    }

    /**
     * Définit la valeur de la propriété numPieceReserCredits.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumPieceReserCredits(String value) {
        this.numPieceReserCredits = value;
    }

    /**
     * Obtient la valeur de la propriété postePieceReserCredits.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPostePieceReserCredits() {
        return this.postePieceReserCredits;
    }

    /**
     * Définit la valeur de la propriété postePieceReserCredits.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setPostePieceReserCredits(String value) {
        this.postePieceReserCredits = value;
    }

    /**
     * Obtient la valeur de la propriété otp.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOTP() {
        return this.otp;
    }

    /**
     * Définit la valeur de la propriété otp.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setOTP(String value) {
        this.otp = value;
    }

    /**
     * Obtient la valeur de la propriété centreFinancier.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCentreFinancier() {
        return this.centreFinancier;
    }

    /**
     * Définit la valeur de la propriété centreFinancier.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCentreFinancier(String value) {
        this.centreFinancier = value;
    }

    /**
     * Obtient la valeur de la propriété fonds.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFonds() {
        return this.fonds;
    }

    /**
     * Définit la valeur de la propriété fonds.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setFonds(String value) {
        this.fonds = value;
    }

    /**
     * Obtient la valeur de la propriété domaineFonctionnel.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDomaineFonctionnel() {
        return this.domaineFonctionnel;
    }

    /**
     * Définit la valeur de la propriété domaineFonctionnel.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDomaineFonctionnel(String value) {
        this.domaineFonctionnel = value;
    }

    /**
     * Obtient la valeur de la propriété activite.
     *
     * @return possible object is {@link String }
     *
     */
    public String getActivite() {
        return this.activite;
    }

    /**
     * Définit la valeur de la propriété activite.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setActivite(String value) {
        this.activite = value;
    }

    /**
     * Obtient la valeur de la propriété trancheFonctionnelle.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTrancheFonctionnelle() {
        return this.trancheFonctionnelle;
    }

    /**
     * Définit la valeur de la propriété trancheFonctionnelle.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setTrancheFonctionnelle(String value) {
        this.trancheFonctionnelle = value;
    }

    /**
     * Obtient la valeur de la propriété localisationInterministerielle.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLocalisationInterministerielle() {
        return this.localisationInterministerielle;
    }

    /**
     * Définit la valeur de la propriété localisationInterministerielle.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setLocalisationInterministerielle(String value) {
        this.localisationInterministerielle = value;
    }

    /**
     * Obtient la valeur de la propriété projetAnalytiqueMinisteriel.
     *
     * @return possible object is {@link String }
     *
     */
    public String getProjetAnalytiqueMinisteriel() {
        return this.projetAnalytiqueMinisteriel;
    }

    /**
     * Définit la valeur de la propriété projetAnalytiqueMinisteriel.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setProjetAnalytiqueMinisteriel(String value) {
        this.projetAnalytiqueMinisteriel = value;
    }

    /**
     * Obtient la valeur de la propriété localisationMinisterielle.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLocalisationMinisterielle() {
        return this.localisationMinisterielle;
    }

    /**
     * Définit la valeur de la propriété localisationMinisterielle.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setLocalisationMinisterielle(String value) {
        this.localisationMinisterielle = value;
    }

    /**
     * Obtient la valeur de la propriété natureDetailleeMinisterielle.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNatureDetailleeMinisterielle() {
        return this.natureDetailleeMinisterielle;
    }

    /**
     * Définit la valeur de la propriété natureDetailleeMinisterielle.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNatureDetailleeMinisterielle(String value) {
        this.natureDetailleeMinisterielle = value;
    }

    /**
     * Obtient la valeur de la propriété cper.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCPER() {
        return this.cper;
    }

    /**
     * Définit la valeur de la propriété cper.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCPER(String value) {
        this.cper = value;
    }

    /**
     * Obtient la valeur de la propriété numPRESAGE.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumPRESAGE() {
        return this.numPRESAGE;
    }

    /**
     * Définit la valeur de la propriété numPRESAGE.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumPRESAGE(String value) {
        this.numPRESAGE = value;
    }

    /**
     * Obtient la valeur de la propriété axeMinisteriel1.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAxeMinisteriel1() {
        return this.axeMinisteriel1;
    }

    /**
     * Définit la valeur de la propriété axeMinisteriel1.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setAxeMinisteriel1(String value) {
        this.axeMinisteriel1 = value;
    }

    /**
     * Obtient la valeur de la propriété axeMinisteriel2.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAxeMinisteriel2() {
        return this.axeMinisteriel2;
    }

    /**
     * Définit la valeur de la propriété axeMinisteriel2.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setAxeMinisteriel2(String value) {
        this.axeMinisteriel2 = value;
    }

}

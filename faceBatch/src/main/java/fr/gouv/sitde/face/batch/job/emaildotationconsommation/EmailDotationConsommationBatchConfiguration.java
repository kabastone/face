/**
 *
 */
package fr.gouv.sitde.face.batch.job.emaildotationconsommation;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class EmailDotationConsommationBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailDotationConsommationBatchConfiguration {

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /**
     * Collectivite reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<Collectivite> collectiviteReader() {
        List<Collectivite> listeCollectivite = this.collectiviteService.rechercherToutesCollectivitesPourMail();
        return new ListItemReader<>(listeCollectivite);
    }

    /**
     * Email consommation writer.
     *
     * @return the email consommation writer
     */
    @Bean
    @StepScope
    public EmailConsommationWriter emailConsommationWriter() {
        return new EmailConsommationWriter();
    }

    /**
     * Step email consommation.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEmailConsommation() {
        return this.stepBuilderFactory.get("step_email_relance").<Collectivite, Collectivite> chunk(1).reader(this.collectiviteReader())
                .writer(this.emailConsommationWriter()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.EMAIL_CONSOMMATION_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.EMAIL_CONSOMMATION_JOB.getNom()).start(this.stepEmailConsommation()).build();
    }
}

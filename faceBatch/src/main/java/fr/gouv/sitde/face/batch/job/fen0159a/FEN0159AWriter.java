/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0159a;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159A;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159ADTO;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class FEN0159AWriter.
 */
public class FEN0159AWriter implements ItemStreamWriter<FEN0159ADTO> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FEN0159AWriter.class);

    /** The Constant NB_CHIFFRES_NOM_FICHIERS. */
    private static final int NB_CHIFFRES_NOM_FICHIERS = 5;

    /** The Constant INIT_MAP_SIZE. */
    private static final int INIT_MAP_SIZE = 5;

    /** The Constant FILE_NAME_SEPARATOR. */
    private static final char FILE_NAME_SEPARATOR = '_';

    /** The chemin output. */
    private String cheminOutput;

    /** The delegates. */
    private Map<String, StaxWriterCustomFEN0159A> delegates = new HashMap<>(INIT_MAP_SIZE);

    /** The filenames. */
    private List<String> filenames = new ArrayList<>(INIT_MAP_SIZE);

    /** The list execution context. */
    private Map<String, ExecutionContext> listExecutionContext = new HashMap<>(INIT_MAP_SIZE);

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /** The chorus suivi batch service. */
    @Inject
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /**
     * Instantiates a new FEN 0159 A writer.
     *
     * @param cheminOutput
     *            the chemin output
     */
    public FEN0159AWriter(String cheminOutput) {
        super();
        this.cheminOutput = cheminOutput;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends FEN0159ADTO> items) throws Exception {
        for (FEN0159ADTO dto : items) {
            this.demandePaiementService.majDemandePaiementEnCoursTransfertEtapeFin(dto.getDemandePaiement().getId());

            StaxWriterCustomFEN0159A writer = this.creerWriter();
            List<FEN0159A> fenListe = new ArrayList<>(1);
            fenListe.add(dto.getFen0159a());
            writer.write(fenListe);
        }
    }

    /**
     * Creer writer.
     *
     * @return the stax event item writer<? super rattachement P j>
     */
    private StaxWriterCustomFEN0159A creerWriter() {
        String fileName = this.genererNomFichier();
        File file = new File(fileName);
        try {
            if (!file.createNewFile()) {
                LOGGER.debug("Le fichier {} existe déjà.", fileName);
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
        if (!file.setWritable(true)) {
            throw new TechniqueException("Ecriture impossible dans le fichier");
        }
        FileSystemResource fsr = new FileSystemResource(fileName);
        StaxWriterCustomFEN0159A staxEventItemWriter = new StaxWriterCustomFEN0159A();
        staxEventItemWriter.setResource(fsr);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(FEN0159A.class);
        staxEventItemWriter.setRootTagName("FEN0159A");
        staxEventItemWriter.setMarshaller(marshaller);
        staxEventItemWriter.setEncoding(ConstantesFace.ISO_8859_15);

        staxEventItemWriter.setOverwriteOutput(true);
        // Ouverture du writer.
        ExecutionContext exec = new ExecutionContext();
        staxEventItemWriter.open(exec);

        // Pour conserver afin de fermer tout les writers créés après coup.
        this.filenames.add(fileName);
        this.delegates.put(fileName, staxEventItemWriter);
        this.listExecutionContext.put(fileName, exec);
        return staxEventItemWriter;
    }

    /**
     * Generer nom fichier.
     *
     * @return the string
     */
    private String genererNomFichier() {

        FluxChorus referentiel = this.fluxChorusService.rechercherFluxChorus();

        StringBuilder builder = new StringBuilder(this.cheminOutput);
        builder.append(File.separator);
        builder.append("FEN0159A");
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append("0159");
        builder.append(this.getNombreFichiers());
        return builder.toString();
    }

    /**
     * Gets the nombre fichiers.
     *
     * @return the nombre fichiers
     */
    private String getNombreFichiers() {

        ChorusSuiviBatch suivi = this.chorusSuiviBatchService.lireSuiviBatch();
        int nbFichiers = suivi.getFen0159a();

        if (nbFichiers == ConstantesFace.MAX_DOCUMENTS_CHORUS) {
            nbFichiers = 1;
        } else {
            nbFichiers++;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(nbFichiers);
        while (builder.length() < NB_CHIFFRES_NOM_FICHIERS) {
            builder.insert(0, "0");
        }
        suivi.setFen0159a(nbFichiers);
        this.chorusSuiviBatchService.mettreAJourSuiviBatch(suivi);

        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#open(org.springframework.batch.item.ExecutionContext)
     */
    @Override
    public void open(ExecutionContext executionContext) {
        // Ne fait rien car il délègue le travail à d'autres writers qui ont leur propre executionContext.
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#update(org.springframework.batch.item.ExecutionContext)
     */
    @Override
    public void update(ExecutionContext executionContext) {
        if (this.delegates != null) {
            for (String filename : this.filenames) {
                StaxWriterCustomFEN0159A writer = this.delegates.get(filename);
                ExecutionContext exec = this.listExecutionContext.get(filename);
                writer.update(exec);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#close()
     */
    @Override
    public void close() {
        if ((this.delegates != null) && (this.filenames != null)) {
            for (String filename : this.filenames) {
                StaxWriterCustomFEN0159A writer = this.delegates.get(filename);
                writer.close();

                this.delegates.remove(filename);
            }
        }
    }

}

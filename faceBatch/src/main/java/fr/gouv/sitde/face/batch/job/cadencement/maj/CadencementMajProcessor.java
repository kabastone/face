/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.maj;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * @author A754839
 *
 */
public class CadencementMajProcessor implements ItemProcessor<Collectivite, CadencementMajDto> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CadencementMajProcessor.class);

    @Autowired
    private CadencementService cadencementService;

    @Autowired
    private TimeOperationTransverse timeUtils;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public CadencementMajDto process(Collectivite collectivite) {

        CadencementMajDto result = new CadencementMajDto();
        List<Cadencement> listeCadencementPourTraitement = this.cadencementService.rechercherCadencementsPourMaj(collectivite.getId());
        listeCadencementPourTraitement.forEach(this::mettreAJourCadencement);
        result.setCadencements(listeCadencementPourTraitement);
        result.setCollectivite(collectivite);
        return result;
    }

    /**
     * Modifier montant cadencement.
     *
     * @param cadencement
     *            the cadencement
     */
    private void mettreAJourCadencement(Cadencement cadencement) {
        BigDecimal nouveauMontant = cadencement.getDemandeSubvention().getTransferts().stream().filter(this::verifierConditionDemandePaiement)
                .map(TransfertPaiement::getMontant).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        this.remplirMontantSelonAnneeCadencement(cadencement, nouveauMontant);
        this.verifierValiditeCadencement(cadencement);

    }

    /**
     * @param cadencement
     */
    private void verifierValiditeCadencement(Cadencement cadencement) {

        if (this.aUnSoldeOuExpire(cadencement)) {
            cadencement.setEstValide(true);
            return;
        }

        cadencement
                .setEstValide(this.cadencementService.isMontantCadencementValide(cadencement, cadencement.getDemandeSubvention().getPlafondAide()));
    }

    /**
     * A un solde.
     *
     * @param cadencement
     *            the cadencement
     * @return true, if successful
     */
    public boolean aUnSolde(Cadencement cadencement) {
        return cadencement.getDemandeSubvention().getDossierSubvention().getDemandesPaiement().stream().map(DemandePaiement::getTypeDemandePaiement)
                .anyMatch(TypeDemandePaiementEnum.SOLDE::equals);
    }

    /**
     * @param cadencement
     * @return
     */
    public boolean aUnSoldeOuExpire(Cadencement cadencement) {
        return this.aUnSolde(cadencement)
                || EtatDossierEnum.EXPIRE.equals(cadencement.getDemandeSubvention().getDossierSubvention().getEtatDossier());
    }

    /**
     * @param cadencement
     * @param nouveauMontant
     */
    private void remplirMontantSelonAnneeCadencement(Cadencement cadencement, BigDecimal nouveauMontant) {

        int differentielAnnee = ((this.timeUtils.getAnneeCourante() - 1) - cadencement.getDemandeSubvention().getDossierSubvention()
                .getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());

        // On verifie a quelle annee correspond le dossier pour remplir la bonne case du cadencement.
        switch (differentielAnnee) {
            case 0:
                cadencement.setMontantAnnee1(nouveauMontant);
                break;
            case 1:
                cadencement.setMontantAnnee2(nouveauMontant);
                break;
            case 2:
                cadencement.setMontantAnnee3(nouveauMontant);
                break;
            case 3:
                cadencement.setMontantAnnee4(nouveauMontant);
                break;
            case 4:
                cadencement.setMontantAnneeProlongation(nouveauMontant);
                break;
            default:
                LOGGER.warn("Cadencement non valide :  annee de programmation du dossier : {}",
                        cadencement.getDemandeSubvention().getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                                .getDotationSousProgramme().getDotationProgramme().getAnnee());
        }

        if (this.aUnSolde(cadencement)) {
            // On verifie a quelle annee correspond le dossier pour remplir la bonne case du cadencement.
            CadencementMajProcessor.gererSoldeSelonAnnee(cadencement, differentielAnnee);
        }
    }

    /**
     * Gerer solde selon annee.
     *
     * @param cadencement
     *            the cadencement
     * @param differentielAnnee
     *            the differentiel annee
     */
    private static void gererSoldeSelonAnnee(Cadencement cadencement, int differentielAnnee) {
        switch (differentielAnnee) {
            case 0:
                cadencement.setMontantAnnee2(BigDecimal.ZERO);
                cadencement.setMontantAnnee3(BigDecimal.ZERO);
                cadencement.setMontantAnnee4(BigDecimal.ZERO);
                cadencement.setMontantAnneeProlongation(BigDecimal.ZERO);
                break;
            case 1:
                cadencement.setMontantAnnee3(BigDecimal.ZERO);
                cadencement.setMontantAnnee4(BigDecimal.ZERO);
                cadencement.setMontantAnneeProlongation(BigDecimal.ZERO);
                break;
            case 2:
                cadencement.setMontantAnnee4(BigDecimal.ZERO);
                cadencement.setMontantAnneeProlongation(BigDecimal.ZERO);
                break;
            case 3:
                cadencement.setMontantAnneeProlongation(BigDecimal.ZERO);
                break;
            case 4:
                break;
            default:
                LOGGER.warn("Solde non calculable:  annee de programmation du dossier : {}", cadencement.getDemandeSubvention().getDossierSubvention()
                        .getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
        }
    }

    /**
     * Verifier condition demande paiement.
     *
     * @param transfert
     *            the transfert
     * @return the boolean
     */
    private Boolean verifierConditionDemandePaiement(TransfertPaiement transfert) {
        return transfert.getDemandePaiement().getAnneeFiscale().equals(this.timeUtils.getAnneeCourante() - 1)
                && transfert.getDemandePaiement().getEtatDemandePaiement().getCode().equals(EtatDemandePaiementEnum.VERSEE.name());
    }

}

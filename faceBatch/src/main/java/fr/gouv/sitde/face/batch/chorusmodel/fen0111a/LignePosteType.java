
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.gouv.sitde.face.batch.chorusmodel.adapter.BooleanAdapter;
import fr.gouv.sitde.face.batch.chorusmodel.adapter.CustomLocalDateTimeAdapter;

/**
 * <p>
 * Classe Java pour LignePosteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="LignePosteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdLigne"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}long"&gt;
 *               &lt;maxInclusive value="9999999999"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Description" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Categorie" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateLivraison" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Quantite" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;totalDigits value="16"/&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *               &lt;minExclusive value="0"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Unite" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}UniteType" minOccurs="0"/&gt;
 *         &lt;element name="CodeTVA" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TypeLigne" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}TypeLigneType" minOccurs="0"/&gt;
 *         &lt;element name="TypeOption" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ServiceFaitAutoFlux2" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Imputation" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}ImputationType" minOccurs="0"/&gt;
 *         &lt;element name="Site" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireType" minOccurs="0"/&gt;
 *         &lt;element name="TiersBeneficiaire" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireType" minOccurs="0"/&gt;
 *         &lt;element name="Demandeur" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LignePosteType", propOrder = { "idLigne", "description", "categorie", "dateLivraison", "quantite", "unite", "codeTVA", "typeLigne",
        "typeOption", "serviceFaitAutoFlux2", "imputation", "site", "tiersBeneficiaire", "demandeur" })
public class LignePosteType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "IdLigne")
    private long idLigne;
    @XmlElement(name = "Description")
    private String description;
    @XmlElement(name = "Categorie")
    private String categorie;
    @XmlElement(name = "DateLivraison", type = String.class)
    @XmlJavaTypeAdapter(CustomLocalDateTimeAdapter.class)
    @XmlSchemaType(name = "date")
    private LocalDateTime dateLivraison;
    @XmlElement(name = "Quantite")
    private BigDecimal quantite;
    @XmlElement(name = "Unite")
    @XmlSchemaType(name = "string")
    private UniteType unite;
    @XmlElement(name = "CodeTVA")
    private String codeTVA;
    @XmlElement(name = "TypeLigne")
    @XmlSchemaType(name = "string")
    private TypeLigneType typeLigne;
    @XmlElement(name = "TypeOption", type = Integer.class)
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private Boolean typeOption;
    @XmlElement(name = "ServiceFaitAutoFlux2", type = Integer.class)
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private Boolean serviceFaitAutoFlux2;
    @XmlElement(name = "Imputation")
    private ImputationType imputation;
    @XmlElement(name = "Site")
    private PartenaireType site;
    @XmlElement(name = "TiersBeneficiaire")
    private PartenaireType tiersBeneficiaire;
    @XmlElement(name = "Demandeur")
    private PartenaireType demandeur;

    /**
     * Obtient la valeur de la propriété idLigne.
     *
     */
    public long getIdLigne() {
        return this.idLigne;
    }

    /**
     * Définit la valeur de la propriété idLigne.
     *
     */
    public void setIdLigne(long value) {
        this.idLigne = value;
    }

    /**
     * Obtient la valeur de la propriété description.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Définit la valeur de la propriété description.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtient la valeur de la propriété categorie.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCategorie() {
        return this.categorie;
    }

    /**
     * Définit la valeur de la propriété categorie.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCategorie(String value) {
        this.categorie = value;
    }

    /**
     * Obtient la valeur de la propriété dateLivraison.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDateTime getDateLivraison() {
        return this.dateLivraison;
    }

    /**
     * Définit la valeur de la propriété dateLivraison.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateLivraison(LocalDateTime value) {
        this.dateLivraison = value;
    }

    /**
     * Obtient la valeur de la propriété quantite.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getQuantite() {
        return this.quantite;
    }

    /**
     * Définit la valeur de la propriété quantite.
     *
     * @param value
     *            allowed object is {@link BigDecimal }
     *
     */
    public void setQuantite(BigDecimal value) {
        this.quantite = value;
    }

    /**
     * Obtient la valeur de la propriété unite.
     *
     * @return possible object is {@link UniteType }
     *
     */
    public UniteType getUnite() {
        return this.unite;
    }

    /**
     * Définit la valeur de la propriété unite.
     *
     * @param value
     *            allowed object is {@link UniteType }
     *
     */
    public void setUnite(UniteType value) {
        this.unite = value;
    }

    /**
     * Obtient la valeur de la propriété codeTVA.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeTVA() {
        return this.codeTVA;
    }

    /**
     * Définit la valeur de la propriété codeTVA.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeTVA(String value) {
        this.codeTVA = value;
    }

    /**
     * Obtient la valeur de la propriété typeLigne.
     *
     * @return possible object is {@link TypeLigneType }
     *
     */
    public TypeLigneType getTypeLigne() {
        return this.typeLigne;
    }

    /**
     * Définit la valeur de la propriété typeLigne.
     *
     * @param value
     *            allowed object is {@link TypeLigneType }
     *
     */
    public void setTypeLigne(TypeLigneType value) {
        this.typeLigne = value;
    }

    /**
     * Obtient la valeur de la propriété typeOption.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isTypeOption() {
        return this.typeOption;
    }

    /**
     * Définit la valeur de la propriété typeOption.
     *
     * @param value
     *            allowed object is {@link Boolean }
     *
     */
    public void setTypeOption(Boolean value) {
        this.typeOption = value;
    }

    /**
     * Obtient la valeur de la propriété serviceFaitAutoFlux2.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isServiceFaitAutoFlux2() {
        return this.serviceFaitAutoFlux2;
    }

    /**
     * Définit la valeur de la propriété serviceFaitAutoFlux2.
     *
     * @param value
     *            allowed object is {@link Boolean }
     *
     */
    public void setServiceFaitAutoFlux2(Boolean value) {
        this.serviceFaitAutoFlux2 = value;
    }

    /**
     * Obtient la valeur de la propriété imputation.
     *
     * @return possible object is {@link ImputationType }
     *
     */
    public ImputationType getImputation() {
        return this.imputation;
    }

    /**
     * Définit la valeur de la propriété imputation.
     *
     * @param value
     *            allowed object is {@link ImputationType }
     *
     */
    public void setImputation(ImputationType value) {
        this.imputation = value;
    }

    /**
     * Obtient la valeur de la propriété site.
     *
     * @return possible object is {@link PartenaireType }
     *
     */
    public PartenaireType getSite() {
        return this.site;
    }

    /**
     * Définit la valeur de la propriété site.
     *
     * @param value
     *            allowed object is {@link PartenaireType }
     *
     */
    public void setSite(PartenaireType value) {
        this.site = value;
    }

    /**
     * Obtient la valeur de la propriété tiersBeneficiaire.
     *
     * @return possible object is {@link PartenaireType }
     *
     */
    public PartenaireType getTiersBeneficiaire() {
        return this.tiersBeneficiaire;
    }

    /**
     * Définit la valeur de la propriété tiersBeneficiaire.
     *
     * @param value
     *            allowed object is {@link PartenaireType }
     *
     */
    public void setTiersBeneficiaire(PartenaireType value) {
        this.tiersBeneficiaire = value;
    }

    /**
     * Obtient la valeur de la propriété demandeur.
     *
     * @return possible object is {@link PartenaireType }
     *
     */
    public PartenaireType getDemandeur() {
        return this.demandeur;
    }

    /**
     * Définit la valeur de la propriété demandeur.
     *
     * @param value
     *            allowed object is {@link PartenaireType }
     *
     */
    public void setDemandeur(PartenaireType value) {
        this.demandeur = value;
    }

}

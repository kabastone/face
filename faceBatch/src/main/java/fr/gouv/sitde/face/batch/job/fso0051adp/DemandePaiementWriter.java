/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051adp;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * The Class DemandePaiementWriter.
 *
 * @author Atos
 */
public class DemandePaiementWriter implements ItemWriter<DemandePaiementChorus> {

    /** The transfert paiement service. */
    @Inject
    private TransfertPaiementService transfertPaiementService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends DemandePaiementChorus> items) throws Exception {
        for (DemandePaiementChorus demandePaiementChorus : items) {
            // save TransfertPaiement
            TransfertPaiement transfertPaiement = demandePaiementChorus.getTransfertPaiement();
            if (transfertPaiement != null) {
                this.transfertPaiementService.saveTransfertPaiement(transfertPaiement);
            }

            // save Demande Paiement
            List<DemandePaiement> listDemandePaiement = demandePaiementChorus.getListDemandePaiement();
            for (DemandePaiement demandePaiement : listDemandePaiement) {
                this.demandePaiementService.majDemandePaiementVersee(demandePaiement.getId());
            }

        }

    }

}

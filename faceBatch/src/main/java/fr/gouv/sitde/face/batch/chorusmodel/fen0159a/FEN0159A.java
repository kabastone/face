
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Rattachement des PJ a des objets metiers CHORUS
 *
 * <p>
 * Classe Java pour FEN0159AType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="FEN0159AType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RattachementPJ" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}RattachementPJType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "FEN0159A")
@XmlType(name = "FEN0159AType", propOrder = { "pieceJointe" })
public class FEN0159A implements Serializable {

    /** The xml namespace. */
    @XmlAttribute(name = "xmlns")
    private final String XML_NAMESPACE = "http://www.finances.gouv.fr/aife/chorus/PJ/";

    /** The xsi schema location. */
    @XmlAttribute(name = "xsi:schemaLocation")
    private final String XSI_SCHEMA_LOCATION = "http://www.finances.gouv.fr/aife/chorus/PJ/XSD_FEN0159A_v1.7.xsd";

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "RattachementPJ", required = true)
    private List<RattachementPJ> pieceJointe;

    /**
     * Gets the value of the pieceJointe property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the pieceJointe property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPieceJointe().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link RattachementPJ }
     *
     *
     */
    public List<RattachementPJ> getPieceJointe() {
        if (this.pieceJointe == null) {
            this.pieceJointe = new ArrayList<>();
        }
        return this.pieceJointe;
    }

    /**
     * @param pieceJointe
     *            the pieceJointe to set
     */
    public void setPieceJointe(List<RattachementPJ> pieceJointe) {
        this.pieceJointe = pieceJointe;
    }

}

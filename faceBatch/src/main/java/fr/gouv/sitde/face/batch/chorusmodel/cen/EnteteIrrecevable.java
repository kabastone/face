
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * Ligne recapitulative pour un fichier irrecevable (signature non valide ou rejet total par le traitement)
 *
 * <p>
 * Classe Java pour EnteteIrrecevableType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EnteteIrrecevableType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TYPE_LIGNE" type="{Compte-rendu_20}TypeLigneIrrType"/&gt;
 *         &lt;element name="ID_FICHIER_EMETTEUR" type="{Compte-rendu_20}IdFichierEmetteurType"/&gt;
 *         &lt;element name="CODE_ERREUR" type="{Compte-rendu_20}CodeErreurType"/&gt;
 *         &lt;element name="LIBELLE_ERREUR" type="{Compte-rendu_20}LibelleErreurType"/&gt;
 *         &lt;element name="DATETIME" type="{Compte-rendu_20}DateTimeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ENTETE_IRRECEVABLE")
@XmlType(name = "EnteteIrrecevableType", propOrder = { "typeLigne", "idFichierEmetteur", "codeErreur", "libelleErreur", "dateTime" })
public class EnteteIrrecevable implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type ligne. */
    @XmlElement(name = "TYPE_LIGNE", required = true)
    @XmlSchemaType(name = "string")
    private TypeLigneIrrEnum typeLigne;

    /** The id fichier emetteur. */
    @XmlElement(name = "ID_FICHIER_EMETTEUR", required = true)
    private String idFichierEmetteur;

    /** The code erreur. */
    @XmlElement(name = "CODE_ERREUR", required = true)
    private String codeErreur;

    /** The libelle erreur. */
    @XmlElement(name = "LIBELLE_ERREUR", required = true)
    private String libelleErreur;

    /** The date time. */
    @XmlElement(name = "DATETIME", required = true)
    private String dateTime;

    /**
     * Obtient la valeur de la propriété typeLigne.
     *
     * @return possible object is {@link TypeLigneIrrEnum }
     *
     */
    public TypeLigneIrrEnum getTypeLigne() {
        return this.typeLigne;
    }

    /**
     * Définit la valeur de la propriété typeLigne.
     *
     * @param value
     *            allowed object is {@link TypeLigneIrrEnum }
     *
     */
    public void setTypeLigne(TypeLigneIrrEnum value) {
        this.typeLigne = value;
    }

    /**
     * Obtient la valeur de la propriété idFichierEmetteur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdFichierEmetteur() {
        return this.idFichierEmetteur;
    }

    /**
     * Définit la valeur de la propriété idFichierEmetteur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdFichierEmetteur(String value) {
        this.idFichierEmetteur = value;
    }

    /**
     * Obtient la valeur de la propriété codeErreur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeErreur() {
        return this.codeErreur;
    }

    /**
     * Définit la valeur de la propriété codeErreur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeErreur(String value) {
        this.codeErreur = value;
    }

    /**
     * Obtient la valeur de la propriété libelleErreur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLibelleErreur() {
        return this.libelleErreur;
    }

    /**
     * Définit la valeur de la propriété libelleErreur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setLibelleErreur(String value) {
        this.libelleErreur = value;
    }

    /**
     * Obtient la valeur de la propriété dateTime.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDateTime() {
        return this.dateTime;
    }

    /**
     * Définit la valeur de la propriété dateTime.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

}

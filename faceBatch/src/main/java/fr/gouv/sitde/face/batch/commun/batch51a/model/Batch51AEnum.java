/**
 *
 */
package fr.gouv.sitde.face.batch.commun.batch51a.model;

/**
 * The Enum Batch51AEnum.
 *
 * @author Atos
 */
public enum Batch51AEnum {

    /** The batch sf. */
    BATCH_SF,
    /** The batch ej. */
    BATCH_EJ,
    /** The batch dp. */
    BATCH_DP;

}

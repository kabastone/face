
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour EngagementJuridiqueType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EngagementJuridiqueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Entete" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}EnteteType"/&gt;
 *         &lt;element name="EJPartenaireEntete" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}EJPartenaireEnteteType"/&gt;
 *         &lt;element name="LignePoste" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}LignePosteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheRecensement" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}FicheRecensementType" minOccurs="0"/&gt;
 *         &lt;element name="PJ" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PJType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EngagementJuridiqueType", propOrder = { "entete", "ejPartenaireEntete", "lignePostes", "ficheRecensement", "pjs" })
public class EngagementJuridiqueType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Entete", required = true)
    private EnteteType entete;
    @XmlElement(name = "EJPartenaireEntete", required = true)
    private EJPartenaireEnteteType ejPartenaireEntete;
    @XmlElement(name = "LignePoste")
    private List<LignePosteType> lignePostes;
    @XmlElement(name = "FicheRecensement")
    private FicheRecensementType ficheRecensement;
    @XmlElement(name = "PJ")
    private List<PJType> pjs;

    /**
     * Obtient la valeur de la propriété entete.
     *
     * @return possible object is {@link EnteteType }
     *
     */
    public EnteteType getEntete() {
        return this.entete;
    }

    /**
     * Définit la valeur de la propriété entete.
     *
     * @param value
     *            allowed object is {@link EnteteType }
     *
     */
    public void setEntete(EnteteType value) {
        this.entete = value;
    }

    /**
     * Obtient la valeur de la propriété ejPartenaireEntete.
     *
     * @return possible object is {@link EJPartenaireEnteteType }
     *
     */
    public EJPartenaireEnteteType getEJPartenaireEntete() {
        return this.ejPartenaireEntete;
    }

    /**
     * Définit la valeur de la propriété ejPartenaireEntete.
     *
     * @param value
     *            allowed object is {@link EJPartenaireEnteteType }
     *
     */
    public void setEJPartenaireEntete(EJPartenaireEnteteType value) {
        this.ejPartenaireEntete = value;
    }

    /**
     * Gets the value of the lignePostes property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the lignePostes property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getLignePostes().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link LignePosteType }
     *
     *
     */
    public List<LignePosteType> getLignePostes() {
        if (this.lignePostes == null) {
            this.lignePostes = new ArrayList<>();
        }
        return this.lignePostes;
    }

    /**
     * Obtient la valeur de la propriété ficheRecensement.
     *
     * @return possible object is {@link FicheRecensementType }
     *
     */
    public FicheRecensementType getFicheRecensement() {
        return this.ficheRecensement;
    }

    /**
     * Définit la valeur de la propriété ficheRecensement.
     *
     * @param value
     *            allowed object is {@link FicheRecensementType }
     *
     */
    public void setFicheRecensement(FicheRecensementType value) {
        this.ficheRecensement = value;
    }

    /**
     * Gets the value of the pjs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the pjs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPJS().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link PJType }
     * 
     * @return the pjs
     */
    public List<PJType> getPjs() {
        if (this.pjs == null) {
            this.pjs = new ArrayList<>();
        }
        return this.pjs;
    }

    /**
     * @param pjs
     *            the pjs to set
     */
    public void setPjs(List<PJType> pjs) {
        this.pjs = pjs;
    }

    /**
     * @param lignePostes
     *            the lignePostes to set
     */
    public void setLignePostes(List<LignePosteType> lignePostes) {
        this.lignePostes = lignePostes;
    }

}

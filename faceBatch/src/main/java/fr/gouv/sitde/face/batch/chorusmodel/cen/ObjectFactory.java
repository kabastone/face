
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.sitde.face.batch.commun.cenmodel package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.sitde.face.batch.commun.cenmodel
     *
     */
    public ObjectFactory() {
        // constructeur non paramétré
    }

    /**
     * Create an instance of {@link CompteRendu }.
     *
     * @return the compte rendu
     */
    public CompteRendu createCompteRendu() {
        return new CompteRendu();
    }

    /**
     * Create an instance of {@link EnteteRecevable }.
     *
     * @return the entete recevable
     */
    public EnteteRecevable createEnteteRecevable() {
        return new EnteteRecevable();
    }

    /**
     * Create an instance of {@link LigneDossier }.
     *
     * @return the ligne dossier
     */
    public LigneDossier createLigneDossier() {
        return new LigneDossier();
    }

    /**
     * Create an instance of {@link EnteteIrrecevable }.
     *
     * @return the entete irrecevable
     */
    public EnteteIrrecevable createEnteteIrrecevable() {
        return new EnteteIrrecevable();
    }

}

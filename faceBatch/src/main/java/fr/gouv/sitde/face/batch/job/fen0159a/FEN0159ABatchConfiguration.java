/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0159a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159ADTO;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Class FEN0159ABatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class FEN0159ABatchConfiguration {

    /** The chunk size. */
    private int chunkSize = 1;

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /**
     * Fen 0159 a reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<DemandePaiement> fen0159aReader() {
        List<DemandePaiement> listeDemandes = this.demandePaiementService.rechercherToutesDemandesPaiementPourFen0159a();
        return new ListItemReader<>(listeDemandes);
    }

    /**
     * Writer.
     *
     * @return the stax event item writer
     */
    @Bean
    @StepScope
    public FEN0159AWriter fen0159aWriter() {
        return new FEN0159AWriter(this.batchCheminRepertoireProperties.getOutput());
    }

    /**
     * Fen 0159 a processor.
     *
     * @return the FEN 0159 A processor
     */
    @Bean
    @StepScope
    public FEN0159AProcessor fen0159aProcessor() {
        return new FEN0159AProcessor();
    }

    /**
     * Step FEN 0159 A output.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepFEN0159AOutput() {
        return this.stepBuilderFactory.get("step_fen_0159a").<DemandePaiement, FEN0159ADTO> chunk(this.chunkSize).reader(this.fen0159aReader())
                .processor(this.fen0159aProcessor()).writer(this.fen0159aWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FEN_0159A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FEN_0159A_JOB.getNom()).start(this.stepFEN0159AOutput()).build();
    }
}

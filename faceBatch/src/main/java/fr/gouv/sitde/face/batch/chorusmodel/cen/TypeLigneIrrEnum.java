
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TypeLigneIrrType.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="TypeLigneIrrType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="3"/&gt;
 *     &lt;enumeration value="IRR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "TypeLigneIrrType")
@XmlEnum
public enum TypeLigneIrrEnum {

    /** The irr. */
    IRR;

    /**
     * Value.
     *
     * @return the string
     */
    public String value() {
        return this.name();
    }

    /**
     * From value.
     *
     * @param v
     *            the v
     * @return the type ligne irr enum
     */
    public static TypeLigneIrrEnum fromValue(String v) {
        return valueOf(v);
    }

}

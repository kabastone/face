
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TexteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="TexteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FSO0028A}Char12Type"/&gt;
 *         &lt;element name="TEXT_ID" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="TEXT_LINE" type="{FSO0028A}Char132Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_POSTE" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TexteType", propOrder = { "idAe", "textId", "textLine", "idPoste" })
public class TexteFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The id ae. */
    @XmlElement(name = "ID_AE", required = true)
    private String idAe;

    /** The text id. */
    @XmlElement(name = "TEXT_ID")
    private String textId;

    /** The text line. */
    @XmlElement(name = "TEXT_LINE")
    private String textLine;

    /** The id poste. */
    @XmlElement(name = "ID_POSTE")
    private String idPoste;

    /**
     * Gets the id ae.
     *
     * @return the idAe
     */
    public String getIdAe() {
        return this.idAe;
    }

    /**
     * Sets the id ae.
     *
     * @param idAe
     *            the idAe to set
     */
    public void setIdAe(String idAe) {
        this.idAe = idAe;
    }

    /**
     * Gets the text id.
     *
     * @return the textId
     */
    public String getTextId() {
        return this.textId;
    }

    /**
     * Sets the text id.
     *
     * @param textId
     *            the textId to set
     */
    public void setTextId(String textId) {
        this.textId = textId;
    }

    /**
     * Gets the text line.
     *
     * @return the textLine
     */
    public String getTextLine() {
        return this.textLine;
    }

    /**
     * Sets the text line.
     *
     * @param textLine
     *            the textLine to set
     */
    public void setTextLine(String textLine) {
        this.textLine = textLine;
    }

    /**
     * Gets the id poste.
     *
     * @return the idPoste
     */
    public String getIdPoste() {
        return this.idPoste;
    }

    /**
     * Sets the id poste.
     *
     * @param idPoste
     *            the idPoste to set
     */
    public void setIdPoste(String idPoste) {
        this.idPoste = idPoste;
    }

}

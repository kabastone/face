/**
 *
 */
package fr.gouv.sitde.face.batch.job.emailexpiration;

import javax.inject.Inject;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * @author A754839
 *
 */
public class ExpirationMajProcessor implements ItemProcessor<DossierSubvention, DossierSubvention> {

    @Inject
    private CadencementService cadencementService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public DossierSubvention process(DossierSubvention dossier) {

        if (dossier.getDemandesSubvention() != null) {
            dossier.getDemandesSubvention().stream().map(DemandeSubvention::getCadencement).forEach(this::validerEtEnregistrer);
        }

        return dossier;
    }

    private void validerEtEnregistrer(Cadencement cadencement) {
        if (cadencement != null) {
            cadencement.setEstValide(true);
            this.cadencementService.enregistrerCadencement(cadencement);
        }
    }

}

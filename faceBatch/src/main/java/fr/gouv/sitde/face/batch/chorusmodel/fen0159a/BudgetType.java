
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'un budget dans CHORUS
 *
 * <p>
 * Classe Java pour BudgetType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="BudgetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="FISC_YEAR" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *         &lt;element name="FM_AREA" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BudgetType", propOrder = { "idChorus", "fiscYear", "fmArea" })
public class BudgetType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "FISC_YEAR", required = true)
    private String fiscYear;
    @XmlElement(name = "FM_AREA", required = true)
    private String fmArea;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété fiscYear.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFiscYear() {
        return this.fiscYear;
    }

    /**
     * Définit la valeur de la propriété fiscYear.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setFiscYear(String value) {
        this.fiscYear = value;
    }

    /**
     * Obtient la valeur de la propriété fmArea.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFmArea() {
        return this.fmArea;
    }

    /**
     * Définit la valeur de la propriété fmArea.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setFmArea(String value) {
        this.fmArea = value;
    }

}

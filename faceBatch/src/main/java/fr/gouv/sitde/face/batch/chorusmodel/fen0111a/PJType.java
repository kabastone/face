
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour PJType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="PJType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Description"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="64"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Data"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}base64Binary"&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PJType", propOrder = { "type", "description", "data" })
public class PJType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Type", required = true)
    private String type;
    @XmlElement(name = "Description", required = true)
    private String description;
    @XmlElement(name = "Data", required = true)
    private byte[] data;

    /**
     * Obtient la valeur de la propriété type.
     *
     * @return possible object is {@link String }
     *
     */
    public String getType() {
        return this.type;
    }

    /**
     * Définit la valeur de la propriété type.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtient la valeur de la propriété description.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Définit la valeur de la propriété description.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtient la valeur de la propriété data.
     *
     * @return possible object is byte[]
     */
    public byte[] getData() {
        return this.data;
    }

    /**
     * Définit la valeur de la propriété data.
     *
     * @param value
     *            allowed object is byte[]
     */
    public void setData(byte[] value) {
        this.data = value;
    }

}

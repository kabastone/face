package fr.gouv.sitde.face.batch.job.emailrappelconsommation;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Class EmailRappelConsommationBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailRappelConsommationBatchConfiguration {
	
	/** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The dossier subvention service. */
    @Inject
    private CollectiviteService collectiviteService;

    /**
     * Collectivite expiration reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<Collectivite> rappelConsommationReader() {
        List<Collectivite> listeCollectivite = this.collectiviteService.rechercherCollectivitePourMailDotationNonConsomme();
        return new ListItemReader<>(listeCollectivite);
    }
    
    /**
     * Email rappel consommation writer.
     *
     * @return the email rappel consommation writer
     */
    @Bean
    @StepScope
    public EmailRappelConsommationWriter emailRappelConsommationWriter() {
        return new EmailRappelConsommationWriter();
    }
    
    /**
     * Step email rappel consommation.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEmailRappelConsommation() {
        return this.stepBuilderFactory.get("step_email_rappel_dotation_consommation").<Collectivite, Collectivite> chunk(1)
                .reader(this.rappelConsommationReader()).writer(this.emailRappelConsommationWriter()).build();
    }
    
    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.EMAIL_RAPPEL_CONSOMMATION_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.EMAIL_RAPPEL_CONSOMMATION_JOB.getNom()).start(this.stepEmailRappelConsommation()).build();
    }

}

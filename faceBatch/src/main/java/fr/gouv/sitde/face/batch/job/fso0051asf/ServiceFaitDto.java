/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051asf;

/**
 * The Class ServiceFaitDto.
 * 
 *
 * @author Atos
 */
public class ServiceFaitDto {

    /**
     * The numero sequence.
     *
     * Champ obligatoire
     */
    private Integer numeroSequence;

    /**
     * The numero EJ.
     *
     * Champ Obligatoire
     */
    private String numeroEJ;

    /**
     * The poste EJ. Champ obligatoire
     */
    private String numLigneLegacy;

    /**
     * The numero service.
     *
     * Champ obligatoire
     */
    private String numeroServiceFait;

    /**
     * The dateCertification.
     *
     * Champ optionnel
     */
    private String dateCertification;

    /**
     * Gets the numero sequence.
     *
     * @return the numeroSequence
     */
    public Integer getNumeroSequence() {
        return this.numeroSequence;
    }

    /**
     * Sets the numero sequence.
     *
     * @param numeroSequence
     *            the numeroSequence to set
     */
    public void setNumeroSequence(Integer numeroSequence) {
        this.numeroSequence = numeroSequence;
    }

    /**
     * Gets the numero EJ.
     *
     * @return the numeroEJ
     */
    public String getNumeroEJ() {
        return this.numeroEJ;
    }

    /**
     * Sets the numero EJ.
     *
     * @param numeroEJ
     *            the numeroEJ to set
     */
    public void setNumeroEJ(String numeroEJ) {
        this.numeroEJ = numeroEJ;
    }

    /**
     * Gets the poste EJ.
     *
     * @return the numLigneLegacy
     */
    public String getNumLigneLegacy() {
        return this.numLigneLegacy;
    }

    /**
     * Sets the poste EJ.
     *
     * @param numLigneLegacy
     *            the numLigneLegacy to set
     */
    public void setNumLigneLegacy(String posteEJ) {
        this.numLigneLegacy = posteEJ;
    }

    /**
     * Gets the numero service fait.
     *
     * @return the numeroServiceFait
     */
    public String getNumeroServiceFait() {
        return this.numeroServiceFait;
    }

    /**
     * Sets the numero service fait.
     *
     * @param numeroServiceFait
     *            the numeroServiceFait to set
     */
    public void setNumeroServiceFait(String numeroServiceFait) {
        this.numeroServiceFait = numeroServiceFait;
    }

    /**
     * Gets the dateCertification.
     *
     * @return the dateCertification
     */
    public String getDateCertification() {
        return this.dateCertification;
    }

    /**
     * Sets the dateCertification.
     *
     * @param dateCertification
     *            the dateCertification to set
     */
    public void setDateCertification(String dateCertification) {
        this.dateCertification = dateCertification;
    }

}

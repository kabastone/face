/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0159a;

import java.io.File;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.cen.LigneDossier;
import fr.gouv.sitde.face.batch.commun.AiguillageTasklet;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.commun.ResourcesUtils;
import fr.gouv.sitde.face.batch.commun.SuppressionFichiersTasklet;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class CEN0159AXmlConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class CEN0159AXmlConfiguration {

    /** The Constant CEN_0111A_REGEXP. */
    private static final String CEN_0159A_REGEXP = "^CEN0159A_[a-zA-Z0-9]+\\w+";

    /** The Constant SOUS_REP_INPUT_CEN0159A. */
    public static final String SOUS_REP_INPUT_CEN0159A = "/cen0159a";

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /** The input file. */
    private File inputFile;

    /** The input global. */
    private File inputGlobal;

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile() {
        if (this.inputFile == null) {
            this.inputFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_CEN0159A);
        }
        return this.inputFile;
    }

    /**
     * Gets the input global.
     *
     * @return the input global
     */
    public File getInputGlobal() {
        if (this.inputGlobal == null) {
            this.inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        }
        return this.inputGlobal;
    }

    /**
     * Step aiguillage CEN 0159 A.
     *
     * Step 0 : on va chercher les fichiers propres au batch dans le repertoire global
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAiguillageCEN0159A() {
        AiguillageTasklet tasklet = new AiguillageTasklet();
        tasklet.setInputDir(this.getInputGlobal());
        tasklet.setOutputDir(this.getInputFile());
        tasklet.setRegexp(CEN_0159A_REGEXP);

        return this.stepBuilderFactory.get("step_aiguillage").tasklet(tasklet).build();
    }

    /**
     * Gets the ressources from input directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromInputDirectory() {
        File repertoireInputJob = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_CEN0159A);
        return ResourcesUtils.getRessourcesFromDirectory(repertoireInputJob);
    }

    /**
     * Multi resource ligne dossier paiement transfert reader.
     *
     * @return the multi resource item reader
     */
    @Bean
    @StepScope
    public MultiResourceItemReader<LigneDossier> multiResourceLigneDossierPaiementTransfertReader() {
        Resource[] tableauResources = this.getRessourcesFromInputDirectory();
        MultiResourceItemReader<LigneDossier> resourceItemReader = new MultiResourceItemReader<>();
        resourceItemReader.setResources(tableauResources);
        // Pour chaque fichier, on délègue la lecture au dossierXmlReader
        resourceItemReader.setDelegate(this.ligneDossierPaiementTransfertXmlReader());
        return resourceItemReader;
    }

    /**
     * Ligne dossier paiement transfert xml reader.
     *
     * @return the stax event item reader
     */
    @Bean
    @StepScope
    public StaxEventItemReader<LigneDossier> ligneDossierPaiementTransfertXmlReader() {
        StaxEventItemReader<LigneDossier> ligneDossierXmlReader = new StaxEventItemReader<>();
        ligneDossierXmlReader.setFragmentRootElementName("LIGNE_DOSSIER");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(LigneDossier.class);
        ligneDossierXmlReader.setUnmarshaller(marshaller);

        return ligneDossierXmlReader;
    }

    /**
     * Ligne dossier paiement transfert processor.
     *
     * @return the ligne dossier paiement transfert processor
     */
    @Bean
    @StepScope
    public LigneDossierPaiementTransfertProcessor ligneDossierPaiementTransfertProcessor() {
        return new LigneDossierPaiementTransfertProcessor();
    }

    /**
     * Ligne dossier paiement transfert writer.
     *
     * @return the ligne dossier paiement transfert writer
     */
    @Bean
    @StepScope
    public LigneDossierPaiementTransfertWriter ligneDossierPaiementTransfertWriter() {
        return new LigneDossierPaiementTransfertWriter();
    }

    /**
     * Step ligne dossier paiement transfert xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepLigneDossierPaiementTransfertXml() {
        return this.stepBuilderFactory.get("step_ligne_dossier_paiement_xml").<LigneDossier, LigneDossierPaiementTransfertTraitementDto> chunk(1)
                .reader(this.multiResourceLigneDossierPaiementTransfertReader()).processor(this.ligneDossierPaiementTransfertProcessor())
                .writer(this.ligneDossierPaiementTransfertWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Step suppression fichiers paiement transfert xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersPaiementTransfertXml() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromInputDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers").tasklet(tasklet).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.CEN_0159A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.CEN_0159A_JOB.getNom()).start(this.stepAiguillageCEN0159A())
                .next(this.stepLigneDossierPaiementTransfertXml()).next(this.stepSuppressionFichiersPaiementTransfertXml()).build();
    }

}

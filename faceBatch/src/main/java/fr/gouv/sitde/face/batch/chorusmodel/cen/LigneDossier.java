
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * Une ligne detail par dossier et par module ayant accepte ce dossier, aucune ou plusieurs lignes detail par dossier et par module ayant envoye un
 * fichier de CR pour le meme fichier entrant d'origine et pour lequel la prise en compte de ce fichier est parametree.
 *
 * <p>
 * Classe Java pour LigneDossierType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="LigneDossierType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TYPE_LIGNE" type="{Compte-rendu_20}TypeLigneDtlType"/&gt;
 *         &lt;element name="NO_SEQ_DOSSIER" type="{Compte-rendu_20}NumeriqueType" minOccurs="0"/&gt;
 *         &lt;element name="ID_DAE" type="{Compte-rendu_20}IdType"/&gt;
 *         &lt;element name="NO_SEQ_LIGNE_DOSSIER" type="{Compte-rendu_20}NoSeqLigneDossierType"/&gt;
 *         &lt;element name="CODE_ERREUR" type="{Compte-rendu_20}CodeErreurType" minOccurs="0"/&gt;
 *         &lt;element name="LIBELLE_ERREUR" type="{Compte-rendu_20}LibelleErreurType" minOccurs="0"/&gt;
 *         &lt;element name="ID_DEST" type="{Compte-rendu_20}IdType" minOccurs="0"/&gt;
 *         &lt;element name="AUTRES_CHAMPS_FONCTIONNELS" type="{Compte-rendu_20}AutresChampsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "LIGNE_DOSSIER")
@XmlType(name = "LigneDossierType", propOrder = { "typeLigne", "numSeqDossier", "idDae", "numSeqLigneDossier", "codeErreur", "libelleErreur",
        "idDest", "autresChampsFonctionnels" })
public class LigneDossier implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type ligne. */
    @XmlElement(name = "TYPE_LIGNE", required = true)
    @XmlSchemaType(name = "string")
    private TypeLigneDtlEnum typeLigne;

    /** The num seq dossier. */
    @XmlElement(name = "NO_SEQ_DOSSIER", nillable = true)
    private Long numSeqDossier;

    /** The id dae. */
    @XmlElement(name = "ID_DAE", required = true)
    private String idDae;

    /** The num seq ligne dossier. */
    @XmlElement(name = "NO_SEQ_LIGNE_DOSSIER", required = true)
    private String numSeqLigneDossier;

    /** The code erreur. */
    @XmlElement(name = "CODE_ERREUR", nillable = true)
    private String codeErreur;

    /** The libelle erreur. */
    @XmlElement(name = "LIBELLE_ERREUR", nillable = true)
    private String libelleErreur;

    /** The id dest. */
    @XmlElement(name = "ID_DEST", nillable = true)
    private String idDest;

    /** The autres champs fonctionnels. */
    @XmlElement(name = "AUTRES_CHAMPS_FONCTIONNELS", nillable = true)
    private String autresChampsFonctionnels;

    /**
     * Obtient la valeur de la propriété typeLigne.
     *
     * @return possible object is {@link TypeLigneDtlEnum }
     *
     */
    public TypeLigneDtlEnum getTypeLigne() {
        return this.typeLigne;
    }

    /**
     * Définit la valeur de la propriété typeLigne.
     *
     * @param value
     *            allowed object is {@link TypeLigneDtlEnum }
     *
     */
    public void setTypeLigne(TypeLigneDtlEnum value) {
        this.typeLigne = value;
    }

    /**
     * Obtient la valeur de la propriété numSeqDossier.
     *
     * @return possible object is {@link Long }
     *
     */
    public Long getNumSeqDossier() {
        return this.numSeqDossier;
    }

    /**
     * Définit la valeur de la propriété numSeqDossier.
     *
     * @param value
     *            allowed object is {@link Long }
     *
     */
    public void setNumSeqDossier(Long value) {
        this.numSeqDossier = value;
    }

    /**
     * Obtient la valeur de la propriété idDae.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdDae() {
        return this.idDae;
    }

    /**
     * Définit la valeur de la propriété idDae.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdDae(String value) {
        this.idDae = value;
    }

    /**
     * Obtient la valeur de la propriété numSeqLigneDossier.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumSeqLigneDossier() {
        return this.numSeqLigneDossier;
    }

    /**
     * Définit la valeur de la propriété numSeqLigneDossier.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumSeqLigneDossier(String value) {
        this.numSeqLigneDossier = value;
    }

    /**
     * Obtient la valeur de la propriété codeErreur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeErreur() {
        return this.codeErreur;
    }

    /**
     * Définit la valeur de la propriété codeErreur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeErreur(String value) {
        this.codeErreur = value;
    }

    /**
     * Obtient la valeur de la propriété libelleErreur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLibelleErreur() {
        return this.libelleErreur;
    }

    /**
     * Définit la valeur de la propriété libelleErreur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setLibelleErreur(String value) {
        this.libelleErreur = value;
    }

    /**
     * Obtient la valeur de la propriété idDest.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdDest() {
        return this.idDest;
    }

    /**
     * Définit la valeur de la propriété idDest.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdDest(String value) {
        this.idDest = value;
    }

    /**
     * Obtient la valeur de la propriété autresChampsFonctionnels.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAutresChampsFonctionnels() {
        return this.autresChampsFonctionnels;
    }

    /**
     * Définit la valeur de la propriété autresChampsFonctionnels.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setAutresChampsFonctionnels(String value) {
        this.autresChampsFonctionnels = value;
    }

}

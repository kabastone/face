
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour PosteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="PosteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FSO0028A}Char12Type"/&gt;
 *         &lt;element name="INFO_REGROUP" type="{FSO0028A}Char40Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_LIG_AMG" type="{FSO0028A}Char5Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_POSTE" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="CATEGORY_ID" type="{FSO0028A}Char20Type" minOccurs="0"/&gt;
 *         &lt;element name="QUANTITY" type="{FSO0028A}Num13.3Type" minOccurs="0"/&gt;
 *         &lt;element name="UNIT" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="GROSS_PRICE" type="{FSO0028A}Num12.2sType" minOccurs="0"/&gt;
 *         &lt;element name="PRICE" type="{FSO0028A}Num12.2sType" minOccurs="0"/&gt;
 *         &lt;element name="UNIT_PRICE" type="{FSO0028A}Num5Type" minOccurs="0"/&gt;
 *         &lt;element name="VALUE" type="{FSO0028A}Num16.2sType" minOccurs="0"/&gt;
 *         &lt;element name="TAX_CODE" type="{FSO0028A}Char2Type" minOccurs="0"/&gt;
 *         &lt;element name="DELIV_DATE" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="PS_IPT" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="DESCRIPTION" type="{FSO0028A}Char40Type" minOccurs="0"/&gt;
 *         &lt;element name="BE_STGE_LOC" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="BE_PLANT" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="OPT_IND" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="OPT_STATUS" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="OPT_POP_START" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="OPT_POP_END" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="OPT_XPER_START" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="OPT_XPER_END" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="CLOSEOUT" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="FORME_PRIX" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="PENAL_RETARD" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PosteType",
        propOrder = { "idAe", "infoRegroup", "idLigAmg", "idPoste", "categoryId", "quantity", "unit", "grossPrice", "price", "unitPrice", "value",
                "taxCode", "delivDate", "psIpt", "description", "beStgeLoc", "bePlant", "optInd", "optStatus", "optPopStart", "optPopEnd",
                "optXperStart", "optXperEnd", "closeOut", "formePrix", "penalRetard" })
public class LignePosteFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The id ae. */
    @XmlElement(name = "ID_AE", required = true)
    private String idAe;

    /** The info regroup. */
    @XmlElement(name = "INFO_REGROUP")
    private String infoRegroup;

    /** The id lig amg. */
    @XmlElement(name = "ID_LIG_AMG")
    private String idLigAmg;

    /** The id poste. */
    @XmlElement(name = "ID_POSTE")
    private String idPoste;

    /** The category id. */
    @XmlElement(name = "CATEGORY_ID")
    private String categoryId;

    /** The quantity. */
    @XmlElement(name = "QUANTITY")
    private String quantity;

    /** The unit. */
    @XmlElement(name = "UNIT")
    private String unit;

    /** The gross price. */
    @XmlElement(name = "GROSS_PRICE")
    private String grossPrice;

    /** The price. */
    @XmlElement(name = "PRICE")
    private String price;

    /** The unit price. */
    @XmlElement(name = "UNIT_PRICE")
    private String unitPrice;

    /** The value. */
    @XmlElement(name = "VALUE")
    private String value;

    /** The tax code. */
    @XmlElement(name = "TAX_CODE")
    private String taxCode;

    /** The deliv date. */
    @XmlElement(name = "DELIV_DATE")
    private String delivDate;

    /** The ps ipt. */
    @XmlElement(name = "PS_IPT")
    private String psIpt;

    /** The description. */
    @XmlElement(name = "DESCRIPTION")
    private String description;

    /** The be stge loc. */
    @XmlElement(name = "BE_STGE_LOC")
    private String beStgeLoc;

    /** The be plant. */
    @XmlElement(name = "BE_PLANT")
    private String bePlant;

    /** The opt ind. */
    @XmlElement(name = "OPT_IND")
    private String optInd;

    /** The opt status. */
    @XmlElement(name = "OPT_STATUS")
    private String optStatus;

    /** The opt pop start. */
    @XmlElement(name = "OPT_POP_START")
    private String optPopStart;

    /** The opt pop end. */
    @XmlElement(name = "OPT_POP_END")
    private String optPopEnd;

    /** The opt xper start. */
    @XmlElement(name = "OPT_XPER_START")
    private String optXperStart;

    /** The opt xper end. */
    @XmlElement(name = "OPT_XPER_END")
    private String optXperEnd;

    /** The close out. */
    @XmlElement(name = "CLOSEOUT")
    private String closeOut;

    /** The forme prix. */
    @XmlElement(name = "FORME_PRIX")
    private String formePrix;

    /** The penal retard. */
    @XmlElement(name = "PENAL_RETARD")
    private String penalRetard;

    /**
     * Gets the id ae.
     *
     * @return the idAe
     */
    public String getIdAe() {
        return this.idAe;
    }

    /**
     * Sets the id ae.
     *
     * @param idAe
     *            the idAe to set
     */
    public void setIdAe(String idAe) {
        this.idAe = idAe;
    }

    /**
     * Gets the info regroup.
     *
     * @return the infoRegroup
     */
    public String getInfoRegroup() {
        return this.infoRegroup;
    }

    /**
     * Sets the info regroup.
     *
     * @param infoRegroup
     *            the infoRegroup to set
     */
    public void setInfoRegroup(String infoRegroup) {
        this.infoRegroup = infoRegroup;
    }

    /**
     * Gets the id lig amg.
     *
     * @return the idLigAmg
     */
    public String getIdLigAmg() {
        return this.idLigAmg;
    }

    /**
     * Sets the id lig amg.
     *
     * @param idLigAmg
     *            the idLigAmg to set
     */
    public void setIdLigAmg(String idLigAmg) {
        this.idLigAmg = idLigAmg;
    }

    /**
     * Gets the id poste.
     *
     * @return the idPoste
     */
    public String getIdPoste() {
        return this.idPoste;
    }

    /**
     * Sets the id poste.
     *
     * @param idPoste
     *            the idPoste to set
     */
    public void setIdPoste(String idPoste) {
        this.idPoste = idPoste;
    }

    /**
     * Gets the category id.
     *
     * @return the categoryId
     */
    public String getCategoryId() {
        return this.categoryId;
    }

    /**
     * Sets the category id.
     *
     * @param categoryId
     *            the categoryId to set
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets the quantity.
     *
     * @return the quantity
     */
    public String getQuantity() {
        return this.quantity;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets the unit.
     *
     * @return the unit
     */
    public String getUnit() {
        return this.unit;
    }

    /**
     * Sets the unit.
     *
     * @param unit
     *            the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Gets the gross price.
     *
     * @return the grossPrice
     */
    public String getGrossPrice() {
        return this.grossPrice;
    }

    /**
     * Sets the gross price.
     *
     * @param grossPrice
     *            the grossPrice to set
     */
    public void setGrossPrice(String grossPrice) {
        this.grossPrice = grossPrice;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public String getPrice() {
        return this.price;
    }

    /**
     * Sets the price.
     *
     * @param price
     *            the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets the unit price.
     *
     * @return the unitPrice
     */
    public String getUnitPrice() {
        return this.unitPrice;
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice
     *            the unitPrice to set
     */
    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the tax code.
     *
     * @return the taxCode
     */
    public String getTaxCode() {
        return this.taxCode;
    }

    /**
     * Sets the tax code.
     *
     * @param taxCode
     *            the taxCode to set
     */
    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    /**
     * Gets the deliv date.
     *
     * @return the delivDate
     */
    public String getDelivDate() {
        return this.delivDate;
    }

    /**
     * Sets the deliv date.
     *
     * @param delivDate
     *            the delivDate to set
     */
    public void setDelivDate(String delivDate) {
        this.delivDate = delivDate;
    }

    /**
     * Gets the ps ipt.
     *
     * @return the psIpt
     */
    public String getPsIpt() {
        return this.psIpt;
    }

    /**
     * Sets the ps ipt.
     *
     * @param psIpt
     *            the psIpt to set
     */
    public void setPsIpt(String psIpt) {
        this.psIpt = psIpt;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the be stge loc.
     *
     * @return the beStgeLoc
     */
    public String getBeStgeLoc() {
        return this.beStgeLoc;
    }

    /**
     * Sets the be stge loc.
     *
     * @param beStgeLoc
     *            the beStgeLoc to set
     */
    public void setBeStgeLoc(String beStgeLoc) {
        this.beStgeLoc = beStgeLoc;
    }

    /**
     * Gets the be plant.
     *
     * @return the bePlant
     */
    public String getBePlant() {
        return this.bePlant;
    }

    /**
     * Sets the be plant.
     *
     * @param bePlant
     *            the bePlant to set
     */
    public void setBePlant(String bePlant) {
        this.bePlant = bePlant;
    }

    /**
     * Gets the opt ind.
     *
     * @return the optInd
     */
    public String getOptInd() {
        return this.optInd;
    }

    /**
     * Sets the opt ind.
     *
     * @param optInd
     *            the optInd to set
     */
    public void setOptInd(String optInd) {
        this.optInd = optInd;
    }

    /**
     * Gets the opt status.
     *
     * @return the optStatus
     */
    public String getOptStatus() {
        return this.optStatus;
    }

    /**
     * Sets the opt status.
     *
     * @param optStatus
     *            the optStatus to set
     */
    public void setOptStatus(String optStatus) {
        this.optStatus = optStatus;
    }

    /**
     * Gets the opt pop start.
     *
     * @return the optPopStart
     */
    public String getOptPopStart() {
        return this.optPopStart;
    }

    /**
     * Sets the opt pop start.
     *
     * @param optPopStart
     *            the optPopStart to set
     */
    public void setOptPopStart(String optPopStart) {
        this.optPopStart = optPopStart;
    }

    /**
     * Gets the opt pop end.
     *
     * @return the optPopEnd
     */
    public String getOptPopEnd() {
        return this.optPopEnd;
    }

    /**
     * Sets the opt pop end.
     *
     * @param optPopEnd
     *            the optPopEnd to set
     */
    public void setOptPopEnd(String optPopEnd) {
        this.optPopEnd = optPopEnd;
    }

    /**
     * Gets the opt xper start.
     *
     * @return the optXperStart
     */
    public String getOptXperStart() {
        return this.optXperStart;
    }

    /**
     * Sets the opt xper start.
     *
     * @param optXperStart
     *            the optXperStart to set
     */
    public void setOptXperStart(String optXperStart) {
        this.optXperStart = optXperStart;
    }

    /**
     * Gets the opt xper end.
     *
     * @return the optXperEnd
     */
    public String getOptXperEnd() {
        return this.optXperEnd;
    }

    /**
     * Sets the opt xper end.
     *
     * @param optXperEnd
     *            the optXperEnd to set
     */
    public void setOptXperEnd(String optXperEnd) {
        this.optXperEnd = optXperEnd;
    }

    /**
     * Gets the close out.
     *
     * @return the closeOut
     */
    public String getCloseOut() {
        return this.closeOut;
    }

    /**
     * Sets the close out.
     *
     * @param closeOut
     *            the closeOut to set
     */
    public void setCloseOut(String closeOut) {
        this.closeOut = closeOut;
    }

    /**
     * Gets the forme prix.
     *
     * @return the formePrix
     */
    public String getFormePrix() {
        return this.formePrix;
    }

    /**
     * Sets the forme prix.
     *
     * @param formePrix
     *            the formePrix to set
     */
    public void setFormePrix(String formePrix) {
        this.formePrix = formePrix;
    }

    /**
     * Gets the penal retard.
     *
     * @return the penalRetard
     */
    public String getPenalRetard() {
        return this.penalRetard;
    }

    /**
     * Sets the penal retard.
     *
     * @param penalRetard
     *            the penalRetard to set
     */
    public void setPenalRetard(String penalRetard) {
        this.penalRetard = penalRetard;
    }

}

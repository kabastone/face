
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * Rattachement d'une PJ a des objets metiers CHORUS
 *
 * <p>
 * Classe Java pour RattachementPJType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="RattachementPJType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TYPE_PJ" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}TypePJType"/&gt;
 *         &lt;element name="NOM_PJ" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}NomPJType"/&gt;
 *         &lt;element name="CODE_APPLI" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char3Type"/&gt;
 *         &lt;element name="ID_USER" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char16Type" minOccurs="0"/&gt;
 *         &lt;element name="VERSION_PJ" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char2Type" minOccurs="0"/&gt;
 *         &lt;element name="CODE_DOC" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="VERSION_DOC" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="REF_FDS_EXTERNE" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char255Type" minOccurs="0"/&gt;
 *         &lt;element name="DATA" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}PJDataType"/&gt;
 *         &lt;element name="ObjetsMetiersChorus" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ObjetsMetiersChorusType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RattachementPJ")
@XmlType(name = "RattachementPJType", propOrder = { "typePieceJointe", "nomPieceJointe", "codeApplication", "idUtilisateur", "versionPieceJointe",
        "codeDocument", "versionDocument", "referenceExternePourPieceJointe", "donneesPieceJointe", "identificationObjetMetierPieceJointe" })
public class RattachementPJ implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "TYPE_PJ", required = true)
    @XmlSchemaType(name = "string")
    private TypePJType typePieceJointe;
    @XmlElement(name = "NOM_PJ", required = true)
    private String nomPieceJointe;
    @XmlElement(name = "CODE_APPLI", required = true)
    private String codeApplication;
    @XmlElement(name = "ID_USER")
    private String idUtilisateur;
    @XmlElement(name = "VERSION_PJ")
    private String versionPieceJointe;
    @XmlElement(name = "CODE_DOC")
    private String codeDocument;
    @XmlElement(name = "VERSION_DOC")
    private String versionDocument;
    @XmlElement(name = "REF_FDS_EXTERNE")
    private String referenceExternePourPieceJointe;
    @XmlElement(name = "DATA", required = true)
    private byte[] donneesPieceJointe;
    @XmlElement(name = "ObjetsMetiersChorus", required = true)
    private List<ObjetsMetiersChorus> identificationObjetMetierPieceJointe;

    /**
     * Obtient la valeur de la propriété typePieceJointe.
     *
     * @return possible object is {@link TypePJType }
     *
     */
    public TypePJType getTypePieceJointe() {
        return this.typePieceJointe;
    }

    /**
     * Définit la valeur de la propriété typePieceJointe.
     *
     * @param value
     *            allowed object is {@link TypePJType }
     *
     */
    public void setTypePieceJointe(TypePJType value) {
        this.typePieceJointe = value;
    }

    /**
     * Obtient la valeur de la propriété nomPieceJointe.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNomPieceJointe() {
        return this.nomPieceJointe;
    }

    /**
     * Définit la valeur de la propriété nomPieceJointe.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNomPieceJointe(String value) {
        this.nomPieceJointe = value;
    }

    /**
     * Obtient la valeur de la propriété codeApplication.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeApplication() {
        return this.codeApplication;
    }

    /**
     * Définit la valeur de la propriété codeApplication.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeApplication(String value) {
        this.codeApplication = value;
    }

    /**
     * Obtient la valeur de la propriété idUtilisateur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdUtilisateur() {
        return this.idUtilisateur;
    }

    /**
     * Définit la valeur de la propriété idUtilisateur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdUtilisateur(String value) {
        this.idUtilisateur = value;
    }

    /**
     * Obtient la valeur de la propriété versionPieceJointe.
     *
     * @return possible object is {@link String }
     *
     */
    public String getVersionPieceJointe() {
        return this.versionPieceJointe;
    }

    /**
     * Définit la valeur de la propriété versionPieceJointe.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setVersionPieceJointe(String value) {
        this.versionPieceJointe = value;
    }

    /**
     * Obtient la valeur de la propriété codeDocument.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeDocument() {
        return this.codeDocument;
    }

    /**
     * Définit la valeur de la propriété codeDocument.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeDocument(String value) {
        this.codeDocument = value;
    }

    /**
     * Obtient la valeur de la propriété versionDocument.
     *
     * @return possible object is {@link String }
     *
     */
    public String getVersionDocument() {
        return this.versionDocument;
    }

    /**
     * Définit la valeur de la propriété versionDocument.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setVersionDocument(String value) {
        this.versionDocument = value;
    }

    /**
     * Obtient la valeur de la propriété referenceExternePourPieceJointe.
     *
     * @return possible object is {@link String }
     *
     */
    public String getReferenceExternePourPieceJointe() {
        return this.referenceExternePourPieceJointe;
    }

    /**
     * Définit la valeur de la propriété referenceExternePourPieceJointe.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setReferenceExternePourPieceJointe(String value) {
        this.referenceExternePourPieceJointe = value;
    }

    /**
     * Obtient la valeur de la propriété donneesPieceJointe.
     *
     * @return possible object is byte[]
     */
    public byte[] getDonneesPieceJointe() {
        return this.donneesPieceJointe;
    }

    /**
     * Définit la valeur de la propriété donneesPieceJointe.
     *
     * @param value
     *            allowed object is byte[]
     */
    public void setDonneesPieceJointe(byte[] value) {
        this.donneesPieceJointe = value;
    }

    /**
     * Gets the value of the identificationObjetMetierPieceJointe property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the identificationObjetMetierPieceJointe property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getIdentificationObjetMetierPieceJointe().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ObjetsMetiersChorus }
     *
     *
     */
    public List<ObjetsMetiersChorus> getIdentificationObjetMetierPieceJointe() {
        if (this.identificationObjetMetierPieceJointe == null) {
            this.identificationObjetMetierPieceJointe = new ArrayList<>();
        }
        return this.identificationObjetMetierPieceJointe;
    }

    /**
     * @param identificationObjetMetierPieceJointe
     *            the identificationObjetMetierPieceJointe to set
     */
    public void setIdentificationObjetMetierPieceJointe(List<ObjetsMetiersChorus> identificationObjetMetierPieceJointe) {
        this.identificationObjetMetierPieceJointe = identificationObjetMetierPieceJointe;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0159a;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159A;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159ADTO;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.ObjetsMetiersChorus;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.RattachementPJ;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.ServiceFait;
import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.TypePJType;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Class FEN0159AProcessor.
 */
public class FEN0159AProcessor implements ItemProcessor<DemandePaiement, FEN0159ADTO> {

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /** The fichier service. */
    @Inject
    private DocumentService documentService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public FEN0159ADTO process(DemandePaiement demandePaiement) throws Exception {

        FluxChorus referentiel = this.fluxChorusService.rechercherFluxChorus();

        // Création du service fait lié à la pièce jointe.
        ServiceFait serviceFait = new ServiceFait();
        serviceFait.setAnneeFiscale(String.valueOf(demandePaiement.getAnneeFiscale()));
        serviceFait.setCodeSociete(referentiel.getCodeSociete());
        serviceFait.setIdChorus(String.valueOf(demandePaiement.getChorusNumSf()));

        // Remplissage de l'objet métier chorus.
        List<ServiceFait> listeServiceFait = new ArrayList<>(1);
        listeServiceFait.add(serviceFait);
        ObjetsMetiersChorus obj = new ObjetsMetiersChorus();
        obj.setServiceFait(listeServiceFait);
        List<ObjetsMetiersChorus> listeObj = new ArrayList<>(1);
        listeObj.add(obj);

        // Boucle de gestion des documents.
        List<RattachementPJ> listePJ = new ArrayList<>(demandePaiement.getDocuments().size());
        for (Document doc : demandePaiement.getDocuments()) {
            RattachementPJ rattachement = new RattachementPJ();

            // On tronque ACE001 en ACE
            rattachement.setCodeApplication(StringUtils.truncate(referentiel.getCodeApplication(), 3));
            rattachement.setNomPieceJointe(doc.getNomFichierSansExtension() + FilenameUtils.EXTENSION_SEPARATOR_STR + doc.getExtensionFichier());
            rattachement.setTypePieceJointe(TypePJType.fromValue(doc.getExtensionFichier().toUpperCase(Locale.FRANCE)));
            rattachement.setVersionPieceJointe("01");
            // Encodage en base64 de la piece jointe.
            rattachement.setDonneesPieceJointe(this.documentService.encoderDocument(doc));
            rattachement.setIdentificationObjetMetierPieceJointe(listeObj);

            // Ajout à la liste des pieces jointes.
            listePJ.add(rattachement);
        }

        // Création de l'objet encapsulant les informations pour transfert au writer.
        FEN0159A fen = new FEN0159A();
        fen.setPieceJointe(listePJ);

        return new FEN0159ADTO(fen, demandePaiement);
    }

}

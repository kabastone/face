/**
 *
 */
package fr.gouv.sitde.face.batch.commun.batch51a;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.gouv.sitde.face.batch.commun.batch51a.model.Batch51AEnum;
import fr.gouv.sitde.face.batch.commun.batch51a.model.SuiviBatch;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Class MiseAJourCompteurTasklet.
 *
 * @author Atos
 */
public class MiseAJourCompteurTasklet implements Tasklet {

    /** The chorus suivi batch service. */
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /** The suivi batch. */
    private SuiviBatch suiviBatch;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution,
     * org.springframework.batch.core.scope.context.ChunkContext)
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        // lireSuiviBatch assure ChorusSuiviBatch not null
        ChorusSuiviBatch chorusSuiviBatch = this.chorusSuiviBatchService.lireSuiviBatch();

        if (!chorusSuiviBatch.isActif()) {
            chorusSuiviBatch.setActif(Boolean.TRUE);
        }

        Batch51AEnum instance = this.suiviBatch.getInstance();

        if (Batch51AEnum.BATCH_DP.equals(instance)) {
            chorusSuiviBatch.setFso0051aDp(this.suiviBatch.getNumeroIntegration());
        } else if (Batch51AEnum.BATCH_EJ.equals(instance)) {
            chorusSuiviBatch.setFso0051aEj(this.suiviBatch.getNumeroIntegration());
        } else if (Batch51AEnum.BATCH_SF.equals(instance)) {
            chorusSuiviBatch.setFso0051aSf(this.suiviBatch.getNumeroIntegration());
        }

        this.chorusSuiviBatchService.mettreAJourSuiviBatch(chorusSuiviBatch);

        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the suivi batch.
     *
     * @param suiviBatch            the suiviBatch to set
     */
    public void setSuiviBatch(SuiviBatch suiviBatch) {
        this.suiviBatch = suiviBatch;
    }

    /**
     * Sets the chorus suivi batch service.
     *
     * @param chorusSuiviBatchService            the chorusSuiviBatchService to set
     */
    public void setChorusSuiviBatchService(ChorusSuiviBatchService chorusSuiviBatchService) {
        this.chorusSuiviBatchService = chorusSuiviBatchService;
    }

}

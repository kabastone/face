/**
 *
 */
package fr.gouv.sitde.face.batch.commun;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class AiguillageTasklet.
 *
 * @author Atos
 */
@Configuration
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class AiguillageTasklet implements Tasklet {

    /** The input dir. */
    private File inputDir;

    /** The output dir. */
    private File outputDir;

    /**
     * Regexp samples.
     *
     * CEN0072A_REGEXP = "^CEN0072A_\\w+";
     *
     * CEN0111A_REGEXP = "^CEN0111A_\\w+";
     *
     * CEN0159A_REGEXP = "^CEN0159A_\\w+";
     *
     * FSO0028A_REGEXP = "^FSO0028A_\\w+";
     *
     * FSO0051A_DP_REGEXP = "^FSO0051A_[a-zA-Z0-9]+EDP\\w+";
     *
     * FSO0051A_EJ_REGEXP = "^FSO0051A_[a-zA-Z0-9]+EEJ\\w+";
     *
     * FSO0051A_SF_REGEXP = "^FSO0051A_[a-zA-Z0-9]+ESF\\w+";.
     */
    private String regexp;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution,
     * org.springframework.batch.core.scope.context.ChunkContext)
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        // si le repertoire d'input du batch n'existe pas : exception , on ne peut rien faire
        if (!this.inputDir.exists()) {
            throw new TechniqueException("Problème de configuration : le répertoire d'input " + this.inputDir.getName() + " n'existe pas.");
        }

        // si l'ouput n'existe pas on le crée
        if (!this.outputDir.exists()) {
            try {
                Files.createDirectory(this.outputDir.toPath());
            } catch (IOException e) {
                throw new TechniqueException("Problème de configuration : impossible de créer le repertoire de travail " + this.outputDir.getName(),
                        e);
            }
        }

        Pattern pattern = Pattern.compile(this.regexp);
        Matcher matcher;

        // recupere la liste des fichiers en input
        File[] tableauFichiers = this.inputDir.listFiles();

        if (tableauFichiers != null) {

            for (File file : tableauFichiers) {
                matcher = pattern.matcher(file.getName());
                if (matcher.matches()) {
                    try {
                        FileUtils.moveToDirectory(file, this.outputDir, false);
                    } catch (FileExistsException e) {
                        throw new TechniqueException("Le fichier " + file.getName() + " existe déjà dans le dossier " + this.outputDir.getName(), e);
                    } catch (IOException e) {
                        throw new TechniqueException(
                                "Impossible de déplacer le fichier " + file.getName() + " dans le dossier " + this.outputDir.getName(), e);
                    }

                }
            }
        }

        return RepeatStatus.FINISHED;
    }

    /**
     * @param inputDir
     *            the inputDir to set
     */
    public void setInputDir(File inputDir) {
        this.inputDir = inputDir;
    }

    /**
     * @param outputDir
     *            the outputDir to set
     */
    public void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

    /**
     * @param regexp
     *            the regexp to set
     */
    public void setRegexp(String regexp) {
        this.regexp = regexp;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.commun;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class BatchCheminRepertoireProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("batch.chemin.repertoire")
@Validated
public class BatchCheminRepertoireProperties {

    /** The input. */
    @NotEmpty
    private String input;

    /** The output. */
    @NotEmpty
    private String output;

    /**
     * Gets the input.
     *
     * @return the input
     */
    public String getInput() {
        return this.input;
    }

    /**
     * Sets the input.
     *
     * @param input
     *            the input to set
     */
    public void setInput(String input) {
        this.input = input;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return this.output;
    }

    /**
     * @param output
     *            the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

}

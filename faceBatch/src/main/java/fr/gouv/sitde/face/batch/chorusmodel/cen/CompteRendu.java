
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour CompteRenduType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="CompteRenduType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ENTETE_IRRECEVABLE" type="{Compte-rendu_20}EnteteIrrecevableType"/&gt;
 *         &lt;sequence maxOccurs="unbounded"&gt;
 *           &lt;element name="ENTETE_RECEVABLE" type="{Compte-rendu_20}EnteteRecevableType"/&gt;
 *           &lt;element name="LIGNE_DOSSIER" type="{Compte-rendu_20}LigneDossierType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "COMPTE_RENDU")
@XmlType(name = "CompteRenduType", propOrder = { "entetesRecevables", "lignesDossiers", "enteteIrrecevable" })
public class CompteRendu implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The entetes recevables. */
    @XmlElement(name = "ENTETE_RECEVABLE")
    private List<EnteteRecevable> entetesRecevables;

    /** The lignes dossiers. */
    @XmlElement(name = "LIGNE_DOSSIER", nillable = true)
    private List<LigneDossier> lignesDossiers;

    /** The entete irrecevable. */
    @XmlElement(name = "ENTETE_IRRECEVABLE")
    private EnteteIrrecevable enteteIrrecevable;

    /**
     * Gets the value of the entetesRecevables property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the entetesRecevables property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getEntetesRecevables().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link EnteteRecevable }
     *
     * @return the entetes recevables
     */
    public List<EnteteRecevable> getEntetesRecevables() {
        if (this.entetesRecevables == null) {
            this.entetesRecevables = new ArrayList<>();
        }
        return this.entetesRecevables;
    }

    /**
     * Gets the value of the lignesDossiers property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the lignesDossiers property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getLignesDossiers().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link LigneDossier }
     *
     * @return the lignes dossiers
     */
    public List<LigneDossier> getLignesDossiers() {
        if (this.lignesDossiers == null) {
            this.lignesDossiers = new ArrayList<>();
        }
        return this.lignesDossiers;
    }

    /**
     * Obtient la valeur de la propriété enteteIrrecevable.
     *
     * @return possible object is {@link EnteteIrrecevable }
     *
     */
    public EnteteIrrecevable getEnteteIrrecevable() {
        return this.enteteIrrecevable;
    }

    /**
     * Définit la valeur de la propriété enteteIrrecevable.
     *
     * @param value
     *            allowed object is {@link EnteteIrrecevable }
     *
     */
    public void setEnteteIrrecevable(EnteteIrrecevable value) {
        this.enteteIrrecevable = value;
    }

}

package fr.gouv.sitde.face.batch.job.emailengagementtravaux;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

public class EmailEngagementTravauxProcessor implements ItemProcessor<List<DossierSubvention>, Map<Collectivite, List<DossierSubvention>>> {

    /**
     * Process.
     *
     * @param items
     *            the items
     * @return the map
     * @throws Exception
     *             the exception
     */
    @Override
    public Map<Collectivite, List<DossierSubvention>> process(List<DossierSubvention> items) throws Exception {

        return items.stream().collect(Collectors.groupingBy(dossier -> dossier.getDotationCollectivite().getCollectivite()));
    }

}

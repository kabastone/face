package fr.gouv.sitde.face.batch.job.emailengagementtravaux;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * The Class EmailEngagementTravauxWriter.
 */
public class EmailEngagementTravauxWriter implements ItemWriter<Map<Collectivite, List<DossierSubvention>>> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailEngagementTravauxWriter.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

    /**
     * Write.
     *
     * @param items
     *            the items
     * @throws Exception
     *             the exception
     */
    @Override
    public void write(List<? extends Map<Collectivite, List<DossierSubvention>>> items) throws Exception {
        for (Map<Collectivite, List<DossierSubvention>> item : items) {
            for (Entry<Collectivite, List<DossierSubvention>> entry : item.entrySet()) {
                Map<String, Object> variablesContexte = new HashMap<>();
                variablesContexte.put("listeNumDossier", entry.getValue());
                this.emailService.creerEtEnvoyerEmail(entry.getKey(), TypeEmailEnum.ENGAGEMENT_TRAVAUX, TemplateEmailEnum.EMAIL_ENGAGEMENT_TRAVAUX,
                        variablesContexte);
                LOGGER.debug("Envoi de l'email pour la collectivite {}", entry.getKey());
            }

        }

    }

}

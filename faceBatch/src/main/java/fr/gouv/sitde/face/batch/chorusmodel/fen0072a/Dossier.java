
package fr.gouv.sitde.face.batch.chorusmodel.fen0072a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour DossierType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="DossierType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ENTETE" type="{FEN0072A}EnteteType"/&gt;
 *         &lt;element name="POSTE" type="{FEN0072A}PosteType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SERVICE_FAIT")
@XmlType(name = "DossierType", propOrder = { "entete", "lignePoste" })
public class Dossier implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ENTETE", required = true)
    private Entete entete;
    @XmlElement(name = "POSTE", required = true)
    private List<LignePoste> lignePoste;

    /**
     * Obtient la valeur de la propriété entete.
     *
     * @return possible object is {@link Entete }
     *
     */
    public Entete getEntete() {
        return this.entete;
    }

    /**
     * Définit la valeur de la propriété entete.
     *
     * @param value
     *            allowed object is {@link Entete }
     *
     */
    public void setEntete(Entete value) {
        this.entete = value;
    }

    /**
     * Gets the value of the lignePoste property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the lignePoste property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getLignePoste().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link LignePoste }
     *
     *
     */
    public List<LignePoste> getLignePoste() {
        if (this.lignePoste == null) {
            this.lignePoste = new ArrayList<>();
        }
        return this.lignePoste;
    }

    /**
     * @param lignePoste
     *            the lignePoste to set
     */
    public void setLignePoste(List<LignePoste> lignePoste) {
        this.lignePoste = lignePoste;
    }

}

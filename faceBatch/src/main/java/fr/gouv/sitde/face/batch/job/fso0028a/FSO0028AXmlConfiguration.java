/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0028a;

import java.io.File;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.fso0028a.FSO0028A;
import fr.gouv.sitde.face.batch.commun.AiguillageTasklet;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.commun.ResourcesUtils;
import fr.gouv.sitde.face.batch.commun.SuppressionFichiersTasklet;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class FSO0028AXmlConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class FSO0028AXmlConfiguration {

    /** The Constant CEN_0111A_REGEXP. */
    private static final String FSO_0028A_REGEXP = "^FSO0028A_[a-zA-Z0-9]+\\w+";

    /** The Constant SOUS_REP_INPUT_FSO0028A. */
    public static final String SOUS_REP_INPUT_FSO0028A = "/fso0028a";

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /** The input file. */
    private File inputFile;

    /** The input global. */
    private File inputGlobal;

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile() {
        if (this.inputFile == null) {
            this.inputFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0028A);
        }
        return this.inputFile;
    }

    /**
     * Gets the input global.
     *
     * @return the input global
     */
    public File getInputGlobal() {
        if (this.inputGlobal == null) {
            this.inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        }
        return this.inputGlobal;
    }

    /**
     * Step aiguillage FSO 0028 A.
     *
     * Step 0 : on va chercher les fichiers propres au batch dans le repertoire global
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAiguillageFSO0028A() {
        AiguillageTasklet tasklet = new AiguillageTasklet();
        tasklet.setInputDir(this.getInputGlobal());
        tasklet.setOutputDir(this.getInputFile());
        tasklet.setRegexp(FSO_0028A_REGEXP);

        return this.stepBuilderFactory.get("step_aiguillage").tasklet(tasklet).build();
    }

    /**
     * Gets the ressources from input directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromInputDirectory() {
        File repertoireInputJob = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0028A);
        return ResourcesUtils.getRessourcesFromDirectory(repertoireInputJob);
    }

    /**
     * Multi resource FSO0028A reader.
     *
     * @return the multi resource item reader
     */
    @Bean
    @StepScope
    public MultiResourceItemReader<FSO0028A> multiResourceFSO0028AReader() {
        Resource[] tableauResources = this.getRessourcesFromInputDirectory();
        MultiResourceItemReader<FSO0028A> resourceItemReader = new MultiResourceItemReader<>();
        resourceItemReader.setResources(tableauResources);
        // Pour chaque fichier, on délègue la lecture au dossierXmlReader
        resourceItemReader.setDelegate(this.fSO0028APaiementXmlReader());
        return resourceItemReader;
    }

    /**
     * Ligne dossier paiement xml reader.
     *
     * @return the stax event item reader
     */
    @Bean
    @StepScope
    public StaxEventItemReader<FSO0028A> fSO0028APaiementXmlReader() {
        StaxEventItemReader<FSO0028A> fso0028APaiementXmlReader = new StaxEventItemReader<>();
        fso0028APaiementXmlReader.setFragmentRootElementName("FSO0028A");
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(FSO0028A.class);
        fso0028APaiementXmlReader.setUnmarshaller(marshaller);

        return fso0028APaiementXmlReader;
    }

    /**
     * Ligne dossier paiement processor.
     *
     * @return the ligne dossier paiement processor
     */
    @Bean
    @StepScope
    public FSO0028AProcessor fso0028aProcessor() {
        return new FSO0028AProcessor();
    }

    /**
     * Ligne dossier paiement writer.
     *
     * @return the ligne dossier paiement writer
     */
    @Bean
    @StepScope
    public FSO0028ADtoWriter fso0028aDtoWriter() {
        return new FSO0028ADtoWriter();
    }

    /**
     * Step ligne dossier paiement xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepFSO0028AXml() {
        return this.stepBuilderFactory.get("step_fso_0028a_xml").<FSO0028A, FSO0028ADto> chunk(1).reader(this.multiResourceFSO0028AReader())
                .processor(this.fso0028aProcessor()).writer(this.fso0028aDtoWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy())
                .build();
    }

    /**
     * Step suppression fichiers fso0028a xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFSO0028AXml() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromInputDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers").tasklet(tasklet).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FSO_0028A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FSO_0028A_JOB.getNom()).start(this.stepAiguillageFSO0028A()).next(this.stepFSO0028AXml())
                .next(this.stepSuppressionFSO0028AXml()).build();
    }
}

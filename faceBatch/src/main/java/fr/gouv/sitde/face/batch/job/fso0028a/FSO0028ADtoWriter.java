/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0028a;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * @author A754839
 *
 */
public class FSO0028ADtoWriter implements ItemWriter<FSO0028ADto> {

    @Inject
    private DossierSubventionService dossierSubventionService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends FSO0028ADto> items) throws Exception {
        for (FSO0028ADto dto : items) {
            for (DossierSubvention dossier : dto.getListeDossier()) {
                this.dossierSubventionService.mettreAJourDossierSubvention(dossier);
            }
        }
    }

}

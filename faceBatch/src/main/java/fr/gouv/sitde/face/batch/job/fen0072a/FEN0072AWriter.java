/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0072a;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.Fen0072A;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class FEN0159AWriter.
 */
public class FEN0072AWriter implements ItemWriter<Fen0072A> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FEN0072AWriter.class);

    /** The Constant NB_CHIFFRES_NOM_FICHIERS. */
    private static final int NB_CHIFFRES_NOM_FICHIERS = 5;

    /** The Constant FILE_NAME_SEPARATOR. */
    private static final char FILE_NAME_SEPARATOR = '_';

    /** The chemin output. */
    private String cheminOutput;

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /** The chorus suivi batch service. */
    @Inject
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /**
     * Instantiates a new FEN 0072 A writer.
     *
     * @param cheminOutput
     *            the chemin output
     */
    public FEN0072AWriter(String cheminOutput) {
        super();
        this.cheminOutput = cheminOutput;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends Fen0072A> items) throws Exception {
        for (Fen0072A dto : items) {

            StaxWriterCustomFEN0072A writer = this.creerWriter();
            List<Fen0072A> fenListe = new ArrayList<>(1);
            fenListe.add(dto);
            writer.write(fenListe);
            writer.close();
        }
    }

    /**
     * Creer writer.
     *
     * @return the stax event item writer<? super rattachement P j>
     */
    private StaxWriterCustomFEN0072A creerWriter() {
        String fileName = this.genererNomFichier();
        File file = new File(fileName);
        try {
            if (!file.createNewFile()) {
                LOGGER.debug("Le fichier {} existe déjà.", fileName);
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
        if (!file.setWritable(true)) {
            throw new TechniqueException("Ecriture impossible dans le fichier");
        }
        FileSystemResource fsr = new FileSystemResource(fileName);
        StaxWriterCustomFEN0072A staxEventItemWriter = new StaxWriterCustomFEN0072A();
        staxEventItemWriter.setResource(fsr);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(Fen0072A.class);

        staxEventItemWriter.setMarshaller(marshaller);
        staxEventItemWriter.setEncoding(ConstantesFace.ISO_8859_15);

        staxEventItemWriter.setOverwriteOutput(true);
        // Ouverture du writer.
        ExecutionContext exec = new ExecutionContext();
        staxEventItemWriter.open(exec);

        return staxEventItemWriter;
    }

    /**
     * Generer nom fichier.
     *
     * @return the string
     */
    private String genererNomFichier() {

        FluxChorus referentiel = this.fluxChorusService.rechercherFluxChorus();

        StringBuilder builder = new StringBuilder(this.cheminOutput);
        builder.append(File.separator);
        builder.append("FEN0072A");
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append("0072");
        builder.append(this.getNombreFichiers());
        return builder.toString();
    }

    /**
     * Gets the nombre fichiers.
     *
     * @return the nombre fichiers
     */
    private String getNombreFichiers() {

        ChorusSuiviBatch suivi = this.chorusSuiviBatchService.lireSuiviBatch();
        int nbFichiers = suivi.getFen0072a();

        if (nbFichiers == ConstantesFace.MAX_DOCUMENTS_CHORUS) {
            nbFichiers = 1;
        } else {
            nbFichiers++;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(nbFichiers);
        while (builder.length() < NB_CHIFFRES_NOM_FICHIERS) {
            builder.insert(0, "0");
        }
        suivi.setFen0072a(nbFichiers);
        this.chorusSuiviBatchService.mettreAJourSuiviBatch(suivi);

        return builder.toString();
    }
}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051aej;

import java.math.BigDecimal;

/**
 * The Class EngagementJuridiqueDto.
 *
 * @author Atos
 */
public class EngagementJuridiqueDto {

    /** The numero sequence. */
    private Integer numeroSequence;

    /** The numero engagement juridique. */
    private String numeroEngagementJuridique;

    /** The poste de document. */
    private String posteDeDocument;

    /** The quantite commandee. */
    private BigDecimal quantiteCommandee;

    /** The date de livraison du poste. */
    private String dateDeLivraisonDuPoste;

    /** The statut cloture poste EJ. */
    private String statutCloturePosteEJ;

    /**
     * Gets the numero sequence.
     *
     * @return the numeroSequence
     */
    public Integer getNumeroSequence() {
        return this.numeroSequence;
    }

    /**
     * Sets the numero sequence.
     *
     * @param numeroSequence
     *            the numeroSequence to set
     */
    public void setNumeroSequence(Integer numeroSequence) {
        this.numeroSequence = numeroSequence;
    }

    /**
     * Gets the numero engagement juridique.
     *
     * @return the numeroEngagementJuridique
     */
    public String getNumeroEngagementJuridique() {
        return this.numeroEngagementJuridique;
    }

    /**
     * Sets the numero engagement juridique.
     *
     * @param numeroEngagementJuridique
     *            the numeroEngagementJuridique to set
     */
    public void setNumeroEngagementJuridique(String numeroEngagementJuridique) {
        this.numeroEngagementJuridique = numeroEngagementJuridique;
    }

    /**
     * Gets the poste de document.
     *
     * @return the posteDeDocument
     */
    public String getPosteDeDocument() {
        return this.posteDeDocument;
    }

    /**
     * Sets the poste de document.
     *
     * @param posteDeDocument
     *            the posteDeDocument to set
     */
    public void setPosteDeDocument(String posteDeDocument) {
        this.posteDeDocument = posteDeDocument;
    }

    /**
     * Gets the quantite commandee.
     *
     * @return the quantiteCommandee
     */
    public BigDecimal getQuantiteCommandee() {
        return this.quantiteCommandee;
    }

    /**
     * Sets the quantite commandee.
     *
     * @param quantiteCommandee
     *            the quantiteCommandee to set
     */
    public void setQuantiteCommandee(BigDecimal quantiteCommandee) {
        this.quantiteCommandee = quantiteCommandee;
    }

    /**
     * Gets the date de livraison du poste.
     *
     * @return the dateDeLivraisonDuPoste
     */
    public String getDateDeLivraisonDuPoste() {
        return this.dateDeLivraisonDuPoste;
    }

    /**
     * Sets the date de livraison du poste.
     *
     * @param dateDeLivraisonDuPoste
     *            the dateDeLivraisonDuPoste to set
     */
    public void setDateDeLivraisonDuPoste(String dateDeLivraisonDuPoste) {
        this.dateDeLivraisonDuPoste = dateDeLivraisonDuPoste;
    }

    /**
     * Gets the statut cloture poste EJ.
     *
     * @return the statutCloturePosteEJ
     */
    public String getStatutCloturePosteEJ() {
        return this.statutCloturePosteEJ;
    }

    /**
     * Sets the statut cloture poste EJ.
     *
     * @param statutCloturePosteEJ
     *            the statutCloturePosteEJ to set
     */
    public void setStatutCloturePosteEJ(String statutCloturePosteEJ) {
        this.statutCloturePosteEJ = statutCloturePosteEJ;
    }

}

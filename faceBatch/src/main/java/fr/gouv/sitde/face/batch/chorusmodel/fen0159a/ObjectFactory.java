
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.sitde.face.batch.job.fen0159a.model package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.sitde.face.batch.job.fen0159a.model
     *
     */
    public ObjectFactory() {
        // Source générée et non utilisée.
    }

    /**
     * Create an instance of {@link FEN0159A }
     *
     */
    public FEN0159A createFEN0159A() {
        return new FEN0159A();
    }

    /**
     * Create an instance of {@link RattachementPJ }
     *
     */
    public RattachementPJ createRattachementPJ() {
        return new RattachementPJ();
    }

    /**
     * Create an instance of {@link ObjetsMetiersChorus }
     *
     */
    public ObjetsMetiersChorus createObjetsMetiersChorus() {
        return new ObjetsMetiersChorus();
    }

    /**
     * Create an instance of {@link DAType }
     *
     */
    public DaType createDAType() {
        return new DaType();
    }

    /**
     * Create an instance of {@link EJType }
     *
     */
    public EjType createEJType() {
        return new EjType();
    }

    /**
     * Create an instance of {@link ServiceFait }
     *
     */
    public ServiceFait createServiceFait() {
        return new ServiceFait();
    }

    /**
     * Create an instance of {@link AvanceType }
     *
     */
    public AvanceType createAvanceType() {
        return new AvanceType();
    }

    /**
     * Create an instance of {@link DpSurEjType }
     *
     */
    public DpSurEjType createDpSurEjType() {
        return new DpSurEjType();
    }

    /**
     * Create an instance of {@link DPDirecteType }
     *
     */
    public DpDirecteType createDPDirecteType() {
        return new DpDirecteType();
    }

    /**
     * Create an instance of {@link BudgetType }
     *
     */
    public BudgetType createBudgetType() {
        return new BudgetType();
    }

    /**
     * Create an instance of {@link ReservationCreditType }
     *
     */
    public ReservationCreditType createReservationCreditType() {
        return new ReservationCreditType();
    }

    /**
     * Create an instance of {@link FicheArticleType }
     *
     */
    public FicheArticleType createFicheArticleType() {
        return new FicheArticleType();
    }

    /**
     * Create an instance of {@link ODType }
     *
     */
    public OdType createODType() {
        return new OdType();
    }

    /**
     * Create an instance of {@link FactureAvecETType }
     *
     */
    public FactureAvecEtType createFactureAvecETType() {
        return new FactureAvecEtType();
    }

    /**
     * Create an instance of {@link FactureSansETType }
     *
     */
    public FactureSansEtType createFactureSansETType() {
        return new FactureSansEtType();
    }

    /**
     * Create an instance of {@link RecettesAuComptantType }
     *
     */
    public RecettesAuComptantType createRecettesAuComptantType() {
        return new RecettesAuComptantType();
    }

    /**
     * Create an instance of {@link FicheFournisseurDonneesGenType }
     *
     */
    public FicheFournisseurDonneesGenType createFicheFournisseurDonneesGenType() {
        return new FicheFournisseurDonneesGenType();
    }

    /**
     * Create an instance of {@link FicheFournisseurSocieteType }
     *
     */
    public FicheFournisseurSocieteType createFicheFournisseurSocieteType() {
        return new FicheFournisseurSocieteType();
    }

    /**
     * Create an instance of {@link FicheFournisseurOrgAchatType }
     *
     */
    public FicheFournisseurOrgAchatType createFicheFournisseurOrgAchatType() {
        return new FicheFournisseurOrgAchatType();
    }

    /**
     * Create an instance of {@link FicheClientType }
     *
     */
    public FicheClientType createFicheClientType() {
        return new FicheClientType();
    }

    /**
     * Create an instance of {@link PiecePSCDType }
     *
     */
    public PiecePSCDType createPiecePSCDType() {
        return new PiecePSCDType();
    }

    /**
     * Create an instance of {@link PartenaireType }
     *
     */
    public PartenaireType createPartenaireType() {
        return new PartenaireType();
    }

    /**
     * Create an instance of {@link FicheImmobilisationType }
     *
     */
    public FicheImmobilisationType createFicheImmobilisationType() {
        return new FicheImmobilisationType();
    }

    /**
     * Create an instance of {@link DefinitionProjetType }
     *
     */
    public DefinitionProjetType createDefinitionProjetType() {
        return new DefinitionProjetType();
    }

    /**
     * Create an instance of {@link ElementOTPType }
     *
     */
    public ElementOTPType createElementOTPType() {
        return new ElementOTPType();
    }

    /**
     * Create an instance of {@link ObjetArchitectureType }
     *
     */
    public ObjetArchitectureType createObjetArchitectureType() {
        return new ObjetArchitectureType();
    }

    /**
     * Create an instance of {@link UniteEconomiqueType }
     *
     */
    public UniteEconomiqueType createUniteEconomiqueType() {
        return new UniteEconomiqueType();
    }

    /**
     * Create an instance of {@link BatimentType }
     *
     */
    public BatimentType createBatimentType() {
        return new BatimentType();
    }

    /**
     * Create an instance of {@link TerrainType }
     *
     */
    public TerrainType createTerrainType() {
        return new TerrainType();
    }

    /**
     * Create an instance of {@link ObjetLoueType }
     *
     */
    public ObjetLoueType createObjetLoueType() {
        return new ObjetLoueType();
    }

    /**
     * Create an instance of {@link ContratImmobilierType }
     *
     */
    public ContratImmobilierType createContratImmobilierType() {
        return new ContratImmobilierType();
    }

}

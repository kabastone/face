/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0072a;

import java.io.File;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.cen.LigneDossier;
import fr.gouv.sitde.face.batch.commun.AiguillageTasklet;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.commun.ResourcesUtils;
import fr.gouv.sitde.face.batch.commun.SuppressionFichiersTasklet;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class CEN0072AXmlConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class CEN0072AXmlConfiguration {

    /** The Constant CEN_0072A_REGEXP. */
    private static final String CEN_0072A_REGEXP = "^CEN0072A_[a-zA-Z0-9]+\\w+";

    /** The Constant SOUS_REP_INPUT_CEN0072A. */
    public static final String SOUS_REP_INPUT_CEN0072A = "/cen0072a";

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /** The input file. */
    private File inputFile;

    /** The input global. */
    private File inputGlobal;

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile() {
        if (this.inputFile == null) {
            this.inputFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_CEN0072A);
        }
        return this.inputFile;
    }

    /**
     * Gets the input global.
     *
     * @return the input global
     */
    public File getInputGlobal() {
        if (this.inputGlobal == null) {
            this.inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        }
        return this.inputGlobal;
    }

    /**
     * Gets the ressources from input directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromInputDirectory() {
        File repertoireInputJob = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_CEN0072A);
        return ResourcesUtils.getRessourcesFromDirectory(repertoireInputJob);
    }

    /**
     * Multi resource ligne dossier paiement reader.
     *
     * @return the multi resource item reader
     */
    @Bean
    @StepScope
    public MultiResourceItemReader<LigneDossier> multiResourceLigneDossierPaiementReader() {
        Resource[] tableauResources = this.getRessourcesFromInputDirectory();
        MultiResourceItemReader<LigneDossier> resourceItemReader = new MultiResourceItemReader<>();
        resourceItemReader.setResources(tableauResources);
        // Pour chaque fichier, on délègue la lecture au dossierXmlReader
        resourceItemReader.setDelegate(this.ligneDossierPaiementXmlReader());
        return resourceItemReader;
    }

    /**
     * Ligne dossier paiement xml reader.
     *
     * @return the stax event item reader
     */
    @Bean
    @StepScope
    public StaxEventItemReader<LigneDossier> ligneDossierPaiementXmlReader() {
        StaxEventItemReader<LigneDossier> ligneDossierXmlReader = new StaxEventItemReader<>();
        ligneDossierXmlReader.setFragmentRootElementName("LIGNE_DOSSIER");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(LigneDossier.class);
        ligneDossierXmlReader.setUnmarshaller(marshaller);

        return ligneDossierXmlReader;
    }

    /**
     * Step aiguillage CEN 0072 A.
     *
     * Step 0 : on va chercher les fichiers propres au batch dans le repertoire global
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAiguillageCEN0072A() {
        AiguillageTasklet tasklet = new AiguillageTasklet();
        tasklet.setInputDir(this.getInputGlobal());
        tasklet.setOutputDir(this.getInputFile());
        tasklet.setRegexp(CEN_0072A_REGEXP);

        return this.stepBuilderFactory.get("step_aiguillage").tasklet(tasklet).build();
    }

    /**
     * Ligne dossier paiement processor.
     *
     * @return the ligne dossier paiement processor
     */
    @Bean
    @StepScope
    public LigneDossierPaiementProcessor ligneDossierPaiementProcessor() {
        return new LigneDossierPaiementProcessor();
    }

    /**
     * Ligne dossier paiement writer.
     *
     * @return the ligne dossier paiement writer
     */
    @Bean
    @StepScope
    public LigneDossierPaiementWriter ligneDossierPaiementWriter() {
        return new LigneDossierPaiementWriter();
    }

    /**
     * Step ligne dossier paiement xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepLigneDossierPaiementXml() {
        return this.stepBuilderFactory.get("step_ligne_dossier_paiement_xml").<LigneDossier, LigneDossierPaiementTraitementDto> chunk(1)
                .reader(this.multiResourceLigneDossierPaiementReader()).processor(this.ligneDossierPaiementProcessor())
                .writer(this.ligneDossierPaiementWriter()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Step suppression fichiers paiement xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersPaiementXml() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromInputDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers").tasklet(tasklet).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.CEN_0072A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.CEN_0072A_JOB.getNom()).start(this.stepAiguillageCEN0072A())
                .next(this.stepLigneDossierPaiementXml()).next(this.stepSuppressionFichiersPaiementXml()).build();
    }

}

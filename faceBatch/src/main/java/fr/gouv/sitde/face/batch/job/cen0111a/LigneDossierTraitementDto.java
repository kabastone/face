/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0111a;

import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * The Class LigneDossierTraitementDto.
 */
public class LigneDossierTraitementDto {

    /** The anomalie. */
    private Anomalie anomalie;

    /** The demande subvention. */
    private DemandeSubvention demandeSubvention;

    /**
     * Instantiates a new ligne dossier traitement DTO.
     *
     * @param demandeSubvention
     *            the demande subvention
     */
    public LigneDossierTraitementDto(DemandeSubvention demandeSubvention) {
        this.anomalie = null;
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * Instantiates a new ligne dossier traitement DTO.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @param anomalie
     *            the anomalie
     */
    public LigneDossierTraitementDto(DemandeSubvention demandeSubvention, Anomalie anomalie) {
        this.anomalie = anomalie;
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * Gets the anomalie.
     *
     * @return the anomalie
     */
    public Anomalie getAnomalie() {
        return this.anomalie;
    }

    /**
     * Sets the anomalie.
     *
     * @param anomalie
     *            the new anomalie
     */
    public void setAnomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
    }

    /**
     * Gets the demande subvention.
     *
     * @return the demande subvention
     */
    public DemandeSubvention getDemandeSubvention() {
        return this.demandeSubvention;
    }

    /**
     * Sets the demande subvention.
     *
     * @param demandeSubvention
     *            the new demande subvention
     */
    public void setDemandeSubvention(DemandeSubvention demandeSubvention) {
        this.demandeSubvention = demandeSubvention;
    }

    /**
     * Checks for anomalie.
     *
     * @return true, if successful
     */
    public boolean hasAnomalie() {
        return this.anomalie != null;
    }
}

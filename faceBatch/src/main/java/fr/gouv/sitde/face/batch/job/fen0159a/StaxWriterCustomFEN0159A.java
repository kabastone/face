/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0159a;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import org.springframework.batch.item.xml.StaxEventItemWriter;

import fr.gouv.sitde.face.batch.chorusmodel.fen0159a.FEN0159A;

/**
 * The Class StaxWriterCustomFEN0159A.
 */
public class StaxWriterCustomFEN0159A extends StaxEventItemWriter<FEN0159A> {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.xml.StaxEventItemWriter#startDocument(javax.xml.stream.XMLEventWriter)
     */
    @Override
    protected void startDocument(XMLEventWriter writer) throws XMLStreamException {
        XMLEventFactory factory = this.createXmlEventFactory();

        // Ecrit la première ligne indispensable au xml et rien d'autre.
        writer.add(factory.createStartDocument(this.getEncoding(), this.getVersion()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.xml.StaxEventItemWriter#endDocument(javax.xml.stream.XMLEventWriter)
     */
    @Override
    protected void endDocument(XMLEventWriter writer) {
        // Laisser vide pour éviter les root elements du StaxWriter.
    }

}

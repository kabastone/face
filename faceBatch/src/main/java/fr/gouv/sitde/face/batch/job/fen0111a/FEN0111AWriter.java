/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0111a;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EngagementsJuridiques;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * @author A754839
 *
 */
public class FEN0111AWriter implements ItemStreamWriter<FEN0111ADto> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FEN0111AWriter.class);

    /** The Constant NB_CHIFFRES_NOM_FICHIERS. */
    private static final int NB_CHIFFRES_NOM_FICHIERS = 5;

    /** The Constant INIT_MAP_SIZE. */
    private static final int INIT_MAP_SIZE = 5;

    /** The Constant FILE_NAME_SEPARATOR. */
    private static final char FILE_NAME_SEPARATOR = '_';

    /** The chemin output. */
    private String cheminOutput;

    /** The delegates. */
    private Map<String, StaxWriterCustomFEN0111A> delegates = new HashMap<>(INIT_MAP_SIZE);

    /** The filenames. */
    private List<String> filenames = new ArrayList<>(INIT_MAP_SIZE);

    /** The list execution context. */
    private Map<String, ExecutionContext> listExecutionContext = new HashMap<>(INIT_MAP_SIZE);

    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /** The chorus suivi batch service. */
    @Inject
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /**
     * Instantiates a new FEN 0111 A writer.
     *
     * @param cheminOutput
     *            the chemin output
     */
    public FEN0111AWriter(String cheminOutput) {
        super();
        this.cheminOutput = cheminOutput;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends FEN0111ADto> items) throws Exception {
        for (FEN0111ADto dto : items) {

            this.demandeSubventionService.initierTransfertDemandeSubvention(dto.getDemande().getId());

            StaxWriterCustomFEN0111A writer = this.creerWriter();
            List<EngagementsJuridiques> tempListe = new ArrayList<>(1);
            tempListe.add(dto.getEngagements());
            writer.write(tempListe);
        }
    }

    /**
     * Creer writer.
     *
     * @return the stax event item writer custom.
     */
    private StaxWriterCustomFEN0111A creerWriter() {
        String fileName = this.genererNomFichier();
        File file = new File(fileName);
        try {
            if (!file.createNewFile()) {
                LOGGER.debug("Le fichier {} existe déjà.", fileName);
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
        if (!file.setWritable(true)) {
            throw new TechniqueException("Ecriture impossible dans le fichier");
        }
        FileSystemResource fsr = new FileSystemResource(fileName);
        StaxWriterCustomFEN0111A staxEventItemWriter = new StaxWriterCustomFEN0111A();
        staxEventItemWriter.setResource(fsr);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(EngagementsJuridiques.class);
        staxEventItemWriter.setMarshaller(marshaller);
        staxEventItemWriter.setEncoding(ConstantesFace.ISO_8859_15);

        staxEventItemWriter.setOverwriteOutput(true);
        // Ouverture du writer.
        ExecutionContext exec = new ExecutionContext();
        staxEventItemWriter.open(exec);

        // Pour conserver afin de fermer tout les writers créés après coup.
        this.filenames.add(fileName);
        this.delegates.put(fileName, staxEventItemWriter);
        this.listExecutionContext.put(fileName, exec);
        return staxEventItemWriter;
    }

    /**
     * Generer nom fichier.
     *
     * @return the string
     */
    private String genererNomFichier() {

        FluxChorus referentiel = this.fluxChorusService.rechercherFluxChorus();

        StringBuilder builder = new StringBuilder(this.cheminOutput);
        builder.append(File.separator);
        builder.append("FEN0111A");
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append(FILE_NAME_SEPARATOR);
        builder.append(referentiel.getCodeApplication());
        builder.append("0111");
        builder.append(this.getNombreFichiers());
        return builder.toString();
    }

    /**
     * Gets the nombre fichiers.
     *
     * @return the nombre fichiers
     */
    private String getNombreFichiers() {
        File dir = new File(this.cheminOutput);

        if (!dir.isDirectory()) {
            throw new TechniqueException("Pas de dossier pour batch FEN0111A");
        }

        ChorusSuiviBatch suivi = this.chorusSuiviBatchService.lireSuiviBatch();
        int nbFichiers = suivi.getFen0111a();

        if (nbFichiers == ConstantesFace.MAX_DOCUMENTS_CHORUS) {
            nbFichiers = 1;
        } else {
            nbFichiers++;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(nbFichiers);
        while (builder.length() < NB_CHIFFRES_NOM_FICHIERS) {
            builder.insert(0, "0");
        }
        suivi.setFen0111a(nbFichiers);
        this.chorusSuiviBatchService.mettreAJourSuiviBatch(suivi);

        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#open(org.springframework.batch.item.ExecutionContext)
     */
    @Override
    public void open(ExecutionContext executionContext) {
        // Ne fait rien car il délègue le travail à d'autres writers qui ont leur propre executionContext.
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#update(org.springframework.batch.item.ExecutionContext)
     */
    @Override
    public void update(ExecutionContext executionContext) {
        if (this.delegates != null) {
            for (String filename : this.filenames) {
                StaxWriterCustomFEN0111A writer = this.delegates.get(filename);
                ExecutionContext exec = this.listExecutionContext.get(filename);
                writer.update(exec);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemStream#close()
     */
    @Override
    public void close() {
        if ((this.delegates != null) && (this.filenames != null)) {
            for (String filename : this.filenames) {
                StaxWriterCustomFEN0111A writer = this.delegates.get(filename);
                writer.close();

                this.delegates.remove(filename);
            }
        }
    }

}

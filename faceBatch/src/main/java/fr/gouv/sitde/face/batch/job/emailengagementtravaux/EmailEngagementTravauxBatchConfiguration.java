package fr.gouv.sitde.face.batch.job.emailengagementtravaux;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Class EmailEngagementTravauxBatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailEngagementTravauxBatchConfiguration {

	/** The step builder factory. */
	@Inject
	private StepBuilderFactory stepBuilderFactory;

	/** The job builder factory. */
	@Inject
	private JobBuilderFactory jobBuilderFactory;

	/** The dossier subvention service. */
	@Inject
	private DossierSubventionService dossierSubventionService;

	/**
	 * Collectivite expiration reader.
	 *
	 * @return the list item reader
	 */
	@Bean
	@StepScope
	public ListItemReader<List<DossierSubvention>> engagementTravauxReader() {
		List<DossierSubvention> listeDossierSubvention = this.dossierSubventionService
				.rechercherDossierSubventionPourMailEngagementTravaux();
		List<List<DossierSubvention>> itemReader = new ArrayList<>();
		itemReader.add(listeDossierSubvention);
		return new ListItemReader<>(itemReader);
	}

	/**
	 * Email engagement travaux writer.
	 *
	 * @return the email engagement travaux writer
	 */
	@Bean
	@StepScope
	public EmailEngagementTravauxWriter emailEngagementTravauxWriter() {
		return new EmailEngagementTravauxWriter();
	}

	/**
	 * Email engagement travaux processor.
	 *
	 * @return the email engagement travaux processor
	 */
	@Bean
	@StepScope
	public EmailEngagementTravauxProcessor emailEngagementTravauxProcessor() {
		return new EmailEngagementTravauxProcessor();
	}

	/**
	 * Step email engagement travaux.
	 *
	 * @return the step
	 */
	@Bean
	@JobScope
	public Step stepEmailEngagementTravaux() {
		return this.stepBuilderFactory.get("step_email_engagement_travaux")
				.<List<DossierSubvention>, Map<Collectivite, List<DossierSubvention>>>chunk(1)
				.reader(this.engagementTravauxReader()).processor(this.emailEngagementTravauxProcessor())
				.writer(this.emailEngagementTravauxWriter()).build();
	}

	/**
	 * Job.
	 *
	 * @return the job
	 */
	@Bean(name = JobEnum.Constantes.EMAIL_ENGAGEMENT_TRAVAUX_JOB)
	public Job job() {
		return this.jobBuilderFactory.get(JobEnum.EMAIL_ENGAGEMENT_TRAVAUX_JOB.getNom())
				.start(this.stepEmailEngagementTravaux()).build();
	}

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.emailrelance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * @author A754839
 *
 */
public class EmailRelanceWriter implements ItemWriter<DossierSubvention> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailRelanceWriter.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

    @Override
    public void write(List<? extends DossierSubvention> items) throws Exception {
        for (DossierSubvention dossier : items) {

            Map<String, Object> variablesContexte = new HashMap<>(1);
            variablesContexte.put("nomDossier", dossier.getNumDossier());
            this.emailService.creerEtEnvoyerEmail(dossier, TypeEmailEnum.DOS_RELANCE, TemplateEmailEnum.EMAIL_RELANCE, variablesContexte);

            LOGGER.debug("Envoi de l'email pour le dossier {}", dossier.getNumDossier());
        }
    }

}

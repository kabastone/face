
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'un engagement juridique dans CHORUS
 *
 * <p>
 * Classe Java pour EJType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EJType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="BE_PUR_ORG" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *         &lt;element name="BE_PUR_GROUP" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EJType", propOrder = { "idChorus", "bePurOrg", "bePurGroup" })
public class EjType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "BE_PUR_ORG", required = true)
    private String bePurOrg;
    @XmlElement(name = "BE_PUR_GROUP", required = true)
    private String bePurGroup;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété bePurOrg.
     *
     * @return possible object is {@link String }
     *
     */
    public String getBePurOrg() {
        return this.bePurOrg;
    }

    /**
     * Définit la valeur de la propriété bePurOrg.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setBePurOrg(String value) {
        this.bePurOrg = value;
    }

    /**
     * Obtient la valeur de la propriété bePurGroup.
     *
     * @return possible object is {@link String }
     *
     */
    public String getBePurGroup() {
        return this.bePurGroup;
    }

    /**
     * Définit la valeur de la propriété bePurGroup.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setBePurGroup(String value) {
        this.bePurGroup = value;
    }

}

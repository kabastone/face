
package fr.gouv.sitde.face.batch.chorusmodel.cen;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * Ligne recapitulative pour un fichier recevable. Une ligne par module ayant envoye un fichier de CR pour le meme fichier entrant d'origine et pour
 * lequel la prise en compte de ce fichier est parametree.
 *
 * <p>
 * Classe Java pour EnteteRecevableType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EnteteRecevableType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TYPE_LIGNE" type="{Compte-rendu_20}TypeLigneRejComType"/&gt;
 *         &lt;element name="ID_FICHIER_EMETTEUR" type="{Compte-rendu_20}IdFichierEmetteurType"/&gt;
 *         &lt;element name="CODE_APPLICATION_CR" type="{Compte-rendu_20}CodeApplicationCrType"/&gt;
 *         &lt;element name="CODE_VENTILATION_CR" type="{Compte-rendu_20}CodeVentilationCrType" minOccurs="0"/&gt;
 *         &lt;element name="NB_DOSSIERS_REMIS_PAR_SE" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="NB_DOSSIERS_RECUS" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="NB_DOSSIERS_TRAITES_OK" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="NB_DOSSIERS_REJETES" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="NB_DOSSIERS_FILTRES" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="NB_DOSSIERS_CREES" type="{Compte-rendu_20}NumeriqueType"/&gt;
 *         &lt;element name="DATETIME" type="{Compte-rendu_20}DateTimeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ENTETE_RECEVABLE")
@XmlType(name = "EnteteRecevableType", propOrder = { "typeLigne", "idFichierEmetteur", "codeApplicationCr", "codeVentilationCr",
        "nbDossiersRemisParSe", "nbDossiersRecus", "nbDossiersTraitesOk", "nbDossiersRejetes", "nbDossiersFiltres", "nbDossiersCrees", "dateTime" })
public class EnteteRecevable implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type ligne. */
    @XmlElement(name = "TYPE_LIGNE", required = true)
    @XmlSchemaType(name = "string")
    private TypeLigneRejComEnum typeLigne;

    /** The id fichier emetteur. */
    @XmlElement(name = "ID_FICHIER_EMETTEUR", required = true)
    private String idFichierEmetteur;

    /** The code application cr. */
    @XmlElement(name = "CODE_APPLICATION_CR", required = true)
    private String codeApplicationCr;

    /** The code ventilation cr. */
    @XmlElement(name = "CODE_VENTILATION_CR", nillable = true)
    private String codeVentilationCr;

    /** The nb dossiers remis par se. */
    @XmlElement(name = "NB_DOSSIERS_REMIS_PAR_SE")
    private long nbDossiersRemisParSe;

    /** The nb dossiers recus. */
    @XmlElement(name = "NB_DOSSIERS_RECUS")
    private long nbDossiersRecus;

    /** The nb dossiers traites ok. */
    @XmlElement(name = "NB_DOSSIERS_TRAITES_OK")
    private long nbDossiersTraitesOk;

    /** The nb dossiers rejetes. */
    @XmlElement(name = "NB_DOSSIERS_REJETES")
    private long nbDossiersRejetes;

    /** The nb dossiers filtres. */
    @XmlElement(name = "NB_DOSSIERS_FILTRES")
    private long nbDossiersFiltres;

    /** The nb dossiers crees. */
    @XmlElement(name = "NB_DOSSIERS_CREES")
    private long nbDossiersCrees;

    /** The date time. */
    @XmlElement(name = "DATETIME", required = true)
    private String dateTime;

    /**
     * Obtient la valeur de la propriété typeLigne.
     *
     * @return possible object is {@link TypeLigneRejComEnum }
     *
     */
    public TypeLigneRejComEnum getTypeLigne() {
        return this.typeLigne;
    }

    /**
     * Définit la valeur de la propriété typeLigne.
     *
     * @param value
     *            allowed object is {@link TypeLigneRejComEnum }
     *
     */
    public void setTypeLigne(TypeLigneRejComEnum value) {
        this.typeLigne = value;
    }

    /**
     * Obtient la valeur de la propriété idFichierEmetteur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdFichierEmetteur() {
        return this.idFichierEmetteur;
    }

    /**
     * Définit la valeur de la propriété idFichierEmetteur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdFichierEmetteur(String value) {
        this.idFichierEmetteur = value;
    }

    /**
     * Obtient la valeur de la propriété codeApplicationCr.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeApplicationCr() {
        return this.codeApplicationCr;
    }

    /**
     * Définit la valeur de la propriété codeApplicationCr.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeApplicationCr(String value) {
        this.codeApplicationCr = value;
    }

    /**
     * Obtient la valeur de la propriété codeVentilationCr.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeVentilationCr() {
        return this.codeVentilationCr;
    }

    /**
     * Définit la valeur de la propriété codeVentilationCr.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeVentilationCr(String value) {
        this.codeVentilationCr = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersRemisParSe.
     *
     * @return the nb dossiers remis par se
     */
    public long getNbDossiersRemisParSe() {
        return this.nbDossiersRemisParSe;
    }

    /**
     * Définit la valeur de la propriété nbDossiersRemisParSe.
     *
     * @param value
     *            the new nb dossiers remis par se
     */
    public void setNbDossiersRemisParSe(long value) {
        this.nbDossiersRemisParSe = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersRecus.
     *
     * @return the nb dossiers recus
     */
    public long getNbDossiersRecus() {
        return this.nbDossiersRecus;
    }

    /**
     * Définit la valeur de la propriété nbDossiersRecus.
     *
     * @param value
     *            the new nb dossiers recus
     */
    public void setNbDossiersRecus(long value) {
        this.nbDossiersRecus = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersTraitesOk.
     *
     * @return the nb dossiers traites ok
     */
    public long getNbDossiersTraitesOk() {
        return this.nbDossiersTraitesOk;
    }

    /**
     * Définit la valeur de la propriété nbDossiersTraitesOk.
     *
     * @param value
     *            the new nb dossiers traites ok
     */
    public void setNbDossiersTraitesOk(long value) {
        this.nbDossiersTraitesOk = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersRejetes.
     *
     * @return the nb dossiers rejetes
     */
    public long getNbDossiersRejetes() {
        return this.nbDossiersRejetes;
    }

    /**
     * Définit la valeur de la propriété nbDossiersRejetes.
     *
     * @param value
     *            the new nb dossiers rejetes
     */
    public void setNbDossiersRejetes(long value) {
        this.nbDossiersRejetes = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersFiltres.
     *
     * @return the nb dossiers filtres
     */
    public long getNbDossiersFiltres() {
        return this.nbDossiersFiltres;
    }

    /**
     * Définit la valeur de la propriété nbDossiersFiltres.
     *
     * @param value
     *            the new nb dossiers filtres
     */
    public void setNbDossiersFiltres(long value) {
        this.nbDossiersFiltres = value;
    }

    /**
     * Obtient la valeur de la propriété nbDossiersCrees.
     *
     * @return the nb dossiers crees
     */
    public long getNbDossiersCrees() {
        return this.nbDossiersCrees;
    }

    /**
     * Définit la valeur de la propriété nbDossiersCrees.
     *
     * @param value
     *            the new nb dossiers crees
     */
    public void setNbDossiersCrees(long value) {
        this.nbDossiersCrees = value;
    }

    /**
     * Obtient la valeur de la propriété dateTime.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDateTime() {
        return this.dateTime;
    }

    /**
     * Définit la valeur de la propriété dateTime.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

}


package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour EJPartenaireEnteteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EJPartenaireEnteteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Site" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireType" minOccurs="0"/&gt;
 *         &lt;element name="TiersBeneficiaire" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="Demandeur" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireType" minOccurs="0"/&gt;
 *         &lt;element name="TitulaireCotraitant" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="TitCotraitMandat" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="SsTraitantadmisPD" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="SsTraitantNonPD" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="CoTraitant" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *         &lt;element name="CoTraitantMandataire" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}PartenaireTypeEntete" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EJPartenaireEnteteType", propOrder = { "site", "tiersBeneficiaire", "demandeur", "titulaireCotraitant", "titCotraitMandat",
        "ssTraitantadmisPD", "ssTraitantNonPD", "coTraitant", "coTraitantMandataire" })
public class EJPartenaireEnteteType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Site")
    private PartenaireType site;
    @XmlElement(name = "TiersBeneficiaire")
    private PartenaireTypeEntete tiersBeneficiaire;
    @XmlElement(name = "Demandeur")
    private PartenaireType demandeur;
    @XmlElement(name = "TitulaireCotraitant")
    private PartenaireTypeEntete titulaireCotraitant;
    @XmlElement(name = "TitCotraitMandat")
    private PartenaireTypeEntete titCotraitMandat;
    @XmlElement(name = "SsTraitantadmisPD")
    private PartenaireTypeEntete ssTraitantadmisPD;
    @XmlElement(name = "SsTraitantNonPD")
    private PartenaireTypeEntete ssTraitantNonPD;
    @XmlElement(name = "CoTraitant")
    private PartenaireTypeEntete coTraitant;
    @XmlElement(name = "CoTraitantMandataire")
    private PartenaireTypeEntete coTraitantMandataire;

    /**
     * Obtient la valeur de la propriété site.
     *
     * @return possible object is {@link PartenaireType }
     *
     */
    public PartenaireType getSite() {
        return this.site;
    }

    /**
     * Définit la valeur de la propriété site.
     *
     * @param value
     *            allowed object is {@link PartenaireType }
     *
     */
    public void setSite(PartenaireType value) {
        this.site = value;
    }

    /**
     * Obtient la valeur de la propriété tiersBeneficiaire.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getTiersBeneficiaire() {
        return this.tiersBeneficiaire;
    }

    /**
     * Définit la valeur de la propriété tiersBeneficiaire.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setTiersBeneficiaire(PartenaireTypeEntete value) {
        this.tiersBeneficiaire = value;
    }

    /**
     * Obtient la valeur de la propriété demandeur.
     *
     * @return possible object is {@link PartenaireType }
     *
     */
    public PartenaireType getDemandeur() {
        return this.demandeur;
    }

    /**
     * Définit la valeur de la propriété demandeur.
     *
     * @param value
     *            allowed object is {@link PartenaireType }
     *
     */
    public void setDemandeur(PartenaireType value) {
        this.demandeur = value;
    }

    /**
     * Obtient la valeur de la propriété titulaireCotraitant.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getTitulaireCotraitant() {
        return this.titulaireCotraitant;
    }

    /**
     * Définit la valeur de la propriété titulaireCotraitant.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setTitulaireCotraitant(PartenaireTypeEntete value) {
        this.titulaireCotraitant = value;
    }

    /**
     * Obtient la valeur de la propriété titCotraitMandat.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getTitCotraitMandat() {
        return this.titCotraitMandat;
    }

    /**
     * Définit la valeur de la propriété titCotraitMandat.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setTitCotraitMandat(PartenaireTypeEntete value) {
        this.titCotraitMandat = value;
    }

    /**
     * Obtient la valeur de la propriété ssTraitantadmisPD.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getSsTraitantadmisPD() {
        return this.ssTraitantadmisPD;
    }

    /**
     * Définit la valeur de la propriété ssTraitantadmisPD.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setSsTraitantadmisPD(PartenaireTypeEntete value) {
        this.ssTraitantadmisPD = value;
    }

    /**
     * Obtient la valeur de la propriété ssTraitantNonPD.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getSsTraitantNonPD() {
        return this.ssTraitantNonPD;
    }

    /**
     * Définit la valeur de la propriété ssTraitantNonPD.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setSsTraitantNonPD(PartenaireTypeEntete value) {
        this.ssTraitantNonPD = value;
    }

    /**
     * Obtient la valeur de la propriété coTraitant.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getCoTraitant() {
        return this.coTraitant;
    }

    /**
     * Définit la valeur de la propriété coTraitant.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setCoTraitant(PartenaireTypeEntete value) {
        this.coTraitant = value;
    }

    /**
     * Obtient la valeur de la propriété coTraitantMandataire.
     *
     * @return possible object is {@link PartenaireTypeEntete }
     *
     */
    public PartenaireTypeEntete getCoTraitantMandataire() {
        return this.coTraitantMandataire;
    }

    /**
     * Définit la valeur de la propriété coTraitantMandataire.
     *
     * @param value
     *            allowed object is {@link PartenaireTypeEntete }
     *
     */
    public void setCoTraitantMandataire(PartenaireTypeEntete value) {
        this.coTraitantMandataire = value;
    }

}

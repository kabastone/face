
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour TypeEJType.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeEJType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ZSUB"/&gt;
 *     &lt;enumeration value="ZMBC"/&gt;
 *     &lt;enumeration value="ZMPC"/&gt;
 *     &lt;enumeration value="ZMPT"/&gt;
 *     &lt;enumeration value="ZMPU"/&gt;
 *     &lt;enumeration value="ZMPX"/&gt;
 *     &lt;enumeration value="ZMT"/&gt;
 *     &lt;enumeration value="ZMU"/&gt;
 *     &lt;enumeration value="ZMX"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeEJType")
@XmlEnum
public enum TypeEJType {


    /**
     * Subventions
     * 
     */
    ZSUB,

    /**
     * Marché à BDC
     * 
     */
    ZMBC,

    /**
     * MAPA à BDC
     * 
     */
    ZMPC,

    /**
     * MAPA à tranches
     * 
     */
    ZMPT,

    /**
     * MAPA unique
     * 
     */
    ZMPU,

    /**
     * MAPA mixte
     * 
     */
    ZMPX,

    /**
     * Marché à tranches
     * 
     */
    ZMT,

    /**
     * Marché unique
     * 
     */
    ZMU,

    /**
     * Marché mixte
     * 
     */
    ZMX;

    public String value() {
        return name();
    }

    public static TypeEJType fromValue(String v) {
        return valueOf(v);
    }

}

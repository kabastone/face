/**
 * Définition des namespaces pour fen0159A.
 */
@javax.xml.bind.annotation.XmlSchema(elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = { @javax.xml.bind.annotation.XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance") })
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051aej;

import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Class EngagementJuridiqueChorus.
 *
 * @author Atos
 */
public class EngagementJuridiqueChorus {

    /** The demande subvention poste. */
    private DemandeSubvention demandeSubventionPoste;

    /** The dossier subvention. */
    private DossierSubvention dossierSubvention;

    /** The demande prolongation. */
    private DemandeProlongation demandeProlongation;

    /**
     * Gets the demande subvention poste.
     *
     * @return the demandeSubventionPoste
     */
    public DemandeSubvention getDemandeSubventionPoste() {
        return this.demandeSubventionPoste;
    }

    /**
     * Sets the demande subvention poste.
     *
     * @param demandeSubventionPoste            the demandeSubventionPoste to set
     */
    public void setDemandeSubventionPoste(DemandeSubvention demandeSubventionPoste) {
        this.demandeSubventionPoste = demandeSubventionPoste;
    }

    /**
     * Gets the dossier subvention.
     *
     * @return the dossierSubvention
     */
    public DossierSubvention getDossierSubvention() {
        return this.dossierSubvention;
    }

    /**
     * Sets the dossier subvention.
     *
     * @param dossierSubvention            the dossierSubvention to set
     */
    public void setDossierSubvention(DossierSubvention dossierSubvention) {
        this.dossierSubvention = dossierSubvention;
    }

    /**
     * Gets the demande prolongation.
     *
     * @return the demandeProlongation
     */
    public DemandeProlongation getDemandeProlongation() {
        return this.demandeProlongation;
    }

    /**
     * Sets the demande prolongation.
     *
     * @param demandeProlongation            the demandeProlongation to set
     */
    public void setDemandeProlongation(DemandeProlongation demandeProlongation) {
        this.demandeProlongation = demandeProlongation;
    }

}


package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour StatutType.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 *
 * <pre>
 * &lt;simpleType name="StatutType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Sauvegardé"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "StatutType")
@XmlEnum
public enum StatutType {

    @XmlEnumValue("Sauvegardé")
    SAUVEGARDE("Sauvegardé");
    private final String value;

    StatutType(String v) {
        this.value = v;
    }

    public String value() {
        return this.value;
    }

    public static StatutType fromValue(String v) {
        for (StatutType c : StatutType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

package fr.gouv.sitde.face.batch.job.fso0051adp;

import java.io.File;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

import fr.gouv.sitde.face.batch.commun.AiguillageTasklet;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.commun.ResourcesUtils;
import fr.gouv.sitde.face.batch.commun.SuppressionFichiersTasklet;
import fr.gouv.sitde.face.batch.commun.SupressionDossierTasklet;
import fr.gouv.sitde.face.batch.commun.batch51a.AlimentationDossierTaslklet;
import fr.gouv.sitde.face.batch.commun.batch51a.MiseAJourCompteurTasklet;
import fr.gouv.sitde.face.batch.commun.batch51a.model.Batch51AEnum;
import fr.gouv.sitde.face.batch.commun.batch51a.model.SuiviBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Class Fso0051ADPConfiguration.
 *
 * @author Atos
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class Fso0051ADPConfiguration {

    /** The Constant FSO0051A_DP_REGEXP. */
    private static final String FSO0051A_DP_REGEXP = "^FSO0051A_[a-zA-Z0-9_]+EDP\\w+";

    /** The Constant SOUS_REP_INPUT_FSO0051ADP. */
    public static final String SOUS_REP_INPUT_FSO0051ADP = "/fso0051adp";

    /** The sous dossier travail. */
    private static final String SOUS_DOSSIER_TRAVAIL = File.separator + "jobBatch51aDP";

    /** The Constant UN. */
    public static final int UN = 1;

    /** The Constant ENO_SEQ_INDEX. */
    public static final int ENO_SEQ_INDEX = 2 - UN;

    /** The Constant AC_DOC_NO_INDEX. */
    public static final int AC_DOC_NO_INDEX = 5 - UN;

    /** The Constant OI_EBELN_INDEX. */
    public static final int OI_EBELN_INDEX = 12 - UN;

    /** The Constant OI_EBELP_INDEX. */
    public static final int OI_EBELP_INDEX = 13 - UN;

    /** The Constant REF_KEY3_INDEX. */
    public static final int REF_KEY3_INDEX = 18 - UN;

    /** The Constant PSTNG_DATE_INDEX. */
    public static final int PSTNG_DATE_INDEX = 22 - UN;

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The chorus suivi batch service. */
    @Inject
    private ChorusSuiviBatchService chorusSuiviBatchService;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /** The input global. */
    private File inputGlobal;

    /** The input file. */
    private File inputFile;

    /** The work file. */
    private File workFile;

    /** The chorus suivi batch. */
    private SuiviBatch suiviBatch;

    /**
     * Gets the ressources from work directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromInputDirectory() {
        File repertoireWorkJob = this.getInputFile();
        return ResourcesUtils.getRessourcesFromDirectory(repertoireWorkJob);
    }

    /**
     * Gets the ressources from input directory.
     *
     * @return the ressources from input directory
     */
    private Resource[] getRessourcesFromWorkDirectory() {
        File repertoireWorkJob = this.getWorkFile();
        return ResourcesUtils.getRessourcesFromDirectory(repertoireWorkJob);
    }

    public File getInputGlobal() {
        if (this.inputGlobal == null) {
            this.inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        }
        return this.inputGlobal;
    }

    /**
     * Gets the input file.
     *
     * @return the input file
     */
    public File getInputFile() {
        if (this.inputFile == null) {
            this.inputFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0051ADP);
        }
        return this.inputFile;
    }

    /**
     * Gets the work file.
     *
     * @return the work file
     */
    public File getWorkFile() {
        if (this.workFile == null) {
            this.workFile = new File(this.batchCheminRepertoireProperties.getInput() + SOUS_REP_INPUT_FSO0051ADP + SOUS_DOSSIER_TRAVAIL);
        }
        return this.workFile;
    }

    /**
     * Creates the line tokenizer.
     *
     * @return the line tokenizer
     */
    private static LineTokenizer createLineTokenizer() {
        DelimitedLineTokenizer dossierLineTokenizer = new DelimitedLineTokenizer();
        dossierLineTokenizer.setDelimiter("|");
        // ENO_SEQ | 0AC_DOC_NO | 0OI_EBELN | 0OI_EBELP | REF_KEY3 | 0PSTNG_DATE
        // numc(20) | char(10) | char(10) | numc(5) | char(20) | DATS(ddMMyyyy)
        dossierLineTokenizer.setNames("numeroSequence", "numeroDemandePaiement", "numeroEngagementJuridique", "posteEJ", "numeroServiceFait",
                "dateComptable");
        dossierLineTokenizer.setIncludedFields(ENO_SEQ_INDEX, AC_DOC_NO_INDEX, OI_EBELN_INDEX, OI_EBELP_INDEX, REF_KEY3_INDEX, PSTNG_DATE_INDEX);
        return dossierLineTokenizer;
    }

    /**
     * Creates the demande paiement line mapper.
     *
     * @return the field set mapper
     */
    private static FieldSetMapper<DemandePaiementChorusDto> createDemandePaiementLineMapper() {
        BeanWrapperFieldSetMapper<DemandePaiementChorusDto> dossierInformationMapper = new BeanWrapperFieldSetMapper<>();
        dossierInformationMapper.setTargetType(DemandePaiementChorusDto.class);
        return dossierInformationMapper;
    }

    /**
     * Creates the demande paiement chorus dto line mapper.
     *
     * @return the line mapper
     */
    private static LineMapper<DemandePaiementChorusDto> createDemandePaiementChorusDtoLineMapper() {
        DefaultLineMapper<DemandePaiementChorusDto> dossierLineMapper = new DefaultLineMapper<>();

        LineTokenizer dossierLineTokenizer = createLineTokenizer();
        dossierLineMapper.setLineTokenizer(dossierLineTokenizer);

        FieldSetMapper<DemandePaiementChorusDto> dossierInformationMapper = createDemandePaiementLineMapper();
        dossierLineMapper.setFieldSetMapper(dossierInformationMapper);

        return dossierLineMapper;
    }

    /**
     * Demande paiment FSO 0051 ADP reader.
     *
     * @return the flat file item reader
     */
    @Bean
    @StepScope
    public FlatFileItemReader<DemandePaiementChorusDto> demandePaimentFSO0051ADPReader() {
        FlatFileItemReader<DemandePaiementChorusDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setLinesToSkip(0);

        LineMapper<DemandePaiementChorusDto> dossierLineMapper = createDemandePaiementChorusDtoLineMapper();
        flatFileItemReader.setLineMapper(dossierLineMapper);
        return flatFileItemReader;

    }

    /**
     * Multi resource FSO 0051 ADP reader.
     *
     * @return the multi resource item reader
     */
    @Bean
    @StepScope
    public MultiResourceItemReader<DemandePaiementChorusDto> multiResourceFSO0051ADPReader() {
        Resource[] tableauResources = this.getRessourcesFromWorkDirectory();
        MultiResourceItemReader<DemandePaiementChorusDto> resourceItemReader = new MultiResourceItemReader<>();
        resourceItemReader.setResources(tableauResources);
        // Pour chaque fichier, on délègue la lecture au dossierFlatFileReader
        resourceItemReader.setDelegate(this.demandePaimentFSO0051ADPReader());
        return resourceItemReader;
    }

    /**
     * Step aiguillage FSO 0051 ADP.
     *
     * Step 0 : on va chercher les fichiers propres au batch dans le repertoire global
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAiguillageFSO0051ADP() {
        AiguillageTasklet tasklet = new AiguillageTasklet();
        tasklet.setInputDir(this.getInputGlobal());
        tasklet.setOutputDir(this.getInputFile());
        tasklet.setRegexp(FSO0051A_DP_REGEXP);

        return this.stepBuilderFactory.get("step_aiguillage").tasklet(tasklet).build();
    }

    /**
     * Step alimentation dossier.
     *
     * Step 1 : on recherche les dossiers intégrables on les déplace dans le dossier de travail Le compteur ChorusSuiviBatch est mis a jour
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepAlimentationDossierFSO0051ADP() {
        this.suiviBatch = new SuiviBatch();
        this.suiviBatch.setInstance(Batch51AEnum.BATCH_DP);

        AlimentationDossierTaslklet tasklet = new AlimentationDossierTaslklet();

        tasklet.setInputDirectory(this.getInputFile());
        tasklet.setTravailDirectory(this.getWorkFile());
        ChorusSuiviBatch chorusSuiviBatch = this.chorusSuiviBatchService.lireSuiviBatch();

        this.suiviBatch.setNumeroIntegration(chorusSuiviBatch.getFso0051aDp());

        tasklet.setSuiviBatch(this.suiviBatch);

        return this.stepBuilderFactory.get("step_alimentation_dossier").tasklet(tasklet).build();
    }

    /**
     * Step Demande Paiement.
     *
     * Step 2 : lecture et traitement des lignes : read, process, write
     *
     * <p>
     * <b>Ce step définit des chunks de 1</b> (commit/rollback pour chaque item traité par le writer).<br/>
     * <br/>
     * <b>Une exception n'est pas bloquante :</b>
     * <ul>
     * <li>Un rollback est effectué si l'exception a été lancée par le writer</li>
     * <li>Le traitement continue pour le prochain item.</li>
     * </ul>
     * </p>
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepDemandePaiementFSO0051ADP() {
        return this.stepBuilderFactory.get("step_service_fait").<DemandePaiementChorusDto, DemandePaiementChorus> chunk(1)
                .reader(this.multiResourceFSO0051ADPReader()).processor(this.demandePaiementProcessorFSO0051ADP())
                .writer(this.demandePaiementWriterFSO0051ADP()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Step maj compteur.
     *
     * Step 3 : une fois les fichiers traités on met a jour ChorusSuiviBatch dans la base
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepMajCompteurFSO0051ADP() {
        MiseAJourCompteurTasklet tasklet = new MiseAJourCompteurTasklet();
        tasklet.setSuiviBatch(this.suiviBatch);
        tasklet.setChorusSuiviBatchService(this.chorusSuiviBatchService);
        return this.stepBuilderFactory.get("step_maj_compteur").tasklet(tasklet).build();
    }

    /**
     * Demande paiement processor FSO 0051 ADP.
     *
     * @return the demande paiement processor
     */
    @Bean
    @StepScope
    public DemandePaiementProcessor demandePaiementProcessorFSO0051ADP() {
        return new DemandePaiementProcessor();

    }

    /**
     * Demande paiement writer FSO 0051 ADP.
     *
     * @return the demande paiement writer
     */
    @Bean
    @StepScope
    public DemandePaiementWriter demandePaiementWriterFSO0051ADP() {
        return new DemandePaiementWriter();
    }

    /**
     * Step supression dossier working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSupressionDossierWorkFSO0051ADP() {
        SupressionDossierTasklet tasklet = new SupressionDossierTasklet();
        tasklet.setDirectoryPath(this.getWorkFile().toPath());
        return this.stepBuilderFactory.get("step_supression_dossier_work").tasklet(tasklet).build();
    }

    /**
     * Step suppression fichiers working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersWorkFSO0051ADP() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromWorkDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers_work").tasklet(tasklet).build();
    }

    /**
     * Step suppression fichiers working dir.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepSuppressionFichiersInputFSO0051ADP() {
        SuppressionFichiersTasklet tasklet = new SuppressionFichiersTasklet();
        tasklet.setResources(this.getRessourcesFromInputDirectory());
        return this.stepBuilderFactory.get("step_suppression_fichiers_input").tasklet(tasklet).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FSO0051A_DP_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FSO0051A_DP_JOB.getNom()).start(this.stepAiguillageFSO0051ADP())
                .next(this.stepAlimentationDossierFSO0051ADP()).next(this.stepDemandePaiementFSO0051ADP())
                .next(this.stepSuppressionFichiersWorkFSO0051ADP()).next(this.stepMajCompteurFSO0051ADP())
                .next(this.stepSupressionDossierWorkFSO0051ADP()).next(this.stepSuppressionFichiersInputFSO0051ADP()).build();
    }
}

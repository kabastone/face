
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.sitde.face.batch.chorusmodel.fso0028a package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.sitde.face.batch.chorusmodel.fso0028a
     *
     */
    public ObjectFactory() {
        // constructeur non parametré.
    }

    /**
     * Create an instance of {@link FSO0028A }
     *
     */
    public FSO0028A createFSO0028A() {
        return new FSO0028A();
    }

    /**
     * Create an instance of {@link EngagementJuridiqueFSO0028A }
     *
     */
    public EngagementJuridiqueFSO0028A createEngagementJuridiqueFSO0028A() {
        return new EngagementJuridiqueFSO0028A();
    }

    /**
     * Create an instance of {@link EnteteFSO0028A }
     *
     */
    public EnteteFSO0028A createEnteteFSO0028A() {
        return new EnteteFSO0028A();
    }

    /**
     * Create an instance of {@link ImputationFSO0028A }
     *
     */
    public ImputationFSO0028A createImputationFSO0028A() {
        return new ImputationFSO0028A();
    }

    /**
     * Create an instance of {@link LignePosteFSO0028A }
     *
     */
    public LignePosteFSO0028A createLignePosteFSO0028A() {
        return new LignePosteFSO0028A();
    }

    /**
     * Create an instance of {@link PartenaireFSO0028A }
     *
     */
    public PartenaireFSO0028A createPartenaireFSO0028A() {
        return new PartenaireFSO0028A();
    }

    /**
     * Create an instance of {@link TexteFSO0028A }
     *
     */
    public TexteFSO0028A createTexteFSO0028A() {
        return new TexteFSO0028A();
    }

}

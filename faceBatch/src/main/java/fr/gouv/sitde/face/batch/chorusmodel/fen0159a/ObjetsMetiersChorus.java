
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Identification des objets metiers CHORUS a rattacher a la PJ
 *
 * <p>
 * Classe Java pour ObjetsMetiersChorusType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ObjetsMetiersChorusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DA" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}DAType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EJ" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}EJType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ServiceFait" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ServiceFaitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Avance" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}AvanceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DpSurEj" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}DpSurEjType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DPDirecte" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}DPDirecteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Budget" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}BudgetType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReservationCredit" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ReservationCreditType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheArticle" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheArticleType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OD" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ODType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FactureAvecET" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FactureAvecETType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FactureSansET" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FactureSansETType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RecettesAuComptant" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}RecettesAuComptantType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheFournisseurDonneesGen" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheFournisseurDonneesGenType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheFournisseurSociete" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheFournisseurSocieteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheFournisseurOrgAchat" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheFournisseurOrgAchatType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheClient" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheClientType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PiecePSCD" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}PiecePSCDType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Partenaire" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}PartenaireType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FicheImmobilisation" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}FicheImmobilisationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DefinitionProjet" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}DefinitionProjetType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ElementOTP" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ElementOTPType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ObjetArchitecture" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ObjetArchitectureType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UniteEconomique" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}UniteEconomiqueType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Batiment" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}BatimentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Terrain" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}TerrainType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ObjetLoue" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ObjetLoueType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ContratImmobilier" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}ContratImmobilierType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjetsMetiersChorusType",
        propOrder = { "das", "ejs", "serviceFait", "avances", "dpSurEjs", "dpDirectes", "budgets", "reservationCredits", "ficheArticles", "ods",
                "factureAvecETs", "factureSansETs", "recettesAuComptants", "ficheFournisseurDonneesGens", "ficheFournisseurSocietes",
                "ficheFournisseurOrgAchats", "ficheClients", "piecePSCDs", "partenaires", "ficheImmobilisations", "definitionProjets", "elementOTPs",
                "objetArchitectures", "uniteEconomiques", "batiments", "terrains", "objetLoues", "contratImmobiliers" })
public class ObjetsMetiersChorus implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "DA")
    private List<DaType> das;
    @XmlElement(name = "EJ")
    private List<EjType> ejs;
    @XmlElement(name = "ServiceFait")
    private List<ServiceFait> serviceFait;
    @XmlElement(name = "Avance")
    private List<AvanceType> avances;
    @XmlElement(name = "DpSurEj")
    private List<DpSurEjType> dpSurEjs;
    @XmlElement(name = "DPDirecte")
    private List<DpDirecteType> dpDirectes;
    @XmlElement(name = "Budget")
    private List<BudgetType> budgets;
    @XmlElement(name = "ReservationCredit")
    private List<ReservationCreditType> reservationCredits;
    @XmlElement(name = "FicheArticle")
    private List<FicheArticleType> ficheArticles;
    @XmlElement(name = "OD")
    private List<OdType> ods;
    @XmlElement(name = "FactureAvecET")
    private List<FactureAvecEtType> factureAvecETs;
    @XmlElement(name = "FactureSansET")
    private List<FactureSansEtType> factureSansETs;
    @XmlElement(name = "RecettesAuComptant")
    private List<RecettesAuComptantType> recettesAuComptants;
    @XmlElement(name = "FicheFournisseurDonneesGen")
    private List<FicheFournisseurDonneesGenType> ficheFournisseurDonneesGens;
    @XmlElement(name = "FicheFournisseurSociete")
    private List<FicheFournisseurSocieteType> ficheFournisseurSocietes;
    @XmlElement(name = "FicheFournisseurOrgAchat")
    private List<FicheFournisseurOrgAchatType> ficheFournisseurOrgAchats;
    @XmlElement(name = "FicheClient")
    private List<FicheClientType> ficheClients;
    @XmlElement(name = "PiecePSCD")
    private List<PiecePSCDType> piecePSCDs;
    @XmlElement(name = "Partenaire")
    private List<PartenaireType> partenaires;
    @XmlElement(name = "FicheImmobilisation")
    private List<FicheImmobilisationType> ficheImmobilisations;
    @XmlElement(name = "DefinitionProjet")
    private List<DefinitionProjetType> definitionProjets;
    @XmlElement(name = "ElementOTP")
    private List<ElementOTPType> elementOTPs;
    @XmlElement(name = "ObjetArchitecture")
    private List<ObjetArchitectureType> objetArchitectures;
    @XmlElement(name = "UniteEconomique")
    private List<UniteEconomiqueType> uniteEconomiques;
    @XmlElement(name = "Batiment")
    private List<BatimentType> batiments;
    @XmlElement(name = "Terrain")
    private List<TerrainType> terrains;
    @XmlElement(name = "ObjetLoue")
    private List<ObjetLoueType> objetLoues;
    @XmlElement(name = "ContratImmobilier")
    private List<ContratImmobilierType> contratImmobiliers;

    /**
     * Gets the value of the serviceFait property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the serviceFait property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getServiceFait().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ServiceFait }
     *
     *
     */
    public List<ServiceFait> getServiceFait() {
        if (this.serviceFait == null) {
            this.serviceFait = new ArrayList<>();
        }
        return this.serviceFait;
    }

    /**
     * Gets the value of the avances property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the avances property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getAvances().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link AvanceType }
     *
     *
     */
    public List<AvanceType> getAvances() {
        if (this.avances == null) {
            this.avances = new ArrayList<>();
        }
        return this.avances;
    }

    /**
     * Gets the value of the dpSurEjs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the dpSurEjs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getDpSurEjs().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link DpSurEjType }
     *
     *
     */
    public List<DpSurEjType> getDpSurEjs() {
        if (this.dpSurEjs == null) {
            this.dpSurEjs = new ArrayList<>();
        }
        return this.dpSurEjs;
    }

    /**
     * Gets the value of the budgets property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the budgets property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getBudgets().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link BudgetType }
     *
     *
     */
    public List<BudgetType> getBudgets() {
        if (this.budgets == null) {
            this.budgets = new ArrayList<>();
        }
        return this.budgets;
    }

    /**
     * Gets the value of the reservationCredits property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the reservationCredits property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getReservationCredits().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ReservationCreditType }
     *
     *
     */
    public List<ReservationCreditType> getReservationCredits() {
        if (this.reservationCredits == null) {
            this.reservationCredits = new ArrayList<>();
        }
        return this.reservationCredits;
    }

    /**
     * Gets the value of the ficheArticles property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheArticles property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheArticles().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheArticleType }
     *
     *
     */
    public List<FicheArticleType> getFicheArticles() {
        if (this.ficheArticles == null) {
            this.ficheArticles = new ArrayList<>();
        }
        return this.ficheArticles;
    }

    /**
     * Gets the value of the factureAvecETs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the factureAvecETs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFactureAvecETs().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FactureAvecETType }
     *
     *
     */
    public List<FactureAvecEtType> getFactureAvecETs() {
        if (this.factureAvecETs == null) {
            this.factureAvecETs = new ArrayList<>();
        }
        return this.factureAvecETs;
    }

    /**
     * Gets the value of the factureSansETs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the factureSansETs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFactureSansETs().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FactureSansETType }
     *
     *
     */
    public List<FactureSansEtType> getFactureSansETs() {
        if (this.factureSansETs == null) {
            this.factureSansETs = new ArrayList<>();
        }
        return this.factureSansETs;
    }

    /**
     * Gets the value of the recettesAuComptants property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the recettesAuComptants property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getRecettesAuComptants().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link RecettesAuComptantType }
     *
     *
     */
    public List<RecettesAuComptantType> getRecettesAuComptants() {
        if (this.recettesAuComptants == null) {
            this.recettesAuComptants = new ArrayList<>();
        }
        return this.recettesAuComptants;
    }

    /**
     * Gets the value of the ficheFournisseurDonneesGens property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheFournisseurDonneesGens property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheFournisseurDonneesGens().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheFournisseurDonneesGenType }
     *
     *
     */
    public List<FicheFournisseurDonneesGenType> getFicheFournisseurDonneesGens() {
        if (this.ficheFournisseurDonneesGens == null) {
            this.ficheFournisseurDonneesGens = new ArrayList<>();
        }
        return this.ficheFournisseurDonneesGens;
    }

    /**
     * Gets the value of the ficheFournisseurSocietes property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheFournisseurSocietes property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheFournisseurSocietes().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheFournisseurSocieteType }
     *
     *
     */
    public List<FicheFournisseurSocieteType> getFicheFournisseurSocietes() {
        if (this.ficheFournisseurSocietes == null) {
            this.ficheFournisseurSocietes = new ArrayList<>();
        }
        return this.ficheFournisseurSocietes;
    }

    /**
     * Gets the value of the ficheFournisseurOrgAchats property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheFournisseurOrgAchats property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheFournisseurOrgAchats().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheFournisseurOrgAchatType }
     *
     *
     */
    public List<FicheFournisseurOrgAchatType> getFicheFournisseurOrgAchats() {
        if (this.ficheFournisseurOrgAchats == null) {
            this.ficheFournisseurOrgAchats = new ArrayList<>();
        }
        return this.ficheFournisseurOrgAchats;
    }

    /**
     * Gets the value of the ficheClients property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheClients property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheClients().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheClientType }
     *
     *
     */
    public List<FicheClientType> getFicheClients() {
        if (this.ficheClients == null) {
            this.ficheClients = new ArrayList<>();
        }
        return this.ficheClients;
    }

    /**
     * Gets the value of the piecePSCDs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the piecePSCDs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPiecePSCDs().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link PiecePSCDType }
     *
     *
     */
    public List<PiecePSCDType> getPiecePSCDs() {
        if (this.piecePSCDs == null) {
            this.piecePSCDs = new ArrayList<>();
        }
        return this.piecePSCDs;
    }

    /**
     * Gets the value of the partenaires property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the partenaires property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPartenaires().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link PartenaireType }
     *
     *
     */
    public List<PartenaireType> getPartenaires() {
        if (this.partenaires == null) {
            this.partenaires = new ArrayList<>();
        }
        return this.partenaires;
    }

    /**
     * Gets the value of the ficheImmobilisations property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the ficheImmobilisations property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getFicheImmobilisations().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FicheImmobilisationType }
     *
     *
     */
    public List<FicheImmobilisationType> getFicheImmobilisations() {
        if (this.ficheImmobilisations == null) {
            this.ficheImmobilisations = new ArrayList<>();
        }
        return this.ficheImmobilisations;
    }

    /**
     * Gets the value of the definitionProjets property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the definitionProjets property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getDefinitionProjets().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link DefinitionProjetType }
     *
     *
     */
    public List<DefinitionProjetType> getDefinitionProjets() {
        if (this.definitionProjets == null) {
            this.definitionProjets = new ArrayList<>();
        }
        return this.definitionProjets;
    }

    /**
     * Gets the value of the elementOTPs property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the elementOTPs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getElementOTPs().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ElementOTPType }
     *
     *
     */
    public List<ElementOTPType> getElementOTPs() {
        if (this.elementOTPs == null) {
            this.elementOTPs = new ArrayList<>();
        }
        return this.elementOTPs;
    }

    /**
     * Gets the value of the objetArchitectures property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the objetArchitectures property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getObjetArchitectures().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ObjetArchitectureType }
     *
     *
     */
    public List<ObjetArchitectureType> getObjetArchitectures() {
        if (this.objetArchitectures == null) {
            this.objetArchitectures = new ArrayList<>();
        }
        return this.objetArchitectures;
    }

    /**
     * Gets the value of the uniteEconomiques property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the uniteEconomiques property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getUniteEconomiques().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link UniteEconomiqueType }
     *
     *
     */
    public List<UniteEconomiqueType> getUniteEconomiques() {
        if (this.uniteEconomiques == null) {
            this.uniteEconomiques = new ArrayList<>();
        }
        return this.uniteEconomiques;
    }

    /**
     * Gets the value of the batiments property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the batiments property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getBatiments().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link BatimentType }
     *
     *
     */
    public List<BatimentType> getBatiments() {
        if (this.batiments == null) {
            this.batiments = new ArrayList<>();
        }
        return this.batiments;
    }

    /**
     * Gets the value of the terrains property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the terrains property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getTerrains().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link TerrainType }
     *
     *
     */
    public List<TerrainType> getTerrains() {
        if (this.terrains == null) {
            this.terrains = new ArrayList<>();
        }
        return this.terrains;
    }

    /**
     * Gets the value of the objetLoues property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the objetLoues property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getObjetLoues().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ObjetLoueType }
     *
     *
     */
    public List<ObjetLoueType> getObjetLoues() {
        if (this.objetLoues == null) {
            this.objetLoues = new ArrayList<>();
        }
        return this.objetLoues;
    }

    /**
     * Gets the value of the contratImmobiliers property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the contratImmobiliers property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getContratImmobiliers().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ContratImmobilierType }
     *
     *
     */
    public List<ContratImmobilierType> getContratImmobiliers() {
        if (this.contratImmobiliers == null) {
            this.contratImmobiliers = new ArrayList<>();
        }
        return this.contratImmobiliers;
    }

    /**
     * @return the das
     */
    public List<DaType> getDas() {
        if (this.das == null) {
            this.das = new ArrayList<>();
        }
        return this.das;
    }

    /**
     * @param das
     *            the das to set
     */
    public void setDas(List<DaType> das) {
        this.das = das;
    }

    /**
     * @return the ejs
     */
    public List<EjType> getEjs() {
        if (this.ejs == null) {
            this.ejs = new ArrayList<>();
        }
        return this.ejs;
    }

    /**
     * @param ejs
     *            the ejs to set
     */
    public void setEjs(List<EjType> ejs) {
        this.ejs = ejs;
    }

    /**
     * @return the dpDirectes
     */
    public List<DpDirecteType> getDpDirectes() {
        if (this.dpDirectes == null) {
            this.dpDirectes = new ArrayList<>();
        }
        return this.dpDirectes;
    }

    /**
     * @param dpDirectes
     *            the dpDirectes to set
     */
    public void setDpDirectes(List<DpDirecteType> dpDirectes) {
        this.dpDirectes = dpDirectes;
    }

    /**
     * @return the ods
     */
    public List<OdType> getOds() {
        if (this.ods == null) {
            this.ods = new ArrayList<>();
        }
        return this.ods;
    }

    /**
     * @param ods
     *            the ods to set
     */
    public void setOds(List<OdType> ods) {
        this.ods = ods;
    }

    /**
     * @param serviceFait
     *            the serviceFait to set
     */
    public void setServiceFait(List<ServiceFait> serviceFait) {
        this.serviceFait = serviceFait;
    }

    /**
     * @param avances
     *            the avances to set
     */
    public void setAvances(List<AvanceType> avances) {
        this.avances = avances;
    }

    /**
     * @param dpSurEjs
     *            the dpSurEjs to set
     */
    public void setDpSurEjs(List<DpSurEjType> dpSurEjs) {
        this.dpSurEjs = dpSurEjs;
    }

    /**
     * @param budgets
     *            the budgets to set
     */
    public void setBudgets(List<BudgetType> budgets) {
        this.budgets = budgets;
    }

    /**
     * @param reservationCredits
     *            the reservationCredits to set
     */
    public void setReservationCredits(List<ReservationCreditType> reservationCredits) {
        this.reservationCredits = reservationCredits;
    }

    /**
     * @param ficheArticles
     *            the ficheArticles to set
     */
    public void setFicheArticles(List<FicheArticleType> ficheArticles) {
        this.ficheArticles = ficheArticles;
    }

    /**
     * @param factureAvecETs
     *            the factureAvecETs to set
     */
    public void setFactureAvecETs(List<FactureAvecEtType> factureAvecETs) {
        this.factureAvecETs = factureAvecETs;
    }

    /**
     * @param factureSansETs
     *            the factureSansETs to set
     */
    public void setFactureSansETs(List<FactureSansEtType> factureSansETs) {
        this.factureSansETs = factureSansETs;
    }

    /**
     * @param recettesAuComptants
     *            the recettesAuComptants to set
     */
    public void setRecettesAuComptants(List<RecettesAuComptantType> recettesAuComptants) {
        this.recettesAuComptants = recettesAuComptants;
    }

    /**
     * @param ficheFournisseurDonneesGens
     *            the ficheFournisseurDonneesGens to set
     */
    public void setFicheFournisseurDonneesGens(List<FicheFournisseurDonneesGenType> ficheFournisseurDonneesGens) {
        this.ficheFournisseurDonneesGens = ficheFournisseurDonneesGens;
    }

    /**
     * @param ficheFournisseurSocietes
     *            the ficheFournisseurSocietes to set
     */
    public void setFicheFournisseurSocietes(List<FicheFournisseurSocieteType> ficheFournisseurSocietes) {
        this.ficheFournisseurSocietes = ficheFournisseurSocietes;
    }

    /**
     * @param ficheFournisseurOrgAchats
     *            the ficheFournisseurOrgAchats to set
     */
    public void setFicheFournisseurOrgAchats(List<FicheFournisseurOrgAchatType> ficheFournisseurOrgAchats) {
        this.ficheFournisseurOrgAchats = ficheFournisseurOrgAchats;
    }

    /**
     * @param ficheClients
     *            the ficheClients to set
     */
    public void setFicheClients(List<FicheClientType> ficheClients) {
        this.ficheClients = ficheClients;
    }

    /**
     * @param piecePSCDs
     *            the piecePSCDs to set
     */
    public void setPiecePSCDs(List<PiecePSCDType> piecePSCDs) {
        this.piecePSCDs = piecePSCDs;
    }

    /**
     * @param partenaires
     *            the partenaires to set
     */
    public void setPartenaires(List<PartenaireType> partenaires) {
        this.partenaires = partenaires;
    }

    /**
     * @param ficheImmobilisations
     *            the ficheImmobilisations to set
     */
    public void setFicheImmobilisations(List<FicheImmobilisationType> ficheImmobilisations) {
        this.ficheImmobilisations = ficheImmobilisations;
    }

    /**
     * @param definitionProjets
     *            the definitionProjets to set
     */
    public void setDefinitionProjets(List<DefinitionProjetType> definitionProjets) {
        this.definitionProjets = definitionProjets;
    }

    /**
     * @param elementOTPs
     *            the elementOTPs to set
     */
    public void setElementOTPs(List<ElementOTPType> elementOTPs) {
        this.elementOTPs = elementOTPs;
    }

    /**
     * @param objetArchitectures
     *            the objetArchitectures to set
     */
    public void setObjetArchitectures(List<ObjetArchitectureType> objetArchitectures) {
        this.objetArchitectures = objetArchitectures;
    }

    /**
     * @param uniteEconomiques
     *            the uniteEconomiques to set
     */
    public void setUniteEconomiques(List<UniteEconomiqueType> uniteEconomiques) {
        this.uniteEconomiques = uniteEconomiques;
    }

    /**
     * @param batiments
     *            the batiments to set
     */
    public void setBatiments(List<BatimentType> batiments) {
        this.batiments = batiments;
    }

    /**
     * @param terrains
     *            the terrains to set
     */
    public void setTerrains(List<TerrainType> terrains) {
        this.terrains = terrains;
    }

    /**
     * @param objetLoues
     *            the objetLoues to set
     */
    public void setObjetLoues(List<ObjetLoueType> objetLoues) {
        this.objetLoues = objetLoues;
    }

    /**
     * @param contratImmobiliers
     *            the contratImmobiliers to set
     */
    public void setContratImmobiliers(List<ContratImmobilierType> contratImmobiliers) {
        this.contratImmobiliers = contratImmobiliers;
    }

}

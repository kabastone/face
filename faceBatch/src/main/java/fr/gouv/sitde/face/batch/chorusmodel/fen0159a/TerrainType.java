
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'un terrain dans CHORUS
 *
 * <p>
 * Classe Java pour TerrainType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="TerrainType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="COMP_CODE" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char4Type"/&gt;
 *         &lt;element name="UNIT_ECO" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char8Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TerrainType", propOrder = { "idChorus", "compCode", "unitEco" })
public class TerrainType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "COMP_CODE", required = true)
    private String compCode;
    @XmlElement(name = "UNIT_ECO", required = true)
    private String unitEco;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété compCode.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCompCode() {
        return this.compCode;
    }

    /**
     * Définit la valeur de la propriété compCode.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCompCode(String value) {
        this.compCode = value;
    }

    /**
     * Obtient la valeur de la propriété unitEco.
     *
     * @return possible object is {@link String }
     *
     */
    public String getUnitEco() {
        return this.unitEco;
    }

    /**
     * Définit la valeur de la propriété unitEco.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setUnitEco(String value) {
        this.unitEco = value;
    }

}

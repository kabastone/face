
package fr.gouv.sitde.face.batch.chorusmodel.fen0072a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>
 * Classe Java pour PosteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="PosteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FEN0072A}Char16Type"/&gt;
 *         &lt;element name="ID_LIG" type="{FEN0072A}Num6Type"/&gt;
 *         &lt;element name="STGE_LOC" type="{FEN0072A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="QUANTITY" type="{FEN0072A}Num10.3sType"/&gt;
 *         &lt;element name="UNIT" type="{FEN0072A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="PO_NUMBER" type="{FEN0072A}Char10Type"/&gt;
 *         &lt;element name="PO_ITEM" type="{FEN0072A}Num5Type"/&gt;
 *         &lt;element name="TXT_PST" type="{FEN0072A}Char50Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_FONC_SUPPL" type="{FEN0072A}Char80Type" minOccurs="0"/&gt;
 *         &lt;element name="GR_COMPTES_SUPPL" type="{FEN0072A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="ID_CHOR_SUPPL" type="{FEN0072A}Char10Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "POSTE")
@XmlType(name = "PosteType", propOrder = { "idAE", "idLigne", "magasinChorus", "montant", "unite", "numCourtEJ", "numPosteChorus", "textePoste",
        "idFournisseur", "groupeComptesFournisseur", "idChorusFournisseur" })
public class LignePoste implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_AE", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String idAE;
    @XmlElement(name = "ID_LIG", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String idLigne;
    @XmlElement(name = "STGE_LOC")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String magasinChorus;
    @XmlElement(name = "QUANTITY", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String montant;
    @XmlElement(name = "UNIT")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String unite;
    @XmlElement(name = "PO_NUMBER", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String numCourtEJ;
    @XmlElement(name = "PO_ITEM", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String numPosteChorus;
    @XmlElement(name = "TXT_PST")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String textePoste;
    @XmlElement(name = "ID_FONC_SUPPL")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String idFournisseur;
    @XmlElement(name = "GR_COMPTES_SUPPL")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String groupeComptesFournisseur;
    @XmlElement(name = "ID_CHOR_SUPPL")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    private String idChorusFournisseur;

    /**
     * Obtient la valeur de la propriété idAE.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdAE() {
        return this.idAE;
    }

    /**
     * Définit la valeur de la propriété idAE.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdAE(String value) {
        this.idAE = value;
    }

    /**
     * Obtient la valeur de la propriété idLigne.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdLigne() {
        return this.idLigne;
    }

    /**
     * Définit la valeur de la propriété idLigne.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdLigne(String value) {
        this.idLigne = value;
    }

    /**
     * Obtient la valeur de la propriété magasinChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMagasinChorus() {
        return this.magasinChorus;
    }

    /**
     * Définit la valeur de la propriété magasinChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setMagasinChorus(String value) {
        this.magasinChorus = value;
    }

    /**
     * Obtient la valeur de la propriété montant.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMontant() {
        return this.montant;
    }

    /**
     * Définit la valeur de la propriété montant.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setMontant(String value) {
        this.montant = value;
    }

    /**
     * Obtient la valeur de la propriété unite.
     *
     * @return possible object is {@link String }
     *
     */
    public String getUnite() {
        return this.unite;
    }

    /**
     * Définit la valeur de la propriété unite.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setUnite(String value) {
        this.unite = value;
    }

    /**
     * Obtient la valeur de la propriété numCourtEJ.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumCourtEJ() {
        return this.numCourtEJ;
    }

    /**
     * Définit la valeur de la propriété numCourtEJ.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumCourtEJ(String value) {
        this.numCourtEJ = value;
    }

    /**
     * Obtient la valeur de la propriété numPosteChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNumPosteChorus() {
        return this.numPosteChorus;
    }

    /**
     * Définit la valeur de la propriété numPosteChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setNumPosteChorus(String value) {
        this.numPosteChorus = value;
    }

    /**
     * Obtient la valeur de la propriété textePoste.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTextePoste() {
        return this.textePoste;
    }

    /**
     * Définit la valeur de la propriété textePoste.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setTextePoste(String value) {
        this.textePoste = value;
    }

    /**
     * Obtient la valeur de la propriété idFournisseur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdFournisseur() {
        return this.idFournisseur;
    }

    /**
     * Définit la valeur de la propriété idFournisseur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdFournisseur(String value) {
        this.idFournisseur = value;
    }

    /**
     * Obtient la valeur de la propriété groupeComptesFournisseur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGroupeComptesFournisseur() {
        return this.groupeComptesFournisseur;
    }

    /**
     * Définit la valeur de la propriété groupeComptesFournisseur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setGroupeComptesFournisseur(String value) {
        this.groupeComptesFournisseur = value;
    }

    /**
     * Obtient la valeur de la propriété idChorusFournisseur.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorusFournisseur() {
        return this.idChorusFournisseur;
    }

    /**
     * Définit la valeur de la propriété idChorusFournisseur.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorusFournisseur(String value) {
        this.idChorusFournisseur = value;
    }

}

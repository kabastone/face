/**
 *
 */
package fr.gouv.sitde.face.batch.job.cadencement.maj;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * @author A754839
 *
 */
public class CadencementMajWriter implements ItemWriter<CadencementMajDto> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CadencementMajWriter.class);

    @Autowired
    private CadencementService cadencementService;

    @Autowired
    private EmailService emailService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends CadencementMajDto> items) throws Exception {
        for (CadencementMajDto dto : items) {

            Boolean envoiEmail = false;
            for (Cadencement cadencement : dto.getCadencements()) {

                cadencement = this.cadencementService.enregistrerCadencement(cadencement);
                envoiEmail = envoiEmail || !cadencement.getEstValide();
            }

            if (envoiEmail && !dto.getCollectivite().getAdressesEmail().isEmpty()) {
                this.emailService.creerEtEnvoyerEmail(dto.getCollectivite(), TypeEmailEnum.CAD_MAJ, TemplateEmailEnum.CAD_MAJ, null);
                LOGGER.debug("Envoi de l'email pour la collectivite {}", dto.getCollectivite());
            }
        }
    }

}

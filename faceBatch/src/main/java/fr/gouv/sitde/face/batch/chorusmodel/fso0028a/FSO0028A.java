
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour FSO0028AType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="FSO0028AType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ENGAGEMENT_JURIDIQUE" type="{FSO0028A}EJType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "FSO0028A")
@XmlType(name = "FSO0028AType", propOrder = { "engagementJuridiqueFSO0028A" })
public class FSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** The engagement juridique FSO 0028 A. */
    @XmlElement(name = "ENGAGEMENT_JURIDIQUE", required = true)
    private List<EngagementJuridiqueFSO0028A> engagementJuridiqueFSO0028A;

    /**
     * Gets the value of the engagementJuridiqueFSO0028A property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the engagementJuridiqueFSO0028A property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getEngagementJuridiqueFSO0028A().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link EngagementJuridiqueFSO0028A }
     *
     * @return the engagement juridique FSO 0028 A
     */
    public List<EngagementJuridiqueFSO0028A> getEngagementJuridiqueFSO0028A() {
        if (this.engagementJuridiqueFSO0028A == null) {
            this.engagementJuridiqueFSO0028A = new ArrayList<>();
        }
        return this.engagementJuridiqueFSO0028A;
    }

    /**
     * Sets the engagement juridique FSO 0028 A.
     *
     * @param engagementJuridiqueFSO0028A
     *            the engagementJuridiqueFSO0028A to set
     */
    public void setEngagementJuridiqueFSO0028A(List<EngagementJuridiqueFSO0028A> engagementJuridiqueFSO0028A) {
        this.engagementJuridiqueFSO0028A = engagementJuridiqueFSO0028A;
    }

}

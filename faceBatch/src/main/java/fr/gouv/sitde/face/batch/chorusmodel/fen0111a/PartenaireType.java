
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour PartenaireType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="PartenaireType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdPartenaire"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartenaireType", propOrder = { "idPartenaire" })
public class PartenaireType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "IdPartenaire", required = true)
    private String idPartenaire;

    /**
     * Obtient la valeur de la propriété idPartenaire.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdPartenaire() {
        return this.idPartenaire;
    }

    /**
     * Définit la valeur de la propriété idPartenaire.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdPartenaire(String value) {
        this.idPartenaire = value;
    }

}

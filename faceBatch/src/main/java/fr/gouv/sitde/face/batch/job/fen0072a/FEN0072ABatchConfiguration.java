/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0072a;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.chorusmodel.fen0072a.Fen0072A;
import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Class FEN0072ABatchConfiguration.
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class FEN0072ABatchConfiguration {

    /** The chunk size. */
    private static final int CHUNK_SIZE = 1;

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    /**
     * Fen 0072 a reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<List<DemandePaiement>> fen0072aReader() {
        List<DemandePaiement> listeDemandes = this.demandePaiementService.rechercherToutesDemandesPaiementPourBatch();
        List<List<DemandePaiement>> readItem = new ArrayList<>();
        readItem.add(listeDemandes);
        return new ListItemReader<>(readItem);
    }

    /**
     * Repartition reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<DemandePaiement> repartitionReader() {
        List<DemandePaiement> listeDemandes = this.demandePaiementService.rechercherToutesPourRepartition();
        return new ListItemReader<>(listeDemandes);
    }

    /**
     * Fen 0072 a processor.
     *
     * @return the FEN 0072 A processor
     */
    @Bean
    @StepScope
    public FEN0072AProcessor fen0072aProcessor() {
        return new FEN0072AProcessor();
    }

    /**
     * Fen 0072 A writer.
     *
     * @return the FEN 0072 A writer
     */
    @Bean
    @StepScope
    public FEN0072AWriter fen0072AWriter() {
        return new FEN0072AWriter(this.batchCheminRepertoireProperties.getOutput());
    }

    /**
     * Repartition paiement.
     *
     * @return the FEN 0072 A repartition
     */
    @Bean
    @StepScope
    public FEN0072ARepartition repartitionPaiement() {
        return new FEN0072ARepartition();
    }

    /**
     * Step repartition paiement.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepRepartitionPaiement() {
        return this.stepBuilderFactory.get("step_repartition_paiement")
                .<DemandePaiement, DemandePaiement> chunk(FEN0072ABatchConfiguration.CHUNK_SIZE).reader(this.repartitionReader())
                .writer(this.repartitionPaiement()).faultTolerant().skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Step FEN 0072 A output.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepFEN0072AOutput() {
        return this.stepBuilderFactory.get("step_fen0072a_xml").<List<DemandePaiement>, Fen0072A> chunk(FEN0072ABatchConfiguration.CHUNK_SIZE)
                .reader(this.fen0072aReader()).processor(this.fen0072aProcessor()).writer(this.fen0072AWriter()).faultTolerant()
                .skipPolicy(new AlwaysSkipItemSkipPolicy()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.FEN_0072A_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.FEN_0072A_JOB.getNom()).start(this.stepRepartitionPaiement()).next(this.stepFEN0072AOutput())
                .build();
    }
}

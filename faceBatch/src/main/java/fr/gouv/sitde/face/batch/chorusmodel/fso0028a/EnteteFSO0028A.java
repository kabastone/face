
package fr.gouv.sitde.face.batch.chorusmodel.fso0028a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour EnteteType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EnteteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_AE" type="{FSO0028A}Char12Type"/&gt;
 *         &lt;element name="ID_EJ" type="{FSO0028A}Char10Type"/&gt;
 *         &lt;element name="ID_LONG" type="{FSO0028A}Char40Type" minOccurs="0"/&gt;
 *         &lt;element name="PROCESS_TYPE" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="INITIATEUR" type="{FSO0028A}Char12Type" minOccurs="0"/&gt;
 *         &lt;element name="CREATED_AT" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="APPROBATEUR" type="{FSO0028A}Char12Type" minOccurs="0"/&gt;
 *         &lt;element name="CHANGED_BY" type="{FSO0028A}Char12Type" minOccurs="0"/&gt;
 *         &lt;element name="CHANGED_AT" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="STATUT" type="{FSO0028A}Char30Type" minOccurs="0"/&gt;
 *         &lt;element name="CO_CODE" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="CURRENCY" type="{FSO0028A}Char5Type" minOccurs="0"/&gt;
 *         &lt;element name="TOTAL_VALUE" type="{FSO0028A}Num16.2sType" minOccurs="0"/&gt;
 *         &lt;element name="PMNTTRMS" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="PS_POSTING_DATE" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="FISC_YEAR" type="{FSO0028A}YearType" minOccurs="0"/&gt;
 *         &lt;element name="BE_PUR_GROUP" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="BE_PUR_ORG" type="{FSO0028A}Char4Type" minOccurs="0"/&gt;
 *         &lt;element name="PS_PGS_ID" type="{FSO0028A}Char16Type" minOccurs="0"/&gt;
 *         &lt;element name="DEBUT_MARCHE" type="{FSO0028A}Char8Type"/&gt;
 *         &lt;element name="DUREE_MARCHE" type="{FSO0028A}Num4Type" minOccurs="0"/&gt;
 *         &lt;element name="FIN_MARCHE" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="RECOND_ANN" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="RECOND_DATE" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="DEBUT_EXEC" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="DELAI_EXEC" type="{FSO0028A}Num3Type" minOccurs="0"/&gt;
 *         &lt;element name="FIN_EXEC" type="{FSO0028A}DateType" minOccurs="0"/&gt;
 *         &lt;element name="SIRET_ACHAT" type="{FSO0028A}Char14Type" minOccurs="0"/&gt;
 *         &lt;element name="TYPE_MARCHE" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="PROCEDURE" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="CCAG" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="REF_ACC_CADRE" type="{FSO0028A}Char40Type" minOccurs="0"/&gt;
 *         &lt;element name="CPV_PRINC" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="CPV_SECOND1" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="CPV_SECOND2" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="CPV_SECOND3" type="{FSO0028A}Char10Type" minOccurs="0"/&gt;
 *         &lt;element name="NB_ENTR_CO" type="{FSO0028A}Num3Type" minOccurs="0"/&gt;
 *         &lt;element name="CARTE_ACHAT" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="CLAUSE_SOCIALE" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="NB_PROP_RECUES" type="{FSO0028A}Num3Type" minOccurs="0"/&gt;
 *         &lt;element name="NB_PROP_DEMAT" type="{FSO0028A}Num3Type" minOccurs="0"/&gt;
 *         &lt;element name="CLAUSE_ENV" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="FORME_PRIX" type="{FSO0028A}Char3Type" minOccurs="0"/&gt;
 *         &lt;element name="PENAL_RETARD" type="{FSO0028A}Char1Type" minOccurs="0"/&gt;
 *         &lt;element name="VZSKZ" type="{FSO0028A}Char2Type" minOccurs="0"/&gt;
 *         &lt;element name="CODE_PAYS_OSD" type="{FSO0028A}Char14Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnteteType",
        propOrder = { "idAe", "idEj", "idLong", "processType", "initiateur", "createdAt", "approbateur", "changedBy", "changedAt", "statut", "coCode",
                "currency", "totalValue", "pmnttrms", "psPostingDate", "fiscYear", "bePurGroup", "bePurOrg", "psPgsId", "debutMarche", "dureeMarche",
                "finMarche", "recondAnn", "recondDate", "debutExec", "delaiExec", "finExec", "siretAchat", "typeMarche", "procedure", "ccag",
                "refAccCadre", "cpvPrinc", "cpvSecond1", "cpvSecond2", "cpvSecond3", "nbEntrCo", "carteAchat", "clauseSociale", "nbPropRecues",
                "nbPropDemat", "clauseEnv", "formePrix", "penalRetard", "vzskz", "codePaysOsd" })
public class EnteteFSO0028A implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The id ae. */
    @XmlElement(name = "ID_AE", required = true)
    private String idAe;

    /** The id ej. */
    @XmlElement(name = "ID_EJ", required = true)
    private String idEj;

    /** The id long. */
    @XmlElement(name = "ID_LONG")
    private String idLong;

    /** The process type. */
    @XmlElement(name = "PROCESS_TYPE")
    private String processType;

    /** The initiateur. */
    @XmlElement(name = "INITIATEUR")
    private String initiateur;

    /** The created at. */
    @XmlElement(name = "CREATED_AT")
    private String createdAt;

    /** The approbateur. */
    @XmlElement(name = "APPROBATEUR")
    private String approbateur;

    /** The changed by. */
    @XmlElement(name = "CHANGED_BY")
    private String changedBy;

    /** The changed at. */
    @XmlElement(name = "CHANGED_AT")
    private String changedAt;

    /** The statut. */
    @XmlElement(name = "STATUT")
    private String statut;

    /** The co code. */
    @XmlElement(name = "CO_CODE")
    private String coCode;

    /** The currency. */
    @XmlElement(name = "CURRENCY")
    private String currency;

    /** The total value. */
    @XmlElement(name = "TOTAL_VALUE")
    private String totalValue;

    /** The pmnttrms. */
    @XmlElement(name = "PMNTTRMS")
    private String pmnttrms;

    /** The ps posting date. */
    @XmlElement(name = "PS_POSTING_DATE")
    private String psPostingDate;

    /** The fisc year. */
    @XmlElement(name = "FISC_YEAR")
    private String fiscYear;

    /** The be pur group. */
    @XmlElement(name = "BE_PUR_GROUP")
    private String bePurGroup;

    /** The be pur org. */
    @XmlElement(name = "BE_PUR_ORG")
    private String bePurOrg;

    /** The ps pgs id. */
    @XmlElement(name = "PS_PGS_ID")
    private String psPgsId;

    /** The debut marche. */
    @XmlElement(name = "DEBUT_MARCHE", required = true)
    private String debutMarche;

    /** The duree marche. */
    @XmlElement(name = "DUREE_MARCHE")
    private String dureeMarche;

    /** The fin marche. */
    @XmlElement(name = "FIN_MARCHE")
    private String finMarche;

    /** The recond ann. */
    @XmlElement(name = "RECOND_ANN")
    private String recondAnn;

    /** The recond date. */
    @XmlElement(name = "RECOND_DATE")
    private String recondDate;

    /** The debut exec. */
    @XmlElement(name = "DEBUT_EXEC")
    private String debutExec;

    /** The delai exec. */
    @XmlElement(name = "DELAI_EXEC")
    private String delaiExec;

    /** The fin exec. */
    @XmlElement(name = "FIN_EXEC")
    private String finExec;

    /** The siret achat. */
    @XmlElement(name = "SIRET_ACHAT")
    private String siretAchat;

    /** The type marche. */
    @XmlElement(name = "TYPE_MARCHE")
    private String typeMarche;

    /** The procedure. */
    @XmlElement(name = "PROCEDURE")
    private String procedure;

    /** The ccag. */
    @XmlElement(name = "CCAG")
    private String ccag;

    /** The ref acc cadre. */
    @XmlElement(name = "REF_ACC_CADRE")
    private String refAccCadre;

    /** The cpv princ. */
    @XmlElement(name = "CPV_PRINC")
    private String cpvPrinc;

    /** The cpv second 1. */
    @XmlElement(name = "CPV_SECOND1")
    private String cpvSecond1;

    /** The cpv second 2. */
    @XmlElement(name = "CPV_SECOND2")
    private String cpvSecond2;

    /** The cpv second 3. */
    @XmlElement(name = "CPV_SECOND3")
    private String cpvSecond3;

    /** The nb entr co. */
    @XmlElement(name = "NB_ENTR_CO")
    private String nbEntrCo;

    /** The carte achat. */
    @XmlElement(name = "CARTE_ACHAT")
    private String carteAchat;

    /** The clause sociale. */
    @XmlElement(name = "CLAUSE_SOCIALE")
    private String clauseSociale;

    /** The nb prop recues. */
    @XmlElement(name = "NB_PROP_RECUES")
    private String nbPropRecues;

    /** The nb prop demat. */
    @XmlElement(name = "NB_PROP_DEMAT")
    private String nbPropDemat;

    /** The clause env. */
    @XmlElement(name = "CLAUSE_ENV")
    private String clauseEnv;

    /** The forme prix. */
    @XmlElement(name = "FORME_PRIX")
    private String formePrix;

    /** The penal retard. */
    @XmlElement(name = "PENAL_RETARD")
    private String penalRetard;

    /** The vzskz. */
    @XmlElement(name = "VZSKZ")
    private String vzskz;

    /** The code pays osd. */
    @XmlElement(name = "CODE_PAYS_OSD")
    private String codePaysOsd;

    /**
     * Gets the id ae.
     *
     * @return the idAe
     */
    public String getIdAe() {
        return this.idAe;
    }

    /**
     * Sets the id ae.
     *
     * @param idAe
     *            the idAe to set
     */
    public void setIdAe(String idAe) {
        this.idAe = idAe;
    }

    /**
     * Gets the id ej.
     *
     * @return the idEj
     */
    public String getIdEj() {
        return this.idEj;
    }

    /**
     * Sets the id ej.
     *
     * @param idEj
     *            the idEj to set
     */
    public void setIdEj(String idEj) {
        this.idEj = idEj;
    }

    /**
     * Gets the id long.
     *
     * @return the idLong
     */
    public String getIdLong() {
        return this.idLong;
    }

    /**
     * Sets the id long.
     *
     * @param idLong
     *            the idLong to set
     */
    public void setIdLong(String idLong) {
        this.idLong = idLong;
    }

    /**
     * Gets the process type.
     *
     * @return the processType
     */
    public String getProcessType() {
        return this.processType;
    }

    /**
     * Sets the process type.
     *
     * @param processType
     *            the processType to set
     */
    public void setProcessType(String processType) {
        this.processType = processType;
    }

    /**
     * Gets the initiateur.
     *
     * @return the initiateur
     */
    public String getInitiateur() {
        return this.initiateur;
    }

    /**
     * Sets the initiateur.
     *
     * @param initiateur
     *            the initiateur to set
     */
    public void setInitiateur(String initiateur) {
        this.initiateur = initiateur;
    }

    /**
     * Gets the created at.
     *
     * @return the createdAt
     */
    public String getCreatedAt() {
        return this.createdAt;
    }

    /**
     * Sets the created at.
     *
     * @param createdAt
     *            the createdAt to set
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets the approbateur.
     *
     * @return the approbateur
     */
    public String getApprobateur() {
        return this.approbateur;
    }

    /**
     * Sets the approbateur.
     *
     * @param approbateur
     *            the approbateur to set
     */
    public void setApprobateur(String approbateur) {
        this.approbateur = approbateur;
    }

    /**
     * Gets the changed by.
     *
     * @return the changedBy
     */
    public String getChangedBy() {
        return this.changedBy;
    }

    /**
     * Sets the changed by.
     *
     * @param changedBy
     *            the changedBy to set
     */
    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    /**
     * Gets the changed at.
     *
     * @return the changedAt
     */
    public String getChangedAt() {
        return this.changedAt;
    }

    /**
     * Sets the changed at.
     *
     * @param changedAt
     *            the changedAt to set
     */
    public void setChangedAt(String changedAt) {
        this.changedAt = changedAt;
    }

    /**
     * Gets the statut.
     *
     * @return the statut
     */
    public String getStatut() {
        return this.statut;
    }

    /**
     * Sets the statut.
     *
     * @param statut
     *            the statut to set
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * Gets the co code.
     *
     * @return the coCode
     */
    public String getCoCode() {
        return this.coCode;
    }

    /**
     * Sets the co code.
     *
     * @param coCode
     *            the coCode to set
     */
    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    /**
     * Gets the currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return this.currency;
    }

    /**
     * Sets the currency.
     *
     * @param currency
     *            the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Gets the total value.
     *
     * @return the totalValue
     */
    public String getTotalValue() {
        return this.totalValue;
    }

    /**
     * Sets the total value.
     *
     * @param totalValue
     *            the totalValue to set
     */
    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    /**
     * Gets the pmnttrms.
     *
     * @return the pmnttrms
     */
    public String getPmnttrms() {
        return this.pmnttrms;
    }

    /**
     * Sets the pmnttrms.
     *
     * @param pmnttrms
     *            the pmnttrms to set
     */
    public void setPmnttrms(String pmnttrms) {
        this.pmnttrms = pmnttrms;
    }

    /**
     * Gets the ps posting date.
     *
     * @return the psPostingDate
     */
    public String getPsPostingDate() {
        return this.psPostingDate;
    }

    /**
     * Sets the ps posting date.
     *
     * @param psPostingDate
     *            the psPostingDate to set
     */
    public void setPsPostingDate(String psPostingDate) {
        this.psPostingDate = psPostingDate;
    }

    /**
     * Gets the fisc year.
     *
     * @return the fiscYear
     */
    public String getFiscYear() {
        return this.fiscYear;
    }

    /**
     * Sets the fisc year.
     *
     * @param fiscYear
     *            the fiscYear to set
     */
    public void setFiscYear(String fiscYear) {
        this.fiscYear = fiscYear;
    }

    /**
     * Gets the be pur group.
     *
     * @return the bePurGroup
     */
    public String getBePurGroup() {
        return this.bePurGroup;
    }

    /**
     * Sets the be pur group.
     *
     * @param bePurGroup
     *            the bePurGroup to set
     */
    public void setBePurGroup(String bePurGroup) {
        this.bePurGroup = bePurGroup;
    }

    /**
     * Gets the be pur org.
     *
     * @return the bePurOrg
     */
    public String getBePurOrg() {
        return this.bePurOrg;
    }

    /**
     * Sets the be pur org.
     *
     * @param bePurOrg
     *            the bePurOrg to set
     */
    public void setBePurOrg(String bePurOrg) {
        this.bePurOrg = bePurOrg;
    }

    /**
     * Gets the ps pgs id.
     *
     * @return the psPgsId
     */
    public String getPsPgsId() {
        return this.psPgsId;
    }

    /**
     * Sets the ps pgs id.
     *
     * @param psPgsId
     *            the psPgsId to set
     */
    public void setPsPgsId(String psPgsId) {
        this.psPgsId = psPgsId;
    }

    /**
     * Gets the debut marche.
     *
     * @return the debutMarche
     */
    public String getDebutMarche() {
        return this.debutMarche;
    }

    /**
     * Sets the debut marche.
     *
     * @param debutMarche
     *            the debutMarche to set
     */
    public void setDebutMarche(String debutMarche) {
        this.debutMarche = debutMarche;
    }

    /**
     * Gets the duree marche.
     *
     * @return the dureeMarche
     */
    public String getDureeMarche() {
        return this.dureeMarche;
    }

    /**
     * Sets the duree marche.
     *
     * @param dureeMarche
     *            the dureeMarche to set
     */
    public void setDureeMarche(String dureeMarche) {
        this.dureeMarche = dureeMarche;
    }

    /**
     * Gets the fin marche.
     *
     * @return the finMarche
     */
    public String getFinMarche() {
        return this.finMarche;
    }

    /**
     * Sets the fin marche.
     *
     * @param finMarche
     *            the finMarche to set
     */
    public void setFinMarche(String finMarche) {
        this.finMarche = finMarche;
    }

    /**
     * Gets the recond ann.
     *
     * @return the recondAnn
     */
    public String getRecondAnn() {
        return this.recondAnn;
    }

    /**
     * Sets the recond ann.
     *
     * @param recondAnn
     *            the recondAnn to set
     */
    public void setRecondAnn(String recondAnn) {
        this.recondAnn = recondAnn;
    }

    /**
     * Gets the recond date.
     *
     * @return the recondDate
     */
    public String getRecondDate() {
        return this.recondDate;
    }

    /**
     * Sets the recond date.
     *
     * @param recondDate
     *            the recondDate to set
     */
    public void setRecondDate(String recondDate) {
        this.recondDate = recondDate;
    }

    /**
     * Gets the debut exec.
     *
     * @return the debutExec
     */
    public String getDebutExec() {
        return this.debutExec;
    }

    /**
     * Sets the debut exec.
     *
     * @param debutExec
     *            the debutExec to set
     */
    public void setDebutExec(String debutExec) {
        this.debutExec = debutExec;
    }

    /**
     * Gets the delai exec.
     *
     * @return the delaiExec
     */
    public String getDelaiExec() {
        return this.delaiExec;
    }

    /**
     * Sets the delai exec.
     *
     * @param delaiExec
     *            the delaiExec to set
     */
    public void setDelaiExec(String delaiExec) {
        this.delaiExec = delaiExec;
    }

    /**
     * Gets the fin exec.
     *
     * @return the finExec
     */
    public String getFinExec() {
        return this.finExec;
    }

    /**
     * Sets the fin exec.
     *
     * @param finExec
     *            the finExec to set
     */
    public void setFinExec(String finExec) {
        this.finExec = finExec;
    }

    /**
     * Gets the siret achat.
     *
     * @return the siretAchat
     */
    public String getSiretAchat() {
        return this.siretAchat;
    }

    /**
     * Sets the siret achat.
     *
     * @param siretAchat
     *            the siretAchat to set
     */
    public void setSiretAchat(String siretAchat) {
        this.siretAchat = siretAchat;
    }

    /**
     * Gets the type marche.
     *
     * @return the typeMarche
     */
    public String getTypeMarche() {
        return this.typeMarche;
    }

    /**
     * Sets the type marche.
     *
     * @param typeMarche
     *            the typeMarche to set
     */
    public void setTypeMarche(String typeMarche) {
        this.typeMarche = typeMarche;
    }

    /**
     * Gets the procedure.
     *
     * @return the procedure
     */
    public String getProcedure() {
        return this.procedure;
    }

    /**
     * Sets the procedure.
     *
     * @param procedure
     *            the procedure to set
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    /**
     * Gets the ccag.
     *
     * @return the ccag
     */
    public String getCcag() {
        return this.ccag;
    }

    /**
     * Sets the ccag.
     *
     * @param ccag
     *            the ccag to set
     */
    public void setCcag(String ccag) {
        this.ccag = ccag;
    }

    /**
     * Gets the ref acc cadre.
     *
     * @return the refAccCadre
     */
    public String getRefAccCadre() {
        return this.refAccCadre;
    }

    /**
     * Sets the ref acc cadre.
     *
     * @param refAccCadre
     *            the refAccCadre to set
     */
    public void setRefAccCadre(String refAccCadre) {
        this.refAccCadre = refAccCadre;
    }

    /**
     * Gets the cpv princ.
     *
     * @return the cpvPrinc
     */
    public String getCpvPrinc() {
        return this.cpvPrinc;
    }

    /**
     * Sets the cpv princ.
     *
     * @param cpvPrinc
     *            the cpvPrinc to set
     */
    public void setCpvPrinc(String cpvPrinc) {
        this.cpvPrinc = cpvPrinc;
    }

    /**
     * Gets the cpv second 1.
     *
     * @return the cpvSecond1
     */
    public String getCpvSecond1() {
        return this.cpvSecond1;
    }

    /**
     * Sets the cpv second 1.
     *
     * @param cpvSecond1
     *            the cpvSecond1 to set
     */
    public void setCpvSecond1(String cpvSecond1) {
        this.cpvSecond1 = cpvSecond1;
    }

    /**
     * Gets the cpv second 2.
     *
     * @return the cpvSecond2
     */
    public String getCpvSecond2() {
        return this.cpvSecond2;
    }

    /**
     * Sets the cpv second 2.
     *
     * @param cpvSecond2
     *            the cpvSecond2 to set
     */
    public void setCpvSecond2(String cpvSecond2) {
        this.cpvSecond2 = cpvSecond2;
    }

    /**
     * Gets the cpv second 3.
     *
     * @return the cpvSecond3
     */
    public String getCpvSecond3() {
        return this.cpvSecond3;
    }

    /**
     * Sets the cpv second 3.
     *
     * @param cpvSecond3
     *            the cpvSecond3 to set
     */
    public void setCpvSecond3(String cpvSecond3) {
        this.cpvSecond3 = cpvSecond3;
    }

    /**
     * Gets the nb entr co.
     *
     * @return the nbEntrCo
     */
    public String getNbEntrCo() {
        return this.nbEntrCo;
    }

    /**
     * Sets the nb entr co.
     *
     * @param nbEntrCo
     *            the nbEntrCo to set
     */
    public void setNbEntrCo(String nbEntrCo) {
        this.nbEntrCo = nbEntrCo;
    }

    /**
     * Gets the carte achat.
     *
     * @return the carteAchat
     */
    public String getCarteAchat() {
        return this.carteAchat;
    }

    /**
     * Sets the carte achat.
     *
     * @param carteAchat
     *            the carteAchat to set
     */
    public void setCarteAchat(String carteAchat) {
        this.carteAchat = carteAchat;
    }

    /**
     * Gets the clause sociale.
     *
     * @return the clauseSociale
     */
    public String getClauseSociale() {
        return this.clauseSociale;
    }

    /**
     * Sets the clause sociale.
     *
     * @param clauseSociale
     *            the clauseSociale to set
     */
    public void setClauseSociale(String clauseSociale) {
        this.clauseSociale = clauseSociale;
    }

    /**
     * Gets the nb prop recues.
     *
     * @return the nbPropRecues
     */
    public String getNbPropRecues() {
        return this.nbPropRecues;
    }

    /**
     * Sets the nb prop recues.
     *
     * @param nbPropRecues
     *            the nbPropRecues to set
     */
    public void setNbPropRecues(String nbPropRecues) {
        this.nbPropRecues = nbPropRecues;
    }

    /**
     * Gets the nb prop demat.
     *
     * @return the nbPropDemat
     */
    public String getNbPropDemat() {
        return this.nbPropDemat;
    }

    /**
     * Sets the nb prop demat.
     *
     * @param nbPropDemat
     *            the nbPropDemat to set
     */
    public void setNbPropDemat(String nbPropDemat) {
        this.nbPropDemat = nbPropDemat;
    }

    /**
     * Gets the clause env.
     *
     * @return the clauseEnv
     */
    public String getClauseEnv() {
        return this.clauseEnv;
    }

    /**
     * Sets the clause env.
     *
     * @param clauseEnv
     *            the clauseEnv to set
     */
    public void setClauseEnv(String clauseEnv) {
        this.clauseEnv = clauseEnv;
    }

    /**
     * Gets the forme prix.
     *
     * @return the formePrix
     */
    public String getFormePrix() {
        return this.formePrix;
    }

    /**
     * Sets the forme prix.
     *
     * @param formePrix
     *            the formePrix to set
     */
    public void setFormePrix(String formePrix) {
        this.formePrix = formePrix;
    }

    /**
     * Gets the penal retard.
     *
     * @return the penalRetard
     */
    public String getPenalRetard() {
        return this.penalRetard;
    }

    /**
     * Sets the penal retard.
     *
     * @param penalRetard
     *            the penalRetard to set
     */
    public void setPenalRetard(String penalRetard) {
        this.penalRetard = penalRetard;
    }

    /**
     * Gets the vzskz.
     *
     * @return the vzskz
     */
    public String getVzskz() {
        return this.vzskz;
    }

    /**
     * Sets the vzskz.
     *
     * @param vzskz
     *            the vzskz to set
     */
    public void setVzskz(String vzskz) {
        this.vzskz = vzskz;
    }

    /**
     * Gets the code pays osd.
     *
     * @return the codePaysOsd
     */
    public String getCodePaysOsd() {
        return this.codePaysOsd;
    }

    /**
     * Sets the code pays osd.
     *
     * @param codePaysOsd
     *            the codePaysOsd to set
     */
    public void setCodePaysOsd(String codePaysOsd) {
        this.codePaysOsd = codePaysOsd;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.job.fen0111a;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EJPartenaireEnteteType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EngagementJuridiqueType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EngagementsJuridiques;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.EnteteType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.ImputationType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.LignePosteType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.PJType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.PartenaireType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.TypeEJType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.TypeImputationType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.TypeLigneType;
import fr.gouv.sitde.face.batch.chorusmodel.fen0111a.UniteType;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * The Class FEN0111AProcessor.
 */
public class FEN0111AProcessor implements ItemProcessor<DemandeSubvention, FEN0111ADto> {

    /** The Constant VERSION_ENGAGEMENTS. */
    private static final String VERSION_ENGAGEMENTS = "1.0";

    /** The Constant NB_PJ_PAR_FICHIER. */
    private static final int NB_PJ_PAR_FICHIER = 2;

    /** The Constant NON_APPLICABLE. */
    private static final String NON_APPLICABLE = "N/A";

    /** The Constant PREFIXE_ZERO. */
    private static final String PREFIXE_ZERO = "0";

    /** The flux chorus service. */
    @Inject
    private FluxChorusService fluxChorusService;

    /** The fichier service. */
    @Inject
    private DocumentService documentService;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public FEN0111ADto process(DemandeSubvention demande) throws Exception {

        FluxChorus fluxChorus = this.fluxChorusService.rechercherFluxChorus();
        EngagementsJuridiques engagements = new EngagementsJuridiques();
        engagements.setCodeAppli(fluxChorus.getCodeApplication());
        engagements.setVersion(VERSION_ENGAGEMENTS);

        // Création de l'entête.
        EnteteType entete = new EnteteType();
        entete.setTypeEJ(TypeEJType.fromValue(fluxChorus.getTypeOperation()));
        entete.setOrganisationAchat(fluxChorus.getOrganisationDachat());
        entete.setGroupeAcheteur(fluxChorus.getGroupeAcheteurs());

        entete.setIdEJAppliExt(demande.getDossierSubvention().getChorusIdAe().toString());
        entete.setDevise(fluxChorus.getCodeDevise());
        entete.setCondPaiement(fluxChorus.getConditionPaiement());
        entete.setCodeSociete(fluxChorus.getCodeSociete());
        entete.setSchemaPartenaire(fluxChorus.getSchemaPartenaire());
        EngagementJuridiqueType engagementSimple = new EngagementJuridiqueType();
        engagementSimple.setEntete(entete);

        // Création de l'EJ partenaire.
        EJPartenaireEnteteType ejPartenaire = new EJPartenaireEnteteType();

        // Ajout du Demandeur.
        PartenaireType demandeur = new PartenaireType();
        demandeur.setIdPartenaire(fluxChorus.getCodeDemandeur());
        ejPartenaire.setDemandeur(demandeur);

        // Ajout du Site.
        PartenaireType site = new PartenaireType();
        site.setIdPartenaire(fluxChorus.getCodeSite());
        ejPartenaire.setSite(site);

        engagementSimple.setEJPartenaireEntete(ejPartenaire);

        // Création de la ligne Poste.
        LignePosteType lignePoste = new LignePosteType();
        lignePoste.setIdLigne(Long.parseLong(demande.getChorusNumLigneLegacy()));
        lignePoste.setDescription(FEN0111AProcessor.creerDescriptionLignePoste(demande.getDossierSubvention()));
        lignePoste.setCategorie(demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getCategorie());

        // Création de la date de livraison pour ligne Poste.
        lignePoste.setDateLivraison(demande.getDossierSubvention().getDateEcheanceAchevement());
        lignePoste.setQuantite(demande.getPlafondAide().setScale(2, RoundingMode.HALF_UP));
        lignePoste.setUnite(UniteType.fromValue(fluxChorus.getUnite()));
        lignePoste.setCodeTVA(fluxChorus.getCodeTVA());
        lignePoste.setTypeLigne(TypeLigneType.fromValue(fluxChorus.getTypeLigne()));
        lignePoste.setTypeOption(fluxChorus.getTypeOption() == "1");
        lignePoste.setServiceFaitAutoFlux2(fluxChorus.getServiceFaitAutoFlux() == "1");

        // Ajout du tiers Bénéficiaire.
        PartenaireType tiersBeneficiaire = new PartenaireType();
        tiersBeneficiaire.setIdPartenaire(demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getIdChorus());
        lignePoste.setTiersBeneficiaire(tiersBeneficiaire);

        List<LignePosteType> listeLignePoste = new ArrayList<>(1);
        listeLignePoste.add(lignePoste);
        engagementSimple.setLignePostes(listeLignePoste);

        // Création de l'Imputation.
        ImputationType imputation = new ImputationType();
        imputation.setTypeImputation(TypeImputationType.fromValue(fluxChorus.getTypeImputation()));
        imputation.setCentreCouts(fluxChorus.getCentreDeCout());
        imputation.setCentreFinancier(demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getCodeCentreFinancier());
        imputation.setDomaineFonctionnel(PREFIXE_ZERO + demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getCodeDomaineFonctionnel());
        imputation.setLocalisationInterministerielle(fluxChorus.getLocalisationInter());
        imputation.setActivite(NON_APPLICABLE);
        lignePoste.setImputation(imputation);

        // Récupération des pièces jointes.
        // Fiche Siret.
        Document ficheSiret = this.demandeSubventionService.recupererDocumentParDemandeEtType(demande, TypeDocumentEnum.FICHE_SIRET);
        PJType ficheSiretJointe = new PJType();
        ficheSiretJointe.setType(ficheSiret.getExtensionFichier());
        ficheSiretJointe.setDescription(ficheSiret.getNomFichierSansExtension());
        ficheSiretJointe.setData(this.documentService.zipperEtEncoderDocument(ficheSiret));

        // Etat Previsionnel.
        Document etatPrev = this.demandeSubventionService.recupererDocumentParDemandeEtType(demande, TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);
        PJType etatPrevJointe = new PJType();
        etatPrevJointe.setType(etatPrev.getExtensionFichier());
        etatPrevJointe.setDescription(etatPrev.getNomFichierSansExtension());
        etatPrevJointe.setData(this.documentService.zipperEtEncoderDocument(etatPrev));

        List<PJType> listePiecesJointes = new ArrayList<>(NB_PJ_PAR_FICHIER);
        listePiecesJointes.add(etatPrevJointe);
        listePiecesJointes.add(ficheSiretJointe);
        engagementSimple.setPjs(listePiecesJointes);

        // Formation du dto complet.
        List<EngagementJuridiqueType> listeEngagement = new ArrayList<>(1);
        listeEngagement.add(engagementSimple);
        engagements.setEngagementJuridiques(listeEngagement);
        return new FEN0111ADto(demande, engagements);
    }

    /**
     * Creer description ligne poste.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the string
     */
    private static String creerDescriptionLignePoste(DossierSubvention dossierSubvention) {
        StringBuilder builder = new StringBuilder();
        builder.append("DGEC");
        builder.append(' ');

        // Validation des différentes contraintes pour le nom du dossier.
        String patternString = "^([0-9]{4})([A-Z]{2,3})(([0-9]{2}[AB])|([0-9]{3}))([0-9]{1,10})(-[0-9]{1,3})?$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(dossierSubvention.getNumDossier());

        if (!matcher.find()) {
            throw new RegleGestionException("Le numero de dossier est invalide");
        }

        // REGEX : Premier groupe entre parenthèses doit représenter l'année.
        final int groupYear = 1;
        builder.append(matcher.group(groupYear));
        builder.append(' ');

        // REGEX : Second groupe entre parenthèses doit représenter le code sous programme.
        final int groupAbreviationSousProgramme = 2;
        builder.append(matcher.group(groupAbreviationSousProgramme));
        builder.append(' ');

        // REGEX : Troisième groupe entre parenthèses doit représenter le code de département.
        final int groupCodeDepartement = 3;
        builder.append(matcher.group(groupCodeDepartement));
        builder.append(' ');

        // REGEX : Sixième groupe entre parenthèses doit représenter le numéro de la collectivité.
        final int groupNumCollectivite = 6;
        builder.append(matcher.group(groupNumCollectivite));
        return builder.toString();
    }

}

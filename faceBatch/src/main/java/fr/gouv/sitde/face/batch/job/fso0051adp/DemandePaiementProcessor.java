/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051adp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class DemandePaiementProcessor.
 *
 * @author Atos
 */
public class DemandePaiementProcessor implements ItemProcessor<DemandePaiementChorusDto, DemandePaiementChorus> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DemandePaiementProcessor.class);

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The transfert paiement service. */
    @Inject
    private TransfertPaiementService transfertPaiementService;

    /** The Constant BATCH_51_DATE_FORMAT. */
    private static final String BATCH_51_DATE_FORMAT = "ddMMyyyy";

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public DemandePaiementChorus process(DemandePaiementChorusDto item) throws Exception {

        DemandePaiementChorus demandePaiementChorus = new DemandePaiementChorus();

        DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(BATCH_51_DATE_FORMAT);

        LocalDateTime dateComptable = null;
        TransfertPaiement transfertPaiement = null;

        try {
            dateComptable = LocalDateTime.of(LocalDate.parse(item.getDateComptable(), dateTimerFormatter), LocalTime.NOON);

        } catch (DateTimeParseException e) {
            LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.erreur.date.comptable", item.getDateComptable(), item.getNumeroServiceFait(),
                    item.getNumeroEngagementJuridique()), e);
        }

        // Step 1 rechercher le transfert de paiement
        List<TransfertPaiement> listeTransfert = this.transfertPaiementService.rechercherParNumSFetLegacyetNumEJ(item.getNumeroServiceFait(),
                item.getPosteEJ(), item.getNumeroEngagementJuridique());

        if (listeTransfert.size() == 1) {
            transfertPaiement = listeTransfert.get(0);
            if (transfertPaiement != null) {
                transfertPaiement.setDateVersement(dateComptable);
            }
        }

        demandePaiementChorus.setTransfertPaiement(transfertPaiement);

        // Step 2 certifier le paiement
        // recherche des demandes de paiement

        List<DemandePaiement> listDemandePaiement = this.demandePaiementService.rechercherVersementNonNulParService(item.getNumeroServiceFait());

        for (DemandePaiement demandePaiement : listDemandePaiement) {

            demandePaiementChorus.addDemandePaiement(demandePaiement);
        }

        return demandePaiementChorus;
    }

}

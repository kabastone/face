/**
 *
 */
package fr.gouv.sitde.face.batch.runner;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum JobEnum.
 *
 * @author a453029
 */
public enum JobEnum {

    /** The cen 0111a job. */
    CEN_0111A_JOB(Constantes.CEN_0111A_JOB),

    /** The cen 0072a job. */
    CEN_0072A_JOB(Constantes.CEN_0072A_JOB),

    /** The cen 0159a job. */
    CEN_0159A_JOB(Constantes.CEN_0159A_JOB),

    /** The mail relance job. */
    MAIL_RELANCE_JOB(Constantes.EMAIL_RELANCE_JOB),

    /** The email expiration job. */
    EMAIL_EXPIRATION_JOB(Constantes.EMAIL_EXPIRATION_JOB),

    /** The email consommation job. */
    EMAIL_CONSOMMATION_JOB(Constantes.EMAIL_CONSOMMATION_JOB),

    /** The cadencement maj job. */
    CADENCEMENT_MAJ_JOB(Constantes.CADENCEMENT_MAJ_JOB),

    /** The email cadencement non valide job. */
    EMAIL_CADENCEMENT_NON_VALIDE_JOB(Constantes.EMAIL_CADENCEMENT_NON_VALIDE_JOB),
    
    /** The email rappel consommation job. */
    EMAIL_RAPPEL_CONSOMMATION_JOB(Constantes.EMAIL_RAPPEL_CONSOMMATION_JOB),

    /** The fso0051a sf job. */
    FSO0051A_SF_JOB(Constantes.FSO0051A_SF_JOB),

    /** The fso0051a dp job. */
    FSO0051A_DP_JOB(Constantes.FSO0051A_DP_JOB),

    /** The fso0051a ej job. */
    FSO0051A_EJ_JOB(Constantes.FSO0051A_EJ_JOB),

    /** The fen 0072a job. */
    FEN_0072A_JOB(Constantes.FEN_0072A_JOB),

    /** The fen 0159a job. */
    FEN_0159A_JOB(Constantes.FEN_0159A_JOB),

    /** The fen 0111a job. */
    FEN_0111A_JOB(Constantes.FEN_0111A_JOB),

    /** The fso 0028a job. */
    FSO_0028A_JOB(Constantes.FSO_0028A_JOB),
    
    /** The perte dotattion job. */
    PERTE_DOTATION_JOB(Constantes.PERTE_DOTATION_JOB),
    
    /** The email engagement travaux job. */
    EMAIL_ENGAGEMENT_TRAVAUX_JOB(Constantes.EMAIL_ENGAGEMENT_TRAVAUX_JOB);

    /** The nom. */
    private final String nom;

    /**
     * Instantiates a new job enum.
     *
     * @param nom
     *            the nom
     */
    JobEnum(final String nom) {
        this.nom = nom;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Renvoie true si le nom en paramètre existe pour un job de l'enum.
     *
     * @param nom
     *            le nom testé
     * @return true, if successful
     */
    public static boolean jobExists(final String nom) {
        if (StringUtils.isEmpty(nom)) {
            return false;
        }
        for (JobEnum jobEnum : JobEnum.values()) {
            if (nom.equals(jobEnum.getNom())) {
                return true;
            }
        }
        return false;
    }

    /**
     * The Class Constantes.
     */
    public static final class Constantes {

        public static final String CADENCEMENT_MAJ_JOB = "cadencement_maj_job";

        /** The Constant FSO_0028A_JOB. */
        public static final String FSO_0028A_JOB = "fso_0028a_job";

        /** The Constant CEN_0111A_JOB. */
        public static final String CEN_0111A_JOB = "cen_0111a_job";

        /** The Constant EMAIL_RELANCE_JOB. */
        public static final String EMAIL_RELANCE_JOB = "email_relance_job";

        /** The Constant EMAIL_EXPIRATION_JOB. */
        public static final String EMAIL_EXPIRATION_JOB = "email_expiration_job";

        /** The Constant CEN_0072A_JOB. */
        public static final String CEN_0072A_JOB = "cen_0072a_job";

        /** The Constant CEN_0159A_JOB. */
        public static final String CEN_0159A_JOB = "cen_0159a_job";

        /** The Constant FSO0051A_SF_JOB. */
        public static final String FSO0051A_SF_JOB = "fso_0051asf_job";

        /** The Constant FSO0051A_DP_JOB. */
        public static final String FSO0051A_DP_JOB = "fso_0051adp_job";

        /** The Constant FSO0051A_EJ_JOB. */
        public static final String FSO0051A_EJ_JOB = "fso_0051aej_job";

        /** The Constant FEN_0072A_JOB. */
        public static final String FEN_0072A_JOB = "fen_0072a_job";

        /** The Constant FEN_0159A_JOB. */
        public static final String FEN_0159A_JOB = "fen_0159a_job";

        /** The Constant FEN_0111A_JOB. */
        public static final String FEN_0111A_JOB = "fen_0111a_job";

        /** The Constant EMAIL_CONSOMMATION_JOB. */
        public static final String EMAIL_CONSOMMATION_JOB = "email_consommation_job";

        /** The Constant EMAIL_EXPIRATION_CADENCEMENT_JOB. */
        public static final String EMAIL_CADENCEMENT_NON_VALIDE_JOB = "email_cadencement_non_valide_job";
        
        /** The Constant EMAIL_RAPPEL_CONSOMMATION_JOB. */
        public static final String EMAIL_RAPPEL_CONSOMMATION_JOB = "email_rappel_consommation_job";

		/** The Constant PERTE_DOTATTION_JOB. */
		public static final String PERTE_DOTATION_JOB = "perte_dotation_job";

		/** The Constant EMAIL_ENGAGEMENT_TRAVAUX_JOB. */
		public static final String EMAIL_ENGAGEMENT_TRAVAUX_JOB = "email_engagement_travaux_job";

        /**
         * Instantiates a new constantes.
         */
        private Constantes() {
            // Constructeur privé, la classe ne doit pas être instanciée.
        }
    }
}

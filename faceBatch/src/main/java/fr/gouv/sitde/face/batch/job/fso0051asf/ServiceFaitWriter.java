/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051asf;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;

import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * The Class ServiceFaitWriter.
 *
 * @author Atos
 */
public class ServiceFaitWriter implements ItemWriter<ServiceFait> {

    /** The transfert paiement service. */
    @Inject
    private TransfertPaiementService transfertPaiementService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends ServiceFait> items) throws Exception {

        for (ServiceFait serviceFait : items) {
            // save TransfertPaiement
            TransfertPaiement transfertPaiement = serviceFait.getTransfertPaiement();
            if (transfertPaiement != null) {
                this.transfertPaiementService.saveTransfertPaiement(transfertPaiement);
            }

            // save Demande Paiement

            List<DemandePaiement> listDemandePaiement = serviceFait.getListDemandePaiement();
            for (DemandePaiement demandePaiement : listDemandePaiement) {
                this.demandePaiementService.majDemandePaiementCertifiee(demandePaiement.getId());
            }

        }

    }

}

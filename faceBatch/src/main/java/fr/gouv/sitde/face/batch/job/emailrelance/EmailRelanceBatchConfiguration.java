/**
 *
 */
package fr.gouv.sitde.face.batch.job.emailrelance;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

/**
 * The Class MailRelanceBatchConfiguration.
 *
 * @author A754839
 */
@Configuration
@EnableBatchProcessing
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class EmailRelanceBatchConfiguration {

    /** The step builder factory. */
    @Inject
    private StepBuilderFactory stepBuilderFactory;

    /** The job builder factory. */
    @Inject
    private JobBuilderFactory jobBuilderFactory;

    /** The collectivite service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /**
     * DossierSubvention reader.
     *
     * @return the list item reader
     */
    @Bean
    @StepScope
    public ListItemReader<DossierSubvention> dossierSubventionReader() {
        List<DossierSubvention> listeDossierSubvention = this.dossierSubventionService.rechercherDossiersPourMailRelance();
        return new ListItemReader<>(listeDossierSubvention);
    }

    /**
     * Ligne dossier writer.
     *
     * @return the ligne dossier writer
     */
    @Bean
    @StepScope
    public EmailRelanceWriter emailRelanceWriter() {
        return new EmailRelanceWriter();
    }

    /**
     * Step ligne dossier xml.
     *
     * @return the step
     */
    @Bean
    @JobScope
    public Step stepEmailRelance() {
        return this.stepBuilderFactory.get("step_email_relance").<DossierSubvention, DossierSubvention> chunk(1)
                .reader(this.dossierSubventionReader()).writer(this.emailRelanceWriter()).build();
    }

    /**
     * Job.
     *
     * @return the job
     */
    @Bean(name = JobEnum.Constantes.EMAIL_RELANCE_JOB)
    public Job job() {
        return this.jobBuilderFactory.get(JobEnum.MAIL_RELANCE_JOB.getNom()).start(this.stepEmailRelance()).build();
    }
}

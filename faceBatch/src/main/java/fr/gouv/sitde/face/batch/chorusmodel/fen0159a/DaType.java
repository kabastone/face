
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Elements d'identification d'une demande d'achat dans CHORUS
 *
 * <p>
 * Classe Java pour DAType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="DAType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_CHORUS" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char30Type"/&gt;
 *         &lt;element name="POSTE_DA" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char10Type"/&gt;
 *         &lt;element name="SERV_MET" type="{http://www.finances.gouv.fr/aife/chorus/PJ/}Char10Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DAType", propOrder = { "idChorus", "posteDa", "servMet" })
public class DaType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "ID_CHORUS", required = true)
    private String idChorus;
    @XmlElement(name = "POSTE_DA", required = true)
    private String posteDa;
    @XmlElement(name = "SERV_MET", required = true)
    private String servMet;

    /**
     * Obtient la valeur de la propriété idChorus.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdChorus() {
        return this.idChorus;
    }

    /**
     * Définit la valeur de la propriété idChorus.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setIdChorus(String value) {
        this.idChorus = value;
    }

    /**
     * Obtient la valeur de la propriété posteDa.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPosteDa() {
        return this.posteDa;
    }

    /**
     * Définit la valeur de la propriété posteDa.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setPosteDa(String value) {
        this.posteDa = value;
    }

    /**
     * Obtient la valeur de la propriété servMet.
     *
     * @return possible object is {@link String }
     *
     */
    public String getServMet() {
        return this.servMet;
    }

    /**
     * Définit la valeur de la propriété servMet.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setServMet(String value) {
        this.servMet = value;
    }

}

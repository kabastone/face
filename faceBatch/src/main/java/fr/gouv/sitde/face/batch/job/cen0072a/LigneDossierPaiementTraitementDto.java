/**
 *
 */
package fr.gouv.sitde.face.batch.job.cen0072a;

import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Class LigneDossierPaiementTraitementDto.
 */
public class LigneDossierPaiementTraitementDto {

    /** The anomalie. */
    private Anomalie anomalie;

    /** The demande paiement. */
    private DemandePaiement demandePaiement;

    /**
     * Instantiates a new ligne dossier paiement traitement DTO.
     *
     * @param demandePaiement
     *            the demande paiement
     */
    public LigneDossierPaiementTraitementDto(DemandePaiement demandePaiement) {
        this.anomalie = null;
        this.demandePaiement = demandePaiement;
    }

    /**
     * Instantiates a new ligne dossier paiement traitement DTO.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param anomalie
     *            the anomalie
     */
    public LigneDossierPaiementTraitementDto(DemandePaiement demandePaiement, Anomalie anomalie) {
        this.anomalie = anomalie;
        this.demandePaiement = demandePaiement;
    }

    /**
     * Gets the anomalie.
     *
     * @return the anomalie
     */
    public Anomalie getAnomalie() {
        return this.anomalie;
    }

    /**
     * Sets the anomalie.
     *
     * @param anomalie
     *            the new anomalie
     */
    public void setAnomalie(Anomalie anomalie) {
        this.anomalie = anomalie;
    }

    /**
     * Gets the demande paiement.
     *
     * @return the demande paiement
     */
    public DemandePaiement getDemandePaiement() {
        return this.demandePaiement;
    }

    /**
     * Sets the demande subvention.
     *
     * @param demandePaiement
     *            the new demande subvention
     */
    public void setDemandeSubvention(DemandePaiement demandePaiement) {
        this.demandePaiement = demandePaiement;
    }

    /**
     * Checks for anomalie.
     *
     * @return true, if successful
     */
    public boolean hasAnomalie() {
        return this.anomalie != null;
    }
}

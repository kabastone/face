/**
 *
 */
package fr.gouv.sitde.face.batch.commun.batch51a.model;

/**
 * The Class SuiviBatch.
 *
 * @author Atos
 */
public class SuiviBatch {

    /** The numero integration. */
    private Integer numeroIntegration;

    /** The instance. */
    private Batch51AEnum instance;

    /**
     * Gets the numero integration.
     *
     * @return the numeroIntegration
     */
    public Integer getNumeroIntegration() {
        return this.numeroIntegration;
    }

    /**
     * Sets the numero integration.
     *
     * @param numeroIntegration
     *            the numeroIntegration to set
     */
    public void setNumeroIntegration(Integer numeroIntegration) {
        this.numeroIntegration = numeroIntegration;
    }

    /**
     * Gets the single instance of SuiviBatch.
     *
     * @return the instance
     */
    public Batch51AEnum getInstance() {
        return this.instance;
    }

    /**
     * Sets the instance.
     *
     * @param instance
     *            the instance to set
     */
    public void setInstance(Batch51AEnum instance) {
        this.instance = instance;
    }

}

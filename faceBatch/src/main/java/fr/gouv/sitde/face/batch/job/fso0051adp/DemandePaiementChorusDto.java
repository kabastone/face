/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051adp;

/**
 * @author Atos
 *
 */
public class DemandePaiementChorusDto {

    private Integer numeroSequence;

    private String numeroDemandePaiement;

    private String numeroEngagementJuridique;

    private String posteEJ;

    private String numeroServiceFait;

    private String dateComptable;

    /**
     * @return the numeroSequence
     */
    public Integer getNumeroSequence() {
        return this.numeroSequence;
    }

    /**
     * @param numeroSequence
     *            the numeroSequence to set
     */
    public void setNumeroSequence(Integer numeroSequence) {
        this.numeroSequence = numeroSequence;
    }

    /**
     * @return the numeroDemandePaiement
     */
    public String getNumeroDemandePaiement() {
        return this.numeroDemandePaiement;
    }

    /**
     * @param numeroDemandePaiement
     *            the numeroDemandePaiement to set
     */
    public void setNumeroDemandePaiement(String numeroDemandePaiement) {
        this.numeroDemandePaiement = numeroDemandePaiement;
    }

    /**
     * @return the numeroEngagementJuridique
     */
    public String getNumeroEngagementJuridique() {
        return this.numeroEngagementJuridique;
    }

    /**
     * @param numeroEngagementJuridique
     *            the numeroEngagementJuridique to set
     */
    public void setNumeroEngagementJuridique(String numeroEngagementJuridique) {
        this.numeroEngagementJuridique = numeroEngagementJuridique;
    }

    /**
     * @return the posteEJ
     */
    public String getPosteEJ() {
        return this.posteEJ;
    }

    /**
     * @param posteEJ
     *            the posteEJ to set
     */
    public void setPosteEJ(String posteEJ) {
        this.posteEJ = posteEJ;
    }

    /**
     * @return the numeroServiceFait
     */
    public String getNumeroServiceFait() {
        return this.numeroServiceFait;
    }

    /**
     * @param numeroServiceFait
     *            the numeroServiceFait to set
     */
    public void setNumeroServiceFait(String numeroServiceFait) {
        this.numeroServiceFait = numeroServiceFait;
    }

    /**
     * @return the dateComptable
     */
    public String getDateComptable() {
        return this.dateComptable;
    }

    /**
     * @param dateComptable
     *            the dateComptable to set
     */
    public void setDateComptable(String dateComptable) {
        this.dateComptable = dateComptable;
    }

}

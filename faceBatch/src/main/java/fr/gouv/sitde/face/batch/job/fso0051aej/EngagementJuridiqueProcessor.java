/**
 *
 */
package fr.gouv.sitde.face.batch.job.fso0051aej;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.sitde.face.domaine.service.email.EmailProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class EngagementJuridiqueProcessor.
 *
 * @author Atos
 */
public class EngagementJuridiqueProcessor implements ItemProcessor<EngagementJuridiqueDto, EngagementJuridiqueChorus> {

    /** The Constant CLOS. */
    public static final String CLOS = "C";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EngagementJuridiqueProcessor.class);

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The etat demande subvention service. */
    @Inject
    private EtatDemandeSubventionService etatDemandeSubventionService;

    /** The demande prolongation service. */
    @Inject
    private DemandeProlongationService demandeProlongationService;

    /** The etat demande prolongation service. */
    @Inject
    private EtatDemandeProlongationService etatDemandeProlongationService;

    /** The email prolongation service. */
    @Inject
    private EmailProlongationService emailProlongationService;

    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** The batch 51 date format. */
    private static String batch51DateFormat = "ddMMyyyy";

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public EngagementJuridiqueChorus process(EngagementJuridiqueDto item) throws Exception {

        EngagementJuridiqueChorus engagementJuridiqueChorus = new EngagementJuridiqueChorus();

        DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(batch51DateFormat);

        LocalDateTime dateLivraisonDuPoste = null;

        // on analyse la date de livraison du poste
        try {

            dateLivraisonDuPoste = LocalDateTime.of(LocalDate.parse(item.getDateDeLivraisonDuPoste(), dateTimerFormatter), LocalTime.NOON);

        } catch (RuntimeException e) {
            LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.erreur.date.livraison.poste", item.getDateDeLivraisonDuPoste(),
                    item.getPosteDeDocument(), item.getNumeroEngagementJuridique()), e);
            return engagementJuridiqueChorus;
        }

        // RG_SUB_112-02
        // recherche Demande Subvention
        DemandeSubvention demandeSubventionPoste = this.demandeSubventionService.rechercherParEJetPoste(item.getNumeroEngagementJuridique(),
                item.getPosteDeDocument());

        // si non trouvé on cherche par num Ej Quantité et Etat
        if (demandeSubventionPoste == null) {
            demandeSubventionPoste = this.demandeSubventionService.rechercherParEJQuantityEtat(item.getNumeroEngagementJuridique(),
                    item.getQuantiteCommandee());
        }

        // Si non trouvé on ne continue pas
        if (demandeSubventionPoste == null) {
            LOGGER.error(
                    MessageUtils.getMsgValidationMetier("batch51a.poste.non.trouve", item.getPosteDeDocument(), item.getNumeroEngagementJuridique()));
            return engagementJuridiqueChorus;
        }

        // on met a jour si nécessaire
        if (!demandeSubventionPoste.getChorusNumLigneLegacy().equals(item.getPosteDeDocument())) {
            demandeSubventionPoste.setChorusNumLigneLegacy(item.getPosteDeDocument());
            engagementJuridiqueChorus.setDemandeSubventionPoste(demandeSubventionPoste);
        }

        // On recherche la liste des demande de subvention liées au dossier de subvention
        List<DemandeSubvention> listeDemandeSubvention = this.demandeSubventionService
                .rechercherDemandesSubvention(demandeSubventionPoste.getDossierSubvention().getId());

        // on a besoin de pouvoir identifier la demande de subvention poste
        Long demandeSubventionPosteId = demandeSubventionPoste.getId();

        // RG_SUB_112-03
        if (CLOS.equals(item.getStatutCloturePosteEJ())) {

            // Etape 1 : cloture poste
            EtatDemandeSubvention etatCloturee = this.etatDemandeSubventionService
                    .rechercherEtatDemandeSubventionParCode(EtatDemandeSubventionEnum.CLOTUREE.toString());
            demandeSubventionPoste.setEtatDemandeSubvention(etatCloturee);

            // on renseigne dans engagementJuridiqueChorus pour le writer
            engagementJuridiqueChorus.setDemandeSubventionPoste(demandeSubventionPoste);

            // etape 2 : on cloture le dossier si c'est possible
            Boolean allClos = calculerIsAllClos(listeDemandeSubvention, demandeSubventionPosteId);

            // si tous clos on cloture le dossier de subvention
            if (allClos) {
                DossierSubvention dossierSubvention = demandeSubventionPoste.getDossierSubvention();
                dossierSubvention.setEtatDossier(EtatDossierEnum.CLOTURE);
                // on renseigne le dossier de subvention dans engagementJuridiqueChorus pour le writer
                engagementJuridiqueChorus.setDossierSubvention(dossierSubvention);

            } // else rien a faire on ne peut pas cloturer le dossier de subvention

        } else { // si le status chorus n'est pas Cloturé on approuve la demande de subvention Poste

            EtatDemandeSubvention etatApprouvee = this.etatDemandeSubventionService
                    .rechercherEtatDemandeSubventionParCode(EtatDemandeSubventionEnum.APPROUVEE.toString());
            demandeSubventionPoste.setEtatDemandeSubvention(etatApprouvee);
            // on renseigne le dossier de subvention dans engagementJuridiqueChorus pour le writer
            engagementJuridiqueChorus.setDemandeSubventionPoste(demandeSubventionPoste);
        }

        // RG_SUB_112-04
        // Etape 1 mise a jour de la date de livraison chorus
        demandeSubventionPoste.setDateChorusDateLivraison(dateLivraisonDuPoste);
        // on renseigne le dossier de subvention dans engagementJuridiqueChorus pour le writer
        engagementJuridiqueChorus.setDemandeSubventionPoste(demandeSubventionPoste);

        // Etape 2
        // on vérifie l'état des demandes de subvention Approuvée et Attribuee puis la date livraison chorus
        Boolean prolongeable = this.calculerIsProlongeable(dateLivraisonDuPoste, listeDemandeSubvention, demandeSubventionPosteId);

        // on recherche la demande de prolongation
        DemandeProlongation demandeProlongation = this.demandeProlongationService.rechercheParDateAchevementEtDossierSubvention(dateLivraisonDuPoste,
                item.getNumeroEngagementJuridique());

        // si non trouvé on trace mais on continue pour mettre a jour le dossier
        if (demandeProlongation == null) {
            LOGGER.error(MessageUtils.getMsgValidationMetier("batch51a.erreur.demande.prolongation", dateLivraisonDuPoste.toString(),
                    item.getPosteDeDocument(), item.getNumeroEngagementJuridique()));
        }
        // else on continue

        if (prolongeable) { // on peut mettre a jour le dossier et la demande de prolongation
            DossierSubvention dossierSubvention = demandeSubventionPoste.getDossierSubvention();
            dossierSubvention.setDateEcheanceAchevement(dateLivraisonDuPoste);
            // on renseigne le dossier de subvention dans engagementJuridiqueChorus pour le writer
            engagementJuridiqueChorus.setDossierSubvention(dossierSubvention);

            if (demandeProlongation != null) {
                EtatDemandeProlongation etatValidee = this.etatDemandeProlongationService
                        .rechercherParCode(EtatDemandeProlongationEnum.VALIDEE.toString());

                demandeProlongation.setEtatDemandeProlongation(etatValidee);

                // on renseigne la demande de prolongation dans engagementJuridiqueChorus pour le writer
                engagementJuridiqueChorus.setDemandeProlongation(demandeProlongation);
                this.emailProlongationService.envoyerEmailDemandeProlongationAcceptee(demandeProlongation.getId());
            }
        } else { // on ne met pas a jour le dossier

            // on met la demande de prolongation a jour
            if (demandeProlongation != null) {
                EtatDemandeProlongation etatIncoherence = this.etatDemandeProlongationService
                        .rechercherParCode(EtatDemandeProlongationEnum.INCOHERENCE_CHORUS.toString());

                demandeProlongation.setEtatDemandeProlongation(etatIncoherence);
                // on renseigne la demande de prolongation dans engagementJuridiqueChorus pour le writer
                engagementJuridiqueChorus.setDemandeProlongation(demandeProlongation);
            }
        }

        return engagementJuridiqueChorus;
    }

    /**
     * Calculer is all clos.
     *
     * @param listeDemandeSubvention
     *            the liste demande subvention
     * @param demandeSubventionPosteId
     *            the demande subvention poste id
     * @return the boolean
     */
    private static Boolean calculerIsAllClos(List<DemandeSubvention> listeDemandeSubvention, Long demandeSubventionPosteId) {
        Iterator<DemandeSubvention> listeDemandeSubventionIterator = listeDemandeSubvention.iterator();
        Boolean allClos = true;

        // on verifie si toutes les demandes de subvention sont cloturées
        while (listeDemandeSubventionIterator.hasNext()) {
            DemandeSubvention demandeSubvention = listeDemandeSubventionIterator.next();
            if (!demandeSubventionPosteId.equals(demandeSubvention.getId())) {
                allClos = allClos && EtatDemandeSubventionEnum.CLOTUREE.toString().equals(demandeSubvention.getEtatDemandeSubvention().getCode());
            } else { // on le supprimer de la liste, il est deja cloruré (mais non flushé)
                listeDemandeSubventionIterator.remove();
            }
        }
        return allClos;
    }

    /**
     * Calculer is prolongeable.
     *
     * @param dateLivraisonDuPoste
     *            the date livraison du poste
     * @param listeDemandeSubvention
     *            the liste demande subvention
     * @param demandeSubventionPosteId
     *            the demande subvention poste id
     * @return the boolean
     */
    private Boolean calculerIsProlongeable(LocalDateTime dateLivraisonDuPoste, List<DemandeSubvention> listeDemandeSubvention,
            Long demandeSubventionPosteId) {
        Iterator<DemandeSubvention> listeDemandeSubventionIterator = listeDemandeSubvention.iterator();

        Boolean prolongeable = true;

        while (listeDemandeSubventionIterator.hasNext()) {
            DemandeSubvention demandeSubvention = listeDemandeSubventionIterator.next();

            if (!demandeSubventionPosteId.equals(demandeSubvention.getId())
                    && (EtatDemandeSubventionEnum.APPROUVEE.toString().equals(demandeSubvention.getEtatDemandeSubvention().getCode())
                            || EtatDemandeSubventionEnum.ATTRIBUEE.toString().equals(demandeSubvention.getEtatDemandeSubvention().getCode()))) {
                // on test si a la bonne date (sans regarder hh:mm:ss)
                prolongeable = prolongeable
                        && this.timeOperationTransverse.compareEgaliteDates(dateLivraisonDuPoste, demandeSubvention.getDateChorusDateLivraison());
                // else c'est la demande de subvention poste elle est deja a la bonne date ou la demande de subvention n'est pas dans un etat a
                // traiter
            }

        }
        return prolongeable;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.chorusmodel.fen0159a;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Class FEN0159ADTO.
 */
public class FEN0159ADTO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5094030661992704574L;

    /** The fen 0159 a. */
    private FEN0159A fen0159a;

    /** The demande paiement. */
    private DemandePaiement demandePaiement;

    /**
     * Instantiates a new fen0159adto.
     *
     * @param fen0159a
     *            the fen 0159 a
     * @param demandePaiement
     *            the demande paiement
     */
    public FEN0159ADTO(FEN0159A fen0159a, DemandePaiement demandePaiement) {
        super();
        this.fen0159a = fen0159a;
        this.demandePaiement = demandePaiement;
    }

    /**
     * Gets the fen 0159 a.
     *
     * @return the fen0159a
     */
    public FEN0159A getFen0159a() {
        return this.fen0159a;
    }

    /**
     * Sets the fen 0159 a.
     *
     * @param fen0159a
     *            the fen0159a to set
     */
    public void setFen0159a(FEN0159A fen0159a) {
        this.fen0159a = fen0159a;
    }

    /**
     * Gets the demande paiement.
     *
     * @return the demandePaiement
     */
    public DemandePaiement getDemandePaiement() {
        return this.demandePaiement;
    }

    /**
     * Sets the demande paiement.
     *
     * @param demandePaiement
     *            the demandePaiement to set
     */
    public void setDemandePaiement(DemandePaiement demandePaiement) {
        this.demandePaiement = demandePaiement;
    }
}

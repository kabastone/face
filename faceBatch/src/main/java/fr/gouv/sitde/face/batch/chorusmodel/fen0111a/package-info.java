/**
 * Définition des namespaces pour fen0111A.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.finances.gouv.fr/aife/chorus/EJ/",
        xmlns = { @javax.xml.bind.annotation.XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
                @javax.xml.bind.annotation.XmlNs(prefix = "ej", namespaceURI = "http://www.finances.gouv.fr/aife/chorus/EJ/") })
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

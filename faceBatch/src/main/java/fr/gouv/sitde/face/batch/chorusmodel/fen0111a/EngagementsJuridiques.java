
package fr.gouv.sitde.face.batch.chorusmodel.fen0111a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour EngagementsJuridiquesType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="EngagementsJuridiquesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Version"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CodeAppli" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *               &lt;pattern value="[0-9a-zA-Z]*"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="EngagementJuridique" type="{http://www.finances.gouv.fr/aife/chorus/EJ/}EngagementJuridiqueType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EngagementsJuridiquesType", propOrder = { "version", "codeAppli", "engagementJuridiques" })
@XmlRootElement(name = "EngagementsJuridiques")
public class EngagementsJuridiques implements Serializable {

    /** The xsi schema location. */
    @XmlAttribute(name = "xsi:schemaLocation")
    private final String XSI_SCHEMA_LOCATION = "http://www.finances.gouv.fr/aife/chorus/EJ/ FEN0111A/EngagementsJuridiques.xsd";

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Version", required = true)
    private String version;
    @XmlElement(name = "CodeAppli")
    private String codeAppli;
    @XmlElement(name = "EngagementJuridique", required = true)
    private List<EngagementJuridiqueType> engagementJuridiques;

    /**
     * Obtient la valeur de la propriété version.
     *
     * @return possible object is {@link String }
     *
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Définit la valeur de la propriété version.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtient la valeur de la propriété codeAppli.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodeAppli() {
        return this.codeAppli;
    }

    /**
     * Définit la valeur de la propriété codeAppli.
     *
     * @param value
     *            allowed object is {@link String }
     *
     */
    public void setCodeAppli(String value) {
        this.codeAppli = value;
    }

    /**
     * Gets the value of the engagementJuridiques property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the engagementJuridiques property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getEngagementJuridiques().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link EngagementJuridiqueType }
     *
     *
     */
    public List<EngagementJuridiqueType> getEngagementJuridiques() {
        if (this.engagementJuridiques == null) {
            this.engagementJuridiques = new ArrayList<>();
        }
        return this.engagementJuridiques;
    }

    /**
     * @param engagementJuridiques
     *            the engagementJuridiques to set
     */
    public void setEngagementJuridiques(List<EngagementJuridiqueType> engagementJuridiques) {
        this.engagementJuridiques = engagementJuridiques;
    }

}

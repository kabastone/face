/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * @author Atos
 *
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FSO0051AEJJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fso0051aEJLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The FSO0051A EJ Job. */
    @Inject
    @Qualifier(JobEnum.Constantes.FSO0051A_EJ_JOB)
    private Job fso0051AEJJob;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    @BeforeEach
    public void setUpFSO0051AEJJobInputDir() {

        File cheminFichierTest51EJ = new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/fso0051aej/");

        // On copie le contenu des dossiers des différents Input vers le global
        File inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        if (!inputGlobal.exists()) {
            try {
                Files.createDirectories(inputGlobal.toPath());
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

        File[] tableauFichiers = cheminFichierTest51EJ.listFiles();
        for (File file : tableauFichiers) {
            try {
                FileUtils.copyFileToDirectory(file, inputGlobal);
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

    }

    @AfterEach
    public void tearDownFSO0051AEJJobInputDir() {
        File inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());

        if (inputGlobal.exists()) {
            try {
                FileUtils.cleanDirectory(inputGlobal);
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }
    }

    /**
     * Attendu :
     *
     * ligne 1 : rejet dsu_chorus_num_ligne_legacy KO et montant KO Etat KO
     *
     * ligne 2 : rejet dos_chorus_num_ej KO et montant KO et Etat KO
     *
     * ligne 3 : rejet dos_chorus_num_ej KO et montant KO
     *
     * ligne 4 : rejet dsu_chorus_num_ligne_legacy KO et Etat KO
     *
     * ligne 5 : acceptee, dsu_chorus_num_ligne_legacy mis a jour, non cloturable , non prolongeable
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fso0051aej/in_erreurs.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fso0051aej/ex_erreurs.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runErrors() throws Exception {
        this.jobLauncherTestUtils.setJob(this.fso0051AEJJob);

        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        // Attendu : succès
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File inputDir = new File(CHEMIN_REPERTOIRE_INPUT_FSO0051AEJ);

        // on verifie que le dossier d'input existe toujours (pour ne pas provoquer d'exception stupide)
        Assertions.assertTrue(inputDir.isDirectory());

        File[] tableauFichiers = inputDir.listFiles();

        // on verifie que le dossier d'input a ete complètement vidé
        Assertions.assertTrue(tableauFichiers.length == 0);

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fso0051aej/in_nominal.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fso0051aej/ex_nominal.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runNominal() throws Exception {
        this.jobLauncherTestUtils.setJob(this.fso0051AEJJob);

        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        // Attendu : succès
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File inputDir = new File(CHEMIN_REPERTOIRE_INPUT_FSO0051AEJ);

        // on verifie que le dossier d'input existe toujours (pour ne pas provoquer d'exception stupide)
        Assertions.assertTrue(inputDir.isDirectory());

        File[] tableauFichiers = inputDir.listFiles();

        // on verifie que le dossier d'input a ete complètement vidé
        Assertions.assertTrue(tableauFichiers.length == 0);

    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * @author A754839
 *
 */
public class FichierPieceJointeBatch extends AbstractTestBatch {

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FEN0159A. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_FEN0159A = "src/test/resources/upload/upload-test/ANNEE/2019/DOSSIER/900001/DEM-PAI/";

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FEN0159A. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_FEN0111A = "src/test/resources/upload/upload-test/ANNEE/2019/DOSSIER/90000";
    protected static final String FIN_CHEMIN_REPERTOIRE_INPUT_FEN0111A = "/DEM-SUB/";

    protected static final String ID_PADDING = "90000";

    protected static final int ID_END_DPA = 90000;

    private static final String CHEMIN_REPERTOIRE_UPLOAD_SOURCE = "src/test/resources/upload/source/";

    @BeforeAll
    public static void setupFichiers() {
        for (int i = 0; i <= 6; i++) {
            String pathName = CHEMIN_REPERTOIRE_INPUT_FEN0159A + File.separatorChar + ID_PADDING + i;
            createDirectory(pathName);
            String cheminFichier = pathName + File.separator + "fichier_test_1.pdf";
            String cheminFichier2 = pathName + File.separator + "fichier_test_2.pdf";
            try {
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source.pdf"), new File(cheminFichier));
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source_2.pdf"), new File(cheminFichier2));

            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

        for (int i = 0; i <= 3; i++) {
            String pathName = CHEMIN_REPERTOIRE_INPUT_FEN0111A + i + FIN_CHEMIN_REPERTOIRE_INPUT_FEN0111A + File.separatorChar + ID_PADDING + i;
            createDirectory(pathName);
            String cheminFichier = pathName + File.separator + "fichier_test_1.pdf";
            String cheminFichier2 = pathName + File.separator + "fichier_test_2.pdf";
            try {
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source.pdf"), new File(cheminFichier));
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source_2.pdf"), new File(cheminFichier2));

            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }
    }

    /**
     * Gets the fichier transfert from pdf file.
     *
     * @param file
     *            the file
     * @return the fichier transfert from pdf file
     */
    public static FichierTransfert getFichierTransfertFromPdfFile() {
        File file = FileUtils.getFile(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source.pdf");
        FichierTransfert fichierTransfert = new FichierTransfert();
        try {
            fichierTransfert.setBytes(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new TechniqueException(e);
        }
        fichierTransfert.setContentTypeTheorique(ExtensionFichierEnum.PDF.getContentType());
        fichierTransfert.setNomFichier(file.getName());

        return fichierTransfert;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.FichierPieceJointeBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class FEN0111aIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FEN0111aIT extends FichierPieceJointeBatch {

    private static final String CHEMIN_FEN_0111A_OUTPUT = "src/test/resources/batch-output";

    private static final String CHEMIN_FEN_0111A_BENCHMARK = "src/test/resources/batch-fichier-valide/FEN0111A";

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fen0111aXmlLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The fen 0111 a job. */
    @Inject
    @Qualifier(JobEnum.Constantes.FEN_0111A_JOB)
    private Job fen0111aJob;

    /**
     * Run fen 0111 a job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fen0111a/in_documents_pour_fen0111a.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fen0111a/ex_demande_paiement_etat.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runFen0111aJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.fen0111aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File repertoireFEN0111A = new File(FEN0111aIT.CHEMIN_FEN_0111A_OUTPUT);
        File[] files = repertoireFEN0111A.listFiles();

        File repertoireBenchmark = new File(FEN0111aIT.CHEMIN_FEN_0111A_BENCHMARK);
        File[] benchmarks = repertoireBenchmark.listFiles();

        Assertions.assertEquals(benchmarks.length, files.length);

        for (File file : files) {
            File currentBenchmark = this.recupererBenchmark(benchmarks, file);
            Assertions.assertNotNull(currentBenchmark);
        }
    }

    /**
     * @param benchmarks
     * @param file
     * @return
     */
    private File recupererBenchmark(File[] benchmarks, File file) {
        for (File benchmark : benchmarks) {
            if (benchmark.getName().equals(file.getName())) {
                return benchmark;
            }
        }
        return null;
    }
}

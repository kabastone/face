/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class FSO0028AXmlIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FSO0028AXmlIT extends AbstractTestBatch {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FSO0028AXmlIT.class);

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fso0028aXmlLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The cen0072aJob. */
    @Inject
    @Qualifier(JobEnum.Constantes.FSO_0028A_JOB)
    private Job fso008aJob;

    @BeforeAll
    public static void setUpXmlInputDir() {
        LOGGER.debug("Setting up input dir");
        String cheminFichierTest1 = AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_FSO0028A + "/input_fso0028a_test1.xml";
        String cheminFichierTest2 = AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_FSO0028A + "/input_fso0028a_test2.xml";

        try {
            FileUtils.copyFile(new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/fso0028a/input_fso0028a_source1.xml"),
                    new File(cheminFichierTest1));
            FileUtils.copyFile(new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/fso0028a/input_fso0028a_source2.xml"),
                    new File(cheminFichierTest2));
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
    }

    /**
     * Run poc pocXmlJob.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fso0028a/in_demande_paie_avec_anomalie.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fso0028a/ex_fso0028a.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runFso0028aJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.fso008aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

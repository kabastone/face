/**
 *
 */
package fr.gouv.sitde.face.batch.configuration;

import org.slf4j.MDC;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.test.JobLauncherTestUtils;

import fr.gouv.sitde.face.batch.runner.BatchApplication;

/**
 * The Class CustomJobLauncherTestUtils.
 *
 * @author a453029
 */
public class CustomJobLauncherTestUtils extends JobLauncherTestUtils {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.test.JobLauncherTestUtils#launchJob()
     */
    @Override
    public JobExecution launchJob() throws Exception {
        if (this.getJob() != null) {
            MDC.put(BatchApplication.MDC_CLE_JOB, this.getJob().getName());
        }
        return super.launchJob();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.batch.test.JobLauncherTestUtils#launchJob(org.springframework.batch.core.JobParameters)
     */
    @Override
    public JobExecution launchJob(JobParameters jobParameters) throws Exception {
        if (this.getJob() != null) {
            MDC.put(BatchApplication.MDC_CLE_JOB, this.getJob().getName());
        }
        return super.launchJob(jobParameters);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.batch.paiement;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.domaine.service.paiement.DirecteurRepartitionPaiementService;

/**
 * The Class RepartitionPaiementIT.
 *
 * @author A754839
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class RepartitionPaiementIT extends AbstractTestBatch {

    @Inject
    private DirecteurRepartitionPaiementService directeurRepartitionPaiement;

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_une_sub_avance.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_une_sub_avance.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionSubventionUneAvance() throws Exception {
        Long idPaiement = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_une_sub_acompte.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_une_sub_acompte.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionUneSubventionAcompte() throws Exception {
        Long idPaiement = new Long("900002");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_une_sub_juste_deux_acomptes.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_une_sub_juste_deux_acomptes.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionUneSubventionDeuxAcomptes() throws Exception {
        Long idPaiement = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
        Long idPaiement2 = new Long("900002");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement2);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_une_sub_juste_solde.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_une_sub_juste_solde.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionUneSubventionJusteSolde() throws Exception {
        Long idPaiement = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_deux_sub_avance.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_deux_sub_avance.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionDeuxSubventionsAvance() throws Exception {
        Long idPaiement = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_deux_sub_acompte.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_deux_sub_acompte.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionDeuxSubventionsAcompte() throws Exception {
        Long idPaiement = new Long("900002");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_deux_sub_solde.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_deux_sub_solde.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionDeuxSubventionsSolde() throws Exception {
        Long idPaiement = new Long("900003");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_avance.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_avance.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsAvance() throws Exception {
        Long idPaiement = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_1er_acompte.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_1er_acompte.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsPremierAcompte() throws Exception {
        Long idPaiement = new Long("900002");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_2eme_acompte.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_2eme_acompte.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsSecondAcompte() throws Exception {
        Long idPaiement = new Long("900003");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_3eme_acompte.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_3eme_acompte.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsTroisiemeAcompte() throws Exception {
        Long idPaiement = new Long("900004");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_solde.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_solde.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsSolde() throws Exception {
        Long idPaiement = new Long("900005");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_complet.xml" })
    @ExpectedDatabase(value = "/dataset/expected/Paiement/ex_trois_sub_complet.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCalculerRepartitionTroisSubventionsComplet() throws Exception {
        Long idPaiement1 = new Long("900001");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement1);
        Long idPaiement2 = new Long("900002");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement2);
        Long idPaiement3 = new Long("900003");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement3);
        Long idPaiement4 = new Long("900004");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement4);
        Long idPaiement5 = new Long("900005");
        this.directeurRepartitionPaiement.realiserRepartition(idPaiement5);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_zero_avance.xml" })
    public void testCalculerResteAvanceTroisSubventionsZeroTransfert() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAvancePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("10000")));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_une_avance.xml" })
    public void testCalculerResteAvanceTroisSubventionsUnTransfert() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAvancePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("5000")));

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_deux_avances.xml" })
    public void testCalculerResteAvanceTroisSubventionsToutTransferts() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAvancePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("0")));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_zero_acompte_deux_avances.xml" })
    public void testCalculerResteAcomptePaiementPourDossierTroisSubventions() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAcomptePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("80000")));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_un_acompte_deux_avances.xml" })
    public void testCalculerResteAcomptePaiementPourDossierTroisSubventionsUnAcompte() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAcomptePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("48000")));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/Paiement/in_trois_sub_repartition_deux_acomptes_deux_avances.xml" })
    public void testCalculerResteAcomptePaiementPourDossierTroisSubventionsDeuxAcomptes() throws Exception {
        Long idDossier = new Long("900001");
        BigDecimal reste = this.directeurRepartitionPaiement.calculerResteAcomptePaiementPourDossier(idDossier);
        Assertions.assertEquals(0, reste.compareTo(new BigDecimal("40000")));
    }

}

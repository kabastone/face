/**
 *
 */
package fr.gouv.sitde.face.batch.mock;

import java.time.LocalDateTime;

import javax.inject.Named;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import fr.gouv.sitde.face.transverse.time.impl.TimeOperationTransverseImpl;

/**
 * @author A754839
 *
 */
@Profile({ "test", "testpic" })
@Primary
@Named
public class TimeOperationTransverseMock extends TimeOperationTransverseImpl {

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.transverse.util.TimeUtils#getAnneeCourante()
     */
    @Override
    public int getAnneeCourante() {
        return 2019;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.transverse.time.impl.TimeOperationTransverseImpl#now()
     */
    @Override
    public LocalDateTime now() {
        // Retourne la date du 16 Octobre 2019 - 12h00m00s en lieu et place de la date actuelle.
        return LocalDateTime.of(2019, 10, 16, 12, 00, 00);
    }
}

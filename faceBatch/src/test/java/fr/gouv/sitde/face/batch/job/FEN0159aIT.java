/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.FichierPieceJointeBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * The Class FEN0159aIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FEN0159aIT extends FichierPieceJointeBatch {

    private static final String CHEMIN_FEN_0159A_OUTPUT = "src/test/resources/batch-output";

    private static final String CHEMIN_FEN_0159A_BENCHMARK = "src/test/resources/batch-fichier-valide/FEN0159A";

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fen0159aLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The fen 0159 a job. */
    @Inject
    @Qualifier(JobEnum.Constantes.FEN_0159A_JOB)
    private Job fen0159aJob;

    @Inject
    private DemandePaiementService demandePaiementService;

    @Inject
    private DocumentService documentService;

    /**
     * Run fen 0159 a job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fen0159a/in_documents_pour_fen0159a.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fen0159a/ex_demande_paiement_etat.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runFen0159aJob() throws Exception {

        Long idDemandePaiement1 = new Long("900001");
        Long idDemandePaiement2 = new Long("900002");
        Long idDemandePaiement3 = new Long("900003");
        Long idDemandePaiement4 = new Long("900004");
        Long idDemandePaiement5 = new Long("900005");

        // Recherche de la demande de subvention 1.
        DemandePaiement demPaiement1 = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement1);
        // Recherche de la demande de subvention 2.
        DemandePaiement demPaiement2 = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement2);
        // Recherche de la demande de subvention 3.
        DemandePaiement demPaiement3 = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement3);
        // Recherche de la demande de subvention 4.
        DemandePaiement demPaiement4 = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement4);
        // Recherche de la demande de subvention 5.
        DemandePaiement demPaiement5 = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement5);

        FichierTransfert fichierTransfert = FichierPieceJointeBatch.getFichierTransfertFromPdfFile();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.DECISION_ATTRIBUTIVE_PAIE);

        this.documentService.creerDocumentDemandePaiement(demPaiement1, fichierTransfert);
        this.documentService.creerDocumentDemandePaiement(demPaiement2, fichierTransfert);
        this.documentService.creerDocumentDemandePaiement(demPaiement3, fichierTransfert);
        this.documentService.creerDocumentDemandePaiement(demPaiement4, fichierTransfert);
        this.documentService.creerDocumentDemandePaiement(demPaiement5, fichierTransfert);

        this.jobLauncherTestUtils.setJob(this.fen0159aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File repertoireFEN0159A = new File(FEN0159aIT.CHEMIN_FEN_0159A_OUTPUT);
        File[] files = repertoireFEN0159A.listFiles();

        File repertoireBenchmark = new File(FEN0159aIT.CHEMIN_FEN_0159A_BENCHMARK);
        File[] benchmarks = repertoireBenchmark.listFiles();

        Assertions.assertEquals(benchmarks.length, files.length);

        for (File file : files) {
            File currentBenchmark = this.recupererBenchmark(benchmarks, file);
            Assertions.assertNotNull(currentBenchmark);
        }
    }

    /**
     * @param benchmarks
     * @param file
     * @return
     */
    private File recupererBenchmark(File[] benchmarks, File file) {
        for (File benchmark : benchmarks) {
            if (benchmark.getName().equals(file.getName())) {
                return benchmark;
            }
        }
        return null;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.batch.mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domaine.service.subvention.impl.DossierSubventionServiceImpl;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * @author A754839
 *
 */
@Component
@Profile({ "test", "testpic" })
public class DossierSubventionServiceMock extends DossierSubventionServiceImpl {

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);

    @Override
    protected LocalDateTime createDateRelance() {
        return LocalDateTime.of(LocalDate.parse("16/09/2019", formatter), LocalTime.MIDNIGHT);
    }

    @Override
    protected LocalDateTime createDateExpiration() {
        return LocalDateTime.of(LocalDate.parse("16/05/2019", formatter), LocalTime.MIDNIGHT);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class EmailExpirationJobIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class EmailCadencementNonValideJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("emailCadencementNonValideLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The email expiration job. */
    @Inject
    @Qualifier(JobEnum.Constantes.EMAIL_CADENCEMENT_NON_VALIDE_JOB)
    private Job emailExpirationJob;

    /**
     * Test run email expiration job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/cadencement/in_collectivite_expiration.xml" })
    @ExpectedDatabase(value = "/dataset/expected/cadencement/ex_collectivite_expiration.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRunEmailExpirationCadencementJob() throws Exception {

        // Durant ce test, parmi les sept dossiers présents en input, seul quatre seront sélectionnés.
        // Un des dossiers présente déjà un email de relance, tandis que les deux derniers ne sont pas
        // en état de relance.

        this.jobLauncherTestUtils.setJob(this.emailExpirationJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

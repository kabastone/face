/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class EmailConsommationJobIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class CadencementMajJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("cadencementMajLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The email consommation job. */
    @Inject
    @Qualifier(JobEnum.Constantes.CADENCEMENT_MAJ_JOB)
    private Job cadencementMajJob;

    /**
     * Test run email consommation job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/cadencement/in_cadencement_maj.xml" })
    @ExpectedDatabase(value = "/dataset/expected/cadencement/ex_cadencement_maj.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRunCadencementMajJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.cadencementMajJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }

    /**
     * Test run email consommation job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/cadencement/in_cadencement_maj_tous_valide.xml" })
    @ExpectedDatabase(value = "/dataset/expected/cadencement/ex_cadencement_maj_tous_valide.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRunCadencementMajJobPasEmail() throws Exception {

        this.jobLauncherTestUtils.setJob(this.cadencementMajJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }

    /**
     * Test run email consommation job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/cadencement/in_cadencement_maj_col_sans_email.xml" })
    public void testRunCadencementMajJobCollectiviteSansEmail() throws Exception {

        this.jobLauncherTestUtils.setJob(this.cadencementMajJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class EmailExpirationJobIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class EmailRappelConsommationJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("emailRappelConsommationLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The email expiration job. */
    @Inject
    @Qualifier(JobEnum.Constantes.EMAIL_RAPPEL_CONSOMMATION_JOB)
    private Job emailRappelConsJob;

    /**
     * Test run email expiration job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/mail-rappel-consommation/in_collectivite_rappel.xml" })
    @ExpectedDatabase(value = "/dataset/expected/mail-rappel-consommation/ex_collectivite_rappel.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRunEmailRappelConsommationJob() throws Exception {

       
        //Le test va récupérer 2 collectivités sur 3 et enregistrer l'envoi du mail
        this.jobLauncherTestUtils.setJob(this.emailRappelConsJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

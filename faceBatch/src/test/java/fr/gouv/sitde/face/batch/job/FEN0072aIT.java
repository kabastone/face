/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class FEN0072aIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FEN0072aIT extends AbstractTestBatch {

    private final String CHEMIN_FEN_0072A_OUTPUT = "src/test/resources/batch-output";

    private final String CHEMIN_FEN_0072A_BENCHMARK = "src/test/resources/batch-fichier-valide/FEN0072A/FEN0072A_benchmark.xml";

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fen0072aLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The fen 0072 a job. */
    @Inject
    @Qualifier(JobEnum.Constantes.FEN_0072A_JOB)
    private Job fen0072aJob;

    /**
     * Run fen 0072 a job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fen0072a/in_transferts_paiement_pour_fen0072a.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fen0072a/ex_fen0072a.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runFen0072aJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.fen0072aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        File repertoireFEN0072A = new File(this.CHEMIN_FEN_0072A_OUTPUT);
        File[] files = repertoireFEN0072A.listFiles();

        Assertions.assertEquals(1, files.length);
        File benchmark = new File(this.CHEMIN_FEN_0072A_BENCHMARK);
        Assertions.assertTrue(FileUtils.contentEquals(files[0], benchmark));
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }

    /**
     * Run fen 0072 a job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fen0072a/in_rien_pour_fen0072a.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fen0072a/ex_fen0072a_rien.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runFen0072aVideJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.fen0072aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        File repertoireFEN0072A = new File(this.CHEMIN_FEN_0072A_OUTPUT);
        File[] files = repertoireFEN0072A.listFiles();

        Assertions.assertEquals(0, files.length);
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

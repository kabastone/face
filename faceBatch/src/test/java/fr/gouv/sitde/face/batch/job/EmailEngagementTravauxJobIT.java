/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class EmailEngagementTravauxJobIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class EmailEngagementTravauxJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("engagementTravauxLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The email engagement travaux job. */
    @Inject
    @Qualifier(JobEnum.Constantes.EMAIL_ENGAGEMENT_TRAVAUX_JOB)
    private Job engagementTravauxJob;

    /**
     * Test run email engagement travaux job
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/engagement-travaux/in_engagement_travaux.xml" })
    @ExpectedDatabase(value = "/dataset/expected/engagement-travaux/ex_engagement_travaux.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testEngagementTravauxJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.engagementTravauxJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }

   
}

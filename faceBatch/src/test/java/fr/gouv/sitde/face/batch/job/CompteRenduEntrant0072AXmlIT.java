/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class CompteRenduEntrant0072AXmlIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class CompteRenduEntrant0072AXmlIT extends AbstractTestBatch {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CompteRenduEntrant0072AXmlIT.class);

    /** The job launcher test utils. */
    @Inject
    @Qualifier("cen0072aXmlLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The cen0072aJob. */
    @Inject
    @Qualifier(JobEnum.Constantes.CEN_0072A_JOB)
    private Job cen0072aJob;

    @BeforeAll
    public static void setUpXmlInputDir() {
        LOGGER.debug("Setting up input dir");
        String cheminFichierTest1 = AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_CEN0072A + "/input_cen0072a_test1.xml";
        String cheminFichierTest2 = AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_CEN0072A + "/input_cen0072a_test2.xml";

        try {
            FileUtils.copyFile(new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/cen0072a/input_cen0072a_source1.xml"),
                    new File(cheminFichierTest1));
            FileUtils.copyFile(new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/cen0072a/input_cen0072a_source2.xml"),
                    new File(cheminFichierTest2));
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
    }

    /**
     * Run poc pocXmlJob.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/cen0072a/in_demande_paie_avec_anomalie.xml" })
    @ExpectedDatabase(value = "/dataset/expected/cen0072a/ex_demande_paie_avec_anomalie.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runCen0072aJob() throws Exception {

        this.jobLauncherTestUtils.setJob(this.cen0072aJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

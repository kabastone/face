/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;

/**
 * The Class EmailRelanceJobIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class EmailRelanceJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("emailRelanceLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The email relance job. */
    @Inject
    @Qualifier(JobEnum.Constantes.EMAIL_RELANCE_JOB)
    private Job emailRelanceJob;

    /**
     * Test run email relance job.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/mail-relance/in_dossier_date_relance.xml" })
    @ExpectedDatabase(value = "/dataset/expected/mail-relance/ex_dossier_date_relance.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRunEmailRelanceJob() throws Exception {

        // Durant ce test, parmi les sept dossiers présents en input, seul quatre seront sélectionnés.
        // Un des dossiers présente déjà un email de relance, tandis que les deux derniers ne sont pas
        // en état de relance.

        this.jobLauncherTestUtils.setJob(this.emailRelanceJob);
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}

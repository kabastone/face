package fr.gouv.sitde.face.batch.configuration;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

import fr.gouv.sitde.face.batch.job.cen0072a.CEN0072AXmlConfiguration;
import fr.gouv.sitde.face.batch.job.cen0111a.CEN0111AXmlConfiguration;
import fr.gouv.sitde.face.batch.job.cen0159a.CEN0159AXmlConfiguration;
import fr.gouv.sitde.face.batch.job.fso0028a.FSO0028AXmlConfiguration;
import fr.gouv.sitde.face.batch.job.fso0051adp.Fso0051ADPConfiguration;
import fr.gouv.sitde.face.batch.job.fso0051aej.Fso0051AEJConfiguration;
import fr.gouv.sitde.face.batch.job.fso0051asf.Fso0051ASFConfiguration;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class AbstractTestBatch.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BatchTestConfig.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
public class AbstractTestBatch {

    private static final String CHEMIN_REPERTOIRE_INPUT = "src/test/resources/batch-input";

    /** The Constant CHEMIN_REPERTOIRE_INPUT_SOURCE. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_SOURCE = "src/test/resources/batch-source";

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FEN0111A. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_CEN0111A = CHEMIN_REPERTOIRE_INPUT + CEN0111AXmlConfiguration.SOUS_REP_INPUT_CEN0111A;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FEN0072A. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_CEN0072A = CHEMIN_REPERTOIRE_INPUT + CEN0072AXmlConfiguration.SOUS_REP_INPUT_CEN0072A;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_CEN0159A. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_CEN0159A = CHEMIN_REPERTOIRE_INPUT + CEN0159AXmlConfiguration.SOUS_REP_INPUT_CEN0159A;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FSO0051ASF. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_FSO0051ASF = CHEMIN_REPERTOIRE_INPUT + Fso0051ASFConfiguration.SOUS_REP_INPUT_FSO0051ASF;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FSO0051ADP. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_FSO0051ADP = CHEMIN_REPERTOIRE_INPUT + Fso0051ADPConfiguration.SOUS_REP_INPUT_FSO0051ADP;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FSO0051AEJ. */
    protected static final String CHEMIN_REPERTOIRE_INPUT_FSO0051AEJ = CHEMIN_REPERTOIRE_INPUT + Fso0051AEJConfiguration.SOUS_REP_INPUT_FSO0051AEJ;

    /** The Constant CHEMIN_REPERTOIRE_INPUT_FSO0028A. */
    public static final String CHEMIN_REPERTOIRE_INPUT_FSO0028A = CHEMIN_REPERTOIRE_INPUT + FSO0028AXmlConfiguration.SOUS_REP_INPUT_FSO0028A;

    /** The Constant CHEMIN_REPERTOIRE_OUTPUT_FEN0072A. */
    protected static final String CHEMIN_REPERTOIRE_OUTPUT = "src/test/resources/batch-output";

    /** The Constant CHEMIN_REPERTOIRE_OUTPUT_UPLOAD. */
    protected static final String CHEMIN_REPERTOIRE_OUTPUT_UPLOAD = "src/test/resources/upload/upload-test";

    /**
     * Création du repertoire d'input du job.
     */
    @BeforeAll
    public static void setUpInputDir() {
        createDirectory(CHEMIN_REPERTOIRE_INPUT);
        createDirectory(CHEMIN_REPERTOIRE_OUTPUT);

    }

    /**
     * Nettoyage du répertoire d'input du job.
     */
    @AfterAll
    public static void tearDownInputDir() {
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_CEN0111A);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_CEN0072A);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_CEN0159A);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_FSO0051ASF);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_FSO0051ADP);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_FSO0051AEJ);
        deleteDirectory(CHEMIN_REPERTOIRE_INPUT_FSO0028A);
        deleteDirectory(CHEMIN_REPERTOIRE_OUTPUT);
        deleteDirectory(CHEMIN_REPERTOIRE_OUTPUT_UPLOAD);
    }

    @AfterEach
    public void tearDownOutputDir() {
        deleteDirectory(CHEMIN_REPERTOIRE_OUTPUT);
        createDirectory(CHEMIN_REPERTOIRE_OUTPUT);
    }

    /**
     * Create directory.
     *
     * @param cheminRepertoire
     *            the chemin repertoire
     */
    protected static void createDirectory(String cheminRepertoire) {
        File repInputJob = new File(cheminRepertoire);

        // Creation du repertoire
        if (!repInputJob.exists()) {
            repInputJob.mkdirs();
        }
    }

    /**
     * Delete directory.
     *
     * @param cheminRepertoire
     *            the chemin repertoire
     */
    protected static void deleteDirectory(String cheminRepertoire) {
        File repInputJob = new File(cheminRepertoire);

        // Delete du repertoire et de tout fichier qu'il contient
        if (repInputJob.exists()) {
            try {
                FileUtils.forceDelete(repInputJob);
            } catch (IOException e) {
                throw new TechniqueException(e);
            }
        }
    }
}

package fr.gouv.sitde.face.batch.configuration;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

import fr.gouv.sitde.face.batch.mock.DossierSubventionServiceMock;
import fr.gouv.sitde.face.batch.mock.MailSenderServiceMock;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.domain.spi.email.MailSenderService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;

/**
 * The Class BatchTestConfig.
 */
@EnableAutoConfiguration()
@EnableJpaRepositories("fr.gouv.sitde.face.persistance")
@EnableAspectJAutoProxy
@EnableTransactionManagement
@EntityScan("fr.gouv.sitde.face.transverse.entities")
@ComponentScan(basePackages = "fr.gouv.sitde.face")
public class BatchTestConfig {

    /** The data source. */
    @Inject
    private DataSource dataSource;

    /**
     * Mail sender service.
     *
     * @return the mail sender service
     */
    @Bean
    @Primary
    public MailSenderService mailSenderService() {
        return new MailSenderServiceMock();
    }

    /**
     * Dossier subvention service.
     *
     * @return the dossier subvention service
     */
    @Bean
    @Primary
    public DossierSubventionService dossierSubventionService() {
        return new DossierSubventionServiceMock();
    }

    /**
     * Gets the fen0072aLauncher test utils.
     *
     * @return the fen0072aLauncher test utils
     */
    @Qualifier("fen0072aLauncher")
    @Bean
    public JobLauncherTestUtils getJobFEN0072ATestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FEN_0072A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the fen0159aLauncher test utils.
     *
     * @return the fen0159aLauncher test utils
     */
    @Qualifier("fen0159aLauncher")
    @Bean
    public JobLauncherTestUtils getJobFEN0159ATestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FEN_0159A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job cen0111a xml launcher test utils.
     *
     * @return the job cen0111a xml launcher test utils
     */
    @Qualifier("cen0111aXmlLauncher")
    @Bean
    public JobLauncherTestUtils getJobCen0111AXmlLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.CEN_0111A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job cen0159a xml launcher test utils.
     *
     * @return the job cen0159a xml launcher test utils
     */
    @Qualifier("cen0159aXmlLauncher")
    @Bean
    public JobLauncherTestUtils getJobCen0159AXmlLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.CEN_0159A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job cen0072a xml launcher test utils.
     *
     * @return the job cen0072a xml launcher test utils
     */
    @Qualifier("cen0072aXmlLauncher")
    @Bean
    public JobLauncherTestUtils getJobCen0072AXmlLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.CEN_0072A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job fso0028a xml launcher test utils.
     *
     * @return the job fso0028a xml launcher test utils
     */
    @Qualifier("fso0028aXmlLauncher")
    @Bean
    public JobLauncherTestUtils getJobFso0028AXmlLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FSO_0028A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job fen0111a xml launcher test utils.
     *
     * @return the job fen0111a xml launcher test utils
     */
    @Qualifier("fen0111aXmlLauncher")
    @Bean
    public JobLauncherTestUtils getJobFEN0111AXmlLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FEN_0111A_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job mail relance test utils.
     *
     * @return the job mail relance launcher test utils
     */
    @Qualifier("emailRelanceLauncher")
    @Bean
    public JobLauncherTestUtils getJobEmailRelanceLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_RELANCE_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    @Qualifier("emailConsommationLauncher")
    @Bean
    public JobLauncherTestUtils getJobEmailConsommationLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_CONSOMMATION_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job mail relance test utils.
     *
     * @return the job mail relance launcher test utils
     */
    @Qualifier("emailExpirationLauncher")
    @Bean
    public JobLauncherTestUtils getJobEmailExpirationLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_EXPIRATION_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job mail relance test utils.
     *
     * @return the job mail relance launcher test utils
     */
    @Qualifier("emailCadencementNonValideLauncher")
    @Bean
    public JobLauncherTestUtils getJobEmailCadencementNonValideLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_CADENCEMENT_NON_VALIDE_JOB) Job job) {
                super.setJob(job);
            }
        };
    }
    
    @Qualifier("emailRappelConsommationLauncher")
    @Bean
    public JobLauncherTestUtils getJobEmailRappelConsommationLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_RAPPEL_CONSOMMATION_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Gets the job mise a jour cadencement.
     *
     * @return the job mail relance launcher test utils
     */
    @Qualifier("cadencementMajLauncher")
    @Bean
    public JobLauncherTestUtils getJobMiseAJourCadencementLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.CADENCEMENT_MAJ_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    @Qualifier("fso0051aSFLauncher")
    @Bean
    public JobLauncherTestUtils getJobFso0051aSFLauncherTestUtils() {
        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FSO0051A_SF_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    @Qualifier("fso0051aDPLauncher")
    @Bean
    public JobLauncherTestUtils getJobFso0051aDPLauncherTestUtils() {
        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FSO0051A_DP_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    @Qualifier("fso0051aEJLauncher")
    @Bean
    public JobLauncherTestUtils getJobFso0051aEJLauncherTestUtils() {
        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.FSO0051A_EJ_JOB) Job job) {
                super.setJob(job);
            }
        };
    }

    /**
     * Db unit database connection.
     *
     * @return the database data source connection factory bean
     */
    @Bean
    public DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection() {
        DatabaseConfigBean bean = new DatabaseConfigBean();
        bean.setDatatypeFactory(new PostgresqlDataTypeFactory());

        DatabaseDataSourceConnectionFactoryBean dbConnectionFactory = new DatabaseDataSourceConnectionFactoryBean(this.dataSource);
        dbConnectionFactory.setDatabaseConfig(bean);
        return dbConnectionFactory;
    }
    
    @Qualifier("engagementTravauxLauncher")
    @Bean
    public JobLauncherTestUtils getEngagementTravauxLauncherTestUtils() {

        return new CustomJobLauncherTestUtils() {
            @Override
            @Autowired
            public void setJob(@Qualifier(JobEnum.Constantes.EMAIL_ENGAGEMENT_TRAVAUX_JOB) Job job) {
                super.setJob(job);
            }
        };
    }
}

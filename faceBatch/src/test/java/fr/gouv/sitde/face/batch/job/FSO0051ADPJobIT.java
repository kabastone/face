/**
 *
 */
package fr.gouv.sitde.face.batch.job;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.batch.commun.BatchCheminRepertoireProperties;
import fr.gouv.sitde.face.batch.configuration.AbstractTestBatch;
import fr.gouv.sitde.face.batch.runner.JobEnum;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * @author Atos
 *
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class FSO0051ADPJobIT extends AbstractTestBatch {

    /** The job launcher test utils. */
    @Inject
    @Qualifier("fso0051aDPLauncher")
    private JobLauncherTestUtils jobLauncherTestUtils;

    /** The FSO0051A SF Job. */
    @Inject
    @Qualifier(JobEnum.Constantes.FSO0051A_DP_JOB)
    private Job fso0051ADPJob;

    /** The batch chemin repertoire properties. */
    @Inject
    private BatchCheminRepertoireProperties batchCheminRepertoireProperties;

    @BeforeEach
    public void setUpFSO0051ASFJobInputDir() {

        File cheminFichierTestDivers = new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/fso0051aej");
        File cheminFichierTest51DP = new File(AbstractTestBatch.CHEMIN_REPERTOIRE_INPUT_SOURCE + "/fso0051adp/");

        // On copie le contenu des dossiers des différents Input vers le global
        File inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());
        if (!inputGlobal.exists()) {
            try {
                Files.createDirectories(inputGlobal.toPath());
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

        File[] tableauFichiers = cheminFichierTest51DP.listFiles();
        for (File file : tableauFichiers) {
            try {
                FileUtils.copyFileToDirectory(file, inputGlobal);
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

        // pour le fun on copie dans le dossier d'autres fichier mais ils ne seront pas traités par le 51 DP
        tableauFichiers = cheminFichierTestDivers.listFiles();
        for (File file : tableauFichiers) {
            try {
                FileUtils.copyFileToDirectory(file, inputGlobal);
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }

    }

    // netoyage input global après le test
    @AfterEach
    public void tearDownFSO0051AEJJobInputDir() {
        File inputGlobal = new File(this.batchCheminRepertoireProperties.getInput());

        if (inputGlobal.exists()) {
            try {
                FileUtils.cleanDirectory(inputGlobal);
            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }
    }

    /**
     * Attendu : pas de modification de suivi batch, on attend le fichier 4 on fournit le fichier 7
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fso0051adp/in_minimum3.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fso0051adp/ex_nochange3.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runNoFileFound() throws Exception {
        this.jobLauncherTestUtils.setJob(this.fso0051ADPJob);

        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        // Attendu : succès
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File inputDir = new File(CHEMIN_REPERTOIRE_INPUT_FSO0051ADP);

        // on verifie que le dossier d'input existe toujours (pour ne pas provoquer d'exception stupide)
        Assertions.assertTrue(inputDir.isDirectory());

        File[] tableauFichiers = inputDir.listFiles();

        // on verifie que le dossier d'input a ete complètement vidé
        Assertions.assertTrue(tableauFichiers.length == 0);

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/fso0051adp/in_minimum6.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fso0051adp/change6.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void runNominal() throws Exception {
        this.jobLauncherTestUtils.setJob(this.fso0051ADPJob);

        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();

        // Attendu : Erreur a l'execution
        Assertions.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        File inputDir = new File(CHEMIN_REPERTOIRE_INPUT_FSO0051ADP);

        // on verifie que le dossier d'input existe toujours (pour ne pas provoquer d'exception stupide)
        Assertions.assertTrue(inputDir.isDirectory());

        File[] tableauFichiers = inputDir.listFiles();

        // on verifie que le dossier d'input a ete complètement vidé
        Assertions.assertTrue(tableauFichiers.length == 0);

    }
}

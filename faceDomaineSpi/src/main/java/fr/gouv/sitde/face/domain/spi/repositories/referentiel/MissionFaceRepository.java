/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import fr.gouv.sitde.face.transverse.entities.MissionFace;

/**
 * The interface MissionFaceRepository
 *
 * @author A687370
 *
 */
public interface MissionFaceRepository {

    /**
     * Rechercher tout ce que contient MissionFace.
     */
    MissionFace rechercherMissionFace();
}

package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.Set;

import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Interface AnomalieRepository.
 */
public interface AnomalieRepository {

    /**
     * Rechercher anomalie par id.
     *
     * @param idAnomalie
     *            the id anomalie
     * @return the anomalie
     */
    Anomalie rechercherAnomalieParId(Long idAnomalie);

    /**
     * Sauvegarde une anomalie.
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie
     */
    Anomalie modifierAnomalie(Anomalie anomalie);

    /**
     * Sauvegarde une anomalie (sans flush).
     *
     * @param anomalie
     *            the anomalie
     */
    Anomalie creerAnomalie(Anomalie anomalie);

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the sets the
     */
    Set<Anomalie> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement);

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the sets the
     */
    Set<Anomalie> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rendre anomalies demande subvention visibles pour AODE.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     */
    void rendreAnomaliesDemandeSubventionVisiblesPourAODE(Long idDemandeSubvention);

    /**
     * Rendre anomalies demande paiement visibles pour AODE.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     */
    void rendreAnomaliesDemandePaiementVisiblesPourAODE(Long idDemandePaiement);
}

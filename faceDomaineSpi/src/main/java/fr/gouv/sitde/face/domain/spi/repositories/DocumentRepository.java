/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;

/**
 * The Interface DocumentRepository.
 *
 * @author a453029
 */
public interface DocumentRepository {

    /**
     * Creer document.
     *
     * @param document
     *            the document
     * @return the document
     */
    Document creerDocument(Document document);

    /**
     * Modifier document.
     *
     * @param document
     *            the document
     * @return the document
     */
    Document modifierDocument(Document document);

    /**
     * Supprimer document.
     *
     * @param document
     *            the document
     */
    void supprimerDocument(Document document);

    /**
     * Rechercher document demande subvention par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentDemandeSubventionParIdDoc(Long idDocument);

    /**
     * Rechercher document demande prolongation par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentDemandeProlongationParIdDoc(Long idDocument);

    /**
     * Rechercher document demande paiement par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentDemandePaiementParIdDoc(Long idDocument);

    /**
     * Rechercher document courrier departement par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentCourrierAnnuelDepartementParIdDoc(Long idDocument);

    /**
     * Rechercher document ligne dotation departement par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentLigneDotationDepartementParIdDoc(Long idDocument);

    /**
     * Rechercher document modele par id doc.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentModeleParIdDoc(Long idDocument);

    /**
     * Rechercher famille document par id document.
     *
     * @param idDocument
     *            the id document
     * @return the famille document
     */
    FamilleDocument rechercherFamilleDocumentParIdDocument(Long idDocument);

    /**
     * Rechercher documents dotation par id collectivite et annee.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the list
     */
    List<Document> rechercherDocumentsDotationParIdCollectiviteEtAnnee(Long idCollectivite, Integer annee);

    /**
     * Rechercher documents dotation par code departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<Document> rechercherDocumentsDotationParCodeDepartement(String codeDepartement);
}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * Interface du service d'acces au repository DemandeSubvention.
 *
 * @author Atos
 */
public interface DemandeSubventionRepository {

    /**
     * Recherche des demandes de subvention via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandeSubvention> findByDossierSubvention(Long idDossierSubvention);

    /**
     * Obtenir dto pour batch fen 0111 A.
     *
     * @return the list
     */
    List<DemandeSubvention> obtenirDtoPourBatchFen0111A();

    /**
     * Recherche des donnees pour pdf de demande de subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the DemandeSubvention
     */
    DemandeSubvention rechercheDemandeSubventionPourPdfParId(Long idDemandeSubvention);

    /**
     * Recherche demande subvention par id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the demande subvention
     */
    DemandeSubvention rechercheDemandeSubventionParId(Long idDemandeSubvention);

    /**
     * Recherche d'une demande de subvention via son id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the DemandeSubvention
     */
    DemandeSubvention findById(Long idDemandeSubvention);

    /**
     * Find demande principale by dossier subvention.
     *
     * @param idDossierChorus
     *            the id dossier chorus
     * @return the demande subvention
     */
    DemandeSubvention findDemandePrincipaleByDossierChorus(Long idDossierChorus);

    /**
     * Find demande principale by dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the demande subvention
     */
    DemandeSubvention findDemandePrincipaleByIdDossier(Long idDossier);

    /**
     * Mettre A jour demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention mettreAJourDemandeSubvention(DemandeSubvention demande);

    /**
     * Recuperer max chorus num ligne legacy.
     *
     * @param idDossier
     *            the id dossier
     * @return the string
     */
    String recupererMaxChorusNumLigneLegacy(Long idDossier);

    /**
     * Enregistrer.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention enregistrer(DemandeSubvention demande);

    /**
     * Rechercher par E jet poste.
     *
     * @param numeroEJ
     *            the numero EJ
     * @param numeroPoste
     *            the numero poste
     * @return the demande subvention
     */
    DemandeSubvention rechercherParEJetPoste(String numeroEJ, String numeroPoste);

    /**
     * Rechercher par EJ quantity etat.
     *
     * @param numeroEJ
     *            the numero EJ
     * @param plafond
     *            the plafond
     * @return the demande subvention
     */
    DemandeSubvention rechercherParEJQuantityEtat(String numeroEJ, BigDecimal plafond);

    /**
     * Find by dossier subvention ordonnee par date demande.
     *
     * @param idDossier
     *            the id dossier
     * @return the list
     */
    List<DemandeSubvention> findByDossierSubventionOrdonneeParDateDemande(Long idDossier);

    /**
     * Compter nombre demande subvention par dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the integer
     */
    Integer compterNombreDemandeSubventionParDossier(Long idDossier);
}

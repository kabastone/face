package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Interface DepartementRepository.
 */
public interface DepartementRepository {

    /**
     * Rechercher departements.
     *
     * @return the list
     */
    List<Departement> rechercherDepartements();

    /**
     * Rechercher departements avec pagination & pénalités.
     *
     * @return the list
     */
    PageReponse<Departement> rechercherDepartementsPagine(PageDemande pageDemande);

    /**
     * Rechercher departement by id.
     *
     * @param id
     *            the id
     * @return the departement
     */
    Departement rechercherDepartementById(Integer id);

    /**
     * Compter collectivite pour departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the integer
     */
    Integer compterCollectivitePourDepartement(Integer idDepartement);

    /**
     * Gets the code par id.
     *
     * @param idDepartement
     *            the id departement
     * @return the code par id
     */
    String getCodeParId(Integer idDepartement);

    /**
     * Maj departement.
     *
     * @param departement
     *            the departement
     * @return the departement
     */
    Departement majDepartement(Departement departement);

    /**
     * Rechercher departement par code.
     *
     * @param code
     *            the code
     * @return the departement
     */
    Departement rechercherDepartementParCode(String code);

    /**
     * Rechercher departement by id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the departement
     */
    Departement rechercherDepartementByIdCollectivite(Long idCollectivite);
}

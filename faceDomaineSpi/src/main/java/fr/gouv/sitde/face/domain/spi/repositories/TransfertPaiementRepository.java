/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * Interface du service d'acces au repository TransfertPaiement.
 *
 * @author Atos
 */
public interface TransfertPaiementRepository {

    /**
     * Recherche tous les transferts de paiements a l'état 'VERSE'<br/>
     * ou 'DEMANDE' liés à une demande de paiement.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    List<TransfertPaiement> findTransfertPaiementByDossierSubvention(Long idDossierSubvention);

    /**
     * Enregistre en base un transfert paiement.
     *
     * @param transfertPaiement
     *            the transfert paiement
     * @return the transfert paiement
     */
    TransfertPaiement saveTransfertPaiement(TransfertPaiement transfertPaiement);

    /**
     * Rechercher par num S fet legacyet num EJ.
     *
     * @param chorusNumSF
     *            the chorus num SF
     * @param chorusNumLigneLegacy
     *            the chorus num ligne legacy
     * @param chorusNumEJ
     *            the chorus num EJ
     * @return the list
     */
    List<TransfertPaiement> rechercherParNumSFetLegacyetNumEJ(String chorusNumSF, String chorusNumLigneLegacy, String chorusNumEJ);
}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.email;

import java.util.Collection;
import java.util.Map;

import fr.gouv.sitde.face.transverse.entities.Email;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * Service d'envoi d'emails.
 *
 * @author Atos
 *
 */
public interface MailSenderService {

    /**
     * Construire email.
     *
     * @param typeEmail
     *            the type email
     * @param templateEmail
     *            the template email
     * @param variablesContexte
     *            the variables contexte
     * @return the email
     */
    Email construireEmail(final TypeEmailEnum typeEmail, final TemplateEmailEnum templateEmail, final Map<String, Object> variablesContexte);

    /**
     * Construit l'email correspondant aux données en paramètres.
     *
     * @param adressesDestinataires
     *            the adresses destinataires
     * @param typeEmail
     *            the type email
     * @param templateEmail
     *            the template email
     * @param variablesContexte
     *            variables de contexte métier à utiliser pour la fusion.
     * @return the email
     */
    Email construireEmail(final Collection<String> adressesDestinataires, final TypeEmailEnum typeEmail, final TemplateEmailEnum templateEmail,
            final Map<String, Object> variablesContexte);

    /**
     * Envoie via le serveur SMTP l'email correspondant à l'objet métier passé en paramètre.
     *
     * @param email
     *            email
     */
    void envoyerMail(Email email);

    /**
     * Envoyer email contact.
     *
     * @param email
     *            the email
     * @param adresseMailEmetteur
     *            the adresse mail emetteur
     * @return the boolean
     */
    Boolean envoyerEmailContact(final Email email, String adresseMailEmetteur);
}

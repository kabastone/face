/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * @author a453029
 *
 */
public interface DroitAgentRepository {

    /**
     * Rechercher droit agent par utilisateur et collectivite.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @param idCollectivite
     *            the id collectivite
     * @return the droit agent
     */
    DroitAgent rechercherDroitAgentParUtilisateurEtCollectivite(Long idUtilisateur, Long idCollectivite);

    /**
     * Rechercher droits agents par collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<DroitAgent> rechercherDroitsAgentsParCollectivite(Long idCollectivite);

    /**
     * Creer droitAgent.
     *
     * @param droitAgent
     *            the droitAgent
     * @return the droitAgent
     */
    DroitAgent creerDroitAgent(DroitAgent droitAgent);

    /**
     * Modifier droitAgent.
     *
     * @param droitAgent
     *            the droitAgent
     * @return the droitAgent
     */
    DroitAgent modifierDroitAgent(DroitAgent droitAgent);

    /**
     * Supprimer droitAgent.
     *
     * @param droitAgent
     *            the droitAgent
     */
    void supprimerDroitAgent(DroitAgent droitAgent);

}

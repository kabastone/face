/**
 *
 */
package fr.gouv.sitde.face.domain.spi.document;

import java.util.List;
import java.util.Map;

import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;

/**
 * Interface du service de genération de documents PDF.
 *
 * @author Atos
 *
 */
public interface GenerationDocService {

    /**
     * Generer fichier pdf.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @param variablesContexte
     *            the variables contexte
     * @return Le tableau de bytes du fichier PDF généré
     */
    FichierTransfert genererFichierPdf(final TemplateDocumentEnum templateDocEnum, final Map<String, Object> variablesContexte);

    /**
     * Generer fichier pdf.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @param listVariablesContexte
     *            the list of variables contexte
     * @return Le tableau de bytes du fichier PDF généré
     */
    FichierTransfert genererFichierPdf(final TemplateDocumentEnum templateDocEnum, final List<Map<String, Object>> listVariablesContexte);
}

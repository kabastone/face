/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.Adresse;

/**
 * The Interface AdresseRepository.
 *
 * @author Atos
 */
public interface AdresseRepository {

    /**
     * Rechercher par id.
     *
     * @param idAdresse
     *            the id adresse
     * @return the adresse
     */
    Adresse rechercherParId(Long idAdresse);

    /**
     * Modifier adresse.
     *
     * @param adresse
     *            the adresse
     * @return the adresse
     */
    Adresse modifierAdresse(Adresse adresse);

    /**
     * Creer adresse.
     *
     * @param adresse
     *            the adresse
     * @return the adresse
     */
    Adresse creerAdresse(Adresse adresse);

    /**
     * Rechercher par departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the adresse
     */
    Adresse rechercherParDepartement(Integer idDepartement);

}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.tableaudebord;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Interface TableauDeBordMferRepository.
 *
 * @author a453029
 */
public interface TableauDeBordMferRepository {

    /**
     * Rechercher tableau de bord mfer.
     *
     * @return the tableau de bord mfer dto
     */
    TableauDeBordMferDto rechercherTableauDeBordMfer();

    /**
     * Rechercher demandes subvention tdb mfer aqualifier subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAqualifierSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer aqualifier paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAqualifierPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer aaccorder subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAaccorderSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer aaccorder paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAaccorderPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer aattribuer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAattribuerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb mfer asignaler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAsignalerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb mfer asignaler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAsignalerPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes prolongation tdb mfer aprolonger subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeProlongation> rechercherDemandesProlongationTdbMferAprolongerSubvention(PageDemande pageDemande);

}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Programme;

/**
 * The Interface ProgrammeRepository.
 *
 * @author a453029
 */
public interface ProgrammeRepository {

    /**
     * Rechercher programmes sans dotation pour annee en cours.
     *
     * @return the list
     */
    List<Programme> rechercherProgrammesSansDotationPourAnneeEnCours();

}

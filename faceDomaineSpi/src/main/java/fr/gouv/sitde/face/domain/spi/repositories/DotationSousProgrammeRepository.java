/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;

/**
 * The Interface DotationProgrammeRepository.
 */
public interface DotationSousProgrammeRepository {

    /**
     * Rechercher dotations sous programmes sur 4 ans.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    List<DotationSousProgramme> rechercherDotationsSousProgrammesSur4Ans(String codeProgramme);

    /**
     * Rechercher par sous programme par annee en cours.
     *
     * @param sprAbreviation
     *            the spr abreviation
     * @param anneeEnCours
     *            the annee en cours
     * @return the dotation sous programme
     */
    DotationSousProgramme rechercherParSousProgrammeParAnneeEnCours(String sprAbreviation, int anneeEnCours);

    /**
     * Creer dotation sous programme.
     *
     * @param dotationSousProgramme
     *            the dotation sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme creerDotationSousProgramme(DotationSousProgramme dotationSousProgramme);

    /**
     * Rechercher dotation sous programme par id.
     *
     * @param idDotationSousProgramme
     *            the id dotation sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme rechercherDotationSousProgrammeParId(Long idDotationSousProgramme);

    /**
     * Modifier dotation sous programme.
     *
     * @param dotationSousProgramme
     *            the dotation sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme modifierDotationSousProgramme(DotationSousProgramme dotationSousProgramme);

    /**
     * Recuperer dotation sous programme par annee et sous programme.
     *
     * @param annee
     *            the annee
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme recupererDotationSousProgrammeParAnneeEtSousProgramme(int annee, Integer idSousProgramme);
}

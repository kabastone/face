/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Interface CollectiviteRepository.
 *
 * @author Atos
 */
public interface CollectiviteRepository {

    /**
     * Rechercher de toutes les collectivités.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivites();

    /**
     * Rechercher toutes collectivites pour mail.
     *
     * @return the list
     */
    List<Collectivite> rechercherToutesCollectivitesPourMail();

    /**
     * Rechercher par id.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the collectivite
     */
    Collectivite rechercherParId(Long idCollectivite);

    /**
     * Rechercher collectivites par code departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<Collectivite> rechercherCollectivitesParCodeDepartement(String codeDepartement);

    /**
     * Modifier collectivite.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite
     */
    Collectivite modifierCollectivite(Collectivite collectivite);

    /**
     * Creer collectivite.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite
     */
    Collectivite creerCollectivite(Collectivite collectivite);

    /**
     * Rechercher id collectivite par dotation collectivite.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @return the long
     */
    Long rechercherIdCollectiviteParDotationCollectivite(Long idDotationCollectivite);

    /**
     * Rechercher id collectivite par dossier subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the long
     */
    Long rechercherIdCollectiviteParDossierSubvention(Long idDossierSubvention);

    /**
     * Rechercher id collectivite par demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rechercher id collectivite par demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandeProlongation(Long idDemandeProlongation);

    /**
     * Rechercher id collectivite par demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the long
     */
    Long rechercherIdCollectiviteParDemandePaiement(Long idDemandePaiement);

    /**
     * Rechercher id collectivite par document demande subvention.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    Long rechercherIdCollectiviteParDocumentDemandeSubvention(Long idDocument);

    /**
     * Rechercher id collectivite par document demande prolongation.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    Long rechercherIdCollectiviteParDocumentDemandeProlongation(Long idDocument);

    /**
     * Rechercher id collectivite par document demande paiement.
     *
     * @param idDocument
     *            the id document
     * @return the long
     */
    Long rechercherIdCollectiviteParDocumentDemandePaiement(Long idDocument);

    /**
     * Rechercher id collectivites par document courrier departement.
     *
     * @param idDocument
     *            the id document
     * @return the list
     */
    List<Long> rechercherIdCollectivitesParDocumentCourrierDepartement(Long idDocument);

    /**
     * Rechercher id collectivites par document ligne dotation departement.
     *
     * @param idDocument
     *            the id document
     * @return the list
     */
    List<Long> rechercherIdCollectivitesParDocumentLigneDotationDepartement(Long idDocument);

    /**
     * @param siret
     * @return
     */
    boolean verifierSiretUnicite(Collectivite collectivite);

    /**
     * Rechercher collectivite pour cadencement maj.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivitePourCadencementMaj();

    /**
     * Rechercher collectivite pour mail cadencement non valide.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivitePourMailCadencementNonValide();

	List<Collectivite> rechercherCollectivitePourMailDotationNonConsomme();


}

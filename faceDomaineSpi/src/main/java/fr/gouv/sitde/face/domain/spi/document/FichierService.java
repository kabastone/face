/**
 *
 */
package fr.gouv.sitde.face.domain.spi.document;

import java.io.File;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * Interface du service de gestion des fichiers.
 *
 * @author Atos
 *
 */
public interface FichierService {

    /**
     * Enregistrer un fichier.
     *
     * @param document
     *            the document
     * @param fichierTransfert
     *            the fichier transfert
     * @return the file
     */
    File enregistrerFichier(Document document, FichierTransfert fichierTransfert);

    /**
     * Charger un fichier.
     *
     * @param document
     *            the document
     * @return the file
     */
    File chargerFichier(Document document);

    /**
     * Supprimer un fichier.
     *
     * @param document
     *            the document
     * @return the file
     */
    void supprimerFichier(Document document);

    /**
     * Compression du fichier et encodage en base 64.
     *
     * @param doc
     *            the doc
     * @return the byte[]
     */
    byte[] zipperEtEncoderDonneesBase64(Document doc);

    /**
     * Permet de deplacer le contenu d'une demande de subvention vers un autre Dossier de Subvention
     *
     * @param cibleSousDossier
     *            the cible sous dossier
     * @param sourceDossier
     *            the source dossier
     * @param supprimerPerentSource
     *            the supprimer perent source
     */
    void deplacerSousDossierDemandeSubvention(Document cibleSousDossier, Document sourceDossier, boolean supprimerPerentSource);

    /**
     * Encoder donnees base 64.
     *
     * @param doc
     *            the doc
     * @return the byte[]
     */
    byte[] encoderDonneesBase64(Document doc);

}

package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;

public interface CadencementRepository {

    /**
     * @param idCollectivite
     * @param deProjet
     * @return
     */
    List<Cadencement> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet);

    /**
     * Rechercher cadencements pour maj.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<Cadencement> rechercherCadencementsPourMaj(Long idCollectivite);

    /**
     * Enregistrer cadencement.
     *
     * @param cadencement
     *            the cadencement
     * @return the cadencement
     */
    Cadencement enregistrerCadencement(Cadencement cadencement);

    Cadencement findById(Long id);

    Boolean estToutCadencementValidePourCollectivite(Long idCollectivite);

    /**
     * @param codeNumerique
     * @return
     */
    List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche,
            String codeNumerique);

}

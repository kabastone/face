/**
 *
 */
package fr.gouv.sitde.face.domain.spi.chorus;

import java.util.List;

import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;

/**
 * Service d'appel au webservice Chorus.
 *
 * @author Atos
 *
 */
public interface ChorusService {

    /**
     * Appel le webservice chorus.
     *
     * @param codeSociete
     *            the code societe
     * @param idFonctionnel
     *            the id fonctionnel
     * @param idTechnique
     *            the id technique
     * @param organisationAchatFournisseur
     *            the organisation Achat Fournisseur
     * @param domaineCommercialeClient
     *            the domaine Commerciale Client
     * @return the list CollectiviteChorus
     */
    List<CollectiviteChorus> rechercherCollectivites(String codeSociete, String idFonctionnel, String idTechnique,
            String organisationAchatFournisseur, String domaineCommercialeClient);

}

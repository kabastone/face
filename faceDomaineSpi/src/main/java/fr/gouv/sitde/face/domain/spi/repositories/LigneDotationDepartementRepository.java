/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Interface LigneDotationDepartementRepository.
 */
public interface LigneDotationDepartementRepository {

    /**
     * Compter dotation annuelle notifiee annee en cours.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @return the integer
     */
    /*
     * Compter les LigneDotationDepartement
     *
     * pour lesquelles DateEnvoi non null
     *
     * pour lesquelles DotationDepartement.Type = "Annuelle"
     *
     * pour lesquelles DotationProgramme.Annee = années en cours
     */
    Integer compterDotationAnnuelleNotifieeAnneeEnCours(int anneeEnCours);

    /**
     * Calculer dotation sans annuelle.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @param sprAbreviation
     *            the spr abreviation
     * @return the big decimal
     */
    BigDecimal calculerDotationSansAnnuelle(int anneeEnCours, String sprAbreviation);

    /**
     * Gets the dotation sous programme.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @param sprAbreviation
     *            the spr abreviation
     * @return the dotation sous programme
     */
    BigDecimal getDotationSousProgramme(int anneeEnCours, String sprAbreviation);

    /**
     * Raz dotation annuelles.
     *
     * @param anneeEnCours
     *            the annee en cours
     */
    void razDotationAnnuelles(int anneeEnCours);

    /**
     * Ajouter ligne dotation departement.
     *
     * @param ligneDotationDepartement
     *            the ligne dotation departement
     * @return the ligne dotation departement
     */
    LigneDotationDepartement ajouterLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement);

    /**
     * Rechercher par departement en preparation.
     *
     * @param idDepartement
     *            the id departement
     * @param LocalDateTime
     *            the offset date time
     * @return the list
     */
    void majDateEnvoiLigneDotationDepartement(Integer idDepartement, LocalDateTime LocalDateTime);

    /**
     * Rechercher par departement et type pour annexe.
     *
     * @param idDepartement
     *            the id departement
     * @param typeDotationDepartementEnum
     *            the type dotation departement enum
     * @param annee
     *            the annee
     * @return the list
     */
    List<LigneDotationDepartement> rechercherParDepartementEtTypePourAnnexe(Integer idDepartement,
            TypeDotationDepartementEnum typeDotationDepartementEnum, Integer annee);

    /**
     * Rechercher par type pour annexe.
     *
     * @param typeDotationDepartementEnum
     *            the type dotation departement enum
     * @param annee
     *            the annee
     * @return the list
     */
    List<LigneDotationDepartement> rechercherParTypePourAnnexe(TypeDotationDepartementEnum typeDotationDepartementEnum, Integer annee);

    /**
     * Maj ligne dotation departement.
     *
     * @param ligne
     *            the ligne
     * @return the ligne dotation departement
     */
    LigneDotationDepartement majLigneDotationDepartement(LigneDotationDepartement ligne);

    /**
     * Rechercher Numero ordre suivant.
     *
     * @param idDepartement
     *            the id departement
     * @return the short
     */
    short rechercherNumeroOrdreSuivant(Integer idDepartement);

    /**
     * Lister les lignes de dotation en préparation d'un département donné pour l'année en cours.
     *
     * @param annee
     *            the annee
     * @param idDepartement
     *            the id departement
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleEnPreparation(Integer annee, Integer idDepartement);

    /**
     * Lister les lignes de dotation notifiées d'un département donné pour l'année en cours.
     *
     * @param annee
     *            the annee
     * @param idDepartement
     *            the id departement
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleNotifiees(Integer annee, Integer idDepartement);

    /**
     * Supprimer une ligne de dotation départementale.
     *
     * @param idLigneDotationDepartementale
     */
    void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale);

    /**
     * Rechercher lignes dotation departementale.
     *
     * @param plafondAide
     *            the plafond aide
     * @param idSousProgramme
     *            the id sous programme
     * @param codeTypeDotationDepartement
     *            the code type dotation departement
     * @return the long
     */
    Long rechercherLignesDotationDepartementale(LocalDateTime dateAccord);

    /**
     * Rechercher ligne par id.
     *
     * @param idLigne
     *            the id ligne
     * @return the ligne dotation departement
     */
    LigneDotationDepartement rechercherLigneParId(Long idLigne);

    /**
     * Recupere document notification par departement et date envoi.
     *
     * @param id
     *            the id
     * @param dateEnvoi
     *            the date envoi
     * @return the document
     */
    Document recupereDocumentNotificationParDepartementEtDateEnvoi(Integer id, LocalDateTime dateEnvoi);

    /**
     * Rechercher lignes pertes et renonciations.
     *
     * @param anneeCourante
     *            the annee courante
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciations(String codeProgramme, Integer anneeCourante);

    /**
     * Rechercher lignes pertes et renonciations.
     *
     * @param anneeCourante
     *            the annee courante
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciationsParIdCollectivite(Long idCollectivite, Integer anneeCourante);

}

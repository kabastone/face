/**
 *
 */
package fr.gouv.sitde.face.domain.spi.document;

import java.math.BigDecimal;

/**
 * The Interface ConversionNombreService.
 */
public interface ConversionNombreService {

    /**
     * Conversion big decimal en toutes lettres.
     *
     * @param nombre
     *            the nombre
     * @return the string
     */
    String conversionBigDecimalEnToutesLettres(BigDecimal nombre);

    /**
     * Conversion big decimal en kilo euros.
     *
     * @param nombre
     *            the nombre
     * @return the big decimal
     */
    BigDecimal conversionBigDecimalEnKiloEuros(BigDecimal nombre);

    /**
     * Formatage chorus montant.
     *
     * @param montant
     *            the montant
     * @return the string
     */
    String formatageChorusMontant(BigDecimal montant);
}

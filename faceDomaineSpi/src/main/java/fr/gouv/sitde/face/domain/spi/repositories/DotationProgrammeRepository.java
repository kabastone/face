/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.DotationProgramme;

/**
 * The Interface DotationProgrammeRepository.
 */
public interface DotationProgrammeRepository {

    /**
     * Creer dotation programme.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme
     */
    DotationProgramme creerDotationProgramme(DotationProgramme dotationProgramme);

    /**
     * Rechercher dotation programme par id.
     *
     * @param idDotationProgramme
     *            the id dotation programme
     * @return the dotation programme
     */
    DotationProgramme rechercherDotationProgrammeParId(Long idDotationProgramme);

    /**
     * Modifier dotation programme.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme
     */
    DotationProgramme modifierDotationProgramme(DotationProgramme dotationProgramme);

}

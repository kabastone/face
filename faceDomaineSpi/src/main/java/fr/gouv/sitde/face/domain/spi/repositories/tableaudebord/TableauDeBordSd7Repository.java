/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.tableaudebord;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Interface TableauDeBordSd7Repository.
 *
 * @author a453029
 */
public interface TableauDeBordSd7Repository {

    /**
     * Rechercher tableau de bord sd 7.
     *
     * @return the tableau de bord sd 7 dto
     */
    TableauDeBordSd7Dto rechercherTableauDeBordSd7();

    /**
     * Rechercher demandes subvention tdb sd 7 acontroler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AcontrolerSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb sd 7 acontroler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AcontrolerPaiement(PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb sd 7 atransferer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AtransfererSubvention(PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb sd 7 atransferer paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AtransfererPaiement(PageDemande pageDemande);
}

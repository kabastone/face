package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

/**
 * Interface du service d'acces au repository des transfert de fongibilite.
 *
 * @author Atos
 */
public interface TransfertFongibiliteRepository {

    TransfertFongibilite enregistrer(TransfertFongibilite transfertFongibilite);

    /**
     * @param idCollectivite
     * @param annee
     * @return
     */
    List<TransfertFongibilite> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee);

    /**
     * @param annee
     * @return
     */
    List<TransfertFongibilite> rechercherTransfertsParAnnee(Integer annee);

}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.Email;

/**
 * Interface du service d'acces au repository Email.
 *
 * @author a453029
 */
public interface EmailRepository {

    /**
     * Creer email.
     *
     * @param email
     *            the email
     * @return the email
     */
    Email creerEmail(Email email);
}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.time.LocalDateTime;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;

/**
 * The Interface DossierSubventionRepository.
 */
public interface DossierSubventionRepository {

    /**
     * Find dossier subvention by id.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the dossier subvention
     */
    DossierSubvention findDossierSubventionById(Long idDossierSubvention);

    /**
     * Rechercher dossiers par criteres.
     *
     * @param criteresRecherche
     *            the criteres recherche
     * @return the page reponse
     */
    PageReponse<DossierSubvention> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche);

    /**
     * Find dossier subvention par numero.
     *
     * @param numeroDossier
     *            the numero dossier
     * @return the dossier subvention
     */
    DossierSubvention findDossierSubventionParNumero(String numeroDossier);

    /**
     * Find dossier subvention pour mail relance.
     *
     * @param dateRelanceMail
     *            the date relance mail
     * @return the list
     */
    List<DossierSubvention> findDossierSubventionPourMailRelance(LocalDateTime dateRelanceMail);

    /**
     * Find dossier subvention pour mail expiration.
     *
     * @param dateExpirationMail
     *            the date expiration mail
     * @return the list
     */
    List<DossierSubvention> findDossierSubventionPourMailExpiration(LocalDateTime dateExpirationMail);

    /**
     * Enregistrer.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention
     */
    DossierSubvention enregistrer(DossierSubvention dossierSubvention);

    /**
     * Checks if is numero dossier unique.
     *
     * @param idDossier
     *            the id dossier
     * @param numDossier
     *            the num dossier
     * @return true, if is numero dossier unique
     */
    boolean isNumeroDossierUnique(Long idDossier, String numDossier);

    /**
     * Find dossier subvention by id chorus ae.
     *
     * @param idAe
     *            the id ae
     * @return the dossier subvention
     */
    DossierSubvention findDossierSubventionByIdChorusAe(Long idAe);

    /**
     * Supprimer dossier subvention par id.
     *
     * @param idDossier
     *            the id dossier
     */
    void supprimerDossierSubventionParId(Long idDossier);

    /**
     * Recherche un dossier par criteres discriminants lors de la creation d'une demande de subvention.
     *
     * @param annee the annee
     * @param idCollectivite the id collectivite
     * @param idSousProgramme the id sous programme
     * @return the list
     */
    List<DossierSubvention> rechercherDossierParAnneeCollectiviteEtSousProgramme(int annee, Long idCollectivite, Integer idSousProgramme);

    /**
     * Rechercher dossiers par criteres.
     *
     * @param criteresRecherche the criteres recherche
     * @return the page reponse
     */
    PageReponse<DossierSubvention> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche);

    /**
     * Recuperer liste numero dossier par dotation collectivite.
     *
     * @param dossierSubvention the dossier subvention
     * @return the list
     */
    List<String> recupererListeNumeroDossierParDotationCollectivite(DossierSubvention dossierSubvention);

	/**
	 * Rechercher dossier subvention cadencement defaut.
	 *
	 * @param codeProgramme the code programme
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionCadencementDefaut(String codeProgramme );

	
	/**
	 * Rechercher dossier subvention en retard.
	 *
	 * @param codeProgramme the code programme
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionEnRetard(String codeProgramme);

	/**
	 * Rechercher dossier subvention pour mail engagement travaux.
	 *
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionPourMailEngagementTravaux();

}

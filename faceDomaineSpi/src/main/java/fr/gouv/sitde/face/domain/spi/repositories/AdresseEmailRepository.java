/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * Interface du service d'acces au repository des adresses e-mails pour la liste de diffusion.
 *
 * @author Atos
 */
public interface AdresseEmailRepository {
    /**
     * Pour la liste de diffusion : recherche d'une adresse e-mail par id.
     *
     * @param id the id
     * @return the adresse email
     */
    AdresseEmail rechercherAdresseEmail(Long id);

    /**
     * Pour la liste de diffusion : recherche toutes les adresses e-mail d'une collectivité.
     *
     * @param idCollectivite the id collectivite
     * @return the list
     */
    List<AdresseEmail> rechercherAdressesEmail(Long idCollectivite);

    /**
     * Suppression d'une adresse e-mail par id.
     *
     * @param id the id
     */
    void supprimerAdresseEmail(Long id);

    /**
     * Ajout de l'e-mail à la liste de diffusion de la collectivité.
     *
     * @param adresseEmail the adresse email
     * @param idCollectivite the id collectivite
     * @return the adresse email dto
     */
    AdresseEmail ajouterAdresseEmail(AdresseEmail adresseEmail);
}

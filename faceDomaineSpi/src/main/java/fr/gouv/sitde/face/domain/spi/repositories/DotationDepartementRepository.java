/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;

/**
 * The Interface DotationDepartementRepository.
 *
 * @author Atos
 */
public interface DotationDepartementRepository {

    /**
     * Enregistrer.
     *
     * @param dotationDepartement
     *            the dotation departement
     * @return the dotation departement
     */
    DotationDepartement enregistrer(DotationDepartement dotationDepartement);

    /**
     * Rechercher tous les éléments pour les départements donnés.
     *
     * @return the list
     */
    List<DotationDepartement> rechercherTousPourDepartements(List<Departement> listeDepartements);

    /**
     * Rechercher dotation departement trois criteres.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @param codeDomaineFonctionnel
     *            the code domaine fonctionnel
     * @return the dotation departement
     */
    DotationDepartement rechercherDotationDepartementTroisCriteres(Integer annee, String departementCode, String codeDomaineFonctionnel);

    /**
     * Ligne dotation departement en preparation existe.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @return the boolean
     */
    Boolean ligneDotationDepartementEnPreparationExiste(Integer annee, String departementCode);

    /**
     * Ligne dotation departement notifiee existe.
     *
     * @param annee
     *            the annee
     * @param departementCode
     *            the departement code
     * @return the boolean
     */
    Boolean ligneDotationDepartementNotifieeExiste(Integer annee, String departementCode);

    /**
     * Ajouter dotation departement.
     *
     * @param dotationDepartement
     *            the dotation departement
     * @return the dotation departement
     */
    DotationDepartement ajouterDotationDepartement(DotationDepartement dotationDepartement);

    /**
     * Rechercher par id.
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @return the dotation departement
     */
    DotationDepartement rechercherParId(Long dotationDepartementId);

    /**
     * Rechercher dotation departement pour repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @param codeDomaineFonctionnel
     *            the code domaine fonctionnel
     * @return the dotation departement
     */
    DotationDepartement rechercherDotationDepartementPourRepartition(String codeDepartement, String codeDomaineFonctionnel);

    /**
     * Rechercher dotation departement par id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation departement
     */
    DotationDepartement rechercherDotationDepartementParIdCollectivite(Long idCollectivite, Integer idSousProgramme);

    /**
     * Creer.
     *
     * @param dotationDepartement
     *            the dotation departement
     * @return the dotation departement
     */
    DotationDepartement creer(DotationDepartement dotationDepartement);
}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;

/**
 * The Interface EtatDemandeProlongationRepository.
 *
 * @author Atos
 */
public interface EtatDemandeProlongationRepository {
    
    /**
     * Find par code.
     *
     * @param codeEtatDemandeProlongation the code etat demande prolongation
     * @return the etat demande prolongation
     */
    EtatDemandeProlongation findParCode(String codeEtatDemandeProlongation);
}

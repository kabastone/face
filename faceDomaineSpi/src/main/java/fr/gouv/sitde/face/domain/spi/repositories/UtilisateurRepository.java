/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * Interface du service d'acces au repository Utilisateur.
 *
 * @author a453029
 */
public interface UtilisateurRepository {

    /**
     * Rechercher par id.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @return the utilisateur
     */
    Utilisateur rechercherParId(Long idUtilisateur);

    /**
     * Rechercher tous.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<Utilisateur> rechercherTous(PageDemande pageDemande);

    /**
     * Rechercher par email.
     *
     * @param email
     *            the email
     * @return the utilisateur
     */
    Utilisateur rechercherParEmail(String email);

    /**
     * Creer utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur
     */
    Utilisateur creerUtilisateur(Utilisateur utilisateur);

    /**
     * Modifier utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur
     */
    Utilisateur modifierUtilisateur(Utilisateur utilisateur);

    /**
     * Supprimer utilisateur.
     *
     * @param idUtilisateur
     *            the id utilisateur
     */
    void supprimerUtilisateur(Long idUtilisateur);
}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Interface DotationCollectiviteRepository.
 */
public interface DotationCollectiviteRepository {

    /**
     * Maj dotation collectivite.
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @return the dotation collectivite
     */
    DotationCollectivite majDotationCollectivite(DotationCollectivite dotationCollectivite);

    /**
     * Rechercher dotations collectivites par id departement et abreviation sous programme.
     *
     * @param codeDepartement
     *            the code departement
     * @param sprEnum
     *            the abreviation du sous programme
     * @param anneeEnCours
     *            the annee en cours
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(String codeDepartement, String sprEnum,
            Integer anneeEnCours);

    /**
     * Rechercher dotation collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @param annee
     *            the annee
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectivite(Long idCollectivite, Integer idSousProgramme, Integer annee);

    /**
     * Rechercher DT opar id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the list
     */
    List<DotationCollectiviteParSousProgrammeDto> rechercherDTOparIdCollectivite(Long idCollectivite, Integer annee);

    /**
     * Verifier existence demande subvention.
     *
     * @param id
     *            the id
     * @param anneeEnCours
     *            the annee en cours
     * @return true, if successful
     */
    boolean verifierExistenceDemandeSubvention(Long id, Integer anneeEnCours);

    /**
     * Rechercher dotation collectivite par id.
     *
     * @param id
     *            the id
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectiviteParId(Long id);

    /**
     * Rechercher dotation collectivite simple par id.
     *
     * @param id
     *            the id
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectiviteSimpleParId(Long id);

    /**
     * Rechercher dotations collectivites par id collectivite et annee et code numerique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @param codeNumerique
     *            the code numerique
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(Long idCollectivite, Integer annee,
            String codeNumerique);

    /**
     * Recuperer dossiers par dotation collectivite.
     *
     * @param id
     *            the id
     * @return the list
     */
    List<DossierSubvention> recupererDossiersParDotationCollectivite(Long id);

    /**
     * Creer.
     *
     * @param dco
     *            the dco
     * @return the dotation collectivite
     */
    DotationCollectivite creer(DotationCollectivite dco);

    /**
     * Rechercher dotation collectivite par id collectivite.
     *
<<<<<<< HEAD
     * @param idCollectivite
     *            the id collectivite
     * @param anneeEnCours
     *            the annee en cours
=======
     * @param idCollectivite the id collectivite
     * @param anneeEnCours the annee en cours
>>>>>>> 3b47b328... #173 RG_DEL_006 gestion envoi mail apres perte dotation
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationCollectiviteParIdCollectivite(Long idCollectivite, Integer anneeEnCours);

    /**
     * Checks if is est unique subvention en cours par sous programme.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param descriptionSousProgramme
     *            the description sous programme
     * @return true, if is est unique subvention en cours par sous programme
     */
    boolean isEstUniqueSubventionEnCoursParSousProgramme(Long idDemandeSubvention, Long idDotationCollectivite, String descriptionSousProgramme);
	/**
	 * Rechercher perte dotation collectivite pour batch.
	 * @param anneEnCours 
	 *
	 * @return the list
	 */
	List<DotationCollectivite> rechercherPerteDotationCollectivitePourBatch(Integer anneEnCours);
}

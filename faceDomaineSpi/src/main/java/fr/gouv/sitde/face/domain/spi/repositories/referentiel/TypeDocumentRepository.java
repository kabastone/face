/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import fr.gouv.sitde.face.transverse.entities.TypeDocument;

/**
 * The Interface TypeDocumentRepository.
 *
 * @author a453029
 */
public interface TypeDocumentRepository {

    /**
     * Rechercher type document par id codife.
     *
     * @param idCodifie
     *            the id codifie
     * @return the type document
     */
    TypeDocument rechercherTypeDocumentParIdCodifie(Integer idCodifie);

}

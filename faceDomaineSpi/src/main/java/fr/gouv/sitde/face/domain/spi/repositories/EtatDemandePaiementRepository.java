/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;

/**
 * Interface du service d'acces au repository EtatDemandePaiement.
 *
 * @author Atos
 */
public interface EtatDemandePaiementRepository {

    /**
     * Recherche d'un état de demande de paiement par code.
     *
     * @param codeEtatdemande the code etatdemande
     * @return the etat demande paiement
     */
    EtatDemandePaiement findByCode(String codeEtatdemande);
}

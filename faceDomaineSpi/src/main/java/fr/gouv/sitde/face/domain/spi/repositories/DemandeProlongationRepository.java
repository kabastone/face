/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.time.LocalDateTime;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;

/**
 * Interface du service d'acces au repository DemandeProlongation.
 *
 * @author Atos
 */
public interface DemandeProlongationRepository {

    /**
     * Recherche des demandes de prolongation via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    List<DemandeProlongation> findByDossierSubvention(Long idDossierSubvention);

    /**
     * Rechercher demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation rechercherDemandeProlongation(Long idDemandeProlongation);

    /**
     * Recherche par date achevement et dossier subvention.
     *
     * @param dateLivraisonDuPoste
     *            the date livraison du poste
     * @param numeroEJ
     *            the numero EJ
     * @return the demande prolongation
     */
    DemandeProlongation rechercheParDateAchevementEtDossierSubvention(LocalDateTime dateLivraisonDuPoste, String numeroEJ);

    /**
     * Mettre A jour demande prolongation.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation mettreAJourDemandeProlongation(DemandeProlongation demandeProlongation);

    /**
     * Enregistrer.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation enregistrer(DemandeProlongation demandeProlongation);

}

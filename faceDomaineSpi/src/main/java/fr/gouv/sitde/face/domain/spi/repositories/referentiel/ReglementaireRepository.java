/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import fr.gouv.sitde.face.transverse.entities.Reglementaire;

/**
 * The Interface SousProgrammeRepository.
 *
 * @author A687370
 *
 */
public interface ReglementaireRepository {

    /**
     * Rechercher ce que contient reglementaire pour la date indiquée.
     *
     * @return Le Reglementaire en Optionel
     */
    Reglementaire rechercherReglementaire(Integer anneeMiseEnApplication);
}

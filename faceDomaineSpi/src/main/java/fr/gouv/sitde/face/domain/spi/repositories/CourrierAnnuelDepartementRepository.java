package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;

/**
 * The Interface CourrierAnnuelDepartementRepository.
 */
public interface CourrierAnnuelDepartementRepository {

    /**
     * Creer courrier annuel departement.
     *
     * @param courrierAnnuelDepartement
     *            the courrier annuel departement
     * @return the courrier annuel departement
     */
    CourrierAnnuelDepartement creerCourrierAnnuelDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement);

    /**
     * Rechercher courrier annuel departement par code departement et annee.
     *
     * @param codeDepartement
     *            the code departement
     * @param annee
     *            the annee
     * @return the courrier annuel departement
     */
    CourrierAnnuelDepartement rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(String codeDepartement, Integer annee);

}

/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Interface FluxChorusRepository.
 */
public interface FluxChorusRepository {

    /**
     * Rechercher flux chorus.
     *
     * @return the flux chorus
     */
    FluxChorus rechercherFluxChorus();

}

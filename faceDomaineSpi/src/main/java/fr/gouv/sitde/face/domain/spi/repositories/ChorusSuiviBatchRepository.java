/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Interface ChorusSuiviBatchRepository.
 *
 * @author Atos
 */
public interface ChorusSuiviBatchRepository {

    /**
     * Lire suivi batch.
     *
     * @return the chorus suivi batch
     */
    ChorusSuiviBatch lireSuiviBatch();

    /**
     * Mettre A jour suivi batch.
     *
     * @param chorusSuiviBatch
     *            the chorus suivi batch
     * @return the chorus suivi batch
     */
    ChorusSuiviBatch mettreAJourSuiviBatch(ChorusSuiviBatch chorusSuiviBatch);

}

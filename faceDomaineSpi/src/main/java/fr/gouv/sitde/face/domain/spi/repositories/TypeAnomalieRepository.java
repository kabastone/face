/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;

/**
 * @author A754839
 *
 */
public interface TypeAnomalieRepository {

    /**
     * Rechercher par code.
     *
     * @param code
     *            the code
     * @return the type anomalie
     */
    TypeAnomalie rechercherParCode(String code);

    /**
     * Rechercher tous les types d'anomalie : pour une demande de paiement.
     *
     * @return the list
     */
    List<TypeAnomalie> rechercherTousPourDemandePaiement();

    /**
     * Rechercher tous les types d'anomalie : pour demande subvention.
     *
     * @return the list
     */
    List<TypeAnomalie> rechercherTousPourDemandeSubvention();
}

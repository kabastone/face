/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * The Interface DemandePaiementRepository.
 */
public interface DemandePaiementRepository {

    /**
     * Rechercher par id.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercherParId(Long idDemandePaiement);

    /**
     * Rechercher par id avec docs.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercherParIdAvecDocs(Long idDemandePaiement);

    /**
     * Rechercher par dossier subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    List<DemandePaiement> rechercherParDossierSubvention(Long idDossierSubvention);

    /**
     * Recherche demande paiement pour pdf par id.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercheDemandePaiementPourPdfParId(Long idDemandePaiement);

    /**
     * Enregistrer.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement
     */
    DemandePaiement enregistrer(DemandePaiement demandePaiement);

    /**
     * Compte accomptes pour dossier par id demande.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the integer
     */
    Integer compteAccomptesPourDossierParIdDemande(Long idDemandePaiement);

    /**
     * Rechercher par id chorus.
     *
     * @param idChorus
     *            the id chorus
     * @return the demande paiement
     */
    DemandePaiement rechercherParIdChorus(Long idChorus);

    /**
     * Mettre A jour demande paiement.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement
     */
    DemandePaiement mettreAJourDemandePaiement(DemandePaiement demandePaiement);

    /**
     * Rechercher toutes demandes paiement pour batch.
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesDemandesPaiementPourBatch();

    /**
     * Rechercher toutes pour repartition.
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesPourRepartition();

    /**
     * Gets the idfrom id ae.
     *
     * @param idAe
     *            the id ae
     * @return the idfrom id ae
     */
    Long getIdfromIdAe(Long idAe);

    /**
     * Rechercher toutes demandes paiement pour fen 0159 a.
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesDemandesPaiementPourFen0159a();

    /**
     * Rechercher certif non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    List<DemandePaiement> rechercherCertifNonNulParService(String chorusNumSf);

    /**
     * Rechercher versement non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    List<DemandePaiement> rechercherVersementNonNulParService(String chorusNumSf);

    /**
     * Rechercher montant avance total demandes paiement dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the big decimal
     */
    BigDecimal rechercherMontantAvanceTotalDemandesPaiementDossier(Long idDossier);

    /**
     * rechercher numero ordre suivant pour les demandes de paiement du dossier voulu.
     *
     * @param idDossier
     *            the id dossier
     * @return the short
     */
    short rechercherNumeroOrdreSuivant(Long idDossier);

    /**
     * rechercher numero ordre d'une demande de paiement.
     *
     * @param idDemandePaiement
     *            the id demande Paiement
     * @return the short
     */
    short rechercherNumeroOrdre(Long idDemandePaiement);

    /**
     * Checks if is est unique demande paiement en cours.
     *
     * @param numDossier
     *            the num dossier
     * @return true, if is est unique demande paiement en cours
     */
    boolean isEstUniqueDemandePaiementEnCours(String numDossier);
}

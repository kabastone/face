/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories.referentiel;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * The Interface SousProgrammeRepository.
 *
 * @author a453029
 */
public interface SousProgrammeRepository {

    /**
     * Rechercher tous les sous programmes.
     *
     * @return the list
     */
    List<SousProgramme> rechercherTousSousProgrammes();

    /**
     * Rechercher sous programmes pour renoncement dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite);

    /**
     * Rechercher sous programmes par collectivite.
     *
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesDeProjet();

    /**
     * Rechercher the sous programme par abreviation.
     *
     * @param abreviation
     *            the abreviation
     * @return sousProgramme
     */
    SousProgramme rechercherSousProgrammeParAbreviation(String abreviation);

    /**
     * Rechercher sous programmes par dotation programme.
     *
     * @param idDotationProgramme
     *            the id dotation programme
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesParDotationProgramme(Long idDotationProgramme);

    /**
     * Rechercher sous programmes pour creation de ligne dotation departementale.
     *
     * @param annee
     *            the annee
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesPourLigneDotationDepartementale(Integer annee);

    /**
     * Find by id.
     *
     * @param idSousProgramme
     *            the id sous programme
     * @return the sous programme
     */
    SousProgramme findById(Integer idSousProgramme);

    /**
     * Rechercher pour dotation departement.
     *
     * @return the list
     */
    List<SousProgramme> rechercherPourDotationDepartement();

    /**
     * Rechercher sous programmes de projet.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite);

    /**
     * @param codeDomaineFonctionnel
     * @return
     */
    SousProgramme rechercherSousProgrammeParCodeDomaineFonctionnel(String codeDomaineFonctionnel);
}

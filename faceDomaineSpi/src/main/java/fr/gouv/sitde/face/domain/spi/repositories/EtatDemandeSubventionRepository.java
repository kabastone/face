/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

/**
 * Interface du service d'acces au repository EtatDemandeSubvention.
 *
 * @author Atos
 */
public interface EtatDemandeSubventionRepository {

    /**
     * Recherche d'un état de demande de subvention par code.
     *
     * @param codeEtatdemande
     *            the code etatdemande
     * @return the etat demande subvention
     */
    EtatDemandeSubvention findByCode(String codeEtatdemande);
}

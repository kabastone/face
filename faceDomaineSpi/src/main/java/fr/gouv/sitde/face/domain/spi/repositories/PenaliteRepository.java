/**
 *
 */
package fr.gouv.sitde.face.domain.spi.repositories;

import fr.gouv.sitde.face.transverse.entities.Penalite;

/**
 * The Interface PenaliteRepository.
 *
 * @author Atos
 */
public interface PenaliteRepository {

    /**
     * Ajouter penalite.
     *
     * @param penalite
     *            the penalite
     * @return the penalite
     */
    Penalite ajouterPenalite(Penalite penalite);

    /**
     * Supprimer penalite par annee.
     *
     * @param annee
     *            the annee
     */
    void supprimerPenaliteParAnnee(int annee);

}

package fr.gouv.sitde.face.webapp.controller.prolongation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DemandeProlongationDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * Classe de test de DossierSubventionRestController.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DemandeProlongationRestControllerIT extends FichiersUploadTestWeb {

    /**
     * Test rechercher demande prolongation par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_prolongation_rechercher.xml" })
    public void testRechercherDemandeProlongationParId() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationDto demandeProlongationDto;

        // recherche
        uri = "/api/prolongation/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeProlongationDto);
        Assertions.assertEquals(900002L, demandeProlongationDto.getId());
        Assertions.assertEquals(900001L, demandeProlongationDto.getIdDossierSubvention());
        Assertions.assertEquals("2019-03-22", demandeProlongationDto.getDateDemande().toLocalDate().toString());
        Assertions.assertEquals("2020-01-20", demandeProlongationDto.getDateEcheanceAchevementDemande().toLocalDate().toString());
        Assertions.assertEquals(EtatDemandePaiementEnum.DEMANDEE.name(), demandeProlongationDto.getEtat());

    }

    /**
     * Test accorder demande prolongation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/prolongation/in_demande_prolongation_accorder.xml" })
    @ExpectedDatabase(value = "/dataset/expected/prolongation/ex_demande_prolongation_accorder.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAccorderDemandeProlongation() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationDto demandeProlongationDto;

        // recherche
        uri = "/api/prolongation/demande/" + idDemande + "/accorder";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeProlongationDto);
        Assertions.assertEquals(EtatDemandePaiementEnum.ACCORDEE.name(), demandeProlongationDto.getEtat());
    }

    /**
     * Test refuser demande prolongation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/prolongation/in_demande_prolongation_refuser.xml" })
    @ExpectedDatabase(value = "/dataset/expected/prolongation/ex_demande_prolongation_refuser.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRefuserDemandeProlongation() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationDto demandeProlongationDto = null;

        // Rechercher
        uri = "/api/prolongation/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // Refuser
        uri = "/api/prolongation/demande/" + idDemande + "/refuser";

        String inputJson = this.mapToJson(demandeProlongationDto);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeProlongationDto);
        Assertions.assertEquals(EtatDemandeProlongationEnum.REFUSEE.name(), demandeProlongationDto.getEtat());
    }

    /**
     * Test initialiser demande prolongation par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/prolongation/in_demande_prolongation_rechercher.xml" })
    public void testInitialiserDemandeProlongationParId() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationDto demandeProlongationDto;

        // recherche
        uri = "/api/prolongation/demande/dossier/900001/initialiser";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeProlongationDto);
        Assertions.assertEquals(ConstantesFace.CODE_DEM_PROLONGATION_INITIAL, demandeProlongationDto.getEtat());
        Assertions.assertEquals("2019CE03330010", demandeProlongationDto.getNumDossier());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/prolongation/in_demande_prolongation_rechercher.xml" })
    public void testCreerDemandeProlongationParId() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationDto demandeProlongationDto = new DemandeProlongationDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.DEMANDE_PROLONGATION.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/prolongation/demande/creer";

        // nouvelle demande de prolongation
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("dateEcheanceAchevementDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("dateEcheanceAchevement", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("idDossierSubvention", "900001")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeProlongationDto = this.mapFromJson(content, DemandeProlongationDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeProlongationDto);
        Assertions.assertEquals(EtatDemandeProlongationEnum.DEMANDEE.name(), demandeProlongationDto.getEtat());
        Assertions.assertEquals(900001L, demandeProlongationDto.getIdDossierSubvention());
    }

}

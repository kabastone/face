/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.tableaudebord;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * Classe de test de TableauDeBordSd7RestController.
 *
 * @author a453029
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TableauDeBordSd7RestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher tableau de bord sd 7.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_sd7.xml" })
    public void testRechercherTableauDeBordSd7() throws Exception {

        // recherche
        String uri = "/api/tableaudebord/sd7/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test rechercher tdb sd 7 acontroler subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_sd7.xml" })
    public void testRechercherTdbSd7AcontrolerSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/sd7/subacontroler/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb sd 7 acontroler paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_sd7.xml" })
    public void testRechercherTdbSd7AcontrolerPaiement() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/sd7/paiacontroler/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb sd 7 atransferer subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_sd7.xml" })
    public void testRechercherTdbSd7AtransfererSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/sd7/subatransferer/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb sd 7 atransferer paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_sd7.xml" })
    public void testRechercherTdbSd7AtransfererPaiement() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/sd7/paiatransferer/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

}

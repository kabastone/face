package fr.gouv.sitde.face.webapp.controller.dotation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.CollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DocumentDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationCollectiviteRestControllerIT extends FichiersUploadTestWeb {

    /** The time operation transverse. */
    @Inject
    TimeOperationTransverse timeOperationTransverse;

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_collectivite_repartition.xml" })
    public void testRechercherDotationCollectiviteRepartition() throws Exception {

        String uri = "/api/dotation/collectivite/repartition/rechercher";

        String codeDepartement = "001";

        MvcResult mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(codeDepartement)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto = this.mapFromJson(content, DotationCollectiviteRepartitionDto.class);

        Assertions.assertNotNull(dotationCollectiviteRepartitionDto);
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto.getCodeDepartement());
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto.getCollectivitesRepartition());
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto.getDotationsDepartement());
        Assertions.assertEquals(dotationCollectiviteRepartitionDto.getDotationsDepartement().size(), 5);
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto.getDocuments());
        Assertions.assertEquals(dotationCollectiviteRepartitionDto.getDocuments().size(), 3);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_collectivite_repartition_televerser.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_collectivite_repartition_televerser.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testTeleverserDocumentCourrierRepartition() throws Exception {

        // init
        String codeDepartement = "001";
        String uri;
        String content;
        MvcResult mvcResult;
        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.COURRIER_REPARTITION.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.COURRIER_DOTATION.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        uri = "/api/dotation/collectivite/repartition/document/televerser";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("codeDepartement", codeDepartement)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationCollectiviteRepartitionDto = this.mapFromJson(content, DotationCollectiviteRepartitionDto.class);

        List<DocumentDto> documents = dotationCollectiviteRepartitionDto.getDocuments();
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto);
        Assertions.assertNotNull(dotationCollectiviteRepartitionDto.getDocuments());
        Assertions.assertEquals(dotationCollectiviteRepartitionDto.getDocuments().size(), 1);
        for (DocumentDto doc : documents) {
            Assertions.assertEquals(doc.getDateTeleversement().toLocalDate(), this.timeOperationTransverse.now().toLocalDate());
        }

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_collectivite_repartition_repartir.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_collectivite_repartition_repartir.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRepartirDotationCollectivite() throws Exception {

        String codeDepartement = "002";

        String uri = "/api/dotation/collectivite/repartition/repartir";

        CollectiviteRepartitionDto collectiviteRepartitionDto = new CollectiviteRepartitionDto();
        collectiviteRepartitionDto.setCodeDepartement(codeDepartement);

        List<DotationCollectiviteRepartitionParSousProgrammeDto> listDotationCollectiviteRepartitionParSousProgrammeDto = new ArrayList<>();

        DotationCollectiviteRepartitionParSousProgrammeDto dotationColRepParSP2 = new DotationCollectiviteRepartitionParSousProgrammeDto();
        dotationColRepParSP2.setId(900002L);
        dotationColRepParSP2.setIdDotationDepartement(900002L);
        dotationColRepParSP2.setMontant(new BigDecimal(800000));
        listDotationCollectiviteRepartitionParSousProgrammeDto.add(dotationColRepParSP2);

        DotationCollectiviteRepartitionParSousProgrammeDto dotationColRepParSP3 = new DotationCollectiviteRepartitionParSousProgrammeDto();
        dotationColRepParSP3.setId(900003L);
        dotationColRepParSP3.setIdDotationDepartement(900003L);
        dotationColRepParSP3.setMontant(new BigDecimal(800000));
        listDotationCollectiviteRepartitionParSousProgrammeDto.add(dotationColRepParSP3);

        DotationCollectiviteRepartitionParSousProgrammeDto dotationColRepParSP4 = new DotationCollectiviteRepartitionParSousProgrammeDto();
        dotationColRepParSP4.setId(900004L);
        dotationColRepParSP4.setIdDotationDepartement(900004L);
        dotationColRepParSP4.setMontant(new BigDecimal(800000));
        listDotationCollectiviteRepartitionParSousProgrammeDto.add(dotationColRepParSP4);

        DotationCollectiviteRepartitionParSousProgrammeDto dotationColRepParSP5 = new DotationCollectiviteRepartitionParSousProgrammeDto();
        dotationColRepParSP5.setId(900005L);
        dotationColRepParSP5.setIdDotationDepartement(900005L);
        dotationColRepParSP5.setMontant(new BigDecimal(800000));
        listDotationCollectiviteRepartitionParSousProgrammeDto.add(dotationColRepParSP5);

        collectiviteRepartitionDto.setDotationsCollectivite(listDotationCollectiviteRepartitionParSousProgrammeDto);

        String inputJson = this.mapToJson(collectiviteRepartitionDto);

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto = this.mapFromJson(content, DotationCollectiviteRepartitionDto.class);

        Assertions.assertNotNull(dotationCollectiviteRepartitionDto);

    }
}

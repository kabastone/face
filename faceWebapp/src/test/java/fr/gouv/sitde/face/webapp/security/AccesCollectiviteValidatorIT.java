/**
 *
 */
package fr.gouv.sitde.face.webapp.security;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;
import fr.gouv.sitde.face.webapp.controller.connexion.UtilisateurConnecteDto;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

/**
 * The Class AccesCollectiviteValidatorIT.
 *
 * @author Atos
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AccesCollectiviteValidatorIT extends AbstractTestWeb {

    /** The Constant UTILISATEUR_GENERIQUE. */
    private static final UtilisateurConnecteDto UTILISATEUR_GENERIQUE;

    /** The Constant UTILISATEUR_MFER. */
    private static final UtilisateurConnecteDto UTILISATEUR_MFER;

    static {
        // Creation utilisateur generique
        UTILISATEUR_GENERIQUE = new UtilisateurConnecteDto();
        UTILISATEUR_GENERIQUE.setId(900010L);
        UTILISATEUR_GENERIQUE.setNom("Gene");
        UTILISATEUR_GENERIQUE.setPrenom("Jean");
        UTILISATEUR_GENERIQUE.setEmail("jean.gene@mock.fr");
        UTILISATEUR_GENERIQUE.setCerbereId("id_mock_gene");
        List<RoleEnum> listeRoles = new ArrayList<>(1);
        listeRoles.add(RoleEnum.GENERIQUE);
        UTILISATEUR_GENERIQUE.setListeRoles(listeRoles);

        // Droits de l'utilisateur generique
        List<DroitAgentCollectiviteDto> listeDroits = new ArrayList<>(1);
        DroitAgentCollectiviteDto droitAgent = new DroitAgentCollectiviteDto();
        // admin de collectivite 900003
        droitAgent.setIdCollectivite(900003L);
        droitAgent.setIdUtilisateur(900010L);
        droitAgent.setAdmin(true);
        listeDroits.add(droitAgent);
        // agent de collectivite 900100
        droitAgent = new DroitAgentCollectiviteDto();
        droitAgent.setIdCollectivite(900100L);
        droitAgent.setIdUtilisateur(900010L);
        droitAgent.setAdmin(false);
        listeDroits.add(droitAgent);
        UTILISATEUR_GENERIQUE.setDroitsAgentCollectivites(listeDroits);

        // Creation utilisateur MFER
        UTILISATEUR_MFER = new UtilisateurConnecteDto();
        UTILISATEUR_MFER.setId(900011L);
        UTILISATEUR_MFER.setNom("mfer");
        UTILISATEUR_MFER.setPrenom("Bruno");
        UTILISATEUR_MFER.setEmail("bruno.mfer@mock.fr");
        UTILISATEUR_MFER.setCerbereId("id_mock_mfer");
        listeRoles = new ArrayList<>(2);
        listeRoles.add(RoleEnum.GENERIQUE);
        listeRoles.add(RoleEnum.MFER_RESPONSABLE);
        UTILISATEUR_MFER.setListeRoles(listeRoles);
    }

    /** The acces validator. */
    @Inject
    private AccesCollectiviteValidator accesValidator;

    /**
     * Test valider acces collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesCollectivite() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesCollectivitePourMferSd7EtAgent(UTILISATEUR_MFER, 900001L);

        try {
            this.accesValidator.validerAccesCollectivitePourMferSd7EtAgent(UTILISATEUR_GENERIQUE, 900001L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesCollectivitePourMferSd7EtAgent(UTILISATEUR_GENERIQUE, 900003L);
    }

    /**
     * Test valider acces admin collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesAdminCollectivite() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesAdminCollectivite(UTILISATEUR_MFER, 900001L);

        try {
            this.accesValidator.validerAccesAdminCollectivite(UTILISATEUR_GENERIQUE, 900001L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesAdminCollectivite(UTILISATEUR_GENERIQUE, 900003L);
    }

    /**
     * Test valider acces dotation collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDotationCollectivite() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDotationCollectivite(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDotationCollectivite(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDotationCollectivite(UTILISATEUR_GENERIQUE, 900002L);
    }

    /**
     * Test valider acces dossier subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDossierSubvention() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDossierSubvention(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDossierSubvention(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDossierSubvention(UTILISATEUR_GENERIQUE, 900001L);
    }

    /**
     * Test valider acces demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDemandeSubvention() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDemandeSubvention(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDemandeSubvention(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDemandeSubvention(UTILISATEUR_GENERIQUE, 900001L);
    }

    /**
     * Test valider acces demande prolongation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDemandeProlongation() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDemandeProlongation(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDemandeProlongation(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDemandeProlongation(UTILISATEUR_GENERIQUE, 900001L);
    }

    /**
     * Test valider acces demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDemandePaiement() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDemandePaiement(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDemandePaiement(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDemandePaiement(UTILISATEUR_GENERIQUE, 900001L);
    }

    /**
     * Test valider acces document demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentDemandeSubvention() throws Exception {

        // Utilisateur MFER, pas de vérification effectuée
        this.accesValidator.validerAccesDocument(UTILISATEUR_MFER, 2500L);

        try {
            this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900001L);
    }

    /**
     * Test valider acces document demande prolongation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentDemandeProlongation() throws Exception {

        try {
            this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900002L);
    }

    /**
     * Test valider acces document demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentDemandePaiement() throws Exception {

        try {
            this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 2500L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900003L);
    }

    /**
     * Test valider acces document dotation departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentCourrierAnnuelDepartement() throws Exception {

        this.accesValidator.validerAccesDocument(UTILISATEUR_MFER, 900004L);

        try {
            this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900004L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900005L);
    }

    /**
     * Test valider acces document ligne de dotation departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentLigneDotationDepartement() throws Exception {

        try {
            this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900004L);
            Assertions.fail("L'utilisateur n'a pas de droit sur la collectivité voulue");
        } catch (TechniqueException e) {
            // Comportement voulu
        }

        // L'utilisateur a les droits d'agent sur la collectivité voulue
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900006L);
    }

    /**
     * Test valider acces document modele.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_recherche_collectivite_origine.xml" })
    public void testValiderAccesDocumentModele() throws Exception {

        this.accesValidator.validerAccesDocument(UTILISATEUR_MFER, 900007L);
        this.accesValidator.validerAccesDocument(UTILISATEUR_GENERIQUE, 900007L);
    }
}

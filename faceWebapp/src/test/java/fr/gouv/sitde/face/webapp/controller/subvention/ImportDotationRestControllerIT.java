/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.subvention;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.webapp.configuration.FichierCsvUploadTestWeb;

/**
 * Classe de test de ImportDotationRestController
 *
 * @author ATOS
 *
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class ImportDotationRestControllerIT extends FichierCsvUploadTestWeb {

    /**
     * Test de l'extension du fichier (fichier 1)
     *
     * Test si le csv est vide ou null (fichier 2)
     *
     * Pas de dataset : le traitement du controlleur est interrompu avant d'acceder a la base
     *
     * @throws Exception
     */
    @Test
    public void testFichierExtensionEtVide() throws Exception {

        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert1 = FichierCsvUploadTestWeb.getTempFile1();

        FichierTransfert fichierTransfert2 = FichierCsvUploadTestWeb.getTempFile2();

        MockMultipartFile fichierEnvoye1 = new MockMultipartFile("importDotationFile", fichierTransfert1.getNomFichier(),
                fichierTransfert1.getContentTypeTheorique(), fichierTransfert1.getBytes());

        // demande d'import fichier .txt
        uri = "/api/subvention/dossier/importer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoye1).contentType("text/csv")).andReturn();

        // resultat attendu : une regle de gestion exception
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException rge = (RegleGestionException) mvcResult.getResolvedException();

        // verification du message d'erreur : "verification de l'extention"
        Assertions.assertTrue(rge.getMessage().equals("upload.erreur.csv"));

        MockMultipartFile fichierEnvoye2 = new MockMultipartFile("importDotationFile", fichierTransfert2.getNomFichier(),
                fichierTransfert2.getContentTypeTheorique(), fichierTransfert2.getBytes());

        // demande d'import fichier .csv vide
        uri = "/api/subvention/dossier/importer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoye2).contentType("text/csv")).andReturn();

        // attendu erreur serveur car le fichier est vide
        Assertions.assertTrue(isResultErreurServeur(mvcResult));

        TechniqueException te = (TechniqueException) mvcResult.getResolvedException();

        // verification du message d'erreur : "fichier vide ou null"
        Assertions.assertTrue(te.getMessage().equals("upload.erreur.videounull"));

    }

    /**
     * Test de détection de Dotations notifiées Doit renvoyer false car il existe des notifiée de l'annee en cours pour le type Annuelle
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_import_dotation_notifiees.xml" })
    public void testDotationNotifieeKO() throws Exception {

        String uri, content;
        MvcResult mvcResult;

        uri = "/api/subvention/dossier/verifier";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri)).andReturn();

        // attendu pas d'erreur car demande sans parametres
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // attendu : retour false il existe des dotation notifiées sur l'année en cours
        content = mvcResult.getResponse().getContentAsString();
        Boolean res = this.mapFromJson(content, Boolean.class);

        Assertions.assertFalse(res);

    }

    /**
     * Test de détectoin de Dotations notifiées Doit renevoyer true car les dotations notifiées soit ne concenrent pas l'année en cours, soit pas le
     * bon type (pas Année)
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_import_dotation_notifiees2.xml" })
    public void testDotationNotifieeOK() throws Exception {
        String uri, content;
        MvcResult mvcResult;

        uri = "/api/subvention/dossier/verifier";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri)).andReturn();

        // attendu pas d'erreur car demande sans parametres
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // attendu : retour true , il existe des notifiées mais pas dans l'année ni sur ANNUELLE
        content = mvcResult.getResponse().getContentAsString();
        Boolean res = this.mapFromJson(content, Boolean.class);

        Assertions.assertTrue(res);
    }

    /**
     * Test coherence fichier import
     *
     * Attendu : base non modifée ca erreur de coherence
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_import_dotation.xml" })
    public void testCoherence() throws Exception {
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichierCsvUploadTestWeb.getTempFile3();

        MockMultipartFile fichierEnvoye = new MockMultipartFile("importDotationFile", fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // demande d'import fichier .txt
        uri = "/api/subvention/dossier/importer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoye).contentType("text/csv")).andReturn();

        // resultat attendu : une regle de gestion exception
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException rge = (RegleGestionException) mvcResult.getResolvedException();

        // verification du message d'erreur : "verification de l'extention"
        Assertions.assertTrue(rge.getMessage().equals("subvention.import.dotation.incoherente"));

    }

    /**
     * Test cas nominal d'import de dotation
     *
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_import_dotation2.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_import_dotation2.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testNominal() throws Exception {
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichierCsvUploadTestWeb.getTempFile3();

        MockMultipartFile fichierEnvoye = new MockMultipartFile("importDotationFile", fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // demande d'import fichier .txt
        uri = "/api/subvention/dossier/importer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoye).contentType("text/csv")).andReturn();

        // attendu pas d'erreur
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

}

package fr.gouv.sitde.face.webapp.controller.commun;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * The Class AnomalieRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DocumentRestControllerIT extends FichiersUploadTestWeb {

    /** The document service. */
    @Inject
    private DocumentService documentService;

    @Inject
    private DemandeSubventionService demandeSubventionService;

    /**
     * Test telecharger document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/document/in_telecharger_document.xml" })
    public void testTelechargerDocument() throws Exception {

        Long idDemandeSubvention = new Long("900011");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));

        String uri = "/api/document/" + doc.getId() + "/telecharger";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test telecharger document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/document/in_telecharger_document.xml" })
    public void testSupprimerDocument() throws Exception {

        Long idDemandeSubvention = new Long("900011");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));
        Long idDoc = doc.getId();
        String uri = "/api/document/" + idDoc + "/supprimer";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        doc = this.documentService.rechercherDocumentParIdDocument(idDoc);
        Assertions.assertNull(doc);
    }
}

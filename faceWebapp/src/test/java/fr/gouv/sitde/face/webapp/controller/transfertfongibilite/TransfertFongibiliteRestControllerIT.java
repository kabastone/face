package fr.gouv.sitde.face.webapp.controller.transfertfongibilite;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DotationTransfertFongibiliteDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.application.dto.lov.TransfertFongibiliteLovDto;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TransfertFongibiliteRestControllerIT extends AbstractTestWeb {

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/fongibilite/in_transfert_fongibilite.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fongibilite/ex_transfert_fongibilite.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testTransfertFongibilite() throws Exception {
        // init
        String idCollectivite = "900001";
        String idSousProgrammeOrigine = "900002";
        String idSousProgrammeDestination = "900001";
        String montant = "50000";
        String uri;
        String content;
        MvcResult mvcResult;
        DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto;

        uri = "/api/dotation/fongibiliter/transferer";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idCollectivite", idCollectivite).param("idSousProgrammeDebiter", idSousProgrammeOrigine)
                .param("idSousProgrammeCrediter", idSousProgrammeDestination).param("montantTransfert", montant)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationTransfertFongibiliteDto = this.mapFromJson(content, DotationTransfertFongibiliteDto.class);

        Assertions.assertNotNull(dotationTransfertFongibiliteDto);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/fongibilite/in_transfert_fongibilite.xml" })
    @ExpectedDatabase(value = "/dataset/expected/fongibilite/ex_transfert_fongibilite_ligne_dotation.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testTransfertFongibilitePuisLigneDotation() throws Exception {
        // init
        String idCollectivite = "900001";
        String idSousProgrammeOrigine = "900002";
        String idSousProgrammeDestination = "900001";
        String montant = "50000";
        String uri;
        String content;
        MvcResult mvcResult;
        DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto;

        uri = "/api/dotation/fongibiliter/transferer";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idCollectivite", idCollectivite).param("idSousProgrammeDebiter", idSousProgrammeOrigine)
                .param("idSousProgrammeCrediter", idSousProgrammeDestination).param("montantTransfert", montant)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationTransfertFongibiliteDto = this.mapFromJson(content, DotationTransfertFongibiliteDto.class);

        Assertions.assertNotNull(dotationTransfertFongibiliteDto);

        // recherche
        uri = "/api/dotation/departement/lignes/";
        LigneDotationDepartementaleDto ligneDotationDepartementaleDto = new LigneDotationDepartementaleDto(null, new Long(900001), null,
                new BigDecimal(10000), TypeDotationDepartementEnum.EXCEPTIONNELLE, null,
                new SousProgrammeDto("AP", "Renforcement des réseaux", "TODO1"));
        String inputJson = this.mapToJson(ligneDotationDepartementaleDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/fongibilite/in_transfert_fongibilite.xml" })
    public void testTransfertFongibiliteMontantDepasse() throws Exception {
        // init
        String idCollectivite = "900001";
        String idSousProgrammeOrigine = "900002";
        String idSousProgrammeDestination = "900001";
        String montantSuperieurDotation = "200000";
        String montantSuperieurMontantDisponible = "90000";
        String uri;
        MvcResult mvcResult;

        uri = "/api/dotation/fongibiliter/transferer";

        mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("idCollectivite", idCollectivite).param("idSousProgrammeDebiter", idSousProgrammeOrigine)
                        .param("idSousProgrammeCrediter", idSousProgrammeDestination).param("montantTransfert", montantSuperieurDotation))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        uri = "/api/dotation/fongibiliter/transferer";

        mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("idCollectivite", idCollectivite).param("idSousProgrammeDebiter", idSousProgrammeOrigine)
                        .param("idSousProgrammeCrediter", idSousProgrammeDestination).param("montantTransfert", montantSuperieurMontantDisponible))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/fongibilite/in_transfert_fongibilite_present.xml" })
    public void testRechercherTransfertFongibiliteAODE() throws Exception {
        // init
        String idCollectivite = "900001";
        String annee = "2019";
        MvcResult mvcResult;

        String uri = "/api/dotation/transfert/collectivite/" + idCollectivite + "/annee/" + annee + "/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        TransfertFongibiliteLovDto[] transferts = this.mapFromJson(content, TransfertFongibiliteLovDto[].class);

        Assertions.assertNotNull(transferts);
        Assertions.assertEquals(transferts.length, 2);
        Assertions.assertEquals(transferts[0].getCollectivite().getId(), new Long("900001"));
        Assertions
                .assertTrue(transferts[0].getMontant().equals(new BigDecimal("3000")) || transferts[0].getMontant().equals(new BigDecimal("47000")));
        Assertions
                .assertTrue(transferts[1].getMontant().equals(new BigDecimal("3000")) || transferts[0].getMontant().equals(new BigDecimal("47000")));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/fongibilite/in_transfert_fongibilite_present_mfer.xml" })
    public void testRechercherTransfertFongibiliteMFER() throws Exception {
        // init
        String annee = "2019";
        MvcResult mvcResult;

        String uri = "/api/dotation/transfert/annee/" + annee + "/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        TransfertFongibiliteLovDto[] transferts = this.mapFromJson(content, TransfertFongibiliteLovDto[].class);

        Assertions.assertNotNull(transferts);
        Assertions.assertEquals(transferts.length, 4);
        Assertions.assertTrue(transferts[0].getCollectivite().getId().equals(new Long("900001"))
                || transferts[0].getCollectivite().getId().equals(new Long("900002")));

        for (TransfertFongibiliteLovDto dto : transferts) {
            Assertions.assertTrue(dto.getMontant().equals(new BigDecimal("3000")) || dto.getMontant().equals(new BigDecimal("47000"))
                    || dto.getMontant().equals(new BigDecimal("22000")) || dto.getMontant().equals(new BigDecimal("28000")));
        }
    }
}

package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.application.dto.CollectiviteDetailDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class CollectiviteRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class CollectiviteRestControllerIT extends AbstractTestWeb {

    @Autowired
    private GestionCollectiviteApplication gestionCollectivite;

    /**
     * Test rechercher collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivites_rechercher.xml" })
    public void testRechercherCollectivite() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/collectivite/" + idCollectivite + "/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        CollectiviteDetailDto collectivite = this.mapFromJson(content, CollectiviteDetailDto.class);

        Assertions.assertNotNull(collectivite);
        Assertions.assertEquals("COLLECTIVITE-900001", collectivite.getNomLong());

        idCollectivite = "899999";
        uri = "/api/collectivite/" + idCollectivite + "/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));
    }

    /**
     * Test modifier collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivites_rechercher.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/collectivite/ex_collectivite_modifier.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierCollectivite() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/collectivite/" + idCollectivite + "/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        CollectiviteDetailDto collectivite = this.mapFromJson(content, CollectiviteDetailDto.class);

        idCollectivite = "900001";
        uri = "/api/collectivite/" + idCollectivite + "/modifier";

        collectivite.setNomLong("COLLECTIVITE-900001-MODIF");
        collectivite.setAvecMoaMoeIdentiques(true);
        collectivite.setAvecPlusieursMoe(false);

        AdresseDto adresse = new AdresseDto();
        adresse.setNom("Toto");
        adresse.setNomVoie("Rue des Grandes Ormes");
        adresse.setCodePostal("12000");
        adresse.setCommune("Trifouillis");
        collectivite.setAdresseMoa(adresse);
        String inputJson = this.mapToJson(collectivite);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        collectivite = this.mapFromJson(content, CollectiviteDetailDto.class);

        Assertions.assertNotNull(collectivite);
        Assertions.assertEquals("COLLECTIVITE-900001-MODIF", collectivite.getNomLong());
    }

    /**
     * Test creer collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/collectivite/ex_collectivite_creer.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerCollectivite() throws Exception {

        String uri = "/api/collectivite/creer";

        DepartementLovDto departement = new DepartementLovDto();
        departement.setId(900001);
        departement.setCode("01");
        departement.setNom("Ain");

        CollectiviteDetailDto collectivite = new CollectiviteDetailDto();
        collectivite.setDepartement(departement);
        collectivite.setNomCourt("Nom Court");
        collectivite.setNomLong("Nom Long");
        collectivite.setNumCollectivite("00001");
        collectivite.setReceveurNom("receveurNom");
        collectivite.setReceveurNumCodique("receveurNumCodique");
        // Le siret est invalide
        collectivite.setSiret("000111222");
        collectivite.setEtatCollectivite(EtatCollectiviteEnum.CREEE);

        String inputJson = this.mapToJson(collectivite);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Cas de réussite
        collectivite.setSiret("78012998703591");
        inputJson = this.mapToJson(collectivite);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();

        content = mvcResult.getResponse().getContentAsString();
        collectivite = this.mapFromJson(content, CollectiviteDetailDto.class);

        Assertions.assertNotNull(collectivite);
        Assertions.assertEquals("Nom Long", collectivite.getNomLong());
        Assertions.assertEquals(0, collectivite.getVersion());
    }

    /**
     * Test creer collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivite_creation.xml" })
    public void testCreerCollectiviteSiretDuplique() throws Exception {

        String uri = "/api/collectivite/creer";

        DepartementLovDto departement = new DepartementLovDto();
        departement.setId(900001);
        departement.setCode("01");
        departement.setNom("Ain");

        CollectiviteDetailDto collectivite = new CollectiviteDetailDto();
        collectivite.setDepartement(departement);
        collectivite.setNomCourt("Nom Court");
        collectivite.setNomLong("Nom Long");
        collectivite.setNumCollectivite("00001");
        collectivite.setReceveurNom("receveurNom");
        collectivite.setReceveurNumCodique("receveurNumCodique");
        // Le siret existe déjà
        collectivite.setSiret("40802471900572");
        collectivite.setEtatCollectivite(EtatCollectiviteEnum.CREEE);

        String inputJson = this.mapToJson(collectivite);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException content = (RegleGestionException) mvcResult.getResolvedException();
        Assertions.assertEquals("siret.non.unique", content.getMessage());

    }

    /**
     * Test creer collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivite_creation.xml" })
    public void testModifierCollectiviteSiretDuplique() throws Exception {

        String idCollectivite = "900002";
        String uri = "/api/collectivite/" + idCollectivite + "/modifier";

        CollectiviteDetailDto collectivite = this.gestionCollectivite.rechercherCollectiviteDetail(new Long(idCollectivite));
        collectivite.setSiret("40802471900572");
        String inputJson = this.mapToJson(collectivite);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException content = (RegleGestionException) mvcResult.getResolvedException();
        Assertions.assertEquals("siret.non.unique", content.getMessage());

    }
}

package fr.gouv.sitde.face.webapp.mock;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;
import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;
import io.jsonwebtoken.Claims;

/**
 * The Class JwtTokenProviderMock.
 *
 * @author a453029
 */
@Component
@Profile({ "test", "testpic" })
public class JwtTokenProviderMock implements JwtTokenProvider {

    /** The Constant HORS_MOCK. */
    private static final String HORS_MOCK = "Hors mock";

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#resolveToken(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public String resolveToken(HttpServletRequest request) {
        throw new NotImplementedException(HORS_MOCK);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#validateToken(java.lang.String)
     */
    @Override
    public boolean validateToken(String token) {
        throw new NotImplementedException(HORS_MOCK);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#createAccessToken(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public String createAccessToken(HttpServletRequest httpServletRequest) {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#getAuthentication(java.lang.String)
     */
    @Override
    public Authentication getAuthentication(String token) {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#createRefreshToken(io.jsonwebtoken.Claims)
     */
    @Override
    public String createRefreshToken(Claims claims) {
        throw new NotImplementedException(HORS_MOCK);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#stockerTokenDansCookieTemporaire(java.lang.String,
     * javax.servlet.http.HttpServletResponse, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void stockerTokenDansCookieTemporaire(String token, HttpServletResponse response, HttpServletRequest request) {
        // Mock qui ne fait rien
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#supprimerCookieTemporaireToken(javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void supprimerCookieTemporaireToken(HttpServletResponse response) {
        // Mock qui ne fait rien
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#getUtilisateurTokenFromToken(java.lang.String)
     */
    @Override
    public UtilisateurToken getUtilisateurTokenFromToken(String token) {
        return this.obtenirUtilisateurToutRoleTokenMock();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#getUtilisateurTokenFromRequest(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public UtilisateurToken getUtilisateurTokenFromRequest(HttpServletRequest request) {

        String role = request.getHeader(AbstractTestWeb.ROLE_HEADER);
        if (RoleEnum.GENERIQUE.name().equals(role)) {
            return this.obtenirUtilisateurGeneriqueTokenMock();
        } else if (RoleEnum.MFER_RESPONSABLE.name().equals(role)) {
            return this.obtenirUtilisateurMferTokenMock();
        } else {
            return this.obtenirUtilisateurToutRoleTokenMock();
        }
    }

    /**
     * Obtenir utilisateur token mock.
     *
     * @return the utilisateur token
     */
    private UtilisateurToken obtenirUtilisateurToutRoleTokenMock() {
        UtilisateurToken utilisateur = new UtilisateurToken();
        utilisateur.setEmail("jean.dubois@mock.fr");
        utilisateur.setNom("Dubois");
        utilisateur.setPrenom("Jean");
        utilisateur.setIdCerbere("ID_MOCK");
        List<RoleEnum> listeRoles = new ArrayList<>(2);
        listeRoles.add(RoleEnum.GENERIQUE);
        listeRoles.add(RoleEnum.MFER_RESPONSABLE);
        listeRoles.add(RoleEnum.SD7_RESPONSABLE);
        utilisateur.setListeRoles(listeRoles);
        return utilisateur;
    }

    /**
     * Obtenir utilisateur token mock.
     *
     * @return the utilisateur token
     */
    private UtilisateurToken obtenirUtilisateurGeneriqueTokenMock() {
        UtilisateurToken utilisateur = new UtilisateurToken();
        utilisateur.setEmail("jean.dubois@mock.fr");
        utilisateur.setNom("Dubois");
        utilisateur.setPrenom("Jean");
        utilisateur.setIdCerbere("ID_MOCK");
        List<RoleEnum> listeRoles = new ArrayList<>(2);
        listeRoles.add(RoleEnum.GENERIQUE);

        utilisateur.setListeRoles(listeRoles);
        return utilisateur;
    }

    /**
     * Obtenir utilisateur token mock.
     *
     * @return the utilisateur token
     */
    private UtilisateurToken obtenirUtilisateurMferTokenMock() {
        UtilisateurToken utilisateur = new UtilisateurToken();
        utilisateur.setEmail("jean.dubois@mock.fr");
        utilisateur.setNom("Dubois");
        utilisateur.setPrenom("Jean");
        utilisateur.setIdCerbere("ID_MOCK");
        List<RoleEnum> listeRoles = new ArrayList<>(2);
        listeRoles.add(RoleEnum.GENERIQUE);
        listeRoles.add(RoleEnum.MFER_RESPONSABLE);
        listeRoles.add(RoleEnum.SD7_RESPONSABLE);
        utilisateur.setListeRoles(listeRoles);
        return utilisateur;
    }
}


package fr.gouv.sitde.face.webapp.controller.subvention;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DemandeSubventionDetailsDto;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * Classe de test de DossierSubventionRestController.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DemandeSubventionRestControllerIT extends FichiersUploadTestWeb {

    /**
     * Dossier de subvention : recherche d'un dossier par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testRechercherDemandeSubventionParId() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        // recherche
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(new BigDecimal("80.00"), demandeSubventionDetailsDto.getTauxAideFacePercent());
        Assertions.assertEquals(900001L, demandeSubventionDetailsDto.getIdDossier());
        Assertions.assertEquals(900002L, demandeSubventionDetailsDto.getIdDotationCollectivite());
        Assertions.assertEquals(900003L, demandeSubventionDetailsDto.getIdCollectivite());
    }

    /**
     * Test generation donnees et document pdf.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_pdf.xml" })
    public void testDonneesDemandeSubventionPdf() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, contentDisposition;
        MvcResult mvcResult;

        // recherche
        uri = "/api/subvention/demande/" + idDemande + "/generation";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        contentDisposition = mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION);
        Assertions.assertNotNull(mvcResult.getResponse().getContentAsString());
        String[] list = contentDisposition.split("=");
        Assertions.assertTrue(list[0].equals("attachment; filename"));
        Assertions.assertTrue(list[1].endsWith(".pdf"));
        Assertions.assertTrue(
                mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_TYPE).contains(ExtensionFichierEnum.PDF.getContentType()));
    }

    /**
     * Test qualifier demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testQualifierDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.FICHE_SIRET.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/qualifier";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("id", idDemande).param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000031").param("version", "0").param("idSousProgramme", "900003")
                .param("numDossier", "2019CE028333").param("idDossier", "900001")).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.QUALIFIEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test qualifier demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testQualifierDemandeSubventionChangementNumeroDossier() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.FICHE_SIRET.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/qualifier";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("id", idDemande).param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000031").param("version", "0").param("idSousProgramme", "900003")
                .param("numDossier", "2019CE028333-1").param("idDossier", "900001")).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.QUALIFIEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test qualifier demande subvention avec deplacement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml",
            "/dataset/input/subvention/in_demande_subvention_recherche_qualifier_dossier_existant.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_recherche_qualifier_dossier_existant.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testQualifierDemandeSubventionAvecDeplacement() throws Exception {
        // init.
        String idDemande = "900010";
        String uri, content;
        MvcResult mvcResult;

        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.FICHE_SIRET.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/qualifier";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("id", idDemande).param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000031").param("version", "0").param("demandeComplementaire", "true")
                .param("idSousProgramme", "900003").param("numDossier", "2019CE03330010").param("idDossier", "900001")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.QUALIFIEE.name(), demandeSubventionDetailsDto.getEtat());
        // on verifie que l'id du dossier a ete changé 900002 -> 900001
        Assertions.assertTrue(demandeSubventionDetailsDto.getIdDossier() == 900001);
    }

    /**
     * Test qualifier demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher_qualif_chorus_inexistant.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_rechercher_qualif_chorus_inexistant.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testQualifierDemandeSubventionChorusCollectiviteNonTrouvee() throws Exception {
        // init.
        String idDemande = "900002";
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.FICHE_SIRET.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/qualifier";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("id", idDemande).param("tauxAideFacePercent", "80").param("idCollectivite", "900001")
                .param("chorusNumLigneLegacy", "0000000031").param("version", "0").param("idSousProgramme", "900003")
                .param("numDossier", "2019CE028333").param("idDossier", "900001")).andReturn();

        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    /**
     * Test qualifier demande subvention taux aide non unique
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_qualifier_taux_aide_non_unique.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_qualifier_taux_aide_non_unique.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testQualifierDemandeSubventionAvecTauxAideNonUnique() throws Exception {
        // init.
        String idDemande = "900003";
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.FICHE_SIRET.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/qualifier";

        // nouvelle demande de subvention
        mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                        .param("montantHTTravauxEligibles", "100").param("id", idDemande).param("tauxAideFacePercent", "70")
                        .param("demandeComplementaire", "true").param("idCollectivite", "900001").param("chorusNumLigneLegacy", "0000000002")
                        .param("version", "0").param("idSousProgramme", "900003").param("numDossier", "2019CE028333").param("idDossier", "900001"))
                .andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test accorder demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testAccorderDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900003";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        // recherche
        uri = "/api/subvention/demande/" + idDemande + "/accorder";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.ACCORDEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test controler demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testControlerDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900004";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        // recherche
        uri = "/api/subvention/demande/" + idDemande + "/controler";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.CONTROLEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test corriger demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testCorrigerDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900005";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = null;

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Corriger
        uri = "/api/subvention/demande/corriger";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // corriger demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("id", String.valueOf(demandeSubventionDetailsDto.getId()))
                .param("idDossier", String.valueOf(demandeSubventionDetailsDto.getIdDossier()))
                .param("dateJour", demandeSubventionDetailsDto.getDateJour().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", String.valueOf(demandeSubventionDetailsDto.getMontantHTTravauxEligibles()))
                .param("tauxAideFacePercent", String.valueOf(demandeSubventionDetailsDto.getTauxAideFacePercent()))
                .param("idCollectivite", String.valueOf(demandeSubventionDetailsDto.getIdCollectivite()))
                .param("chorusNumLigneLegacy", demandeSubventionDetailsDto.getChorusNumLigneLegacy())
                .param("version", String.valueOf(demandeSubventionDetailsDto.getVersion()))
                .param("idSousProgramme", String.valueOf(demandeSubventionDetailsDto.getIdSousProgramme()))
                .param("montantAnnee1", JsonMapper.getJsonFromData(demandeSubventionDetailsDto.getCadencementDto().getMontantAnnee1()))
                .param("montantAnnee2", JsonMapper.getJsonFromData(demandeSubventionDetailsDto.getCadencementDto().getMontantAnnee2()))
                .param("montantAnnee3", JsonMapper.getJsonFromData(demandeSubventionDetailsDto.getCadencementDto().getMontantAnnee3()))
                .param("montantAnnee4", JsonMapper.getJsonFromData(demandeSubventionDetailsDto.getCadencementDto().getMontantAnnee4()))
                .param("estValide", JsonMapper.getJsonFromData(demandeSubventionDetailsDto.getCadencementDto().getEstValide()))).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test transferer demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_transferer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_transferer.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testTransfererDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = null;

        // Qualifier
        uri = "/api/subvention/demande/" + idDemande + "/transferer";

        String inputJson = this.mapToJson(demandeSubventionDetailsDto);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.EN_ATTENTE_TRANSFERT_MAN.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test refuser demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_demande_subvention_refuser.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_refuser.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRefuserDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = null;

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Refuser
        uri = "/api/subvention/demande/" + idDemande + "/refuser";

        String inputJson = this.mapToJson(demandeSubventionDetailsDto);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.REFUSEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test rejeter pour taux demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_refuser.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_rejet_taux.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRejeterPourTauxDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        Long tauxDemande = new Long("70");

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Refuser
        uri = "/api/subvention/demande/" + idDemande + "/rejet-taux";

        String inputJson = this.mapToJson(tauxDemande);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.REJET_TAUX.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test rejeter pour taux demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_refuser.xml" })
    public void testRejeterPourTauxDemandeSubventionTAuxIdentiques() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        Long tauxDemande = new Long("80");

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Refuser
        uri = "/api/subvention/demande/" + idDemande + "/rejet-taux";

        String inputJson = this.mapToJson(tauxDemande);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultRegleGestionException((mvcResult)));
    }

    /**
     * Test detecter en anomalie demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_detecter_anomalie.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_detecter_anomalie.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDetecterEnAnomalieDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = null;

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Refuser
        uri = "/api/subvention/demande/" + idDemande + "/anomalies/detecter";

        String inputJson = this.mapToJson(demandeSubventionDetailsDto);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test transferer demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_signaler_anomalie.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_signaler_anomalie.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSignalerAnomalieDemandeSubvention() throws Exception {
        // init.
        String idDemande = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = null;

        // Rechercher
        uri = "/api/subvention/demande/" + idDemande + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // Signaler en anomalie
        uri = "/api/subvention/demande/" + idDemande + "/anomalies/signaler";

        String inputJson = this.mapToJson(demandeSubventionDetailsDto);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE.name(), demandeSubventionDetailsDto.getEtat());
    }

    /**
     * Test transferer demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_recup_dotation_dispo.xml" })
    public void testRecupererDotationDisponibleDemandeSubvention() throws Exception {
        // init.
        Integer idSousProgramme = 900003;
        Long idCollectivite = 900003L;
        String uri, content;
        MvcResult mvcResult;

        // Signaler en anomalie
        uri = "/api/subvention/demande/sous-programme/" + idSousProgramme + "/collectivite/" + idCollectivite + "/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        BigDecimal dotationDispoReturn = new BigDecimal((this.mapFromJson(content, BigDecimal.class)).toString());
        BigDecimal dotationDispoToTest = new BigDecimal("47600.00");

        Assertions.assertEquals(dotationDispoReturn, dotationDispoToTest);
    }

    /**
     * Test initialiser demande subvention par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testInitialiserDemandeSubventionParId() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;

        // recherche
        uri = "/api/subvention/demande/initialiser";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL, demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    /**
     * Test initialiser demande subvention avec dossier par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testInitialiserDemandeSubventionAvecDossierParId() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto;
        String idDossier = "900001";

        // recherche
        uri = "/api/subvention/demande/dossier/" + idDossier + "/initialiser";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(ConstantesFace.CODE_DEM_SUBVENTION_INITIAL, demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    /**
     * Test creer demande subvention par id.
     *
     *
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_demande_subvention_creer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_cadencement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDemandeSubventionParId() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900003").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_demande_subvention_creer_demande_deja_en_cours.xml" })
    public void testCreerDemandeSubventionDetravauxAvecDemandeDejaEnCours() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900003").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException rge = (RegleGestionException) mvcResult.getResolvedException();

        // verification du message d'erreur : "verification de l'extention"
        Assertions.assertTrue(rge.getMessage().equals("subvention.demande.non.unique.par.programme"));

    }

    /**
     * Test creer demande subvention par id.
     *
     *
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_demande_subvention_creer.xml" })
    public void testCreerDemandeSubventionParIdPasCadencementValide() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900003").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

    }

    /**
     * Test creer demande subvention par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testCreerDemandeSubventionParIdFail() throws Exception {
        // init.
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900003").param("montantAnnee1", "200")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

    }

    /**
     * Test creer demande subvention par id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_numero_dossier_duplique.xml" })
    public void testCreerDemandeSubventionParIdNumeroDossierDuplique() throws Exception {
        // init.
        String uri;
        MvcResult mvcResult;
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(
                MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                        .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                        .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900010"))
                .andReturn();

        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException content = (RegleGestionException) mvcResult.getResolvedException();
        Assertions.assertEquals("dossier.numero.non.unique", content.getMessage());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_projet.xml" })
    public void testCreerDemandeSubventionEtDossierSubventionEtDotationCollectiviteEtDotationDepartement() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900007").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_projet_dde_existe.xml" })
    public void testCreerDemandeSubventionEtDossierSubventionEtDotationCollectiviteEtDotationDepartementAvecExistants() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900007").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_projet.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_projet.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDeuxDemandesProjet() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900007").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        DemandeSubventionDetailsDto demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());

        // seconde demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900007").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        DemandeSubventionDetailsDto demandeSubventionDetailsDto2 = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto2);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto2.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto2.getChorusNumLigneLegacy().length());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_projet_dossiers_presents.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_projet_dossiers_presents.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerUneDemandeProjetDossiersPresents() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.toString(), fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/creer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("dateJour", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("montantHTTravauxEligibles", "100").param("tauxAideFacePercent", "80").param("idCollectivite", "900003")
                .param("chorusNumLigneLegacy", "0000000001").param("version", "0").param("idSousProgramme", "900007").param("montantAnnee1", "20")
                .param("montantAnnee2", "20").param("montantAnnee3", "20").param("montantAnnee4", "20").param("estValide", "false")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        DemandeSubventionDetailsDto demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.DEMANDEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());

    }

    /**
     * Test attribuer demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_attribuer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_email_envoye.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAttribuerDemandeSubvention() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        String idDemandeSubvention = "900001";
        DemandeSubventionDetailsDto demandeSubventionDetailsDto = new DemandeSubventionDetailsDto();

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // Gestion des 3 fichiers à uploader
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.DECISION_ATTRIBUTIVE_SUBV.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // Création
        uri = "/api/subvention/demande/" + idDemandeSubvention + "/attribuer";

        // nouvelle demande de subvention
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandeSubventionDetailsDto = this.mapFromJson(content, DemandeSubventionDetailsDto.class);

        // on doit bien avoir cette demande
        Assertions.assertNotNull(demandeSubventionDetailsDto);
        Assertions.assertEquals(EtatDemandeSubventionEnum.ATTRIBUEE.name(), demandeSubventionDetailsDto.getEtat());
        Assertions.assertEquals(10, demandeSubventionDetailsDto.getChorusNumLigneLegacy().length());
    }

    /**
     * Test recuperer dossiers par collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_dossiers_collectivite_rechercher.xml" })
    public void testRecupererDossiersParCollectivite() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/subvention/demande/recuperer-dossiers/" + idCollectivite;

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

        idCollectivite = "900002";
        uri = "/api/subvention/demande/recuperer-dossiers/" + idCollectivite;

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        idCollectivite = "900003";
        uri = "/api/subvention/demande/recuperer-dossiers/" + idCollectivite;

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

}

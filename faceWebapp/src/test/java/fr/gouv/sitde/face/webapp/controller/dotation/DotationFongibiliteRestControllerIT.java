/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.dotation;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.DotationCollectiviteFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationListeLiensFongibiliteDto;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * @author a768251
 *
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationFongibiliteRestControllerIT extends FichiersUploadTestWeb {

    /**
     * Test rechercher fongibilite dotation par id collectivite et id sous programme.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_fongibilite_rechercher.xml" })
    public void testRechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme() throws Exception {

        String idCollectivite = "900001";
        String idSousProgramme = "900001";
        String uri = "/api/dotation/fongibilite/collectivite/" + idCollectivite + "/sous-programme/" + idSousProgramme + "/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationCollectiviteFongibiliteDto dotationFongibilteDto = this.mapFromJson(content, DotationCollectiviteFongibiliteDto.class);

        Assertions.assertNotNull(dotationFongibilteDto);
        Assertions.assertNotNull(dotationFongibilteDto.getDotationDisponible());
        Assertions.assertEquals(new BigDecimal(10.20).setScale(2, RoundingMode.HALF_UP), dotationFongibilteDto.getDotationDisponible());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/dotation/in_dotation_fongibilite_rechercher.xml" })
    public void testRechercherFongibiliteDotationParIdCollectivite() throws Exception {
        // init.
        String idCollectivite = "900001";
        String uri, content;
        MvcResult mvcResult;
        DotationListeLiensFongibiliteDto dotationFongibilteDto;

        // recherche
        uri = "/api/dotation/fongibilite/collectivite/" + idCollectivite + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationFongibilteDto = this.mapFromJson(content, DotationListeLiensFongibiliteDto.class);

        Assertions.assertNotNull(dotationFongibilteDto);
        // on doit bien avoir une dotation et deux liens de fongibilite pour le sous-programme 900002 vers 900001 (avec le lien vers 900013 aussi) En
        // receveur, on a 900001 donc ça prend le lien avec donneur 900003.
        Assertions.assertEquals(2, dotationFongibilteDto.getDotationCollectiviteFongibiliteDto().size());
        Assertions.assertEquals(4, dotationFongibilteDto.getLienFongibilite().size());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml",
            "/dataset/input/dotation/in_dotation_fongibilite_rechercher_pas_dotation_dispo_creditable.xml" })
    public void testRechercherFongibiliteDotationParIdCollectiviteDotationDisponibleNulleCreditable() throws Exception {
        // init.
        String idCollectivite = "900001";
        String uri, content;
        MvcResult mvcResult;
        DotationListeLiensFongibiliteDto dotationFongibilteDto;

        // recherche
        uri = "/api/dotation/fongibilite/collectivite/" + idCollectivite + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationFongibilteDto = this.mapFromJson(content, DotationListeLiensFongibiliteDto.class);

        Assertions.assertNotNull(dotationFongibilteDto);
        // on doit bien avoir une dotation et deux liens de fongibilite pour le sous-programme 900002 vers 900001 (avec le lien vers 900013 aussi) En
        // receveur, on a 900001 donc ça prend le lien avec donneur 900003 et donneur 9000013. => tout les cadencements
        Assertions.assertEquals(2, dotationFongibilteDto.getDotationCollectiviteFongibiliteDto().size());
        Assertions.assertEquals(4, dotationFongibilteDto.getLienFongibilite().size());
    }
}

package fr.gouv.sitde.face.webapp.application;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestApplication;

/**
 * The Class GestionUtilisateurApplicationIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class GestionUtilisateurApplicationIT extends AbstractTestApplication {

    /** The gestion utilisateur application. */
    @Inject
    private GestionUtilisateurApplication gestionUtilisateurApplication;

    /**
     * Test rechercher tous utilisateurs.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherTousUtilisateurs() {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setIndexPage(0);
        pageDemande.setTaillePage(50);
        PageReponse<UtilisateurAdminDto> pageUserDtos = this.gestionUtilisateurApplication.rechercherTousUtilisateurs(pageDemande);

        Assertions.assertEquals(2, pageUserDtos.getListeResultats().size());
        Assertions.assertEquals(900001, pageUserDtos.getListeResultats().get(0).getId().intValue());
    }

    /**
     * Rechercher utilisateur par email.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherUtilisateurParEmail() throws Exception {

        // Cas utilisateur existant
        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParEmail("nerepondezpas@developpementdurable.gouv.fr");

        Assertions.assertNotNull(userDto);
        Assertions.assertEquals(900001, userDto.getId().intValue());

        // Cas utilisateur inexistant
        userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParEmail("toto@planetToto.fr");
        Assertions.assertNull(userDto);
    }

    /**
     * Testsynchroniser utilisateur non existant.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_creation.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSynchroniserUtilisateurNonExistant() {
        UtilisateurAdminDto userDto = new UtilisateurAdminDto();
        userDto.setCerbereId("1234");
        userDto.setNom("Dubois");
        userDto.setPrenom("Jean");
        userDto.setEmail("jd@testInt.fr");

        userDto = this.gestionUtilisateurApplication.synchroniserUtilisateurConnecte(userDto);
        Assertions.assertNotNull(userDto.getId());
    }

    /**
     * Test synchroniser utilisateur existant.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/utilisateur/in_utilisateur_modification.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_synchronisation_modif.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSynchroniserUtilisateurExistant() {
        UtilisateurAdminDto userDto = new UtilisateurAdminDto();
        userDto.setPrenom("Jean");
        userDto.setEmail("jd@testInt.fr");
        userDto.setCerbereId("1234");
        userDto.setNom("Superman");
        userDto = this.gestionUtilisateurApplication.synchroniserUtilisateurConnecte(userDto);
        Assertions.assertEquals(900002, userDto.getId().intValue());
        // Le cerbereId a été modifié mais pas le nom
    }

    /**
     * Test synchroniser utilisateur existant.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml",
            "/dataset/input/administration/utilisateur/in_utilisateur_attente_connexion_modification.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_synchron_modif_attente_connexion.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSynchroniserUtilisateurExistantAttenteConnexion() {
        UtilisateurAdminDto userDto = new UtilisateurAdminDto();
        userDto.setPrenom("Jean");
        userDto.setEmail("jd@testInt.fr");
        userDto.setCerbereId("1234");
        userDto.setNom("Dubois");
        userDto = this.gestionUtilisateurApplication.synchroniserUtilisateurConnecte(userDto);
        Assertions.assertEquals(900002, userDto.getId().intValue());
        // Le cerbereId, le nom et le prenom ont été modifiés
    }

}

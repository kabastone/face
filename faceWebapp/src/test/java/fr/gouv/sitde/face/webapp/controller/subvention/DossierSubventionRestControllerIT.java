package fr.gouv.sitde.face.webapp.controller.subvention;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.DossierSubventionTableMferDto;
import fr.gouv.sitde.face.application.dto.DemandePaiementSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeProlongationSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionSimpleDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionDetailDto;
import fr.gouv.sitde.face.application.dto.ResultatRechercheDossierDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * Classe de test de DossierSubventionRestController.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DossierSubventionRestControllerIT extends AbstractTestWeb {

    /**
     * Dossier de subvention : recherche d'un dossier par id.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_dossier_subvention_rechercher.xml" })
    public void testRechercherDossierSubventionParId() throws Exception {
        // init.
        String idDossier = "900001";
        String uri, content;
        MvcResult mvcResult;
        DossierSubventionDetailDto dossierSubventionDetailDto;

        // init. des valeurs attendues
        BigDecimal plafondAideAttendu = new BigDecimal("56000.00");
        BigDecimal resteDispoAttendu = new BigDecimal("47500.00");
        BigDecimal tauxConsoAttendu = new BigDecimal("15.18");

        // recherche
        uri = "/api/subvention/dossier/" + idDossier + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        dossierSubventionDetailDto = this.mapFromJson(content, DossierSubventionDetailDto.class);

        // on doit bien avoir ce dossier
        Assertions.assertNotNull(dossierSubventionDetailDto);
        Assertions.assertEquals("2019CE02820010", dossierSubventionDetailDto.getNumDossier());

        // vérification des montants attendus (vérif du trigger)
        Assertions.assertEquals(plafondAideAttendu, dossierSubventionDetailDto.getPlafondAide());
        Assertions.assertEquals(resteDispoAttendu, dossierSubventionDetailDto.getResteDisponible());
        Assertions.assertEquals(tauxConsoAttendu, dossierSubventionDetailDto.getTauxConsommationCalcule());
    }

    /**
     * Dossier de subvention : lister les demandes de subvention.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_rechercher.xml" })
    public void testRechercherDemandesSubvention() throws Exception {
        // init.
        String idDossier = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeSubventionSimpleDto[] tabDemandeSubventionDto;

        // recherche
        uri = "/api/subvention/dossier/" + idDossier + "/demandes-subvention/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDemandeSubventionDto = this.mapFromJson(content, DemandeSubventionSimpleDto[].class);

        // on doit bien avoir ces demandes
        Assertions.assertNotNull(tabDemandeSubventionDto);
        Assertions.assertEquals(5, tabDemandeSubventionDto.length);
    }

    /**
     * Dossier de subvention : lister les demandes de paiement.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_paiement_rechercher.xml" })
    public void testRechercherDemandesPaiement() throws Exception {
        // init.
        String idDossier = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandePaiementSimpleDto[] tabDemandePaiementDto;

        // recherche
        uri = "/api/subvention/dossier/" + idDossier + "/demandes-paiement/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDemandePaiementDto = this.mapFromJson(content, DemandePaiementSimpleDto[].class);

        // on doit bien avoir ces demandes
        Assertions.assertNotNull(tabDemandePaiementDto);
        Assertions.assertEquals(3, tabDemandePaiementDto.length);
    }

    /**
     * Dossier de subvention : lister les demandes de prolongation.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_prolongation_rechercher.xml" })
    public void testRechercherDemandesProlongation() throws Exception {
        // init.
        String idDossier = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandeProlongationSimpleDto[] tabDemandeProlongationDto;

        // recherche
        uri = "/api/subvention/dossier/" + idDossier + "/demandes-prolongation/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDemandeProlongationDto = this.mapFromJson(content, DemandeProlongationSimpleDto[].class);

        // on doit bien avoir ces demandes
        Assertions.assertNotNull(tabDemandeProlongationDto);
        Assertions.assertEquals(3, tabDemandeProlongationDto.length);
    }

    /**
     * Test rechercher dossiers par criteres.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_rechercher_dossier_par_critere.xml" })
    public void testRechercherDossiersParCriteres() throws Exception {

        CritereRechercheDossierPourDemandeSubventionQo criteresQo = new CritereRechercheDossierPourDemandeSubventionQo();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        criteresQo.setPageDemande(pageDemande);

        // recherche
        String uri = "/api/subvention/dossier/criteres/rechercher";

        // Test critères vides
        String inputJson = this.mapToJson(criteresQo);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Test pas de resultat
        criteresQo.setIdCollectivite(900001L);
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<ResultatRechercheDossierDto> pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertTrue(pageReponseResultat.getListeResultats().isEmpty());

        // Test reussite multi resultats
        criteresQo.setIdCollectivite(900003L);
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertEquals(2, pageReponseResultat.getListeResultats().size());

        // Test reussite resultat unique
        criteresQo.setNumDossier("2019CE02820010");
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // Test sur etat demande de subvention
        criteresQo.setCodeEtatDemande(EtatDemandeSubventionEnum.QUALIFIEE.name());
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertEquals(1, pageReponseResultat.getListeResultats().size());

        // (Montant aide = 10000*80/100 + 3000*80/100 )
        Assertions.assertEquals(10400, pageReponseResultat.getListeResultats().get(0).getPlafondAide().intValue());

    }

    /**
     * Test rechercher dossiers par criteres demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_rechercher_dossier_par_critere.xml" })
    public void testRechercherPaiementsParCriteres() throws Exception {

        CritereRechercheDossierPourDemandePaiementQo criteresQo = new CritereRechercheDossierPourDemandePaiementQo();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        criteresQo.setPageDemande(pageDemande);

        // recherche
        String uri = "/api/paiement/dossier/criteres/rechercher";

        // Test critères vides
        String inputJson = this.mapToJson(criteresQo);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Test pas de resultat
        criteresQo.setIdCollectivite(900001L);
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<ResultatRechercheDossierDto> pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertTrue(pageReponseResultat.getListeResultats().isEmpty());

        // Test reussite multi resultats
        criteresQo.setIdCollectivite(900003L);
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertEquals(2, pageReponseResultat.getListeResultats().size());
        Assertions.assertEquals(2, pageReponseResultat.getNbTotalResultats());

        // Test reussite resultat unique
        criteresQo.setNumDossier("2019CE02820010");
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // Test sur etat demande de subvention
        criteresQo.setCodeEtatDemande(EtatDemandeSubventionEnum.QUALIFIEE.name());
        inputJson = this.mapToJson(criteresQo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        pageReponseResultat = this.mapPageReponseFromJson(content, ResultatRechercheDossierDto.class);
        Assertions.assertEquals(1, pageReponseResultat.getListeResultats().size());
        Assertions.assertEquals(1, pageReponseResultat.getNbTotalResultats());

        // (Montant aide = 10000*80/100 + 3000*80/100 )
        Assertions.assertEquals("2019CE02820010", pageReponseResultat.getListeResultats().get(0).getNumDossier());

        
    }
    
    /**
     * Cadencement : lister les dossiers subventions ayant cadencement en defauts.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_dossier_subvention_defaut_rechercher.xml" })
    public void testRechercherDossierSubventionDefaut() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DossierSubventionTableMferDto[] dossierSubventionDefautDto;
        
        String codeProgramme = "793";
        // recherche
        uri = "/api/subvention/dossier/defaut/" + codeProgramme + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        dossierSubventionDefautDto = this.mapFromJson(content, DossierSubventionTableMferDto [].class);

        // on doit bien avoir ces demandes
        Assertions.assertNotNull(dossierSubventionDefautDto);
        Assertions.assertEquals(1, dossierSubventionDefautDto.length);
    }
    
    /**
     * Cadencement : lister les dossiers subventions ayant cadencement en defauts.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_dossier_subvention_defaut_rechercher.xml" })
    public void testRechercherDossierSubventionRetard() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        DossierSubventionTableMferDto[] dossierSubventionDefautDto;
        
        String codeProgramme = "793";
        // recherche
        uri = "/api/subvention/dossier/retard/" + codeProgramme + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        dossierSubventionDefautDto = this.mapFromJson(content, DossierSubventionTableMferDto [].class);

        // on doit bien avoir ces demandes
        Assertions.assertNotNull(dossierSubventionDefautDto);
        Assertions.assertEquals(1, dossierSubventionDefautDto.length);
    }

}

package fr.gouv.sitde.face.webapp.controller.paiement;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DemandePaiementDto;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * Classe de test de DemandePaiementRestController.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DemandePaiementRestControllerIT extends FichiersUploadTestWeb {

    @Inject
    DemandePaiementService demandePaiementService;

    /**
     * Demande de paiement : initialiser.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_init_demande_paiement.xml" })
    public void testInitNouvelleDemandePaiement() throws Exception {
        // init.
        String idDossierSubvention = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandePaiementDto demandePaiementDto;
        BigDecimal resteConsommer = new BigDecimal("39000");
        resteConsommer = resteConsommer.setScale(2);

        // initialisation nouvelle demande de paiement
        uri = "/api/paiement/demande/dossier/" + idDossierSubvention + "/initialiser";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandePaiementDto = this.mapFromJson(content, DemandePaiementDto.class);

        // vérifs de la demande initialisée
        Assertions.assertNotNull(demandePaiementDto);
        Assertions.assertEquals("45723A2AF3788C", demandePaiementDto.getNumDossier());
        Assertions.assertEquals(ConstantesFace.CODE_DEM_PAIEMENT_INITIAL, demandePaiementDto.getCodeEtatDemande());
        Assertions.assertEquals(0, resteConsommer.compareTo(demandePaiementDto.getResteConsommer()));
        Assertions.assertEquals(new BigDecimal("5000.00"), demandePaiementDto.getMontantTotalHtTravaux());
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + maj existante).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_realiser_demande_paiement.xml" })
    public void testRealiserDemandePaiement() throws Exception {
        // init.
        String idDossierSubvention = "900001";
        String idDemandeExistante = "900003";
        String uri, content;
        MvcResult mvcResult;
        DemandePaiementDto demandePaiementDto = new DemandePaiementDto();
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        // gestion des 8 fichiers à uploader (on soumet 8x le même)
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer4 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer5 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_FINA_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer6 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_FINANCIER_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer7 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_TECH_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer8 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // nouvelle demande de paiement
        uri = "/api/paiement/demander";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .file(fichierEnvoyer4).file(fichierEnvoyer5).file(fichierEnvoyer6).file(fichierEnvoyer7).file(fichierEnvoyer8)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", ConstantesFace.CODE_DEM_PAIEMENT_INITIAL).param("typeDemande", "ACOMPTE").param("montantTravauxHt", "123")
                .param("tauxAideFacePercent", "20").param("version", "0")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandePaiementDto = this.mapFromJson(content, DemandePaiementDto.class);

        // vérifications
        Assertions.assertEquals(idDossierSubvention, String.valueOf(demandePaiementDto.getIdDossierSubvention()));
        Assertions.assertEquals(EtatDemandePaiementEnum.DEMANDEE.toString(), demandePaiementDto.getCodeEtatDemande());

        // maj demande de paiement (montants incorrects - AVANCE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "3000000").param("tauxAideFacePercent", "80")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // maj demande de paiement (montants incorrects - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "3000000").param("tauxAideFacePercent", "20")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // maj demande de paiement (taux incorrects)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "3000000").param("tauxAideFacePercent", "200")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // maj demande de paiement (montant negatif)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "-3000000").param("tauxAideFacePercent", "20")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // maj demande de paiement (taux negatif)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "3000000").param("tauxAideFacePercent", "-3")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + maj existante).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_realiser_demande_paiement.xml" })
    public void testRealiserDemandePaiementPartie2() throws Exception {
        // init.
        String idDossierSubvention = "900001";
        String idDemandeExistante = "900003";
        String uri;
        MvcResult mvcResult;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer5 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_FINA_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer6 = new MockMultipartFile(TypeDocumentEnum.ETAT_ACHEVEMENT_FINANCIER_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        uri = "/api/paiement/demander";

        // maj demande de paiement - maj des documents
        mvcResult = this.mvc.perform(
                MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer5).file(fichierEnvoyer6).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                        .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                        .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                        .param("codeEtatDemande", EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString()).param("typeDemande", "SOLDE")
                        .param("montantTravauxHt", "300").param("tauxAideFacePercent", "20"))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + maj existante).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_realiser_demande_paiement.xml" })
    public void testRealiserDemandePaiementPartie3() throws Exception {
        // init.
        String idDossierSubvention = "900001";
        String idDemandeExistante = "900003";
        String uri;
        MvcResult mvcResult;

        uri = "/api/paiement/demander";

        // Vérif (avance non versée existante - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idDemande", idDemandeExistante).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "3000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_avance.xml" })
    public void testRealiserDemandePaiementAvanceNonVersee() throws Exception {
        String uri = "/api/paiement/demander";

        String idDemandeAvance = "900001";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (avance non versée existante - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "3000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Vérif (avance non versée existante - SOLDE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "SOLDE").param("montantTravauxHt", "3000")
                .param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Vérif (avance non versée existante - AVANCE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDemande", idDemandeAvance)
                .param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "3000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_avance_multiples.xml" })
    public void testRealiserDemandePaiementAvanceMultiples() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (avance non versée existante - AVANCE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "AVANCE")
                .param("montantTravauxHt", "3000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertFalse(isResultRegleGestionException(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_realiser_demande_paiement_deja_en_cours.xml" })
    public void testRealiserDemandePaiementAvecDemandeDejaEnCours() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (avance non versée existante - AVANCE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "3000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        RegleGestionException rge = (RegleGestionException) mvcResult.getResolvedException();

        // verification du message d'erreur
        Assertions.assertTrue(rge.getMessage().equals("paiement.demande.non.unique"));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml",
            "/dataset/input/paiement/in_realiser_demande_paiement_verif_acompte_inferieur_avances.xml" })
    public void testRealiserDemandePaiementAcompteInferieurAvances() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "200").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Vérif (montant supérieur aux avances - AVANCE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "10000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml",
            "/dataset/input/paiement/in_realiser_demande_paiement_verif_acompte_inferieur_avances.xml" })
    public void testRealiserDemandePaiementDeuxAcompteInferieurAvances() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant superieur aux avances - ACOMPTE => Passe)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.VERSEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "10000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // Vérif (montant inferieur aux avances - ACOMPTE => doit passer)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "200").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_solde_apres_avances.xml" })
    public void testRealiserDemandePaiementAcompteApresAvances() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "1000").param("tauxAideFacePercent", "50")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "9000").param("tauxAideFacePercent", "50")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_solde_apres_avances.xml" })
    public void testRealiserDemandePaiementAcompteSuperieurApresAvances() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "9900").param("tauxAideFacePercent", "50")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_solde_apres_avances.xml" })
    public void testRealiserDemandePaiementAcompteTauxDecimal() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "9900").param("tauxAideFacePercent", "49.5")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_solde_apres_avances.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_realiser_demande_paiement_verif_solde_apres_avances.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRealiserDemandePaiementSoldeApresAvances() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant supérieur aux avances - SOLDE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "SOLDE")
                .param("montantTravauxHt", "11000")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_verif_avance_apres_acompte.xml" })
    public void testRealiserDemandePaiementAvanceApresAcompteImpossible() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "AVANCE").param("montantTravauxHt", "200")
                .param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_avance_defalquee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_realiser_demande_paiement_avance_defalquee.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRealiserDemandePaiementAcompteAvanceDefalquee() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "50000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Demande de paiement : réaliser une demande (nouvelle + verif état avance).
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_avance_defalquee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_realiser_deux_demande_paiement_avance_defalquee.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRealiserDemandePaiementDeuxAcompteAvanceDefalquee() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "50000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/paiement/in_realiser_demande_paiement_avance_defalqueePartie2.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_realiser_deux_demande_paiement_avance_defalqueePartie2.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRealiserDemandePaiementDeuxAcompteAvanceDefalqueePartie2() throws Exception {
        String uri = "/api/paiement/demander";

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();

        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer3 = new MockMultipartFile(TypeDocumentEnum.ETAT_REALISATION_TRAVAUX_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        String idDossierSubvention = "900001";
        MvcResult mvcResult;

        // Vérif (montant inférieur aux avances - ACOMPTE)
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2).file(fichierEnvoyer3)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDossierSubvention", idDossierSubvention)
                .param("dateDemande", LocalDateTime.now().format(DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE)))
                .param("codeEtatDemande", EtatDemandePaiementEnum.DEMANDEE.toString()).param("typeDemande", "ACOMPTE")
                .param("montantTravauxHt", "50000").param("tauxAideFacePercent", "10")).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Demande de paiement : consulter.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_consulter_demande_paiement.xml" })
    public void testConsulterDemandePaiement() throws Exception {
        // init.
        String idDemandePaiement = "900001";
        String uri, content;
        MvcResult mvcResult;
        DemandePaiementDto demandePaiementDto;
        BigDecimal resteConsommer = new BigDecimal("39000"); // string = bonne pratique
        resteConsommer = resteConsommer.setScale(2);

        // consultation demande de paiement existante
        uri = "/api/paiement/demande/" + idDemandePaiement + "/details";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        demandePaiementDto = this.mapFromJson(content, DemandePaiementDto.class);

        // vérifications des calculs
        Assertions.assertNotNull(demandePaiementDto);
        Assertions.assertEquals("45723A2AF3788C", demandePaiementDto.getNumDossier());
        // => calcul vérifié du reste à consommer :
        // plafond d'aide : 200 000 x 20 / 100 = 40 000 (une seule demande de subvention)
        // aide demandee : 500 x 2 = 1 000 (tous les paiements non refusés)
        // 40 000 - 1000 = 39000.00
        //
        // ATTENTION : il faut faire un compareTo() avec les BigDecimal, car sinon un equals()
        // vérifie l'égalité à TOUS les niveaux (donc même au niveau du scale)
        Assertions.assertEquals(0, resteConsommer.compareTo(demandePaiementDto.getResteConsommer()));
    }

    /**
     * Test generation donnees et document pdf.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_pdf_demande_paiement.xml" })
    public void testGenererDecisionAttributive() throws Exception {
        // init.
        String idDemande = "900002";
        String uri, contentDisposition;
        MvcResult mvcResult;

        // recherche
        uri = "/api/paiement/demande/" + idDemande + "/generation-decision-attributive";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        contentDisposition = mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION);
        Assertions.assertNotNull(mvcResult.getResponse().getContentAsString());
        String[] list = contentDisposition.split("=");
        Assertions.assertTrue(list[0].equals("attachment; filename"));
        Assertions.assertTrue(list[1].endsWith(".pdf"));
        Assertions.assertTrue(
                mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_TYPE).contains(ExtensionFichierEnum.PDF.getContentType()));
    }

    /**
     * Mise à jour de la demande de paiement vers : ANOMALIE_DETECTEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_anomalie_detectee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_anomalie_detectee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandeAnomalieDetectee() throws Exception {
        // mise à jour de la demande
        String uri = "/api/paiement/demande/900011/maj-etat/anomalie-detectee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : ANOMALIE_SIGNALEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_anomalie_signalee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_anomalie_signalee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandePaiementAnomalieSignalee() throws Exception {
        // mise à jour de la demande
        String uri = "/api/paiement/demande/900011/maj-etat/anomalie-signalee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : REFUSEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_refusee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_refusee.xml" })
    public void testMajDemandePaiementRefusee() throws Exception {
        // init.
        String uri = "/api/paiement/demande/900011/maj-etat/refusee";
        DemandePaiementDto demandePaiementDto = new DemandePaiementDto();
        demandePaiementDto.setMotifRefus("test motif refus");
        String inputJson = this.mapToJson(demandePaiementDto);

        // mise à jour de la demande
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : QUALIFIEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_qualifiee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_qualifiee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandeQualifiee() throws Exception {
        // init.
        String idDemande = "900011";
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierEnvoyer = new MockMultipartFile(TypeDocumentEnum.DECISION_ATTRIBUTIVE_PAIE.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // mise à jour de la demande
        String uri = "/api/paiement/demande/maj-etat/qualifiee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDemande", idDemande)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : ACCORDEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_accordee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_accordee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandePaiementAccordee() throws Exception {
        // mise à jour de la demande
        String uri = "/api/paiement/demande/900011/maj-etat/accordee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : CONTROLEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_controlee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_controlee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandePaiementControlee() throws Exception {
        // mise à jour de la demande
        String uri = "/api/paiement/demande/900011/maj-etat/controlee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : EN_ATTENTE_TRANSFERT.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_en_cours_transfert.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_en_attente_transfert.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandePaiementEnAttenteTransfert() throws Exception {
        // mise à jour de la demande
        String uri = "/api/paiement/demande/900011/maj-etat/en-attente-transfert";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Mise à jour de la demande de paiement vers : CORRIGEE.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/paiement/in_maj_paiement_corrigee.xml" })
    @ExpectedDatabase(value = "/dataset/expected/paiement/ex_maj_paiement_corrigee.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testMajDemandePaiementCorrigee() throws Exception {
        // init.
        String idDemande = "900011";
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierEnvoyer1 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        MockMultipartFile fichierEnvoyer2 = new MockMultipartFile(TypeDocumentEnum.ETAT_MARCHES_PASSES_TABLEUR.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        // mise à jour de la demande
        String uri = "/api/paiement/demande/maj-etat/corrigee";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer1).file(fichierEnvoyer2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE).param("idDemande", idDemande)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }
}

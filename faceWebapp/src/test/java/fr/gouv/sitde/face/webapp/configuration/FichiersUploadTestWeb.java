/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * Classe fournissant le répertoire d'upload en environnement de test ainsi que 2 fichiers PDF temporaires de test.
 *
 * @author Atos
 *
 */
public class FichiersUploadTestWeb extends AbstractTestWeb {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FichiersUploadTestWeb.class);

    /** The Constant CHEMIN_REPERTOIRE_UPLOAD. */
    public static final String CHEMIN_REPERTOIRE_UPLOAD = "src/test/resources/upload/";

    /** The Constant CHEMIN_REPERTOIRE_UPLOAD_SOURCE. */
    protected static final String CHEMIN_REPERTOIRE_UPLOAD_SOURCE = CHEMIN_REPERTOIRE_UPLOAD + "source/";

    /** Fichier PDF temporaire de test. */
    protected static File tempFile1;

    /** Fichier PDF temporaire de test. */
    protected static File tempFile2;

    /** Répertoire d'upload pour les tests. */
    protected static File uploadDir;

    /**
     * Sets the up before class. <br/>
     * Création du repertoire d'upload s'il n'existe pas.
     */
    @BeforeAll
    public static void setUpBeforeClass() {
        if ((uploadDir == null) || !uploadDir.exists()) {
            String cheminUploadDir = CHEMIN_REPERTOIRE_UPLOAD + "upload-test";
            uploadDir = new File(cheminUploadDir);
            uploadDir.mkdir();
            LOGGER.debug("Creation du repertoire d'upload de test");
        }
    }

    /**
     * On initialise 2 fichiers temporaires qu'on pourra utiliser en tant que fichiers mock.
     */
    @BeforeEach
    public void setup() {
        try {
            if ((tempFile1 == null) || !tempFile1.exists()) {
                String cheminFichier1 = CHEMIN_REPERTOIRE_UPLOAD + "fichier_test_1.pdf";
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source_1.pdf"), new File(cheminFichier1));
                tempFile1 = new File(cheminFichier1);
            }
            if ((tempFile2 == null) || !tempFile2.exists()) {
                String cheminFichier2 = CHEMIN_REPERTOIRE_UPLOAD + "fichier_test_2.pdf";
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE + "fichier_source_2.pdf"), new File(cheminFichier2));
                tempFile2 = new File(cheminFichier2);
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
    }

    /**
     * Nettoyage des 2 fichiers temporaires potentiellement créés ainsi que du repertoire d'upload.
     */
    @AfterAll
    public static void tearDownAfterClass() {
        if (tempFile1.exists()) {
            tempFile1.delete();
        }
        if (tempFile2.exists()) {
            tempFile2.delete();
        }

        if (uploadDir.exists()) {
            // Commenter le bloc ci-dessous si on veut lire les éventuels fichiers générés par le test.

            try {
                FileUtils.forceDelete(uploadDir);
                LOGGER.debug("Suppression du repertoire de test d'upload");
            } catch (IOException e) {
                throw new TechniqueException(e);
            }

        }
    }

    /**
     * Gets the fichier transfert temp file 1.
     *
     * @return the fichier transfert temp file 1
     */
    protected static FichierTransfert getFichierTransfertTempFile1() {
        return getFichierTransfertFromPdfFile(tempFile1);
    }

    /**
     * Gets the fichier transfert temp file 2.
     *
     * @return the fichier transfert temp file 2
     */
    protected static FichierTransfert getFichierTransfertTempFile2() {
        return getFichierTransfertFromPdfFile(tempFile2);
    }

    /**
     * Gets the fichier transfert from pdf file.
     *
     * @param file
     *            the file
     * @return the fichier transfert from pdf file
     */
    private static FichierTransfert getFichierTransfertFromPdfFile(File file) {
        FichierTransfert fichierTransfert = new FichierTransfert();
        try {
            fichierTransfert.setBytes(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new TechniqueException(e);
        }
        fichierTransfert.setContentTypeTheorique(ExtensionFichierEnum.PDF.getContentType());
        fichierTransfert.setNomFichier(file.getName());

        return fichierTransfert;
    }
}

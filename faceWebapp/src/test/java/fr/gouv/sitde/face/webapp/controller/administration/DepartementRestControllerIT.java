package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class CollectiviteRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DepartementRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/administration/departement/in_adresse_rechercher.xml" })
    public void testRechercherAdresseDepartement() throws Exception {

        String idDepartement = "900004";
        String uri = "/api/administration/departement/" + idDepartement + "/adresse/recherche";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        AdresseDto adresseDto = this.mapFromJson(content, AdresseDto.class);

        Assertions.assertNotNull(adresseDto);
        Assertions.assertEquals(Long.valueOf("900001"), adresseDto.getId());

        idDepartement = "900001";
        uri = "/api/administration/departement/" + idDepartement + "/adresse/recherche";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertEquals(mvcResult.getResponse().getContentAsString(), "");

        idDepartement = "899999";
        uri = "/api/administration/departement/" + idDepartement + "/adresse/recherche";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertEquals(mvcResult.getResponse().getContentAsString(), "");
    }

    @Test
    @DatabaseSetup({ "/dataset/input/administration/departement/in_adresse_rechercher.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/departement/ex_departement_ajout.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAjoutAdresseDepartement() throws Exception {
        AdresseDto adresse = new AdresseDto();
        adresse.setNom("Toto");
        adresse.setCodePostal("13133");
        adresse.setCommune("Traves-sous-Lambesc");
        adresse.setNomVoie("Rue des trois frères");

        String inputJson = this.mapToJson(adresse);

        String idDepartement = "900003";
        String uri = "/api/administration/departement/" + idDepartement + "/adresse/enregistrer";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, inputJson).contentType(MediaType.APPLICATION_JSON).content(inputJson)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        AdresseDto adresseDto = this.mapFromJson(content, AdresseDto.class);

        adresseDto.setCivilite("Monsieur");
        inputJson = this.mapToJson(adresseDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, inputJson).contentType(MediaType.APPLICATION_JSON).content(inputJson)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        idDepartement = "900008";
        uri = "/api/administration/departement/" + idDepartement + "/adresse/enregistrer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, adresse).contentType(MediaType.APPLICATION_JSON).content(inputJson)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultErreurServeur(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/administration/departement/in_adresse_rechercher.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/departement/ex_departement_donnees_fiscales.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testEnregistrerDonneesFiscalesDepartement() throws Exception {

        DepartementDto depDto = new DepartementDto();

        Integer idDepartement = 900003;

        depDto.setId(idDepartement);
        depDto.setCodique("codique");
        depDto.setNomCodique("nomCodique");
        String uri = "/api/administration/departement/" + idDepartement + "/donnees-fiscales/enregistrer";

        String inputJson = this.mapToJson(depDto);

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, inputJson).contentType(MediaType.APPLICATION_JSON).content(inputJson)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        depDto = this.mapFromJson(content, DepartementDto.class);

        Assertions.assertTrue(depDto.getCodique().equals("codique"));
        Assertions.assertTrue(depDto.getNomCodique().equals("nomCodique"));

        idDepartement = 3;
        depDto.setId(idDepartement);
        inputJson = this.mapToJson(depDto);
        uri = "/api/administration/departement/" + idDepartement + "/donnees-fiscales/enregistrer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, inputJson).contentType(MediaType.APPLICATION_JSON).content(inputJson)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultErreurServeur(mvcResult));

    }
}

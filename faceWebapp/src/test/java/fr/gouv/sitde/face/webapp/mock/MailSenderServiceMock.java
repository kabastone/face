package fr.gouv.sitde.face.webapp.mock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.infrastructure.email.MailSenderServiceImpl;
import fr.gouv.sitde.face.transverse.entities.Email;

/**
 * Mock du service qui assure l'envoi des emails dans l'application.
 *
 */
@Component
@Profile({ "test", "testpic" })
public class MailSenderServiceMock extends MailSenderServiceImpl {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MailSenderServiceMock.class);

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.infrastructure.email.MailSenderServiceImpl#envoyerMail(fr.gouv.sitde.face.transverse.entities.Email)
     */
    @Override
    public void envoyerMail(final Email email) {
        LOGGER.info("=== BOUCHON d'envoi de l'email " + email.getObjet() + " ===");
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.infrastructure.email.MailSenderServiceImpl#envoyerMail(fr.gouv.sitde.face.transverse.entities.Email)
     */
    @Override
    public Boolean envoyerEmailContact(final Email email, String adresseEmailContact) {
        LOGGER.info("=== BOUCHON d'envoi de l'email " + email.getObjet() + " ===");
        return true;
    }
}

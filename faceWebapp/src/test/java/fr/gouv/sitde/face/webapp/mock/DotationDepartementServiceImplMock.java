/**
 *
 */
package fr.gouv.sitde.face.webapp.mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domaine.service.dotation.impl.DotationDepartementServiceImpl;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class DotationDepartementServiceImplMock.
 */
@Component
@Profile({ "test", "testpic" })
public class DotationDepartementServiceImplMock extends DotationDepartementServiceImpl {

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);

    @Override
    protected LocalDateTime now() {
        return LocalDateTime.of(LocalDate.parse("16/09/2019", formatter), LocalTime.MIDNIGHT);
    }
}

package fr.gouv.sitde.face.webapp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class AccueilControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AccueilControllerIT extends AbstractTestWeb {

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testAccueillirIndexGeneral() throws Exception {

        String uri = "/";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(AbstractTestWeb.CODE_HTTP_REDIRECT == mvcResult.getResponse().getStatus());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testAccueillirIndexApp() throws Exception {

        String uri = "/app";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(AbstractTestWeb.CODE_HTTP_REDIRECT == mvcResult.getResponse().getStatus());
    }
}

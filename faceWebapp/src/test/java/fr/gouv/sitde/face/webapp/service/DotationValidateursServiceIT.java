/**
 *
 */
package fr.gouv.sitde.face.webapp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestApplication;

/**
 * The Class DotationValidateursServiceIT.
 *
 * @author Atos
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationValidateursServiceIT extends AbstractTestApplication {

    /** The dotation validateur service. */
    @Inject
    private DotationValidateursService dotationValidateurService;

    /** The dotation programme repository. */
    @Inject
    private DotationProgrammeRepository dotationProgrammeRepository;

    /** The dotation sous programme repository. */
    @Inject
    private DotationSousProgrammeRepository dotationSousProgrammeRepository;

    @Inject
    private DotationCollectiviteRepository dotationCollectiviteRepository;

    /**
     * Test params nulls.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testParamsNulls() {

        DotationCollectivite dotationCollectiviteVide = new DotationCollectivite();

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerProgrammeAvecSousProgrammes(null, null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerSousProgrammeAvecProgrammeInitial(null, null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerSousProgrammeAvecDepartements(null, null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerDepartementAvecSousProgramme(Long.valueOf(0), null, null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerDepartementImportAvecSousProgramme(Long.valueOf(0), null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerCollectiviteAvecDepartement(dotationCollectiviteVide, null, null);
        });

        // montant null
        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerCollectiviteAvecSubventions(dotationCollectiviteVide, null, null);
        });

    }

    /**
     * Test null value sgbd.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testNullValueSgbd() {

        BigDecimal montantMock = BigDecimal.valueOf(0);
        Long idMock = Long.valueOf(0);

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerProgrammeAvecSousProgrammes(null, montantMock);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerSousProgrammeAvecProgrammeInitial(null, montantMock);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerSousProgrammeAvecDepartements(null, montantMock);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerDepartementAvecSousProgramme(idMock, montantMock, null);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerDepartementImportAvecSousProgramme(idMock, montantMock);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerCollectiviteAvecDepartement(null, montantMock, null);
        });

        Assertions.assertThrows(TechniqueException.class, () -> {
            this.dotationValidateurService.validerCollectiviteAvecSubventions(null, montantMock, null);
        });
    }

    /**
     * Test CUDOT 401.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT401() {

        BigDecimal montantMock = BigDecimal.valueOf(100);
        DotationProgramme dotationProgramme = this.dotationProgrammeRepository.rechercherDotationProgrammeParId(900000L);

        // CU_DOT_401
        // 100 < 5000 attendu : erreur
        try {
            this.dotationValidateurService.validerProgrammeAvecSousProgrammes(dotationProgramme, montantMock);
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.programme.sous.dote", e.getMessage());
        }

        // CU_DOT_401
        // 5000 <= 5000 attendu : OK pas d'erreur
        montantMock = BigDecimal.valueOf(5000);
        try {
            this.dotationValidateurService.validerProgrammeAvecSousProgrammes(dotationProgramme, montantMock);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }

    }

    /**
     * Test CUDOT 411.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT411() {

        DotationSousProgramme dotationSousProgramme = this.dotationSousProgrammeRepository.rechercherDotationSousProgrammeParId(900001L);

        // CU_DOT_411
        // 5000 - 4000 + 14500 > 150000 attendu : erreur
        BigDecimal montantMock = BigDecimal.valueOf(14500);
        try {
            this.dotationValidateurService.validerSousProgrammeAvecProgrammeInitial(dotationSousProgramme, montantMock);
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.sous.programme.sur.dote", e.getMessage());
        }

        // CU_DOT_411
        // 5000 - 4000 + 6000 <= 150000 attendu : OK pas d'erreur
        montantMock = BigDecimal.valueOf(6000);
        try {
            this.dotationValidateurService.validerSousProgrammeAvecProgrammeInitial(dotationSousProgramme, montantMock);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }
    }

    /**
     * Test CUDOT 412.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT412() {
        DotationSousProgramme dotationSousProgramme = this.dotationSousProgrammeRepository.rechercherDotationSousProgrammeParId(900001L);

        // CU_DOT_412
        // 2000 < 3000 attendu : erreur
        BigDecimal montantMock = BigDecimal.valueOf(2000);
        try {
            this.dotationValidateurService.validerSousProgrammeAvecDepartements(dotationSousProgramme, montantMock);
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.sous.programme.sous.dote", e.getMessage());
        }

        // CU_DOT_412
        // 3000 >= 3000 attendu : OK pas d'erreur
        montantMock = BigDecimal.valueOf(3000);
        try {
            this.dotationValidateurService.validerSousProgrammeAvecDepartements(dotationSousProgramme, montantMock);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }

    }

    /**
     * Test CUDOT 421.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT421() {
        // CU_DOT_421
        // 3000 - 2000 + 3500 > 4000 attendu : erreur
        Long idMock = Long.valueOf(900001);
        BigDecimal montantMock = BigDecimal.valueOf(3500);
        try {
            this.dotationValidateurService.validerDepartementAvecSousProgramme(idMock, montantMock, TypeDotationDepartementEnum.EXCEPTIONNELLE);
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.departement.sur.dote", e.getMessage());
        }

        // CU_DOT_421
        // 3000 - 2000 + 1000 <= 4000 attendu : pas d'erreur
        idMock = Long.valueOf(900001);
        montantMock = BigDecimal.valueOf(1000);
        try {
            this.dotationValidateurService.validerDepartementAvecSousProgramme(idMock, montantMock, TypeDotationDepartementEnum.EXCEPTIONNELLE);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }
    }

    /**
     * Test CUDOT 422.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT422() {
        // CU_DOT_422
        // 3000 + 2000 > 4000 attendu : erreur
        Long idMock = Long.valueOf(900001);
        BigDecimal montantMock = BigDecimal.valueOf(3000);
        try {
            this.dotationValidateurService.validerDepartementImportAvecSousProgramme(idMock, montantMock);
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.departement.sur.dote.import", e.getMessage());
        }

        // CU_DOT_422
        // 2000 + 2000 <= 4000 attendu : OK pas d'erreurs
        idMock = Long.valueOf(900001);
        montantMock = BigDecimal.valueOf(2000);
        try {
            this.dotationValidateurService.validerDepartementImportAvecSousProgramme(idMock, montantMock);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }
    }

    /**
     * Test CUDOT 431.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur.xml" })
    public void testCUDOT431() {
        // CU_DOT_431
        // 700 - 500 + 400 > 450 attendu : erreur

        DotationCollectivite dotationCollectivite = this.dotationCollectiviteRepository.rechercherDotationCollectiviteParId(900001L);
        BigDecimal montantMock = BigDecimal.valueOf(400);
        try {

            List<MessageProperty> listeErreurs = new ArrayList<>();
            this.dotationValidateurService.validerCollectiviteAvecDepartement(dotationCollectivite, montantMock, listeErreurs);

            // Envoi des exceptions
            if (!listeErreurs.isEmpty()) {
                throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
            }
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.departement.sur.dote", e.getMessage());
        }

        // CU_DOT_431
        // 700 - 500 + 100 <= 450 attendu : OK pas d'erreur

        montantMock = BigDecimal.valueOf(100);
        try {
            this.dotationValidateurService.validerCollectiviteAvecDepartement(dotationCollectivite, montantMock, null);
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }
    }

    /**
     * Test CUDOT 432.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_validateur_col_sous_dot.xml" })
    public void testCUDOT432() {
        // CU_DOT_432
        // 600 (dotation_collectivite_saisi) > 1000 (dotation_repartie) => test faux donc erreur

        DotationCollectivite dotationCollectivite = this.dotationCollectiviteRepository.rechercherDotationCollectiviteParId(900001L);
        BigDecimal montantMock = BigDecimal.valueOf(600);
        try {
            List<MessageProperty> listeErreurs = new ArrayList<>();
            this.dotationValidateurService.validerCollectiviteAvecSubventions(dotationCollectivite, montantMock, listeErreurs);

            // Envoi des exceptions
            if (!listeErreurs.isEmpty()) {
                throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
            }
            // on doit lever une exception donc ne pas passer ci dessous
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof RegleGestionException);
            Assertions.assertEquals("dotation.collectivite.sous.dote", e.getMessage());
        }

        // CU_DOT_432
        // 3500 (dotation_collectivite_saisi) > 1000 (dotation_repartie) => test vrai

        montantMock = BigDecimal.valueOf(3500);
        try {
            List<MessageProperty> listeErreurs = new ArrayList<>();
            this.dotationValidateurService.validerCollectiviteAvecSubventions(dotationCollectivite, montantMock, listeErreurs);
            // Envoi des exceptions
            if (!listeErreurs.isEmpty()) {
                throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
            }
        } catch (Exception e) {
            // si on a une exception : KO
            Assertions.fail();
        }
    }

}

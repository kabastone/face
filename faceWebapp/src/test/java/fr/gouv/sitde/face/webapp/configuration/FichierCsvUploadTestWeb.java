/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * Classe fournissant les resources des test de fichiers CSV
 *
 * Utilise les mécanismes mis en place dans FichiersUploadTestWeb ainsi que les dossiers de travail
 * fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb.CHEMIN_REPERTOIRE_UPLOAD
 * fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb.CHEMIN_REPERTOIRE_UPLOAD_SOURCE
 *
 * @see FichiersUploadTestWeb
 *
 */
public class FichierCsvUploadTestWeb extends AbstractTestWeb {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FichierCsvUploadTestWeb.class);

    public static final String CHEMIN_REPERTOIRE_UPLOAD_CSV = "src/test/resources/upload/";

    protected static final String CHEMIN_REPERTOIRE_UPLOAD_SOURCE_CSV = CHEMIN_REPERTOIRE_UPLOAD_CSV + "source/import_csv/";

    public static final String CHEMIN_REPERTOIRE_UPLOAD_TEMP = CHEMIN_REPERTOIRE_UPLOAD_CSV + "upload-test-csv/";

    /** Répertoire d'upload pour les tests. */
    protected static File uploadDir;

    /** fichier vide extension txt */
    protected static File tempFile1;

    /** fichier vide extension csv */
    protected static File tempFile2;

    /** fichier csv avec erreurs */
    protected static File tempFile3;

    /**
     * Sets the up before class. <br/>
     * Création du repertoire d'upload s'il n'existe pas.
     */
    @BeforeAll
    public static void setUpBeforeClass() {
        if ((uploadDir == null) || !uploadDir.exists()) {
            String cheminUploadDir = CHEMIN_REPERTOIRE_UPLOAD_TEMP;
            uploadDir = new File(cheminUploadDir);
            uploadDir.mkdir();
            LOGGER.debug("Creation du repertoire d'upload de test");
        }
    }

    /**
     * On initialise 3 fichiers temporaires qu'on pourra utiliser en tant que fichiers mock.
     */
    @BeforeEach
    public void setup() {
        try {
            if ((tempFile1 == null) || !tempFile1.exists()) {
                String cheminFichier1 = CHEMIN_REPERTOIRE_UPLOAD_TEMP + "empty.txt";
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE_CSV + "empty.txt"), new File(cheminFichier1));
                tempFile1 = new File(cheminFichier1);
            }
            if ((tempFile2 == null) || !tempFile2.exists()) {
                String cheminFichier2 = CHEMIN_REPERTOIRE_UPLOAD_TEMP + "empty.csv";
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE_CSV + "empty.csv"), new File(cheminFichier2));
                tempFile2 = new File(cheminFichier2);
            }
            if ((tempFile3 == null) || !tempFile3.exists()) {
                String cheminFichier4 = CHEMIN_REPERTOIRE_UPLOAD_TEMP + "import_ok.csv";
                FileUtils.copyFile(new File(CHEMIN_REPERTOIRE_UPLOAD_SOURCE_CSV + "import_ok.csv"), new File(cheminFichier4));
                tempFile3 = new File(cheminFichier4);
            }

        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
    }

    /**
     * Nettoyage des 3 fichiers temporaires potentiellement créés ainsi que du repertoire d'upload.
     */
    @AfterAll
    public static void tearDownAfterClass() {
        if (tempFile1.exists()) {
            tempFile1.delete();
        }
        if (tempFile2.exists()) {
            tempFile2.delete();
        }
        if (tempFile3.exists()) {
            tempFile3.delete();
        }

        if (uploadDir.exists()) {
            // Commenter le bloc ci-dessous si on veut lire les éventuels fichiers générés par le test.
            try {
                FileUtils.forceDelete(uploadDir);
                LOGGER.debug("Suppression du repertoire de test d'upload");
            } catch (IOException e) {
                throw new TechniqueException(e);
            }

        }
    }

    /**
     * Gets the fichier transfert from csv file.
     *
     * @param file
     *            the file
     * @return the fichier transfert from csv file
     */
    private static FichierTransfert getFichierTransfertFromCsvFile(File file) {
        FichierTransfert fichierTransfert = new FichierTransfert();
        try {
            fichierTransfert.setBytes(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new TechniqueException(e);
        }
        fichierTransfert.setContentTypeTheorique("text/csv");
        fichierTransfert.setNomFichier(file.getName());

        return fichierTransfert;
    }

    /**
     * @return the tempFile1
     */
    public static FichierTransfert getTempFile1() {
        return getFichierTransfertFromCsvFile(tempFile1);
    }

    public static FichierTransfert getTempFile2() {
        return getFichierTransfertFromCsvFile(tempFile2);
    }

    public static FichierTransfert getTempFile3() {
        return getFichierTransfertFromCsvFile(tempFile3);
    }

}

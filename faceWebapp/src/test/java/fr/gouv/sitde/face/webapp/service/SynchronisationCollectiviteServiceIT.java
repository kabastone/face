package fr.gouv.sitde.face.webapp.service;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.domaine.service.subvention.SynchronisationCollectiviteService;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestApplication;

/**
 * The Class SynchronisationCollectiviteServiceIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class SynchronisationCollectiviteServiceIT extends AbstractTestApplication {

    /** The synchronisation collectivite service. */
    @Inject
    private SynchronisationCollectiviteService synchronisationCollectiviteService;

    /**
     * Test synchronisation collectivite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/subvention/in_demande_subvention_synchroniser_collectivite.xml" })
    @ExpectedDatabase(value = "/dataset/expected/subvention/ex_demande_subvention_synchroniser_collectivite.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSynchroniserCollectivite() throws Exception {

        boolean synchroniserCollectivite = this.synchronisationCollectiviteService.synchroniserCollectivite(900001L, 1L);

        Assertions.assertTrue(synchroniserCollectivite);

    }

}

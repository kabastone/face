/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.CollectiviteSimpleDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class AccesCollectiviteValidatorIT.
 *
 * @author Atos
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class MenuRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher utilisateur par id.
     *
     * @throws Exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivites_rechercher.xml" })
    public void testRechercherCollectivites() throws Exception {

        String uri = "/api/collectivite/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        CollectiviteSimpleDto[] collectivites = this.mapFromJson(content, CollectiviteSimpleDto[].class);

        Assertions.assertNotNull(collectivites);
        Assertions.assertEquals(4, collectivites.length);
    }
}

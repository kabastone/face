package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DroitAgentUtilisateurDto;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class DroitsAgentRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DroitsAgentRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher agents par idCollectivite.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/droitagent/in_droit_agent_rechercher.xml" })
    public void testRechercherAgentsParCollectivite() throws Exception {
        // init.
        String idCollectivite = "900003";
        String uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";

        // requête
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        // transformation des données reçues
        String content = mvcResult.getResponse().getContentAsString();
        DroitAgentUtilisateurDto[] tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);

        // vérifications
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
        Assertions.assertEquals("Dubois", tabDroitsUtilisateurs[0].getNom());
    }

    /**
     * Test ajout d'un droit agent.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/droitagent/in_droit_agent_ajouter.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/droitagent/ex_droit_agent_ajouter.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAjouterDroitAgent() throws Exception {
        // init.
        int idCollectivite = 900004;
        int idUtilisateur = 900002;
        String uri, content;
        MvcResult mvcResult;
        DroitAgentUtilisateurDto[] tabDroitsUtilisateurs;

        // avant : la collectivité est vide
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

        // ajout de l'agent
        uri = "/api/collectivite/" + idCollectivite + "/agent/" + idUtilisateur + "/ajouter";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        // l'utilisateur ajouté à bien les droits dans l'objet retourné
        DroitAgentUtilisateurDto utilisateurAjoute = this.mapFromJson(content, DroitAgentUtilisateurDto.class);
        Assertions.assertEquals(idCollectivite, utilisateurAjoute.getIdCollectivite().intValue());

        // apres : l'utilisateur est présent dans la collectivité
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);
        Assertions.assertNotNull(tabDroitsUtilisateurs);
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
    }

    /**
     * Test de changement d'état (agent/admin) d'un utilisateur sur une collectivité.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/droitagent/in_droit_agent_changer_etat.xml" })
    public void testChangerEtatAdminDroitAgent() throws Exception {
        // init.
        String idCollectivite = "900003";
        String idUtilisateur = "900002";
        String uri, content;
        MvcResult mvcResult;
        DroitAgentUtilisateurDto[] tabDroitsUtilisateurs;
        DroitAgentUtilisateurDto utilisateur;

        // avant : un seul utilisateur non admin
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);
        Assertions.assertNotNull(tabDroitsUtilisateurs);
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
        Assertions.assertFalse(tabDroitsUtilisateurs[0].isAdmin());

        // mise à jour : agent => admin (upgrade)
        uri = "/api/collectivite/" + idCollectivite + "/agent/" + idUtilisateur + "/changer-etat";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        utilisateur = this.mapFromJson(content, DroitAgentUtilisateurDto.class);
        Assertions.assertTrue(utilisateur.isAdmin());

        // la liste est aussi mise à jour
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);
        Assertions.assertNotNull(tabDroitsUtilisateurs);
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
        Assertions.assertTrue(tabDroitsUtilisateurs[0].isAdmin());

        // mise à jour : admin => agent (downgrade)
        uri = "/api/collectivite/" + idCollectivite + "/agent/" + idUtilisateur + "/changer-etat";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        utilisateur = this.mapFromJson(content, DroitAgentUtilisateurDto.class);
        Assertions.assertFalse(utilisateur.isAdmin());

        // la liste est aussi mise à jour
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);
        Assertions.assertNotNull(tabDroitsUtilisateurs);
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
        Assertions.assertFalse(tabDroitsUtilisateurs[0].isAdmin());
    }

    /**
     * Test suppression d'un droit agent.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/droitagent/in_droit_agent_supprimer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/droitagent/ex_droit_agent_supprimer.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSupprimerDroitAgent() throws Exception {
        // init.
        String idCollectivite = "900003";
        String idUtilisateur = "900002";
        String uri, content;
        MvcResult mvcResult;
        DroitAgentUtilisateurDto[] tabDroitsUtilisateurs;

        // avant : un seul agent dans la collectivité (Dubois)
        uri = "/api/collectivite/" + idCollectivite + "/agents/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabDroitsUtilisateurs = this.mapFromJson(content, DroitAgentUtilisateurDto[].class);
        Assertions.assertNotNull(tabDroitsUtilisateurs);
        Assertions.assertEquals(1, tabDroitsUtilisateurs.length);
        Assertions.assertEquals("Dubois", tabDroitsUtilisateurs[0].getNom());

        // suppression de l'agent dans la collectivité
        uri = "/api/collectivite/" + idCollectivite + "/agent/" + idUtilisateur + "/supprimer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        // résultat OK
        Assertions.assertEquals(ConstantesFace.STATUS_OK, content);
        // après : plus aucun agent dans la collectivité (voir ex_utilisateur_supprimer.xml)
    }
}

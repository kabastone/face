package fr.gouv.sitde.face.webapp.configuration;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

import fr.gouv.sitde.face.domain.spi.email.MailSenderService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.webapp.mock.DotationDepartementServiceImplMock;
import fr.gouv.sitde.face.webapp.mock.JwtTokenProviderMock;
import fr.gouv.sitde.face.webapp.mock.MailSenderServiceMock;
import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;

/**
 * The Class IntegrationConfig.
 */
@EnableAutoConfiguration()
@EnableJpaRepositories("fr.gouv.sitde.face.persistance")
@EnableAspectJAutoProxy
@EnableTransactionManagement
@EntityScan("fr.gouv.sitde.face.transverse.entities")
@ComponentScan(basePackages = { "fr.gouv.sitde.face" })
public class IntegrationConfig {

    /** The data source. */
    @Inject
    private DataSource dataSource;

    /**
     * Mail sender service.
     *
     * @return the mail sender service
     */
    @Bean
    @Primary
    public MailSenderService mailSenderService() {
        return new MailSenderServiceMock();
    }

    /**
     * Jwt token provider.
     *
     * @return the jwt token provider
     */
    @Bean
    @Primary
    public JwtTokenProvider jwtTokenProvider() {
        return new JwtTokenProviderMock();
    }

    /**
     * Dotation departement service.
     *
     * @return the dotation departement service
     */
    @Bean
    @Primary
    public DotationDepartementService dotationDepartementService() {
        return new DotationDepartementServiceImplMock();
    }

    /**
     * Db unit database connection.
     *
     * @return the database data source connection factory bean
     */
    @Bean
    public DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection() {
        DatabaseConfigBean bean = new DatabaseConfigBean();
        bean.setDatatypeFactory(new PostgresqlDataTypeFactory());

        DatabaseDataSourceConnectionFactoryBean dbConnectionFactory = new DatabaseDataSourceConnectionFactoryBean(this.dataSource);
        dbConnectionFactory.setDatabaseConfig(bean);
        return dbConnectionFactory;
    }
}

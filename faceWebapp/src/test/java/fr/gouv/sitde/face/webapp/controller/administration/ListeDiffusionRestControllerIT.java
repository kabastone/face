package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.AdresseEmailDto;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class UtilisateurRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class ListeDiffusionRestControllerIT extends AbstractTestWeb {

    /**
     * Pour la liste de diffusion : recherche toutes les adresses e-mail d'une collectivité.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_email_rechercher.xml" })
    public void testRechercherAdressesEmail() throws Exception {
        // init.
        String idCollectivite = "900001";
        String uri, content;
        MvcResult mvcResult;
        AdresseEmailDto[] tabAdressesEmail;

        // recherche
        uri = "/api/collectivite/" + idCollectivite + "/liste-diffusion/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabAdressesEmail = this.mapFromJson(content, AdresseEmailDto[].class);

        // on doit bien avoir 3 adresses e-mail dans cette collectivité
        Assertions.assertNotNull(tabAdressesEmail);
        Assertions.assertEquals(3, tabAdressesEmail.length);
    }

    /**
     * Pour la liste de diffusion : supprimer une adresse e-mail.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_email_supprimer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/collectivite/ex_email_supprimer.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSupprimerAdresseEmail() throws Exception {
        // init.
        String idCollectivite = "900001";
        String idAdresseEmail = "900001";
        String uri, content;
        MvcResult mvcResult;

        // suppression d'une adresse e-mail dans la liste de diffusion
        uri = "/api/collectivite/" + idCollectivite + "/liste-diffusion/" + idAdresseEmail + "/supprimer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        // résultat OK
        Assertions.assertEquals(ConstantesFace.STATUS_OK, content);
    }

    /**
     * Pour la liste de diffusion : supprimer une adresse e-mail.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_email_supprimer.xml" })
    public void testSupprimerDerniereAdresseEmailMaisCollectiviteToujoursOuverte() throws Exception {
        // init.
        String idCollectivite = "900003";
        String idAdresseEmail = "900004";
        String uri;
        MvcResult mvcResult;

        // suppression d'une adresse e-mail dans la liste de diffusion
        uri = "/api/collectivite/" + idCollectivite + "/liste-diffusion/" + idAdresseEmail + "/supprimer";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    /**
     * Ajout de l'e-mail à la liste de diffusion de la collectivité.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_email_ajouter.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/collectivite/ex_email_ajouter.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAjouterAdresseEmailCollectivite() throws Exception {
        // init.
        String idCollectivite = "900001";
        String adresseAjouter = "utilisateur.lambda@fai.tld";
        String uri, content;
        MvcResult mvcResult;
        AdresseEmailDto adresseEmailDto;

        // ajout d'une adresse e-mail dans la liste de diffusion
        uri = "/api/collectivite/" + idCollectivite + "/liste-diffusion/ajouter";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON_VALUE).content(adresseAjouter)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        adresseEmailDto = this.mapFromJson(content, AdresseEmailDto.class);

        // l'adresse correspond
        Assertions.assertNotNull(adresseEmailDto);
        Assertions.assertEquals(adresseAjouter, adresseEmailDto.getEmail());
    }
}

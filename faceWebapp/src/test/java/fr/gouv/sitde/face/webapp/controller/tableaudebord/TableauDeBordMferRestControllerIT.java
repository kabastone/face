/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.tableaudebord;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeProlongationTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * Classe de test de TableauDeBordMferRestControllerIT.
 *
 * @author a453029
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TableauDeBordMferRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher tableau de bord mfer.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTableauDeBordMfer() throws Exception {

        // recherche
        String uri = "/api/tableaudebord/mfer/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test rechercher tdb mfer aqualifier subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAqualifierSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/subaqualifier/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer aqualifier paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAqualifierPaiement() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/paiaqualifier/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer aaccorder subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAaccorderSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/subaaccorder/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertFalse(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer aaccorder paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAaccorderPaiement() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/paiaaccorder/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer aattribuer subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAattribuerSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/subaattribuer/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer asignaler subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAsignalerSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/subasignaler/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer asignaler paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAsignalerPaiement() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/paiasignaler/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb mfer aprolonger subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_mfer.xml" })
    public void testRechercherTdbMferAprolongerSubvention() throws Exception {

        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);

        // recherche
        String uri = "/api/tableaudebord/mfer/subaprolonger/rechercher";
        String inputJson = this.mapToJson(pageDemande);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeProlongationTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeProlongationTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

}

package fr.gouv.sitde.face.webapp.service;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * The Class DocumentServiceIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DocumentServiceIT extends FichiersUploadTestWeb {

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /**
     * Test creer document demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_subvention.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_demande_subvention.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentDemandeSubvention() throws Exception {

        Long idDemandeSubvention = new Long("900001");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));
    }

    /**
     * Test creer document demande prolongation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_prolongation.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_demande_prolongation.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentDemandeProlongation() throws Exception {

        DemandeProlongation demProlongation = new DemandeProlongation();
        demProlongation.setId(900001L);
        DossierSubvention dossier = new DossierSubvention();
        dossier.setId(900001L);
        dossier.setDateCreation(LocalDateTime.of(LocalDate.parse("2019-03-25", DateTimeFormatter.ofPattern("yyyy-MM-dd")), LocalTime.MIDNIGHT));
        demProlongation.setDossierSubvention(dossier);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.DEMANDE_PROLONGATION);

        Document doc = this.documentService.creerDocumentDemandeProlongation(demProlongation, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.DEMANDE_PROLONGATION.getCode()));
    }

    /**
     * Test creer document demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_paiement.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_demande_paiement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentDemandePaiement() throws Exception {

        Long idDemandePaiement = new Long("900001");

        DemandePaiement demPaiement = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF);

        Document doc = this.documentService.creerDocumentDemandePaiement(demPaiement, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_MARCHES_PASSES_PDF.getCode()));
    }

    /**
     * Test creer document dotation departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_courrier_departement.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_courrier_departement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentCourrierDepartement() throws Exception {

        CourrierAnnuelDepartement couDepartement = new CourrierAnnuelDepartement();
        couDepartement.setId(900001L);
        couDepartement
                .setDateCreation(LocalDateTime.of(LocalDate.parse("2019-03-25", DateTimeFormatter.ofPattern("yyyy-MM-dd")), LocalTime.MIDNIGHT));

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.COURRIER_REPARTITION);

        Document doc = this.documentService.creerDocumentCourrierDepartement(couDepartement, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.COURRIER_REPARTITION.getCode()));
    }

    /**
     * Test creer document ligne dotation departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_ligne_dotation_departement.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_ligne_dotation_departement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentLigneDotationDepartement() throws Exception {

        LigneDotationDepartement ligneDotaDep = new LigneDotationDepartement();
        ligneDotaDep.setId(900001L);
        DotationDepartement dotaDepartement = new DotationDepartement();
        dotaDepartement.setId(900001L);
        dotaDepartement
                .setDateCreation(LocalDateTime.of(LocalDate.parse("2019-03-25", DateTimeFormatter.ofPattern("yyyy-MM-dd")), LocalTime.MIDNIGHT));
        ligneDotaDep.setDotationDepartement(dotaDepartement);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION);

        Document doc = this.documentService.creerDocumentLigneDotationDepartement(ligneDotaDep, fichierTransfert);
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION.getCode()));
    }

    /**
     * Test creer document modele.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_subvention.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_creation_document_modele.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerDocumentModele() throws Exception {

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.MODELE_ETAT_ACHEVEMENT_FINANCIER);

        Document doc = this.documentService.creerDocumentModele(fichierTransfert);

        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.MODELE_ETAT_ACHEVEMENT_FINANCIER.getCode()));
    }

    /**
     * Test modifier document demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_subvention.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_modification_document_demande_subvention.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierFichierDocument() throws Exception {

        Long idDemandeSubvention = new Long("900001");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        // Creation du document
        FichierTransfert fichierTransfert1 = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert1.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);
        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert1);

        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));

        // Attente d'une seconde pour ne pas avoir le même nom de fichier
        TimeUnit.SECONDS.sleep(1);

        // Modification du fichier
        FichierTransfert fichierTransfert2 = FichiersUploadTestWeb.getFichierTransfertTempFile2();
        fichierTransfert2.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR);

        Document doc2 = this.documentService.modifierFichierDocument(doc.getId(), fichierTransfert2);
        Assertions.assertNotEquals(doc.getTypeDocument(), doc2.getTypeDocument());
    }

    /**
     * Test charger fichier document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_document_demande_subvention.xml" })
    public void testChargerFichierDocument() throws Exception {

        Long idDemandeSubvention = new Long("900001");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert);
        // Le document a bien été créé
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));

        File fichier = this.documentService.chargerFichierDocument(doc.getId());
        Assertions.assertTrue(fichier.exists());
    }

    /**
     * Test charger fichier document fichier inexistant : le fichier n'existe pas.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_charger_document_fichier_inexistant.xml" })
    public void testChargerFichierDocumentFichierInexistant() throws Exception {

        try {
            this.documentService.chargerFichierDocument(900001L);
            Assertions.fail("Une exception technique aurait dû etre lancée");
        } catch (TechniqueException te) {
            // Comportement normal, le chemin du fichier n'existe pas
        }
    }

    /**
     * Test rechercher document par id document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_charger_document_fichier_inexistant.xml" })
    public void testRechercherDocumentParIdDocument() throws Exception {

        Document doc = this.documentService.rechercherDocumentParIdDocument(63L);
        Assertions.assertNull(doc);
        doc = this.documentService.rechercherDocumentParIdDocument(900001L);
        Assertions.assertNotNull(doc);
    }

    /**
     * Test rechercher document par id document et famille.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_charger_document_fichier_inexistant.xml" })
    public void testRechercherDocumentParIdDocumentEtFamille() throws Exception {

        Document doc = this.documentService.rechercherDocumentParIdDocumentEtFamille(63L, FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION);
        Assertions.assertNull(doc);

        doc = this.documentService.rechercherDocumentParIdDocumentEtFamille(900001L, FamilleDocumentEnum.DOC_DEMANDE_PAIEMENT);
        Assertions.assertNull(doc);

        doc = this.documentService.rechercherDocumentParIdDocumentEtFamille(900001L, FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION);
        Assertions.assertNotNull(doc);
    }

    /**
     * Test supprimer document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_suppression_document.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_suppression_document.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSupprimerDocument() throws Exception {

        Long idDemandeSubvention = new Long("900001");

        // Recherche de la demande de subvention.
        DemandeSubvention demSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubvention, fichierTransfert);
        // Le document a bien été créé
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));

        // Le fichier correspondant au document existe
        File fichier = this.documentService.chargerFichierDocument(doc.getId());
        Assertions.assertTrue(fichier.exists());

        this.documentService.supprimerDocument(doc.getId());
        Assertions.assertFalse(fichier.exists());
        Assertions.assertNull(this.documentService.rechercherDocumentParIdDocument(doc.getId()));
    }

    /**
     * Test supprimer document.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_charger_document_fichier_inexistant.xml" })
    @ExpectedDatabase(value = "/dataset/expected/service/ex_suppression_document_fichier_inexistant.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSupprimerDocumentFichierInexistant() throws Exception {

        Document doc = this.documentService.rechercherDocumentParIdDocument(900001L);
        Assertions.assertNotNull(doc);

        try {
            this.documentService.chargerFichierDocument(900001L);
            Assertions.fail("Une exception technique aurait dû etre lancée");
        } catch (TechniqueException te) {
            // Comportement normal, le chemin du fichier n'existe pas
        }

        // Le fichier n'existe pas (évenement loggué en warn) mais cela n'empeche pas de supprimer le document en base.
        this.documentService.supprimerDocument(doc.getId());
        Assertions.assertNull(this.documentService.rechercherDocumentParIdDocument(doc.getId()));
    }

    /**
     * Testdeplacer directory demande subvention.
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_charger_document_fichier_inexistant_pour_deplacement.xml" })
    public void testDeplacerDirectoryDemandeSubvention() {

        Long idDemandeSubventionOrigine = new Long("900011");
        Long idChorusDossierSubventionCible = new Long("900002");

        // Recherche de la demande de subvention d'origine.
        DemandeSubvention demSubventionOrigine = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubventionOrigine);

        // Création de la demande de subvention avec dossier cible.
        DemandeSubvention demSubventionCible = new DemandeSubvention();
        demSubventionCible.setId(900011L);

        DossierSubvention dossierCible = this.dossierSubventionService.rechercherDossierSubventionParIdChorusAe(idChorusDossierSubventionCible);
        demSubventionCible.setDossierSubvention(dossierCible);

        // recuperation du fichier a créer
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        fichierTransfert.setTypeDocument(TypeDocumentEnum.ETAT_PREVISIONNEL_PDF);

        Document doc = this.documentService.creerDocumentDemandeSubvention(demSubventionOrigine, fichierTransfert);
        // Le document a bien été créé
        Assertions.assertTrue(StringUtils.startsWith(doc.getNomFichierSansExtension(), TypeDocumentEnum.ETAT_PREVISIONNEL_PDF.getCode()));

        // Le fichier correspondant au document existe
        File fichier = this.documentService.chargerFichierDocument(doc.getId());
        Assertions.assertTrue(fichier.exists());

        this.documentService.deplacerDirectoryDemandeSubvention(demSubventionCible, demSubventionOrigine, false);

        // le fichier d'origine n'existe plus
        Assertions.assertFalse(fichier.exists());

        String newLocation = StringUtils.substringBeforeLast(fichier.getPath(), File.separator);
        newLocation = StringUtils.substringBeforeLast(newLocation, File.separator);
        newLocation = StringUtils.substringBeforeLast(newLocation, File.separator);
        newLocation = StringUtils.substringBeforeLast(newLocation, File.separator);
        newLocation += File.separator + "900012" + File.separator + "DEM-SUB" + File.separator + "900011" + File.separator + fichier.getName();

        File newFile = new File(newLocation);
        Assertions.assertTrue(newFile.exists());

    }
}

package fr.gouv.sitde.face.webapp.controller.dotation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.DotationAnnuelleDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class DotationAnnuelleRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationAnnuelleRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher dotation annuelle.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_annuelle.xml" })
    public void testRechercherDotationAnnuelle() throws Exception {

        String uri = "/api/dotation/annuelle/rechercher";

        DotationAnnuelleDto dotationAnnuelleDto = new DotationAnnuelleDto();
        dotationAnnuelleDto.setIdCollectivite(new Long(900001));
        dotationAnnuelleDto.setAnnee(2019);
        String inputJson = this.mapToJson(dotationAnnuelleDto);

        // Recherche
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationAnnuelleDto dotationAnnuelleDtoRecupere = this.mapFromJson(content, DotationAnnuelleDto.class);

        Assertions.assertNotNull(dotationAnnuelleDtoRecupere);

        // Vérification dotations programme principal
        Assertions.assertNotNull(dotationAnnuelleDtoRecupere.getDotationsCollectiviteProgrammePrincipal());
        Assertions.assertEquals(1, dotationAnnuelleDtoRecupere.getDotationsCollectiviteProgrammePrincipal().size());

        // Vérification dotations programme spécial
        Assertions.assertNotNull(dotationAnnuelleDtoRecupere.getDotationsCollectiviteProgrammeSpecial());
        Assertions.assertEquals(1, dotationAnnuelleDtoRecupere.getDotationsCollectiviteProgrammeSpecial().size());

        // Vérification documents
        Assertions.assertNotNull(dotationAnnuelleDtoRecupere.getDocuments());
        Assertions.assertEquals(4, dotationAnnuelleDtoRecupere.getDocuments().size());
    }
}

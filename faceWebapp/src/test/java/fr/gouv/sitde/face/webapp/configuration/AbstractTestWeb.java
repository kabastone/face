package fr.gouv.sitde.face.webapp.configuration;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.exception.DataNotFoundException;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

/**
 * The Class AbstractTestApplication.
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { IntegrationConfig.class })
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@WebAppConfiguration
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
public class AbstractTestWeb {

    /** The code http redirect. */
    public static int CODE_HTTP_REDIRECT = 302;

    public static String ROLE_HEADER = "ROLE_HEADER";

    /** The mvc. */
    protected MockMvc mvc;

    /** The web application context. */
    @Inject
    protected WebApplicationContext webApplicationContext;

    /**
     * Sets the up.
     */
    @BeforeEach
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).addFilter(((request, response, chain) -> {
            response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
            chain.doFilter(request, response);
        })).build();
    }

    /**
     * Map from json.
     *
     * @param <T>
     *            the generic type
     * @param json
     *            the json
     * @param clazz
     *            the clazz
     * @return the t
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat(ConstantesFace.FORMAT_DATE);
        objectMapper.setDateFormat(df);
        return objectMapper.readValue(json, clazz);
    }

    protected <T extends Serializable> List<T> mapFromListJson(String json, Class<T> clazz) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat(ConstantesFace.FORMAT_DATE);
        objectMapper.setDateFormat(df);
        JavaType type = objectMapper.getTypeFactory().constructParametricType(List.class, clazz);
        return objectMapper.readValue(json, type);
    }

    /**
     * Map page reponse from json.
     *
     * @param <U>
     *            the generic type
     * @param json
     *            the json
     * @param beanClass
     *            the bean class
     * @return the page reponse
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected <U extends Serializable> PageReponse<U> mapPageReponseFromJson(String json, Class<U> clazz) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat(ConstantesFace.FORMAT_DATE);
        objectMapper.setDateFormat(df);
        JavaType type = objectMapper.getTypeFactory().constructParametricType(PageReponse.class, clazz);
        return objectMapper.readValue(json, type);
    }

    /**
     * Map to json.
     *
     * @param obj
     *            the obj
     * @return the string
     * @throws JsonProcessingException
     *             the json processing exception
     */
    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    /**
     * Checks if is result ok sans exception.
     *
     * @param mvcResult
     *            the mvc result
     * @return true, if is result ok sans exception
     */
    protected static boolean isResultOkSansException(MvcResult mvcResult) {
        return (MockHttpServletResponse.SC_OK == mvcResult.getResponse().getStatus()) && (mvcResult.getResolvedException() == null);
    }

    /**
     * Checks if is result regle gestion exception.
     *
     * @param mvcResult
     *            the mvc result
     * @return true, if is result regle gestion exception
     */
    protected static boolean isResultRegleGestionException(MvcResult mvcResult) {
        return (MockHttpServletResponse.SC_OK == mvcResult.getResponse().getStatus()) && (mvcResult.getResolvedException() != null)
                && (mvcResult.getResolvedException() instanceof RegleGestionException);
    }

    /**
     * Checks if is result data not found exception.
     *
     * @param mvcResult
     *            the mvc result
     * @return true, if is result data not found exception
     */
    protected static boolean isResultDataNotFoundException(MvcResult mvcResult) {
        return (MockHttpServletResponse.SC_NOT_FOUND == mvcResult.getResponse().getStatus()) && (mvcResult.getResolvedException() != null)
                && (mvcResult.getResolvedException() instanceof DataNotFoundException);
    }

    /**
     * Checks if is result erreur serveur.
     *
     * @param mvcResult
     *            the mvc result
     * @return true, if is result erreur serveur
     */
    protected static boolean isResultErreurServeur(MvcResult mvcResult) {
        return (MockHttpServletResponse.SC_INTERNAL_SERVER_ERROR == mvcResult.getResponse().getStatus());
    }

    /**
     * Construit la requête vers l'uri donnée spécifiant le role pour le mock JwtToken.
     *
     * @param uri
     *            the uri
     * @param requestMethod
     *            the request method
     * @param role
     *            the role
     * @return the mock http servlet request builder
     */
    protected static MockHttpServletRequestBuilder construireRequetePourRole(String uri, RequestMethod requestMethod, RoleEnum role) {
        switch (requestMethod) {
            case GET:
                return MockMvcRequestBuilders.get(uri).header(AbstractTestWeb.ROLE_HEADER, role.name());
            case POST:
                return MockMvcRequestBuilders.post(uri).header(AbstractTestWeb.ROLE_HEADER, role.name());
            default:
                throw new TechniqueException("Seules les methodes POST et GET sont implémentées");
        }
    }
}

package fr.gouv.sitde.face.webapp.controller.dotation;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DotationProgrammeDto;
import fr.gouv.sitde.face.application.dto.DotationRepartitionNationaleDto;
import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class DotationNationaleRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationNationaleRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher dotation repartition nationale sans init annuelle.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    public void testRechercherDotationRepartitionNationaleSansInitAnnuelle() throws Exception {
        String codeProgramme = "793";

        // recherche
        String uri = "/api/dotation/repartition-nationale/code/" + codeProgramme + "/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(900004L, dotationNationaleDto.getDotationProgramme().getIdDotationProgramme());
        Assertions.assertEquals(91690000, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        Assertions.assertEquals(5, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(590000, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
    }

    /**
     * Test rechercher dotation repartition nationale sans init annuelle.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale_non_valide.xml" })
    public void testRechercherDotationRepartitionNationaleSansInitAnnuelleAvecInvalides() throws Exception {
        String codeProgramme = "793";

        // recherche
        String uri = "/api/dotation/repartition-nationale/code/" + codeProgramme + "/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(900004L, dotationNationaleDto.getDotationProgramme().getIdDotationProgramme());
        Assertions.assertEquals(690000, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        Assertions.assertEquals(6, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(590000, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
        Assertions.assertEquals(681000, dotationNationaleDto.getSommeDotationsSpAnneeMoins1().intValue());
        Assertions.assertEquals(2018, dotationNationaleDto.getListeLignesDotationsSousProgrammes().stream()
                .filter(ligne -> ligne.getIdSousProgramme() == 900018).findFirst().get().getAnneeFinValidite());
    }

    /**
     * Test rechercher dotation repartition nationale avec init annuelle.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale_avec_init.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_nationale_avec_init.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRechercherDotationRepartitionNationaleAvecInitAnnuellePasDeSousProgrammeNonValide() throws Exception {
        String codeProgramme = "793";

        // recherche
        String uri = "/api/dotation/repartition-nationale/code/" + codeProgramme + "/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(0, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        // il y a 7 sous-programmes liés au programme 793 dans le referentiel.
        Assertions.assertEquals(7, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(0, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
    }

    /**
     * Test rechercher dotation repartition nationale avec init annuelle.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale_avec_init_non_valide.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_nationale_avec_init.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRechercherDotationRepartitionNationaleAvecInitAnnuelleAvecSousProgrammeNonValide() throws Exception {
        String codeProgramme = "793";

        // recherche
        String uri = "/api/dotation/repartition-nationale/code/" + codeProgramme + "/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(0, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        // il y a 8 sous-programmes liés au programme 793 dans le referentiel, un n'est pas valide donc 7 valides
        Assertions.assertEquals(7, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(0, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
    }

    /**
     * Test modifier montant dotation programme repartition nationale.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_nationale_modif_montant_programme.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierMontantDotationProgrammeRepartitionNationale() throws Exception {

        // recherche
        DotationProgrammeDto dotationProgrammeDto = new DotationProgrammeDto();
        dotationProgrammeDto.setIdDotationProgramme(900004L);
        dotationProgrammeDto.setMontant(new BigDecimal(800000));
        String inputJson = this.mapToJson(dotationProgrammeDto);
        String uri = "/api/dotation/repartition-nationale/dotation-programme/montant/modifier";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationProgrammeDto dotationProgrammeResult = this.mapFromJson(content, DotationProgrammeDto.class);

        Assertions.assertEquals(900004L, dotationProgrammeResult.getIdDotationProgramme());
        Assertions.assertEquals(800000, dotationProgrammeResult.getMontant().intValue());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_nationale_modif_montant_sous_programme.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierMontantDotationSousProgrammeRepartitionNationale() throws Exception {

        // recherche
        DotationSousProgrammeMontantBo ligneDotationDto = new DotationSousProgrammeMontantBo();
        ligneDotationDto.setIdDotationSousProgramme(900035L);
        ligneDotationDto.setMontant(new BigDecimal("1000000"));
        ligneDotationDto.setConcerneInitial(true);
        String inputJson = this.mapToJson(ligneDotationDto);
        String uri = "/api/dotation/repartition-nationale/dotation-sous-programme/montant/modifier";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(900004L, dotationNationaleDto.getDotationProgramme().getIdDotationProgramme());
        Assertions.assertEquals(91690000, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        Assertions.assertEquals(5, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(1590000, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_nationale_modif_montant_sous_programme_effectif_uniquement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierMontantEffectifDotationSousProgrammeRepartitionNationale() throws Exception {

        // recherche
        DotationSousProgrammeMontantBo ligneDotationDto = new DotationSousProgrammeMontantBo();
        ligneDotationDto.setIdDotationSousProgramme(900035L);
        ligneDotationDto.setMontant(new BigDecimal("1000000"));
        ligneDotationDto.setConcerneInitial(false);
        String inputJson = this.mapToJson(ligneDotationDto);
        String uri = "/api/dotation/repartition-nationale/dotation-sous-programme/montant/modifier";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRepartitionNationaleDto dotationNationaleDto = this.mapFromJson(content, DotationRepartitionNationaleDto.class);

        Assertions.assertEquals(900004L, dotationNationaleDto.getDotationProgramme().getIdDotationProgramme());
        Assertions.assertEquals(91690000, dotationNationaleDto.getDotationProgramme().getMontant().intValue());
        Assertions.assertEquals(2019, dotationNationaleDto.getDotationProgramme().getAnnee());
        Assertions.assertEquals(5, dotationNationaleDto.getListeLignesDotationsSousProgrammes().size());
        Assertions.assertEquals(1590000, dotationNationaleDto.getSommeDotationsSpAnneeN().intValue());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    public void testModifierMontantDotationSousProgrammePasBoolean() throws Exception {

        // recherche
        DotationSousProgrammeMontantBo ligneDotationDto = new DotationSousProgrammeMontantBo();
        ligneDotationDto.setIdDotationSousProgramme(900035L);
        ligneDotationDto.setMontant(new BigDecimal("1000000"));
        ligneDotationDto.setConcerneInitial(null);
        String inputJson = this.mapToJson(ligneDotationDto);
        String uri = "/api/dotation/repartition-nationale/dotation-sous-programme/montant/modifier";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultErreurServeur(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_nationale.xml" })
    public void testModifierMontantDotationSousProgrammeTropDeMontant() throws Exception {

        // recherche
        DotationSousProgrammeMontantBo ligneDotationDto = new DotationSousProgrammeMontantBo();
        ligneDotationDto.setIdDotationSousProgramme(900035L);
        ligneDotationDto.setMontant(new BigDecimal("1000000000"));
        ligneDotationDto.setConcerneInitial(true);
        String inputJson = this.mapToJson(ligneDotationDto);
        String uri = "/api/dotation/repartition-nationale/dotation-sous-programme/montant/modifier";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

}

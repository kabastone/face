/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.administration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.transverse.email.EmailContactDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class AccesCollectiviteValidatorIT.
 *
 * @author Atos
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class EmailContactRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher utilisateur par id.
     *
     * @throws Exception
     */
    @Test
    public void testEmailContact() throws Exception {

        String uri = "/api/nous-contacter/email/envoi";

        EmailContactDto emailContactDto = new EmailContactDto();
        emailContactDto.setEmail("toto.baba@toto.toto");
        emailContactDto.setMessage("Bonjour, Je voudrais savoir si FACE fonctionne. Cordialement, Toto Baba");
        emailContactDto.setNom("Toto Baba");
        emailContactDto.setObjet("Demande d'informations sans tarder.");
        String inputJson = this.mapToJson(emailContactDto);

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri, inputJson).accept(MediaType.APPLICATION_JSON_VALUE).content(inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertNotNull(content);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.dotation;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * Classe de test du controller Rest DotationDepartement.
 *
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationDepartementRestControllerIT extends FichiersUploadTestWeb {

    /**
     * Test generation donnees et document pdf.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_creation_annexe_dotation_departement.xml" })
    public void testDonneesAnnexeDotationPdf() throws Exception {
        // init.
        String idDepartement = "900001";
        String uri, contentDisposition;
        MvcResult mvcResult;

        // recherche
        uri = "/api/dotation/" + idDepartement + "/generation";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        contentDisposition = mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION);
        Assertions.assertNotNull(mvcResult.getResponse().getContentAsString());
        String[] list = contentDisposition.split("=");
        Assertions.assertTrue(list[0].equals("attachment; filename"));
        Assertions.assertTrue(list[1].endsWith(".pdf"));
        Assertions.assertTrue(
                mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_TYPE).contains(ExtensionFichierEnum.PDF.getContentType()));
    }

    /**
     * Test generation donnees et document pdf.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/service/in_dotation_departementale_multiple.xml" })
    public void testDonneesAnnexeDotationTousDepartementsPdf() throws Exception {
        // init.
        String uri, contentDisposition;
        MvcResult mvcResult;

        // recherche
        uri = "/api/dotation/departement/generation";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        contentDisposition = mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION);
        Assertions.assertNotNull(mvcResult.getResponse().getContentAsString());
        String[] list = contentDisposition.split("=");
        Assertions.assertTrue(list[0].equals("attachment; filename"));
        Assertions.assertTrue(list[1].endsWith(".pdf"));
        Assertions.assertTrue(
                mvcResult.getResponse().getHeader(ConstantesFace.ENTETE_CONTENT_TYPE).contains(ExtensionFichierEnum.PDF.getContentType()));
    }

    /**
     * Test notifier dotations.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_notifier_dotation.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_notifier_dotation.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testNotifierDotations() throws Exception {

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierUploade = new MockMultipartFile("fichier", fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        String idDepartement = "900001";
        String uri = "/api/dotation/" + idDepartement + "/televerser";

        // Notif
        MvcResult mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.multipart(uri).file(fichierUploade).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test notifier dotations.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_notifier_dotation.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_notifier_dotation_2.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testNotifierDotationsSansRepartition() throws Exception {
        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierUploade = new MockMultipartFile("fichier", fichierTransfert.getNomFichier(),
                fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());
        String idDepartement = "900002";
        String uri = "/api/dotation/" + idDepartement + "/televerser";

        // Notif
        MvcResult mvcResult = this.mvc
                .perform(MockMvcRequestBuilders.multipart(uri).file(fichierUploade).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testRechercherDotationsDepartementalesParAnneeEnCours() throws Exception {
        // init. recherche
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);
        String inputJson = this.mapToJson(pageDemande);
        String uri = "/api/dotation/departement/rechercher";

        // recherche
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DotationDepartementaleDto> pageReponseResultat = this.mapPageReponseFromJson(content, DotationDepartementaleDto.class);

        // on doit bien avoir 3 dotations departementales
        Assertions.assertEquals(3, pageReponseResultat.getListeResultats().size());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher_non_valide.xml" })
    public void testRechercherDotationsDepartementalesParAnneeEnCoursSousProgrammeNonValide() throws Exception {
        // init. recherche
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(10);
        pageDemande.setIndexPage(0);
        String inputJson = this.mapToJson(pageDemande);
        String uri = "/api/dotation/departement/rechercher";

        // recherche
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DotationDepartementaleDto> pageReponseResultat = this.mapPageReponseFromJson(content, DotationDepartementaleDto.class);

        // on doit bien avoir 3 dotations departementales
        Assertions.assertEquals(3, pageReponseResultat.getListeResultats().size());
        Assertions.assertEquals(5, pageReponseResultat.getListeResultats().get(0).getListeDotationsDepartementalesParSousProgramme().size());
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testRechercherLignesDotationDepartementaleEnPreparation() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        String idDepartement = "900001";
        LigneDotationDepartementaleDto[] tabLignesDotationDepartementale;

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/enPreparation/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabLignesDotationDepartementale = this.mapFromJson(content, LigneDotationDepartementaleDto[].class);

        // On doit bien avoir 1 ligne dotation departementale
        Assertions.assertNotNull(tabLignesDotationDepartementale);
        Assertions.assertEquals(1, tabLignesDotationDepartementale.length);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testRechercherLignesDotationDepartementaleNotifiees() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        String idDepartement = "900001";
        LigneDotationDepartementaleDto[] tabLignesDotationDepartementale;

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/notifiees/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        tabLignesDotationDepartementale = this.mapFromJson(content, LigneDotationDepartementaleDto[].class);

        // On doit bien avoir 1 dotation departementale
        Assertions.assertNotNull(tabLignesDotationDepartementale);
        Assertions.assertEquals(1, tabLignesDotationDepartementale.length);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testDetruireLigneDotationDepartementale() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        String idLigneDotationDepartementale = "900001";
        String idDepartement = "900001";
        LigneDotationDepartementaleDto[] tabLignesDotationDepartementale;

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/enPreparation/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabLignesDotationDepartementale = this.mapFromJson(content, LigneDotationDepartementaleDto[].class);

        // On doit bien avoir 1 ligne dotation departementale
        Assertions.assertNotNull(tabLignesDotationDepartementale);
        Assertions.assertEquals(1, tabLignesDotationDepartementale.length);

        // recherche
        uri = "/api/dotation/departement/lignes/" + idLigneDotationDepartementale;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/enPreparation/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testAjouterLigneDotationDepartementale() throws Exception {
        // init.
        String uri, content;
        MvcResult mvcResult;
        String idDepartement = "900001";
        LigneDotationDepartementaleDto[] tabLignesDotationDepartementale;

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/enPreparation/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabLignesDotationDepartementale = this.mapFromJson(content, LigneDotationDepartementaleDto[].class);

        // On doit bien avoir 1 ligne dotation departementale
        Assertions.assertNotNull(tabLignesDotationDepartementale);
        Assertions.assertEquals(1, tabLignesDotationDepartementale.length);

        // recherche
        uri = "/api/dotation/departement/lignes/";
        LigneDotationDepartementaleDto ligneDotationDepartementaleDto = new LigneDotationDepartementaleDto(null, new Long(900001), null,
                new BigDecimal(999), TypeDotationDepartementEnum.EXCEPTIONNELLE, null,
                new SousProgrammeDto("CE", "Enfouissement et pose en façade", "TODO3"));
        String inputJson = this.mapToJson(ligneDotationDepartementaleDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        // recherche
        uri = "/api/dotation/departement/rechercher/lignes/enPreparation/" + idDepartement;
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tabLignesDotationDepartementale = this.mapFromJson(content, LigneDotationDepartementaleDto[].class);

        // On doit bien avoir 1 ligne dotation departementale
        Assertions.assertNotNull(tabLignesDotationDepartementale);
        Assertions.assertEquals(2, tabLignesDotationDepartementale.length);

    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testAjouterLigneDotationDepartementaleQuandDotationSousProgrammeExistePas() throws Exception {
        // init.
        String uri;
        MvcResult mvcResult;

        // recherche
        uri = "/api/dotation/departement/lignes/";
        LigneDotationDepartementaleDto ligneDotationDepartementaleDto = new LigneDotationDepartementaleDto(null, new Long(900001), null,
                new BigDecimal(999), TypeDotationDepartementEnum.EXCEPTIONNELLE, null,
                new SousProgrammeDto("AP", "Renforcement des réseaux", "TODO1"));
        String inputJson = this.mapToJson(ligneDotationDepartementaleDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testAjouterLigneDotationDepartementaleQuandMontantTropEleve() throws Exception {
        // init.
        String uri;
        MvcResult mvcResult;

        // recherche
        uri = "/api/dotation/departement/lignes/";
        LigneDotationDepartementaleDto ligneDotationDepartementaleDto = new LigneDotationDepartementaleDto(null, new Long(900001), null,
                new BigDecimal(100001), TypeDotationDepartementEnum.EXCEPTIONNELLE, null,
                new SousProgrammeDto("CE", "Enfouissement et pose en façade", "TODO3"));
        String inputJson = this.mapToJson(ligneDotationDepartementaleDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));
    }

}

package fr.gouv.sitde.face.webapp.controller.administration;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;
import fr.gouv.sitde.face.webapp.controller.connexion.UtilisateurConnecteDto;

/**
 * The Class UtilisateurRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class UtilisateurRestControllerIT extends AbstractTestWeb {

    /** The gestion utilisateur application. */
    @Inject
    private GestionUtilisateurApplication gestionUtilisateurApplication;

    /**
     * Rechercher utilisateur par email test.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherUtilisateurConnecte() throws Exception {
        // Cas utilisateur existant
        String uri = "/api/utilisateur-connecte/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        UtilisateurConnecteDto userDto = this.mapFromJson(content, UtilisateurConnecteDto.class);

        Assertions.assertNotNull(userDto);
        Assertions.assertEquals("ID_MOCK", userDto.getCerbereId());
    }

    /**
     * Rechercher utilisateur par id test.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherUtilisateurParId() throws Exception {
        // Cas utilisateur existant
        String uri = "/api/utilisateur/id/999999/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        UtilisateurAdminDto userDto = this.mapFromJson(content, UtilisateurAdminDto.class);

        Assertions.assertNotNull(userDto);
        Assertions.assertEquals("ID_MOCK", userDto.getCerbereId());

        // Cas utilisateur différent de l'utilisateur connecté
        uri = "/api/utilisateur/id/9876543210/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultErreurServeur(mvcResult));
    }

    /**
     * Rechercher utilisateur par email test.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void rechercherUtilisateurParEmail() throws Exception {
        // Cas utilisateur existant
        String uri = "/api/utilisateur/email/nerepondezpas@developpementdurable.gouv.fr/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        UtilisateurAdminDto userDto = this.mapFromJson(content, UtilisateurAdminDto.class);

        Assertions.assertNotNull(userDto);
        Assertions.assertEquals(900001, userDto.getId().intValue());

        // Cas utilisateur inexistant
        uri = "/api//utilisateur/email/toto@planetToto.fr/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));
    }

    /**
     * Creer utilisateur attente connexion test.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/utilisateur/in_utilisateur_creation.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_attente_connexion_creation.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCreerUtilisateurAttenteConnexion() throws Exception {
        String uri = "/api/utilisateur/creer";

        // Test RG email obligatoire : une RG exception est lancée
        UtilisateurAdminDto userDto = new UtilisateurAdminDto();
        // Ajout de droits agent sur une collectivité
        DroitAgentCollectiviteDto droitAgentCollectiviteDto = new DroitAgentCollectiviteDto();
        droitAgentCollectiviteDto.setIdCollectivite(900001L);
        userDto.getDroitsAgentCollectivites().add(droitAgentCollectiviteDto);

        String inputJson = this.mapToJson(userDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Test reussite
        userDto.setEmail("jd@testInt.fr");

        inputJson = this.mapToJson(userDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        userDto = this.mapFromJson(content, UtilisateurAdminDto.class);

        Assertions.assertNotNull(userDto);
        Assertions.assertNotNull(userDto.getId());
    }

    /**
     * modifier utilisateur test.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/utilisateur/in_utilisateur_modification.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_modification.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierUtilisateur() throws Exception {

        // Test cas utilisateur différent de l'utilisateur connecté.
        String uri = "/api/utilisateur/900002/modifier";
        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParId(900002L);
        String inputJson = this.mapToJson(userDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        Assertions.assertTrue(isResultErreurServeur(mvcResult));

        // Test validation email obligatoire
        uri = "/api/utilisateur/999999/modifier";
        userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParId(999999L);
        Assertions.assertEquals("ID_MOCK", userDto.getCerbereId());
        userDto.setCerbereId("1234");
        userDto.setEmail("");
        inputJson = this.mapToJson(userDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultRegleGestionException(mvcResult));

        // Test reussite
        userDto.setEmail("jdModifie@testInt.fr");
        inputJson = this.mapToJson(userDto);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test supprimer utilisateur.
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/utilisateur/in_utilisateur_modification.xml" })
    @ExpectedDatabase(value = "/dataset/expected/administration/utilisateur/ex_utilisateur_suppression.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSupprimerUtilisateur() throws Exception {
        String uri = "/api/utilisateur/900002/supprimer";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        // résultat OK
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(ConstantesFace.STATUS_OK, content);
    }
}

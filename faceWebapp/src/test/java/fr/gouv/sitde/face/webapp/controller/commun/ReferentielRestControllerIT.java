package fr.gouv.sitde.face.webapp.controller.commun;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.application.dto.lov.CollectiviteLovDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandePaiementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandeSubventionLovDto;
import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeAnomalieLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * The Class ReferentielRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class ReferentielRestControllerIT extends AbstractTestWeb {

    /**
     * Test lister departements.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerDepartements() throws Exception {

        String uri = "/api/referentiel/departement/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DepartementLovDto[] departements = this.mapFromJson(content, DepartementLovDto[].class);

        Assertions.assertNotNull(departements);
        Assertions.assertEquals(3, departements.length);

    }

    /**
     * Test rechercher departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherDepartement() throws Exception {

        String idDepartement = "900001";
        String uri = "/api/referentiel/departement/" + idDepartement + "/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DepartementDto departement = this.mapFromJson(content, DepartementDto.class);

        Assertions.assertNotNull(departement);
        Assertions.assertEquals("Ain", departement.getNom());
    }

    /**
     * Test lister departements.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerSousProgrammes() throws Exception {

        String uri = "/api/referentiel/sous-programme/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertEquals(10, sousProgrammes.length);
    }

    /**
     * Test rechercher sous programme.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherSousProgramme() throws Exception {

        String uri = "/api/referentiel/sous-programme/900001/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeDto sousProgramme = this.mapFromJson(content, SousProgrammeDto.class);

        Assertions.assertEquals("TODO1", sousProgramme.getCodeDomaineFonctionnel());
    }

    /**
     * Test lister collectivites.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/collectivite/in_collectivites_rechercher.xml" })
    public void testListerCollectivites() throws Exception {

        String uri = "/api/referentiel/collectivite/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        CollectiviteLovDto[] collectivites = this.mapFromJson(content, CollectiviteLovDto[].class);

        Assertions.assertEquals(4, collectivites.length);
        Assertions.assertEquals("COL-01", collectivites[0].getNomCourt());
    }

    /**
     * Test lister etats demandes subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerEtatsDemandeSubvention() throws Exception {

        String uri = "/api/referentiel/etat-demande-subvention/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        EtatDemandeSubventionLovDto[] etatsDemandes = this.mapFromJson(content, EtatDemandeSubventionLovDto[].class);

        Assertions.assertEquals(17, etatsDemandes.length);
    }

    /**
     * Test lister etats demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerEtatsDemandePaiement() throws Exception {

        String uri = "/api/referentiel/etat-demande-paiement/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        EtatDemandePaiementLovDto[] etatsDemandes = this.mapFromJson(content, EtatDemandePaiementLovDto[].class);

        Assertions.assertEquals(15, etatsDemandes.length);
    }

    /**
     * Test rechercher departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_renoncement_renoncer.xml" })
    public void testRechercherSousProgrammesPourRenoncementDotation() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/referentiel/collectivite/" + idCollectivite + "/sous-programme-travaux/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertNotNull(sousProgrammes);
        Assertions.assertEquals(1, sousProgrammes.length);
        Assertions.assertEquals("Renforcement des réseaux", sousProgrammes[0].getDescription());
    }

    /**
     * Test rechercher departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_renoncement_renoncer_non_valide.xml" })
    public void testRechercherSousProgrammesPourRenoncementDotationAvecNonValides() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/referentiel/collectivite/" + idCollectivite + "/sous-programme-travaux/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertNotNull(sousProgrammes);
        Assertions.assertEquals(2, sousProgrammes.length);
        Assertions.assertEquals("Renforcement des réseaux", sousProgrammes[0].getDescription());
    }

    /**
     * Test rechercher departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_sous_programme_projet.xml" })
    public void testRechercherSousProgrammesProjet() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/referentiel/collectivite/" + idCollectivite + "/sous-programme-projet/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertNotNull(sousProgrammes);
        Assertions.assertEquals(2, sousProgrammes.length);
    }

    /**
     * Test rechercher departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_sous_programme_projet_non_valide.xml" })
    public void testRechercherSousProgrammesProjetAvecNonValide() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/referentiel/collectivite/" + idCollectivite + "/sous-programme-projet/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertNotNull(sousProgrammes);
        Assertions.assertEquals(2, sousProgrammes.length);
    }

    /**
     * Test rechercher sous programmes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testRechercherSousProgrammes() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/referentiel/collectivite/" + idCollectivite + "/sous-programme/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));
    }

    /**
     * Test lister collectivites par departement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/administration/departement/in_collectivites_liste.xml" })
    public void testListerCollectivitesParDepartement() throws Exception {
        String codeDepartement = "001";
        String uri = "/api/referentiel/collectivite/departement/" + codeDepartement + "/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        CollectiviteLovDto[] collectivites = this.mapFromJson(content, CollectiviteLovDto[].class);

        Assertions.assertTrue(collectivites.length == 3);

        codeDepartement = "002";
        uri = "/api/referentiel/collectivite/departement/" + codeDepartement + "/lister";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));
    }

    /**
     * Test lister type demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerTypeDemandePaiement() throws Exception {

        String uri = "/api/referentiel/type-demande-paiement/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        TypeDemandePaiementLovDto[] typeDemandes = this.mapFromJson(content, TypeDemandePaiementLovDto[].class);

        Assertions.assertEquals(3, typeDemandes.length);
    }

    /**
     * Test lister types anomalie pour demande paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerTypesAnomaliePourDemandePaiement() throws Exception {

        String uri = "/api/referentiel/type-anomalie/paiement/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        TypeAnomalieLovDto[] typeAnomalies = this.mapFromJson(content, TypeAnomalieLovDto[].class);

        Assertions.assertEquals(4, typeAnomalies.length);
    }

    /**
     * Test lister types anomalie pour demande subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml" })
    public void testListerTypesAnomaliePourDemandeSubvention() throws Exception {

        String uri = "/api/referentiel/type-anomalie/subvention/lister";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        TypeAnomalieLovDto[] typeAnomalies = this.mapFromJson(content, TypeAnomalieLovDto[].class);

        Assertions.assertEquals(4, typeAnomalies.length);
    }

    /**
     * Lister sous programmes pour creation de ligne dotation departementale.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_departementale_rechercher.xml" })
    public void testRechercherSousProgrammesPourLigneDotationDepartementale() throws Exception {

        String uri = "/api/referentiel/dotation/departement/lignes/sous-programme/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        SousProgrammeLovDto[] sousProgrammes = this.mapFromJson(content, SousProgrammeLovDto[].class);

        Assertions.assertNotNull(sousProgrammes);
        Assertions.assertEquals(1, sousProgrammes.length);

    }

}

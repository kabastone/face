package fr.gouv.sitde.face.webapp.controller.commun;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.AnomalieDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

/**
 * The Class AnomalieRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AnomalieRestControllerIT extends AbstractTestWeb {

    /**
     * Test ajouter anomalie.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_ajouter.xml" })
    @ExpectedDatabase(value = "/dataset/expected/anomalie/ex_anomalie_ajouter.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAjouterAnomalie() throws Exception {
        String uri = "/api/anomalie/ajouter";
        MvcResult mvcResult;

        // Création de l'anomalie
        AnomalieDto anomalie = new AnomalieDto();
        anomalie.setIdDemandeSubvention(900001L);
        anomalie.setCorrigee(false);
        anomalie.setProblematique("Ceci est ma problématique");
        anomalie.setCodeTypeAnomalie("ANO_SUB_0001");

        String inputJson = this.mapToJson(anomalie);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test modifier anomalie.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_modifier.xml" })
    @ExpectedDatabase(value = "/dataset/expected/anomalie/ex_anomalie_modifier.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierAnomalie() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/subvention/900001/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(1, anomalies.length);

        AnomalieDto anomalie = anomalies[0];
        anomalie.setProblematique("okok");
        anomalie.setIdDemandePaiement(null);

        uri = "/api/anomalie/" + anomalie.getId() + "/modifier";

        String inputJson = this.mapToJson(anomalie);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test modifier anomalie sans réponse.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_modifier.xml" })
    @ExpectedDatabase(value = "/dataset/expected/anomalie/ex_anomalie_modifier_sans_reponse.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testModifierAnomalieSansReponse() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/subvention/900001/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(1, anomalies.length);

        AnomalieDto anomalie = anomalies[0];
        anomalie.setProblematique("okok");
        anomalie.setReponse(null);
        anomalie.setIdDemandePaiement(null);

        uri = "/api/anomalie/" + anomalie.getId() + "/modifier";

        String inputJson = this.mapToJson(anomalie);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    /**
     * Test modifier anomalie.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_rechercher.xml" })
    public void testRechercherAnomalieSubvention() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/subvention/900001/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(5, anomalies.length);
    }

    /**
     * Test modifier anomalie.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_rechercher.xml" })
    public void testRechercherAnomalieSubventionAODE() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/subvention/900001/rechercher";

        mvcResult = this.mvc.perform(
                AbstractTestWeb.construireRequetePourRole(uri, RequestMethod.GET, RoleEnum.GENERIQUE).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(2, anomalies.length);
    }

    /**
     * Test modifier anomalie.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_pai_rechercher.xml" })
    public void testRechercherAnomaliePaiement() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/paiement/900001/rechercher";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(5, anomalies.length);
    }

    /**
     * Test rechercher anomalie demande de paiement par agent de collectivité.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_pai_rechercher.xml" })
    public void testRechercherAnomalieAODEPaiement() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/paiement/900001/rechercher";

        mvcResult = this.mvc.perform(
                AbstractTestWeb.construireRequetePourRole(uri, RequestMethod.GET, RoleEnum.GENERIQUE).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        AnomalieDto[] anomalies = this.mapFromJson(content, AnomalieDto[].class);

        Assertions.assertEquals(2, anomalies.length);
    }

    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_pai.xml" })
    public void testDecorrigerAnomaliePaiement() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/900002/paiement/900001/maj-correction";

        String inputJson = this.mapToJson(true);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_pai.xml" })
    @ExpectedDatabase(value = "/dataset/expected/anomalie/ex_anomalie_decorriger_paiement.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCorrigerAnomaliePaiement() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/900002/paiement/900001/maj-correction";

        String inputJson = this.mapToJson(false);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_pai.xml" })
    public void testDecorrigerAnomalieSubvention() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/900001/subvention/900001/maj-correction";

        String inputJson = this.mapToJson(true);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }

    @Test
    @Transactional
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/anomalie/in_anomalie_sub_pai.xml" })
    @ExpectedDatabase(value = "/dataset/expected/anomalie/ex_anomalie_decorriger_subvention.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testCorrigerAnomalieSubvention() throws Exception {
        MvcResult mvcResult;
        String uri = "/api/anomalie/900001/subvention/900001/maj-correction";

        String inputJson = this.mapToJson(false);

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
    }
}

package fr.gouv.sitde.face.webapp.controller.dotation;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.gouv.sitde.face.application.dto.DotationRenoncementDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.webapp.configuration.FichiersUploadTestWeb;

/**
 * The Class DotationRenoncementRestControllerIT.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class DotationRenoncementRestControllerIT extends FichiersUploadTestWeb {

    /**
     * Test rechercher renoncement dotation par id collectivite et id sous programme.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_renoncement_renoncer.xml" })
    public void testRechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme() throws Exception {

        String idCollectivite = "900001";
        String idSousProgramme = "900001";
        String uri = "/api/dotation/renoncement/collectivite/" + idCollectivite + "/sous-programme/" + idSousProgramme + "/rechercher";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));

        String content = mvcResult.getResponse().getContentAsString();
        DotationRenoncementDto dotationRenoncementDto = this.mapFromJson(content, DotationRenoncementDto.class);

        Assertions.assertNotNull(dotationRenoncementDto);
        Assertions.assertNotNull(dotationRenoncementDto.getDotationDisponible());
        Assertions.assertEquals(new BigDecimal(10.20).setScale(2, RoundingMode.HALF_UP), dotationRenoncementDto.getDotationDisponible());
    }

    /**
     * Test renoncer dotation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/dotation/in_dotation_renoncement_renoncer.xml" })
    @ExpectedDatabase(value = "/dataset/expected/dotation/ex_dotation_renoncement_renoncer.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testRenoncerDotation() throws Exception {

        // init
        String idCollectivite = "900001";
        String idSousProgramme = "900001";
        String montant = "10";
        String uri;
        String content;
        MvcResult mvcResult;
        DotationRenoncementDto dotationRenoncementDto;

        FichierTransfert fichierTransfert = FichiersUploadTestWeb.getFichierTransfertTempFile1();
        MockMultipartFile fichierEnvoyer = new MockMultipartFile(TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION.toString(),
                fichierTransfert.getNomFichier(), fichierTransfert.getContentTypeTheorique(), fichierTransfert.getBytes());

        uri = "/api/dotation/renoncement/renoncer";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.multipart(uri).file(fichierEnvoyer).contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .param("idCollectivite", idCollectivite).param("idSousProgramme", idSousProgramme).param("montant", montant)).andReturn();

        Assertions.assertTrue(isResultOkSansException(mvcResult));

        content = mvcResult.getResponse().getContentAsString();
        dotationRenoncementDto = this.mapFromJson(content, DotationRenoncementDto.class);

        Assertions.assertNotNull(dotationRenoncementDto);
    }
}

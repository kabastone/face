
package fr.gouv.sitde.face.webapp.controller.subvention;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.CadencementTableDto;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;

/**
 * Classe de test de DossierSubventionRestController.
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class CadencementRestControllerIT extends AbstractTestWeb {

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_cadencement_rechercher.xml" })
    public void testRecupererCadencementParCollectivite() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/subvention/cadencement/recuperer/collectivite/" + idCollectivite + "?deProjet=true";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

        uri = "/api/subvention/cadencement/recuperer/collectivite/" + idCollectivite + "?deProjet=false";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

        idCollectivite = "900002";
        uri = "/api/subvention/cadencement/recuperer/collectivite/" + idCollectivite + "?deProjet=false";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        List<CadencementTableDto> cadencements = this.mapFromListJson(content, CadencementTableDto.class);
        Assertions.assertTrue(cadencements.size() == 4);
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_cadencement_rechercher_prolongation.xml" })
    public void testRecupererCadencementAvecProlongationParCollectivite() throws Exception {

        String idCollectivite = "900001";
        String uri = "/api/subvention/cadencement/recuperer/collectivite/" + idCollectivite + "?deProjet=false";

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultDataNotFoundException(mvcResult));

        idCollectivite = "900002";
        uri = "/api/subvention/cadencement/recuperer/collectivite/" + idCollectivite + "?deProjet=false";

        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        List<CadencementTableDto> cadencements = this.mapFromListJson(content, CadencementTableDto.class);
        Assertions.assertEquals(5, cadencements.size());

        for (CadencementTableDto dto : cadencements) {
            if (dto.getHasProlongation()) {
                Assertions.assertEquals(2015, dto.getAnneeProgrammation());
                Assertions.assertEquals(new BigDecimal("10000"), dto.getCadencement().getMontantAnneeProlongation());
            } else {
                Assertions.assertEquals(BigDecimal.ZERO, dto.getCadencement().getMontantAnneeProlongation());
            }
        }
    }

    @Test
    @DatabaseSetup({ "/dataset/input/in_referentielV2.xml", "/dataset/input/subvention/in_cadencement_rechercher_prolongation.xml" })
    public void testRechercherCadencement() throws Exception {

        String codeProgramme = "793";
        String uri = "/api/subvention/cadencement/criteres/code/" + codeProgramme + "/rechercher";

        CritereRechercheCadencementQo qo = new CritereRechercheCadencementQo();
        qo.setAnneeProgrammation(null);
        String input = this.mapToJson(qo);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).content(input).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();

        qo.setAnneeProgrammation(2008);
        input = this.mapToJson(qo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).content(input).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        qo.setAnneeProgrammation(2019);
        input = this.mapToJson(qo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).content(input).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        qo.setAnneeProgrammation(null);
        qo.setIdCollectivite(new Long("900001"));
        input = this.mapToJson(qo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).content(input).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

        qo.setIdCollectivite(new Long("900001"));
        qo.setIdDepartement(new Integer("900001"));
        input = this.mapToJson(qo);
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).content(input).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();

    }

}

/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.tableaudebord;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DossierSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DotationCollectiviteTdbDto;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;
import fr.gouv.sitde.face.webapp.configuration.AbstractTestWeb;
import fr.gouv.sitde.face.webapp.controller.api.subvention.PageDemandeWithCollectiviteDto;

/**
 * Classe de test de TableauDeBordGeneRestControllerIT.
 *
 * @author a453029
 */
@DatabaseTearDown(value = "/dataset/teardown_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TableauDeBordGeneRestControllerIT extends AbstractTestWeb {

    /**
     * Test rechercher tableau de bord generique ademander subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTableauDeBordGenerique() throws Exception {

        String idCollectivite = "900003";

        // recherche
        String uri = "/api/tableaudebord/gene/" + idCollectivite + "/rechercher";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        TableauDeBordGeneDto tableauDeBordGeneDto = this.mapFromJson(content, TableauDeBordGeneDto.class);

        Assertions.assertEquals(0L, tableauDeBordGeneDto.getNbAdemanderSubvention());

        idCollectivite = "900001";
        uri = "/api/tableaudebord/gene/" + idCollectivite + "/rechercher";
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        content = mvcResult.getResponse().getContentAsString();
        tableauDeBordGeneDto = this.mapFromJson(content, TableauDeBordGeneDto.class);

        Assertions.assertEquals(1L, tableauDeBordGeneDto.getNbAdemanderSubvention());
    }

    /**
     * Test rechercher tdb gene ademander subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAdemanderSubvention() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/subademander/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DotationCollectiviteTdbDto> pageReponse = this.mapPageReponseFromJson(content, DotationCollectiviteTdbDto.class);
        Assertions.assertFalse(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb gene ademander paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAdemanderPaiement() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/paiademander/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DossierSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DossierSubventionTdbDto.class);
        Assertions.assertEquals(1, pageReponse.getListeResultats().size());
    }

    /**
     * Test rechercher tdb gene acompleter subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAcompleterSubvention() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/subacompleter/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DotationCollectiviteTdbDto> pageReponse = this.mapPageReponseFromJson(content, DotationCollectiviteTdbDto.class);
        Assertions.assertEquals(1, pageReponse.getListeResultats().size());
    }

    /**
     * Test rechercher tdb gene acorriger subvention.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAcorrigerSubvention() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/subacorriger/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandeSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb gene acorriger paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAcorrigerPaiement() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/paiacorriger/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DemandePaiementTdbDto> pageReponse = this.mapPageReponseFromJson(content, DemandePaiementTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb gene acommencer paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAcommencerPaiement() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/paiacommencer/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DossierSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DossierSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }

    /**
     * Test rechercher tdb gene asolder paiement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @DatabaseSetup({ "/dataset/input/in_referentiel.xml", "/dataset/input/tableaudebord/in_rechercher_tableau_de_bord_gene.xml" })
    public void testRechercherTdbGeneAsolderPaiement() throws Exception {

        PageDemandeWithCollectiviteDto pageDemandeWithColDto = new PageDemandeWithCollectiviteDto();
        PageDemande pageDemande = new PageDemande();
        pageDemande.setTaillePage(2);
        pageDemande.setIndexPage(0);
        pageDemandeWithColDto.setPageDemande(pageDemande);
        pageDemandeWithColDto.setIdCollectivite(900001L);

        // recherche
        String uri = "/api/tableaudebord/gene/paiasolder/rechercher";
        String inputJson = this.mapToJson(pageDemandeWithColDto);
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        Assertions.assertTrue(isResultOkSansException(mvcResult));
        String content = mvcResult.getResponse().getContentAsString();
        PageReponse<DossierSubventionTdbDto> pageReponse = this.mapPageReponseFromJson(content, DossierSubventionTdbDto.class);
        Assertions.assertTrue(pageReponse.getListeResultats().isEmpty());
    }
}

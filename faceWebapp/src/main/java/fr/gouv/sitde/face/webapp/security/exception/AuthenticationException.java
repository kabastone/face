package fr.gouv.sitde.face.webapp.security.exception;

/**
 * The Class AuthenticationException.
 */
public class AuthenticationException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new authentication exception.
     */
    public AuthenticationException() {
        super();
    }

    /**
     * Instantiates a new authentication exception.
     *
     * @param message
     *            the message
     */
    public AuthenticationException(String message) {
        super(message);
    }

    /**
     * Instantiates a new authentication exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public AuthenticationException(String message, Exception cause) {
        super(message, cause);
    }
}

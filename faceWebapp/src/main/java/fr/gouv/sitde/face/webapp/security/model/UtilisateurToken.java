package fr.gouv.sitde.face.webapp.security.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Utilisateur correspondant aux données stockées dans le token JWT.
 *
 * @author Atos
 */
public class UtilisateurToken implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7497580025293649940L;

    /**
     * L'identifiant Cerbere.
     */
    private String idCerbere;

    /**
     * Le nom.
     */
    private String nom;

    /**
     * Le prenom.
     */
    private String prenom;

    /**
     * L'adresse email.
     */
    private String email;

    /**
     * Liste des Roles de l'utilisateur.
     */
    private List<RoleEnum> listeRoles = new ArrayList<>(0);

    /**
     * Gets the id cerbere.
     *
     * @return the id cerbere
     */
    public String getIdCerbere() {
        return this.idCerbere;
    }

    /**
     * Sets the id cerbere.
     *
     * @param id
     *            the new id cerbere
     */
    public void setIdCerbere(final String id) {
        this.idCerbere = id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets the nom.
     *
     * @param pnom
     *            the new nom
     */
    public void setNom(final String pnom) {
        this.nom = pnom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets the prenom.
     *
     * @param pprenom
     *            the new prenom
     */
    public void setPrenom(final String pprenom) {
        this.prenom = pprenom;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param mail
     *            the new email
     */
    public void setEmail(final String mail) {
        this.email = mail;
    }

    /**
     * Gets the liste Roles.
     *
     * @return the listeRoles
     */
    public List<RoleEnum> getListeRoles() {
        return this.listeRoles;
    }

    /**
     * Sets the liste Roles.
     *
     * @param listeRoles
     *            the listeRoles to set
     */
    public void setListeRoles(final List<RoleEnum> listeRoles) {
        this.listeRoles = listeRoles;
    }

    /**
     * renvoie true si l'utilisateur est authentifié.
     *
     * @return flag
     */
    public boolean isAuthentifie() {
        // Si l'utilisateur possède un idCerbere, c'est qu'il a été authentifié par Cerbere.
        return StringUtils.isNotEmpty(this.idCerbere);
    }

    /**
     * renvoie true si l'utilisateur possède le role MFER_INSTRUCTEUR.
     *
     * @return flag
     */
    public boolean isMferInstructeur() {
        return this.getListeRoles().contains(RoleEnum.MFER_INSTRUCTEUR);
    }

    /**
     * renvoie true si l'utilisateur possède le role MFER_RESPONSABLE.
     *
     * @return flag
     */
    public boolean isMferResponsable() {
        return this.getListeRoles().contains(RoleEnum.MFER_RESPONSABLE);
    }

    /**
     * renvoie true si l'utilisateur possède le role SD7_INSTRUCTEUR.
     *
     * @return flag
     */
    public boolean isSd7Instructeur() {
        return this.getListeRoles().contains(RoleEnum.SD7_INSTRUCTEUR);
    }

    /**
     * renvoie true si l'utilisateur possède le role SD7_RESPONSABLE.
     *
     * @return flag
     */
    public boolean isSd7Responsable() {
        return this.getListeRoles().contains(RoleEnum.SD7_RESPONSABLE);
    }
}

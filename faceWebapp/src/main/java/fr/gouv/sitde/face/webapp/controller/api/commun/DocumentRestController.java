/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.commun;

import java.io.File;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.document.GestionDocumentApplication;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelDownloadUtils;

/**
 * Controller REST de téléchargement des documents.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class DocumentRestController extends ConnexionAwareController {

    /** The gestion document application. */
    @Inject
    private GestionDocumentApplication gestionDocumentApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Téléchargement d'un document par id.
     *
     * @param request
     *            the request
     * @param idDocument
     *            the id document
     * @param response
     *            the response
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/document/{idDocument:[0-9]+}/telecharger", method = RequestMethod.GET)
    public void telechargerDocument(HttpServletRequest request, @PathVariable(value = "idDocument", required = true) Long idDocument,
            HttpServletResponse response) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter télécharger un document
        this.accesCollectiviteValidator.validerAccesDocument(this.getUtilisateurConnecte(request), idDocument);

        // obtention du fichier
        File fichierTelecharger = this.gestionDocumentApplication.telechargerDocument(idDocument);

        // lancement du téléchargement
        ModelDownloadUtils.telechargerFichierFromFile(fichierTelecharger, response);
    }

    /**
     * Supprimer document.
     *
     * @param request
     *            the request
     * @param idDocument
     *            the id document
     * @param response
     *            the response
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/document/{idDocument:[0-9]+}/supprimer", method = RequestMethod.DELETE)
    public String supprimerDocument(HttpServletRequest request, @PathVariable(value = "idDocument", required = true) Long idDocument,
            HttpServletResponse response) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter télécharger un document
        this.accesCollectiviteValidator.validerAccesDocument(this.getUtilisateurConnecte(request), idDocument);

        // suppression du fichier
        this.gestionDocumentApplication.supprimerDocument(idDocument);

        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }
}

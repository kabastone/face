package fr.gouv.sitde.face.webapp.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.MessageUtils;
import fr.gouv.sitde.face.webapp.error.ApiError;
import fr.gouv.sitde.face.webapp.exception.DataNotFoundException;

/**
 * The Class CustomRestExceptionHandler.
 *
 * @author a453029
 */
@ControllerAdvice
public class CustomRestExceptionHandler {

    /**
     * Logger SLF4J.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestExceptionHandler.class);

    /**
     * Intercepte toute DataNotFoundException, la logue et construit le bean ApiError renvoyé en Json.
     *
     * @param ex
     *            the ex
     * @param request
     *            the request
     * @return the response entity
     */
    @ExceptionHandler({ DataNotFoundException.class })
    public ResponseEntity<Object> handleDataNotFoundException(Exception ex, WebRequest request) {
        // Log en debug
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("NoDataFoundException - {}", request.getDescription(false));
        }

        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, DataNotFoundException.class.getSimpleName(), ex.getMessage());
        return getResponseEntityFromApiError(apiError);
    }

    /**
     * Intercepte toute RegleGestionException, la logue et construit le bean ApiError renvoyé en Json.
     *
     * @param ex
     *            the ex
     * @param request
     *            the request
     * @return the response entity
     */
    @ExceptionHandler({ RegleGestionException.class })
    public ResponseEntity<Object> handleRegleGestionException(Exception ex, WebRequest request) {
        // Log en debug
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("RegleGestionException - {}", ex.getMessage());
        }

        RegleGestionException rge = (RegleGestionException) ex;
        ApiError apiError = new ApiError(HttpStatus.OK, RegleGestionException.class.getSimpleName(), MessageUtils.getMessageMetier(rge),
                MessageUtils.getListeMessagesMetier(rge));
        return getResponseEntityFromApiError(apiError);
    }

    /**
     * Intercepte toute TechniqueException ou toute exception non prévue, la logue et construit le bean ApiError renvoyé en Json.
     *
     * @param ex
     *            the ex
     * @param request
     *            the request
     * @return the response entity
     */
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request) {
        // Log en erreur
        LOGGER.error(ex.getMessage(), ex);

        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, TechniqueException.class.getSimpleName(),
                MessageUtils.getMsgValidationMetier("erreur.technique.message.generique"));
        return getResponseEntityFromApiError(apiError);
    }

    /**
     * Gets the response entity from api error.
     *
     * @param apiError
     *            the api error
     * @return the response entity from api error
     */
    private static ResponseEntity<Object> getResponseEntityFromApiError(ApiError apiError) {
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}

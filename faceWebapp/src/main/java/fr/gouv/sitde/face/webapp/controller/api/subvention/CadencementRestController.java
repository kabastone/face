/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.subvention;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dto.CadencementDto;
import fr.gouv.sitde.face.application.dto.CadencementTableDto;
import fr.gouv.sitde.face.application.subvention.GestionCadencementApplication;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * ImportDotationRestController : controlleur REST d'import de fichier CSV des dotations departementales.
 *
 * @author Atos
 */

@RestController
@RequestMapping("/api/subvention/cadencement")
public class CadencementRestController extends ConnexionAwareController {

    @Inject
    private GestionCadencementApplication gestionCadencement;

    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Verifier dotation annuelle notifiee true : validation OK, false : validation KO.
     *
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_GENERIQUE })
    @RequestMapping(value = "/recuperer/collectivite/{idCollectivite:[0-9]+}", method = RequestMethod.GET)
    public String recupererCadencementsPourCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite, @RequestParam(required = true) Boolean deProjet) {
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferEtAgent(this.getUtilisateurConnecte(request), idCollectivite);
        List<CadencementTableDto> results = this.gestionCadencement.recupererCadencementsPourCollectivite(idCollectivite, deProjet);
        return JsonMapper.getJsonFromData(results);
    }

    /**
     * Modifier cadencement.
     *
     * @param request
     *            the request
     * @param idCadencement
     *            the id cadencement
     * @param cadencementDto
     *            the cadencement dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_GENERIQUE })
    @RequestMapping(value = "/{idCadencement}/modifier", method = RequestMethod.POST)
    public String modifierCadencement(HttpServletRequest request, @PathVariable(value = "idCadencement", required = true) Long idCadencement,
            @RequestBody CadencementDto cadencementDto) {

        CadencementDto cadencementDtoBdd = this.gestionCadencement.updateCadencement(cadencementDto);
        return JsonMapper.getJsonFromData(cadencementDtoBdd);

    }

    @RolesAllowed({ RoleEnum.Constantes.ROLE_GENERIQUE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/estValide", method = RequestMethod.GET)
    public String estCadencementValide(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        Boolean cadencementEstValide = this.gestionCadencement.estToutCadencementValidePourCollectivite(idCollectivite);
        return JsonMapper.getJsonFromData(cadencementEstValide);
    }

    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "criteres/code/{codeProgramme:[0-9]+}/rechercher", method = RequestMethod.POST)
    public String rechercherCadencementsParCriteres(@RequestBody CritereRechercheCadencementQo criteresRecherche,
            @PathVariable(value = "codeProgramme", required = true) String codeProgramme, HttpServletRequest request) {

        List<ResultatRechercheCadencementDto> resultatRechercheCadencement = this.gestionCadencement
                .rechercherCadencementsParCriteres(criteresRecherche, codeProgramme);

        return JsonMapper.getJsonFromData(resultatRechercheCadencement);

    }

}

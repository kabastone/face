/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.connexion;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.webapp.configuration.AngularProperties;
import fr.gouv.sitde.face.webapp.security.CerbereService;
import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;

/**
 * ConnexionController : controller de connexion / déconnexion.
 *
 * @author Atos
 */
@Controller
public class ConnexionController {

    /** The jwt token provider. */
    @Inject
    private JwtTokenProvider jwtTokenProvider;

    /** The gestion utilisateur application. */
    @Inject
    private GestionUtilisateurApplication gestionUtilisateurApplication;

    /** The angular properties. */
    @Inject
    private AngularProperties angularProperties;

    /**
     * Connexion à l'application. <br/>
     * La requete a été interceptée par le filtre Cerbere. <br/>
     * On construit un token JWT à partir des infos cerbere et on place le token dans un cookie.<br/>
     * On redirige ensuite le client vers l'appli Angular.
     *
     * @param request
     *            the http servlet request
     * @param response
     *            the response
     * @return the string
     */
    @RequestMapping("/connecter")
    public String connecter(HttpServletRequest request, HttpServletResponse response) {

        // Créer le token JWT
        String token = this.creerTokenJwt(request);

        UtilisateurToken utilisateurToken = this.jwtTokenProvider.getUtilisateurTokenFromToken(token);

        // Si l'utilisateur est authentifié, on synchronise l'utilisateur avec la base
        if (utilisateurToken.isAuthentifie()) {

            // L'utilisateur est créé en base s'il n'existe pas, sinon son cerbereId est enregistré s'il a été modifié.
            UtilisateurAdminDto utilisateurDto = getUtilisateurAdminDtoFromUtilisateurToken(utilisateurToken);
            this.gestionUtilisateurApplication.synchroniserUtilisateurConnecte(utilisateurDto);
        }

        // On met le token dans un cookie qu'on place en réponse.
        this.jwtTokenProvider.stockerTokenDansCookieTemporaire(token, response, request);

        return "redirect:" + this.angularProperties.getAppUrl();
    }

    /**
     * Supprime un éventuel existant cookie temporaire de token.<br/>
     * Déconnecte l'utilisateur via l'API cerbere. <br/>
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return message de retour
     */
    @RequestMapping("/deconnecter")
    public void deconnecter(HttpServletRequest request, HttpServletResponse response) {
        // Suppression d'un éventuel existant cookie temporaire de token.
        this.jwtTokenProvider.supprimerCookieTemporaireToken(response);

        // Déconnexion Cerbere
        CerbereService.deconnecterUtilisateur(request, response);
    }

    /**
     * Creer token jwt.
     *
     * @param httpServletRequest
     *            the http servlet request
     * @return the string
     */
    private String creerTokenJwt(HttpServletRequest httpServletRequest) {
        String token = this.jwtTokenProvider.createAccessToken(httpServletRequest);
        Authentication authentication = this.jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return token;
    }

    /**
     * Gets the utilisateur admin dto from utilisateur token.
     *
     * @param tokenUser
     *            the token user
     * @return the utilisateur admin dto from utilisateur token
     */
    private static UtilisateurAdminDto getUtilisateurAdminDtoFromUtilisateurToken(UtilisateurToken tokenUser) {
        UtilisateurAdminDto userDto = new UtilisateurAdminDto();
        userDto.setCerbereId(tokenUser.getIdCerbere());
        userDto.setEmail(tokenUser.getEmail());
        userDto.setNom(tokenUser.getNom());
        userDto.setPrenom(tokenUser.getPrenom());
        return userDto;
    }

}

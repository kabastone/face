/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.commun.GestionEmailContactApplication;
import fr.gouv.sitde.face.transverse.email.EmailContactDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;

/**
 * The Class EmailContactRestController.
 */
@RestController
@RequestMapping("/api")
public class EmailContactRestController extends ConnexionAwareController {

    @Inject
    private GestionEmailContactApplication gestionEmailContactApplication;

    @RequestMapping(value = "/nous-contacter/email/envoi", method = RequestMethod.POST)
    public Boolean envoyerEmailContact(@RequestBody EmailContactDto emailContactDto) {
        return this.gestionEmailContactApplication.envoyerEmailContact(emailContactDto);

    }
}

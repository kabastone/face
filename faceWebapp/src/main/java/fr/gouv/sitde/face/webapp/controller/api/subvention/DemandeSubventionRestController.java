package fr.gouv.sitde.face.webapp.controller.api.subvention;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonMappingException;

import fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CadencementDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionDetailsDto;
import fr.gouv.sitde.face.application.dto.lov.DossierSubventionLovDto;
import fr.gouv.sitde.face.application.subvention.GenerationDemandeSubventionApplication;
import fr.gouv.sitde.face.application.subvention.GestionDemandeSubventionApplication;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelDownloadUtils;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class DemandeSubventionRestController.
 */
@RestController
@RequestMapping("/api/subvention/")
public class DemandeSubventionRestController extends ConnexionAwareController {

    /** The gestion demande subvention application. */
    @Inject
    private GestionDemandeSubventionApplication gestionDemandeSubventionApplication;

    /** The generation demande subvention application. */
    @Inject
    private GenerationDemandeSubventionApplication generationDemandeSubventionApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    @Inject
    private GestionCollectiviteApplication gestionCollectiviteApplication;

    /**
     * Rechercher demande subvention par id.
     *
     * @param request
     *            the request
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/{idDemandeSubvention}/rechercher", method = RequestMethod.GET)
    public String rechercherDemandeSubventionParId(HttpServletRequest request,
            @PathVariable(value = "idDemandeSubvention", required = true) Long idDemandeSubvention) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDemandeSubvention(this.getUtilisateurConnecte(request), idDemandeSubvention);

        DemandeSubventionDetailsDto demandeSubventionDetailDto = this.gestionDemandeSubventionApplication
                .rechercherDemandeSubventionParId(idDemandeSubvention);
        return JsonMapper.getJsonFromData(demandeSubventionDetailDto);
    }

    /**
     * Generer demande subvention pdf par id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param response
     *            the response
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE,
            RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention:[0-9]+}/generation", method = RequestMethod.GET)
    public void genererDemandeSubventionPdfParId(@PathVariable(value = "idDemandeSubvention", required = true) Long idDemandeSubvention,
            HttpServletResponse response) {
        ModelDownloadUtils.telechargerFichierFromFichierTransfert(
                this.generationDemandeSubventionApplication.genererDemandeSubventionPdf(idDemandeSubvention), response);
    }

    /**
     * Inits the nouvelle demande subvention.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/initialiser", method = RequestMethod.GET)
    public String initNouvelleDemandeSubvention() {

        DemandeSubventionDetailsDto demandeSubvention = this.gestionDemandeSubventionApplication.initNouvelleDemandeSubvention();
        return JsonMapper.getJsonFromData(demandeSubvention);
    }

    /**
     * Inits the nouvelle demande subvention avec dossier.
     *
     * @param request
     *            the request
     * @param idDossier
     *            the id dossier
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/dossier/{idDossier}/initialiser", method = RequestMethod.GET)
    public String initNouvelleDemandeSubventionAvecDossier(HttpServletRequest request,
            @PathVariable(value = "idDossier", required = true) Long idDossier) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossier);

        DemandeSubventionDetailsDto demandeSubvention = this.gestionDemandeSubventionApplication.initNouvelleDemandeSubvention(idDossier);
        return JsonMapper.getJsonFromData(demandeSubvention);
    }

    /**
     * Creer nouvelle demande subvention.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/creer", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String creerNouvelleDemandeSubvention(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // Initialisation
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // Récupération de tous les propriétés de l'objet Demande Subvention Details DTO depuis la Http Request.
        DemandeSubventionDetailsDto demandeSubvention = genererDtoViaRequest(servletRequest);

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                demandeSubvention.getIdCollectivite());

        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.creerDemandeSubvention(demandeSubvention);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Generer dto via request.
     *
     * @param servletRequest
     *            the servlet request
     * @return the demande subvention details dto
     */
    private static DemandeSubventionDetailsDto genererDtoViaRequest(MultipartHttpServletRequest servletRequest) {
        DemandeSubventionDetailsDto demande = new DemandeSubventionDetailsDto();
        DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);
        CadencementDto cadencementDto = new CadencementDto();

        // Mapping manuel des propriétés
        if (StringUtils.isNotBlank(servletRequest.getParameter("id"))) {
            demande.setId(Long.valueOf(servletRequest.getParameter("id")));
            cadencementDto.setId(Long.valueOf(servletRequest.getParameter("id")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("etat"))) {
            demande.setEtat(servletRequest.getParameter("etat"));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("numDossier"))) {
            demande.setNumDossier(servletRequest.getParameter("numDossier"));
        }
        String dateJour = servletRequest.getParameter("dateJour");
        if (StringUtils.isNotBlank(dateJour)) {
            demande.setDateJour(LocalDateTime.of(LocalDate.parse(dateJour, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("chorusNumLigneLegacy"))) {
            demande.setChorusNumLigneLegacy(servletRequest.getParameter("chorusNumLigneLegacy"));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantHTTravauxEligibles"))) {
            demande.setMontantHTTravauxEligibles(new BigDecimal(servletRequest.getParameter("montantHTTravauxEligibles")));
        }
        String dateEtat = servletRequest.getParameter("dateEtat");
        if (StringUtils.isNotBlank(dateEtat)) {
            demande.setDateEtat(LocalDateTime.of(LocalDate.parse(dateEtat, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        String dateAccord = servletRequest.getParameter("dateAccord");
        if (StringUtils.isNotBlank(dateAccord)) {
            demande.setDateAccord(LocalDateTime.of(LocalDate.parse(dateAccord, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("tauxAideFacePercent"))) {
            demande.setTauxAideFacePercent(new BigDecimal(servletRequest.getParameter("tauxAideFacePercent")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("message"))) {
            demande.setMessage(servletRequest.getParameter("message"));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("motifRefus"))) {
            demande.setMotifRefus(servletRequest.getParameter("motifRefus"));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("demandeComplementaire"))) {
            demande.setDemandeComplementaire(Boolean.parseBoolean(servletRequest.getParameter("demandeComplementaire")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("plafondAide"))) {
            demande.setPlafondAide(new BigDecimal(servletRequest.getParameter("plafondAide")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idDotationCollectivite"))) {
            demande.setIdDotationCollectivite(Long.valueOf(servletRequest.getParameter("idDotationCollectivite")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idCollectivite"))) {
            demande.setIdCollectivite(Long.valueOf(servletRequest.getParameter("idCollectivite")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idDossier"))) {
            demande.setIdDossier(Long.valueOf(servletRequest.getParameter("idDossier")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idSousProgramme"))) {
            demande.setIdSousProgramme(Long.valueOf(servletRequest.getParameter("idSousProgramme")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("version"))) {
            demande.setVersion(Integer.parseInt(servletRequest.getParameter("version")));
        }
        genererDtoCadencementViaRequest(servletRequest, cadencementDto);

        demande.setCadencementDto(cadencementDto);
        // Mapping des fichiers, uniquement des "DOC_DEMANDE_SUBVENTION"
        alimenterListeFichiersDemande(servletRequest, demande);

        // Mapping des id des docs complémentaires à supprimer
        alimenterIdsDocsComplSuppr(servletRequest, demande);

        return demande;
    }

    /**
     * @param servletRequest
     * @param cadencementDto
     */
    private static void genererDtoCadencementViaRequest(MultipartHttpServletRequest servletRequest, CadencementDto cadencementDto) {
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantAnnee1"))
                && !"null".equalsIgnoreCase(servletRequest.getParameter("montantAnnee1"))) {

            cadencementDto.setMontantAnnee1(new BigDecimal(servletRequest.getParameter("montantAnnee1")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantAnnee2"))
                && !"null".equalsIgnoreCase(servletRequest.getParameter("montantAnnee2"))) {
            cadencementDto.setMontantAnnee2(new BigDecimal(servletRequest.getParameter("montantAnnee2")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantAnnee3"))
                && !"null".equalsIgnoreCase(servletRequest.getParameter("montantAnnee3"))) {
            cadencementDto.setMontantAnnee3(new BigDecimal(servletRequest.getParameter("montantAnnee3")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantAnnee4"))
                && !"null".equalsIgnoreCase(servletRequest.getParameter("montantAnnee4"))) {
            cadencementDto.setMontantAnnee4(new BigDecimal(servletRequest.getParameter("montantAnnee4")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("montantAnneeProl"))
                && !"null".equalsIgnoreCase(servletRequest.getParameter("montantAnneeProl"))) {
            cadencementDto.setMontantAnneeProlongation(new BigDecimal(servletRequest.getParameter("montantAnneeProl")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("estValide"))) {
            cadencementDto.setEstValide(Boolean.valueOf(servletRequest.getParameter("estValide")));
        }
    }

    /**
     * Alimenter liste fichiers demande.
     *
     * @param servletRequest
     *            the servlet request
     * @param demande
     *            the demande
     */
    private static void alimenterListeFichiersDemande(MultipartHttpServletRequest servletRequest, DemandeSubventionDetailsDto demande) {

        // Mapping des fichiers, uniquement des "DOC_DEMANDE_SUBVENTION"
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.getListeTypeDocumentsPourFamille(FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION)) {
            List<MultipartFile> fichierUploade = servletRequest.getFiles(typeDocument.getCode());
            for (MultipartFile multipartFile : fichierUploade) {
                FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(multipartFile);
                demande.getListeFichiersEnvoyer().add(fichierTransfert);
            }
        }
    }

    /**
     * Alimenter ids docs compl suppr.
     *
     * @param servletRequest
     *            the servlet request
     * @param demande
     *            the demande
     */
    private static void alimenterIdsDocsComplSuppr(MultipartHttpServletRequest servletRequest, DemandeSubventionDetailsDto demande) {

        // Mapping des id des docs complémentaires à supprimer
        if (StringUtils.isNotBlank(servletRequest.getParameter("idsDocsComplSuppr"))) {
            String[] tabIdsDocsComplSuppr = servletRequest.getParameter("idsDocsComplSuppr").split(",");
            List<Long> idsDocsComplSuppr = new ArrayList<>(tabIdsDocsComplSuppr.length);
            for (String id : tabIdsDocsComplSuppr) {
                idsDocsComplSuppr.add(Long.valueOf(id));
            }
            demande.setIdsDocsComplSuppr(idsDocsComplSuppr);
        }
    }

    /**
     * Qualifier demande subvention.
     *
     * @param id
     *            the id
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/qualifier", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String qualifierDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id, HttpServletRequest request,
            HttpServletResponse response) {

        // Initialisation
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        DemandeSubventionDetailsDto demandeSubvention = genererDtoViaRequest(servletRequest);

        List<FichierTransfert> fichiers = new ArrayList<>();

        // Mapping des fichiers, uniquement des "DOC_DEMANDE_SUBVENTION"
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.getListeTypeDocumentsPourFamille(FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION)) {
            MultipartFile fichierUploade = servletRequest.getFile(typeDocument.getCode());
            if (fichierUploade != null) {
                FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichierUploade);
                fichiers.add(fichierTransfert);
            }
        }

        this.gestionCollectiviteApplication.synchroniserCollectivite(demandeSubvention.getIdCollectivite(), demandeSubvention.getId());
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.qualifierDemandeSubvention(demandeSubvention,
                fichiers);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Recuperer dossiers par collectivite.
     *
     * @param id
     *            the id
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/recuperer-dossiers/{idCollectivite}", method = RequestMethod.GET)
    public String recupererDossiersParCollectivite(@PathVariable(value = "idCollectivite", required = true) Long id) {
        List<DossierSubventionLovDto> dossierLov = this.gestionDemandeSubventionApplication.recupererDossiersParDotationCollectivite(id);

        return JsonMapper.getJsonFromData(dossierLov);
    }

    /**
     * Accorder demande subvention.
     *
     * @param id
     *            the id
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/accorder", method = RequestMethod.GET)
    public String accorderDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.accorderDemandeSubvention(id);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Controler demande subvention.
     *
     * @param id
     *            the id
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_RESPONSABLE, RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/controler", method = RequestMethod.GET)
    public String controlerDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.controlerDemandeSubvention(id);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Transferer demande subvention.
     *
     * @param id
     *            the id
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_RESPONSABLE, RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/transferer", method = RequestMethod.GET)
    public String transfererDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.transfererDemandeSubvention(id);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Refuser demande subvention.
     *
     * @param id
     *            the id
     * @param demandeSubvention
     *            the demande subvention
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/refuser", method = RequestMethod.POST)
    public String refuserDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id,
            @RequestBody DemandeSubventionDetailsDto demandeSubvention) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.refuserDemandeSubvention(id, demandeSubvention.getMotifRefus());

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Refuser demande subvention.
     *
     * @param id
     *            the id
     * @param demandeSubvention
     *            the demande subvention
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/rejet-taux", method = RequestMethod.POST)
    public String rejeterPourTauxDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id,
            @RequestBody BigDecimal tauxDemande) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.rejeterPourTauxDemandeSubvention(id,
                tauxDemande);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Signaler anomalie demande subvention.
     *
     * @param id
     *            the id
     * @param demandeSubvention
     *            the demande subvention
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/anomalies/signaler", method = RequestMethod.POST)
    public String signalerAnomalieDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id,
            @RequestBody DemandeSubventionDetailsDto demandeSubvention) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.signalerAnomalieDemandeSubvention(id);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Detecter anomalie demande subvention.
     *
     * @param id
     *            the id
     * @param demandeSubvention
     *            the demande subvention
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE,
            RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/anomalies/detecter", method = RequestMethod.POST)
    public String detecterAnomalieDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long id,
            @RequestBody DemandeSubventionDetailsDto demandeSubvention) {
        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication
                .detecterAnomalieDemandeSubvention(demandeSubvention);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Attribuer nouvelle demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeSubvention}/attribuer", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String attribuerNouvelleDemandeSubvention(@PathVariable(value = "idDemandeSubvention", required = true) Long idDemandeSubvention,
            HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // Initialisation
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        List<FichierTransfert> fichiers = new ArrayList<>();

        // Mapping des fichiers, uniquement des "DOC_DEMANDE_SUBVENTION"
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.getListeTypeDocumentsPourFamille(FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION)) {
            MultipartFile fichierUploade = servletRequest.getFile(typeDocument.getCode());
            if (fichierUploade != null) {
                FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichierUploade);
                fichiers.add(fichierTransfert);
            }
        }

        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.attribuerDemandeSubvention(idDemandeSubvention,
                fichiers);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Corriger demande subvention.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/corriger", method = RequestMethod.POST)
    public String corrigerDemandeSubvention(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // Initialisation
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // Récupération de tous les propriétés de l'objet Demande Subvention Details DTO depuis la Http Request.
        DemandeSubventionDetailsDto demandeSubvention = genererDtoViaRequest(servletRequest);

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDemandeSubvention(this.getUtilisateurConnecte(request), demandeSubvention.getId());

        DemandeSubventionDetailsDto retourDemandeSubvention = this.gestionDemandeSubventionApplication.corrigerDemandeSubvention(demandeSubvention);

        return JsonMapper.getJsonFromData(retourDemandeSubvention);
    }

    /**
     * Recuperer dotation disponible.
     *
     * @param request
     *            the request
     * @param idSousProgramme
     *            the id sous programme
     * @param idCollectivite
     *            the id collectivite
     * @return the big decimal
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/sous-programme/{id:[0-9]+}/collectivite/{idCollectivite:[0-9]+}/rechercher", method = RequestMethod.GET)
    public BigDecimal recupererDotationDisponible(HttpServletRequest request, @PathVariable(value = "id", required = true) Integer idSousProgramme,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        return this.gestionDemandeSubventionApplication.recupererDotationDisponible(idCollectivite, idSousProgramme);
    }

}

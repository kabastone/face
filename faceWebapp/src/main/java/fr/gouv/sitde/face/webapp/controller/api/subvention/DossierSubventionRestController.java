/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.subvention;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dto.DemandePaiementSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeProlongationSimpleDto;
import fr.gouv.sitde.face.application.dto.DemandeSubventionSimpleDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionDetailDto;
import fr.gouv.sitde.face.application.dto.DossierSubventionTableMferDto;
import fr.gouv.sitde.face.application.dto.ResultatRechercheDossierDto;
import fr.gouv.sitde.face.application.subvention.GestionDossierSubventionApplication;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.controller.connexion.UtilisateurConnecteDto;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * DossierSubvention.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class DossierSubventionRestController extends ConnexionAwareController {

    /** The gestion dossier subvention application. */
    @Inject
    private GestionDossierSubventionApplication gestionDossierSubventionApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Consultation d'un dossier subvention par ID : avec toutes ses informations, <br/>
     * pour consultation du dossier avec calculs des montants nécessaires.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return réponse json du dto de dossier subvention
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/dossier/{idDossierSubvention:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherDossierSubventionParId(HttpServletRequest request,
            @PathVariable(value = "idDossierSubvention", required = true) Long idDossierSubvention) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter le dossier de subvention
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossierSubvention);

        DossierSubventionDetailDto dossierSubventionDetailDto = this.gestionDossierSubventionApplication
                .rechercherDossierSubventionParId(idDossierSubvention);
        return JsonMapper.getJsonFromData(dossierSubventionDetailDto);
    }

    /**
     * Ecran consultation dossier subvention : listing des demandes de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/dossier/{idDossierSubvention:[0-9]+}/demandes-subvention/rechercher", method = RequestMethod.GET)
    public String rechercherDemandesSubvention(HttpServletRequest request,
            @PathVariable(value = "idDossierSubvention", required = true) Long idDossierSubvention) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter le dossier de subvention
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossierSubvention);

        List<DemandeSubventionSimpleDto> listeDemandeSubvention = this.gestionDossierSubventionApplication
                .rechercherDemandesSubvention(idDossierSubvention);

        return JsonMapper.getJsonFromData(listeDemandeSubvention);
    }

    /**
     * Ecran consultation dossier subvention : listing des demandes de paiement.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/dossier/{idDossierSubvention:[0-9]+}/demandes-paiement/rechercher", method = RequestMethod.GET)
    public String rechercherDemandesPaiement(HttpServletRequest request,
            @PathVariable(value = "idDossierSubvention", required = true) Long idDossierSubvention) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter le dossier de subvention
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossierSubvention);

        List<DemandePaiementSimpleDto> listeDemandePaiement = this.gestionDossierSubventionApplication
                .rechercherDemandesPaiement(idDossierSubvention);

        return JsonMapper.getJsonFromData(listeDemandePaiement);
    }

    /**
     * Ecran consultation dossier subvention : listing des demandes de prolongation.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/dossier/{idDossierSubvention:[0-9]+}/demandes-prolongation/rechercher", method = RequestMethod.GET)
    public String rechercherDemandesProlongation(HttpServletRequest request,
            @PathVariable(value = "idDossierSubvention", required = true) Long idDossierSubvention) {

        // on valide que l'utilisateur connecté possède les droits : pour consulter le dossier de subvention
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossierSubvention);

        List<DemandeProlongationSimpleDto> listeDemandeProlongation = this.gestionDossierSubventionApplication
                .rechercherDemandesProlongation(idDossierSubvention);

        return JsonMapper.getJsonFromData(listeDemandeProlongation);
    }

    /**
     * Recherche paginée de dossiers subvention par criteres de recherche.
     *
     * @param request
     *            the request
     * @param criteresRecherche
     *            the criteres recherche
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/dossier/criteres/rechercher", method = RequestMethod.POST)
    public String rechercherDossiersSubventionParCriteres(@RequestBody CritereRechercheDossierPourDemandeSubventionQo criteresRecherche,
            HttpServletRequest request) {

        // On valide que l'utilisateur connecté possède les droits agent sur la collectivité des criteres de recherche.
        // La validation échouera si les criteres ne possedent pas d'ID collectivité.
        UtilisateurConnecteDto utilisateur = this.getUtilisateurConnecte(request);
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(utilisateur, criteresRecherche.getIdCollectivite());

        // Recherche en base
        PageReponse<ResultatRechercheDossierDto> pageReponseResultat = this.gestionDossierSubventionApplication
                .rechercherDossiersParCriteres(criteresRecherche);

        return JsonMapper.getJsonFromData(pageReponseResultat);
    }

    /**
     * Recherche paginée de dossiers subvention par criteres de recherche pour demande de paiement.
     *
     * @param request
     *            the request
     * @param criteresRecherche
     *            the criteres recherche
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/dossier/criteres/rechercher", method = RequestMethod.POST)
    public String rechercherDossiersPaiementParCriteres(@RequestBody CritereRechercheDossierPourDemandePaiementQo criteresRecherche,
            HttpServletRequest request) {

        // On valide que l'utilisateur connecté possède les droits agent sur la collectivité des criteres de recherche.
        // La validation échouera si les criteres ne possedent pas d'ID collectivité.
        UtilisateurConnecteDto utilisateur = this.getUtilisateurConnecte(request);
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(utilisateur, criteresRecherche.getIdCollectivite());

        // Recherche en base
        PageReponse<ResultatRechercheDossierDto> pageReponseResultat = this.gestionDossierSubventionApplication
                .rechercherDossiersParCriteresPaiement(criteresRecherche);

        return JsonMapper.getJsonFromData(pageReponseResultat);
    }

    /**
     * recupère les cadencements qui sont pas à jour.
     *
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/subvention/dossier/defaut/{codeProgramme:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherDossierSubventionEnDefaut(HttpServletRequest request, 
    		@PathVariable(value = "codeProgramme") String codeProgramme) {
    	
        List<DossierSubventionTableMferDto> results = this.gestionDossierSubventionApplication.rechercherDossierSubventionCadencementEnDefaut(codeProgramme);
        return JsonMapper.getJsonFromData(results);
    }
    
    /**
    * recupère les travaux qui sont pas à jour.
    *
    * @return the string
    */
   @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
   @RequestMapping(value = "/subvention/dossier/retard/{codeProgramme:[0-9]+}/rechercher", method = RequestMethod.GET)
   public String rechercherDossierSubventionEnRetard(HttpServletRequest request, 
   		@PathVariable(value = "codeProgramme") String codeProgramme) {
   	
       List<DossierSubventionTableMferDto> results = this.gestionDossierSubventionApplication.rechercherDossierSubventionTravauxEnRetard(codeProgramme);
       return JsonMapper.getJsonFromData(results);
   }
}

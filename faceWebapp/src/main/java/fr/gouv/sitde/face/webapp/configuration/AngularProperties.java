/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class AngularProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("angular")
@Validated
public class AngularProperties {

    /** The app url. */
    @NotEmpty
    private String appUrl;

    /**
     * Gets the app url.
     *
     * @return the appUrl
     */
    public String getAppUrl() {
        return this.appUrl;
    }

    /**
     * Sets the app url.
     *
     * @param appUrl
     *            the appUrl to set
     */
    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.webapp.security.model;

/**
 * The Enum CustomJwtClaimEnum.
 *
 * @author a453029
 */
public enum CustomJwtClaimEnum {

    /** The authorities. */
    AUTHORITIES("auth"),

    /** The email. */
    EMAIL("email"),

    /** The nom. */
    NOM("nom"),

    /** The prenom. */
    PRENOM("prenom"),

    /** The id cerbere. */
    ID_CERBERE("idCerbere"),

    /**
     * The initial issued at.<br/>
     * Timestamp de la création initiale de l'access token
     */
    INITIAL_ISSUED_AT("initialIssuedAt");

    /** The cle. */
    private final String cle;

    /**
     * Constructeur privé.
     *
     * @param cle
     *            the cle
     */
    CustomJwtClaimEnum(final String cle) {
        this.cle = cle;
    }

    /**
     * Gets the cle.
     *
     * @return the cle
     */
    public String getCle() {
        return this.cle;
    }

}

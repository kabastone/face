/**
 *
 */
package fr.gouv.sitde.face.webapp.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.gouv.sitde.face.webapp.configuration.AngularProperties;

/**
 * AccueilController : indexs de redirection vers l'application Angular.
 *
 * @author Atos
 */
@Controller
public class AccueilController {

    /** The angular properties. */
    @Inject
    private AngularProperties angularProperties;

    /**
     * Index racine : redirection vers l'application Angular.
     *
     * @return the string
     */
    @RequestMapping("/")
    public String accueillirIndexGeneral() {
        return "redirect:" + this.angularProperties.getAppUrl();
    }

    /**
     * Index app : redirection vers l'application Angular.
     *
     * @return the string
     */
    @RequestMapping("/app")
    public String accueillirIndexApp() {
        return "redirect:" + this.angularProperties.getAppUrl();
    }
}

package fr.gouv.sitde.face.webapp.controller.api.commun;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.commun.GestionReferentielApplication;
import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.application.dto.SousProgrammeDto;
import fr.gouv.sitde.face.application.dto.lov.CollectiviteLovDto;
import fr.gouv.sitde.face.application.dto.lov.DepartementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandePaiementLovDto;
import fr.gouv.sitde.face.application.dto.lov.EtatDemandeSubventionLovDto;
import fr.gouv.sitde.face.application.dto.lov.SousProgrammeLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeAnomalieLovDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class ReferentielRestController.
 */
@RestController
@RequestMapping("/api/referentiel")
public class ReferentielRestController extends ConnexionAwareController {

    /** The gestion referentiel application. */
    @Inject
    private GestionReferentielApplication gestionReferentielApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Lister departements.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/departement/lister", method = RequestMethod.GET)
    public String listerDepartements() {

        List<DepartementLovDto> listeDepartements = this.gestionReferentielApplication.rechercherDepartements();
        return JsonMapper.getJsonFromData(listeDepartements);
    }

    /**
     * Rechercher departement par id.
     *
     * @param id
     *            the id
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/departement/{id:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherDepartement(@PathVariable(value = "id", required = true) Integer id) {

        DepartementDto departement = this.gestionReferentielApplication.rechercherDepartement(id);
        return JsonMapper.getJsonFromData(departement);
    }

    /**
     * Lister sous programmes.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/sous-programme/lister", method = RequestMethod.GET)
    public String listerSousProgrammes() {

        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication.rechercherSousProgrammes();
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

    /**
     * Lister sous programmes pour renoncement dotation.
     *
     * @param request
     *            the request
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/sous-programme-travaux/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammesDeTravauxParCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication
                .rechercherSousProgrammesDeTravauxParCollectivite(idCollectivite);
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

    /**
     * Lister sous programmes de projet.
     *
     * @param request
     *            the request
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/sous-programme-projet/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammesDeProjetParCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication
                .rechercherSousProgrammesDeProjetParCollectivite(idCollectivite);
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

    /**
     * Rechercher sous programme par id.
     *
     * @param idSousProgramme
     *            the id sous programme
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/sous-programme/{idSousProgramme:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammeParId(@PathVariable(value = "idSousProgramme", required = true) Integer idSousProgramme) {

        SousProgrammeDto ssp = this.gestionReferentielApplication.rechercherSousProgrammeParId(idSousProgramme);
        return JsonMapper.getJsonFromData(ssp);
    }

    /**
     * Rechercher sous programmes par collectivite.
     *
     * @param request
     *            the request
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/sous-programme/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammesParCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication.rechercherSousProgrammesParCollectivite(idCollectivite);
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

    /**
     * Lister collectivites.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/sous-programme-projet/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammesDeProjet() {

        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication.rechercherSousProgrammesDeProjet();
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

    /**
     * Lister collectivites.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/lister", method = RequestMethod.GET)
    public String listerCollectivites() {

        List<CollectiviteLovDto> listeCollectivites = this.gestionReferentielApplication.rechercherCollectivites();
        return JsonMapper.getJsonFromData(listeCollectivites);
    }

    /**
     * Lister collectivites.
     *
     * @param codeDepartement
     *            the code departement
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/departement/{codeDepartement}/lister", method = RequestMethod.GET)
    public String listerCollectivitesParDepartement(@PathVariable(value = "codeDepartement", required = true) String codeDepartement) {

        List<CollectiviteLovDto> listeCollectivites = this.gestionReferentielApplication.rechercherCollectivitesParDepartement(codeDepartement);
        return JsonMapper.getJsonFromData(listeCollectivites);
    }

    /**
     * Lister etats dossiers.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/etat-demande-subvention/lister", method = RequestMethod.GET)
    public String listerEtatsDossiersSubvention() {

        List<EtatDemandeSubventionLovDto> listeEtatDossiers = this.gestionReferentielApplication.rechercherEtatsDemandeSubvention();
        return JsonMapper.getJsonFromData(listeEtatDossiers);
    }

    /**
     * Lister etats dossiers.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/etat-demande-paiement/lister", method = RequestMethod.GET)
    public String listerEtatsDossiersPaiement() {

        List<EtatDemandePaiementLovDto> listeEtatDossiers = this.gestionReferentielApplication.rechercherEtatsDemandePaiement();
        return JsonMapper.getJsonFromData(listeEtatDossiers);
    }

    /**
     * Lister les types de demande de paiement.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/type-demande-paiement/lister", method = RequestMethod.GET)
    public String listerTypeDemandePaiement() {

        List<TypeDemandePaiementLovDto> listeTypeDemandePaiement = this.gestionReferentielApplication.rechercherTypesDemandePaiement();
        return JsonMapper.getJsonFromData(listeTypeDemandePaiement);
    }

    /**
     * Lister les types d'anomalie : pour une demande de paiement.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/type-anomalie/paiement/lister", method = RequestMethod.GET)
    public String listerTypesAnomaliePourDemandePaiement() {
        List<TypeAnomalieLovDto> listeTypesAnomalie = this.gestionReferentielApplication.rechercherTypesAnomaliePourDemandePaiement();
        return JsonMapper.getJsonFromData(listeTypesAnomalie);
    }

    /**
     * Lister les types d'anomalie : pour une demande de subvention.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/type-anomalie/subvention/lister", method = RequestMethod.GET)
    public String listerTypesAnomaliePourDemandeSubvention() {
        List<TypeAnomalieLovDto> listeTypesAnomalie = this.gestionReferentielApplication.rechercherTypesAnomaliePourDemandeSubvention();
        return JsonMapper.getJsonFromData(listeTypesAnomalie);
    }

    /**
     * Lister sous programmes pour creation de ligne dotation departementale.
     *
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/dotation/departement/lignes/sous-programme/rechercher", method = RequestMethod.GET)
    public String rechercherSousProgrammesPourLigneDotationDepartementale() {
        List<SousProgrammeLovDto> listeSousProgrammes = this.gestionReferentielApplication.rechercherSousProgrammesPourLigneDotationDepartementale();
        return JsonMapper.getJsonFromData(listeSousProgrammes);
    }

}

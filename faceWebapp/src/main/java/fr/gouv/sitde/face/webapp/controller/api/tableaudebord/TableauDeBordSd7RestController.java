/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.tableaudebord;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordSd7Application;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class TableauDeBordSd7RestController.
 *
 * @author a453029
 */
@RestController
@RequestMapping("/api")
public class TableauDeBordSd7RestController extends ConnexionAwareController {

    /** The tableau de bord sd7 application. */
    @Inject
    private TableauDeBordSd7Application tableauDeBordSd7Application;

    /**
     * Rechercher tableau de bord sd 7.
     *
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/sd7/rechercher", method = RequestMethod.GET)
    public String rechercherTableauDeBordSd7() {

        TableauDeBordSd7Dto tdbSd7Dto = this.tableauDeBordSd7Application.rechercherTableauDeBordSd7();
        return JsonMapper.getJsonFromData(tdbSd7Dto);
    }

    /**
     * Rechercher tdb sd 7 acontroler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/sd7/subacontroler/rechercher", method = RequestMethod.POST)
    public String rechercherTdbSd7AcontrolerSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordSd7Application
                .rechercherDemandesSubventionTdbSd7AcontrolerSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb sd 7 acontroler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/sd7/paiacontroler/rechercher", method = RequestMethod.POST)
    public String rechercherTdbSd7AcontrolerPaiement(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordSd7Application
                .rechercherDemandesPaiementTdbSd7AcontrolerPaiement(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb sd 7 atransferer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/sd7/subatransferer/rechercher", method = RequestMethod.POST)
    public String rechercherTdbSd7AtransfererSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordSd7Application
                .rechercherDemandesSubventionTdbSd7AtransfererSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb sd 7 atransferer paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/sd7/paiatransferer/rechercher", method = RequestMethod.POST)
    public String rechercherTdbSd7AtransfererPaiement(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordSd7Application
                .rechercherDemandesPaiementTdbSd7AtransfererPaiement(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

}

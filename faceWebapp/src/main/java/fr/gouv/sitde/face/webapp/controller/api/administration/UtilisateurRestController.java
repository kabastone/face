/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * UtilisateurRestController.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class UtilisateurRestController extends ConnexionAwareController {

    /** The gestion utilisateur application. */
    @Inject
    private GestionUtilisateurApplication gestionUtilisateurApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher utilisateur connecte.
     *
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/utilisateur-connecte/rechercher", method = RequestMethod.GET)
    public String rechercherUtilisateurConnecte(HttpServletRequest request) {
        return JsonMapper.getJsonFromData(this.getUtilisateurConnecte(request));
    }

    /**
     * Rechercher utilisateur par id.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @return the string json de l'utilisateur
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/utilisateur/id/{idUtilisateur:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherUtilisateurParId(HttpServletRequest request, @PathVariable(value = "idUtilisateur", required = true) Long idUtilisateur) {

        // On valide que l'idUtilisateur correspond à celle de l'utilisateur connecté.
        this.validerEstUtilisateurConnecte(request, idUtilisateur);

        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParId(idUtilisateur);
        return JsonMapper.getJsonFromData(userDto);
    }

    /**
     * Rechercher utilisateur par email.
     *
     * @param emailUtilisateur
     *            the emailUtilisateur
     * @return the string json de l'utilisateur
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/utilisateur/email/{emailUtilisateur}/rechercher", method = RequestMethod.GET)
    public String rechercherUtilisateurParEmail(@PathVariable(value = "emailUtilisateur", required = true) String emailUtilisateur) {
        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.rechercherUtilisateurParEmail(emailUtilisateur);
        return JsonMapper.getJsonFromData(userDto);
    }

    /**
     * Creer utilisateur.
     *
     * @param utilisateurDto
     *            the utilisateur dto
     * @return the string json de l'utilisateur
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/utilisateur/creer", method = { RequestMethod.POST, RequestMethod.PUT })
    public String creerUtilisateurAttenteConnexion(HttpServletRequest request, @RequestBody UtilisateurAdminDto utilisateurDto) {

        if ((utilisateurDto == null) || (utilisateurDto.getDroitsAgentCollectivites().isEmpty())) {
            throw new TechniqueException("La requete ne contient pas un utilisateurDto avec un droitagent sur une collectivité");
        }

        // On vérifie que l'utilisateur connecté est bien admin de la collectivité pour laquelle il vaut créer l'utilisateur agent.
        DroitAgentCollectiviteDto droitAgentDto = utilisateurDto.getDroitsAgentCollectivites().get(0);

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), droitAgentDto.getIdCollectivite());

        // Création de l'utilisateur
        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.creerUtilisateurEnAttenteConnexion(utilisateurDto);
        return JsonMapper.getJsonFromData(userDto);
    }

    /**
     * Modifier utilisateur.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @param utilisateurDto
     *            the utilisateur dto
     * @return the string json de l'utilisateur
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/utilisateur/{idUtilisateur:[0-9]+}/modifier", method = { RequestMethod.POST, RequestMethod.PUT })
    public String modifierUtilisateur(HttpServletRequest request, @PathVariable("idUtilisateur") long idUtilisateur,
            @RequestBody UtilisateurAdminDto utilisateurDto) {

        // On valide que l'idUtilisateur correspond à celle de l'utilisateur connecté.
        this.validerEstUtilisateurConnecte(request, idUtilisateur);

        UtilisateurAdminDto userDto = this.gestionUtilisateurApplication.modifierUtilisateur(utilisateurDto);
        return JsonMapper.getJsonFromData(userDto);
    }

    /**
     * Supprimer un utilisateur.
     *
     * @param idUtilisateur
     *            the id utilisateur
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/utilisateur/{idUtilisateur:[0-9]+}/supprimer", method = { RequestMethod.DELETE })
    public String supprimerUtilisateur(@PathVariable("idUtilisateur") long idUtilisateur) {
        // Suppression de l'utilisateur
        this.gestionUtilisateurApplication.supprimerUtilisateur(idUtilisateur);

        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }

}

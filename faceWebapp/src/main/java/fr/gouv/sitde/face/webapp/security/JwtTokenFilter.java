package fr.gouv.sitde.face.webapp.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import fr.gouv.sitde.face.webapp.security.exception.AuthenticationException;
import io.jsonwebtoken.ExpiredJwtException;

/**
 * The Class JwtTokenFilter.
 *
 * @author a453029
 */
public class JwtTokenFilter extends OncePerRequestFilter {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenFilter.class);

    /** The jwt token provider. */
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * Instantiates a new jwt token filter.
     *
     * @param jwtTokenProvider
     *            the jwt token provider
     */
    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    /**
     * Do filter internal.
     *
     * @param httpServletRequest
     *            the http servlet request
     * @param httpServletResponse
     *            the http servlet response
     * @param filterChain
     *            the filter chain
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws ServletException, IOException {

        String token = this.jwtTokenProvider.resolveToken(httpServletRequest);

        try {
            if ((token != null) && this.jwtTokenProvider.validateToken(token)) {
                // Le token existe et est valide.
                // On place les caractéristiques d'authentification de l'utilisateur dans le contexte de Spring Security.
                Authentication auth = this.jwtTokenProvider.getAuthentication(token);
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        } catch (ExpiredJwtException expiredEx) {
            LOGGER.debug("Le token a expiré. {}", expiredEx.getMessage());
            try {
                // Le token a expiré, tentative de création d'un refresh token.
                String refreshToken = this.jwtTokenProvider.createRefreshToken(expiredEx.getClaims());

                // Le refresh token a été créé avec succès
                // On place les caractéristiques d'authentification de l'utilisateur dans le contexte de Spring Security.
                Authentication auth = this.jwtTokenProvider.getAuthentication(refreshToken);
                SecurityContextHolder.getContext().setAuthentication(auth);

                // On met le refresh token dans un cookie qu'on place dans la réponse.
                this.jwtTokenProvider.stockerTokenDansCookieTemporaire(refreshToken, httpServletResponse, httpServletRequest);

            } catch (AuthenticationException authEx) {
                // token expiré (durée totale cumulée), impossible de le rafraichir.
                LOGGER.debug("Le token est complètement expiré, impossible de rafraichir. {}", authEx.getMessage());
                SecurityContextHolder.clearContext();
                httpServletResponse.sendError(HttpStatus.UNAUTHORIZED.value(), Encode.forHtml(authEx.getMessage()));
                return;
            }
        } catch (AuthenticationException authEx) {
            LOGGER.info("Le token est invalide", authEx);
            // Le token est invalide.
            SecurityContextHolder.clearContext();
            httpServletResponse.sendError(HttpStatus.UNAUTHORIZED.value(), Encode.forHtml(authEx.getMessage()));
            return;
        }

        // Soit la requête ne contient aucun token, soit elle contient un token valide.
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}

package fr.gouv.sitde.face.webapp.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;
import io.jsonwebtoken.Claims;

/**
 *
 *
 * @author a453029
 */
public interface JwtTokenProvider {

    /**
     * Resolve token.
     *
     * @param request
     *            the request
     * @return the string
     */
    String resolveToken(HttpServletRequest request);

    /**
     * Valide le token. <br/>
     * Renvoie une ExpiredJwtException si le jeton est expiré et une AuthenticationException si le jeton est invalide.
     *
     * @param token
     *            the token
     * @return true, if successful
     */
    boolean validateToken(String token);

    /**
     * Creates the access token.
     *
     * @param httpServletRequest
     *            the http servlet request
     * @return le token créé
     */
    String createAccessToken(HttpServletRequest httpServletRequest);

    /**
     * Récupère les caractéristiques d'authentification de l'utilisateur à partir du token.
     *
     * @param token
     *            the token
     * @return the authentication
     */
    Authentication getAuthentication(String token);

    /**
     * Creates the refresh token.<br/>
     * Renvoie une AuthenticationException si la durée de vie totale des tokens est dépassée.
     *
     * @param claims
     *            the claims
     * @return le nouveau refresh token créé
     */
    String createRefreshToken(Claims claims);

    /**
     * Stocke le token dans un cookie temporaire qui est ajouté à la réponse passée en paramètre.
     *
     * @param token
     *            the token
     * @param response
     *            the response
     */
    void stockerTokenDansCookieTemporaire(String token, HttpServletResponse response, HttpServletRequest request);

    /**
     * Supprime le cookie temporaire qui contient le token (s'il existe).
     *
     * @param response
     *            the response
     */
    void supprimerCookieTemporaireToken(HttpServletResponse response);

    /**
     * Gets the utilisateur cerbere from token.
     *
     * @param token
     *            the token
     * @return the utilisateur cerbere from token
     */
    UtilisateurToken getUtilisateurTokenFromToken(String token);

    /**
     * Gets the utilisateur token from request.
     * <p>
     * <b>IMPORTANT</b> : Les informations de l'utilisateur seront ramenées de la requete, même si le token est expiré.
     * </p>
     *
     * @param request
     *            the request
     * @return the utilisateur token from request
     */
    UtilisateurToken getUtilisateurTokenFromRequest(HttpServletRequest request);
}

package fr.gouv.sitde.face.webapp.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("fr.gouv.sitde.face.persistance")
@EnableAspectJAutoProxy
@EntityScan("fr.gouv.sitde.face.transverse.entities")
@ComponentScan(basePackages = "fr.gouv.sitde.face")
@Profile({ "prod", "dev", "devang" })
public class FaceInitializerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FaceInitializerApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(FaceInitializerApplication.class, args);
    }
}

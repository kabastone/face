/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.dotation;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dotation.GestionDotationNationaleApplication;
import fr.gouv.sitde.face.application.dto.DotationProgrammeDto;
import fr.gouv.sitde.face.application.dto.DotationRepartitionNationaleDto;
import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class DotationNationaleRestController.
 *
 * @author 453029
 */
@RestController
@RequestMapping("/api")
public class DotationNationaleRestController {

    /** The gestion dotation programme application. */
    @Inject
    private GestionDotationNationaleApplication gestionDotationNationaleApplication;

    /**
     * Rechercher dotation repartition nationale.
     *
     * @param codeProgramme
     *            the code programme
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/dotation/repartition-nationale/code/{codeProgramme:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherDotationRepartitionNationale(@PathVariable(value = "codeProgramme", required = true) String codeProgramme) {
        DotationRepartitionNationaleDto dotationNationaleDto = this.gestionDotationNationaleApplication
                .rechercherDonneesDotationRepartitionNationale(codeProgramme);
        return JsonMapper.getJsonFromData(dotationNationaleDto);
    }

    /**
     * Modifier montant dotation programme repartition nationale.
     *
     * @param dotationProgrammeDto
     *            the dotation programme dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/dotation/repartition-nationale/dotation-programme/montant/modifier", method = RequestMethod.POST)
    public String modifierMontantDotationProgrammeRepartitionNationale(@RequestBody DotationProgrammeDto dotationProgrammeDto) {
        DotationProgrammeDto dotationProgramme = this.gestionDotationNationaleApplication.enregistrerMontantDotationProgramme(dotationProgrammeDto);
        return JsonMapper.getJsonFromData(dotationProgramme);
    }

    /**
     * Modifier montant dotation sous programme repartition nationale.
     *
     * @param ligneDotationsSousProgrammeDto
     *            the ligne dotations sous programme dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/dotation/repartition-nationale/dotation-sous-programme/montant/modifier", method = RequestMethod.POST)
    public String modifierMontantDotationSousProgrammeRepartitionNationale(
            @RequestBody DotationSousProgrammeMontantBo dotationSousProgrammeMontantBo) {
        DotationRepartitionNationaleDto dotationNationaleDto = this.gestionDotationNationaleApplication
                .enregistrerMontantDotationSousProgramme(dotationSousProgrammeMontantBo);
        return JsonMapper.getJsonFromData(dotationNationaleDto);
    }

}

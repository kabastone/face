package fr.gouv.sitde.face.webapp.exception;

/**
 * Data not found Exception : aucune donnée n'a été trouvée (étend runtime exception).
 *
 * @author Atos
 *
 */
public class DataNotFoundException extends RuntimeException {

    /**
     * Attribut de version.
     */
    private static final long serialVersionUID = 1L;

    public DataNotFoundException() {
        super("Aucune donnée n'a été trouvée");
    }

    /**
     * Constructeur.
     *
     * @param message
     *            le message d'erreur
     */
    public DataNotFoundException(final String message) {
        super(message);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.webapp.interceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;

/**
 * Intercepteur qui renseigne la variable de log MDC liée à l'utilisateur connecté.
 *
 * @author Atos
 *
 */

@Component
public class LoggingContextInterceptor extends HandlerInterceptorAdapter {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingContextInterceptor.class);

    /** The jwt token provider. */
    @Inject
    private JwtTokenProvider jwtTokenProvider;

    /**
     * Clé pour l'utilisateur dans la map du MDC.
     */
    private static final String MDC_CLE_UTILISATEUR = "utilisateur";

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        UtilisateurToken userToken = this.jwtTokenProvider.getUtilisateurTokenFromRequest(request);

        if (userToken.isAuthentifie()) {
            // On met le nom de l'utilisateur dans le MDC
            String chaineUtilisateur = userToken.getPrenom() + " " + userToken.getNom();
            MDC.put(MDC_CLE_UTILISATEUR, chaineUtilisateur);
        }

        if (LOGGER.isDebugEnabled() && !StringUtils.startsWith(handler.toString(), "ResourceHttpRequestHandler")) {
            LOGGER.debug("Entrée dans le handler {}", handler);
        }

        return true;
    }
}

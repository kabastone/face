/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionDepartementApplication;
import fr.gouv.sitde.face.application.dto.AdresseDto;
import fr.gouv.sitde.face.application.dto.DepartementDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

/**
 * The Class DepartementRestController.
 */
@RestController
@RequestMapping("/api")
public class DepartementRestController extends ConnexionAwareController {

    /** The gestion departement application. */
    @Inject
    private GestionDepartementApplication gestionDepartementApplication;

    /**
     * Rechercher adresse pour departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the adresse dto
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/administration/departement/{idDepartement:[0-9]+}/adresse/recherche", method = RequestMethod.GET)
    public AdresseDto rechercherAdressePourDepartement(@PathVariable(value = "idDepartement", required = true) Integer idDepartement) {
        return this.gestionDepartementApplication.rechercherAdressePourDepartement(idDepartement);
    }

    /**
     * Enregistrer adresse pour departement.
     *
     * @param idDepartement
     *            the id departement
     * @param adresseDto
     *            the adresse dto
     * @return the adresse dto
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/administration/departement/{idDepartement:[0-9]+}/adresse/enregistrer", method = RequestMethod.POST)
    public AdresseDto enregistrerAdressePourDepartement(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            @RequestBody AdresseDto adresseDto) {
        return this.gestionDepartementApplication.enregistrerAdressepourDepartement(idDepartement, adresseDto);
    }

    /**
     * Enregistrer donnees fiscales departement.
     *
     * @param idDepartement
     *            the id departement
     * @param departement
     *            the departement
     * @return the departement dto
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/administration/departement/{idDepartement:[0-9]+}/donnees-fiscales/enregistrer", method = RequestMethod.POST)
    public DepartementDto enregistrerDonneesFiscalesDepartement(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            @RequestBody DepartementDto departement) {
        return this.gestionDepartementApplication.enregistrerDonneesFiscalesDepartement(idDepartement, departement);
    }
}

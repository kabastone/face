package fr.gouv.sitde.face.webapp.controller.api.dotation;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dotation.GestionDotationAnnuelleApplication;
import fr.gouv.sitde.face.application.dto.DotationAnnuelleDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

@RestController
@RequestMapping("/api/dotation/")
public class DotationAnnuelleRestController extends ConnexionAwareController {

    /** The gestion dotation annuelle application. */
    @Inject
    private GestionDotationAnnuelleApplication gestionDotationAnnuelleApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/annuelle/rechercher", method = RequestMethod.POST)
    public String rechercherDotationAnnuelle(HttpServletRequest request, @RequestBody DotationAnnuelleDto dotationAnnuelleDto) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), dotationAnnuelleDto.getIdCollectivite());

        DotationAnnuelleDto dotationAnnuelleDtoRecupere = this.gestionDotationAnnuelleApplication.rechercherDotationAnnuelle(dotationAnnuelleDto);

        return JsonMapper.getJsonFromData(dotationAnnuelleDtoRecupere);
    }
}

package fr.gouv.sitde.face.webapp.controller.api.dotation;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonMappingException;

import fr.gouv.sitde.face.application.dotation.GestionDotationCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CollectiviteRepartitionDto;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteRepartitionDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class DotationCollectiviteRestController.
 */
@RestController
@RequestMapping("/api/dotation/")
public class DotationCollectiviteRestController extends ConnexionAwareController {

    /** The gestion dotation collectivite application. */
    @Inject
    private GestionDotationCollectiviteApplication gestionDotationCollectiviteApplication;

    /**
     * Rechercher dotation collectivite repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/repartition/rechercher", method = RequestMethod.POST)
    public String rechercherDotationCollectiviteRepartition(@RequestBody String codeDepartement) {
        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto = this.gestionDotationCollectiviteApplication
                .rechercherDotationCollectiviteRepartition(codeDepartement);

        return JsonMapper.getJsonFromData(dotationCollectiviteRepartitionDto);
    }

    /**
     * Televerser document courrier repartition.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/repartition/document/televerser", method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String televerserDocumentCourrierRepartition(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // mapping du fichier en pièce jointe 'COURRIER_REPARTITION' uniquement
        MultipartFile fichierUpload = servletRequest.getFile(TypeDocumentEnum.COURRIER_REPARTITION.getCode());
        FichierTransfert fichierTransfert = new FichierTransfert();
        if (fichierUpload != null) {
            fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichierUpload);
        }

        // mapping code département
        String codeDepartement = "";
        if (StringUtils.isNotBlank(servletRequest.getParameter("codeDepartement"))) {
            codeDepartement = (String.valueOf(servletRequest.getParameter("codeDepartement")));
        }

        // téléversement du fichier
        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto = this.gestionDotationCollectiviteApplication
                .televerserDocumentCourrierRepartition(codeDepartement, fichierTransfert);

        return JsonMapper.getJsonFromData(dotationCollectiviteRepartitionDto);
    }

    /**
     * Repartir dotation collectivite.
     *
     * @param collectiviteRepartitionDto
     *            the collectivite repartition dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/repartition/repartir", method = RequestMethod.POST)
    public String repartirDotationCollectivite(@RequestBody CollectiviteRepartitionDto collectiviteRepartitionDto) {

        DotationCollectiviteRepartitionDto dotationCollectiviteRepartitionDto = this.gestionDotationCollectiviteApplication
                .repartirDotationCollectivite(collectiviteRepartitionDto);

        return JsonMapper.getJsonFromData(dotationCollectiviteRepartitionDto);
    }
}

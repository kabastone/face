/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.subvention;

import java.io.Serializable;

import fr.gouv.sitde.face.transverse.pagination.PageDemande;

/**
 * The Class PageDemandeWithCollectiviteDto.
 *
 * @author a453029
 */
public class PageDemandeWithCollectiviteDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1641284538158608792L;

    /** The page demande. */
    private PageDemande pageDemande;

    /** The id collectivite. */
    private Long idCollectivite;

    /**
     * Gets the page demande.
     *
     * @return the pageDemande
     */
    public PageDemande getPageDemande() {
        return this.pageDemande;
    }

    /**
     * Sets the page demande.
     *
     * @param pageDemande
     *            the pageDemande to set
     */
    public void setPageDemande(PageDemande pageDemande) {
        this.pageDemande = pageDemande;
    }

    /**
     * Gets the id collectivite.
     *
     * @return the idCollectivite
     */
    public Long getIdCollectivite() {
        return this.idCollectivite;
    }

    /**
     * Sets the id collectivite.
     *
     * @param idCollectivite
     *            the idCollectivite to set
     */
    public void setIdCollectivite(Long idCollectivite) {
        this.idCollectivite = idCollectivite;
    }

}

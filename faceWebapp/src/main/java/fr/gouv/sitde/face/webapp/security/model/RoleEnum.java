package fr.gouv.sitde.face.webapp.security.model;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The Enum Role.
 */
public enum RoleEnum implements GrantedAuthority {

    /** The mfer responsable. */
    MFER_RESPONSABLE(Constantes.MFER_RESPONSABLE, Constantes.ROLE_MFER_RESPONSABLE),

    /** The mfer instructeur. */
    MFER_INSTRUCTEUR(Constantes.MFER_INSTRUCTEUR, Constantes.ROLE_MFER_INSTRUCTEUR),

    /** The sd7 responsable. */
    SD7_RESPONSABLE(Constantes.SD7_RESPONSABLE, Constantes.ROLE_SD7_RESPONSABLE),

    /** The sd7 instructeur. */
    SD7_INSTRUCTEUR(Constantes.SD7_INSTRUCTEUR, Constantes.ROLE_SD7_INSTRUCTEUR),

    /** The generique. */
    GENERIQUE(Constantes.GENERIQUE, Constantes.ROLE_GENERIQUE);

    /** The authority. */
    private final String libelleCourt;

    /** The authority. */
    private final String authority;

    /**
     * The Class Constantes.
     */
    public static final class Constantes {
        private static final String PREFIXE_ROLE = "ROLE_";
        private static final String MFER_RESPONSABLE = "MFER_RESPONSABLE";
        private static final String MFER_INSTRUCTEUR = "MFER_INSTRUCTEUR";
        private static final String SD7_RESPONSABLE = "SD7_RESPONSABLE";
        private static final String SD7_INSTRUCTEUR = "SD7_INSTRUCTEUR";
        private static final String GENERIQUE = "GENERIQUE";
        public static final String ROLE_MFER_RESPONSABLE = PREFIXE_ROLE + MFER_RESPONSABLE;
        public static final String ROLE_MFER_INSTRUCTEUR = PREFIXE_ROLE + MFER_INSTRUCTEUR;
        public static final String ROLE_SD7_RESPONSABLE = PREFIXE_ROLE + SD7_RESPONSABLE;
        public static final String ROLE_SD7_INSTRUCTEUR = PREFIXE_ROLE + SD7_INSTRUCTEUR;
        public static final String ROLE_GENERIQUE = PREFIXE_ROLE + GENERIQUE;

        /**
         * Constructeur privé.
         */
        private Constantes() {
            // Constructeur privé, la classe ne doit pas être instanciée.
        }
    }

    /**
     * Constructeur privé.
     *
     * @param authority
     *            authority du role
     */
    RoleEnum(final String libelleCourt, final String authority) {
        this.libelleCourt = libelleCourt;
        this.authority = authority;
    }

    /**
     * Gets the libelle court.
     *
     * @return the libelle court
     */
    @JsonValue
    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.GrantedAuthority#getAuthority()
     */
    @Override
    public String getAuthority() {
        return this.authority;
    }

    /**
     * rechercher le role par libelle court.
     *
     * @param libelleCourt
     *            the libelleCourt
     * @return the role par libelleCourt
     */
    public static RoleEnum rechercherRoleParLibelleCourt(final String libelleCourt) {
        if ((libelleCourt == null) || libelleCourt.isEmpty()) {
            return null;
        }

        for (RoleEnum roleEnum : RoleEnum.values()) {
            if (libelleCourt.equals(roleEnum.getLibelleCourt())) {
                return roleEnum;
            }
        }

        return null;
    }

    /**
     * Renvoie true si le libelle court n'est pas null et que ce libellé existe pour un role.
     *
     * @param libelleCourt
     *            l'authority testée
     * @return true ou false
     */
    public static boolean roleExists(final String libelleCourt) {
        if ((libelleCourt == null) || libelleCourt.isEmpty()) {
            return false;
        }
        for (RoleEnum roleEnum : RoleEnum.values()) {
            if (libelleCourt.equals(roleEnum.getLibelleCourt())) {
                return true;
            }
        }
        return false;
    }
}

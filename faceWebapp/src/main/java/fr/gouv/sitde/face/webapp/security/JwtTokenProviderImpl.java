package fr.gouv.sitde.face.webapp.security;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.webapp.security.configuration.JwtProperties;
import fr.gouv.sitde.face.webapp.security.exception.AuthenticationException;
import fr.gouv.sitde.face.webapp.security.model.CustomJwtClaimEnum;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * The Class JwtTokenProviderImpl.
 *
 * @author a453029
 */
@Component
public class JwtTokenProviderImpl implements JwtTokenProvider {

    /** The Constant TOKEN_REQUEST_HEADER. */
    private static final String TOKEN_REQUEST_HEADER = "Authorization";

    /** The Constant TOKEN_TEMP_COOKIE. */
    private static final String TOKEN_TEMP_COOKIE = "temptoken";

    /** The Constant MAX_AGE_TEMP_COOKIE_IN_SECONDS. */
    private static final int MAX_AGE_TEMP_COOKIE_IN_SECONDS = 300;

    /** The Constant INDEX_BEARER_ENTETE. */
    private static final int INDEX_BEARER_ENTETE = 7;

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProviderImpl.class);

    /** The jwt properties. */
    @Inject
    private JwtProperties jwtProperties;

    /** The base 64 secret key. */
    private String base64SecretKey;

    /** The environnement. */
    @Inject
    private Environment environnement;

    /**
     * Inits the base64SecretKey.
     */
    @PostConstruct
    private void init() {
        this.base64SecretKey = Base64.encodeBase64String(this.jwtProperties.getTokenSecretKey().getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Resolve token.
     *
     * @param request
     *            the request
     * @return the string
     */
    @Override
    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(TOKEN_REQUEST_HEADER);
        if ((bearerToken != null) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(INDEX_BEARER_ENTETE);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#validateToken(java.lang.String)
     */
    @Override
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(this.base64SecretKey).parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException e) {
            throw e;
        } catch (RuntimeException e) {
            throw new AuthenticationException("Le token JWT est invalide", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#createAccessToken(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public String createAccessToken(HttpServletRequest httpServletRequest) {

        UtilisateurToken utilisateurToken = CerbereService.construireUtilisateurToken(httpServletRequest);
        Claims claims = getClaimsFromUtilisateurToken(utilisateurToken);

        // ID du token
        claims.setId(UUID.randomUUID().toString());

        Date now = new Date();
        // Date de fabrication initiale de l'access token
        claims.put(CustomJwtClaimEnum.INITIAL_ISSUED_AT.getCle(), now.getTime());

        // Date de fabrication du jeton
        claims.setIssuedAt(now);

        // Durée de validité
        Date validity = new Date(now.getTime() + this.jwtProperties.getAccessTokenExpireLength());
        claims.setExpiration(validity);

        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, this.base64SecretKey).compact();
    }

    /**
     * Gets the claims from utilisateur token.
     *
     * @param utilisateurToken
     *            the utilisateur token
     * @return the claims from utilisateur token
     */
    private static Claims getClaimsFromUtilisateurToken(UtilisateurToken utilisateurToken) {
        Claims claims = Jwts.claims().setSubject(utilisateurToken.getEmail());
        claims.put(CustomJwtClaimEnum.AUTHORITIES.getCle(), utilisateurToken.getListeRoles().stream()
                .map(s -> new SimpleGrantedAuthority(s.getLibelleCourt())).filter(Objects::nonNull).collect(Collectors.toList()));
        claims.put(CustomJwtClaimEnum.EMAIL.getCle(), utilisateurToken.getEmail());
        claims.put(CustomJwtClaimEnum.NOM.getCle(), utilisateurToken.getNom());
        claims.put(CustomJwtClaimEnum.PRENOM.getCle(), utilisateurToken.getPrenom());
        claims.put(CustomJwtClaimEnum.ID_CERBERE.getCle(), utilisateurToken.getIdCerbere());

        return claims;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#getAuthentication(java.lang.String)
     */
    @Override
    public Authentication getAuthentication(String token) {

        UtilisateurToken utilisateurToken = this.getUtilisateurTokenFromToken(token);
        UserDetails userDetails = new User(utilisateurToken.getEmail(), "", utilisateurToken.getListeRoles());
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#createRefreshToken(io.jsonwebtoken.Claims)
     */
    @Override
    public String createRefreshToken(Claims claims) {

        // On vérifie si le Token a dépassé la durée de vie totale cumulée
        Date now = new Date();
        Long timeStampCreationAccessToken = claims.get(CustomJwtClaimEnum.INITIAL_ISSUED_AT.getCle(), Long.class);
        if (now.getTime() > (timeStampCreationAccessToken + this.jwtProperties.getTotalTokensExpireLength())) {
            throw new AuthenticationException("Impossible de rafraichir le token, durée de vie totale dépassée.");
        }

        Claims refreshClaims = getClonedClaims(claims);

        // ID du token
        refreshClaims.setId(UUID.randomUUID().toString());

        // Date de création du refresh token
        refreshClaims.setIssuedAt(now);

        // Durée de validité du refresh token
        Date validity = new Date(now.getTime() + this.jwtProperties.getRefreshTokenExpireLength());
        refreshClaims.setExpiration(validity);

        // On renvoie le refresh token
        return Jwts.builder().setClaims(refreshClaims).signWith(SignatureAlgorithm.HS256, this.base64SecretKey).compact();
    }

    /**
     * Gets the cloned claims.
     *
     * @param existingClaims
     *            the existing claims
     * @return the cloned claims
     */
    private static Claims getClonedClaims(Claims existingClaims) {
        Claims refreshClaims = Jwts.claims().setSubject(existingClaims.getSubject());
        refreshClaims.put(CustomJwtClaimEnum.AUTHORITIES.getCle(), existingClaims.get(CustomJwtClaimEnum.AUTHORITIES.getCle()));
        refreshClaims.put(CustomJwtClaimEnum.EMAIL.getCle(), existingClaims.get(CustomJwtClaimEnum.EMAIL.getCle()));
        refreshClaims.put(CustomJwtClaimEnum.NOM.getCle(), existingClaims.get(CustomJwtClaimEnum.NOM.getCle()));
        refreshClaims.put(CustomJwtClaimEnum.PRENOM.getCle(), existingClaims.get(CustomJwtClaimEnum.PRENOM.getCle()));
        refreshClaims.put(CustomJwtClaimEnum.ID_CERBERE.getCle(), existingClaims.get(CustomJwtClaimEnum.ID_CERBERE.getCle()));

        // Date de fabrication initiale de l'access token
        refreshClaims.put(CustomJwtClaimEnum.INITIAL_ISSUED_AT.getCle(), existingClaims.get(CustomJwtClaimEnum.INITIAL_ISSUED_AT.getCle()));

        return refreshClaims;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#stockerTokenDansCookieTemporaire(java.lang.String,
     * javax.servlet.http.HttpServletResponse, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void stockerTokenDansCookieTemporaire(String token, HttpServletResponse response, HttpServletRequest request) {

        // On met le token dans un cookie qu'on place en réponse.
        Cookie cookie = new Cookie(TOKEN_TEMP_COOKIE, token);
        // le navigateur ne doit envoyer le cookie que pour une connexion HTTPS
        cookie.setSecure(this.jwtProperties.getHttpsObligatoire());
        // Durée de vie de 5 mn, le cookie doit juste être transmis à l'appli Angular
        cookie.setMaxAge(MAX_AGE_TEMP_COOKIE_IN_SECONDS);

        // Modification pour fonctionnement avec "ng serve"
        if (Arrays.asList(this.environnement.getActiveProfiles()).contains("devang")) {
            cookie.setPath(request.getContextPath());
        }

        response.addCookie(cookie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#supprimerCookieTemporaireToken(javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void supprimerCookieTemporaireToken(HttpServletResponse response) {

        // On écrase un éventuel cookie existant avec un nouveau cookie ayant une durée de vie de zero seconde.
        Cookie cookie = new Cookie(TOKEN_TEMP_COOKIE, "");
        cookie.setMaxAge(0);
        cookie.setSecure(this.jwtProperties.getHttpsObligatoire());
        response.addCookie(cookie);
    }

    /**
     * Gets the utilisateur token from token.
     *
     * @param token
     *            the token
     * @param accepterTokenExpire
     *            the flag accepter token expire
     * @return the utilisateur token from token
     */
    @Override
    public UtilisateurToken getUtilisateurTokenFromToken(String token) {

        Claims claims = Jwts.parser().setSigningKey(this.base64SecretKey).parseClaimsJws(token).getBody();
        return getUtilisateurTokenFromClaims(claims);
    }

    /**
     * Gets the utilisateur token from claims.
     *
     * @param claims
     *            the claims
     * @return the utilisateur token from claims
     */
    private static UtilisateurToken getUtilisateurTokenFromClaims(Claims claims) {

        UtilisateurToken utilisateur = new UtilisateurToken();
        utilisateur.setEmail(claims.get(CustomJwtClaimEnum.EMAIL.getCle(), String.class));
        utilisateur.setNom(claims.get(CustomJwtClaimEnum.NOM.getCle(), String.class));
        utilisateur.setPrenom(claims.get(CustomJwtClaimEnum.PRENOM.getCle(), String.class));
        utilisateur.setIdCerbere(claims.get(CustomJwtClaimEnum.ID_CERBERE.getCle(), String.class));

        // On sette les roles de l'utilisateur
        @SuppressWarnings("unchecked")
        List<Map<String, String>> listeAutorites = (List<Map<String, String>>) claims.get(CustomJwtClaimEnum.AUTHORITIES.getCle());
        List<RoleEnum> listeRoles = new ArrayList<>(listeAutorites.size());

        for (Map<String, String> autorite : listeAutorites) {
            listeRoles.add(RoleEnum.rechercherRoleParLibelleCourt(autorite.get("authority")));
        }
        utilisateur.setListeRoles(listeRoles);

        return utilisateur;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.webapp.security.JwtTokenProvider#getUtilisateurTokenFromRequest(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public UtilisateurToken getUtilisateurTokenFromRequest(HttpServletRequest request) {
        String token = this.resolveToken(request);

        // Sans token dans la requete, on renvoie un utilisateur vierge.
        if (token == null) {
            return new UtilisateurToken();
        }

        Claims claims = null;

        try {
            claims = Jwts.parser().setSigningKey(this.base64SecretKey).parseClaimsJws(token).getBody();

        } catch (ExpiredJwtException e) {
            // Si le token a expiré, on va récupérer les claims du token dans l'exception
            LOGGER.debug("Le token en entete est expiré, on récupère les claims à partir de l'exception.");
            claims = e.getClaims();
        }

        return getUtilisateurTokenFromClaims(claims);
    }

}

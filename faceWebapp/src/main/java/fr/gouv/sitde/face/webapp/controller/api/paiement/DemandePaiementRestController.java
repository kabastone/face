/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.paiement;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonMappingException;

import fr.gouv.sitde.face.application.dto.DemandePaiementDto;
import fr.gouv.sitde.face.application.dto.lov.TypeDemandePaiementLovDto;
import fr.gouv.sitde.face.application.paiement.GestionDemandePaiementApplication;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelDownloadUtils;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * Controller REST des demandes de paiment.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class DemandePaiementRestController extends ConnexionAwareController {

    /** The gestion demande paiement application. */
    @Inject
    private GestionDemandePaiementApplication gestionDemandePaiementApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /** Constante pour l'idDemande. */
    private static final String ID_DEMANDE = "idDemande";

    /**
     * Pour réaliser une nouvelle demande de paiment.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return réponse json du dto de recherche de demande de paiement
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/demande/dossier/{idDossierSubvention:[0-9]+}/initialiser", method = RequestMethod.GET)
    public String initNouvelleDemandePaiement(HttpServletRequest request,
            @PathVariable(value = "idDossierSubvention", required = true) Long idDossierSubvention) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request), idDossierSubvention);

        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.initNouvelleDemandePaiement(idDossierSubvention);
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Effectue une demande de paiement sur un dossier de subvention <br/>
     * ou met à jour une demande existante.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string json de l'utilisateur
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/demander", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String realiserDemandePaiement(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {
        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // on valide que l'utilisateur connecté possède les droits : pour créer ou modifier la demande de paiement
        if (servletRequest.getParameter(ID_DEMANDE) != null) {
            Long idDemande = Long.valueOf(servletRequest.getParameter(ID_DEMANDE));
            this.accesCollectiviteValidator.validerAccesDemandePaiement(this.getUtilisateurConnecte(request), idDemande);
        }

        // mapping via les données reçues
        DemandePaiementDto demandePaiementDto = genererDtoViaRequest(servletRequest);

        // appels
        DemandePaiementDto demandePaiementRealise = this.gestionDemandePaiementApplication.realiserDemandePaiement(demandePaiementDto);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementRealise);
    }

    /**
     * Génère le DemandePaiementDto via les données reçues (MultipartHttpServletRequest).
     *
     * @param servletRequest
     *            the multipartHttpServletRequest
     * @return le dto DemandePaiementDto
     */
    private static DemandePaiementDto genererDtoViaRequest(MultipartHttpServletRequest servletRequest) {
        DemandePaiementDto demandePaiementDto = new DemandePaiementDto();

        // mapping des données
        if (StringUtils.isNotBlank(servletRequest.getParameter(ID_DEMANDE))) {
            demandePaiementDto.setIdDemande(Long.valueOf(servletRequest.getParameter(ID_DEMANDE)));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idDossierSubvention"))) {
            demandePaiementDto.setIdDossierSubvention(Long.valueOf(servletRequest.getParameter("idDossierSubvention")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("version"))) {
            demandePaiementDto.setVersion(Integer.parseInt(servletRequest.getParameter("version")));
        }
        demandePaiementDto.setCodeEtatDemande(servletRequest.getParameter("codeEtatDemande"));

        String dateDemande = servletRequest.getParameter("dateDemande");
        if (StringUtils.isNotBlank(dateDemande)) {
            DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);
            demandePaiementDto.setDateDemande(LocalDateTime.of(LocalDate.parse(dateDemande, dateTimerFormatter), LocalTime.MIDNIGHT));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("montantTravauxHt"))) {
            demandePaiementDto.setMontantTravauxHt(new BigDecimal(servletRequest.getParameter("montantTravauxHt")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("tauxAideFacePercent"))
                && !"undefined".equals(servletRequest.getParameter("tauxAideFacePercent"))) {
            demandePaiementDto.setTauxAideFacePercent(new BigDecimal(servletRequest.getParameter("tauxAideFacePercent")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("message"))) {
            demandePaiementDto.setMessage(servletRequest.getParameter("message"));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("typeDemande"))) {
            TypeDemandePaiementLovDto typeDemandePaiementLovDto = new TypeDemandePaiementLovDto();
            typeDemandePaiementLovDto.setCode(servletRequest.getParameter("typeDemande"));
            demandePaiementDto.setTypeDemande(typeDemandePaiementLovDto);
        }

        // mapping des fichiers : via recherche des types de documents de type 'DOC_DEMANDE_PAIEMENT' uniquement
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.getListeTypeDocumentsPourFamille(FamilleDocumentEnum.DOC_DEMANDE_PAIEMENT)) {
            List<MultipartFile> fichierUploade = servletRequest.getFiles(typeDocument.getCode());
            for (MultipartFile multipartFile : fichierUploade) {
                FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(multipartFile);
                demandePaiementDto.getListeFichiersEnvoyer().add(fichierTransfert);
            }
        }

        // mapping des id des docs complémentaires à supprimer
        if (StringUtils.isNotBlank(servletRequest.getParameter("idsDocsComplSuppr"))) {
            String[] tabIdsDocsComplSuppr = servletRequest.getParameter("idsDocsComplSuppr").split(",");
            List<Long> idsDocsComplSuppr = new ArrayList<>(tabIdsDocsComplSuppr.length);
            for (String id : tabIdsDocsComplSuppr) {
                idsDocsComplSuppr.add(Long.valueOf(id));
            }
            demandePaiementDto.setIdsDocsComplSuppr(idsDocsComplSuppr);
        }

        return demandePaiementDto;
    }

    /**
     * Consultation d'une demande de paiement.
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            the id demande paiement
     * @return réponse json du dto de recherche de demande de paiement
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/details", method = RequestMethod.GET)
    public String consulterDemandePaiement(HttpServletRequest request,
            @PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on valide que l'utilisateur connecté possède les droits : pour modifier la demande de paiement
        this.accesCollectiviteValidator.validerAccesDemandePaiement(this.getUtilisateurConnecte(request), idDemandePaiement);

        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.rechercheDemandePaiement(idDemandePaiement);
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Génération de la décision attributive d'une demande de paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @param response
     *            the response
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE,
            RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/generation-decision-attributive", method = RequestMethod.GET)
    public void genererDecisionAttributive(@PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement,
            HttpServletResponse response) {

        FichierTransfert fichierTransfert = this.gestionDemandePaiementApplication.genererDecisionAttributive(idDemandePaiement);

        ModelDownloadUtils.telechargerFichierFromFichierTransfert(fichierTransfert, response);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : ANOMALIE_DETECTEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            the id demande paiement
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE,
            RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/anomalie-detectee", method = RequestMethod.GET)
    public String majDemandeAnomalieDetectee(@PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.majDemandePaiementAnomalieDetectee(idDemandePaiement);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : ANOMALIE_SIGNALEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/anomalie-signalee", method = RequestMethod.GET)
    public String majDemandePaiementAnomalieSignalee(@PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.majDemandePaiementAnomalieSignalee(idDemandePaiement);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : REFUSEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/refusee", method = RequestMethod.POST)
    public String majDemandePaiementRefusee(@PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement,
            @RequestBody DemandePaiementDto demandePaiementDto) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementMaj = this.gestionDemandePaiementApplication.majDemandePaiementRefusee(idDemandePaiement,
                demandePaiementDto.getMotifRefus());

        // retour
        return JsonMapper.getJsonFromData(demandePaiementMaj);
    }

    /**
     * Maj demande qualifiee.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/maj-etat/qualifiee", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String majDemandeQualifiee(HttpServletRequest request, HttpServletResponse response) {
        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // on valide que l'utilisateur connecté possède les droits : pour modifier la demande de paiement
        if (StringUtils.isBlank(servletRequest.getParameter(ID_DEMANDE))) {
            throw new TechniqueException("ID de la demande de paiement non spécifiée");
        }

        // mapping via les données reçues : on a eut uniquement l'id de demande + 1 fichier DECISION_ATTRIBUTIVE_PAIE
        DemandePaiementDto demandePaiementDto = genererDtoViaRequest(servletRequest);

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementMaj = this.gestionDemandePaiementApplication.majDemandePaiementQualifiee(demandePaiementDto);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementMaj);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : ACCORDEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/accordee", method = RequestMethod.GET)
    public String majDemandePaiementAccordee(HttpServletRequest request,
            @PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.majDemandePaiementAccordee(idDemandePaiement);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : CONTROLEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_RESPONSABLE, RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/controlee", method = RequestMethod.GET)
    public String majDemandePaiementControlee(HttpServletRequest request,
            @PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.majDemandePaiementControlee(idDemandePaiement);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : EN_ATTENTE_TRANSFERT
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_SD7_RESPONSABLE, RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/paiement/demande/{idDemandePaiement:[0-9]+}/maj-etat/en-attente-transfert", method = RequestMethod.GET)
    public String majDemandePaiementEnAttenteTransfert(@PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        // on met à jour la demande de paiement avec le nouvel état souhaité
        DemandePaiementDto demandePaiementDto = this.gestionDemandePaiementApplication.majDemandePaiementEnAttenteTransfert(idDemandePaiement);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementDto);
    }

    /**
     * Change l'état de la demande de paiement avec l'état : CORRIGEE
     *
     * @param request
     *            the request
     * @param idDemandePaiement
     *            id de la demande de paiement
     * @return réponse json de la demande de paiement mise à jour
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/demande/maj-etat/corrigee", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String majDemandePaiementPourCorrection(HttpServletRequest request, HttpServletResponse response) {
        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // on valide que l'utilisateur connecté possède les droits : pour créer ou modifier la demande de paiement
        if (servletRequest.getParameter(ID_DEMANDE) != null) {
            Long idDemande = Long.valueOf(servletRequest.getParameter(ID_DEMANDE));
            this.accesCollectiviteValidator.validerAccesDemandePaiement(this.getUtilisateurConnecte(request), idDemande);
        }

        // mapping via les données reçues
        DemandePaiementDto demandePaiementDto = genererDtoViaRequest(servletRequest);

        // appels
        DemandePaiementDto demandePaiementRealise = this.gestionDemandePaiementApplication
                .enregistrerDemandePaiementPourCorrection(demandePaiementDto);

        // retour
        return JsonMapper.getJsonFromData(demandePaiementRealise);
    }
}

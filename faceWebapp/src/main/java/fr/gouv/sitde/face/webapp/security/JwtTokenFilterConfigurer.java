package fr.gouv.sitde.face.webapp.security;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * The Class JwtTokenFilterConfigurer.
 */
public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    /** The jwt token provider. */
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * Instantiates a new jwt token filter configurer.
     *
     * @param jwtTokenProvider
     *            the jwt token provider
     */
    public JwtTokenFilterConfigurer(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.config.annotation.SecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.
     * SecurityBuilder)
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        JwtTokenFilter jwtFilter = new JwtTokenFilter(this.jwtTokenProvider);
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

}

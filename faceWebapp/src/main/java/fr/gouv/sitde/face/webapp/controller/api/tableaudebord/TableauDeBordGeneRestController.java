/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.tableaudebord;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DossierSubventionTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DotationCollectiviteTdbDto;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordGeneApplication;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;
import fr.gouv.sitde.face.webapp.controller.api.subvention.PageDemandeWithCollectiviteDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class TableauDeBordGeneRestController.
 *
 * @author a453029
 */
@RestController
@RequestMapping("/api")
public class TableauDeBordGeneRestController extends ConnexionAwareController {

    /** The tableau de bord application. */
    @Inject
    private TableauDeBordGeneApplication tableauDeBordGeneApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher tableau de bord generique.
     *
     * @param request
     *            the request
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/{idCollectivite:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherTableauDeBordGenerique(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        TableauDeBordGeneDto tdbGeneriqueDto = this.tableauDeBordGeneApplication.rechercherTableauDeBordGenerique(idCollectivite);
        return JsonMapper.getJsonFromData(tdbGeneriqueDto);
    }

    /**
     * Rechercher la liste de dotation collectivites pour les subventions à demander du tableau de bord.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/subademander/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAdemanderSubvention(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DotationCollectiviteTdbDto> pageReponse = this.tableauDeBordGeneApplication
                .rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(pageDemandeWithCollectiviteDto.getIdCollectivite(),
                        pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene ademander paiement.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/paiademander/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAdemanderPaiement(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DossierSubventionTdbDto> pageReponse = this.tableauDeBordGeneApplication.rechercherDossiersSubventionTdbGeneAdemanderPaiement(
                pageDemandeWithCollectiviteDto.getIdCollectivite(), pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene acompleter subvention.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/subacompleter/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAcompleterSubvention(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DotationCollectiviteTdbDto> pageReponse = this.tableauDeBordGeneApplication
                .rechercherDotationCollectiviteTdbGeneACompleterSubvention(pageDemandeWithCollectiviteDto.getIdCollectivite(),
                        pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene a corriger subvention.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/subacorriger/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAcorrigerSubvention(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordGeneApplication.rechercherDemandesSubventionTdbGeneAcorrigerSubvention(
                pageDemandeWithCollectiviteDto.getIdCollectivite(), pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene a corriger paiement.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/paiacorriger/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAcorrigerPaiement(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordGeneApplication.rechercherDemandesPaiementTdbGeneAcorrigerPaiement(
                pageDemandeWithCollectiviteDto.getIdCollectivite(), pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene acommencer paiement.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/paiacommencer/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAcommencerPaiement(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DossierSubventionTdbDto> pageReponse = this.tableauDeBordGeneApplication.rechercherDossiersSubventionTdbGeneAcommencerPaiement(
                pageDemandeWithCollectiviteDto.getIdCollectivite(), pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb gene asolder paiement.
     *
     * @param pageDemandeWithCollectiviteDto
     *            the page demande with collectivite dto
     * @param request
     *            the request
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/tableaudebord/gene/paiasolder/rechercher", method = RequestMethod.POST)
    public String rechercherTdbGeneAsolderPaiement(@RequestBody PageDemandeWithCollectiviteDto pageDemandeWithCollectiviteDto,
            HttpServletRequest request) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                pageDemandeWithCollectiviteDto.getIdCollectivite());

        // Recherche en base
        PageReponse<DossierSubventionTdbDto> pageReponse = this.tableauDeBordGeneApplication.rechercherDossiersSubventionTdbGeneAsolderPaiement(
                pageDemandeWithCollectiviteDto.getIdCollectivite(), pageDemandeWithCollectiviteDto.getPageDemande());
        return JsonMapper.getJsonFromData(pageReponse);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * The Class StringTrimToNullModule.
 *
 * @author a453029
 */
@Component
public class StringTrimToNullModule extends SimpleModule {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6333958319070976090L;

    /**
     * The Class StringStdScalarDeserializer.
     */
    private static class StringStdScalarDeserializer extends StdScalarDeserializer<String> {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = -7818646913292108155L;

        /**
         * Instantiates a new string std scalar deserializer.
         */
        public StringStdScalarDeserializer() {
            super(String.class);
        }

        /*
         * (non-Javadoc)
         *
         * @see com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml.jackson.core.JsonParser,
         * com.fasterxml.jackson.databind.DeserializationContext)
         */
        @Override
        public String deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {
            return StringUtils.trimToNull(jsonParser.getValueAsString());
        }

    }

    /**
     * Instantiates a new string trim to null module.
     */
    public StringTrimToNullModule() {
        this.addDeserializerWithTrim();
    }

    /**
     * Adds the deserializer with trim.
     */
    private void addDeserializerWithTrim() {
        this.addDeserializer(String.class, new StringStdScalarDeserializer());
    }
}

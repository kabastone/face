/**
 *
 */
package fr.gouv.sitde.face.webapp.utils.filetransfert;

import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * Classe utilitaire liée à l'upload de fichiers.
 *
 * @author Atos
 *
 */
public final class ModelUploadUtils {

    /** The Constant EXTENSION_CSV. */
    private static final String EXTENSION_CSV = "csv";

    /**
     * Constructeur privé car il s'agit d'une classe utilitaire qui ne doit pas etre instanciée.
     */
    private ModelUploadUtils() {

    }

    /**
     * Valider le type du fichier uploadé. Si le format n'est pas autorisé, la méthode envoie une regleGestionException.
     *
     * @param multipartFile
     *            le multipartFile à tester
     */
    public static void validerTypeFichierUploade(MultipartFile multipartFile) {

        // Contrôle de la présence d'un fichier uploadé
        if ((multipartFile == null) || multipartFile.isEmpty()) {
            throw new TechniqueException("TODO : message types autorisés");
        }

        ExtensionFichierEnum typeFichierTrouve = null;

        // Controle de l'extension du fichier uploadé
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
        typeFichierTrouve = ExtensionFichierEnum.rechercherEnumParExtension(extension);
        if (typeFichierTrouve == null) {
            throw new RegleGestionException("TODO : message types autorisés");
        }

        // Controle du content-type du fichier uploadé
        typeFichierTrouve = ExtensionFichierEnum.rechercherEnumParContentType(multipartFile.getContentType());
        if (typeFichierTrouve == null) {
            throw new RegleGestionException("TODO : message types autorisés");
        }
    }

    /**
     * Valider fichier csv uploaded.
     *
     * @param multipartFile
     *            the multipart file
     */
    public static void validerFichierCsvUploaded(MultipartFile multipartFile) {

        // Contrôle de la présence d'un fichier uploadé
        if ((multipartFile == null) || multipartFile.isEmpty()) {
            throw new TechniqueException("upload.erreur.videounull");
        }

        // Controle de l'extension du fichier uploadé
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

        if (!EXTENSION_CSV.equalsIgnoreCase(extension)) {
            throw new RegleGestionException("upload.erreur.csv");
        }
    }

    /**
     * Construit le fichier transfere à partir d'un MultipartFile.
     *
     * @param multipartFile
     *            the multipart file
     * @return the fichier transfere
     */
    public static FichierTransfert getFichierTransfereFromMultiPartFile(MultipartFile multipartFile) {

        FichierTransfert fichierTransfert = new FichierTransfert();

        try {
            fichierTransfert.setBytes(multipartFile.getBytes());
        } catch (IOException e) {
            throw new TechniqueException("Impossible de recuperer le tableau de bytes du multipartFile", e);
        }

        fichierTransfert.setNomFichier(multipartFile.getOriginalFilename());
        fichierTransfert.setContentTypeTheorique(multipartFile.getContentType());

        TypeDocumentEnum typeDocument = TypeDocumentEnum.rechercherEnumParCode(multipartFile.getName());
        fichierTransfert.setTypeDocument(typeDocument);

        return fichierTransfert;
    }
}

package fr.gouv.sitde.face.webapp.configuration;

import java.util.List;

import javax.inject.Inject;

import org.apache.catalina.filters.SetCharacterEncodingFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.gouv.sitde.face.webapp.interceptor.LoggingContextInterceptor;
import i2.application.cerbere.filtre.FiltreCerbere;

/**
 * The Class WebConfig.
 */
@EnableWebMvc
@Configuration
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class, SecurityAutoConfiguration.class })
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
public class WebConfig implements WebMvcConfigurer {

    /** The logging interceptor. */
    @Inject
    private LoggingContextInterceptor loggingInterceptor;

    /** The cerbere properties. */
    @Inject
    private CerbereProperties cerbereProperties;

    /**
     * Character encoding filter bean.
     *
     * @return the filter registration bean
     */
    @Bean
    public FilterRegistrationBean<SetCharacterEncodingFilter> characterEncodingFilterBean() {
        FilterRegistrationBean<SetCharacterEncodingFilter> registrationBean = new FilterRegistrationBean<>();
        SetCharacterEncodingFilter characterEncodingFilter = new SetCharacterEncodingFilter();
        registrationBean.setFilter(characterEncodingFilter);
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        registrationBean.addInitParameter("encoding", "UTF-8");
        registrationBean.addInitParameter("ignore", "true");

        return registrationBean;
    }

    /**
     * Cerbere filter bean.
     *
     * @return the filter registration bean
     */
    @Bean
    public FilterRegistrationBean<FiltreCerbere> cerbereFilterBean() {
        FilterRegistrationBean<FiltreCerbere> registrationBean = new FilterRegistrationBean<>();
        FiltreCerbere cerbereFilter = new FiltreCerbere();
        registrationBean.setFilter(cerbereFilter);
        registrationBean.addUrlPatterns("/connecter");
        registrationBean.addUrlPatterns("/deconnecter");
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE + 1);

        // Ajout des paramètres d'initialisation du filtre Cerbere
        registrationBean.addInitParameter(CerbereParamEnum.APPLICATION_ID.getNom(), this.cerbereProperties.getApplicationId());

        if (!StringUtils.isBlank(this.cerbereProperties.getSessionSecure())) {
            registrationBean.addInitParameter(CerbereParamEnum.SESSION_SECURE.getNom(), this.cerbereProperties.getSessionSecure());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getSessionHttpOnly())) {
            registrationBean.addInitParameter(CerbereParamEnum.SESSION_HTTP_ONLY.getNom(), this.cerbereProperties.getSessionHttpOnly());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getConf())) {
            registrationBean.addInitParameter(CerbereParamEnum.CONF.getNom(), this.cerbereProperties.getConf());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getUrlsOuvertes())) {
            registrationBean.addInitParameter(CerbereParamEnum.URLS_OUVERTES.getNom(), this.cerbereProperties.getUrlsOuvertes());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getApplicationEntree())) {
            registrationBean.addInitParameter(CerbereParamEnum.APPLICATION_ENTREE.getNom(), this.cerbereProperties.getApplicationEntree());
        }

        // Ajout des paramètres d'initialisation specifiques au Cerbere bouchon
        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getLogNiveau())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_LOG_NIVEAU.getNom(), this.cerbereProperties.getBouchon().getLogNiveau());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getLogFichier())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_LOG_FICHIER.getNom(), this.cerbereProperties.getBouchon().getLogFichier());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getCle())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_CLE.getNom(), this.cerbereProperties.getBouchon().getCle());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getChallengeType())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_CHALLENGE_TYPE.getNom(),
                    this.cerbereProperties.getBouchon().getChallengeType());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getCertKs())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_CERT_KS.getNom(), this.cerbereProperties.getBouchon().getCertKs());
        }

        if (!StringUtils.isBlank(this.cerbereProperties.getBouchon().getCertKsPasse())) {
            registrationBean.addInitParameter(CerbereParamEnum.BOUCHON_CERT_KS_PASSE.getNom(), this.cerbereProperties.getBouchon().getCertKsPasse());
        }

        return registrationBean;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#addResourceHandlers(org.springframework.web.servlet.config.annotation.
     * ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // specifying static resource location for themes related files(css etc)
        registry.addResourceHandler("/app/**").addResourceLocations("/app/");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#addInterceptors(org.springframework.web.servlet.config.annotation.
     * InterceptorRegistry)
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.loggingInterceptor);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#extendMessageConverters(java.util.List)
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        for (HttpMessageConverter<?> converter : converters) {
            if ((converter instanceof MappingJackson2HttpMessageConverter) || (converter instanceof MappingJackson2XmlHttpMessageConverter)) {
                ObjectMapper objectMapper = ((AbstractJackson2HttpMessageConverter) converter).getObjectMapper();
                objectMapper.registerModule(new StringTrimToNullModule());
                return;
            }
        }
    }
}

/**
 *
 */
package fr.gouv.sitde.face.webapp.utils.jsonutils;

import java.util.Collection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.webapp.configuration.StringTrimToNullModule;
import fr.gouv.sitde.face.webapp.exception.DataNotFoundException;

/**
 * Mapper des objets vers une chaine json.
 *
 * @author Atos
 *
 */
public final class JsonMapper {

    /**
     * Constructer privé.
     */
    private JsonMapper() {
        super();
    }

    /**
     * Gets the json from any object.
     * <p>
     * Si l'objet data est null ou est une instance de collection vide, une DataNotFoundException est lancée.
     * </p>
     *
     * @param data
     *            the data
     * @return the json from data
     */
    @SuppressWarnings("rawtypes")
    public static String getJsonFromData(Object data) {

        if ((data == null) || ((data instanceof Collection) && ((Collection) data).isEmpty())) {
            throw new DataNotFoundException("Aucune donnée n'a été trouvée");
        }

        ObjectMapper mapper = new ObjectMapper().registerModules(new StringTrimToNullModule())
                .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        String jsonResult = "";

        try {
            jsonResult = mapper.writeValueAsString(data);

        } catch (JsonProcessingException e) {
            throw new TechniqueException("Erreur pendant le mapping de l'objet en json", e);
        }
        return jsonResult;
    }
}

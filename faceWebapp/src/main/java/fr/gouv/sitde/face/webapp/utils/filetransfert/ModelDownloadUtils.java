/**
 *
 */
package fr.gouv.sitde.face.webapp.utils.filetransfert;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * Classe utilitaire pour le téléchargement de fichiers.
 *
 * @author Atos
 *
 */
public final class ModelDownloadUtils {

    /** The Constant CONTENT_TYPE_OCTET_STREAM. */
    public static final String CONTENT_TYPE_OCTET_STREAM = "application/octet-stream";

    /**
     * Constructeur privé car il s'agit d'une classe utilitaire qui ne doit pas etre instanciée.
     */
    private ModelDownloadUtils() {

    }

    /**
     * Permet de télécharger un fichier.
     *
     * @param fileToDownload
     *            the file to download
     * @param response
     *            the response
     */
    public static void telechargerFichierFromFile(File fileToDownload, HttpServletResponse response) {
        if (fileToDownload == null) {
            throw new TechniqueException("Il n'y a aucun fichier à télécharger");
        }
        try (FileInputStream fileInputStream = new FileInputStream(fileToDownload); OutputStream outputStream = response.getOutputStream()) {
            response.setContentType(obtenirContentType(fileToDownload));
            response.setHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION, String.format("attachment;filename=%s", fileToDownload.getName()));
            response.setContentLength((int) fileToDownload.length());
            IOUtils.copy(fileInputStream, outputStream);
            response.flushBuffer();
        } catch (IOException exception) {
            throw new TechniqueException(exception.getMessage(), exception);
        }
    }

    /**
     * Permet de télécharger un fichier à partir d'un fichierTransfert.
     *
     * @param fichierTransfert
     *            the fichier transfert
     * @param response
     *            the response
     */
    public static void telechargerFichierFromFichierTransfert(FichierTransfert fichierTransfert, HttpServletResponse response) {
        if ((fichierTransfert == null) || (fichierTransfert.getBytes() == null)) {
            throw new TechniqueException("Il n'y a aucun fichier à télécharger");
        }
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(fichierTransfert.getBytes());
                OutputStream outputStream = response.getOutputStream()) {
            response.setContentType(fichierTransfert.getContentTypeTheorique());
            response.setHeader(ConstantesFace.ENTETE_CONTENT_DISPOSITION, String.format("attachment; filename=%s", fichierTransfert.getNomFichier()));
            response.setContentLength(fichierTransfert.getBytes().length);
            IOUtils.copy(inputStream, outputStream);
            response.flushBuffer();
        } catch (IOException exception) {
            throw new TechniqueException(exception.getMessage(), exception);
        }
    }

    /**
     * Recupère le contentType du file passé en paramètre. <br/>
     * Si aucune extension connue n'est identifiée à partir du fileName, le content type octet-stream est renvoyé.
     *
     */
    private static String obtenirContentType(File fileToDownload) {

        if ((fileToDownload == null) || StringUtils.isEmpty(fileToDownload.getName())) {
            return CONTENT_TYPE_OCTET_STREAM;
        }

        String extension = StringUtils.substringAfterLast(fileToDownload.getName(), ".");

        ExtensionFichierEnum extensionEnum = ExtensionFichierEnum.rechercherEnumParExtension(extension);

        // Si le content type n'est pas connu, on renvoie CONTENT_TYPE_OCTET_STREAM
        if (extensionEnum == null) {
            return CONTENT_TYPE_OCTET_STREAM;
        } else {
            return extensionEnum.getContentType();
        }

    }
}

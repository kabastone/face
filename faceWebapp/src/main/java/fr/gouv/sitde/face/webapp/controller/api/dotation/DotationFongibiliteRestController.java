/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.dotation;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import fr.gouv.sitde.face.application.dotation.GestionFongibiliteDotationApplication;
import fr.gouv.sitde.face.application.dto.DotationCollectiviteFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationListeLiensFongibiliteDto;
import fr.gouv.sitde.face.application.dto.DotationTransfertFongibiliteDto;
import fr.gouv.sitde.face.application.dto.lov.TransfertFongibiliteLovDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * @author a768251
 *
 */
@RestController
@RequestMapping("/api/dotation/")
public class DotationFongibiliteRestController extends ConnexionAwareController {

    @Inject
    private GestionFongibiliteDotationApplication gestionFongibiliteDotationApplication;

    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/fongibilite/collectivite/{idCollectivite}/rechercher", method = RequestMethod.GET)
    public String rechercherFongibiliteDotationParIdCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        DotationListeLiensFongibiliteDto dotationFongibiliteDto = this.gestionFongibiliteDotationApplication
                .rechercherFongibiliteDotationParIdCollectivite(idCollectivite);
        return JsonMapper.getJsonFromData(dotationFongibiliteDto);
    }

    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/fongibilite/collectivite/{idCollectivite}/sous-programme/{idSousProgramme}/rechercher", method = RequestMethod.GET)
    public String rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "idSousProgramme", required = true) Integer idSousProgramme) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        DotationCollectiviteFongibiliteDto dotationCollectiviteFongibilteDto = this.gestionFongibiliteDotationApplication
                .rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);
        return JsonMapper.getJsonFromData(dotationCollectiviteFongibilteDto);
    }

    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/fongibiliter/transferer", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String enregisterTransfertFongibilite(HttpServletRequest request, HttpServletResponse response) {
        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // mapping via les données reçues
        DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto = genererDtoViaRequest(servletRequest);

        // On valide si l'utilissateur connecté possède les droits d'accès à la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                dotationTransfertFongibiliteDto.getIdCollectivite());
        DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto2 = this.gestionFongibiliteDotationApplication
                .enregistrerDotationTransfertFonbilite(dotationTransfertFongibiliteDto);

        return JsonMapper.getJsonFromData(dotationTransfertFongibiliteDto2);
    }

    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/transfert/collectivite/{idCollectivite:[0-9]+}/annee/{annee}/rechercher", method = RequestMethod.GET)
    public String rechercherTransfertCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "annee", required = true) Integer annee) {

        // On valide si l'utilissateur connecté possède les droits d'accès à la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);
        List<TransfertFongibiliteLovDto> transferts = this.gestionFongibiliteDotationApplication
                .rechercherTransfertsCollectiviteParAnnee(idCollectivite, annee);
        return JsonMapper.getJsonFromData(transferts);
    }

    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/transfert/annee/{annee}/rechercher", method = RequestMethod.GET)
    public String rechercherTransfertCollectiviteMfer(HttpServletRequest request, @PathVariable(value = "annee", required = true) Integer annee) {

        // On valide si l'utilissateur connecté possède les droits d'accès à la collectivité
        List<TransfertFongibiliteLovDto> transferts = this.gestionFongibiliteDotationApplication.rechercherTransfertsParAnnee(annee);
        return JsonMapper.getJsonFromData(transferts);
    }

    /**
     * Génère le DotationTransfertFongibiliteDto via les données reçues (MultipartHttpServletRequest).
     *
     * @param servletRequest
     *            the multipartHttpServletRequest
     * @return le dto DotationTransfertFongibiliteDto
     */
    private static DotationTransfertFongibiliteDto genererDtoViaRequest(MultipartHttpServletRequest servletRequest) {

        DotationTransfertFongibiliteDto dotationTransfertFongibiliteDto = new DotationTransfertFongibiliteDto();

        // mapping des données

        if (StringUtils.isNotBlank(servletRequest.getParameter("idSousProgrammeCrediter"))) {
            dotationTransfertFongibiliteDto.setIdSousProgrammeCrediter(Integer.valueOf(servletRequest.getParameter("idSousProgrammeCrediter")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("idSousProgrammeDebiter"))) {
            dotationTransfertFongibiliteDto.setIdSousProgrammeDebiter(Integer.valueOf(servletRequest.getParameter("idSousProgrammeDebiter")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("montantTransfert"))) {
            dotationTransfertFongibiliteDto.setMontantTransfert(new BigDecimal(servletRequest.getParameter("montantTransfert")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("idCollectivite"))) {
            dotationTransfertFongibiliteDto.setIdCollectivite(Long.valueOf(servletRequest.getParameter("idCollectivite")));
        }

        return dotationTransfertFongibiliteDto;
    }
}

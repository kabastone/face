/**
 *
 */
package fr.gouv.sitde.face.webapp.error;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * API Error : Objet d'erreur de l'API.
 *
 *
 * @author a453029
 *
 */
public class ApiError {

    /** The status. */
    private final HttpStatus status;

    /** The type. */
    private final String type;

    /** The message. */
    private final String message;

    /** The erreurs multiples. */
    private final List<String> erreursMultiples;

    /**
     * Instantiates a new api error.
     *
     * @param status
     *            the status
     * @param type
     *            the type
     * @param message
     *            the message
     * @param erreursMultiples
     *            the erreurs multiples
     */
    public ApiError(HttpStatus status, String type, String message, List<String> erreursMultiples) {
        super();
        this.status = status;
        this.type = type;
        this.message = message;
        this.erreursMultiples = erreursMultiples;
    }

    /**
     * Instantiates a new api error.
     *
     * @param status
     *            the status
     * @param type
     *            the type
     * @param message
     *            the message
     */
    public ApiError(HttpStatus status, String type, String message) {
        super();
        this.status = status;
        this.type = type;
        this.message = message;
        this.erreursMultiples = new ArrayList<>(0);
    }

    /**
     * Instantiates a new api error.
     *
     * @param status
     *            the status
     * @param type
     *            the type
     * @param message
     *            the message
     * @param erreursMultiples
     *            the erreurs multiples
     */
    public ApiError(HttpStatus status, String type, String message, String erreursMultiples) {
        super();
        this.status = status;
        this.type = type;
        this.message = message;
        this.erreursMultiples = Arrays.asList(erreursMultiples);
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public HttpStatus getStatus() {
        return this.status;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Gets the erreurs multiples.
     *
     * @return the erreurs multiples
     */
    public List<String> getErreursMultiples() {
        return this.erreursMultiples;
    }
}

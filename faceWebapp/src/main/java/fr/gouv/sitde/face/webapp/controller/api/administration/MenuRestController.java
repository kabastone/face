/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CollectiviteSimpleDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class CollectiviteRestController.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class MenuRestController extends ConnexionAwareController {

    /** The gestion collectivite application. */
    @Inject
    private GestionCollectiviteApplication gestionCollectiviteApplication;

    /**
     * Liste toutes les collectivitées existantes.
     *
     * @return the string json de l'utilisateur
     */
    @RequestMapping(value = "/collectivite/lister", method = RequestMethod.GET)
    public String listerToutesCollectivites() {
        List<CollectiviteSimpleDto> collectivites = this.gestionCollectiviteApplication.rechercherCollectivitesSimple();
        return JsonMapper.getJsonFromData(collectivites);
    }

}

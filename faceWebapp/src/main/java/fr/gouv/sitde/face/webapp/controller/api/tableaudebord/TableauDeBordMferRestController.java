/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.tableaudebord;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.dto.tdb.DemandePaiementTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeProlongationTdbDto;
import fr.gouv.sitde.face.application.dto.tdb.DemandeSubventionTdbDto;
import fr.gouv.sitde.face.application.tableaudebord.TableauDeBordMferApplication;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class TableauDeBordMferRestController.
 *
 * @author a453029
 */
@RestController
@RequestMapping("/api")
public class TableauDeBordMferRestController extends ConnexionAwareController {

    /** The tableau de bord mfer application. */
    @Inject
    private TableauDeBordMferApplication tableauDeBordMferApplication;

    /**
     * Rechercher tableau de bord MFER.
     *
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/rechercher", method = RequestMethod.GET)
    public String rechercherTableauDeBordMfer() {

        TableauDeBordMferDto tdbMferDto = this.tableauDeBordMferApplication.rechercherTableauDeBordMfer();
        return JsonMapper.getJsonFromData(tdbMferDto);
    }

    /**
     * Rechercher tdb mfer aqualifier subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/subaqualifier/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAqualifierSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesSubventionTdbMferAqualifierSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer aqualifier paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/paiaqualifier/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAqualifierPaiement(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesPaiementTdbMferAqualifierPaiement(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer aaccorder subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/subaaccorder/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAaccorderSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesSubventionTdbMferAaccorderSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer aaccorder paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/paiaaccorder/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAaccorderPaiement(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesPaiementTdbMferAaccorderPaiement(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer aattribuer subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/subaattribuer/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAattribuerSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesSubventionTdbMferAattribuerSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer asignaler subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/subasignaler/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAsignalerSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeSubventionTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesSubventionTdbMferAsignalerSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer asignaler paiement.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/paiasignaler/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAsignalerPaiement(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandePaiementTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesPaiementTdbMferAsignalerPaiement(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

    /**
     * Rechercher tdb mfer aprolonger subvention.
     *
     * @param pageDemande
     *            the page demande
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/tableaudebord/mfer/subaprolonger/rechercher", method = RequestMethod.POST)
    public String rechercherTdbMferAprolongerSubvention(@RequestBody PageDemande pageDemande) {

        // Recherche en base
        PageReponse<DemandeProlongationTdbDto> pageReponse = this.tableauDeBordMferApplication
                .rechercherDemandesProlongationTdbMferAprolongerSubvention(pageDemande);
        return JsonMapper.getJsonFromData(pageReponse);
    }

}

package fr.gouv.sitde.face.webapp.controller.api.prolongation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonMappingException;

import fr.gouv.sitde.face.application.dto.DemandeProlongationDto;
import fr.gouv.sitde.face.application.prolongation.GestionDemandeProlongationApplication;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class DemandeProlongationRestController.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api/prolongation/")
public class DemandeProlongationRestController extends ConnexionAwareController {

    /** The gestion demande prolongation application. */
    @Inject
    private GestionDemandeProlongationApplication gestionDemandeProlongationApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher demande prolongation par id.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/{idDemandeProlongation}/rechercher", method = RequestMethod.GET)
    public String rechercherDemandeProlongationParId(HttpServletRequest request,
            @PathVariable(value = "idDemandeProlongation", required = true) Long idDemandeProlongation) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDemandeProlongation(this.getUtilisateurConnecte(request), idDemandeProlongation);

        DemandeProlongationDto demandeProlongationDto = this.gestionDemandeProlongationApplication
                .rechercherDemandeProlongationParId(idDemandeProlongation);
        return JsonMapper.getJsonFromData(demandeProlongationDto);
    }

    /**
     * Inits the nouvelle demande prolongation.
     *
     * @param idDossier
     *            the id dossier
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/dossier/{idDossier}/initialiser", method = RequestMethod.GET)
    public String initNouvelleDemandeProlongation(@PathVariable(value = "idDossier", required = true) Long idDossier) {

        DemandeProlongationDto demandeProlongation = this.gestionDemandeProlongationApplication.initNouvelleDemandeProlongation(idDossier);
        return JsonMapper.getJsonFromData(demandeProlongation);
    }

    /**
     * Creer nouvelle demande Prolongation.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/demande/creer", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String creerNouvelleDemandeProlongation(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // Initialisation
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // Récupération de tous les propriétés de l'objet Demande Prolongation DTO depuis la Http Request.
        DemandeProlongationDto demandeProlongation = genererDtoViaRequest(servletRequest);

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDossierSubvention(this.getUtilisateurConnecte(request),
                demandeProlongation.getIdDossierSubvention());

        DemandeProlongationDto retourDemandeProlongation = this.gestionDemandeProlongationApplication.creerDemandeProlongation(demandeProlongation);

        return JsonMapper.getJsonFromData(retourDemandeProlongation);
    }

    /**
     * Accorder demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeProlongation}/accorder", method = RequestMethod.GET)
    public String accorderDemandeProlongation(HttpServletRequest request,
            @PathVariable(value = "idDemandeProlongation", required = true) Long idDemandeProlongation) {
        DemandeProlongationDto retourDemandeProlongation = this.gestionDemandeProlongationApplication
                .accorderDemandeProlongation(idDemandeProlongation);

        return JsonMapper.getJsonFromData(retourDemandeProlongation);
    }

    /**
     * Refuser demande Prolongation.
     *
     * @param id
     *            the id
     * @param demandeProlongation
     *            the demande prolongation
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/demande/{idDemandeProlongation}/refuser", method = RequestMethod.POST)
    public String refuserDemandeProlongation(@PathVariable(value = "idDemandeProlongation", required = true) Long id,
            @RequestBody DemandeProlongationDto demandeProlongation) {
        DemandeProlongationDto retourDemandeProlongation = this.gestionDemandeProlongationApplication.refuserDemandeProlongation(demandeProlongation);

        return JsonMapper.getJsonFromData(retourDemandeProlongation);
    }

    /**
     * Generer dto via request.
     *
     * @param servletRequest
     *            the servlet request
     * @return the demande prolongation dto
     */
    private static DemandeProlongationDto genererDtoViaRequest(MultipartHttpServletRequest servletRequest) {
        DemandeProlongationDto demande = new DemandeProlongationDto();
        DateTimeFormatter dateTimerFormatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE);

        // Mapping manuel des propriétés
        if (StringUtils.isNotBlank(servletRequest.getParameter("id"))) {
            demande.setId(Long.valueOf(servletRequest.getParameter("id")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("etat"))) {
            demande.setEtat(servletRequest.getParameter("etat"));
        }
        String dateEcheanceAchevement = servletRequest.getParameter("dateEcheanceAchevement");
        if (StringUtils.isNotBlank(dateEcheanceAchevement)) {
            demande.setDateEcheanceAchevement(LocalDateTime.of(LocalDate.parse(dateEcheanceAchevement, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        String dateEcheanceAchevementDemande = servletRequest.getParameter("dateEcheanceAchevementDemande");
        if (StringUtils.isNotBlank(dateEcheanceAchevementDemande)) {
            demande.setDateEcheanceAchevementDemande(
                    LocalDateTime.of(LocalDate.parse(dateEcheanceAchevementDemande, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("numDossier"))) {
            demande.setNumDossier(servletRequest.getParameter("numDossier"));
        }
        String dateDemande = servletRequest.getParameter("dateDemande");
        if (StringUtils.isNotBlank(dateDemande)) {
            demande.setDateDemande(LocalDateTime.of(LocalDate.parse(dateDemande, dateTimerFormatter), LocalTime.MIDNIGHT));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("motifRefus"))) {
            demande.setMotifRefus(servletRequest.getParameter("motifRefus"));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("idDossierSubvention"))) {
            demande.setIdDossierSubvention(Long.valueOf(servletRequest.getParameter("idDossierSubvention")));
        }
        if (StringUtils.isNotBlank(servletRequest.getParameter("version"))) {
            demande.setVersion(Integer.parseInt(servletRequest.getParameter("version")));
        }

        // Mapping des fichiers, uniquement des "DOC_DEMANDE_PROLONGATION"
        for (TypeDocumentEnum typeDocument : TypeDocumentEnum.getListeTypeDocumentsPourFamille(FamilleDocumentEnum.DOC_DEMANDE_PROLONGATION)) {
            MultipartFile fichierUploade = servletRequest.getFile(typeDocument.getCode());
            if (fichierUploade != null) {
                FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichierUploade);
                demande.getListeFichiersEnvoyer().add(fichierTransfert);
            }
        }

        return demande;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.webapp.security;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.application.administration.RechercheCollectiviteOrigineApplication;
import fr.gouv.sitde.face.transverse.document.OrigineDocumentDto;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.util.MessageUtils;
import fr.gouv.sitde.face.webapp.controller.connexion.UtilisateurConnecteDto;

/**
 * Validateur d'accès selon le profil et les droits de l'utilisateur sur les collectivités.
 *
 * @author a453029
 *
 */
@Component
public class AccesCollectiviteValidator {

    private static final String CLE_MSG_DROIT_AGENT = "utilisateur.acces.collectivite.agent";

    /** The recherche collectivite origine application. */
    @Inject
    private RechercheCollectiviteOrigineApplication rechercheCollectiviteOrigineApplication;

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idCollectivite
     */
    public void validerAccesCollectivitePourMferSd7EtAgent(UtilisateurConnecteDto utilisateurConnecteDto, Long idCollectivite) {

        if ((utilisateurConnecteDto == null)
                || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(idCollectivite))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * La méthode valide si l'utilisateur a le rôle de MFER ou bien est uniquement générique et a les droits d'agent sur la collectivité donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idCollectivite
     */
    public void validerAccesCollectivitePourMferEtAgent(UtilisateurConnecteDto utilisateurConnecteDto, Long idCollectivite) {

        if ((utilisateurConnecteDto == null) || utilisateurConnecteDto.estSD7()
                || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(idCollectivite))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur n'a pas le rôle MFER, la méthode valide qu'il a des droits d'admin sur la collectivité donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idCollectivite
     */
    public void validerAccesAdminCollectivite(UtilisateurConnecteDto utilisateurConnecteDto, Long idCollectivite) {

        if ((utilisateurConnecteDto == null) || (!utilisateurConnecteDto.estMfer() && !utilisateurConnecteDto.estAdminCollectivite(idCollectivite))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier("utilisateur.acces.collectivite.admin"));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité de la dotation de
     * collectivité donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDotationCollectivite
     */
    public void validerAccesDotationCollectivite(UtilisateurConnecteDto utilisateurConnecteDto, Long idDotationCollectivite) {

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(
                this.rechercheCollectiviteOrigineApplication.rechercherIdCollectiviteParDotationCollectivite(idDotationCollectivite)))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité du dossier donné.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDossierSubvention
     */
    public void validerAccesDossierSubvention(UtilisateurConnecteDto utilisateurConnecteDto, Long idDossierSubvention) {

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(
                this.rechercheCollectiviteOrigineApplication.rechercherIdCollectiviteParDossierSubvention(idDossierSubvention)))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité de la demande de subvention
     * donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDemandeSubvention
     */
    public void validerAccesDemandeSubvention(UtilisateurConnecteDto utilisateurConnecteDto, Long idDemandeSubvention) {

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(
                this.rechercheCollectiviteOrigineApplication.rechercherIdCollectiviteParDemandeSubvention(idDemandeSubvention)))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité de la demande de prolongation
     * donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDemandeProlongation
     */
    public void validerAccesDemandeProlongation(UtilisateurConnecteDto utilisateurConnecteDto, Long idDemandeProlongation) {

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto.estAgentCollectivite(
                this.rechercheCollectiviteOrigineApplication.rechercherIdCollectiviteParDemandeProlongation(idDemandeProlongation)))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la collectivité de la demande de paiement
     * donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDemandePaiement
     */
    public void validerAccesDemandePaiement(UtilisateurConnecteDto utilisateurConnecteDto, Long idDemandePaiement) {

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.estUniquementGenerique() && !utilisateurConnecteDto
                .estAgentCollectivite(this.rechercheCollectiviteOrigineApplication.rechercherIdCollectiviteParDemandePaiement(idDemandePaiement)))) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }
    }

    /**
     * Si l'utilisateur a uniquement le rôle générique, la méthode valide qu'il a des droits d'agent sur la ou les collectivités liées au document
     * donnée.<br/>
     * Dans le cas contraire, une technique exception est lancée.
     *
     * @param utilisateurConnecteDto
     * @param idDocument
     */
    public void validerAccesDocument(UtilisateurConnecteDto utilisateurConnecteDto, Long idDocument) {

        if (utilisateurConnecteDto == null) {
            throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
        }

        if (utilisateurConnecteDto.estUniquementGenerique()) {
            OrigineDocumentDto origineDocument = this.rechercheCollectiviteOrigineApplication.rechercherCollectiviteOrigineParDocument(idDocument);

            boolean droitAgentTrouve = false;

            for (Long idCollectiviteDoc : origineDocument.getListeIdCollectivitesOrigine()) {
                if (utilisateurConnecteDto.estAgentCollectivite(idCollectiviteDoc)) {
                    droitAgentTrouve = true;
                }
            }

            if (!origineDocument.isDocSansLienCollectivite() && !droitAgentTrouve) {
                throw new TechniqueException(MessageUtils.getMsgValidationMetier(CLE_MSG_DROIT_AGENT));
            }
        }
    }
}

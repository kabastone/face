package fr.gouv.sitde.face.webapp.controller.api.dotation;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonMappingException;

import fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication;
import fr.gouv.sitde.face.application.dotation.GestionRenoncementDotationApplication;
import fr.gouv.sitde.face.application.dto.DotationRenoncementDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleTableauPertesRenoncementDto;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

@RestController
@RequestMapping("/api/dotation/")
public class DotationRenoncementRestController extends ConnexionAwareController {

    @Inject
    private GestionRenoncementDotationApplication gestionRenoncementDotationApplication;

    @Inject
    private GestionDotationDepartementApplication gestionDotationDepartementApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher renoncement dotation par id collectivite et id sous programme.
     *
     * @param request
     *            the request
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/renoncement/collectivite/{idCollectivite}/sous-programme/{idSousProgramme}/rechercher", method = RequestMethod.GET)
    public String rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "idSousProgramme", required = true) Integer idSousProgramme) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        DotationRenoncementDto dotationRenoncementDto = this.gestionRenoncementDotationApplication
                .rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);
        return JsonMapper.getJsonFromData(dotationRenoncementDto);
    }

    /**
     * Renoncer dotation.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/renoncement/renoncer", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String renoncerDotation(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException {

        // init
        MultipartHttpServletRequest servletRequest = (MultipartHttpServletRequest) request;

        // mapping via les données reçues
        DotationRenoncementDto dotationRenoncementDto = genererDtoViaRequest(servletRequest);

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request),
                dotationRenoncementDto.getIdCollectivite());

        // on renonce à la dotation
        DotationRenoncementDto dotRenoncementDtoRealise = this.gestionRenoncementDotationApplication.renoncerDotation(dotationRenoncementDto);

        return JsonMapper.getJsonFromData(dotRenoncementDtoRealise);
    }

    /**
     * Lister pertes et renoncement dotation.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/renoncement/collectivite/{idCollectivite}/rechercher", method = RequestMethod.GET)
    public String listerLignesDotationCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // on valide que l'utilisateur connecté possède les droits d'accès à la collectivité.
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        // on renonce à la dotation
        List<LigneDotationDepartementaleTableauPertesRenoncementDto> listeLignes = this.gestionDotationDepartementApplication
                .listerLignesDotationsCollectivitePertesEtRenoncement(idCollectivite);

        return JsonMapper.getJsonFromData(listeLignes);
    }

    /**
     * Lister pertes et renoncement dotation.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     * @throws JsonMappingException
     *             the json mapping exception
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/renoncement/code/{codeProgramme}/rechercher", method = RequestMethod.GET)
    public String listerLignesDotation(HttpServletRequest request, @PathVariable(value = "codeProgramme", required = true) String codeProgramme) {
        // on renonce à la dotation
        List<LigneDotationDepartementaleTableauPertesRenoncementDto> listeLignes = this.gestionDotationDepartementApplication
                .listerLignesPertesEtRenoncement(codeProgramme);

        return JsonMapper.getJsonFromData(listeLignes);
    }

    /**
     * Génère le DotationRenoncementDto via les données reçues (MultipartHttpServletRequest).
     *
     * @param servletRequest
     *            the multipartHttpServletRequest
     * @return le dto DemandePaiementDto
     */
    private static DotationRenoncementDto genererDtoViaRequest(MultipartHttpServletRequest servletRequest) {

        DotationRenoncementDto dotationRenoncementDto = new DotationRenoncementDto();

        // mapping des données

        if (StringUtils.isNotBlank(servletRequest.getParameter("idSousProgramme"))) {
            dotationRenoncementDto.setIdSousProgramme(Integer.valueOf(servletRequest.getParameter("idSousProgramme")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("montant"))) {
            dotationRenoncementDto.setMontant(new BigDecimal(servletRequest.getParameter("montant")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("idCollectivite"))) {
            dotationRenoncementDto.setIdCollectivite(Long.valueOf(servletRequest.getParameter("idCollectivite")));
        }

        if (StringUtils.isNotBlank(servletRequest.getParameter("dotationDisponible"))) {
            dotationRenoncementDto.setDotationDisponible(new BigDecimal(servletRequest.getParameter("dotationDisponible")));
        }

        // mapping du fichier en pièce jointe 'RENONCEMENT_MONTANT_DOTATION' uniquement
        MultipartFile fichierUpload = servletRequest.getFile(TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION.getCode());
        if (fichierUpload != null) {
            FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichierUpload);
            dotationRenoncementDto.setFichierEnvoyer(fichierTransfert);
        }

        return dotationRenoncementDto;
    }
}

package fr.gouv.sitde.face.webapp.controller.connexion;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.sitde.face.application.dto.DroitAgentCollectiviteDto;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

/**
 * The Class UtilisateurConnecteDto.
 */
public class UtilisateurConnecteDto extends UtilisateurAdminDto {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2929185538367882691L;

    /**
     * Liste des Roles de l'utilisateur.
     */
    private List<RoleEnum> listeRoles = new ArrayList<>(0);

    /**
     * @return the listeRoles
     */
    public List<RoleEnum> getListeRoles() {
        return this.listeRoles;
    }

    /**
     * @param listeRoles
     *            the listeRoles to set
     */
    public void setListeRoles(List<RoleEnum> listeRoles) {
        this.listeRoles = listeRoles;
    }

    /**
     * Renvoie true si l'utilisateur a des droits d'admin sur la collectivité dont l'ID est passé en paramètre.
     * <p>
     * Remarque : renvoie FALSE si l'idCollectivite en paramètre vaut null.
     * </p>
     *
     * @param idCollectivite
     *            the id collectivite
     * @return true, if is admin collectivite
     */
    public boolean estAdminCollectivite(Long idCollectivite) {

        if (idCollectivite == null) {
            return false;
        }

        for (DroitAgentCollectiviteDto droitAgentCollectivite : this.getDroitsAgentCollectivites()) {
            if (droitAgentCollectivite.getIdCollectivite().equals(idCollectivite) && droitAgentCollectivite.isAdmin()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Renvoie true si l'utilisateur a des droits d'agent sur la collectivité dont l'ID est passé en paramètre.
     * <p>
     * Remarque : renvoie FALSE si l'idCollectivite en paramètre vaut null.
     * </p>
     *
     * @param idCollectivite
     *            the id collectivite
     * @return true, if is agent collectivite
     */
    public boolean estAgentCollectivite(Long idCollectivite) {

        if (idCollectivite == null) {
            return false;
        }

        for (DroitAgentCollectiviteDto droitAgentCollectivite : this.getDroitsAgentCollectivites()) {
            if (droitAgentCollectivite.getIdCollectivite().equals(idCollectivite)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Est uniquement generique.
     *
     * @return true, if successful
     */
    public boolean estUniquementGenerique() {
        return this.getListeRoles().contains(RoleEnum.GENERIQUE) && (this.getListeRoles().size() == 1);
    }

    /**
     * Est MFER.
     *
     * @return true, if successful
     */
    public boolean estMfer() {
        return this.getListeRoles().contains(RoleEnum.MFER_INSTRUCTEUR) || this.getListeRoles().contains(RoleEnum.MFER_RESPONSABLE);
    }

    /**
     * @return
     */
    public boolean estSD7() {
        return (this.getListeRoles().contains(RoleEnum.SD7_INSTRUCTEUR) || this.getListeRoles().contains(RoleEnum.SD7_RESPONSABLE))
                && (this.getListeRoles().size() == 1);
    }
}

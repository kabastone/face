/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class CerbereProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("cerbere")
@Validated
public class CerbereProperties {

    /** The application id. */
    /**
     * Identifiant de l'application.
     */
    @NotEmpty
    private String applicationId;

    /**
     * Jeton de session Cerbère sur protocole TLS uniquement : oui|non.
     */
    private String sessionSecure;

    /**
     * Interdire l'accès au jeton de session Cerbère par le code Javascript.
     */
    private String sessionHttpOnly;

    /**
     * Chemin complet du fichier référentiel XML, ou chemin relatif sous WEB-INF/ ou dans le classpath.
     */
    private String conf;

    /**
     * URL ignorées par cerbere.
     */
    private String urlsOuvertes;

    /**
     * Point d'entrée dans l'application.
     */
    private String applicationEntree;

    /** Propriétés spécifiques au bouchon. */
    private Bouchon bouchon;

    /**
     * The Class Bouchon.
     */
    public static class Bouchon {

        /**
         * Niveau de log de l'application (OFF|INFO|CONFIG|DEBUG|DEV).
         */
        private String logNiveau;

        /**
         * Emplacement du fichier de traces (ou stdout par défaut).
         */
        private String logFichier;

        /**
         * Clé de génération des jetons.
         */
        private String cle;

        /**
         * Type de challenge : 1=challenge fort, 2=challenge simplifié (clusters).
         */
        private String challengeType;

        /**
         * Emplacement du keystore pour simulation d'une authentification par certificat.
         */
        private String certKs;

        /** Mot de passe du keystore simulant une authentification par certificat. */
        private String certKsPasse;

        /**
         * Gets the log niveau.
         *
         * @return the logNiveau
         */
        public String getLogNiveau() {
            return this.logNiveau;
        }

        /**
         * Sets the log niveau.
         *
         * @param logNiveau
         *            the logNiveau to set
         */
        public void setLogNiveau(String logNiveau) {
            this.logNiveau = logNiveau;
        }

        /**
         * Gets the log fichier.
         *
         * @return the logFichier
         */
        public String getLogFichier() {
            return this.logFichier;
        }

        /**
         * Sets the log fichier.
         *
         * @param logFichier
         *            the logFichier to set
         */
        public void setLogFichier(String logFichier) {
            this.logFichier = logFichier;
        }

        /**
         * Gets the cle.
         *
         * @return the cle
         */
        public String getCle() {
            return this.cle;
        }

        /**
         * Sets the cle.
         *
         * @param cle
         *            the cle to set
         */
        public void setCle(String cle) {
            this.cle = cle;
        }

        /**
         * Gets the challenge type.
         *
         * @return the challengeType
         */
        public String getChallengeType() {
            return this.challengeType;
        }

        /**
         * Sets the challenge type.
         *
         * @param challengeType
         *            the challengeType to set
         */
        public void setChallengeType(String challengeType) {
            this.challengeType = challengeType;
        }

        /**
         * Gets the cert ks.
         *
         * @return the certKs
         */
        public String getCertKs() {
            return this.certKs;
        }

        /**
         * Sets the cert ks.
         *
         * @param certKs
         *            the certKs to set
         */
        public void setCertKs(String certKs) {
            this.certKs = certKs;
        }

        /**
         * Gets the cert ks passe.
         *
         * @return the certKsPasse
         */
        public String getCertKsPasse() {
            return this.certKsPasse;
        }

        /**
         * Sets the cert ks passe.
         *
         * @param certKsPasse
         *            the certKsPasse to set
         */
        public void setCertKsPasse(String certKsPasse) {
            this.certKsPasse = certKsPasse;
        }

    }

    /**
     * Gets the application id.
     *
     * @return the applicationId
     */
    public String getApplicationId() {
        return this.applicationId;
    }

    /**
     * Sets the application id.
     *
     * @param applicationId
     *            the applicationId to set
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * Gets the session secure.
     *
     * @return the sessionSecure
     */
    public String getSessionSecure() {
        return this.sessionSecure;
    }

    /**
     * Sets the session secure.
     *
     * @param sessionSecure
     *            the sessionSecure to set
     */
    public void setSessionSecure(String sessionSecure) {
        this.sessionSecure = sessionSecure;
    }

    /**
     * Gets the session httpOnly.
     *
     * @return the sessionHttpOnly
     */
    public String getSessionHttpOnly() {
        return this.sessionHttpOnly;
    }

    /**
     * Sets the session httpOnly.
     *
     * @param sessionHttpOnly
     *            the sessionHttpOnly to set
     */
    public void setSessionHttpOnly(String sessionHttpOnly) {
        this.sessionHttpOnly = sessionHttpOnly;
    }

    /**
     * Gets the conf.
     *
     * @return the conf
     */
    public String getConf() {
        return this.conf;
    }

    /**
     * Sets the conf.
     *
     * @param conf
     *            the conf to set
     */
    public void setConf(String conf) {
        this.conf = conf;
    }

    /**
     * Gets the urls ouvertes.
     *
     * @return the urlsOuvertes
     */
    public String getUrlsOuvertes() {
        return this.urlsOuvertes;
    }

    /**
     * Sets the urls ouvertes.
     *
     * @param urlsOuvertes
     *            the urlsOuvertes to set
     */
    public void setUrlsOuvertes(String urlsOuvertes) {
        this.urlsOuvertes = urlsOuvertes;
    }

    /**
     * Gets the application entree.
     *
     * @return the applicationEntree
     */
    public String getApplicationEntree() {
        return this.applicationEntree;
    }

    /**
     * Sets the application entree.
     *
     * @param applicationEntree
     *            the applicationEntree to set
     */
    public void setApplicationEntree(String applicationEntree) {
        this.applicationEntree = applicationEntree;
    }

    /**
     * Gets the bouchon.
     *
     * @return the bouchon
     */
    public Bouchon getBouchon() {
        if (this.bouchon == null) {
            return new Bouchon();
        }
        return this.bouchon;
    }

    /**
     * Sets the bouchon.
     *
     * @param bouchon
     *            the bouchon to set
     */
    public void setBouchon(Bouchon bouchon) {
        this.bouchon = bouchon;
    }

}

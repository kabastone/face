/**
 *
 */
package fr.gouv.sitde.face.webapp.security.configuration;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class JwtProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("jwt")
@Validated
public class JwtProperties {

    /** The token secret key. */
    @NotEmpty
    private String tokenSecretKey;

    /** The access token expire length. */
    @NotNull
    private Long accessTokenExpireLength;

    /** The refresh token expire length. */
    @NotNull
    private Long refreshTokenExpireLength;

    /** The total tokens expire length. */
    @NotNull
    private Long totalTokensExpireLength;

    /** The https obligatoire. */
    @NotNull
    private Boolean httpsObligatoire;

    /**
     * Gets the token secret key.
     *
     * @return the tokenSecretKey
     */
    public String getTokenSecretKey() {
        return this.tokenSecretKey;
    }

    /**
     * Sets the token secret key.
     *
     * @param tokenSecretKey
     *            the tokenSecretKey to set
     */
    public void setTokenSecretKey(String tokenSecretKey) {
        this.tokenSecretKey = tokenSecretKey;
    }

    /**
     * Gets the access token expire length in milliseconds.
     *
     * @return the accessTokenExpireLength
     */
    public Long getAccessTokenExpireLength() {
        return this.accessTokenExpireLength;
    }

    /**
     * Sets the access token expire length.
     *
     * @param accessTokenExpireLength
     *            the accessTokenExpireLength to set
     */
    public void setAccessTokenExpireLength(Long accessTokenExpireLength) {
        this.accessTokenExpireLength = accessTokenExpireLength;
    }

    /**
     * Gets the refresh token expire length in milliseconds.
     *
     * @return the refreshTokenExpireLength
     */
    public Long getRefreshTokenExpireLength() {
        return this.refreshTokenExpireLength;
    }

    /**
     * Sets the refresh token expire length.
     *
     * @param refreshTokenExpireLength
     *            the refreshTokenExpireLength to set
     */
    public void setRefreshTokenExpireLength(Long refreshTokenExpireLength) {
        this.refreshTokenExpireLength = refreshTokenExpireLength;
    }

    /**
     * Gets the total tokens expire length in milliseconds.
     *
     * @return the totalTokensExpireLength
     */
    public Long getTotalTokensExpireLength() {
        return this.totalTokensExpireLength;
    }

    /**
     * Sets the total tokens expire length.
     *
     * @param totalTokensExpireLength
     *            the totalTokensExpireLength to set
     */
    public void setTotalTokensExpireLength(Long totalTokensExpireLength) {
        this.totalTokensExpireLength = totalTokensExpireLength;
    }

    /**
     * Gets the https obligatoire.
     *
     * @return the httpsObligatoire
     */
    public Boolean getHttpsObligatoire() {
        return this.httpsObligatoire;
    }

    /**
     * Sets the https obligatoire.
     *
     * @param httpsObligatoire
     *            the httpsObligatoire to set
     */
    public void setHttpsObligatoire(Boolean httpsObligatoire) {
        this.httpsObligatoire = httpsObligatoire;
    }

}

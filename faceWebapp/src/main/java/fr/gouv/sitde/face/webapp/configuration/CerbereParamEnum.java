/**
 *
 */
package fr.gouv.sitde.face.webapp.configuration;

/**
 * The Enum CerbereParamEnum.
 *
 * @author a453029
 */
public enum CerbereParamEnum {

    /** The application id. */
    APPLICATION_ID("applicationId"),

    /** The session secure. */
    SESSION_SECURE("session.secure"),

    /** The session http only. */
    SESSION_HTTP_ONLY("session.httponly"),

    /** The conf. */
    CONF("conf"),

    /** The urls ouvertes. */
    URLS_OUVERTES("urls-ouvertes"),

    /** The application entree. */
    APPLICATION_ENTREE("applicationEntree"),

    /** The bouchon log niveau. */
    BOUCHON_LOG_NIVEAU("log.niveau"),

    /** The bouchon log fichier. */
    BOUCHON_LOG_FICHIER("log.fichier"),

    /** The bouchon cle. */
    BOUCHON_CLE("cle"),

    /** The bouchon challenge type. */
    BOUCHON_CHALLENGE_TYPE("challenge.type"),

    /** The bouchon cert ks. */
    BOUCHON_CERT_KS("bouchon.cert.ks"),

    /** The bouchon cert ks passe. */
    BOUCHON_CERT_KS_PASSE("bouchon.cert.ks.passe");

    /** The nom. */
    private String nom;

    /**
     * Instantiates a new CerbereParamenum.
     *
     * @param nom
     *            the nom
     */
    CerbereParamEnum(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

}

package fr.gouv.sitde.face.webapp.security.configuration;

import javax.inject.Inject;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import fr.gouv.sitde.face.webapp.security.JwtTokenFilterConfigurer;
import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@EnableWebSecurity
@Profile({ "prod", "dev", "devang" })
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private JwtTokenProvider jwtTokenProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                // Protection de l'API rest
                .antMatchers("/api/**").hasRole(RoleEnum.GENERIQUE.getLibelleCourt())
                // Autorisation de toutes les autres URL
                .anyRequest().permitAll();

        // Application du filtre JWT
        http.apply(new JwtTokenFilterConfigurer(this.jwtTokenProvider));
    }
}

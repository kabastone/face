package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionCollectiviteApplication;
import fr.gouv.sitde.face.application.dto.CollectiviteDetailDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class CollectiviteRestController.
 */
@RestController
@RequestMapping("/api")
public class CollectiviteRestController extends ConnexionAwareController {

    /** The gestion collectivite application. */
    @Inject
    private GestionCollectiviteApplication gestionCollectiviteApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher collectivite par id.
     *
     * @param id
     *            the id
     * @return the string json de l'utilisateur
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{id:[0-9]+}/rechercher", method = RequestMethod.GET)
    public String rechercherCollectivite(HttpServletRequest request, @PathVariable(value = "id", required = true) Long id) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), id);

        CollectiviteDetailDto collectivite = this.gestionCollectiviteApplication.rechercherCollectiviteDetail(id);
        return JsonMapper.getJsonFromData(collectivite);
    }

    /**
     * Modifier collectivite.
     *
     * @param id
     *            the id
     * @param collectiviteDto
     *            the collectivite dto
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{id:[0-9]+}/modifier", method = RequestMethod.POST)
    public String modifierCollectivite(HttpServletRequest request, @PathVariable(value = "id", required = true) Long id,
            @RequestBody CollectiviteDetailDto collectiviteDto) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferEtAgent(this.getUtilisateurConnecte(request), id);

        CollectiviteDetailDto collectivite = this.gestionCollectiviteApplication.modifierCollectivite(collectiviteDto);
        return JsonMapper.getJsonFromData(collectivite);
    }

    /**
     * Fermer collectivite.
     *
     * @param id
     *            the id
     * @param collectiviteDto
     *            the collectivite dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/{id:[0-9]+}/fermer", method = RequestMethod.POST)
    public String fermerCollectivite(HttpServletRequest request, @PathVariable(value = "id", required = true) Long id,
            @RequestBody CollectiviteDetailDto collectiviteDto) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferEtAgent(this.getUtilisateurConnecte(request), id);

        CollectiviteDetailDto collectivite = this.gestionCollectiviteApplication.modifierCollectivite(collectiviteDto);
        return JsonMapper.getJsonFromData(collectivite);
    }

    /**
     * Modifier collectivite.
     *
     * @param id
     *            the id
     * @param collectiviteDto
     *            the collectivite dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/collectivite/creer", method = RequestMethod.POST)
    public String creerCollectivite(@RequestBody CollectiviteDetailDto collectiviteDto) {

        CollectiviteDetailDto collectivite = this.gestionCollectiviteApplication.creerCollectivite(collectiviteDto);
        return JsonMapper.getJsonFromData(collectivite);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.webapp.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;
import i2.application.cerbere.commun.Cerbere;
import i2.application.cerbere.commun.CerbereConnexionException;
import i2.application.cerbere.commun.CerbereException;
import i2.application.cerbere.commun.Habilitation;
import i2.application.cerbere.commun.Profil;
import i2.application.cerbere.commun.Utilisateur;

/**
 * Classe de service qui permet de récupérer des informations sur l'utilisateur authentifié sur Cerbere ou de déconnecter un utilisateur Cerbere via
 * l'API CERBERE.
 *
 * @author Atos
 *
 */
public final class CerbereService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CerbereService.class);

    /**
     * Identifiant de l'attribut nom de l'objet Utilisateur.
     */
    private static final String NOM_CLE = "NOM";

    /**
     * Identifiant de l'attribut prenom de l'objet Utilisateur.
     */
    private static final String PRENOM_CLE = "PRENOM";

    /**
     * Identifiant de l'attribut email de l'objet Utilisateur.
     */
    private static final String EMAIL_CLE = "MEL";

    /**
     * Cette classe ne contient que des membres statiques et n'a pas à être instanciée.
     */
    private CerbereService() {

    }

    /**
     * Construit l'utilisateur qui sera stocké dans le token à partir des informations Cerbere.
     *
     * @param request
     *            servletRequest
     * @return le bean UtilisateurToken authentifié ou vide (mais pas null).
     */
    public static UtilisateurToken construireUtilisateurToken(final HttpServletRequest request) {
        UtilisateurToken utilisateurToken = new UtilisateurToken();
        Cerbere cerbere = getCerbereService(request);

        if (cerbere != null) {

            try {
                // recuperation de l'objet Cerbere Utilisateur
                Utilisateur utilisateurCerbere = null;
                utilisateurCerbere = cerbere.getUtilisateur();

                // remplissage de l'objet UtilisateurCerbere
                utilisateurToken.setIdCerbere(utilisateurCerbere.getIdentifiant());
                utilisateurToken.setNom(utilisateurCerbere.getValeur(NOM_CLE));
                utilisateurToken.setPrenom(utilisateurCerbere.getValeur(PRENOM_CLE));
                utilisateurToken.setEmail(utilisateurCerbere.getValeur(EMAIL_CLE));

                // Remplissage de la liste de profils de l'utilisateur
                Habilitation habilitation = cerbere.getHabilitation();
                List<Profil> listeProfilsCerbere = habilitation.getTousProfils();

                for (Profil profilCerbere : listeProfilsCerbere) {
                    if (!RoleEnum.roleExists(profilCerbere.getNom())) {
                        LOGGER.error("Le role d'utilisateur {} n'existe pas", profilCerbere.getNom());
                    }
                    utilisateurToken.getListeRoles().add(RoleEnum.rechercherRoleParLibelleCourt(profilCerbere.getNom()));

                }

            } catch (CerbereException cue) {
                LOGGER.error("Impossible de récupérer une propriété utilisateur Cerbere ", cue);
            }
        }

        return utilisateurToken;
    }

    /**
     * Déconnecte l'utilisateur via l'API Cerbere.
     *
     * @param request
     *            servletRequest
     * @param response
     *            servletResponse
     */
    public static void deconnecterUtilisateur(final HttpServletRequest request, final HttpServletResponse response) {

        if (request != null) {

            try {
                Cerbere cerbere = getCerbereService(request);

                if (cerbere != null) {
                    cerbere.logoff(request, response);
                }

            } catch (CerbereException cue) {
                LOGGER.warn("Impossible de déconnecter l'utilisateur par Cerbere ", cue);
            }
        }
    }

    /**
     * Récupère l'objet Cerbere de l'API Cerbere.
     *
     * @param request
     *            servletRequest
     * @return Le service Cerbere
     */
    private static Cerbere getCerbereService(final HttpServletRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Méthode getCerbereService");
        }

        Cerbere cerbere = null;
        try {
            cerbere = Cerbere.creation(request);

        } catch (CerbereConnexionException cce) {
            LOGGER.info("Impossible de récupérer l'objet Cerbere. " + cce.getLocalizedMessage(), cce);
        } catch (RuntimeException e) {
            LOGGER.info("Impossible de récupérer les informations CERBERE. " + e.getLocalizedMessage(), e);
        }

        return cerbere;
    }
}

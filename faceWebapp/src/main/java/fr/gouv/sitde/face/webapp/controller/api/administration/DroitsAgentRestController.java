/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionDroitAgentApplication;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * DroitsAgentRestController : contrôleur REST des droits des agents sur une collectitivté.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class DroitsAgentRestController extends ConnexionAwareController {

    /** The gestion droit utilisateur application. */
    @Inject
    private GestionDroitAgentApplication gestionDroitAgentApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Rechercher les utilisateurs d'une collectivité.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/agents/rechercher", method = RequestMethod.GET)
    public String rechercherAgentsParCollectivite(HttpServletRequest request,
            @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {
        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        return JsonMapper.getJsonFromData(this.gestionDroitAgentApplication.rechercherAgentsParCollectivite(idCollectivite));
    }

    /**
     * Ajoute l'utilisateur sur la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/agent/{idUtilisateur:[0-9]+}/ajouter", method = RequestMethod.GET)
    public String ajouterDroitAgent(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "idUtilisateur", required = true) Long idUtilisateur) {

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), idCollectivite);

        return JsonMapper.getJsonFromData(this.gestionDroitAgentApplication.ajouterDroitAgent(idCollectivite, idUtilisateur));
    }

    /**
     * Change l'état d'un utilisateur (agent/admin) sur la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/agent/{idUtilisateur:[0-9]+}/changer-etat", method = RequestMethod.GET)
    public String changerEtatAdminDroitAgent(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "idUtilisateur", required = true) Long idUtilisateur) {

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), idCollectivite);

        return JsonMapper.getJsonFromData(this.gestionDroitAgentApplication.changerEtatAdminDroitAgent(idCollectivite, idUtilisateur));
    }

    /**
     * Supprime un utilisateur d'une collectivité.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/agent/{idUtilisateur:[0-9]+}/supprimer", method = RequestMethod.DELETE)
    public String supprimerDroitAgent(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable(value = "idUtilisateur", required = true) Long idUtilisateur) {

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), idCollectivite);

        this.gestionDroitAgentApplication.supprimerDroitAgent(idCollectivite, idUtilisateur);

        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }
}

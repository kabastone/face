/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.subvention;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import fr.gouv.sitde.face.application.dto.ImportLigneDotationDto;
import fr.gouv.sitde.face.application.subvention.ImportDotationApplication;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * ImportDotationRestController : controlleur REST d'import de fichier CSV des dotations departementales.
 *
 * @author Atos
 */

@RestController
@RequestMapping("/api")
public class ImportDotationRestController extends ConnexionAwareController {

    /** Import Dotation application. */
    @Inject
    private ImportDotationApplication importDossierDotationApplication;

    /**
     * Importer dossier dotation.
     *
     * @param importDotationFile
     *            the import dossier dotation file
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/subvention/dossier/importer", method = RequestMethod.POST)
    public String importerDotation(@RequestParam("importDotationFile") MultipartFile importDotationFile) {

        List<ImportLigneDotationDto> listeImportDotationDto = null;
        /** CsvToBean */
        CsvToBean<ImportLigneDotationDto> csvToBean;

        ModelUploadUtils.validerFichierCsvUploaded(importDotationFile);

        try (InputStream is = importDotationFile.getInputStream()) {
            CsvToBeanBuilder<ImportLigneDotationDto> csvToBeanBuilder = new CsvToBeanBuilder<ImportLigneDotationDto>(
                    new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1))).withType(ImportLigneDotationDto.class)
                            .withSeparator(',').withQuoteChar('\"').withThrowExceptions(false).withOrderedResults(false)
                            .withErrorLocale(Locale.FRANCE);

            csvToBean = csvToBeanBuilder.build();

            listeImportDotationDto = csvToBean.parse();

            if (CollectionUtils.isEmpty(listeImportDotationDto)) {
                /** @see /faceDomaineService/src/main/resources/ValidationMessages.properties */
                throw new RegleGestionException("subvention.import.fichierVide");
            } else {
                this.importDossierDotationApplication.importerDossierDotation(listeImportDotationDto);
            }

        } catch (IOException e) {
            /** @see /faceDomaineService/src/main/resources/ValidationMessages.properties */
            throw new TechniqueException(e);
        }

        /** @see /faceDomaineService/src/main/resources/ValidationMessages.properties */
        return JsonMapper.getJsonFromData("subvention.import.succes");
    }

    /**
     * Verifier dotation annuelle notifiee true : validation OK, false : validation KO.
     *
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @RequestMapping(value = "/subvention/dossier/verifier", method = RequestMethod.POST)
    public String verifierDotationAnnuelleNotifiee() {

        Boolean result = this.importDossierDotationApplication.verifierDotationAnnuelleNotifiee();

        return JsonMapper.getJsonFromData(result);
    }

}

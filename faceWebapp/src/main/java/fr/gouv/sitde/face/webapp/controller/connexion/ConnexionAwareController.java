/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.connexion;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.application.administration.GestionUtilisateurApplication;
import fr.gouv.sitde.face.application.dto.UtilisateurAdminDto;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.webapp.security.JwtTokenProvider;
import fr.gouv.sitde.face.webapp.security.model.UtilisateurToken;

/**
 * Classe faite pour être entendue par d'autres controllers afin de fournir des informations sur l'utilisateur connecté.
 *
 * @author Atos
 *
 */
@Component
public class ConnexionAwareController {

    /** The jwt token provider. */
    @Inject
    private JwtTokenProvider jwtTokenProvider;

    /** The gestion utilisateur application. */
    @Inject
    private GestionUtilisateurApplication gestionUtilisateurApplication;

    /**
     * Constructeur protected ne devant être appelé que par un super des classes filles.
     */
    protected ConnexionAwareController() {
        // Constructeur protected.
    }

    /**
     * Construire utilisateur connecte.
     *
     * @param utilisateurAdminDto
     *            the utilisateur admin dto
     * @param listeRoles
     *            the liste roles
     * @return the utilisateur connecte
     */
    private static UtilisateurConnecteDto construireUtilisateurConnecte(UtilisateurAdminDto utilisateurAdminDto, UtilisateurToken utilisateurToken) {

        UtilisateurConnecteDto utilisateurConnecte = new UtilisateurConnecteDto();

        if ((utilisateurAdminDto != null) && utilisateurToken.isAuthentifie()) {
            utilisateurConnecte.setCerbereId(utilisateurAdminDto.getCerbereId());
            utilisateurConnecte.setEmail(utilisateurAdminDto.getEmail());
            utilisateurConnecte.setId(utilisateurAdminDto.getId());
            utilisateurConnecte.setNom(utilisateurAdminDto.getNom());
            utilisateurConnecte.setPrenom(utilisateurAdminDto.getPrenom());
            utilisateurConnecte.setTelephone(utilisateurAdminDto.getTelephone());
            utilisateurConnecte.setVersion(utilisateurAdminDto.getVersion());
            utilisateurConnecte.setDroitsAgentCollectivites(utilisateurAdminDto.getDroitsAgentCollectivites());
            utilisateurConnecte.setListeRoles(utilisateurToken.getListeRoles());
        }

        return utilisateurConnecte;
    }

    /**
     * Gets the utilisateur connecte.
     *
     * @param request
     *            the request
     * @return the utilisateur connecte
     */
    protected UtilisateurConnecteDto getUtilisateurConnecte(HttpServletRequest request) {
        UtilisateurToken utilisateurToken = this.jwtTokenProvider.getUtilisateurTokenFromRequest(request);
        UtilisateurAdminDto utilisateurAdminDto = this.gestionUtilisateurApplication.rechercherUtilisateurParEmail(utilisateurToken.getEmail());

        return construireUtilisateurConnecte(utilisateurAdminDto, utilisateurToken);
    }

    /**
     * Valide que l'idUtilisateur en paramètre corresponde bien à celle de l'utilisateur connecté. <br/>
     * Dans le cas contraire, une TechniqueException est lancée.
     *
     * @param request
     *            the request
     * @param idUtilisateur
     *            the id utilisateur
     * @return true, if is utilisateur connecte
     */
    protected void validerEstUtilisateurConnecte(HttpServletRequest request, Long idUtilisateur) {
        UtilisateurConnecteDto utilisateurConnecteDto = this.getUtilisateurConnecte(request);

        if ((utilisateurConnecteDto == null) || (utilisateurConnecteDto.getId() == null) || !utilisateurConnecteDto.getId().equals(idUtilisateur)) {
            throw new TechniqueException("L'idUtilisateur ne correspond pas à celui de l'utilisateur connecté");
        }
    }

}

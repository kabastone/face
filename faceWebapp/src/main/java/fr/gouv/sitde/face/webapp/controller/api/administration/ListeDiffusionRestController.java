/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.administration;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.administration.GestionListeDiffusionApplication;
import fr.gouv.sitde.face.application.dto.AdresseEmailDto;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * ListeDiffusionRestController : contrôleur REST de gestion de la liste de diffusion.
 *
 * @author Atos
 */
@RestController
@RequestMapping("/api")
public class ListeDiffusionRestController extends ConnexionAwareController {

    /** The gestion liste diffusion application. */
    @Inject
    private GestionListeDiffusionApplication gestionListeDiffusionApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Pour la liste de diffusion : recherche toutes les adresses e-mail d'une collectivité.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/liste-diffusion/rechercher", method = RequestMethod.GET)
    public String rechercherAdressesEmail(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite) {

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesCollectivitePourMferSd7EtAgent(this.getUtilisateurConnecte(request), idCollectivite);

        return JsonMapper.getJsonFromData(this.gestionListeDiffusionApplication.rechercherAdressesEmail(idCollectivite));
    }

    /**
     * Supprimer une adresse e-mail.
     *
     * @param idAdresseEmail
     *            the id adresse email
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/liste-diffusion/{idAdresseEmail:[0-9]+}/supprimer",
            method = { RequestMethod.DELETE })
    public String supprimerAdresseEmail(HttpServletRequest request, @PathVariable(value = "idCollectivite", required = true) Long idCollectivite,
            @PathVariable("idAdresseEmail") Long idAdresseEmail) {

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), idCollectivite);

        // Suppression de l'utilisateur
        this.gestionListeDiffusionApplication.supprimerAdresseEmail(idAdresseEmail);

        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }

    /**
     * Ajout de l'e-mail à la liste de diffusion de la collectivité.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param adresseEmail
     *            the adresse email
     * @return the adresse email dto
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/collectivite/{idCollectivite:[0-9]+}/liste-diffusion/ajouter", method = { RequestMethod.POST })
    public AdresseEmailDto ajouterAdresseEmailCollectivite(HttpServletRequest request, @PathVariable("idCollectivite") Long idCollectivite,
            @RequestBody String adresseEmail) {

        // On valide que l'utilisateur connecté possède les droits admin sur la collectivité
        this.accesCollectiviteValidator.validerAccesAdminCollectivite(this.getUtilisateurConnecte(request), idCollectivite);

        return this.gestionListeDiffusionApplication.ajouterAdresseEmailCollectivite(adresseEmail, idCollectivite);
    }
}

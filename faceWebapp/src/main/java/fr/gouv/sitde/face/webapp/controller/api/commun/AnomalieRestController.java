package fr.gouv.sitde.face.webapp.controller.api.commun;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.sitde.face.application.commun.GestionAnomalieApplication;
import fr.gouv.sitde.face.application.dto.AnomalieDto;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.controller.connexion.UtilisateurConnecteDto;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class AnomalieRestController.
 */
@RestController
@RequestMapping("/api/anomalie")
public class AnomalieRestController extends ConnexionAwareController {

    /** The gestion anomalie application. */
    @Inject
    private GestionAnomalieApplication gestionAnomalieApplication;

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /**
     * Ajouter une nouvelle anomalie : pour une demande de paiement ou de subvention.
     *
     * @param anomalieDto
     *            the anomalie dto
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_SD7_RESPONSABLE,
            RoleEnum.Constantes.ROLE_SD7_INSTRUCTEUR })
    @RequestMapping(value = "/ajouter", method = RequestMethod.POST)
    public String ajouterAnomalie(@RequestBody AnomalieDto anomalieDto) {
        AnomalieDto anomalieDtoBdd = this.gestionAnomalieApplication.ajouterAnomalie(anomalieDto);
        return JsonMapper.getJsonFromData(anomalieDtoBdd);
    }

    /**
     * Motification d'une anomalie.
     *
     * @param idAnomalie
     *            the id anomalie
     * @param anomalieDto
     *            the anomalie dto
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/{idAnomalie}/modifier", method = RequestMethod.POST)
    public String modifierAnomalie(HttpServletRequest request, @PathVariable(value = "idAnomalie", required = true) Long idAnomalie,
            @RequestBody AnomalieDto anomalieDto) {

        // On valide que l'utilisateur connecté possède les droits d'agent
        if (anomalieDto.getIdDemandePaiement() != null) {
            // sur la collectivité de la demande de paiement
            this.accesCollectiviteValidator.validerAccesDemandePaiement(this.getUtilisateurConnecte(request), anomalieDto.getIdDemandePaiement());
        } else if (anomalieDto.getIdDemandeSubvention() != null) {
            // ou sur la collectivité de la demande de subvention
            this.accesCollectiviteValidator.validerAccesDemandeSubvention(this.getUtilisateurConnecte(request), anomalieDto.getIdDemandeSubvention());
        }

        AnomalieDto anomalieDtoBdd = this.gestionAnomalieApplication.updateAnomalie(anomalieDto);
        return JsonMapper.getJsonFromData(anomalieDtoBdd);
    }

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/subvention/{idDemandeSubvention}/rechercher", method = RequestMethod.GET)
    public String rechercherAnomaliesParIdDemandeSubvention(HttpServletRequest request,
            @PathVariable(value = "idDemandeSubvention", required = true) Long idDemandeSubvention) {

        UtilisateurConnecteDto utilisateurConnecte = this.getUtilisateurConnecte(request);
        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDemandeSubvention(utilisateurConnecte, idDemandeSubvention);
        if (utilisateurConnecte.estUniquementGenerique()) {
            List<AnomalieDto> anomaliesDtoBdd = this.gestionAnomalieApplication
                    .rechercherAnomaliesParIdDemandeSubventionPourAODE(idDemandeSubvention);
            return JsonMapper.getJsonFromData(anomaliesDtoBdd);
        } else {
            List<AnomalieDto> anomaliesDtoBdd = this.gestionAnomalieApplication.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention);
            return JsonMapper.getJsonFromData(anomaliesDtoBdd);
        }
    }

    /**
     * Rechercher anomalies par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/paiement/{idDemandePaiement}/rechercher", method = RequestMethod.GET)
    public String rechercherAnomaliesParIdDemandePaiement(HttpServletRequest request,
            @PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement) {

        UtilisateurConnecteDto utilisateurConnecte = this.getUtilisateurConnecte(request);

        // On valide que l'utilisateur connecté possède les droits d'agent sur la collectivité
        this.accesCollectiviteValidator.validerAccesDemandePaiement(utilisateurConnecte, idDemandePaiement);

        if (utilisateurConnecte.estUniquementGenerique()) {
            List<AnomalieDto> anomaliesDtoBdd = this.gestionAnomalieApplication.rechercherAnomaliesParIdDemandePaiementPourAODE(idDemandePaiement);
            return JsonMapper.getJsonFromData(anomaliesDtoBdd);
        } else {
            List<AnomalieDto> anomaliesDtoBdd = this.gestionAnomalieApplication.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement);
            return JsonMapper.getJsonFromData(anomaliesDtoBdd);
        }

    }

    /**
     * Pour une demande de paiement : mettre à jour la correction d'une anomalie.
     *
     * @param idAnomalie
     *            the id anomalie
     * @param idDemandePaiement
     *            the id demande paiement
     * @param corrige
     *            the corrige
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/{idAnomalie:[0-9]+}/paiement/{idDemandePaiement:[0-9]+}/maj-correction", method = RequestMethod.POST)
    public String majCorrectionAnomaliePourPaiement(@PathVariable(value = "idAnomalie", required = true) Long idAnomalie,
            @PathVariable(value = "idDemandePaiement", required = true) Long idDemandePaiement, @RequestBody boolean corrige) {
        AnomalieDto anomalieDto = this.gestionAnomalieApplication.majCorrectionAnomaliePourPaiement(idDemandePaiement, idAnomalie, corrige);
        return JsonMapper.getJsonFromData(anomalieDto);
    }

    /**
     * Pour une demande de subvention : mettre à jour la correction d'une anomalie.
     *
     * @param idAnomalie
     *            the id anomalie
     * @param idDemandePaiement
     *            the id demande paiement
     * @param corrige
     *            the corrige
     * @return the string
     */
    @RolesAllowed(RoleEnum.Constantes.ROLE_GENERIQUE)
    @RequestMapping(value = "/{idAnomalie:[0-9]+}/subvention/{idDemandeSubvention:[0-9]+}/maj-correction", method = RequestMethod.POST)
    public String majCorrectionAnomaliePourSubvention(@PathVariable(value = "idAnomalie", required = true) Long idAnomalie,
            @PathVariable(value = "idDemandeSubvention", required = true) Long idDemandePaiement, @RequestBody boolean corrige) {
        AnomalieDto anomalieDto = this.gestionAnomalieApplication.majCorrectionAnomaliePourSubvention(idDemandePaiement, idAnomalie, corrige);
        return JsonMapper.getJsonFromData(anomalieDto);
    }
}

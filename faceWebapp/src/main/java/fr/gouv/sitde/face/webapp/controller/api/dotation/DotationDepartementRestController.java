/**
 *
 */
package fr.gouv.sitde.face.webapp.controller.api.dotation;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.gouv.sitde.face.application.document.GestionDocumentApplication;
import fr.gouv.sitde.face.application.dotation.GestionDotationDepartementApplication;
import fr.gouv.sitde.face.application.dto.DotationDepartementaleDto;
import fr.gouv.sitde.face.application.dto.LigneDotationDepartementaleDto;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.webapp.controller.connexion.ConnexionAwareController;
import fr.gouv.sitde.face.webapp.security.AccesCollectiviteValidator;
import fr.gouv.sitde.face.webapp.security.model.RoleEnum;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelDownloadUtils;
import fr.gouv.sitde.face.webapp.utils.filetransfert.ModelUploadUtils;
import fr.gouv.sitde.face.webapp.utils.jsonutils.JsonMapper;

/**
 * The Class DotationDepartementRestController.
 */
@RestController
@RequestMapping(value = "/api")
public class DotationDepartementRestController extends ConnexionAwareController {

    /** The acces collectivite validator. */
    @Inject
    private AccesCollectiviteValidator accesCollectiviteValidator;

    /** The gestion dotation departement application. */
    @Inject
    private GestionDotationDepartementApplication gestionDotationDepartementApplication;

    @Inject
    private GestionDocumentApplication gestionDocumentApplication;

    /**
     * Generer annexe dotation pdf par id.
     *
     * @param idDepartement
     *            the id departement
     * @param response
     *            the response
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_RESPONSABLE, RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR })
    @GetMapping(value = "/dotation/{idDepartement:[0-9]+}/generation")
    public void genererAnnexeDotationPdfParId(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            HttpServletResponse response) {
        ModelDownloadUtils.telechargerFichierFromFichierTransfert(this.gestionDotationDepartementApplication.genererAnnexeDotationPdf(idDepartement),
                response);
    }

    /**
     * Generer annexe dotation pdf pour tous les départements.
     *
     * @param response
     *            the response
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/dotation/departement/generation", method = RequestMethod.GET)
    public void genererAnnexeDotationPdf(HttpServletResponse response) {
        ModelDownloadUtils.telechargerFichierFromFichierTransfert(this.gestionDotationDepartementApplication.genererAnnexeDotationPdf(), response);
    }

    /**
     * Televerser et notifier dotations.
     *
     * @param idDepartement
     *            the id departement
     * @param response
     *            the response
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/dotation/{idDepartement:[0-9]+}/televerser", method = RequestMethod.POST,
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String televerserEtNotifierDotations(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            @RequestParam(value = "fichier", required = true) MultipartFile fichier) {

        FichierTransfert fichierTransfert = ModelUploadUtils.getFichierTransfereFromMultiPartFile(fichier);
        fichierTransfert.setTypeDocument(TypeDocumentEnum.COURRIER_DOTATION);

        this.gestionDotationDepartementApplication.televerserEtNotifierDotations(idDepartement, fichierTransfert);
        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }

    /**
     * Rechercher dotation departementales par annee en cours.
     *
     * @return la liste des dotations par département
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @RequestMapping(value = "/dotation/departement/rechercher", method = RequestMethod.POST)
    public String rechercherDotationsDepartementalesParAnneeEnCours(@RequestBody PageDemande pageDemande) {
        PageReponse<DotationDepartementaleDto> listeDotationsDepartementales = this.gestionDotationDepartementApplication
                .rechercherDotationsDepartementalesParAnneeEnCours(pageDemande);
        return JsonMapper.getJsonFromData(listeDotationsDepartementales);
    }

    /**
     * Lister les lignes de dotation départementale notifiées pour l'année en cours.
     *
     * @param idDepartement
     *            the id departement
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @GetMapping(value = "/dotation/departement/rechercher/lignes/notifiees/{idDepartement:[0-9]+}")
    public String rechercherLignesDotationDepartementaleNotifiees(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            HttpServletResponse response) {
        List<LigneDotationDepartementaleDto> lignesDotationDepartementale = this.gestionDotationDepartementApplication
                .rechercherLignesDotationDepartementaleNotifiees(idDepartement);
        return JsonMapper.getJsonFromData(lignesDotationDepartementale);
    }

    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @GetMapping(value = "/dotation/departement/rechercher/document/lignes/notifiees/{idLigne:[0-9]+}")
    public void rechercherDocumentNotification(HttpServletRequest request, @PathVariable(value = "idLigne", required = true) Long idLigne,
            HttpServletResponse response) {
        Document doc = this.gestionDotationDepartementApplication.rechercherDocumentNotification(idLigne);
        this.accesCollectiviteValidator.validerAccesDocument(this.getUtilisateurConnecte(request), doc.getId());

        ModelDownloadUtils.telechargerFichierFromFile(this.gestionDocumentApplication.telechargerDocument(doc.getId()), response);
    }

    /**
     * Lister les lignes de dotation départementale en préparation pour l'année en cours.
     *
     * @param idDepartement
     *            the id departement
     * @return the string
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @GetMapping(value = "/dotation/departement/rechercher/lignes/enPreparation/{idDepartement:[0-9]+}")
    public String rechercherLignesDotationDepartementaleEnPreparation(@PathVariable(value = "idDepartement", required = true) Integer idDepartement,
            HttpServletResponse response) {
        List<LigneDotationDepartementaleDto> lignesDotationDepartementale = this.gestionDotationDepartementApplication
                .rechercherLignesDotationDepartementaleEnPreparation(idDepartement);
        return JsonMapper.getJsonFromData(lignesDotationDepartementale);
    }

    /**
     * Supprimer une ligne de dotation départementale.
     *
     * @param idLigneDotationDepartementale
     *            the id of ligne de dotation départementale
     * @return
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @DeleteMapping(value = "/dotation/departement/lignes/{idLigneDotationDepartementale:[0-9]+}")
    public String detruireLigneDotationDepartementale(
            @PathVariable(value = "idLigneDotationDepartementale", required = true) Long idLigneDotationDepartementale) {
        this.gestionDotationDepartementApplication.detruireLigneDotationDepartementale(idLigneDotationDepartementale);
        // retour avec succès
        return ConstantesFace.STATUS_OK;
    }

    /**
     * Ajouter une ligne de dotation départementale.
     *
     * @param ligneDotationDepartementaleDto
     * @return
     */
    @RolesAllowed({ RoleEnum.Constantes.ROLE_MFER_INSTRUCTEUR, RoleEnum.Constantes.ROLE_MFER_RESPONSABLE })
    @PostMapping(value = "/dotation/departement/lignes")
    public String ajouterLigneDotationDepartementale(@RequestBody LigneDotationDepartementaleDto ligneDotationDepartementaleDto) {
        LigneDotationDepartementaleDto result = this.gestionDotationDepartementApplication.ajouterLigneDotationDepartementale(ligneDotationDepartementaleDto);
        return JsonMapper.getJsonFromData(result);
    }

}

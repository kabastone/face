/**
 *
 */
package fr.gouv.sitde.face.clientchorus.service;

import java.util.List;

import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;

/**
 * The Interface ClientChorusWs.
 *
 * @author Atos
 */
public interface ClientChorusWs {

    /**
     * Recherche collectivités par le WS Chorus.
     *
     * @return La collectivité recherchée.
     */
    List<CollectiviteChorus> rechercherCollectivites(String codeSociete, String idFonctionnel, String idTechnique,
            String organisationAchatFournisseur, String domaineCommercialeClient);
}

package fr.gouv.sitde.face.clientchorus.service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ammws2.wschorus.CodeSociete8F55E412ComplexType;
import ammws2.wschorus.DC5058D5B7ComplexType;
import ammws2.wschorus.Donnees09807E94ComplexType;
import ammws2.wschorus.IdFonctionnel52763Bd2ComplexType;
import ammws2.wschorus.IdTechnique3364Ce0CComplexType;
import ammws2.wschorus.OA408220D8ComplexType;
import ammws2.wschorus.ObjectFactory;
import ammws2.wschorus.RequeteAMMWS2282F6AbeComplexType;
import ammws2.wschorus.ResultatAMMWS2C86Cadc1ComplexType;
import ammws2.wschorus.Statut6Cb9Aa2CComplexType;
import ammws2.wschorus.WSWSP0117AWebS2AMM;
import ammws2.wschorus.WSWSP0117AWebS2AMMPortType;
import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;
import fr.gouv.sitde.face.transverse.chorus.WsChorusCodesErreursEnum;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;

/**
 * The Class ClientChorusWsImpl.
 *
 * @author Atos
 */
@Named(value = "clientChorusWs")
public class ClientChorusWsImpl implements ClientChorusWs {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientChorusWsImpl.class);

    /** The Constant FIN_URL_WSDL. */
    private static final String FIN_URL_WSDL = "?wsdl";

    /** The Constant RETOUR_DONNEES_OK. */
    private static final String RETOUR_DONNEES_OK = "OK";

    /** The chorus web service properties. */
    @Inject
    private ChorusWebServiceProperties chorusWebServiceProperties;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sit3p.revtc.clientpayzen.service.ClientPayzenWs#rechercherCollectivite(java.lang.Long)
     */
    @Override
    public List<CollectiviteChorus> rechercherCollectivites(String codeSociete, String idFonctionnel, String idTechnique,
            String organisationAchatFournisseur, String domaineCommercialeClient) {

        // RG1 : Au moins un des champs doit être renseigné (idFonctionnel & idTechnique).
        // Si ce n'est pas le cas le message d'erreur ECF_09 sera renvoyé.
        if ((idFonctionnel == null) && (idTechnique == null)) {
            throw new RegleGestionException(WsChorusCodesErreursEnum.ECF_09.getCode());
        }

        // RG2 : L'un ou l'autre des champs organisationAchatFournisseur ou domaineCommercialeClient peut être renseigné, mais pas les deux.
        // Si ce n'est pas le cas le message d'erreur ECF_08 sera renvoyé.
        if ((organisationAchatFournisseur != null) && (domaineCommercialeClient != null)) {
            throw new RegleGestionException(WsChorusCodesErreursEnum.ECF_08.getCode());
        }

        ObjectFactory objectFactory = new ObjectFactory();
        RequeteAMMWS2282F6AbeComplexType requeteComplexType = new RequeteAMMWS2282F6AbeComplexType();

        if (!StringUtils.isEmpty(codeSociete)) {
            // Création de l'objet codeSociete
            CodeSociete8F55E412ComplexType codeSocieteComplexType = objectFactory.createCodeSociete8F55E412ComplexType();
            codeSocieteComplexType.setValue(codeSociete);
            JAXBElement<CodeSociete8F55E412ComplexType> requeteComplexTypeCodeSociete = objectFactory
                    .createRequeteAMMWS2282F6AbeComplexTypeCodeSociete(codeSocieteComplexType);

            requeteComplexType.setCodeSociete(requeteComplexTypeCodeSociete);
        }

        if (!StringUtils.isEmpty(idFonctionnel)) {
            // Création de l'objet idFonctionnel
            IdFonctionnel52763Bd2ComplexType idFonctionnelComplexType = objectFactory.createIdFonctionnel52763Bd2ComplexType();
            idFonctionnelComplexType.setValue(idFonctionnel);
            JAXBElement<IdFonctionnel52763Bd2ComplexType> requeteComplexTypeIdFonctionnel = objectFactory
                    .createRequeteAMMWS2282F6AbeComplexTypeIdFonctionnel(idFonctionnelComplexType);

            requeteComplexType.setIdFonctionnel(requeteComplexTypeIdFonctionnel);
        }

        if (!StringUtils.isEmpty(idTechnique)) {
            // Création de l'objet idTechnique
            IdTechnique3364Ce0CComplexType idTechniqueComplexType = objectFactory.createIdTechnique3364Ce0CComplexType();
            idTechniqueComplexType.setValue(idTechnique);
            JAXBElement<IdTechnique3364Ce0CComplexType> requeteComplexTypeIdTechnique = objectFactory
                    .createRequeteAMMWS2282F6AbeComplexTypeIdTechnique(idTechniqueComplexType);

            requeteComplexType.setIdTechnique(requeteComplexTypeIdTechnique);
        }

        if (!StringUtils.isEmpty(organisationAchatFournisseur)) {
            // Création de l'objet organisationAchatFournisseur
            OA408220D8ComplexType organisationAchatFournisseurComplexType = objectFactory.createOA408220D8ComplexType();
            organisationAchatFournisseurComplexType.setValue(organisationAchatFournisseur);
            JAXBElement<OA408220D8ComplexType> requeteComplexTypeOrganisationAchatFournisseur = objectFactory
                    .createRequeteAMMWS2282F6AbeComplexTypeOA(organisationAchatFournisseurComplexType);

            requeteComplexType.setOA(requeteComplexTypeOrganisationAchatFournisseur);
        }

        if (!StringUtils.isEmpty(domaineCommercialeClient)) {
            // Création de l'objet domaineCommercialeClient
            DC5058D5B7ComplexType domaineCommercialeClientComplexType = objectFactory.createDC5058D5B7ComplexType();
            domaineCommercialeClientComplexType.setValue(domaineCommercialeClient);
            JAXBElement<DC5058D5B7ComplexType> requeteComplexTypeDomaineCommercialeClient = objectFactory
                    .createRequeteAMMWS2282F6AbeComplexTypeDC(domaineCommercialeClientComplexType);

            requeteComplexType.setDC(requeteComplexTypeDomaineCommercialeClient);
        }

        WSWSP0117AWebS2AMMPortType api = this.getChorusApi();

        ResultatAMMWS2C86Cadc1ComplexType resultatComplexType = api.getInfoByID(requeteComplexType);

        return traiterChorusResult(resultatComplexType);
    }

    /**
     * Traiter chorus result.
     *
     * @param resultatComplexType
     *            the result
     * @return the collectivite Chorus
     */
    private static List<CollectiviteChorus> traiterChorusResult(ResultatAMMWS2C86Cadc1ComplexType resultatComplexType) {

        // Traitement des erreurs applicatives
        Statut6Cb9Aa2CComplexType statut = resultatComplexType.getStatut();
        // Si le code retour n'est pas OK, on retourne l'erreur
        if (!RETOUR_DONNEES_OK.equals(statut.getRetourDonnees().getValue().getValue())) {

            throw new RegleGestionException(statut.getMessageErreur().getValue().getValue());
        }

        List<Donnees09807E94ComplexType> donnees = resultatComplexType.getDonnees();

        // Si aucune collectivité n'a été trouvée pour la recherche, on renvoie null.
        if ((donnees == null) || donnees.isEmpty()) {
            return new ArrayList<>(0);
        }

        List<CollectiviteChorus> collectivitesChorus = new ArrayList<>(resultatComplexType.getDonnees().size());

        for (Donnees09807E94ComplexType donneesComplexType : donnees) {

            CollectiviteChorus collectiviteChorus = new CollectiviteChorus();
            collectiviteChorus.setTypeTiers(donneesComplexType.getTypeTiers().getValue().getValue());
            collectiviteChorus.setGroupeDeComptes(donneesComplexType.getGroupeDeComptes().getValue().getValue());
            collectiviteChorus.setIdTechnique(donneesComplexType.getIdTechnique().getValue().getValue());
            collectiviteChorus.setNom(donneesComplexType.getNom().getValue().getValue());
            collectiviteChorus.setCodePostal(donneesComplexType.getCodePostal().getValue().getValue());
            collectiviteChorus.setVille(donneesComplexType.getVille().getValue().getValue());
            collectiviteChorus.setNumeroRue(donneesComplexType.getNumeroRue().getValue());
            collectiviteChorus.setNomRue(donneesComplexType.getNomRue().getValue());
            collectiviteChorus.setComplementAdresse(donneesComplexType.getComplementAdresse().getValue());
            collectiviteChorus.setCodePays(donneesComplexType.getCodePays().getValue());

            XMLGregorianCalendar xmlDate = donneesComplexType.getDateSynchro().getValue().getValue();
            if (xmlDate != null) {
                collectiviteChorus.setDateSynchro(xmlDate.toGregorianCalendar().toZonedDateTime().toLocalDateTime());
            }

            collectivitesChorus.add(collectiviteChorus);
        }

        return collectivitesChorus;
    }

    /**
     * Gets the chorus api.
     *
     * @return the chorus api
     */
    private WSWSP0117AWebS2AMMPortType getChorusApi() {

        String wsdlURLStr = null;
        String endPointUrlStr = null;

        if (StringUtils.endsWith(this.chorusWebServiceProperties.getUrl(), FIN_URL_WSDL)) {
            wsdlURLStr = this.chorusWebServiceProperties.getUrl();
            endPointUrlStr = StringUtils.remove(this.chorusWebServiceProperties.getUrl(), FIN_URL_WSDL);
        } else {
            wsdlURLStr = this.chorusWebServiceProperties.getUrl() + FIN_URL_WSDL;
            endPointUrlStr = this.chorusWebServiceProperties.getUrl();
        }

        try {
            URL wsdlURL = new URL(wsdlURLStr);
            WSWSP0117AWebS2AMM wswsp0117aWebS2AMM = new WSWSP0117AWebS2AMM(wsdlURL);

            WSWSP0117AWebS2AMMPortType portType = wswsp0117aWebS2AMM.getPort(WSWSP0117AWebS2AMMPortType.class);
            BindingProvider bindingProvider = (BindingProvider) portType;
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrlStr);
            return portType;

        } catch (IOException e) {
            LOGGER.error("IOException getChorusApi", e);
            throw new RegleGestionException(WsChorusCodesErreursEnum.WEBS2_00.getCode(), e);
        }
    }
}

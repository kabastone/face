/**
 *
 */
package fr.gouv.sitde.face.clientchorus.service;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class ChorusWebServiceProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("chorus.web-service")
@Validated
public class ChorusWebServiceProperties {

    /** The url. */
    @NotEmpty
    private String url;

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

}

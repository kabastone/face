
package ammws2.wschorus;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour ResultatAMMWS2_c86cadc1_ComplexType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 *  
 * &lt;complexType name="ResultatAMMWS2_c86cadc1_ComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="Statut" type="{http://wschorus.ammws2}Statut_6cb9aa2c_ComplexType"/&gt;
 *           &lt;element name="Donnees" type="{http://wschorus.ammws2}Donnees_09807e94_ComplexType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultatAMMWS2_c86cadc1_ComplexType", propOrder = { "statut", "donnees" })
public class ResultatAMMWS2C86Cadc1ComplexType {

    @XmlElement(name = "Statut", required = true)
    protected Statut6Cb9Aa2CComplexType statut;
    @XmlElement(name = "Donnees")
    protected List<Donnees09807E94ComplexType> donnees;

    /**
     * Obtient la valeur de la propriété statut.
     *
     * @return possible object is {@link Statut6Cb9Aa2CComplexType }
     * 
     */
    public Statut6Cb9Aa2CComplexType getStatut() {
        return this.statut;
    }

    /**
     * Définit la valeur de la propriété statut.
     *
     * @param value
     *            allowed object is {@link Statut6Cb9Aa2CComplexType }
     * 
     */
    public void setStatut(Statut6Cb9Aa2CComplexType value) {
        this.statut = value;
    }

    /**
     * Gets the value of the donnees property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
     * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the donnees property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDonnees().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Donnees09807E94ComplexType }
     *
     *
     */
    public List<Donnees09807E94ComplexType> getDonnees() {
        if (this.donnees == null) {
            this.donnees = new ArrayList<>();
        }
        return this.donnees;
    }

}

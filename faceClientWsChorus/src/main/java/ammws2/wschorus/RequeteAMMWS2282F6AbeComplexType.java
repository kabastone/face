
package ammws2.wschorus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour RequeteAMMWS2_282f6abe_ComplexType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="RequeteAMMWS2_282f6abe_ComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="CodeSociete" type="{http://wschorus.ammws2}CodeSociete_8f55e412_ComplexType" minOccurs="0"/&gt;
 *           &lt;element name="IdFonctionnel" type="{http://wschorus.ammws2}IdFonctionnel_52763bd2_ComplexType" minOccurs="0"/&gt;
 *           &lt;element name="IdTechnique" type="{http://wschorus.ammws2}IdTechnique_3364ce0c_ComplexType" minOccurs="0"/&gt;
 *           &lt;element name="OA" type="{http://wschorus.ammws2}OA_408220d8_ComplexType" minOccurs="0"/&gt;
 *           &lt;element name="DC" type="{http://wschorus.ammws2}DC_5058d5b7_ComplexType" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequeteAMMWS2_282f6abe_ComplexType", propOrder = { "codeSociete", "idFonctionnel", "idTechnique", "oa", "dc" })
public class RequeteAMMWS2282F6AbeComplexType {

    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "CodeSociete", required = false)
    protected JAXBElement<CodeSociete8F55E412ComplexType> codeSociete;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "IdFonctionnel", required = false)
    protected JAXBElement<IdFonctionnel52763Bd2ComplexType> idFonctionnel;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "IdTechnique", required = false)
    protected JAXBElement<IdTechnique3364Ce0CComplexType> idTechnique;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "OA", required = false)
    protected JAXBElement<OA408220D8ComplexType> oa;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "DC", required = false)
    protected JAXBElement<DC5058D5B7ComplexType> dc;

    /**
     * Obtient la valeur de la propriété codeSociete.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link CodeSociete8F55E412ComplexType }{@code >}
     *
     */
    public JAXBElement<CodeSociete8F55E412ComplexType> getCodeSociete() {
        return this.codeSociete;
    }

    /**
     * Définit la valeur de la propriété codeSociete.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link CodeSociete8F55E412ComplexType }{@code >}
     *
     */
    public void setCodeSociete(JAXBElement<CodeSociete8F55E412ComplexType> value) {
        this.codeSociete = value;
    }

    /**
     * Obtient la valeur de la propriété idFonctionnel.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link IdFonctionnel52763Bd2ComplexType }{@code >}
     *
     */
    public JAXBElement<IdFonctionnel52763Bd2ComplexType> getIdFonctionnel() {
        return this.idFonctionnel;
    }

    /**
     * Définit la valeur de la propriété idFonctionnel.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link IdFonctionnel52763Bd2ComplexType }{@code >}
     *
     */
    public void setIdFonctionnel(JAXBElement<IdFonctionnel52763Bd2ComplexType> value) {
        this.idFonctionnel = value;
    }

    /**
     * Obtient la valeur de la propriété idTechnique.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link IdTechnique3364Ce0CComplexType }{@code >}
     *
     */
    public JAXBElement<IdTechnique3364Ce0CComplexType> getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Définit la valeur de la propriété idTechnique.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link IdTechnique3364Ce0CComplexType }{@code >}
     *
     */
    public void setIdTechnique(JAXBElement<IdTechnique3364Ce0CComplexType> value) {
        this.idTechnique = value;
    }

    /**
     * Obtient la valeur de la propriété oa.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link OA408220D8ComplexType }{@code >}
     *
     */
    public JAXBElement<OA408220D8ComplexType> getOA() {
        return this.oa;
    }

    /**
     * Définit la valeur de la propriété oa.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link OA408220D8ComplexType }{@code >}
     *
     */
    public void setOA(JAXBElement<OA408220D8ComplexType> value) {
        this.oa = value;
    }

    /**
     * Obtient la valeur de la propriété dc.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link DC5058D5B7ComplexType }{@code >}
     *
     */
    public JAXBElement<DC5058D5B7ComplexType> getDC() {
        return this.dc;
    }

    /**
     * Définit la valeur de la propriété dc.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link DC5058D5B7ComplexType }{@code >}
     *
     */
    public void setDC(JAXBElement<DC5058D5B7ComplexType> value) {
        this.dc = value;
    }

}

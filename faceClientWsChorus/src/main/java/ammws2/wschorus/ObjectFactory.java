
package ammws2.wschorus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the ammws2.wschorus package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private static final String NAMESPACE_AMMWS2 = "http://wschorus.ammws2";

    private static final QName _RequeteAMMWS2_QNAME = new QName(NAMESPACE_AMMWS2, "RequeteAMMWS2");
    private static final QName _ResultatAMMWS2_QNAME = new QName(NAMESPACE_AMMWS2, "ResultatAMMWS2");
    private static final QName _Donnees09807E94ComplexTypeTypeTiers_QNAME = new QName(NAMESPACE_AMMWS2, "TypeTiers");
    private static final QName _Donnees09807E94ComplexTypeGroupeDeComptes_QNAME = new QName(NAMESPACE_AMMWS2, "GroupeDeComptes");
    private static final QName _Donnees09807E94ComplexTypeIdTechnique_QNAME = new QName(NAMESPACE_AMMWS2, "IdTechnique");
    private static final QName _Donnees09807E94ComplexTypeIdFonctionnel_QNAME = new QName(NAMESPACE_AMMWS2, "IdFonctionnel");
    private static final QName _Donnees09807E94ComplexTypeNom_QNAME = new QName(NAMESPACE_AMMWS2, "Nom");
    private static final QName _Donnees09807E94ComplexTypeCodePostal_QNAME = new QName(NAMESPACE_AMMWS2, "CodePostal");
    private static final QName _Donnees09807E94ComplexTypeVille_QNAME = new QName(NAMESPACE_AMMWS2, "Ville");
    private static final QName _Donnees09807E94ComplexTypeDateSynchro_QNAME = new QName(NAMESPACE_AMMWS2, "DateSynchro");
    private static final QName _Statut6Cb9Aa2CComplexTypeRetourDonnees_QNAME = new QName(NAMESPACE_AMMWS2, "RetourDonnees");
    private static final QName _Statut6Cb9Aa2CComplexTypeMessageErreur_QNAME = new QName(NAMESPACE_AMMWS2, "MessageErreur");
    private static final QName _RequeteAMMWS2282F6AbeComplexTypeCodeSociete_QNAME = new QName(NAMESPACE_AMMWS2, "CodeSociete");
    private static final QName _RequeteAMMWS2282F6AbeComplexTypeOA_QNAME = new QName(NAMESPACE_AMMWS2, "OA");
    private static final QName _RequeteAMMWS2282F6AbeComplexTypeDC_QNAME = new QName(NAMESPACE_AMMWS2, "DC");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ammws2.wschorus
     *
     */
    public ObjectFactory() {
        // Constructeur non parapetre.
    }

    /**
     * Create an instance of {@link RequeteAMMWS2282F6AbeComplexType }
     *
     */
    public RequeteAMMWS2282F6AbeComplexType createRequeteAMMWS2282F6AbeComplexType() {
        return new RequeteAMMWS2282F6AbeComplexType();
    }

    /**
     * Create an instance of {@link ResultatAMMWS2C86Cadc1ComplexType }
     *
     */
    public ResultatAMMWS2C86Cadc1ComplexType createResultatAMMWS2C86Cadc1ComplexType() {
        return new ResultatAMMWS2C86Cadc1ComplexType();
    }

    /**
     * Create an instance of {@link CodeSociete8F55E412ComplexType }
     *
     */
    public CodeSociete8F55E412ComplexType createCodeSociete8F55E412ComplexType() {
        return new CodeSociete8F55E412ComplexType();
    }

    /**
     * Create an instance of {@link IdFonctionnel52763Bd2ComplexType }
     *
     */
    public IdFonctionnel52763Bd2ComplexType createIdFonctionnel52763Bd2ComplexType() {
        return new IdFonctionnel52763Bd2ComplexType();
    }

    /**
     * Create an instance of {@link IdTechnique3364Ce0CComplexType }
     *
     */
    public IdTechnique3364Ce0CComplexType createIdTechnique3364Ce0CComplexType() {
        return new IdTechnique3364Ce0CComplexType();
    }

    /**
     * Create an instance of {@link OA408220D8ComplexType }
     *
     */
    public OA408220D8ComplexType createOA408220D8ComplexType() {
        return new OA408220D8ComplexType();
    }

    /**
     * Create an instance of {@link DC5058D5B7ComplexType }
     *
     */
    public DC5058D5B7ComplexType createDC5058D5B7ComplexType() {
        return new DC5058D5B7ComplexType();
    }

    /**
     * Create an instance of {@link Statut6Cb9Aa2CComplexType }
     *
     */
    public Statut6Cb9Aa2CComplexType createStatut6Cb9Aa2CComplexType() {
        return new Statut6Cb9Aa2CComplexType();
    }

    /**
     * Create an instance of {@link RetourDonnees88A44A95ComplexType }
     *
     */
    public RetourDonnees88A44A95ComplexType createRetourDonnees88A44A95ComplexType() {
        return new RetourDonnees88A44A95ComplexType();
    }

    /**
     * Create an instance of {@link MessageErreur97344010ComplexType }
     *
     */
    public MessageErreur97344010ComplexType createMessageErreur97344010ComplexType() {
        return new MessageErreur97344010ComplexType();
    }

    /**
     * Create an instance of {@link Donnees09807E94ComplexType }
     *
     */
    public Donnees09807E94ComplexType createDonnees09807E94ComplexType() {
        return new Donnees09807E94ComplexType();
    }

    /**
     * Create an instance of {@link TypeTiers9B9F3C10ComplexType }
     *
     */
    public TypeTiers9B9F3C10ComplexType createTypeTiers9B9F3C10ComplexType() {
        return new TypeTiers9B9F3C10ComplexType();
    }

    /**
     * Create an instance of {@link GroupeDeComptes25A25B5FComplexType }
     *
     */
    public GroupeDeComptes25A25B5FComplexType createGroupeDeComptes25A25B5FComplexType() {
        return new GroupeDeComptes25A25B5FComplexType();
    }

    /**
     * Create an instance of {@link IdTechniqueBd2257D8ComplexType }
     *
     */
    public IdTechniqueBd2257D8ComplexType createIdTechniqueBd2257D8ComplexType() {
        return new IdTechniqueBd2257D8ComplexType();
    }

    /**
     * Create an instance of {@link IdFonctionnel241Be0A6ComplexType }
     *
     */
    public IdFonctionnel241Be0A6ComplexType createIdFonctionnel241Be0A6ComplexType() {
        return new IdFonctionnel241Be0A6ComplexType();
    }

    /**
     * Create an instance of {@link NomF9Bfe903ComplexType }
     *
     */
    public NomF9Bfe903ComplexType createNomF9Bfe903ComplexType() {
        return new NomF9Bfe903ComplexType();
    }

    /**
     * Create an instance of {@link CodePostal90272970ComplexType }
     *
     */
    public CodePostal90272970ComplexType createCodePostal90272970ComplexType() {
        return new CodePostal90272970ComplexType();
    }

    /**
     * Create an instance of {@link VilleDad77A13ComplexType }
     *
     */
    public VilleDad77A13ComplexType createVilleDad77A13ComplexType() {
        return new VilleDad77A13ComplexType();
    }

    /**
     * Create an instance of {@link NumeroRue72Ca89DbComplexType }
     *
     */
    public NumeroRue72Ca89DbComplexType createNumeroRue72Ca89DbComplexType() {
        return new NumeroRue72Ca89DbComplexType();
    }

    /**
     * Create an instance of {@link NomRueC469A157ComplexType }
     *
     */
    public NomRueC469A157ComplexType createNomRueC469A157ComplexType() {
        return new NomRueC469A157ComplexType();
    }

    /**
     * Create an instance of {@link ComplementAdresse0Bb87343ComplexType }
     *
     */
    public ComplementAdresse0Bb87343ComplexType createComplementAdresse0Bb87343ComplexType() {
        return new ComplementAdresse0Bb87343ComplexType();
    }

    /**
     * Create an instance of {@link CodePaysCf75De22ComplexType }
     *
     */
    public CodePaysCf75De22ComplexType createCodePaysCf75De22ComplexType() {
        return new CodePaysCf75De22ComplexType();
    }

    /**
     * Create an instance of {@link DateSynchro34Dd2416ComplexType }
     *
     */
    public DateSynchro34Dd2416ComplexType createDateSynchro34Dd2416ComplexType() {
        return new DateSynchro34Dd2416ComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequeteAMMWS2282F6AbeComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RequeteAMMWS2282F6AbeComplexType }{@code >}
     */
    @XmlElementDecl(namespace = NAMESPACE_AMMWS2, name = "RequeteAMMWS2")
    public JAXBElement<RequeteAMMWS2282F6AbeComplexType> createRequeteAMMWS2(RequeteAMMWS2282F6AbeComplexType value) {
        return new JAXBElement<>(_RequeteAMMWS2_QNAME, RequeteAMMWS2282F6AbeComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultatAMMWS2C86Cadc1ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ResultatAMMWS2C86Cadc1ComplexType }{@code >}
     */
    @XmlElementDecl(namespace = NAMESPACE_AMMWS2, name = "ResultatAMMWS2")
    public JAXBElement<ResultatAMMWS2C86Cadc1ComplexType> createResultatAMMWS2(ResultatAMMWS2C86Cadc1ComplexType value) {
        return new JAXBElement<>(_ResultatAMMWS2_QNAME, ResultatAMMWS2C86Cadc1ComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeTiers9B9F3C10ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TypeTiers9B9F3C10ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "TypeTiers")
    public JAXBElement<TypeTiers9B9F3C10ComplexType> createDonnees09807E94ComplexTypeTypeTiers(TypeTiers9B9F3C10ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeTypeTiers_QNAME, TypeTiers9B9F3C10ComplexType.class, Donnees09807E94ComplexType.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupeDeComptes25A25B5FComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link GroupeDeComptes25A25B5FComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "GroupeDeComptes")
    public JAXBElement<GroupeDeComptes25A25B5FComplexType> createDonnees09807E94ComplexTypeGroupeDeComptes(GroupeDeComptes25A25B5FComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeGroupeDeComptes_QNAME, GroupeDeComptes25A25B5FComplexType.class,
                Donnees09807E94ComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdTechniqueBd2257D8ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link IdTechniqueBd2257D8ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "IdTechnique")
    public JAXBElement<IdTechniqueBd2257D8ComplexType> createDonnees09807E94ComplexTypeIdTechnique(IdTechniqueBd2257D8ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeIdTechnique_QNAME, IdTechniqueBd2257D8ComplexType.class, Donnees09807E94ComplexType.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdFonctionnel241Be0A6ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link IdFonctionnel241Be0A6ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "IdFonctionnel")
    public JAXBElement<IdFonctionnel241Be0A6ComplexType> createDonnees09807E94ComplexTypeIdFonctionnel(IdFonctionnel241Be0A6ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeIdFonctionnel_QNAME, IdFonctionnel241Be0A6ComplexType.class,
                Donnees09807E94ComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NomF9Bfe903ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NomF9Bfe903ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "Nom")
    public JAXBElement<NomF9Bfe903ComplexType> createDonnees09807E94ComplexTypeNom(NomF9Bfe903ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeNom_QNAME, NomF9Bfe903ComplexType.class, Donnees09807E94ComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodePostal90272970ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CodePostal90272970ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "CodePostal")
    public JAXBElement<CodePostal90272970ComplexType> createDonnees09807E94ComplexTypeCodePostal(CodePostal90272970ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeCodePostal_QNAME, CodePostal90272970ComplexType.class, Donnees09807E94ComplexType.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VilleDad77A13ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link VilleDad77A13ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "Ville")
    public JAXBElement<VilleDad77A13ComplexType> createDonnees09807E94ComplexTypeVille(VilleDad77A13ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeVille_QNAME, VilleDad77A13ComplexType.class, Donnees09807E94ComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateSynchro34Dd2416ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DateSynchro34Dd2416ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Donnees09807E94ComplexType.class, namespace = NAMESPACE_AMMWS2, name = "DateSynchro")
    public JAXBElement<DateSynchro34Dd2416ComplexType> createDonnees09807E94ComplexTypeDateSynchro(DateSynchro34Dd2416ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeDateSynchro_QNAME, DateSynchro34Dd2416ComplexType.class, Donnees09807E94ComplexType.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetourDonnees88A44A95ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RetourDonnees88A44A95ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Statut6Cb9Aa2CComplexType.class, namespace = NAMESPACE_AMMWS2, name = "RetourDonnees")
    public JAXBElement<RetourDonnees88A44A95ComplexType> createStatut6Cb9Aa2CComplexTypeRetourDonnees(RetourDonnees88A44A95ComplexType value) {
        return new JAXBElement<>(_Statut6Cb9Aa2CComplexTypeRetourDonnees_QNAME, RetourDonnees88A44A95ComplexType.class,
                Statut6Cb9Aa2CComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageErreur97344010ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link MessageErreur97344010ComplexType }{@code >}
     */
    @XmlElementDecl(scope = Statut6Cb9Aa2CComplexType.class, namespace = NAMESPACE_AMMWS2, name = "MessageErreur")
    public JAXBElement<MessageErreur97344010ComplexType> createStatut6Cb9Aa2CComplexTypeMessageErreur(MessageErreur97344010ComplexType value) {
        return new JAXBElement<>(_Statut6Cb9Aa2CComplexTypeMessageErreur_QNAME, MessageErreur97344010ComplexType.class,
                Statut6Cb9Aa2CComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodeSociete8F55E412ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CodeSociete8F55E412ComplexType }{@code >}
     */
    @XmlElementDecl(scope = RequeteAMMWS2282F6AbeComplexType.class, namespace = NAMESPACE_AMMWS2, name = "CodeSociete")
    public JAXBElement<CodeSociete8F55E412ComplexType> createRequeteAMMWS2282F6AbeComplexTypeCodeSociete(CodeSociete8F55E412ComplexType value) {
        return new JAXBElement<>(_RequeteAMMWS2282F6AbeComplexTypeCodeSociete_QNAME, CodeSociete8F55E412ComplexType.class,
                RequeteAMMWS2282F6AbeComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdFonctionnel52763Bd2ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link IdFonctionnel52763Bd2ComplexType }{@code >}
     */
    @XmlElementDecl(scope = RequeteAMMWS2282F6AbeComplexType.class, namespace = NAMESPACE_AMMWS2, name = "IdFonctionnel")
    public JAXBElement<IdFonctionnel52763Bd2ComplexType> createRequeteAMMWS2282F6AbeComplexTypeIdFonctionnel(IdFonctionnel52763Bd2ComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeIdFonctionnel_QNAME, IdFonctionnel52763Bd2ComplexType.class,
                RequeteAMMWS2282F6AbeComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdTechnique3364Ce0CComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link IdTechnique3364Ce0CComplexType }{@code >}
     */
    @XmlElementDecl(scope = RequeteAMMWS2282F6AbeComplexType.class, namespace = NAMESPACE_AMMWS2, name = "IdTechnique")
    public JAXBElement<IdTechnique3364Ce0CComplexType> createRequeteAMMWS2282F6AbeComplexTypeIdTechnique(IdTechnique3364Ce0CComplexType value) {
        return new JAXBElement<>(_Donnees09807E94ComplexTypeIdTechnique_QNAME, IdTechnique3364Ce0CComplexType.class,
                RequeteAMMWS2282F6AbeComplexType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OA408220D8ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link OA408220D8ComplexType }{@code >}
     */
    @XmlElementDecl(scope = RequeteAMMWS2282F6AbeComplexType.class, namespace = NAMESPACE_AMMWS2, name = "OA")
    public JAXBElement<OA408220D8ComplexType> createRequeteAMMWS2282F6AbeComplexTypeOA(OA408220D8ComplexType value) {
        return new JAXBElement<>(_RequeteAMMWS2282F6AbeComplexTypeOA_QNAME, OA408220D8ComplexType.class, RequeteAMMWS2282F6AbeComplexType.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DC5058D5B7ComplexType }{@code >}
     *
     * @param value
     *            Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DC5058D5B7ComplexType }{@code >}
     */
    @XmlElementDecl(scope = RequeteAMMWS2282F6AbeComplexType.class, namespace = NAMESPACE_AMMWS2, name = "DC")
    public JAXBElement<DC5058D5B7ComplexType> createRequeteAMMWS2282F6AbeComplexTypeDC(DC5058D5B7ComplexType value) {
        return new JAXBElement<>(_RequeteAMMWS2282F6AbeComplexTypeDC_QNAME, DC5058D5B7ComplexType.class, RequeteAMMWS2282F6AbeComplexType.class,
                value);
    }

}

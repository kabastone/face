
package ammws2.wschorus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour Donnees_09807e94_ComplexType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="Donnees_09807e94_ComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TypeTiers" type="{http://wschorus.ammws2}TypeTiers_9b9f3c10_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="GroupeDeComptes" type="{http://wschorus.ammws2}GroupeDeComptes_25a25b5f_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="IdTechnique" type="{http://wschorus.ammws2}IdTechnique_bd2257d8_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="IdFonctionnel" type="{http://wschorus.ammws2}IdFonctionnel_241be0a6_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="Nom" type="{http://wschorus.ammws2}Nom_f9bfe903_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="CodePostal" type="{http://wschorus.ammws2}CodePostal_90272970_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="Ville" type="{http://wschorus.ammws2}Ville_dad77a13_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="NumeroRue" type="{http://wschorus.ammws2}NumeroRue_72ca89db_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="NomRue" type="{http://wschorus.ammws2}NomRue_c469a157_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="ComplementAdresse" type="{http://wschorus.ammws2}ComplementAdresse_0bb87343_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="CodePays" type="{http://wschorus.ammws2}CodePays_cf75de22_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="DateSynchro" type="{http://wschorus.ammws2}DateSynchro_34dd2416_ComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Donnees_09807e94_ComplexType", propOrder = { "typeTiers", "groupeDeComptes", "idTechnique", "idFonctionnel", "nom", "codePostal",
        "ville", "numeroRue", "nomRue", "complementAdresse", "codePays", "dateSynchro" })
public class Donnees09807E94ComplexType {

    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "TypeTiers", required = false)
    protected JAXBElement<TypeTiers9B9F3C10ComplexType> typeTiers;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "GroupeDeComptes", required = false)
    protected JAXBElement<GroupeDeComptes25A25B5FComplexType> groupeDeComptes;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "IdTechnique", required = false)
    protected JAXBElement<IdTechniqueBd2257D8ComplexType> idTechnique;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "IdFonctionnel", required = false)
    protected JAXBElement<IdFonctionnel241Be0A6ComplexType> idFonctionnel;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "Nom", required = false)
    protected JAXBElement<NomF9Bfe903ComplexType> nom;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "CodePostal", required = false)
    protected JAXBElement<CodePostal90272970ComplexType> codePostal;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "Ville", required = false)
    protected JAXBElement<VilleDad77A13ComplexType> ville;
    @XmlElement(name = "NumeroRue")
    protected NumeroRue72Ca89DbComplexType numeroRue;
    @XmlElement(name = "NomRue")
    protected NomRueC469A157ComplexType nomRue;
    @XmlElement(name = "ComplementAdresse")
    protected ComplementAdresse0Bb87343ComplexType complementAdresse;
    @XmlElement(name = "CodePays")
    protected CodePaysCf75De22ComplexType codePays;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "DateSynchro", required = false)
    protected JAXBElement<DateSynchro34Dd2416ComplexType> dateSynchro;

    /**
     * Obtient la valeur de la propriété typeTiers.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link TypeTiers9B9F3C10ComplexType }{@code >}
     *
     */
    public JAXBElement<TypeTiers9B9F3C10ComplexType> getTypeTiers() {
        return this.typeTiers;
    }

    /**
     * Définit la valeur de la propriété typeTiers.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link TypeTiers9B9F3C10ComplexType }{@code >}
     *
     */
    public void setTypeTiers(JAXBElement<TypeTiers9B9F3C10ComplexType> value) {
        this.typeTiers = value;
    }

    /**
     * Obtient la valeur de la propriété groupeDeComptes.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link GroupeDeComptes25A25B5FComplexType }{@code >}
     *
     */
    public JAXBElement<GroupeDeComptes25A25B5FComplexType> getGroupeDeComptes() {
        return this.groupeDeComptes;
    }

    /**
     * Définit la valeur de la propriété groupeDeComptes.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GroupeDeComptes25A25B5FComplexType }{@code >}
     *
     */
    public void setGroupeDeComptes(JAXBElement<GroupeDeComptes25A25B5FComplexType> value) {
        this.groupeDeComptes = value;
    }

    /**
     * Obtient la valeur de la propriété idTechnique.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link IdTechniqueBd2257D8ComplexType }{@code >}
     *
     */
    public JAXBElement<IdTechniqueBd2257D8ComplexType> getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Définit la valeur de la propriété idTechnique.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link IdTechniqueBd2257D8ComplexType }{@code >}
     *
     */
    public void setIdTechnique(JAXBElement<IdTechniqueBd2257D8ComplexType> value) {
        this.idTechnique = value;
    }

    /**
     * Obtient la valeur de la propriété idFonctionnel.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link IdFonctionnel241Be0A6ComplexType }{@code >}
     *
     */
    public JAXBElement<IdFonctionnel241Be0A6ComplexType> getIdFonctionnel() {
        return this.idFonctionnel;
    }

    /**
     * Définit la valeur de la propriété idFonctionnel.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link IdFonctionnel241Be0A6ComplexType }{@code >}
     *
     */
    public void setIdFonctionnel(JAXBElement<IdFonctionnel241Be0A6ComplexType> value) {
        this.idFonctionnel = value;
    }

    /**
     * Obtient la valeur de la propriété nom.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link NomF9Bfe903ComplexType }{@code >}
     *
     */
    public JAXBElement<NomF9Bfe903ComplexType> getNom() {
        return this.nom;
    }

    /**
     * Définit la valeur de la propriété nom.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link NomF9Bfe903ComplexType }{@code >}
     *
     */
    public void setNom(JAXBElement<NomF9Bfe903ComplexType> value) {
        this.nom = value;
    }

    /**
     * Obtient la valeur de la propriété codePostal.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link CodePostal90272970ComplexType }{@code >}
     *
     */
    public JAXBElement<CodePostal90272970ComplexType> getCodePostal() {
        return this.codePostal;
    }

    /**
     * Définit la valeur de la propriété codePostal.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link CodePostal90272970ComplexType }{@code >}
     *
     */
    public void setCodePostal(JAXBElement<CodePostal90272970ComplexType> value) {
        this.codePostal = value;
    }

    /**
     * Obtient la valeur de la propriété ville.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link VilleDad77A13ComplexType }{@code >}
     *
     */
    public JAXBElement<VilleDad77A13ComplexType> getVille() {
        return this.ville;
    }

    /**
     * Définit la valeur de la propriété ville.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link VilleDad77A13ComplexType }{@code >}
     *
     */
    public void setVille(JAXBElement<VilleDad77A13ComplexType> value) {
        this.ville = value;
    }

    /**
     * Obtient la valeur de la propriété numeroRue.
     *
     * @return possible object is {@link NumeroRue72Ca89DbComplexType }
     *
     */
    public NumeroRue72Ca89DbComplexType getNumeroRue() {
        return this.numeroRue;
    }

    /**
     * Définit la valeur de la propriété numeroRue.
     *
     * @param value
     *            allowed object is {@link NumeroRue72Ca89DbComplexType }
     *
     */
    public void setNumeroRue(NumeroRue72Ca89DbComplexType value) {
        this.numeroRue = value;
    }

    /**
     * Obtient la valeur de la propriété nomRue.
     *
     * @return possible object is {@link NomRueC469A157ComplexType }
     *
     */
    public NomRueC469A157ComplexType getNomRue() {
        return this.nomRue;
    }

    /**
     * Définit la valeur de la propriété nomRue.
     *
     * @param value
     *            allowed object is {@link NomRueC469A157ComplexType }
     *
     */
    public void setNomRue(NomRueC469A157ComplexType value) {
        this.nomRue = value;
    }

    /**
     * Obtient la valeur de la propriété complementAdresse.
     *
     * @return possible object is {@link ComplementAdresse0Bb87343ComplexType }
     *
     */
    public ComplementAdresse0Bb87343ComplexType getComplementAdresse() {
        return this.complementAdresse;
    }

    /**
     * Définit la valeur de la propriété complementAdresse.
     *
     * @param value
     *            allowed object is {@link ComplementAdresse0Bb87343ComplexType }
     *
     */
    public void setComplementAdresse(ComplementAdresse0Bb87343ComplexType value) {
        this.complementAdresse = value;
    }

    /**
     * Obtient la valeur de la propriété codePays.
     *
     * @return possible object is {@link CodePaysCf75De22ComplexType }
     *
     */
    public CodePaysCf75De22ComplexType getCodePays() {
        return this.codePays;
    }

    /**
     * Définit la valeur de la propriété codePays.
     *
     * @param value
     *            allowed object is {@link CodePaysCf75De22ComplexType }
     *
     */
    public void setCodePays(CodePaysCf75De22ComplexType value) {
        this.codePays = value;
    }

    /**
     * Obtient la valeur de la propriété dateSynchro.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link DateSynchro34Dd2416ComplexType }{@code >}
     *
     */
    public JAXBElement<DateSynchro34Dd2416ComplexType> getDateSynchro() {
        return this.dateSynchro;
    }

    /**
     * Définit la valeur de la propriété dateSynchro.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link DateSynchro34Dd2416ComplexType }{@code >}
     *
     */
    public void setDateSynchro(JAXBElement<DateSynchro34Dd2416ComplexType> value) {
        this.dateSynchro = value;
    }

}


package ammws2.wschorus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour Statut_6cb9aa2c_ComplexType complex type.
 *
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="Statut_6cb9aa2c_ComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetourDonnees" type="{http://wschorus.ammws2}RetourDonnees_88a44a95_ComplexType" minOccurs="0"/&gt;
 *         &lt;element name="MessageErreur" type="{http://wschorus.ammws2}MessageErreur_97344010_ComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Statut_6cb9aa2c_ComplexType", propOrder = { "retourDonnees", "messageErreur" })
public class Statut6Cb9Aa2CComplexType {

    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "RetourDonnees", required = false)
    protected JAXBElement<RetourDonnees88A44A95ComplexType> retourDonnees;
    @XmlElementRef(type = JAXBElement.class, namespace = "http://wschorus.ammws2", name = "MessageErreur", required = false)
    protected JAXBElement<MessageErreur97344010ComplexType> messageErreur;

    /**
     * Obtient la valeur de la propriété retourDonnees.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link RetourDonnees88A44A95ComplexType }{@code >}
     *
     */
    public JAXBElement<RetourDonnees88A44A95ComplexType> getRetourDonnees() {
        return this.retourDonnees;
    }

    /**
     * Définit la valeur de la propriété retourDonnees.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link RetourDonnees88A44A95ComplexType }{@code >}
     *
     */
    public void setRetourDonnees(JAXBElement<RetourDonnees88A44A95ComplexType> value) {
        this.retourDonnees = value;
    }

    /**
     * Obtient la valeur de la propriété messageErreur.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link MessageErreur97344010ComplexType }{@code >}
     *
     */
    public JAXBElement<MessageErreur97344010ComplexType> getMessageErreur() {
        return this.messageErreur;
    }

    /**
     * Définit la valeur de la propriété messageErreur.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link MessageErreur97344010ComplexType }{@code >}
     *
     */
    public void setMessageErreur(JAXBElement<MessageErreur97344010ComplexType> value) {
        this.messageErreur = value;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration;

import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * Classe métier de Utilisateur.
 *
 * @author a453029
 */
public interface UtilisateurService {

    /**
     * Rechercher utilisateur par id.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @return the utilisateur
     */
    Utilisateur rechercherUtilisateurParId(Long idUtilisateur);

    /**
     * Rechercher utilisateur par email.
     *
     * @param email
     *            the email
     * @return the utilisateur
     */
    Utilisateur rechercherUtilisateurParEmail(String email);

    /**
     * Creer un utilisateur en attente de connexion avec les droits d'agent sur une collectivité. <br/>
     * Son nom, son prénom et son id Cerbere sont des flags qui seront synchronisés avec Cerbere lors de sa premère connexion.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur
     */
    Utilisateur creerUtilisateurEnAttenteConnexion(Utilisateur utilisateur);

    /**
     * Synchroniser l'utilisateur connecté.<br/>
     * Crée l'utilisateur s'il n'existe pas en base, sinon met à jour le cerbereId de l'utilisateur existant s'il a été modifié.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur
     */
    Utilisateur synchroniserUtilisateurConnecte(Utilisateur utilisateur);

    /**
     * Modifier un utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     * @return the utilisateur
     */
    Utilisateur modifierUtilisateur(Utilisateur utilisateur);

    /**
     * Supprimer utilisateur.
     *
     * @param idUtilisateur
     *            the id utilisateur
     */
    void supprimerUtilisateur(Long idUtilisateur);

    /**
     * Rechercher tous utilisateurs.
     *
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<Utilisateur> rechercherTousUtilisateurs(PageDemande pageDemande);

}

package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.TransfertFongibiliteRepository;
import fr.gouv.sitde.face.domaine.service.dotation.TransfertFongibiliteService;
import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;

@Named
public class TransfertFongibiliteServiceImpl implements TransfertFongibiliteService {

    @Inject
    private TransfertFongibiliteRepository transfertFongibiliteRepository;

    @Inject
    private DotationCollectiviteRepository dotationCollectiviteRepository;
    @Inject
    private DotationDepartementRepository dotationDepartementRepository;
    @Inject
    private DotationSousProgrammeRepository dotationSousProgrammeRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.TransfertFongibiliteService#enregistrerTransfert(fr.gouv.sitde.face.transverse.entities.
     * TransfertFongibilite)
     */
    @Override
    public TransfertFongibilite enregistrerTransfert(TransfertFongibilite transfertFongibilite) {
        TransfertFongibiliteServiceImpl.verifierReglesTransfert(transfertFongibilite);
        this.repercuterTransfertSurDotations(transfertFongibilite);
        return this.transfertFongibiliteRepository.enregistrer(transfertFongibilite);
    }

    /**
     * @param transfertFongibilite
     */
    private void repercuterTransfertSurDotations(TransfertFongibilite transfertFongibilite) {
        this.gestionDotationOrigine(transfertFongibilite);
        this.gestionDotationDestination(transfertFongibilite);
    }

    /**
     * @param transfertFongibilite
     */
    private void gestionDotationOrigine(TransfertFongibilite transfertFongibilite) {
        BigDecimal montant = transfertFongibilite.getMontant();

        // Gestion sur la dotation collectivite d'origine
        transfertFongibilite.getDotationCollectiviteOrigine()
                .setMontant(transfertFongibilite.getDotationCollectiviteOrigine().getMontant().subtract(montant));
        this.dotationCollectiviteRepository.majDotationCollectivite(transfertFongibilite.getDotationCollectiviteOrigine());

        // Gestion sur la dotation departement d'origine
        transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement()
                .setMontantTotal(transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement().getMontantTotal().subtract(montant));
        transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement().setMontantNotifie(
                transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement().getMontantNotifie().subtract(montant));
        this.dotationDepartementRepository.enregistrer(transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement());

        // Gestion sur la dotation sous-programme d'origine
        transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement().getDotationSousProgramme().setMontant(transfertFongibilite
                .getDotationCollectiviteOrigine().getDotationDepartement().getDotationSousProgramme().getMontant().subtract(montant));
        this.dotationSousProgrammeRepository.modifierDotationSousProgramme(
                transfertFongibilite.getDotationCollectiviteOrigine().getDotationDepartement().getDotationSousProgramme());
    }

    /**
     * @param transfertFongibilite
     */
    private void gestionDotationDestination(TransfertFongibilite transfertFongibilite) {
        BigDecimal montant = transfertFongibilite.getMontant();

        // Gestion sur la dotation sous-programme d'origine
        transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement().getDotationSousProgramme().setMontant(transfertFongibilite
                .getDotationCollectiviteDestination().getDotationDepartement().getDotationSousProgramme().getMontant().add(montant));
        this.dotationSousProgrammeRepository.modifierDotationSousProgramme(
                transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement().getDotationSousProgramme());

        // Gestion sur la dotation departement d'origine
        transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement()
                .setMontantTotal(transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement().getMontantTotal().add(montant));
        transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement().setMontantNotifie(
                transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement().getMontantNotifie().add(montant));
        this.dotationDepartementRepository.enregistrer(transfertFongibilite.getDotationCollectiviteDestination().getDotationDepartement());

        // Gestion sur la dotation collectivite d'origine
        transfertFongibilite.getDotationCollectiviteDestination()
                .setMontant(transfertFongibilite.getDotationCollectiviteDestination().getMontant().add(montant));
        this.dotationCollectiviteRepository.majDotationCollectivite(transfertFongibilite.getDotationCollectiviteDestination());

    }

    /**
     * Verification de la coherence du transfert effectue avec les regles de gestion
     *
     * @param transfertFongibilite
     */
    private static void verifierReglesTransfert(TransfertFongibilite transfertFongibilite) {

        BigDecimal montant = transfertFongibilite.getMontant();

        if (montant.compareTo(BigDecimal.ZERO) < 0) {
            throw new RegleGestionException("fongibilite.transfert.montant.negatif");
        }

        if (montant.compareTo(BigDecimal.ZERO) == 0) {
            throw new RegleGestionException("fongibilite.transfert.montant.nul");
        }

        // Exception si le montant est supérieur à la dotation restante (soit montant de dotation - dotation deja repartie)
        if (montant.compareTo(transfertFongibilite.getDotationCollectiviteOrigine().getMontant()
                .subtract(transfertFongibilite.getDotationCollectiviteOrigine().getDotationRepartie())) > 0) {
            throw new RegleGestionException("fongibilite.transfert.montant.depasse.dotation.origine");
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.TransfertFongibiliteService#rechercherTransfertsCollectiviteParAnnee(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<TransfertFongibilite> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee) {
        return this.transfertFongibiliteRepository.rechercherTransfertsCollectiviteParAnnee(idCollectivite, annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.TransfertFongibiliteService#rechercherTransfertsParAnnee(java.lang.Integer)
     */
    @Override
    public List<TransfertFongibilite> rechercherTransfertsParAnnee(Integer annee) {
        return this.transfertFongibiliteRepository.rechercherTransfertsParAnnee(annee);

    }

}

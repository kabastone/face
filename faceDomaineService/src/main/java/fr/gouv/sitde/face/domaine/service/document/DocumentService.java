/**
 *
 */
package fr.gouv.sitde.face.domaine.service.document;

import java.io.File;
import java.util.List;
import java.util.Map;

import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;

/**
 * Interface du service des documents.
 *
 * @author Atos
 *
 */
public interface DocumentService {

    /**
     * Generer document pdf.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @param variablesContexte
     *            the variables contexte
     * @return Le fichier transfert du fichier PDF généré
     */
    FichierTransfert genererDocumentPdf(final TemplateDocumentEnum templateDocEnum, final Map<String, Object> variablesContexte);

    /**
     * Generer document pdf.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @param listVariablesContexte
     *            the list of variables contexte
     * @return Le fichier transfert du fichier PDF généré
     */
    FichierTransfert genererDocumentPdf(final TemplateDocumentEnum templateDocEnum, final List<Map<String, Object>> listVariablesContexte);

    /**
     * Rechercher document par id document.
     *
     * @param idDocument
     *            the id document
     * @return the document
     */
    Document rechercherDocumentParIdDocument(Long idDocument);

    /**
     * Rechercher document par id document.
     *
     * @param idDocument
     *            the id document
     * @param familleDocEnum
     *            the famille doc enum
     * @return the document
     */
    Document rechercherDocumentParIdDocumentEtFamille(Long idDocument, FamilleDocumentEnum familleDocEnum);

    /**
     * Charger fichier document.
     *
     * @param idDocument
     *            the id document
     * @return the file
     */
    File chargerFichierDocument(Long idDocument);

    /**
     * Supprimer document.
     *
     * @param idDocument
     *            the id document
     */
    void supprimerDocument(Long idDocument);

    /**
     * Modifier fichier document.
     *
     * @param idDocument
     *            the id document
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document modifierFichierDocument(Long idDocument, FichierTransfert fichierTransfert);

    /**
     * Creer document demande subvention.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @param fichierTransfert
     *            Le tranfert fichier
     * @return the document
     */
    Document creerDocumentDemandeSubvention(DemandeSubvention demandeSubvention, FichierTransfert fichierTransfert);

    /**
     * Creer document demande prolongation.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document creerDocumentDemandeProlongation(DemandeProlongation demandeProlongation, FichierTransfert fichierTransfert);

    /**
     * Creer document demande paiement.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document creerDocumentDemandePaiement(DemandePaiement demandePaiement, FichierTransfert fichierTransfert);

    /**
     * Creer document courrier departement.
     *
     * @param courriernDepartement
     *            the courrier departement
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document creerDocumentCourrierDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement, FichierTransfert fichierTransfert);

    /**
     * Creer document ligne dotation departement.
     *
     * @param ligneDotationDepartement
     *            the ligne dotation departement
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document creerDocumentLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement, FichierTransfert fichierTransfert);

    /**
     * Creer document modele.
     *
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document creerDocumentModele(FichierTransfert fichierTransfert);

    /**
     * Rechercher famille document par id document.
     *
     * @param idDocument
     *            the id document
     * @return the famille document
     */
    FamilleDocument rechercherFamilleDocumentParIdDocument(Long idDocument);

    /**
     * Zipper et encoder document.
     *
     * @param document
     *            the document
     * @return the byte[]
     */
    byte[] zipperEtEncoderDocument(Document document);

    /**
     * Rechercher documents dotation par id collectivite et annee.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @return the list
     */
    List<Document> rechercherDocumentsDotationParIdCollectiviteEtAnnee(Long idCollectivite, Integer annee);

    /**
     * Deplacer directory demande subvention.
     *
     * @param demandeSubvention
     *            the demande subvention avec DossierSubvention cible
     * @param idDossierSubventionOrigine
     *            the id dossier subvention origine
     * @param supprimerDirectoryDossier
     *            flag indiquant si il faut supprimer le repertoire du dossier de subvention d'origine
     */
    void deplacerDirectoryDemandeSubvention(DemandeSubvention demandeSubventionCible, DemandeSubvention demandeSubventionSource,
            boolean supprimerDirectoryDossier);

    /**
     * Rechercher documents dotation par code departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<Document> rechercherDocumentsDotationParCodeDepartement(String codeDepartement);

    /**
     * Encoder document.
     *
     * @param doc
     *            the doc
     * @return the byte[]
     */
    byte[] encoderDocument(Document doc);
}

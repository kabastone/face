/**
 *
 */
package fr.gouv.sitde.face.domaine.service.email.impl;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domaine.service.email.EmailProlongationService;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * The Class EmailProlongationServiceImpl.
 */
@Named
public class EmailProlongationServiceImpl implements EmailProlongationService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailProlongationServiceImpl.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

    /** The demande prolongation service. */
    @Inject
    private DemandeProlongationService demandeProlongationService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.email.EmailProlongationService#envoyerEmailDemandePrologationRefusee(java.lang.Long)
     */
    @Override
    public void envoyerEmailDemandeProlongationRefusee(Long idDemandeProlongation) {

        DemandeProlongation demande = this.demandeProlongationService.rechercherDemandeProlongation(idDemandeProlongation);
        DossierSubvention dossier = demande.getDossierSubvention();

        Map<String, Object> variablesContexte = new HashMap<>(2);
        variablesContexte.put("nomDossier", dossier.getNumDossier());
        variablesContexte.put("dateAchevement", demande.getDateAchevementDemandee());
        this.emailService.creerEtEnvoyerEmail(dossier, TypeEmailEnum.DOS_PROLONGATION_REFUSEE, TemplateEmailEnum.EMAIL_PROLONGATION_REFUSEE,
                variablesContexte);

        LOGGER.debug("Envoi de l'email pour le dossier {}", dossier.getNumDossier());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.email.EmailProlongationService#envoyerEmailDemandePrologationAcceptee(java.lang.Long)
     */
    @Override
    public void envoyerEmailDemandeProlongationAcceptee(Long idDemandeProlongation) {

        DemandeProlongation demande = this.demandeProlongationService.rechercherDemandeProlongation(idDemandeProlongation);
        DossierSubvention dossier = demande.getDossierSubvention();

        // Si le dossier n'a pas d'adresse mail, ne rien faire.
        if (dossier.getDotationCollectivite().getCollectivite().getAdressesEmail().isEmpty()) {
            return;
        }

        Map<String, Object> variablesContexte = new HashMap<>(1);
        variablesContexte.put("nomDossier", dossier.getNumDossier());
        this.emailService.creerEtEnvoyerEmail(dossier, TypeEmailEnum.DOS_PROLONGATION_ACCEPTEE, TemplateEmailEnum.EMAIL_PROLONGATION_ACCEPTEE,
                variablesContexte);

        LOGGER.debug("Envoi de l'email pour le dossier {}", dossier.getNumDossier());
    }

}

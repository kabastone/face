/**
 *
 */
package fr.gouv.sitde.face.domaine.service.email.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import fr.gouv.sitde.face.domain.spi.email.MailSenderService;
import fr.gouv.sitde.face.domaine.service.email.EmailContactService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.email.EmailContactDto;
import fr.gouv.sitde.face.transverse.entities.Email;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * The Class EmailContactServiceImpl.
 */
@Named
public class EmailContactServiceImpl extends BeanValidationService<EmailContactDto> implements EmailContactService {

    /** The mail sender service. */
    @Inject
    private MailSenderService mailSenderService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.email.EmailContactService#envoyerEmailContact(fr.gouv.sitde.face.transverse.email.EmailContactDto)
     */
    @Override
    public Boolean envoyerEmailContact(EmailContactDto emailContactDto) {

        List<MessageProperty> listeErreurs = this.getErreursValidationBean(emailContactDto, Default.class);
        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }

        Map<String, Object> variablesContexte = new HashMap<>(4);
        variablesContexte.put("nomPrenom", emailContactDto.getNom());
        variablesContexte.put("objetEmail", emailContactDto.getObjet());
        variablesContexte.put("messageEmail", emailContactDto.getMessage());
        variablesContexte.put("emailExpediteur", emailContactDto.getEmail());
        Email email = this.mailSenderService.construireEmail(TypeEmailEnum.CONTACT, TemplateEmailEnum.EMAIL_CONTACT, variablesContexte);
        return this.mailSenderService.envoyerEmailContact(email, emailContactDto.getEmail());
    }

}

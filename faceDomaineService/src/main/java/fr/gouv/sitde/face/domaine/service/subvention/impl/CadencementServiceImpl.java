/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository;
import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;

/**
 * @author A754839
 *
 */
@Named
public class CadencementServiceImpl extends BeanValidationService<Cadencement> implements CadencementService {

    @Inject
    private CadencementRepository cadencementRepository;

    @Inject
    private DemandeSubventionService demandeSubventionService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.CadencementService#recupererCadencementsPourCollectivite(java.lang.Long)
     */
    @Override
    public List<Cadencement> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet) {
        return this.cadencementRepository.recupererCadencementsPourCollectivite(idCollectivite, deProjet);
    }

    @Override
    public void validerMontantCadencement(Cadencement cd, BigDecimal aideDemandee) {
        Boolean montantValide = this.isMontantCadencementValide(cd, aideDemandee);
        if (!montantValide) {
            throw new RegleGestionException("cadencement.montant.total.incorrect");
        }

        cd.setEstValide(true);
    }

    /**
     * @param cd
     * @param aideDemandee
     * @return
     */
    @Override
    public Boolean isMontantCadencementValide(Cadencement cd, BigDecimal aideDemandee) {
        List<MessageProperty> erreurs = this.getErreursValidationBean(cd, Default.class);
        if (!erreurs.isEmpty()) {
            throw new RegleGestionException(erreurs, erreurs.get(0).getCleMessage());
        }
        BigDecimal totalMntCadencement = cd.getMontantAnnee1().add(cd.getMontantAnnee2()).add(cd.getMontantAnnee3()).add(cd.getMontantAnnee4())
                .add(cd.getMontantAnneeProlongation());
        return (totalMntCadencement.compareTo(aideDemandee) == 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.CadencementService#rechercherCadencementsPourMaj(java.lang.Long)
     */
    @Override
    public List<Cadencement> rechercherCadencementsPourMaj(Long idCollectivite) {
        return this.cadencementRepository.rechercherCadencementsPourMaj(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.subvention.CadencementService#enregistrerCadencement(fr.gouv.sitde.face.transverse.entities.Cadencement)
     */
    @Override
    public Cadencement enregistrerCadencement(Cadencement cadencement) {
        return this.cadencementRepository.enregistrerCadencement(cadencement);
    }

    @Override
    public Cadencement findCadencementById(Long id) {
        return this.cadencementRepository.findById(id);
    }

    /*
     * @see fr.gouv.sitde.face.domaine.service.subvention.CadencementService#modifierCadencement(fr.gouv.sitde.face.transverse.entities.Cadencement)
     */
    @Override
    public Cadencement modifierCadencement(Cadencement cadencement) {
        BigDecimal aideDemandee = this.demandeSubventionService.rechercherDemandeSubventionParId(cadencement.getId()).getPlafondAide();
        this.validerMontantCadencement(cadencement, aideDemandee);
        return this.cadencementRepository.enregistrerCadencement(cadencement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#estCadencementValide(java.lang.Long)
     */
    @Override
    public Boolean estToutCadencementValidePourCollectivite(Long idCollectivite) {
        return this.cadencementRepository.estToutCadencementValidePourCollectivite(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.CadencementService#rechercherCadencementParCodeNumerique(fr.gouv.sitde.face.transverse.
     * queryobject.CritereRechercheCadencementQo, java.lang.String)
     */
    @Override
    public List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche,
            String codeNumerique) {
        return this.cadencementRepository.rechercherCadencementParCodeNumerique(criteresRecherche, codeNumerique);
    }

}

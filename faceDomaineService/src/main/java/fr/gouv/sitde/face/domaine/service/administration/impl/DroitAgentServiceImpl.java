/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository;
import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository;
import fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository;
import fr.gouv.sitde.face.domaine.service.administration.DroitAgentService;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DroitAgent;
import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class DroitAgentServiceImpl.
 *
 * @author Atos
 */
@Named
public class DroitAgentServiceImpl implements DroitAgentService {

    /** The collectivite repository. */
    @Inject
    private CollectiviteRepository collectiviteRepository;

    /** The utilisateur repository. */
    @Inject
    private UtilisateurRepository utilisateurRepository;

    /** The droit agent repository. */
    @Inject
    private DroitAgentRepository droitAgentRepository;

    @Inject
    private AdresseEmailRepository adresseEmailRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.DroitAgentService#ajouterDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public DroitAgent ajouterDroitAgent(Long idCollectivite, Long idUtilisateur) {
        // vérif
        if ((idCollectivite == null) || (idUtilisateur == null)) {
            throw new TechniqueException("Pas de collectivité ou d'utilisateur spécifié");
        }

        // recherche de l'utilisateur
        Utilisateur utilisateur = this.utilisateurRepository.rechercherParId(idUtilisateur);

        // l'utilisateur n'existe pas
        if (utilisateur == null) {
            throw new TechniqueException("L'utilisateur spécifié n'existe pas");
        }

        // recherche de la collectivité
        Collectivite collectivite = this.collectiviteRepository.rechercherParId(idCollectivite);

        // si la collectivité n'existe pas
        if (collectivite == null) {
            throw new TechniqueException("La collectivité spécifiée n'existe pas");
        }

        // ajout l'utilisateur à la collectivité
        DroitAgent droitAgent = new DroitAgent();
        droitAgent.setCollectivite(collectivite);
        droitAgent.setUtilisateur(utilisateur);

        if (collectivite.getDroitsAgent().isEmpty()) {
            AdresseEmail adresseUtilisateur = new AdresseEmail();
            adresseUtilisateur.setAdresse(utilisateur.getEmail());
            adresseUtilisateur.setCollectivite(collectivite);
            this.adresseEmailRepository.ajouterAdresseEmail(adresseUtilisateur);
        }

        return this.droitAgentRepository.creerDroitAgent(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.DroitAgentService#creerDroitAgent(fr.gouv.sitde.face.transverse.entities.DroitAgent)
     */
    @Override
    public DroitAgent creerDroitAgent(DroitAgent droitAgent) {
        return this.droitAgentRepository.creerDroitAgent(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.DroitAgentService#modifierEtatAdminDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public DroitAgent modifierEtatAdminDroitAgent(Long idCollectivite, Long idUtilisateur) {
        // vérif
        if ((idCollectivite == null) || (idUtilisateur == null)) {
            throw new TechniqueException("Pas de collectivité ou d'utilisateur spécifié");
        }

        DroitAgent droitAgent = this.droitAgentRepository.rechercherDroitAgentParUtilisateurEtCollectivite(idUtilisateur, idCollectivite);

        // si la droit agent n'existe pas
        if (droitAgent == null) {
            throw new TechniqueException("Le droit agent spécifié n'existe pas");
        }

        if (droitAgent.isAdmin()) {
            droitAgent.setAdmin(false);
        } else {
            droitAgent.setAdmin(true);
        }

        return this.droitAgentRepository.modifierDroitAgent(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.DroitAgentService#supprimerDroitAgent(java.lang.Long, java.lang.Long)
     */
    @Override
    public void supprimerDroitAgent(Long idCollectivite, Long idUtilisateur) {
        // vérif
        if ((idCollectivite == null) || (idUtilisateur == null)) {
            throw new TechniqueException("Pas de collectivité ou d'utilisateur spécifié");
        }

        DroitAgent droitAgent = this.droitAgentRepository.rechercherDroitAgentParUtilisateurEtCollectivite(idUtilisateur, idCollectivite);

        // si la droit agent n'existe pas
        if (droitAgent == null) {
            throw new TechniqueException("Le droit agent spécifié n'existe pas");
        }

        this.droitAgentRepository.supprimerDroitAgent(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.DroitAgentService#rechercherDroitsAgentParCollectivite(java.lang.Long)
     */
    @Override
    public List<DroitAgent> rechercherDroitsAgentParCollectivite(Long idCollectivite) {

        return this.droitAgentRepository.rechercherDroitsAgentsParCollectivite(idCollectivite);
    }
}

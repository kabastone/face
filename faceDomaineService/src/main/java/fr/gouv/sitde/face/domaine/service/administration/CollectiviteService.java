package fr.gouv.sitde.face.domaine.service.administration;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Collectivite;

/**
 * The Interface CollectiviteService.
 */
public interface CollectiviteService {

    /**
     * Rechercher de toutes les collectivités.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivites();

    /**
     * Rechercher collectivites par code departement.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<Collectivite> rechercherCollectivitesParCodeDepartement(String codeDepartement);

    /**
     * Rechercher toutes collectivites pour mail.
     *
     * @return the list
     */
    List<Collectivite> rechercherToutesCollectivitesPourMail();

    /**
     * Recherche d'une collectivité par id.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the collectivite
     */
    Collectivite rechercherCollectiviteParId(Long idCollectivite);

    /**
     * Modifier collectivite.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite
     */
    Collectivite modifierCollectivite(Collectivite collectivite);

    /**
     * Creer collectivite.
     *
     * @param collectivite
     *            the collectivite
     * @return the collectivite
     */
    Collectivite creerCollectivite(Collectivite collectivite);

    /**
     * @param idCollectivite
     * @param idDemandeSubvention
     */
    void synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention);

    /**
     * Rechercher collectivite pour cadencement maj.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivitePourCadencementMaj();

    /**
     * Rechercher collectivite pour mail cadencement non valide.
     *
     * @return the list
     */
    List<Collectivite> rechercherCollectivitePourMailCadencementNonValide();

	List<Collectivite> rechercherCollectivitePourMailDotationNonConsomme();

}

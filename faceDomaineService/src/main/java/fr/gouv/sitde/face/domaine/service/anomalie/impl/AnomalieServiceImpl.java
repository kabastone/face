package fr.gouv.sitde.face.domaine.service.anomalie.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;

/**
 * The Class AnomalieServiceImpl.
 */
@Named
public class AnomalieServiceImpl implements AnomalieService {

    /** The anomalie repository. */
    @Inject
    private AnomalieRepository anomalieRepository;

    /** The type anomalie service. */
    @Inject
    private TypeAnomalieService typeAnomalieService;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande Paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rechercherAnomalieParId(java.lang.Long)
     */
    @Override
    public Anomalie rechercherAnomalieParId(Long idAnomalie) {
        return this.anomalieRepository.rechercherAnomalieParId(idAnomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#enregistrerAnomalie(fr.gouv.sitde.face.transverse.entities.Anomalie)
     */
    @Override
    public Anomalie creerEtValiderAnomalie(Anomalie anomalie) {
        // vérification du rattachement à une demande de paiement ou une demande de subvention
        boolean rattachement = false;
        if ((anomalie.getDemandePaiement() != null) && (anomalie.getDemandePaiement().getId() != null)) {
            rattachement = true;
            anomalie.setDemandePaiement(this.demandePaiementService.rechercherDemandePaiement(anomalie.getDemandePaiement().getId()));
        } else if (anomalie.getDemandePaiement() != null) {
            anomalie.setDemandePaiement(null);
        }

        if ((anomalie.getDemandeSubvention() != null) && (anomalie.getDemandeSubvention().getId() != null)) {
            rattachement = true;
            anomalie.setDemandeSubvention(this.demandeSubventionService.rechercherDemandeSubventionParId(anomalie.getDemandeSubvention().getId()));
        } else if (anomalie.getDemandePaiement() != null) {
            anomalie.setDemandeSubvention(null);
        }

        if (!rattachement) {
            throw new RegleGestionException("anomalie.aucun.rattachement");
        }

        // on vérifie la problématique
        if (StringUtils.isBlank(anomalie.getProblematique())) {
            throw new RegleGestionException("anomalie.problematique.vide");
        }

        anomalie.setTypeAnomalie(this.typeAnomalieService.rechercherParCode(anomalie.getTypeAnomalie().getCode()));

        // tout est ok : on enregistre
        Anomalie saveAnomalie = this.anomalieRepository.creerAnomalie(anomalie);
        if (!saveAnomalie.isCorrigee()) {
            if (saveAnomalie.getDemandeSubvention() != null) {
                this.demandeSubventionService.detecterAnomalieDemandeSubvention(saveAnomalie.getDemandeSubvention());
            } else if (saveAnomalie.getDemandePaiement() != null) {
                this.demandePaiementService.majDemandePaiementAnomalieDetectee(saveAnomalie.getDemandePaiement().getId());
            }
        }

        return saveAnomalie;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#enregistrerAnomalieSansFlush(fr.gouv.sitde.face.transverse.entities.Anomalie)
     */
    @Override
    public Anomalie modifierAnomalie(Anomalie anomalie) {

        // vérification du rattachement à une demande de paiement ou une demande de subvention
        boolean rattachement = false;
        if ((anomalie.getDemandePaiement() != null) && (anomalie.getDemandePaiement().getId() != null)) {
            rattachement = true;
            anomalie.setDemandePaiement(this.demandePaiementService.rechercherDemandePaiement(anomalie.getDemandePaiement().getId()));
        } else if (anomalie.getDemandePaiement() != null) {
            anomalie.setDemandePaiement(null);
        }

        if ((anomalie.getDemandeSubvention() != null) && (anomalie.getDemandeSubvention().getId() != null)) {
            rattachement = true;
            anomalie.setDemandeSubvention(this.demandeSubventionService.rechercherDemandeSubventionParId(anomalie.getDemandeSubvention().getId()));
        } else if (anomalie.getDemandePaiement() != null) {
            anomalie.setDemandeSubvention(null);
        }

        if (!rattachement) {
            throw new RegleGestionException("anomalie.aucun.rattachement");
        }

        // on vérifie la problématique
        if (StringUtils.isBlank(anomalie.getProblematique())) {
            throw new RegleGestionException("anomalie.problematique.vide");
        }

        anomalie.setTypeAnomalie(this.typeAnomalieService.rechercherParCode(anomalie.getTypeAnomalie().getCode()));
        anomalie.setCorrigee((anomalie.getReponse() != null) && !anomalie.getReponse().isEmpty());
        Anomalie savedAnomalie = this.anomalieRepository.modifierAnomalie(anomalie);

        if ((anomalie.getDemandeSubvention() != null)
                && !this.contientAnomaliesEtACorrigerParIdDemandeSubvention(anomalie.getDemandeSubvention().getId())
                && !EtatDemandeSubventionEnum.CORRIGEE.name().equals(anomalie.getDemandeSubvention().getEtatDemandeSubvention().getCode())) {
            this.demandeSubventionService.corrigerDemandeSubvention(anomalie.getDemandeSubvention().getId());
        }

        if ((anomalie.getDemandePaiement() != null) && !this.contientAnomaliesEtACorrigerParIdDemandePaiement(anomalie.getDemandePaiement().getId())
                && !EtatDemandePaiementEnum.CORRIGEE.name().equals(anomalie.getDemandePaiement().getEtatDemandePaiement().getCode())) {
            this.demandePaiementService.majDemandePaiementCorrigee(anomalie.getDemandePaiement().getId());
        }

        return savedAnomalie;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#creerAnomalie(fr.gouv.sitde.face.transverse.entities.Anomalie)
     */
    @Override
    public void creerAnomalie(Anomalie anomalie) {
        this.anomalieRepository.creerAnomalie(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#modifierAnomalieCorrigee(java.lang.Long, java.lang.Long, boolean)
     */
    @Override
    public Anomalie majCorrectionAnomaliePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee) {
        Anomalie anomalie = this.rechercherAnomalieParId(idAnomalie);

        if (anomalie == null) {
            throw new TechniqueException("L'anomalie n'existe pas.");
        }

        if ((anomalie.getDemandeSubvention() == null) || !anomalie.getDemandeSubvention().getId().equals(idDemandeSubvention)) {
            throw new TechniqueException("L'anomalie n'a pas de demande de subvention ou n'est pas lié à notre demande de subvention.");
        }

        // Changement de valeur 'corrigée' de l'anomalie.
        anomalie.setCorrigee(corrigee);

        return this.creerEtValiderAnomalie(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#modifierAnomalieCorrigeePourPaiement(java.lang.Long, java.lang.Long, boolean)
     */
    @Override
    public Anomalie majCorrectionAnomaliePourPaiement(Long idDemandePaiement, Long idAnomalie, boolean corrigee) {
        Anomalie anomalie = this.rechercherAnomalieParId(idAnomalie);

        if (anomalie == null) {
            throw new TechniqueException("L'anomalie n'existe pas.");
        }

        if ((anomalie.getDemandePaiement() == null) || !anomalie.getDemandePaiement().getId().equals(idDemandePaiement)) {
            throw new TechniqueException("L'anomalie n'a pas de demande de paiement ou n'est pas lié à notre demande de paiement.");
        }

        // Changement de valeur 'corrigée' de l'anomalie.
        anomalie.setCorrigee(corrigee);

        return this.creerEtValiderAnomalie(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesACorriger(java.util.Set)
     */
    @Override
    public boolean contientAnomaliesEtACorriger(Set<Anomalie> anomalies) {
        return !anomalies.isEmpty() && anomalies.stream().anyMatch(anomalie -> !anomalie.isCorrigee());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesACorriger(java.util.Set)
     */
    @Override
    public boolean contientAnomaliesEtACorrigerParIdDemandePaiement(Long idDemandePaiement) {
        return this.contientAnomaliesEtACorriger(this.anomalieRepository.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesACorrigerSansReponseParIdDemandePaiement(java.lang.Long)
     */
    @Override
    public boolean contientAnomaliesSansReponseParIdDemandePaiement(Long idDemandePaiement) {
        return this.contientAnomaliesSansReponse(this.anomalieRepository.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesEtACorrigerParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public boolean contientAnomaliesEtACorrigerParIdDemandeSubvention(Long idDemandeSubvention) {
        return this.contientAnomaliesEtACorriger(this.anomalieRepository.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesEtACorrigerSansReponseParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public boolean contientAnomaliesSansReponseParIdDemandeSubvention(Long idDemandeSubvention) {
        return this.contientAnomaliesSansReponse(this.anomalieRepository.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#contientAnomaliesACorriger(java.util.Set)
     */
    @Override
    public boolean contientAnomaliesSansReponse(Set<Anomalie> anomalies) {
        return !anomalies.isEmpty()
                && anomalies.stream().anyMatch(anomalie -> ((anomalie.getReponse() == null) || anomalie.getReponse().trim().isEmpty()));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rechercherAnomaliesParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public List<Anomalie> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention) {
        return new ArrayList<>(this.anomalieRepository.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rechercherAnomaliesParIdDemandePaiement(java.lang.Long)
     */
    @Override
    public List<Anomalie> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement) {
        return new ArrayList<>(this.anomalieRepository.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rechercherAnomaliesParIdDemandeSubventionPourAODE(java.lang.Long)
     */
    @Override
    public List<Anomalie> rechercherAnomaliesParIdDemandeSubventionPourAODE(Long idDemandeSubvention) {
        Set<Anomalie> anomalies = this.anomalieRepository.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention);
        anomalies.removeIf(anomalie -> !anomalie.isVisiblePourAODE());
        return new ArrayList<>(anomalies);

    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rechercherAnomaliesParIdDemandePaiementPourAODE(java.lang.Long)
     */
    @Override
    public List<Anomalie> rechercherAnomaliesParIdDemandePaiementPourAODE(Long idDemandePaiement) {
        Set<Anomalie> anomalies = this.anomalieRepository.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement);
        anomalies.removeIf(anomalie -> !anomalie.isVisiblePourAODE());
        return new ArrayList<>(anomalies);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rendreAnomaliesVisiblesPourAODE(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public void rendreAnomaliesDemandeSubventionVisiblesPourAODE(Long idDemandeSubvention) {
        this.anomalieRepository.rendreAnomaliesDemandeSubventionVisiblesPourAODE(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService#rendreAnomaliesDemandePaiementVisiblesPourAODE(java.lang.Long)
     */
    @Override
    public void rendreAnomaliesDemandePaiementVisiblesPourAODE(Long idDemandePaiement) {
        this.anomalieRepository.rendreAnomaliesDemandePaiementVisiblesPourAODE(idDemandePaiement);

    }
}

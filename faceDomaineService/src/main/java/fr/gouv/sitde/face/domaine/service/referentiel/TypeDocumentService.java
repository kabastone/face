/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel;

import fr.gouv.sitde.face.transverse.entities.TypeDocument;

/**
 * @author a453029
 *
 */
public interface TypeDocumentService {

    /**
     * Rechercher type document par id codifie.
     *
     * @param idCodifie
     *            the id codifie
     * @return the type document
     */
    TypeDocument rechercherTypeDocumentParIdCodifie(Integer idCodifie);

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.domaine.service.administration.AdresseService;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.subvention.SynchronisationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;

/**
 * The Class CollectiviteServiceImpl.
 *
 * @author Atos
 */
@Named
public class CollectiviteServiceImpl extends BeanValidationService<Collectivite> implements CollectiviteService {

    /** The collectivite repository. */
    @Inject
    private CollectiviteRepository collectiviteRepository;

    @Inject
    private AdresseService adresseService;

    @Inject
    private SynchronisationCollectiviteService synchronisationCollectiviteService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherCollectivitesParUtilisateurId(java.lang.Long)
     */
    @Override
    public List<Collectivite> rechercherCollectivites() {
        return this.collectiviteRepository.rechercherCollectivites();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherCollectivitesParCodeDepartement(java.lang.String)
     */
    @Override
    public List<Collectivite> rechercherCollectivitesParCodeDepartement(String codeDepartement) {
        return this.collectiviteRepository.rechercherCollectivitesParCodeDepartement(codeDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherCollectiviteParId(java.lang.Long)
     */
    @Override
    public Collectivite rechercherCollectiviteParId(Long idCollectivite) {
        return this.collectiviteRepository.rechercherParId(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#modifierCollectivite(fr.gouv.sitde.face.transverse.entities.Collectivite)
     */
    @Override
    public Collectivite modifierCollectivite(Collectivite collectivite) {
        this.validerCollectivite(collectivite);

        if (collectivite.getAdresseMoa() != null) {
            // Gestion des adresses selon leur présence et la collectivite.
            this.adresseService.gererAdressePourCollectivite(collectivite);
        }

        // Update de la collectivite.
        return this.collectiviteRepository.modifierCollectivite(collectivite);
    }

    /**
     * Valider collectivite.
     *
     * @param collectivite
     *            the collectivite
     */
    private void validerCollectivite(Collectivite collectivite) {
        List<MessageProperty> listeErreursValidation = this.getErreursValidationBean(collectivite, Default.class);
        listeErreursValidation.addAll(this.validerSiretUnique(collectivite));

        // Envoi des exceptions
        if (!listeErreursValidation.isEmpty()) {
            throw new RegleGestionException(listeErreursValidation, listeErreursValidation.get(0).getCleMessage());
        }
    }

    /**
     * @param collectivite
     * @return
     */
    private List<MessageProperty> validerSiretUnique(Collectivite collectivite) {
        List<MessageProperty> listeErreurSiret = new ArrayList<>();
        if (this.collectiviteRepository.verifierSiretUnicite(collectivite)) {
            MessageProperty errorMsg = new MessageProperty("siret.non.unique");
            listeErreurSiret.add(errorMsg);
        }
        return listeErreurSiret;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#creerCollectivite(fr.gouv.sitde.face.transverse.entities.Collectivite)
     */
    @Override
    public Collectivite creerCollectivite(Collectivite collectivite) {
        this.validerCollectivite(collectivite);
        return this.collectiviteRepository.creerCollectivite(collectivite);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherToutesCollectivitesPourMail()
     */
    @Override
    public List<Collectivite> rechercherToutesCollectivitesPourMail() {
        return this.collectiviteRepository.rechercherToutesCollectivitesPourMail();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#synchroniserCollectivite(java.lang.Long, java.lang.Long)
     */
    @Override
    public void synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention) {
        this.synchronisationCollectiviteService.synchroniserCollectivite(idCollectivite, idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherCollectivitePourCadencementMaj()
     */
    @Override
    public List<Collectivite> rechercherCollectivitePourCadencementMaj() {
        return this.collectiviteRepository.rechercherCollectivitePourCadencementMaj();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteService#rechercherCollectivitePourMailCadencementNonValide()
     */
    @Override
    public List<Collectivite> rechercherCollectivitePourMailCadencementNonValide() {
        return this.collectiviteRepository.rechercherCollectivitePourMailCadencementNonValide();

    }

	@Override
	public List<Collectivite> rechercherCollectivitePourMailDotationNonConsomme() {
		return this.collectiviteRepository.rechercherCollectivitePourMailDotationNonConsomme();
	}
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.batch.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.ChorusSuiviBatchRepository;
import fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Class ChorusSuiviBatchServiceImpl.
 *
 * @author Atos
 */
@Named
public class ChorusSuiviBatchServiceImpl implements ChorusSuiviBatchService {

    /** The chrorus suivi batch repository. */
    @Inject
    private ChorusSuiviBatchRepository chrorusSuiviBatchRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService#lireSuiviBatch()
     */
    @Override
    public ChorusSuiviBatch lireSuiviBatch() {
        return this.chrorusSuiviBatchRepository.lireSuiviBatch();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.batch.ChorusSuiviBatchService#mettreAJourSuiviBatch(fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch)
     */
    @Override
    public ChorusSuiviBatch mettreAJourSuiviBatch(ChorusSuiviBatch chorusSuiviBatch) {
        return this.chrorusSuiviBatchRepository.mettreAJourSuiviBatch(chorusSuiviBatch);
    }
}

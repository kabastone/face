/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.chorus.ChorusService;
import fr.gouv.sitde.face.domain.spi.repositories.referentiel.FluxChorusRepository;
import fr.gouv.sitde.face.domaine.service.administration.AdresseService;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.SynchronisationCollectiviteService;
import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;
import fr.gouv.sitde.face.transverse.chorus.WsChorusCodesErreursEnum;
import fr.gouv.sitde.face.transverse.entities.Adresse;
import fr.gouv.sitde.face.transverse.entities.Anomalie;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.referentiel.TypeAnomalieEnum;

/**
 * The Class SynchronisationCollectiviteServiceImpl.
 */
@Named
public class SynchronisationCollectiviteServiceImpl implements SynchronisationCollectiviteService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronisationCollectiviteServiceImpl.class);

    private static final String EN_ATTENTE = "EN_ATTENTE";

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The type anomalie service. */
    @Inject
    private TypeAnomalieService typeAnomalieService;

    /** The flux chorus repository. */
    @Inject
    private FluxChorusRepository fluxChorusRepository;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /** The adresse service. */
    @Inject
    private AdresseService adresseService;

    /** The client Chorus service. */
    @Inject
    private ChorusService chorusService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /*
     *
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.SynchronisationCollectiviteService#synchroniserCollectivite(java.lang.Long, java.lang.Long)
     */
    @Override
    public boolean synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention) {

        boolean synchroniserCollectiviteSucceed = false;

        Collectivite collectivite = this.collectiviteService.rechercherCollectiviteParId(idCollectivite);

        if (collectivite != null) {
            FluxChorus fluxChorus = this.fluxChorusRepository.rechercherFluxChorus();
            String codeSociete = fluxChorus.getCodeSociete();
            String organisationDachat = fluxChorus.getOrganisationDachat();

            // On appelle le service avec l'id chorus si disponible, sinon avec le siret collectivité
            String idFonctionnel = collectivite.getIdChorus() != null ? null : collectivite.getSiret();
            String idTechnique = collectivite.getIdChorus();

            try {
                List<CollectiviteChorus> collectivites = this.chorusService.rechercherCollectivites(codeSociete, idFonctionnel, idTechnique,
                        organisationDachat, null);

                if (CollectionUtils.isNotEmpty(collectivites) && (CollectionUtils.size(collectivites) == 1)) {

                    CollectiviteChorus collectiviteChorus = IterableUtils.get(collectivites, 0);
                    // mise à jour des champs par le résultat du WS Chorus
                    collectivite.setIdChorus(collectiviteChorus.getIdTechnique());
                    collectivite.setNomLong(collectiviteChorus.getNom());

                    collectivite.setChorusDateDerniereSynchro(collectiviteChorus.getDateSynchro());
                    collectivite.setChorusCodeKoDerniereSynchro(null);

                    if (collectivite.getAdresseMoa() != null) {

                        Adresse adresseMoa = collectivite.getAdresseMoa();

                        adresseMoa.setCodePostal(collectiviteChorus.getCodePostal());
                        adresseMoa.setCommune(collectiviteChorus.getVille());
                        adresseMoa.setNumeroVoie(collectiviteChorus.getNumeroRue());
                        adresseMoa.setNomVoie(collectiviteChorus.getNomRue());
                        adresseMoa.setComplement(collectiviteChorus.getComplementAdresse());

                        adresseMoa = this.adresseService.modifierAdresse(adresseMoa);
                        collectivite.setAdresseMoa(adresseMoa);

                    } else {

                        Adresse adresseMoa = new Adresse();
                        adresseMoa.setNom(SynchronisationCollectiviteServiceImpl.EN_ATTENTE);
                        adresseMoa.setCodePostal(collectiviteChorus.getCodePostal());
                        adresseMoa.setCommune(collectiviteChorus.getVille());
                        adresseMoa.setNumeroVoie(collectiviteChorus.getNumeroRue());
                        adresseMoa.setNomVoie(collectiviteChorus.getNomRue());
                        adresseMoa.setComplement(collectiviteChorus.getComplementAdresse());

                        adresseMoa = this.adresseService.creerAdresse(adresseMoa);
                        collectivite.setAdresseMoa(adresseMoa);
                    }

                    this.collectiviteService.modifierCollectivite(collectivite);

                    synchroniserCollectiviteSucceed = true;
                } else {
                    // Le ws chorus nous retourne plusieurs résultats
                    this.enregistreAnomalieEtCollectivite(idDemandeSubvention, collectivite, WsChorusCodesErreursEnum.WEBS2_01.getCode());
                }
            } catch (RegleGestionException rge) {
                LOGGER.info("RegleGestionException durant la synchronisation - ", rge);

                // L'appel au ws chorus a déclenché une RegleGestionException
                this.enregistreAnomalieEtCollectivite(idDemandeSubvention, collectivite, rge.getMessage());
            }
        }

        return synchroniserCollectiviteSucceed;
    }

    /**
     * Méthode centralisée de sauvegarde de l'anomalie liée à la demande de subvention ainsi que du champ ChorusCodeKoDerniereSynchro de la
     * collectivité
     *
     * @param idDemandeSubvention
     * @param collectivite
     * @param codeErreur
     */
    private void enregistreAnomalieEtCollectivite(Long idDemandeSubvention, Collectivite collectivite, String codeErreur) {

        // Un code erreur anormal a été renvoyé par le web service Chorus.
        WsChorusCodesErreursEnum wsChorusCodesErreursEnum = WsChorusCodesErreursEnum.valueOf(codeErreur);

        String messageErreur = "Erreur lors de l'appel au web service Chorus : Code : " + wsChorusCodesErreursEnum.getCode() + " - Détail : "
                + wsChorusCodesErreursEnum.getDescription();

        collectivite.setChorusCodeKoDerniereSynchro(wsChorusCodesErreursEnum.getCode());
        this.collectiviteService.modifierCollectivite(collectivite);

        DemandeSubvention demandeSubvention = this.demandeSubventionService.rechercherDemandeSubventionParId(idDemandeSubvention);

        Anomalie anomalie = new Anomalie();
        anomalie.setCorrigee(false);
        anomalie.setProblematique(messageErreur);
        anomalie.setTypeAnomalie(this.typeAnomalieService.rechercherParCode(TypeAnomalieEnum.ANOMALIE_CHORUS_SUBVENTION.getCode()));
        anomalie.setDemandeSubvention(demandeSubvention);

        this.anomalieService.creerEtValiderAnomalie(anomalie);
    }

}

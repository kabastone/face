/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface DemandePaiementService.
 */
public interface DemandePaiementService {

    /**
     * Rechercher demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercherDemandePaiement(Long idDemandePaiement);

    /**
     * Rechercher demande paiement avec documents.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercherDemandePaiementAvecDocuments(Long idDemandePaiement);

    /**
     * Rechercher demande paiement pour pdf.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement rechercherDemandePaiementPourPdf(Long idDemandePaiement);

    /**
     * Rechercher demandes paiement.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    List<DemandePaiement> rechercherDemandesPaiement(Long idDossierSubvention);

    /**
     * Realiser demande paiement.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @return the demande paiement
     */
    DemandePaiement realiserDemandePaiement(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader);

    /**
     * Compte accomptes pour dossier par id demande.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the integer
     */
    Integer compteAccomptesPourDossierParIdDemande(Long idDemandePaiement);

    /**
     * Maj demande paiement anomalie detectee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementAnomalieDetectee(Long idDemandePaiement);

    /**
     * Maj demande paiement anomalie signalee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementAnomalieSignalee(Long idDemandePaiement);

    /**
     * Maj demande paiement refusee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @param motifRefus
     *            the motif refus
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementRefusee(Long idDemandePaiement, String motifRefus);

    /**
     * Maj demande paiement qualifiee.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementQualifiee(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader);

    /**
     * Maj demande paiement accordee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementAccordee(Long idDemandePaiement);

    /**
     * Maj demande paiement controlee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementControlee(Long idDemandePaiement);

    /**
     * Maj demande paiement corrigee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementCorrigee(Long idDemandePaiement);

    /**
     * Maj demande paiement en cours transfert etape deux.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementEnCoursTransfertEtapeDeux(Long idDemandePaiement);

    /**
     * Rechercher demande paiement par id chorus.
     *
     * @param idChorus
     *            the id chorus
     * @return the demande paiement
     */
    DemandePaiement rechercherDemandePaiementParIdChorus(Long idChorus);

    /**
     * Maj demande paiement en attente transfert.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementEnAttenteTransfert(Long idDemandePaiement);

    /**
     * Maj demande paiement corrigee.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @param idsDocsComplSuppr
     *            the ids docs compl suppr
     * @return the demande paiement
     */
    DemandePaiement enregistrerDemandePaiementPourCorrection(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader,
            List<Long> idsDocsComplSuppr);

    /**
     * Maj demande paiement bdd.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementBdd(DemandePaiement demandePaiement);

    /**
     * Maj demande paiement transferee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementTransferee(Long idDemandePaiement);

    /**
     * Maj demande paiement certifiee.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementCertifiee(Long idDemandePaiement);

    /**
     * Rechercher toutes demandes paiement pour batch.
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesDemandesPaiementPourBatch();

    /**
     * Rechercher toutes pour repartition.
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesPourRepartition();

    /**
     * Maj demande paiement en cours transfert etape une.
     *
     * @param id
     *            the id
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementEnCoursTransfertEtapeUne(Long id);

    /**
     * Maj demande paiement versée.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementVersee(Long idDemandePaiement);

    /**
     * Gets the idfrom id ae.
     *
     * @param idAe
     *            the id AE
     * @return the idfrom id ae
     */
    Long getIdfromIdAe(Long idAe);

    /**
     * Rechercher toutes demandes paiement pour fen 0159 a.
     *
     *
     * @return the list
     */
    List<DemandePaiement> rechercherToutesDemandesPaiementPourFen0159a();

    /**
     * Maj demande paiement en cours transfert etape fin.
     *
     * @param id
     *            the id
     * @return the demande paiement
     */
    DemandePaiement majDemandePaiementEnCoursTransfertEtapeFin(Long id);

    /**
     * Rechercher certif non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    List<DemandePaiement> rechercherCertifNonNulParService(String chorusNumSf);

    /**
     * Rechercher versement non nul par service.
     *
     * @param chorusNumSf
     *            the chorus num sf
     * @return the list
     */
    List<DemandePaiement> rechercherVersementNonNulParService(String chorusNumSf);

    /**
     * Calculer avance maximum demande.
     *
     * @param dossier
     *            the dossier
     * @return the big decimal
     */
    BigDecimal calculerAvanceMaximumDemande(DossierSubvention dossier);

    /**
     * Calculer acompte maximum demande.
     *
     * @param dossier
     *            the dossier
     * @return the big decimal
     */
    BigDecimal calculerAcompteMaximumDemande(DossierSubvention dossier);

    /**
     * Checks if is est unique demande paiement en cours.
     *
     * @param nomLongCollectivite
     *            the nom long collectivite
     * @param numDossier
     *            the num dossier
     * @return true, if is est unique demande paiement en cours
     */
    boolean isEstUniqueDemandePaiementEnCours(String numDossier);

}

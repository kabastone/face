/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

/**
 * The Interface EtatDemandeSubventionService.
 *
 */
public interface EtatDemandeSubventionService {

    /**
     * Rechercher etat demande subvention par code.
     *
     * @param codeEtat
     *            the code etat
     * @return the etat demande subvention
     */
    EtatDemandeSubvention rechercherEtatDemandeSubventionParCode(String codeEtat);
}

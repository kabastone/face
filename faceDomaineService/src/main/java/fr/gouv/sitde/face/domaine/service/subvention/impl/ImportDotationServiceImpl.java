/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.PenaliteRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.domaine.service.subvention.ImportDotationService;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.entities.Penalite;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.imports.ImportLigneDotation;
import fr.gouv.sitde.face.transverse.imports.SousProgrammeTypeEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class ImportDotationServiceImpl.
 *
 * @author Atos
 */
@Named
public class ImportDotationServiceImpl implements ImportDotationService {

    /** The ligne dotation departement repository. */
    @Inject
    private LigneDotationDepartementRepository ligneDotationDepartementRepository;

    /** The penalite repository. */
    @Inject
    private PenaliteRepository penaliteRepository;

    /** The departement repository. */
    @Inject
    private DepartementRepository departementRepository;

    /** The dotation departement repository. */
    @Inject
    private DotationDepartementRepository dotationDepartementRepository;

    /** The dotation sous programme repository. */
    @Inject
    private DotationSousProgrammeRepository dotationSousProgrammeRepository;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** The Constant MILLE. */
    private static final BigDecimal MILLE = new BigDecimal(1_000);

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.ImportDotationService#ImporterDossierDodation(java.util.List, java.lang.Boolean)
     */
    @Override
    public Boolean importerDotation(List<ImportLigneDotation> listeImportLigneDotation) {

        int anneeEncours = this.timeOperationTransverse.getAnneeCourante();

        // execution regle de validation
        this.verifierCoherenceFichier(listeImportLigneDotation);

        // execution des regles d'action

        this.penaliteRepository.supprimerPenaliteParAnnee(anneeEncours);

        // On recupere la liste des departement, on la met dans une map pour ne pas tout reparcourir a chaque fois
        List<Departement> listeDepartement = this.departementRepository.rechercherDepartements();
        Map<String, Departement> hashDepartement = new HashMap<>(listeDepartement.size());
        String codeCourt;
        for (Departement departement : listeDepartement) {
            // on supprime les espaces et 0 en trop pour etre coherent avec le fichier chorus et independant du formatage dans la base
            codeCourt = StringUtils.removeStart(StringUtils.removeStart(StringUtils.trim(StringUtils.substring(departement.getCode(), 0, 3)), "00"),
                    "0");
            if (codeCourt.matches("20[AB]")) {
                codeCourt = codeCourt.replace("0", "");
            }
            hashDepartement.put(codeCourt, departement);
        }

        this.majPenalite(listeImportLigneDotation, anneeEncours, hashDepartement);

        this.ligneDotationDepartementRepository.razDotationAnnuelles(anneeEncours);

        this.ajoutDotationAnnuelles(listeImportLigneDotation, anneeEncours, hashDepartement);

        return true;
    }

    /**
     * RG_DOT_212-01.
     *
     * @return the boolean
     *
     *         true : si il n'existe pas de Dotation Annuelle notifiée avec une date d'envoi non null
     *
     *         false : si il en existe une
     */
    @Override
    public Boolean verifierDotationAnnuelleNotifiee() {
        int nbDotationNotifiee = 0;
        nbDotationNotifiee = this.ligneDotationDepartementRepository
                .compterDotationAnnuelleNotifieeAnneeEnCours(this.timeOperationTransverse.getAnneeCourante());
        return nbDotationNotifiee == 0;
    }

    /**
     * RG_DOT_212-02.
     *
     * @param listeImportLigneDotation
     *            the liste import ligne dotation
     */
    private void verifierCoherenceFichier(List<ImportLigneDotation> listeImportLigneDotation) {

        BigDecimal[] sommeSousProgramme = { BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO };

        String numeriqueDepartement = null;
        BigDecimal temp = null;
        BigDecimal read = null;

        // calcul de la somme des dotation annuelle par type de sous programme
        for (ImportLigneDotation importLigneDotation : listeImportLigneDotation) {
            numeriqueDepartement = importLigneDotation.getCodeDepartement();

            if (!StringUtils.isEmpty(numeriqueDepartement)) {

                for (SousProgrammeTypeEnum sousProgrammeType : SousProgrammeTypeEnum.values()) {
                    temp = sommeSousProgramme[sousProgrammeType.getIndex()];
                    // Les valeurs sont en k€ dans le fichier donc on multiplie par 1000
                    read = this.multiplierParMille(importLigneDotation.getMontantParSousProgramme(sousProgrammeType));
                    if (read != null) {
                        sommeSousProgramme[sousProgrammeType.getIndex()] = temp.add(read);
                    }
                }

            }
        }

        BigDecimal detailMontant;

        // Pour chaque sous programme on calcule la coherence
        for (SousProgrammeTypeEnum sousProgrammeType : SousProgrammeTypeEnum.values()) {
            // recuperation des dotation dans le SGBD (dotation - dotation 'annuelle')
            detailMontant = this.ligneDotationDepartementRepository.calculerDotationSansAnnuelle(this.timeOperationTransverse.getAnneeCourante(),
                    sousProgrammeType.getAbreviation());

            if (detailMontant == null) {
                detailMontant = BigDecimal.ZERO;
            }

            detailMontant = detailMontant.add(sommeSousProgramme[sousProgrammeType.getIndex()]);

            BigDecimal dotationSousProgramme = this.ligneDotationDepartementRepository
                    .getDotationSousProgramme(this.timeOperationTransverse.getAnneeCourante(), sousProgrammeType.getAbreviation());
            if (dotationSousProgramme == null) {
                dotationSousProgramme = BigDecimal.ZERO;
            }

            if (dotationSousProgramme.compareTo(detailMontant) < 0) {
                throw new RegleGestionException("subvention.import.dotation.incoherente", sousProgrammeType.getDescription(),
                        detailMontant.subtract(dotationSousProgramme).toString());
            }
        }

    }

    /**
     * RG_DOT_212-11.
     *
     * @param listeImportLigneDossierDotation
     *            the liste import ligne dossier dotation
     * @param anneeEnCours
     *            the annee en cours
     * @param hashDepartement
     *            the hash departement
     */
    private void majPenalite(List<ImportLigneDotation> listeImportLigneDossierDotation, int anneeEnCours, Map<String, Departement> hashDepartement) {

        Penalite penalite = null;
        for (ImportLigneDotation importLigneDotation : listeImportLigneDossierDotation) {
            String dept = importLigneDotation.getCodeDepartement();

            if (!StringUtils.isEmpty(dept)) {
                if (dept.matches("20[AB]")) {
                    dept = dept.replace("0", "");
                }
                penalite = new Penalite();

                penalite.setAnnee(anneeEnCours);
                penalite.setDepartement(hashDepartement.get(dept));
                // Les valeurs sont en k€ dans le fichier donc on multiplie par 1000
                penalite.setNonRegroupement(importLigneDotation.getPenaliteNonRegroupement());
                penalite.setStock(importLigneDotation.getPenaliteGestionStock());

                this.penaliteRepository.ajouterPenalite(penalite);
            }
        }

    }

    /**
     * RG_DOT_212-22.
     *
     * @param listeImportLigneDossierDotation
     *            the liste import ligne dossier dotation
     * @param anneeEncours
     *            the annee encours
     * @param hashDepartement
     *            the hash departement
     */
    private void ajoutDotationAnnuelles(List<ImportLigneDotation> listeImportLigneDossierDotation, int anneeEncours,
            Map<String, Departement> hashDepartement) {

        List<SousProgramme> listeSousProgrammes = this.sousProgrammeService.rechercherPourDotationDepartement();

        for (ImportLigneDotation importLigneDotation : listeImportLigneDossierDotation) {
            String numeriqueDepartement = importLigneDotation.getCodeDepartement();
            Departement departement = null;

            if (!StringUtils.isEmpty(numeriqueDepartement)) {
                for (SousProgramme sousProgramme : listeSousProgrammes) {
                    SousProgrammeTypeEnum sousProgrammeType = SousProgrammeTypeEnum.rechercherParAbreviation(sousProgramme.getAbreviation());
                    if (importLigneDotation.getMontantParSousProgramme(sousProgrammeType).compareTo(BigDecimal.ZERO) > 0) {
                        LigneDotationDepartement ligneDotationDepartement = new LigneDotationDepartement();
                        // Les valeurs sont en k€ dans le fichier donc on multiplie par 1000
                        ligneDotationDepartement
                                .setMontant(this.multiplierParMille(importLigneDotation.getMontantParSousProgramme(sousProgrammeType)));
                        ligneDotationDepartement.setTypeDotationDepartement(TypeDotationDepartementEnum.ANNUELLE);
                        ligneDotationDepartement.setDateEnvoi(null);

                        if (numeriqueDepartement.matches("20[AB]")) {
                            numeriqueDepartement = numeriqueDepartement.replace("0", "");
                        }

                        departement = hashDepartement.get(numeriqueDepartement);

                        ligneDotationDepartement
                                .setDotationDepartement(this.lireOuCreerDotationDepartement(anneeEncours, departement, sousProgramme));

                        this.ligneDotationDepartementRepository.ajouterLigneDotationDepartement(ligneDotationDepartement);
                    }
                }
            }
        }
    }

    /**
     * Recherche ou creation de la Dotation Departement.
     *
     * @param anneeEnCours
     *            the annee en cours
     * @param departement
     *            the departement
     * @param sousProgramme
     *            the sous programme
     * @return the dotation departement
     */
    private DotationDepartement lireOuCreerDotationDepartement(int anneeEnCours, Departement departement, SousProgramme sousProgramme) {
        DotationDepartement dotationDepartement = null;

        // on commence par la chercher dans la base
        dotationDepartement = this.dotationDepartementRepository.rechercherDotationDepartementTroisCriteres(anneeEnCours, departement.getCode(),
                sousProgramme.getCodeDomaineFonctionnel());

        // elle existe : OK
        if (dotationDepartement != null) {
            return dotationDepartement;
        }

        // elle n'existe pas : on la crée
        dotationDepartement = new DotationDepartement();
        dotationDepartement.setVersion(1);

        dotationDepartement.setDepartement(departement);

        dotationDepartement.setDotationSousProgramme(this.dotationSousProgrammeRepository
                .rechercherParSousProgrammeParAnneeEnCours(sousProgramme.getCodeDomaineFonctionnel(), anneeEnCours));

        return this.dotationDepartementRepository.ajouterDotationDepartement(dotationDepartement);
    }

    /**
     * Multiplier par mille.
     *
     * @param nombre
     *            the nombre
     * @return the big decimal
     */
    private BigDecimal multiplierParMille(BigDecimal nombre) {
        return nombre != null ? nombre.multiply(MILLE) : nombre;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import java.time.LocalDateTime;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * Classe métier de DemandeProlongation.
 *
 * @author Atos
 */
public interface DemandeProlongationService {

    /**
     * Recherche des demandes de prolongation via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandeProlongation> rechercherDemandesProlongation(Long idDossierSubvention);

    /**
     * Rechercher demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation rechercherDemandeProlongation(Long idDemandeProlongation);

    /**
     * Recherche par date achevement et dossier subvention.
     *
     * @param dateLivraisonDuPoste
     *            the date livraison du poste
     * @param numeroEJ
     *            the numero EJ
     * @return the demande prolongation
     */
    DemandeProlongation rechercheParDateAchevementEtDossierSubvention(LocalDateTime dateLivraisonDuPoste, String numeroEJ);

    /**
     * Mettre A jour demande prolongation.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation mettreAJourDemandeProlongation(DemandeProlongation demandeProlongation);

    /**
     * Inits the nouvelle demande prolongation.
     *
     * @param idDossier
     *            the id dossier
     * @return the demande prolongation
     */
    DemandeProlongation initNouvelleDemandeProlongation(Long idDossier);

    /**
     * Creer demande prolongation.
     *
     * @param demandeProlongation
     *            the demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation creerDemandeProlongation(DemandeProlongation demandeProlongation, List<FichierTransfert> fichiersTransfert);

    /**
     * Accorder demande prolongation.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     * @return the demande prolongation
     */
    DemandeProlongation accorderDemandeProlongation(Long idDemandeProlongation);

    /**
     * Refuser demande prolongation.
     *
     * @param demande
     *            the demande
     * @return the demande prolongation
     */
    DemandeProlongation refuserDemandeProlongation(DemandeProlongation demande);
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;

/**
 * Interface métier de DossierSubvention.
 *
 * @author Atos
 */
public interface DossierSubventionService {

    /**
     * Recherche d'un dossier de subvention par id avec calcul du reste à consommer, <br/>
     * du plafond d'aide et du taux de consommation.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the utilisateur
     */
    DossierSubvention rechercherDossierSubventionParId(Long idDossierSubvention);

    /**
     * Recherche paginée des dossiers de subvention selon les critères en paramètre.
     *
     * @param criteresRecherche
     *            critères de recherche
     * @return la page de réponse correspondante.
     */
    PageReponse<DossierSubvention> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche);

    /**
     * Algorithme de calcul du reste à consommer (aussi appellé reste disponible)<br/>
     * pour une demande de paiement, liée à un dossier de subvention.<br/>
     * Soustrait le plafond d'aide du dossier à la somme des transferses à l'état 'VERSE' ou 'DEMANDE'.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    BigDecimal calculerResteConsommer(DossierSubvention dossierSubvention);

    /**
     * Algorithme de calcul du taux de consommation.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    BigDecimal calculerTauxConsommation(DossierSubvention dossierSubvention);

    /**
     * Algorithme de calcul de l'aide versee.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    BigDecimal calculerAideVersee(DossierSubvention dossierSubvention);

    /**
     * Inits the nouveau dossier subvention.
     *
     * @return the dossier subvention
     */
    DossierSubvention initNouveauDossierSubvention();

    /**
     * Rechercher dossiers pour mail relance.
     *
     * @return the list
     */
    List<DossierSubvention> rechercherDossiersPourMailRelance();

    /**
     * Rechercher dossiers pour mail expiration.
     *
     * @return the list
     */
    List<DossierSubvention> rechercherDossiersPourMailExpiration();

    /**
     * Creer dossier subvention.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention
     */
    DossierSubvention creerDossierSubventionDepuisDemandeSubvention(DossierSubvention dossierSubvention);

    /**
     * Checks if is numero dossier unique.
     *
     * @param idDossier
     *            the id dossier
     * @param numDossier
     *            the num dossier
     * @return true, if is numero dossier unique
     */
    boolean isNumeroDossierUnique(Long idDossier, String numDossier);

    /**
     * Mettre A jour dossier subvention.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention
     */
    DossierSubvention mettreAJourDossierSubvention(DossierSubvention dossierSubvention);

    /**
     * Rechercher dossier subvention par id chorus ae.
     *
     * @param idAe
     *            the id ae
     * @return the dossier subvention
     */
    DossierSubvention rechercherDossierSubventionParIdChorusAe(Long idAe);

    /**
     * Supprimer dossier subvention par id.
     *
     * @param idDossier
     *            the id dossier
     */
    void supprimerDossierSubventionParId(Long idDossier);

    /**
     * Retrouve s'il existe le dossier de subvention ayant la meme collectivite, le meme sous programme et la meme annee que
     *
     * @param demande
     * @return
     */
    List<DossierSubvention> rechercherDossierPourDemande(DemandeSubvention demande);

    /**
     * Recherche paginée des dossiers de subvention selon les critères en paramètre.
     *
     * @param criteresRecherche
     * @return la page réponse contenant la liste des dossiers
     */
    PageReponse<DossierSubvention> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche);

	/**
	 * Rechercher dossier subvention cadencement defaut.
	 *
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionCadencementDefaut(String codeProgramme);

	/**
	 * Rechercher dossier subvention retard travaux.
	 *
	 * @param codeProgramme the code programme
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionRetardTravaux(String codeProgramme);

	/**
	 * Rechercher dossier subvention pour mail engagement travaux.
	 *
	 * @return the list
	 */
	List<DossierSubvention> rechercherDossierSubventionPourMailEngagementTravaux();

}

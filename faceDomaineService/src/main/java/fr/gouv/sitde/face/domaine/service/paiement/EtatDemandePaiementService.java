/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement;

import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;

/**
 * Classe métier de EtatDemandePaiement.
 *
 * @author Atos
 */
public interface EtatDemandePaiementService {

    /**
     * Recherche d'un état de demande de paiement par code.
     *
     * @param codeEtatdemande the code etatdemande
     * @return the demande paiement
     */
    EtatDemandePaiement rechercherParCode(String codeEtatdemande);
}

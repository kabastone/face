/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.administration.ListeDiffusionService;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;

/**
 * The Class ListeDiffusionServiceImpl.
 *
 * @author Atos
 */
@Named
public class ListeDiffusionServiceImpl implements ListeDiffusionService {

    /** The adresse email repository. */
    @Inject
    private AdresseEmailRepository adresseEmailRepository;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.ListeDiffusionService#rechercherAdressesEmail(java.lang.Long)
     */
    @Override
    public List<AdresseEmail> rechercherAdressesEmail(Long idCollectivite) {
        return this.adresseEmailRepository.rechercherAdressesEmail(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.ListeDiffusionService#supprimerAdresseEmail(java.lang.Long)
     */
    @Override
    public void supprimerAdresseEmail(Long idAdresseEmail) {
        // vérif
        if (idAdresseEmail == null) {
            throw new TechniqueException("Pas de collectivité ou d'utilisateur spécifié");
        }

        AdresseEmail adresse = this.adresseEmailRepository.rechercherAdresseEmail(idAdresseEmail);
        if (adresse.getCollectivite().getEtatCollectivite().equals(EtatCollectiviteEnum.CREEE)
                && (adresse.getCollectivite().getAdressesEmail().size() == 1)) {
            throw new RegleGestionException("collectivite.adressemail.derniere");
        }

        // suppression par id
        this.adresseEmailRepository.supprimerAdresseEmail(idAdresseEmail);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.ListeDiffusionService#ajouterAdresseEmailCollectivite(java.lang.String, java.lang.Long)
     */
    @Override
    public AdresseEmail ajouterAdresseEmailCollectivite(String adresseEmail, Long idCollectivite) {
        // vérif
        if (StringUtils.isBlank(adresseEmail) || (idCollectivite == null)) {
            throw new TechniqueException("Pas de collectivité ou d'adresse e-mail spécifiée");
        }

        // recherche de la collectivité
        Collectivite collectiviteExistante = this.collectiviteService.rechercherCollectiviteParId(idCollectivite);

        // si la collectivité n'existe pas
        if (collectiviteExistante == null) {
            throw new TechniqueException("La collectivité spécifiée n'existe pas");
        }

        // création de l'adresse mail
        AdresseEmail nouvelleAdresseEmail = new AdresseEmail();
        nouvelleAdresseEmail.setAdresse(adresseEmail);
        nouvelleAdresseEmail.setCollectivite(collectiviteExistante);

        // ajout de l'adresse en base
        return this.adresseEmailRepository.ajouterAdresseEmail(nouvelleAdresseEmail);
    }

}

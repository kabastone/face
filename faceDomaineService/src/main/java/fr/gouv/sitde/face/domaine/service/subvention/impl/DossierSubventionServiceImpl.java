/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service métier de DossierSubvention.
 *
 * @author Atos
 */
@Named
public class DossierSubventionServiceImpl extends BeanValidationService<DossierSubvention> implements DossierSubventionService {

    /** The Constant NB_MOIS_AVANT_ECHEANCE_POUR_RELANCE. */
    private static final long NB_MOIS_AVANT_ECHEANCE_POUR_RELANCE = 4;

    /** The dossier subvention repository. */
    @Inject
    private DossierSubventionRepository dossierSubventionRepository;

    /** The reglementaire service. */
    @Inject
    private ReglementaireService reglementaireService;

    /** The dotation collectivite. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossiersParCriteres(fr.gouv.sitde.face.transverse.queryobject.
     * CritereRechercheDossierPourDemandeSubventionQo)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche) {
        DossierSubventionServiceImpl.validerCriteres(criteresRecherche);
        return this.dossierSubventionRepository.rechercherDossiersParCriteres(criteresRecherche);
    }

    /**
     * Rechercher dossier subvention par id.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the dossier subvention
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossierSubventionParId(java.lang.Long)
     */
    @Override
    public DossierSubvention rechercherDossierSubventionParId(Long idDossierSubvention) {
        // vérif
        if (idDossierSubvention == null) {
            throw new TechniqueException("Pas d'id de dossier de subvention spécifié");
        }

        // recherche
        DossierSubvention dossierSubvention = this.dossierSubventionRepository.findDossierSubventionById(idDossierSubvention);

        this.calculerChampsDossierSubvention(dossierSubvention);

        return dossierSubvention;
    }

    /**
     * Calculer reste consommer.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#calculerResteConsommer(fr.gouv.sitde.face.transverse.entities.
     * DossierSubvention)
     */
    @Override
    public BigDecimal calculerResteConsommer(DossierSubvention dossierSubvention) {
        // vérif
        if (dossierSubvention == null) {
            throw new TechniqueException("Dossier de subvention null");
        }

        BigDecimal plafondAide = dossierSubvention.getPlafondAide();
        BigDecimal aideDemandee = dossierSubvention.getAideDemandee();

        return plafondAide.subtract(aideDemandee);
    }

    /**
     * Calculer taux consommation.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#calculerTauxConsommation(fr.gouv.sitde.face.transverse.entities.
     * DossierSubvention)
     */
    @Override
    public BigDecimal calculerTauxConsommation(DossierSubvention dossierSubvention) {
        // vérif
        if (dossierSubvention == null) {
            throw new TechniqueException("Dossier de subvention null");
        }

        // pas de division par zéro
        if (dossierSubvention.getPlafondAide().compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }

        // calcul du reste à consommer
        BigDecimal aideDemandee = dossierSubvention.getAideDemandee();

        // calcul du taux de consommation
        BigDecimal calculTauxConsommation = aideDemandee.divide(dossierSubvention.getPlafondAide(), 4, RoundingMode.HALF_UP);
        calculTauxConsommation = calculTauxConsommation.multiply(new BigDecimal("100"));
        return calculTauxConsommation.setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Calculer taux consommation.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the big decimal
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#calculerTauxConsommation(fr.gouv.sitde.face.transverse.entities.
     * DossierSubvention)
     */
    @Override
    public BigDecimal calculerAideVersee(DossierSubvention dossierSubvention) {
        // vérif
        if (dossierSubvention == null) {
            throw new TechniqueException("Dossier de subvention null");
        }

        BigDecimal aideVerseeCalcule = new BigDecimal(0);

        if (!dossierSubvention.getDemandesPaiement().isEmpty()) {

            Set<DemandePaiement> demandesPaiement = dossierSubvention.getDemandesPaiement();

            for (DemandePaiement demandePaiement : demandesPaiement) {
                if ("VERSEE".equals(demandePaiement.getEtatDemandePaiement().getCode())) {
                    aideVerseeCalcule = aideVerseeCalcule.add(new BigDecimal(demandePaiement.getAideDemandee().intValue()));
                }
            }
        }
        return aideVerseeCalcule;

    }

    /**
     * Inits the nouveau dossier subvention.
     *
     * @return the dossier subvention
     */
    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#initNouveauDossierSubvention()
     */
    @Override
    public DossierSubvention initNouveauDossierSubvention() {
        DossierSubvention dossier = new DossierSubvention();

        this.gererDatesNouveauDossier(dossier);

        return dossier;
    }

    /**
     * Calculer echeance dossier.
     *
     * @param nbYearToAdd
     *            the nb year to add
     * @return the date
     */
    private LocalDateTime calculerEcheanceDossier(int nbYearToAdd) {
        return this.timeOperationTransverse.get31DecemberOfCurrentYear().plusYears(nbYearToAdd);
    }

    /**
     * Calculer numero dossier.
     *
     * @param dossierSubvention
     *            the dossier subvention
     */
    private void calculerNumeroDossier(DossierSubvention dossierSubvention) {
        StringBuilder builder = new StringBuilder();

        builder.append(dossierSubvention.getDateCreationDossier().getYear());
        builder.append(
                dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme().getAbreviation());
        builder.append(StringUtils.leftPad(dossierSubvention.getDotationCollectivite().getCollectivite().getDepartement().getCode(), 3, '0'));
        builder.append(dossierSubvention.getDotationCollectivite().getCollectivite().getNumCollectivite());

        builder.append(this.calculerNumeroOrdreDossier(dossierSubvention));

        String numeroDossier = builder.toString();
        this.validerNumeroExistanDossier(numeroDossier);
        dossierSubvention.setNumDossier(numeroDossier);
    }

    /**
     * @param dossierSubvention
     * @return
     */
    private String calculerNumeroOrdreDossier(DossierSubvention dossierSubvention) {
        String numero = "";
        int numeroOrdre = this.recupererProchainNumeroOrdre(dossierSubvention);
        if (numeroOrdre > 0) {
            numero += '-';
            numero += numeroOrdre;
        }
        return numero;
    }

    /**
     * @param dossierSubvention
     * @return
     */
    private int recupererProchainNumeroOrdre(DossierSubvention dossierSubvention) {

        List<String> listeNumeroDossier = this.dossierSubventionRepository.recupererListeNumeroDossierParDotationCollectivite(dossierSubvention);

        if (listeNumeroDossier.isEmpty()) {
            return 0;
        }

        int plusHautNumeroOrdre = 0;
        for (String numero : listeNumeroDossier) {
            String[] splitNumero = numero.split("-");
            if ((splitNumero.length == 2) && (Integer.parseInt(splitNumero[1]) > plusHautNumeroOrdre)) {
                plusHautNumeroOrdre = Integer.parseInt(splitNumero[1]);
            }
        }
        return plusHautNumeroOrdre + 1;
    }

    /**
     * @param numeroDossier
     * @throws RegleGestionException
     */
    private void validerNumeroExistanDossier(String numeroDossier) {
        if (this.dossierSubventionRepository.findDossierSubventionParNumero(numeroDossier) != null) {
            throw new RegleGestionException("dossier.numero.non.unique");
        }
    }

    /**
     * Calcul des champs du dossier subvention.
     *
     * @param dossierSubvention
     *            the dossier subvention
     */
    private void calculerChampsDossierSubvention(DossierSubvention dossierSubvention) {
        dossierSubvention.setResteConsommerCalcule(this.calculerResteConsommer(dossierSubvention));
        dossierSubvention.setTauxConsommationCalcule(this.calculerTauxConsommation(dossierSubvention));
        dossierSubvention.setAideVerseeCalcule(this.calculerAideVersee(dossierSubvention));
    }

    /**
     * Calcul des champs du dossier subvention.
     *
     * @param dossierSubvention
     *            the dossier subvention
     */
    private void calculerChampsDossierSubventionPourMail(DossierSubvention dossierSubvention) {
        dossierSubvention.setResteConsommerCalcule(this.calculerResteConsommer(dossierSubvention));
        dossierSubvention.setTauxConsommationCalcule(this.calculerTauxConsommation(dossierSubvention));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossiersPourMailRelance()
     */
    @Override
    public List<DossierSubvention> rechercherDossiersPourMailRelance() {

        // Création de la date de relance.
        LocalDateTime dateRelanceMail = this.createDateRelance();

        // Recherche.
        List<DossierSubvention> dossierSubvention = this.dossierSubventionRepository.findDossierSubventionPourMailRelance(dateRelanceMail);

        // Calcul des champs manquants.
        for (DossierSubvention dossier : dossierSubvention) {
            this.calculerChampsDossierSubventionPourMail(dossier);
        }

        return dossierSubvention;
    }

    /**
     * Creates the date relance.
     *
     * @return the offset date time
     */
    protected LocalDateTime createDateRelance() {
        return this.timeOperationTransverse.now().plusMonths(NB_MOIS_AVANT_ECHEANCE_POUR_RELANCE);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossiersPourMailExpiration()
     */
    @Override
    public List<DossierSubvention> rechercherDossiersPourMailExpiration() {

        // Création de la date d'expiration.
        LocalDateTime dateExpirationMail = this.createDateExpiration();

        // Recherche.
        List<DossierSubvention> dossierSubvention = this.dossierSubventionRepository.findDossierSubventionPourMailExpiration(dateExpirationMail);

        // Calcul des champs manquants.
        for (DossierSubvention dossier : dossierSubvention) {
            this.calculerChampsDossierSubventionPourMail(dossier);
        }

        return dossierSubvention;
    }

    /**
     * Creates the date expiration.
     *
     * @return the offset date time
     */
    protected LocalDateTime createDateExpiration() {
        return this.timeOperationTransverse.now();
    }

    /**
     * Creer dossier subvention depuis demande subvention.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @return the dossier subvention
     */
    @Override
    public DossierSubvention creerDossierSubventionDepuisDemandeSubvention(DossierSubvention dossierSubvention) {
        DossierSubventionServiceImpl.validerDossierPourCreation(dossierSubvention);
        this.gererDotationCollectiviteDossier(dossierSubvention);
        this.gererDatesDossier(dossierSubvention);
        this.calculerNumeroDossier(dossierSubvention);

        dossierSubvention.setEtatDossier(EtatDossierEnum.OUVERT);
        return this.dossierSubventionRepository.enregistrer(dossierSubvention);
    }

    /**
     * @param dossierSubvention
     */
    private void gererDotationCollectiviteDossier(DossierSubvention dossierSubvention) {
        // Récupération de la Dotation de collectivité.
        Long idCollectivite = dossierSubvention.getDotationCollectivite().getCollectivite().getId();
        Integer idSousProgramme = dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme()
                .getId();

        DotationCollectivite dco = this.dotationCollectiviteService.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(idCollectivite,
                idSousProgramme);

        // Si la dotation collectivite n'existe pas, on fait la création.
        if (dco == null) {
            dco = this.dotationCollectiviteService.creerDotationCollectivite(idCollectivite, idSousProgramme);
        }

        dossierSubvention.setDotationCollectivite(dco);
    }

    /**
     * @param dossierSubvention
     */
    private void gererDatesDossier(DossierSubvention dossierSubvention) {
        // Délais réglementaire
        Reglementaire regle = this.reglementaireService.rechercherReglementaire(
                dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
        this.gererDatesSelonReglementaire(dossierSubvention, regle);
    }

    /**
     * @param dossierSubvention
     */
    private void gererDatesNouveauDossier(DossierSubvention dossierSubvention) {
        // Délais réglementaire
        Reglementaire regle = this.reglementaireService.rechercherReglementaire();
        this.gererDatesSelonReglementaire(dossierSubvention, regle);
    }

    /**
     * @param dossierSubvention
     * @param regle
     */
    private void gererDatesSelonReglementaire(DossierSubvention dossierSubvention, Reglementaire regle) {
        int delaiReglementAchevement = regle.getNbAnFinTrxDefaut();
        int delaiReglementLancement = regle.getNbAnDebutTrx();
        dossierSubvention.setDateEcheanceAchevement(this.calculerEcheanceDossier(delaiReglementAchevement));
        dossierSubvention.setDateEcheanceLancement(this.calculerEcheanceDossier(delaiReglementLancement));

        // Date du jour
        dossierSubvention.setDateEtat(this.timeOperationTransverse.now());
        dossierSubvention.setDateCreationDossier(this.timeOperationTransverse.now());
    }

    /**
     * @param dossierSubvention
     * @throws TechniqueException
     */
    private static void validerDossierPourCreation(DossierSubvention dossierSubvention) {
        if ((dossierSubvention.getDotationCollectivite() == null) || (dossierSubvention.getDotationCollectivite().getCollectivite() == null)) {
            throw new TechniqueException("DotationCollectivite ou Collectivite est null");
        }
        if ((dossierSubvention.getDotationCollectivite().getDotationDepartement() == null)
                || (dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme() == null)
                || (dossierSubvention.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme() == null)) {
            throw new TechniqueException("DotationDepartement ou DotationSousProgramme ou SousProgramme est null");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#isNumeroDossierUnique(java.lang.Long, java.lang.String)
     */
    @Override
    public boolean isNumeroDossierUnique(Long idDossier, String numDossier) {
        return this.dossierSubventionRepository.isNumeroDossierUnique(idDossier, numDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#mettreAJourDossierSubvention(fr.gouv.sitde.face.transverse.entities.
     * DossierSubvention)
     */
    @Override
    public DossierSubvention mettreAJourDossierSubvention(DossierSubvention dossierSubvention) {
        return this.dossierSubventionRepository.enregistrer(dossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossierSubventionParIdChorusAe(java.lang.Long)
     */
    @Override
    public DossierSubvention rechercherDossierSubventionParIdChorusAe(Long idAe) {
        return this.dossierSubventionRepository.findDossierSubventionByIdChorusAe(idAe);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#supprimerDossierSubventionParId(java.lang.Long)
     */
    @Override
    public void supprimerDossierSubventionParId(Long idDossier) {
        this.dossierSubventionRepository.supprimerDossierSubventionParId(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#retrouverDossierPourDemande(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public List<DossierSubvention> rechercherDossierPourDemande(DemandeSubvention demande) {
        if (demande.getDossierSubvention().getId() != null) {
            List<DossierSubvention> listeDossier = new ArrayList<>();
            listeDossier.add(this.rechercherDossierSubventionParId(demande.getDossierSubvention().getId()));
            return listeDossier;
        }

        int annee = this.timeOperationTransverse.now().getYear();
        Long idCollectivite = demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getId();
        Integer idSousProgramme = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getId();

        return this.dossierSubventionRepository.rechercherDossierParAnneeCollectiviteEtSousProgramme(annee, idCollectivite, idSousProgramme);
    }

    /**
     * @param criteresRecherche
     * @throws RegleGestionException
     */
    private static void validerCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche) {
        // Au moins un critere de recherche doit être présent
        if ((criteresRecherche == null) || criteresRecherche.isEmpty()) {
            throw new RegleGestionException("dossier.recherche.critere.obligatoire");
        }
    }

    /**
     * @param criteresRecherche
     */
    private static void validerCriteres(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {
        // Au moins un critere de recherche doit être présent
        if ((criteresRecherche == null) || criteresRecherche.isEmpty()) {
            throw new RegleGestionException("dossier.recherche.critere.obligatoire");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService#rechercherDossiersParCriteresPaiement(fr.gouv.sitde.face.transverse.
     * queryobject.CritereRechercheDossierPourDemandePaiementQo)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {
        DossierSubventionServiceImpl.validerCriteres(criteresRecherche);
        return this.dossierSubventionRepository.rechercherDossiersParCriteresPaiement(criteresRecherche);
    }

    /**
     * Rechercher dossier subvention cadencement defaut.
     *
     * @return the list
     */
    @Override
    public List<DossierSubvention> rechercherDossierSubventionCadencementDefaut(String codeProgramme) {

        return this.dossierSubventionRepository.rechercherDossierSubventionCadencementDefaut(codeProgramme);
    }

    /**
     * Rechercher dossier subvention retard travaux.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    @Override
    public List<DossierSubvention> rechercherDossierSubventionRetardTravaux(String codeProgramme) {
        return this.dossierSubventionRepository.rechercherDossierSubventionEnRetard(codeProgramme);
    }

	@Override
	public List<DossierSubvention> rechercherDossierSubventionPourMailEngagementTravaux() {
		return this.dossierSubventionRepository.rechercherDossierSubventionPourMailEngagementTravaux();
	}


}

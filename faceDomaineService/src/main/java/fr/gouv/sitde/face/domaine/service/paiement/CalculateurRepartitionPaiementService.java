/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement;

import fr.gouv.sitde.face.domaine.service.paiement.dto.MontantsRepartitionDto;
import fr.gouv.sitde.face.domaine.service.paiement.dto.RepartitionPaiementDto;

/**
 * The Interface CalculateurRepartitionPaiementService.
 *
 * @author A754839
 */
public interface CalculateurRepartitionPaiementService {

    /**
     * Repartir ancien solde.
     *
     * @param montants
     *            the montants
     */
    void repartirAncienSolde(MontantsRepartitionDto montantsDto);

    /**
     * Repartir ancien acompte.
     *
     * @param montants
     *            the montants
     */
    void repartirAncienAcompte(MontantsRepartitionDto montantsDto);

    /**
     * Repartir ancienne avance.
     *
     * @param montants
     *            the montants
     */
    void repartirAncienneAvance(MontantsRepartitionDto montantsDto);

    /**
     * Repartir nouveau solde.
     *
     * @param donneesRepartition
     *            the donneesRepartition
     */
    void repartirNouveauSolde(RepartitionPaiementDto donneesRepartition);

    /**
     * Repartir nouveau acompte.
     *
     * @param donneesRepartition
     *            the donneesRepartition
     */
    void repartirNouveauAcompte(RepartitionPaiementDto donneesRepartition);

    /**
     * Repartir nouvelle avance.
     *
     * @param donneesRepartition
     *            the donneesRepartition
     */
    void repartirNouvelleAvance(RepartitionPaiementDto donneesRepartition);

}

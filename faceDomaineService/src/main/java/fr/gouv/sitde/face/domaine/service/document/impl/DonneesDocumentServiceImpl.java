
/**
 *
 */
package fr.gouv.sitde.face.domaine.service.document.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.document.ConversionNombreService;
import fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository;
import fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.referentiel.MissionFaceService;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.transverse.document.LigneDotationDepartementAnnexeDto;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.entities.MissionFace;
import fr.gouv.sitde.face.transverse.entities.Penalite;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.OrdinauxNombresEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.referentiel.VersionAlgorithmeEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class DonneesDocumentServiceImpl.
 */
@Named
public class DonneesDocumentServiceImpl implements DonneesDocumentService {

    /** The Constant TAILLE_HASH_MAP. */
    private static final int TAILLE_HASH_MAP = 100;

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DonneesDocumentServiceImpl.class);

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE_DOCUMENTS);

    /** Le repository contenant toutes les informations hormis Reglementaire et MissionFace pour le pdf d'une demande de subvention. */
    @Inject
    private DemandeSubventionRepository demandeSubventionRepository;

    /** The demande paiement repository. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The ligne dotation departement repository. */
    @Inject
    private LigneDotationDepartementRepository ligneDotationDepartementRepository;

    /** Le service récupérant les données de réglementaire. */
    @Inject
    private ReglementaireService reglementaireService;

    /** Le service récupérant les données de réglementaire. */
    @Inject
    private MissionFaceService missionFaceService;

    /** Le service de gestion des documents. */
    @Inject
    private DocumentService documentService;

    /** Le service de traduction des BigDecimal en texte toutes lettres. */
    @Inject
    private ConversionNombreService conversionNombreService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /**
     * Récupération des données pour la génération du pdf.
     *
     * @param idDemandeSubvention
     *            identifiant de la demande de subvention désirée.
     *
     * @return Map contenant les variables pour Thymeleaf.
     */
    private Map<String, Object> donneesDemandeSubventionPdf(Long idDemandeSubvention) {

        // Récupération de la demande de subvention.
        DemandeSubvention demande = this.demandeSubventionRepository.rechercheDemandeSubventionPourPdfParId(idDemandeSubvention);

        if (demande == null) {
            throw new TechniqueException("Impossible de créer le document pdf sans demande de subvention.");
        }

        Map<String, Object> variablesContexte = new HashMap<>(TAILLE_HASH_MAP);

        // Récupération des champs voulus.
        variablesContexte.put("dsuEstDemandeComplementaire", demande.isDemandeComplementaire());
        variablesContexte.put("dsuDateDemandeAnnee", demande.getDateDemande().getYear());
        variablesContexte.put("dosChorusNumEj", demande.getDossierSubvention().getChorusNumEj());
        variablesContexte.put("dosNumDossier", demande.getDossierSubvention().getNumDossier());
        variablesContexte.put("dosDateEcheanceAchevement", demande.getDossierSubvention().getDateEcheanceAchevement().format(formatter));
        variablesContexte.put("colNomLong", demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getNomLong());
        variablesContexte.put("colReceveurNumCodique",
                demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getReceveurNumCodique());
        variablesContexte.put("sprNumSousAction", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getNumSousAction());
        variablesContexte.put("sprDescription", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getDescription());
        variablesContexte.put("proDesignation", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getProgramme().getDesignation());
        variablesContexte.put("proCodeNumerique", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getProgramme().getCodeNumerique());
        variablesContexte.put("bop", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getBop());
        variablesContexte.put("uo", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getUo());

        // récupération du plafond d'aide
        BigDecimal plafondAide = demande.getPlafondAide();
        variablesContexte.put("plafondAide", plafondAide);
        variablesContexte.put("plafondAideTexte", this.conversionNombreService.conversionBigDecimalEnToutesLettres(plafondAide));

        // Récupération des données de reglementaire.
        this.ajoutDonneesReglementaireDemandeSubventionPdf(variablesContexte, demande.getDossierSubvention().getDotationCollectivite()
                .getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());

        // Récupération des données de missionFace.
        this.ajoutDonneesMissionFaceDemandeSubventionPdf(variablesContexte);

        // Gestion de l'affichage de l'autre signataire selon le plafond d'aide.
        final int million = 1_000_000;
        variablesContexte.put("afficherSignataire", (plafondAide.compareTo(new BigDecimal(million)) >= 0));

        return variablesContexte;
    }

    /**
     * Récupération des données pour la génération du pdf demande de paiement.
     *
     * @param idDemandePaiement
     *            identifiant de la demande de paiement désirée.
     *
     * @return Map contenant les variables pour Thymeleaf.
     */
    private Map<String, Object> donneesDemandePaiementPdf(Long idDemandePaiement) {

        // Récupération de la demande de paiement.
        DemandePaiement demande = this.demandePaiementService.rechercherDemandePaiementPourPdf(idDemandePaiement);

        if (demande == null) {
            throw new TechniqueException("Impossible de créer le document pdf sans demande de paiement.");
        }

        Map<String, Object> variablesContexte = new HashMap<>(TAILLE_HASH_MAP);

        // Récupération des champs voulus.
        variablesContexte.put("dpaDateDemande", demande.getDateDemande().format(formatter));
        variablesContexte.put("dpaDateDemandeAnnee", demande.getDateDemande().getYear());
        variablesContexte.put("dosChorusNumEj", demande.getDossierSubvention().getChorusNumEj());
        variablesContexte.put("dosNumDossier", demande.getDossierSubvention().getNumDossier());
        variablesContexte.put("dosDateCreationDossier", demande.getDossierSubvention().getDateCreationDossier().format(formatter));
        variablesContexte.put("colNomLong", demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getNomLong());
        variablesContexte.put("colReceveurNumCodique",
                demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getReceveurNumCodique());
        variablesContexte.put("sprNumSousAction", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getNumSousAction());
        variablesContexte.put("sprDescription", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getDescription());
        variablesContexte.put("proDesignation", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getProgramme().getDesignation());
        variablesContexte.put("proCodeNumerique", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getProgramme().getCodeNumerique());
        variablesContexte.put("bop", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getBop());
        variablesContexte.put("uo", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getProgramme().getUo());

        // Gestion du type de paiement.
        boolean isAcompte = demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.ACOMPTE);
        boolean isAvance = demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE);
        String selonTypeDemande = isAvance ? "de l'" : "du ";
        variablesContexte.put("isAcompte", isAcompte);
        variablesContexte.put("selonTypeDemande", selonTypeDemande);
        variablesContexte.put("eSiAvance", isAvance);
        variablesContexte.put("typeDemande", demande.getTypeDemandePaiement().getLibelle().toLowerCase(Locale.FRENCH));

        // Récupération du nombre d'acomptes si c'en est un.
        if (isAcompte) {
            variablesContexte.put("ordinalAccompte", OrdinauxNombresEnum
                    .rechercherEnumParCode(this.demandePaiementService.compteAccomptesPourDossierParIdDemande(idDemandePaiement)).getLibelle() + " ");
        }

        // Calcul de l'aide demandée
        BigDecimal aideDemandee = demande.getAideDemandee();
        variablesContexte.put("aideDemandee", aideDemandee);
        variablesContexte.put("aideDemandeeTexte", this.conversionNombreService.conversionBigDecimalEnToutesLettres(aideDemandee));

        // Récupération de la date d'attribution venant de la plus ancienne demande de subvention.
        LocalDateTime date = this.timeOperationTransverse.now();
        for (DemandeSubvention dsu : demande.getDossierSubvention().getDemandesSubvention()) {
            if (dsu.getDateAttribution() != null) {
                date = (dsu.getDateAttribution().isBefore(date)) ? LocalDateTime.from(dsu.getDateAttribution()) : date;
            }
        }
        variablesContexte.put("dsuDateAttribution", date.format(formatter));
        // Récupération des données de reglementaire.
        this.ajoutDonneesReglementaireDemandeSubventionPdf(variablesContexte, demande.getDossierSubvention().getDotationCollectivite()
                .getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());

        // Récupération des données de missionFace.
        this.ajoutDonneesMissionFaceDemandeSubventionPdf(variablesContexte);

        return variablesContexte;
    }

    /**
     * Donnees annexe dotation.
     *
     * @param idDepartement
     *            the id departement
     * @return the map
     */
    private Map<String, Object> donneesAnnexeDotation(Integer idDepartement) {

        Integer anneeCourante = this.timeOperationTransverse.getAnneeCourante();

        List<LigneDotationDepartement> listeLigneAnnuelle = this.ligneDotationDepartementRepository
                .rechercherParDepartementEtTypePourAnnexe(idDepartement, TypeDotationDepartementEnum.ANNUELLE, anneeCourante);
        List<LigneDotationDepartement> listeLigneExceptionnelle = this.ligneDotationDepartementRepository
                .rechercherParDepartementEtTypePourAnnexe(idDepartement, TypeDotationDepartementEnum.EXCEPTIONNELLE, anneeCourante);

        // Récupération d'une dotation pour référence de date.
        LigneDotationDepartement dotationReference;
        if (!listeLigneAnnuelle.isEmpty()) {
            dotationReference = listeLigneAnnuelle.get(0);
        } else if (!listeLigneExceptionnelle.isEmpty()) {
            dotationReference = listeLigneExceptionnelle.get(0);
        } else {
            throw new TechniqueException("Impossible de créer le document pdf sans dotations.");
        }

        Map<String, Object> variablesContexte = new HashMap<>(TAILLE_HASH_MAP);

        // Récupération des données de missionFace.
        this.ajoutDonneesMissionFaceDemandeSubventionPdf(variablesContexte);

        // Mise en DTO des lignes dotation département annuelles pour thymeleaf.
        int tailleAnnuelle = listeLigneAnnuelle.size();
        List<LigneDotationDepartementAnnexeDto> listeLigneAnnuelleDTO = new ArrayList<>(tailleAnnuelle);
        BigDecimal sommeAnnuelle = new BigDecimal("0");
        BigDecimal sommeExceptionnelle = new BigDecimal("0");

        for (int i = 0; i < tailleAnnuelle; i++) {
            LigneDotationDepartement ldd = listeLigneAnnuelle.get(i);
            listeLigneAnnuelleDTO.add(i,
                    new LigneDotationDepartementAnnexeDto(ldd.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                            this.conversionNombreService.conversionBigDecimalEnKiloEuros(ldd.getMontant())));
            sommeAnnuelle = sommeAnnuelle.add(ldd.getMontant());
        }

        sommeAnnuelle = this.conversionNombreService.conversionBigDecimalEnKiloEuros(sommeAnnuelle);

        // Mise en DTO des lignes dotation département exceptionnelles pour thymeleaf.
        int tailleExceptionnelle = listeLigneExceptionnelle.size();
        List<LigneDotationDepartementAnnexeDto> listeLigneExceptionnelleDTO = new ArrayList<>(tailleExceptionnelle);
        for (int i = 0; i < tailleExceptionnelle; i++) {
            LigneDotationDepartement ldd = listeLigneExceptionnelle.get(i);
            listeLigneExceptionnelleDTO.add(i,
                    new LigneDotationDepartementAnnexeDto(ldd.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                            this.conversionNombreService.conversionBigDecimalEnKiloEuros(ldd.getMontant())));
            sommeExceptionnelle = sommeExceptionnelle.add(ldd.getMontant());
        }

        sommeExceptionnelle = this.conversionNombreService.conversionBigDecimalEnKiloEuros(sommeExceptionnelle);

        // Insertion des données dans la variable contexte.
        variablesContexte.put("dprAnnee", anneeCourante);
        variablesContexte.put("depNom", dotationReference.getDotationDepartement().getDepartement().getNom());
        variablesContexte.put("listeLigneDotationDepartementAnnuelle", listeLigneAnnuelleDTO);
        variablesContexte.put("listeLigneDotationDepartementExceptionnelle", listeLigneExceptionnelleDTO);

        LOGGER.debug(sommeAnnuelle.toPlainString());
        LOGGER.debug(sommeExceptionnelle.toPlainString());

        variablesContexte.put("sommeAnnuelle", sommeAnnuelle);
        variablesContexte.put("sommeExceptionnelle", sommeExceptionnelle);

        for (Penalite penalite : dotationReference.getDotationDepartement().getDepartement().getPenalites()) {
            if (anneeCourante.equals(penalite.getAnnee())) {
                if ((penalite.getNonRegroupement() != null) && (penalite.getNonRegroupement().compareTo(BigDecimal.ZERO) > 0)) {
                    variablesContexte.put("regroupement", penalite.getNonRegroupement());
                }
                if ((penalite.getStock() != null) && (penalite.getStock().compareTo(BigDecimal.ZERO) > 0)) {
                    variablesContexte.put("stock", penalite.getStock());
                }
            }
        }

        return variablesContexte;
    }

    /**
     * Donnees annexe dotation pour tous les départements.
     *
     * @return the list of map
     */
    private List<Map<String, Object>> donneesAnnexeDotation() {

        Integer anneeCourante = this.timeOperationTransverse.getAnneeCourante();

        List<LigneDotationDepartement> listeLigneAnnuelleTousDepartement = this.ligneDotationDepartementRepository
                .rechercherParTypePourAnnexe(TypeDotationDepartementEnum.ANNUELLE, anneeCourante);
        List<LigneDotationDepartement> listeLigneExceptionnelleTousDepartement = this.ligneDotationDepartementRepository
                .rechercherParTypePourAnnexe(TypeDotationDepartementEnum.EXCEPTIONNELLE, anneeCourante);

        // Je constitue la liste de dotationReference pour référence de date pour chaque département
        List<LigneDotationDepartement> listDotationReference = new ArrayList<>();

        // Je crée une map de LigneDotationDepartement par département à partir de la liste listeLigneAnnuelle
        Map<Integer, List<LigneDotationDepartement>> mapListeLigneAnnuelleTousDepartement = calculerMapListeAnnuelleTousDepartement(
                listeLigneAnnuelleTousDepartement, listDotationReference);

        // Je crée une map de LigneDotationDepartement par département à partir de la liste listeLigneExceptionnelle
        Map<Integer, List<LigneDotationDepartement>> mapListeLigneExceptionnelleTousDepartement = calculerMapListeLigneExceptionnelleTousDepartement(
                listeLigneExceptionnelleTousDepartement, listDotationReference, mapListeLigneAnnuelleTousDepartement);

        // je vais constituer une map VariablesContexte pour chacun des départements en me basant sur ma liste de dotations références
        List<Map<String, Object>> listVariablesContexte = new ArrayList<>(listDotationReference.size());

        for (LigneDotationDepartement dotationReference : listDotationReference) {

            Integer idDepartement = dotationReference.getDotationDepartement().getDepartement().getId();

            Map<String, Object> variablesContexte = new HashMap<>(TAILLE_HASH_MAP);
            BigDecimal sommeAnnuelle = new BigDecimal("0");
            BigDecimal sommeExceptionnelle = new BigDecimal("0");

            // Récupération des données de missionFace (rien de spécifique au département).
            this.ajoutDonneesMissionFaceDemandeSubventionPdf(variablesContexte);

            // Mise en DTO des lignes dotation département annuelles pour thymeleaf.
            int tailleAnnuelle = CollectionUtils.size(mapListeLigneAnnuelleTousDepartement.get(idDepartement));
            List<LigneDotationDepartementAnnexeDto> listeLigneAnnuelleDTO = new ArrayList<>(tailleAnnuelle);
            for (int i = 0; i < tailleAnnuelle; i++) {
                LigneDotationDepartement ldd = mapListeLigneAnnuelleTousDepartement.get(idDepartement).get(i);
                listeLigneAnnuelleDTO.add(i,
                        new LigneDotationDepartementAnnexeDto(
                                ldd.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                                this.conversionNombreService.conversionBigDecimalEnKiloEuros(ldd.getMontant())));
                sommeAnnuelle = sommeAnnuelle.add(ldd.getMontant());
            }

            sommeAnnuelle = this.conversionNombreService.conversionBigDecimalEnKiloEuros(sommeAnnuelle);

            // Mise en DTO des lignes dotation département exceptionnelles pour thymeleaf.
            int tailleExceptionnelle = CollectionUtils.size(mapListeLigneExceptionnelleTousDepartement.get(idDepartement));
            List<LigneDotationDepartementAnnexeDto> listeLigneExceptionnelleDTO = new ArrayList<>(tailleExceptionnelle);
            for (int i = 0; i < tailleExceptionnelle; i++) {
                LigneDotationDepartement ldd = mapListeLigneExceptionnelleTousDepartement.get(idDepartement).get(i);
                listeLigneExceptionnelleDTO.add(i,
                        new LigneDotationDepartementAnnexeDto(
                                ldd.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                                this.conversionNombreService.conversionBigDecimalEnKiloEuros(ldd.getMontant())));

                sommeExceptionnelle = sommeExceptionnelle.add(ldd.getMontant());
            }

            sommeExceptionnelle = this.conversionNombreService.conversionBigDecimalEnKiloEuros(sommeExceptionnelle);

            // Insertion des données dans la variable contexte.
            variablesContexte.put("dprAnnee", anneeCourante);
            variablesContexte.put("depNom", dotationReference.getDotationDepartement().getDepartement().getNom());
            variablesContexte.put("listeLigneDotationDepartementAnnuelle", listeLigneAnnuelleDTO);
            variablesContexte.put("listeLigneDotationDepartementExceptionnelle", listeLigneExceptionnelleDTO);
            variablesContexte.put("sommeAnnuelle", sommeAnnuelle);
            variablesContexte.put("sommeExceptionnelle", sommeExceptionnelle);

            for (Penalite penalite : dotationReference.getDotationDepartement().getDepartement().getPenalites()) {
                if (anneeCourante.equals(penalite.getAnnee())) {
                    if ((penalite.getNonRegroupement() != null) && (penalite.getNonRegroupement().compareTo(BigDecimal.ZERO) > 0)) {
                        variablesContexte.put("regroupement", penalite.getNonRegroupement());
                    }
                    if ((penalite.getStock() != null) && (penalite.getStock().compareTo(BigDecimal.ZERO) > 0)) {
                        variablesContexte.put("stock", penalite.getStock());
                    }
                }
            }

            listVariablesContexte.add(variablesContexte);
        }

        return listVariablesContexte;
    }

    /**
     * Calculer map liste annuelle tous departement.
     *
     * @param listeLigneAnnuelleTousDepartement
     *            the liste ligne annuelle tous departement
     * @param listDotationReference
     *            the list dotation reference
     * @return the map
     */
    private static Map<Integer, List<LigneDotationDepartement>> calculerMapListeAnnuelleTousDepartement(
            List<LigneDotationDepartement> listeLigneAnnuelleTousDepartement, List<LigneDotationDepartement> listDotationReference) {
        Map<Integer, List<LigneDotationDepartement>> mapListeLigneAnnuelleTousDepartement = new HashMap<>();

        for (LigneDotationDepartement ligneDotationDepartement : listeLigneAnnuelleTousDepartement) {

            if (!mapListeLigneAnnuelleTousDepartement.containsKey(ligneDotationDepartement.getDotationDepartement().getDepartement().getId())) {
                // J'ajoute la première ligneDotationDepartement de son département à la liste de dotationReference par département
                listDotationReference.add(ligneDotationDepartement);

                // J'initialise la liste des ligneDotationDepartement de la liste du département
                List<LigneDotationDepartement> listeLigneAnnuelleParDepartement = new ArrayList<>();
                listeLigneAnnuelleParDepartement.add(ligneDotationDepartement);
                mapListeLigneAnnuelleTousDepartement.put(ligneDotationDepartement.getDotationDepartement().getDepartement().getId(),
                        listeLigneAnnuelleParDepartement);
            } else {
                // J'ajoute ligneDotationDepartement à sa liste du département
                mapListeLigneAnnuelleTousDepartement.get(ligneDotationDepartement.getDotationDepartement().getDepartement().getId())
                        .add(ligneDotationDepartement);
            }

        }
        return mapListeLigneAnnuelleTousDepartement;
    }

    /**
     * Calculer map liste ligne exceptionnelle tous departement.
     *
     * @param listeLigneExceptionnelleTousDepartement
     *            the liste ligne exceptionnelle tous departement
     * @param listDotationReference
     *            the list dotation reference
     * @param mapListeLigneAnnuelleTousDepartement
     *            the map liste ligne annuelle tous departement
     * @return the map
     * @throws TechniqueException
     *             the technique exception
     */
    private static Map<Integer, List<LigneDotationDepartement>> calculerMapListeLigneExceptionnelleTousDepartement(
            List<LigneDotationDepartement> listeLigneExceptionnelleTousDepartement, List<LigneDotationDepartement> listDotationReference,
            Map<Integer, List<LigneDotationDepartement>> mapListeLigneAnnuelleTousDepartement) {
        Map<Integer, List<LigneDotationDepartement>> mapListeLigneExceptionnelleTousDepartement = new HashMap<>();

        for (LigneDotationDepartement ligneDotationDepartement : listeLigneExceptionnelleTousDepartement) {

            if (!mapListeLigneExceptionnelleTousDepartement.containsKey(ligneDotationDepartement.getDotationDepartement().getDepartement().getId())) {

                // Pour la première ligneDotationDepartement de son département, je vérifie si elle n'est pas déjà présente dans la liste de
                // mapListeLigneAnnuelleTousDepartement par département
                if (!mapListeLigneAnnuelleTousDepartement.containsKey(ligneDotationDepartement.getDotationDepartement().getDepartement().getId())) {
                    listDotationReference.add(ligneDotationDepartement);
                }

                // J'initialise la liste des ligneDotationDepartement de la liste du département
                List<LigneDotationDepartement> listeLigneExceptionnelleParDepartement = new ArrayList<>();
                listeLigneExceptionnelleParDepartement.add(ligneDotationDepartement);
                mapListeLigneExceptionnelleTousDepartement.put(ligneDotationDepartement.getDotationDepartement().getDepartement().getId(),
                        listeLigneExceptionnelleParDepartement);
            } else {
                // J'ajoute ligneDotationDepartement à sa liste du département
                mapListeLigneExceptionnelleTousDepartement.get(ligneDotationDepartement.getDotationDepartement().getDepartement().getId())
                        .add(ligneDotationDepartement);
            }

        }

        // Erreur en cas de liste de dotations référence vide
        if (listDotationReference.isEmpty()) {
            throw new RegleGestionException("Impossible de créer le document pdf sans dotations.");
        }
        return mapListeLigneExceptionnelleTousDepartement;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DemandeSubventionPdfService#genererDemandeSubventionPdf(java.util.Map)
     */
    @Override
    public FichierTransfert genererDemandeSubventionPdf(Long idDemandeSubvention) {

        // Récupération des données.
        Map<String, Object> variablesContexte = this.donneesDemandeSubventionPdf(idDemandeSubvention);

        // Génération du Pdf.
        return this.documentService.genererDocumentPdf(TemplateDocumentEnum.DOC_DEMANDE_SUBVENTION, variablesContexte);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService#genererDecisionAttributive(java.lang.Long)
     */
    @Override
    public FichierTransfert genererDecisionAttributive(Long idDemandePaiement) {

        // Récupération des données.
        Map<String, Object> variablesContexte = this.donneesDemandePaiementPdf(idDemandePaiement);

        // Génération du Pdf.
        FichierTransfert fichierTransfert = this.documentService.genererDocumentPdf(TemplateDocumentEnum.DOC_DEMANDE_PAIEMENT, variablesContexte);
        fichierTransfert.setTypeDocument(TypeDocumentEnum.DECISION_ATTRIBUTIVE_PAIE);
        return fichierTransfert;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService#genererAnnexeDotationPdf(java.lang.Long)
     */
    @Override
    public FichierTransfert genererAnnexeDotationPdf(Integer idDepartement) {
        // Récupération des données.
        Map<String, Object> variablesContexte = this.donneesAnnexeDotation(idDepartement);

        // Génération du Pdf.
        return this.documentService.genererDocumentPdf(TemplateDocumentEnum.DOC_ANNEXE_DOTATION, variablesContexte);
    }

    /*
     *
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DonneesDocumentService#genererAnnexeDotationPdf()
     */
    @Override
    public FichierTransfert genererAnnexeDotationPdf() {
        // Récupération des données.
        List<Map<String, Object>> listVariablesContexte = this.donneesAnnexeDotation();

        // Génération du Pdf.
        return this.documentService.genererDocumentPdf(TemplateDocumentEnum.DOC_ANNEXE_DOTATION, listVariablesContexte);
    }

    /**
     * Ajout donnees reglementaire demande subvention pdf.
     *
     * @param variablesContexte
     *            the variable contexte
     * @return the map
     */
    private void ajoutDonneesReglementaireDemandeSubventionPdf(Map<String, Object> variablesContexte, Integer anneeReglementaire) {

        // Recuperation des donnees venant de la BDD r_reglementaire.
        Reglementaire regle = this.reglementaireService.rechercherReglementaire(anneeReglementaire);

        // Verification de la presence d'un element au moins.
        if (regle == null) {
            throw new TechniqueException("Impossible d'obtenir les données venant de r_reglementaire pour pdf demande de subvention.");
        }

        // Recuperation des donnees necessaires.
        variablesContexte.put("regLoiOrganique", regle.getLoiOrganique());
        variablesContexte.put("regLoiOrganiqueModifiee", regle.getLoiOrganiqueModifiee());
        variablesContexte.put("regCodeColTerritorialesArticle1", regle.getCodeColTerritorialesArticle1());
        variablesContexte.put("regCodeColTerritorialesArticle2", regle.getCodeColTerritorialesArticle2());
        variablesContexte.put("regLoiFinances", regle.getLoiFinances());
        variablesContexte.put("regDecretCreditsDecouvertsAutorises", regle.getDecretCreditsDecouvertsAutorises());
        variablesContexte.put("regDecretAidesElectrificationRurale", regle.getDecretAidesElectrificationRurale());
        variablesContexte.put("regDecretModificatifAidesElectrificationRurale", regle.getDecretModificatifAidesElectrificationRurale());
        variablesContexte.put("regArrete", regle.getArrete());
        variablesContexte.put("regCirculaire", regle.getCirculaire());
        variablesContexte.put("regCategorie", regle.getCategorie());

        // Gestion de l'affichage de l'arrêté pour la V2 mais pas pour la V1
        variablesContexte.put("afficherV1", (VersionAlgorithmeEnum.V1.equals(regle.getVersionAlgorithme())));
        variablesContexte.put("afficherVX", (!VersionAlgorithmeEnum.V1.equals(regle.getVersionAlgorithme())));

    }

    /**
     * Ajout donnees mission face demande subvention pdf.
     *
     * @param variablesContexte
     *            the variable contexte
     * @return the map
     */
    private void ajoutDonneesMissionFaceDemandeSubventionPdf(Map<String, Object> variablesContexte) {

        // Recuperation des donnees venant de la BDD r_mission_face.
        MissionFace missionFace = this.missionFaceService.rechercherMissionFace();

        // Verification de la presence d'un element au moins.
        if (missionFace == null) {
            throw new TechniqueException("Impossible d'obtenir les données venant de r_mission_face pour pdf demande de subvention.");
        }

        // Recuperation des donnees necessaires.
        variablesContexte.put("misNomMinistere", missionFace.getMinistere());
        variablesContexte.put("misNomMission", missionFace.getMission());
        variablesContexte.put("misDirection", missionFace.getDirection());
        variablesContexte.put("misSousDirection", missionFace.getSousDirection());
        variablesContexte.put("misSignataireDCB", missionFace.getSignataireDCB());
    }
}

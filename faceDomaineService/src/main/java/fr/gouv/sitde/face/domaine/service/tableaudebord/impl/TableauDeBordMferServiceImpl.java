/**
 *
 */
package fr.gouv.sitde.face.domaine.service.tableaudebord.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Class TableauDeBordMferServiceImpl.
 *
 * @author a453029
 */
@Named
public class TableauDeBordMferServiceImpl implements TableauDeBordMferService {

    /** The tableau de bord mfer repository. */
    @Inject
    private TableauDeBordMferRepository tableauDeBordMferRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherTableauDeBordMfer()
     */
    @Override
    public TableauDeBordMferDto rechercherTableauDeBordMfer() {
        return this.tableauDeBordMferRepository.rechercherTableauDeBordMfer();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesSubventionTdbMferAqualifierSubvention(fr.gouv.sitde
     * .face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAqualifierSubvention(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesSubventionTdbMferAqualifierSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesPaiementTdbMferAqualifierPaiement(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAqualifierPaiement(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesPaiementTdbMferAqualifierPaiement(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesSubventionTdbMferAaccorderSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAaccorderSubvention(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesSubventionTdbMferAaccorderSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesPaiementTdbMferAaccorderPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAaccorderPaiement(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesPaiementTdbMferAaccorderPaiement(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesSubventionTdbMferAattribuerSubvention(fr.gouv.sitde
     * .face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAattribuerSubvention(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesSubventionTdbMferAattribuerSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesSubventionTdbMferAsignalerSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAsignalerSubvention(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesSubventionTdbMferAsignalerSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesPaiementTdbMferAsignalerPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAsignalerPaiement(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesPaiementTdbMferAsignalerPaiement(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordMferService#rechercherDemandesProlongationTdbMferAprolongerSubvention(fr.gouv.
     * sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeProlongation> rechercherDemandesProlongationTdbMferAprolongerSubvention(PageDemande pageDemande) {
        return this.tableauDeBordMferRepository.rechercherDemandesProlongationTdbMferAprolongerSubvention(pageDemande);
    }

}

package fr.gouv.sitde.face.domaine.service.dotation;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

public interface TransfertFongibiliteService {
    TransfertFongibilite enregistrerTransfert(TransfertFongibilite transfertFongibilite);

    /**
     * @param idCollectivite
     * @param annee
     * @return
     */
    List<TransfertFongibilite> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee);

    /**
     * @param annee
     * @return
     */
    List<TransfertFongibilite> rechercherTransfertsParAnnee(Integer annee);
}

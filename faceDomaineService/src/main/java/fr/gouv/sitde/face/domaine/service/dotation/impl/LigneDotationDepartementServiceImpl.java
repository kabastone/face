package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class LigneDotationDepartementServiceImpl.
 */
@Named
public class LigneDotationDepartementServiceImpl implements LigneDotationDepartementService {

    /** The ligne dotation departement repository. */
    @Inject
    private LigneDotationDepartementRepository ligneDotationDepartementRepository;

    /** The departement repository. */
    @Inject
    private DepartementRepository departementRepository;

    /** The dotation departement repository. */
    @Inject
    private DotationDepartementRepository dotationDepartementRepository;

    /** The dotation sous programme repository. */
    @Inject
    private DotationSousProgrammeRepository dotationSousProgrammeRepository;

    /** The dotation validateurs service. */
    @Inject
    private DotationValidateursService dotationValidateursService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    @Inject
    private SousProgrammeRepository sousProgrammeRepository;

    /**
     * Now.
     *
     * @return the offset date time
     */
    protected LocalDateTime now() {
        return this.timeOperationTransverse.now();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService#ajouterLigneDotationDepartement(fr.gouv.sitde.face.transverse.
     * entities.LigneDotationDepartement, fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum)
     */
    @Override
    public LigneDotationDepartement ajouterLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement,
            TypeDotationDepartementEnum typeDotationDepartement) {

        Integer anneeEnCours = this.now().getYear();
        Departement lDepartement = this.departementRepository
                .rechercherDepartementById(ligneDotationDepartement.getDotationDepartement().getDepartement().getId());
        short numeroOrdreSuivant = this.ligneDotationDepartementRepository.rechercherNumeroOrdreSuivant(lDepartement.getId());
        ligneDotationDepartement.setNumOrdre(numeroOrdreSuivant);
        ligneDotationDepartement.setTypeDotationDepartement(typeDotationDepartement);
        SousProgramme sousProgramme = this.sousProgrammeRepository.rechercherSousProgrammeParCodeDomaineFonctionnel(
                ligneDotationDepartement.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getCodeDomaineFonctionnel());
        DotationDepartement dotationDepartement = this.dotationDepartementRepository.rechercherDotationDepartementTroisCriteres(anneeEnCours,
                lDepartement.getCode(), sousProgramme.getCodeDomaineFonctionnel());
        // La dotation par département n'existe pas : on la crée
        if (dotationDepartement == null) {
            dotationDepartement = new DotationDepartement();
            dotationDepartement.setVersion(1);
            dotationDepartement.setDepartement(lDepartement);
            DotationSousProgramme lDotationSousProgramme = this.dotationSousProgrammeRepository
                    .rechercherParSousProgrammeParAnneeEnCours(sousProgramme.getCodeDomaineFonctionnel(), anneeEnCours);
            if (lDotationSousProgramme == null) {
                throw new RegleGestionException("dotation.sous.programme.inexistant",
                        ligneDotationDepartement.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription());
            }
            dotationDepartement.setDotationSousProgramme(lDotationSousProgramme);
            dotationDepartement = this.dotationDepartementRepository.ajouterDotationDepartement(dotationDepartement);
        }
        ligneDotationDepartement.setDotationDepartement(dotationDepartement);
        this.dotationValidateursService.validerDepartementAvecSousProgramme(dotationDepartement.getId(), ligneDotationDepartement.getMontant(),
                ligneDotationDepartement.getTypeDotationDepartement());
        return this.ligneDotationDepartementRepository.ajouterLigneDotationDepartement(ligneDotationDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation#rechercherLignesDotationDepartementaleEnPreparation(java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesDotationDepartementaleEnPreparation(Integer idDepartement) {
        Integer anneeEnCours = this.now().getYear();
        return this.ligneDotationDepartementRepository.rechercherLignesDotationDepartementaleEnPreparation(anneeEnCours, idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation#rechercherLignesDotationDepartementaleNotifiees(java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesDotationDepartementaleNotifiees(Integer idDepartement) {
        Integer anneeEnCours = this.now().getYear();
        return this.ligneDotationDepartementRepository.rechercherLignesDotationDepartementaleNotifiees(anneeEnCours, idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation#detruireLigneDotationDepartementale(java.lang.Integer)
     */
    @Override
    public void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale) {
        this.ligneDotationDepartementRepository.detruireLigneDotationDepartementale(idLigneDotationDepartementale);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService#rechercherDocumentNotificationPourLigne(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentNotificationPourLigne(Long idLigne) {
        LigneDotationDepartement ldd = this.ligneDotationDepartementRepository.rechercherLigneParId(idLigne);
        Document doc = this.ligneDotationDepartementRepository
                .recupereDocumentNotificationParDepartementEtDateEnvoi(ldd.getDotationDepartement().getDepartement().getId(), ldd.getDateEnvoi());
        if (doc == null) {
            throw new RegleGestionException("Le document n'a pas été trouvé.");
        }

        return doc;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService#rechercherLignesDotationDepartementale(java.math.BigDecimal,
     * java.lang.Integer, java.lang.String)
     */
    @Override
    public Long rechercherLignesDotationDepartementale(LocalDateTime dateAccord) {
        return this.ligneDotationDepartementRepository.rechercherLignesDotationDepartementale(dateAccord);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService#rechercherLignesPertesEtRenonciations()
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesPertesEtRenonciations(String codeProgramme) {
        return this.ligneDotationDepartementRepository.rechercherLignesPertesEtRenonciations(codeProgramme,
                this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService#rechercherLignesPertesEtRenonciationsParIdCollectivite(java.lang.
     * Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesPertesEtRenonciationsParIdCollectivite(Long idCollectivite) {
        return this.ligneDotationDepartementRepository.rechercherLignesPertesEtRenonciationsParIdCollectivite(idCollectivite,
                this.timeOperationTransverse.getAnneeCourante());

    }
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation;

import java.util.List;

import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleBo;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Interface DotationDepartementService.
 */
public interface DotationDepartementService {

    /**
     * Televerser et notifier dotation departement.
     *
     * @param idDepartement
     *            the id departement
     * @param fichierCourrierDotation
     *            the fichier courrier dotation
     */
    void televerserEtNotifierDotationDepartement(Integer idDepartement, FichierTransfert fichierCourrierDotation);

    /**
     * Lister les dotations départementales pour l'année en cours.
     *
     * @return the list
     */
    PageReponse<DotationDepartementaleBo> rechercherDotationsDepartementalesParAnneeEnCours(PageDemande pageDemande);

    /**
     * Rechercher dotation departement pour repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<DotationDepartement> rechercherDotationDepartementPourRepartition(String codeDepartement);

    /**
     * Rechercher dotation departement pour repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @param codeDomaineFonctionnel
     *            the code domaine fonctionnel
     * @return the dotation departement
     */
    DotationDepartement rechercherDotationDepartementPourRepartition(String codeDepartement, String codeDomaineFonctionnel);

    /**
     * Rechercher par id.
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @return the dotation departement
     */
    DotationDepartement rechercherParId(Long dotationDepartementId);

    /**
     * Rechercher dotation departement par id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation departement
     */
    DotationDepartement rechercherDotationDepartementParIdCollectivite(Long idCollectivite, Integer idSousProgramme);

    /**
     * Creer dotation departement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation departement
     */
    DotationDepartement creerDotationDepartement(Long idCollectivite, Integer idSousProgramme);
}

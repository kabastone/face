package fr.gouv.sitde.face.domaine.service.dotation;

import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface CourrierAnnuelDepartementService.
 */
public interface CourrierAnnuelDepartementService {

    /**
     * Creer courrier annuel departement.
     *
     * @param courrierAnnuelDepartement
     *            the courrier annuel departement
     * @return the courrier annuel departement
     */
    CourrierAnnuelDepartement creerCourrierAnnuelDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement);

    /**
     * Rechercher courrier annuel departement par code departement et annee.
     *
     * @param codeDepartement
     *            the code departement
     * @param annee
     *            the annee
     * @return the courrier annuel departement
     */
    CourrierAnnuelDepartement rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(String codeDepartement, Integer annee);

    /**
     * Televerser document courrier repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    Document televerserEtCreerDocumentCourrierRepartition(String codeDepartement, FichierTransfert fichierTransfert);

}

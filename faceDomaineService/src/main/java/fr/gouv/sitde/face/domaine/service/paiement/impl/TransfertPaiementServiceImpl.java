/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.document.ConversionNombreService;
import fr.gouv.sitde.face.domain.spi.repositories.TransfertPaiementRepository;
import fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * Service métier de TransfertPaiement.
 *
 * @author Atos
 */
@Named
public class TransfertPaiementServiceImpl extends BeanValidationService<TransfertPaiement> implements TransfertPaiementService {

    @Inject
    private ConversionNombreService conversionNombreService;

    /** The demande paiement repository. */
    @Inject
    private TransfertPaiementRepository transfertPaiementRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService#rechercheTransfersByDemandePaiement(java.lang.Long)
     */
    @Override
    public List<TransfertPaiement> rechercheTransfertByDossierSubvention(Long idDossierSubvention) {
        return this.transfertPaiementRepository.findTransfertPaiementByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService#formatageChorusMontant(java.math.BigDecimal)
     */
    @Override
    public String formatageChorusMontant(BigDecimal montant) {
        return this.conversionNombreService.formatageChorusMontant(montant);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService#save(fr.gouv.sitde.face.transverse.entities.TransfertPaiement)
     */
    @Override
    public TransfertPaiement saveTransfertPaiement(TransfertPaiement transfertPaiement) {
        return this.transfertPaiementRepository.saveTransfertPaiement(transfertPaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.TransfertPaiementService#rechercherParNumSFetLegacyetNumEJ(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public List<TransfertPaiement> rechercherParNumSFetLegacyetNumEJ(String chorusNumSF, String chorusNumLigneLegacy, String chorusNumEJ) {
        return this.transfertPaiementRepository.rechercherParNumSFetLegacyetNumEJ(chorusNumSF, chorusNumLigneLegacy, chorusNumEJ);
    }

}

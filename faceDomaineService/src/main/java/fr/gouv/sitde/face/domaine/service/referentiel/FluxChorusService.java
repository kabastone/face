/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel;

import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Interface FluxChorusService.
 */
public interface FluxChorusService {

    /**
     * Rechercher flux chorus.
     *
     * @return the flux chorus
     */
    FluxChorus rechercherFluxChorus();
}

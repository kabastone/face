/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * Classe métier de DemandeSubvention.
 *
 * @author Atos
 */
public interface DemandeSubventionService {

    /**
     * Recherche des demandes de subvention via l'id du dossier de subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subention
     * @return the list
     */
    List<DemandeSubvention> rechercherDemandesSubvention(Long idDossierSubvention);

    /**
     * Obtenir dto pour batch fen 0111 A.
     *
     * @return the list
     */
    List<DemandeSubvention> obtenirDtoPourBatchFen0111A();

    /**
     * Rechercher demande subvention principale (i.e. non complémentaire).
     *
     * @param idDossierChorus
     *            the id dossier chorus
     * @return the demande subvention
     */
    DemandeSubvention rechercherDemandeSubventionPrincipaleParIdChorus(Long idDossierChorus);

    /**
     * Rechercher demande subvention principale par id dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the demande subvention
     */
    DemandeSubvention rechercherDemandeSubventionPrincipaleParIdDossier(Long idDossier);

    /**
     * Rechercher demande subvention par id.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the demande subvention
     */
    DemandeSubvention rechercherDemandeSubventionParId(Long idDemandeSubvention);

    /**
     * Recuperer chorus num ligne legacy.
     *
     * @param idDossier
     *            the id dossier
     * @return the string
     */
    String recupererChorusNumLigneLegacy(Long idDossier);

    /**
     * Inits the nouvelle demande subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the demande subvention
     */
    DemandeSubvention initNouvelleDemandeSubvention(Long idDossierSubvention);

    /**
     * Mettre A jour demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention mettreAJourDemandeSubvention(DemandeSubvention demande);

    /**
     * Initier transfert demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubvention initierTransfertDemandeSubvention(Long id);

    /**
     * Creer demande subvention.
     *
     * @param demande
     *            the demande
     * @param fichiersTransferts
     *            the fichiers transferts
     * @return the demande subvention
     */
    DemandeSubvention creerDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts);

    /**
     * Qualifier demande subvention.
     *
     * @param demande
     *            the demande
     * @param fichiersTransferts
     *            the fichiers transferts
     * @return the demande subvention
     */
    DemandeSubvention qualifierDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts);

    /**
     * Accorder demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubvention accorderDemandeSubvention(Long id);

    /**
     * Controler demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubvention controlerDemandeSubvention(Long id);

    /**
     * Transferer demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubvention transfererDemandeSubvention(Long id);

    /**
     * Refuser demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention refuserDemandeSubvention(Long idDemande, String motifRefus);

    /**
     * Signaler anomalie demande subvention.
     *
     * @param idDemandeSubvention
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention signalerAnomalieDemandeSubvention(Long idDemandeSubvention);

    /**
     *
     * /** Detecter anomalie demande subvention.
     *
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    DemandeSubvention detecterAnomalieDemandeSubvention(DemandeSubvention demande);

    /**
     * Corriger demande subvention.
     *
     * @param demande
     *            the demande
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @param idsDocsComplSuppr
     *            the ids docs compl suppr
     * @return the demande subvention
     */
    DemandeSubvention enregistrerPourCorrectionDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> listeFichiersUploader,
            List<Long> idsDocsComplSuppr);

    /**
     * Attribuer demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param fichiersTransferts
     *            the fichiers transferts
     * @return the demande subvention
     */
    DemandeSubvention attribuerDemandeSubvention(Long idDemandeSubvention, List<FichierTransfert> fichiersTransferts);

    /**
     * Rechercher par Ej et poste.
     *
     * @param numeroEJ
     *            the numero EJ
     * @param numeroPoste
     *            the numero poste
     * @return the demande subvention
     */
    DemandeSubvention rechercherParEJetPoste(String numeroEJ, String numeroPoste);

    /**
     * Rechercher par EJ quantity etat.
     *
     * @param numeroEJ
     *            the numero EJ
     * @param plafond
     *            the plafond
     * @return the demande subvention
     */
    DemandeSubvention rechercherParEJQuantityEtat(String numeroEJ, BigDecimal plafond);

    /**
     * Recuperer document par demande et type.
     *
     * @param demande
     *            the demande
     * @param typeDocument
     *            the fiche siret
     * @return the document
     */
    Document recupererDocumentParDemandeEtType(DemandeSubvention demande, TypeDocumentEnum typeDocument);

    /**
     * Rechercher demandes subvention ordonnee par date demande.
     *
     * @param idDossier
     *            the id dossier
     * @return the list
     */
    List<DemandeSubvention> rechercherDemandesSubventionOrdonneeParDateDemande(Long idDossier);

    /**
     * Corriger demande subvention.
     *
     * @param id
     *            the id
     * @return the demande subvention
     */
    DemandeSubvention corrigerDemandeSubvention(Long id);

    /**
     * Rejeter pour taux demande subvention.
     *
     * @param id
     *            the id
     * @param tauxDemande
     *            the taux demande
     * @return the demande subvention
     */
    DemandeSubvention rejeterPourTauxDemandeSubvention(Long id, BigDecimal tauxDemande);

}

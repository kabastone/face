/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.dto;

import java.math.BigDecimal;

/**
 * @author A754839
 *
 */
public class MontantsRepartitionDto {

    private BigDecimal tempAvance;
    private BigDecimal tempAcompte;
    private BigDecimal tempSolde;
    private BigDecimal tempTotal;
    private BigDecimal montantTransfert;

    /**
     * @return the tempAvance
     */
    public BigDecimal getTempAvance() {
        return this.tempAvance;
    }

    /**
     * @param tempAvance
     *            the tempAvance to set
     */
    public void setTempAvance(BigDecimal tempAvance) {
        this.tempAvance = tempAvance;
    }

    /**
     * @return the tempAcompte
     */
    public BigDecimal getTempAcompte() {
        return this.tempAcompte;
    }

    /**
     * @param tempAcompte
     *            the tempAcompte to set
     */
    public void setTempAcompte(BigDecimal tempAcompte) {
        this.tempAcompte = tempAcompte;
    }

    /**
     * @return the tempSolde
     */
    public BigDecimal getTempSolde() {
        return this.tempSolde;
    }

    /**
     * @param tempSolde
     *            the tempSolde to set
     */
    public void setTempSolde(BigDecimal tempSolde) {
        this.tempSolde = tempSolde;
    }

    /**
     * @return the tempTotal
     */
    public BigDecimal getTempTotal() {
        return this.tempTotal;
    }

    /**
     * @param tempTotal
     *            the tempTotal to set
     */
    public void setTempTotal(BigDecimal tempTotal) {
        this.tempTotal = tempTotal;
    }

    /**
     * @return the montantTransfert
     */
    public BigDecimal getMontantTransfert() {
        return this.montantTransfert;
    }

    /**
     * @param montantTransfert
     *            the montantTransfert to set
     */
    public void setMontantTransfert(BigDecimal montantTransfert) {
        this.montantTransfert = montantTransfert;
    }
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.MissionFaceRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.MissionFaceService;
import fr.gouv.sitde.face.transverse.entities.MissionFace;

/**
 * The Class MissionFaceServiceImpl.
 */
@Named
public class MissionFaceServiceImpl implements MissionFaceService {

    /** Le repository contenant les infos venant de MissionFace. */
    @Inject
    private MissionFaceRepository missionFaceRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.MissionFaceService#rechercherMissionFace()
     */
    @Override
    public MissionFace rechercherMissionFace() {
        /** Recuperation des donnees venant de la BDD r_mission_face. */
        return this.missionFaceRepository.rechercherMissionFace();
    }

}

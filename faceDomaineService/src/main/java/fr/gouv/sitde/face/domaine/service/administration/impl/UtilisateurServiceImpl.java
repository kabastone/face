/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository;
import fr.gouv.sitde.face.domaine.service.administration.DroitAgentService;
import fr.gouv.sitde.face.domaine.service.administration.UtilisateurService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * Service métier de Utilisateur.
 *
 * @author Atos
 */
@Named
public class UtilisateurServiceImpl extends BeanValidationService<Utilisateur> implements UtilisateurService {

    /** The utilisateur repository. */
    @Inject
    private UtilisateurRepository utilisateurRepository;

    /** The droit agent service. */
    @Inject
    private DroitAgentService droitAgentService;

    /**
     * The Constant FLAG_NOM_PRENOM_EN_ATTENTE.<br/>
     * Ce flag indique que le nom et le prénom sont en attente de la prmière connexion Cerbere de l'utilisateur.
     */
    private static final String FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE = "EN_ATTENTE";

    private static final String FLAG_ID_CERBERE_EN_ATTENTE = "0000";

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.UtilisateurService#rechercherParId(java.lang.Long)
     */
    @Override
    public Utilisateur rechercherUtilisateurParId(Long idUtilisateur) {
        return this.utilisateurRepository.rechercherParId(idUtilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.UtilisateurService#rechercherUtilisateurParEmail(java.lang.String)
     */
    @Override
    public Utilisateur rechercherUtilisateurParEmail(String email) {
        return this.utilisateurRepository.rechercherParEmail(email);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.UtilisateurService#creerUtilisateurEnAttenteConnexion(fr.gouv.sitde.face.transverse.entities.
     * Utilisateur)
     */
    @Override
    public Utilisateur creerUtilisateurEnAttenteConnexion(Utilisateur utilisateur) {
        // On sette les flags en attente de connexion
        utilisateur.setCerbereId(FLAG_ID_CERBERE_EN_ATTENTE);
        utilisateur.setNom(FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE);
        utilisateur.setPrenom(FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE);
        utilisateur.setTelephone(FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE);

        // Validation de l'email de l'utilisateur
        this.validerUtilisateur(utilisateur);

        // Validation droits agent
        if (utilisateur.getDroitsAgent().size() != 1) {
            throw new TechniqueException(
                    "La creation d'un utilisateur en attente de connexion doit etre faite avec un droit agent sur une collectivite");
        }

        // Création de l'utilisateur en base
        utilisateur = this.utilisateurRepository.creerUtilisateur(utilisateur);

        // Rajout d'un droit agent à l'utilisateur créé.
        this.droitAgentService.ajouterDroitAgent(utilisateur.getDroitsAgent().iterator().next().getCollectivite().getId(), utilisateur.getId());

        return utilisateur;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.UtilisateurService#modifierUtilisateur(fr.gouv.sitde.face.transverse.entities.Utilisateur)
     */
    @Override
    public Utilisateur modifierUtilisateur(Utilisateur utilisateur) {
        this.validerUtilisateur(utilisateur);
        return this.utilisateurRepository.modifierUtilisateur(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.UtilisateurService#supprimerUtilisateur(java.lang.Long)
     */
    @Override
    public void supprimerUtilisateur(Long idUtilisateur) {
        this.utilisateurRepository.supprimerUtilisateur(idUtilisateur);
    }

    /**
     * Valider utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     */
    private void validerUtilisateur(Utilisateur utilisateur) {
        List<MessageProperty> listeErreurs = this.getErreursValidationBean(utilisateur, Default.class);

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.UtilisateurService#synchroniserUtilisateurConnecte(fr.gouv.sitde.face.transverse.entities.
     * Utilisateur)
     */
    @Override
    public Utilisateur synchroniserUtilisateurConnecte(Utilisateur utilisateur) {

        this.validerUtilisateur(utilisateur);

        // Recherche de l'utilisateur à synchroniser
        Utilisateur utilisateurExistant = this.rechercherUtilisateurParEmail(utilisateur.getEmail());

        // L'utilisateur n'existe pas encore en base, on le crée
        if (utilisateurExistant == null) {
            return this.utilisateurRepository.creerUtilisateur(utilisateur);

        } else {
            boolean modificationEstNecessaire = false;

            // L'utilisateur esite déjà en base
            // Mise à jour de l'idCerbere s'il a été modifié.
            if (!utilisateurExistant.getCerbereId().equals(utilisateur.getCerbereId())) {
                utilisateurExistant.setCerbereId(utilisateur.getCerbereId());
                modificationEstNecessaire = true;
            }
            // Mise à jour du nom et du prénom si le nom ou le prénom est égal au flag d'attente de connexion.
            // On est dans le cas de la première connexion de l'utilisateur.
            if (FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE.equals(utilisateurExistant.getNom())
                    || FLAG_NOM_PRENOM_TELEPHONE_EN_ATTENTE.equals(utilisateurExistant.getPrenom())) {
                utilisateurExistant.setNom(utilisateur.getNom());
                utilisateurExistant.setPrenom(utilisateur.getPrenom());
                modificationEstNecessaire = true;
            }

            // Mise à jour de l'utilisateur en base si nécessaire
            if (modificationEstNecessaire) {
                utilisateurExistant = this.utilisateurRepository.modifierUtilisateur(utilisateurExistant);
            }

            // On renvoie l'utilisateur existant éventuellement mis à jour
            return utilisateurExistant;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.UtilisateurService#rechercherTous(fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<Utilisateur> rechercherTousUtilisateurs(PageDemande pageDemande) {
        return this.utilisateurRepository.rechercherTous(pageDemande);
    }
}

package fr.gouv.sitde.face.domaine.service.referentiel;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.SousProgramme;

/**
 * The Interface SousProgrammeService.
 */
public interface SousProgrammeService {

    /**
     * Rechercher tous sous programmes.
     *
     * @return the list
     */
    List<SousProgramme> rechercherTousSousProgrammes();

    /**
     * Rechercher sous programmes pour renoncement dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite);

    /**
     * Rechercher sous programmes par collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesParCollectivite(Long idCollectivite);

    /**
     * Recherche sous programmes pour creation de ligne dotation departementale.
     *
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesPourLigneDotationDepartementale();

    /**
     * Rechercher par id.
     *
     * @param idSousProgramme
     *            the id sous programme
     * @return the sous programme
     */
    SousProgramme rechercherParId(Integer idSousProgramme);

    /**
     * Rechercher the sous programme par abreviation.
     *
     * @param abreviation
     *            the abreviation
     * @return sousProgramme
     */
    SousProgramme rechercherSousProgrammeParAbreviation(String abreviation);

    /**
     * Rechercher sous programme par id.
     *
     * @param idSousProgramme
     *            the id sous programme
     * @return the sous programme
     */
    SousProgramme rechercherSousProgrammeParId(Integer idSousProgramme);

    /**
     * Rechercher pour dotation departement.
     *
     * @return the list
     */
    List<SousProgramme> rechercherPourDotationDepartement();

    /**
     * Recherche un sous programme par id pour savoir s'il est de projet ou non
     * 
     * @param id
     */
    boolean estDeProjetParId(Integer id);
    
    /**
     * Rechercher sous programmes de projet.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<SousProgramme> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite);

    /**
     * @return
     */
    List<SousProgramme> rechercherSousProgrammesDeProjet();
}

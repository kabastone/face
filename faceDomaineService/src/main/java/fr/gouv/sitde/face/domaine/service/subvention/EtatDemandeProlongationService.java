/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;

/**
 * The Interface EtatDemandeProlongationService.
 *
 * @author Atos
 */
public interface EtatDemandeProlongationService {

    /**
     * Rechercher par code.
     *
     * @param codeEtatDemandeProlongation the code etat demande prolongation
     * @return the etat demande prolongation
     */
    EtatDemandeProlongation rechercherParCode(String codeEtatDemandeProlongation);

}

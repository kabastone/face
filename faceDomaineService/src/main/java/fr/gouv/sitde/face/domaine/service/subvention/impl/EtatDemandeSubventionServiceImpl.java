/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeSubventionRepository;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

/**
 * The Class EtatDemandeSubventionServiceImpl.
 *
 */
@Named
public class EtatDemandeSubventionServiceImpl implements EtatDemandeSubventionService {

    /** The etat demande subvention repository. */
    @Inject
    private EtatDemandeSubventionRepository etatDemandeSubventionRepository;

    /**
     * Rechercher etat demande subvention par code.
     *
     * @param codeEtat
     *            the code etat
     * @return the etat demande subvention
     */
    @Override
    public EtatDemandeSubvention rechercherEtatDemandeSubventionParCode(String codeEtat) {
        return this.etatDemandeSubventionRepository.findByCode(codeEtat);
    }

}

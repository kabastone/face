/**
 *
 */
package fr.gouv.sitde.face.domaine.service.email;

import java.util.Map;

import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * Interface du service d'email.
 *
 * @author Atos
 *
 */
public interface EmailService {

    /**
     * Creer et envoyer email.<br/>
     * Méthode qui construit et envoie l'email et l'enregistre en base.
     *
     * @param collectivite
     *            the collectivite
     * @param typeEmail
     *            the type email
     * @param templateEmail
     *            the template email
     * @param variablesContexte
     *            the variables contexte
     */
    void creerEtEnvoyerEmail(final Collectivite collectivite, final TypeEmailEnum typeEmail, final TemplateEmailEnum templateEmail,
            final Map<String, Object> variablesContexte);

    /**
     * Creer et envoyer email.<br/>
     * Méthode qui construit et envoie l'email et l'enregistre en base.
     *
     * @param dossierSubvention
     *            the dossier subvention
     * @param typeEmail
     *            the type email
     * @param templateEmail
     *            the template email
     * @param variablesContexte
     *            the variables contexte
     */
    void creerEtEnvoyerEmail(final DossierSubvention dossierSubvention, final TypeEmailEnum typeEmail, final TemplateEmailEnum templateEmail,
            final Map<String, Object> variablesContexte);

}

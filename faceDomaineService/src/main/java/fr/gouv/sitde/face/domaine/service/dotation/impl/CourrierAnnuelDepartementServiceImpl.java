package fr.gouv.sitde.face.domaine.service.dotation.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.CourrierAnnuelDepartementRepository;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService;
import fr.gouv.sitde.face.domaine.service.referentiel.DepartementService;
import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class CourrierAnnuelDepartementServiceImpl.
 */
@Named
public class CourrierAnnuelDepartementServiceImpl implements CourrierAnnuelDepartementService {

    /** The courrier annuel departement repository. */
    @Inject
    private CourrierAnnuelDepartementRepository courrierAnnuelDepartementRepository;

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The departement service. */
    @Inject
    private DepartementService departementService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService#creerCourrierAnnuelDepartement(fr.gouv.sitde.face.transverse.
     * entities.CourrierAnnuelDepartement)
     */
    @Override
    public CourrierAnnuelDepartement creerCourrierAnnuelDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement) {
        return this.courrierAnnuelDepartementRepository.creerCourrierAnnuelDepartement(courrierAnnuelDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService#rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(java.
     * lang.String, java.lang.Integer)
     */
    @Override
    public CourrierAnnuelDepartement rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(String codeDepartement, Integer annee) {
        return this.courrierAnnuelDepartementRepository.rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(codeDepartement, annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService#televerserDocumentCourrierRepartition(java.lang.String,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document televerserEtCreerDocumentCourrierRepartition(String codeDepartement, FichierTransfert fichierTransfert) {

        Integer annee = this.timeOperationTransverse.getAnneeCourante();

        CourrierAnnuelDepartement courrierAnnuelDepartement = this.rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(codeDepartement,
                annee);

        // Si n'existe pas, on crée
        if (courrierAnnuelDepartement == null) {

            courrierAnnuelDepartement = new CourrierAnnuelDepartement();

            courrierAnnuelDepartement.setAnnee(annee);

            Departement departement = this.departementService.rechercherDepartementParCode(codeDepartement);
            courrierAnnuelDepartement.setDepartement(departement);

            courrierAnnuelDepartement = this.creerCourrierAnnuelDepartement(courrierAnnuelDepartement);
        }

        return this.documentService.creerDocumentCourrierDepartement(courrierAnnuelDepartement, fichierTransfert);
    }
}

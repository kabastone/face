package fr.gouv.sitde.face.domaine.service.administration;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * The Interface DroitAgentService.
 */
public interface DroitAgentService {
    /**
     * Ajoute un droit agent pour l'utilisateur et la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the droit agent
     */
    DroitAgent ajouterDroitAgent(Long idCollectivite, Long idUtilisateur);

    /**
     * Creer droit agent.
     *
     * @param droitAgent
     *            the droit agent
     * @return the droit agent
     */
    DroitAgent creerDroitAgent(DroitAgent droitAgent);

    /**
     * Modifie le droit admin d'un agent (simple agent/admin) pour l'utilisateur et la collectivité spécifiée.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     * @return the droit agent
     */
    DroitAgent modifierEtatAdminDroitAgent(Long idCollectivite, Long idUtilisateur);

    /**
     * Rechercher droits agent par collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<DroitAgent> rechercherDroitsAgentParCollectivite(Long idCollectivite);

    /**
     * Supprimer droit agent.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idUtilisateur
     *            the id utilisateur
     */
    void supprimerDroitAgent(Long idCollectivite, Long idUtilisateur);
}

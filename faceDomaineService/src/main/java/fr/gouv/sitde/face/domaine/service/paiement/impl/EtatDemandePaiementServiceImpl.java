/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandePaiementRepository;
import fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;

/**
 * Service métier de EtatDemandePaiement.
 *
 * @author Atos
 */
@Named
public class EtatDemandePaiementServiceImpl extends BeanValidationService<EtatDemandePaiement> implements EtatDemandePaiementService {

    /** The etat demande paiement repository. */
    @Inject
    private EtatDemandePaiementRepository etatDemandePaiementRepository;

    /* (non-Javadoc)
     * @see fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService#rechercherParCode(java.lang.String)
     */
    @Override
    public EtatDemandePaiement rechercherParCode(String codeEtatdemande) {
        return this.etatDemandePaiementRepository.findByCode(codeEtatdemande);
    }
}

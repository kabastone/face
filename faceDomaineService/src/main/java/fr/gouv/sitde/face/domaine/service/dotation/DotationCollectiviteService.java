/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.dotation.CollectiviteRepartitionBo;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Interface DotationCollectiviteService.
 */
public interface DotationCollectiviteService {

    /**
     * Email information consommation.
     *
     * @param collectivite
     *            the collectivite
     */
    void emailInformationConsommation(Collectivite collectivite);

    /**
     * Rechercher dotation collectivite par id.
     *
     * @param id
     *            the id
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectiviteParId(Long id);

    /**
     * Rechercher dotation collectivite simple par id.
     *
     * @param id
     *            the id
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectiviteSimpleParId(Long id);

    /**
     * Rechercher dotation collectivite par id collectivite et id sous programme.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation collectivite
     */
    DotationCollectivite rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(Long idCollectivite, Integer idSousProgramme);

    /**
     * Maj dotation collectivite.
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @return the dotation collectivite
     */
    DotationCollectivite majDotationCollectivite(DotationCollectivite dotationCollectivite);

    /**
     * Rechercher dotations collectivites par code departement et abreviation sous programme.
     *
     * @param codeDepartement
     *            the code departement
     * @param sprEnum
     *            the spr enum
     * @param anneeEnCours
     *            the annee en cours
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(String codeDepartement, String sprEnum,
            Integer anneeEnCours);

    /**
     * Rechercher dotations collectivites par id collectivite et annee et code numerique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param annee
     *            the annee
     * @param codeNumerique
     *            the code numerique
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(Long idCollectivite, Integer annee,
            String codeNumerique);

    /**
     * Rechercher dotations collectivites pour repartition.
     *
     * @param codeDepartement
     *            the code departement
     * @return the list
     */
    List<CollectiviteRepartitionBo> rechercherDotationsCollectivitesPourRepartition(String codeDepartement);

    /**
     * Repartir dotation collectivite.
     *
     * @param collectiviteRepartitionDTO
     *            the collectivite repartition DTO
     */
    void repartirDotationCollectivite(CollectiviteRepartitionBo collectiviteRepartitionDTO);

    /**
     * Recuperer dossiers par dotation collectivite.
     *
     * @param id
     *            the id
     * @return the list
     */
    List<DossierSubvention> recupererDossiersParDotationCollectivite(Long id);

    /**
     * Creer dotation collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation collectivite
     */
    DotationCollectivite creerDotationCollectivite(Long idCollectivite, Integer idSousProgramme);

    /**
     * Calculer dotation disponible.
     *
     * @param dotation
     *            the dotation
     * @return the big decimal
     */
    BigDecimal calculerDotationDisponible(DotationCollectivite dotation);

    /**
     * Rechercher dotation collectivite par id collectivite.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<DotationCollectivite> rechercherDotationCollectiviteParIdCollectivite(Long idCollectivite);

    /**
     * Checks if is est unique subvention en cours par sous programme.
     *
     * @param idDotationCollectivite
     *            the id dotation collectivite
     * @param descriptionSousProgramme
     *            the description sous programme
     * @return the boolean
     */
    boolean isEstUniqueSubventionEnCoursParSousProgramme(Long idDemandeSubvention, Long idDotationCollectivite, String descriptionSousProgramme);

	/**
	 * Rechercher perte dotation collectivite pour batch.
	 *
	 * @return the list
	 */
	List<DotationCollectivite> rechercherPerteDotationCollectivitePourBatch();
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository;
import fr.gouv.sitde.face.domain.spi.repositories.referentiel.ReglementaireRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class ReglementaireServiceImpl.
 */
@Named
public class ReglementaireServiceImpl implements ReglementaireService {

    /** Le repository contenant les infos venant de Reglementaire. */
    @Inject
    private ReglementaireRepository reglementaireRepository;

    @Inject
    private TimeOperationTransverse time;

    @Inject
    private DossierSubventionRepository dossierRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService#rechercherReglementaire()
     */
    @Override
    public Reglementaire rechercherReglementaire() {
        /** Recuperation des donnees venant de la BDD r_reglementaire. */
        return this.reglementaireRepository.rechercherReglementaire(this.time.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService#rechercherReglementaire(java.time.LocalDate)
     */
    @Override
    public Reglementaire rechercherReglementaire(Integer anneeMiseEnApplication) {
        return this.reglementaireRepository.rechercherReglementaire(anneeMiseEnApplication);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService#rechercherReglementaireParIdDossier(java.lang.Long)
     */
    @Override
    public Reglementaire rechercherReglementaireParIdDossier(Long idDossier) {
        DossierSubvention dossier = this.dossierRepository.findDossierSubventionById(idDossier);
        return this.rechercherReglementaire(
                dossier.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
    }

}

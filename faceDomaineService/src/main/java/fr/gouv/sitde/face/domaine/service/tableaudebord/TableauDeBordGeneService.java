/**
 *
 */
package fr.gouv.sitde.face.domaine.service.tableaudebord;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;

/**
 * The Interface TableauDeBordGeneService.
 *
 * @author a453029
 */
public interface TableauDeBordGeneService {

    /**
     * Rechercher tableau de bord generique.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the tableau de bord gene dto
     */
    TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite);

    /**
     * Rechercher dotations collectivite tdb gene ademander subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DotationCollectivite> rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene ademander subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAdemanderPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a completer subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DotationCollectivite> rechercherDotationCollectiviteTdbGeneAcompleterSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher demandes subvention tdb gene a corriger subvention.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbGeneAcorrigerSubvention(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher demandes paiement tdb gene a corriger paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DemandePaiement> rechercherDemandesPaiementTdbGeneAcorrigerPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a commencer paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAcommencerPaiement(Long idCollectivite, PageDemande pageDemande);

    /**
     * Rechercher dossiers subvention tdb gene a solder paiement.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param pageDemande
     *            the page demande
     * @return the page reponse
     */
    PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAsolderPaiement(Long idCollectivite, PageDemande pageDemande);

}

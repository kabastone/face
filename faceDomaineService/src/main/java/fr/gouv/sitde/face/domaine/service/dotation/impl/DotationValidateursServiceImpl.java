/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class DotationValidateursServiceImpl.
 *
 * @author Atos
 */
@Named
public class DotationValidateursServiceImpl implements DotationValidateursService {

    /** The Constant MONTANT_NULL. */
    private static final String MONTANT_NULL = "Montant null";

    private static final String DOTATION_COLLECTIVITE_NULL = "DotationCollectivite null";

    /** The dotation departement repository. */
    @Inject
    private DotationDepartementRepository dotationDepartementRepository;

    @Inject
    private DotationProgrammeRepository dotationProgrammeRepository;

    /*
     * (non-Javadoc)
     *
     * CU_DOT_401
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerProgrammeAvecSousProgrammes(fr.gouv.sitde.face.transverse.
     * entities.DotationProgramme, java.math.BigDecimal)
     */
    @Override
    public void validerProgrammeAvecSousProgrammes(DotationProgramme dotationProgramme, BigDecimal dotationProgrammeSaisi) {

        if (dotationProgramme == null) {
            throw new TechniqueException("le programme est null");
        }

        if (dotationProgrammeSaisi == null) {
            throw new RegleGestionException("dotation.programme.montant.non.renseigne");
        }

        if (dotationProgrammeSaisi.compareTo(dotationProgramme.getDotationRepartie()) < 0) {
            throw new RegleGestionException("dotation.programme.sous.dote", MessageUtils.formatMontant(dotationProgramme.getDotationRepartie()));
        }

    }

    /*
     * (non-Javadoc)
     *
     * CU_DOT_411
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerSousProgrammeAvecProgrammeInitial(fr.gouv.sitde.face.transverse.
     * entities .DotationSousProgramme, java.math.BigDecimal)
     */
    @Override
    public void validerSousProgrammeAvecProgrammeInitial(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi) {

        if ((montantDotationSousProgrammeSaisi == null) || (dotationSousProgramme == null)) {
            throw new TechniqueException("Le montant ou le sous-programme est null");
        }

        DotationProgramme dpr = this.dotationProgrammeRepository
                .rechercherDotationProgrammeParId(dotationSousProgramme.getDotationProgramme().getId());

        BigDecimal dprDotationRepartieInitiale = dpr.getDotationsSousProgrammes().stream().map(DotationSousProgramme::getMontantInitial)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal dprMontant = dpr.getMontant();
        BigDecimal ancienneDotationSousProgrammeInitiale = dotationSousProgramme.getMontantInitial();

        if (dprMontant
                .compareTo(dprDotationRepartieInitiale.subtract(ancienneDotationSousProgrammeInitiale).add(montantDotationSousProgrammeSaisi)) < 0) {
            throw new RegleGestionException("dotation.sous.programme.sur.dote", dotationSousProgramme.getSousProgramme().getDescription(),
                    MessageUtils.formatMontant(dprMontant.subtract(dprDotationRepartieInitiale).add(ancienneDotationSousProgrammeInitiale)));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerSousProgrammeAvecProgrammeEffectif(fr.gouv.sitde.face.transverse.
     * entities.DotationSousProgramme, java.math.BigDecimal)
     */
    @Override
    public void validerSousProgrammeAvecProgrammeEffectif(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi) {

        if ((montantDotationSousProgrammeSaisi == null) || (dotationSousProgramme == null)) {
            throw new TechniqueException("Le montant ou le sous-programme est null");
        }

        DotationProgramme dpr = dotationSousProgramme.getDotationProgramme();
        BigDecimal dprDotationRepartie = dpr.getDotationRepartie();
        BigDecimal dprMontant = dpr.getMontant();
        BigDecimal ancienneDotationSousProgramme = dotationSousProgramme.getMontant();

        if (dprMontant.compareTo(dprDotationRepartie.subtract(ancienneDotationSousProgramme).add(montantDotationSousProgrammeSaisi)) < 0) {
            throw new RegleGestionException("dotation.sous.programme.sur.dote", dotationSousProgramme.getSousProgramme().getDescription(),
                    MessageUtils.formatMontant(dprMontant.subtract(dprDotationRepartie).add(ancienneDotationSousProgramme)));
        }
    }

    /*
     * (non-Javadoc)
     *
     * CU_DOT_412
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerSousProgrammeAvecDepartements(fr.gouv.sitde.face.transverse.
     * entities.DotationSousProgramme, java.math.BigDecimal)
     */
    @Override
    public void validerSousProgrammeAvecDepartements(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi) {
        if ((montantDotationSousProgrammeSaisi == null) || (dotationSousProgramme == null)) {
            throw new TechniqueException("Le montant ou le sous-programme est null");
        }

        if (montantDotationSousProgrammeSaisi.compareTo(dotationSousProgramme.getDotationRepartie()) < 0) {
            BigDecimal dsptationRepartie = dotationSousProgramme.getDotationRepartie();

            throw new RegleGestionException("dotation.sous.programme.sous.dote", dotationSousProgramme.getSousProgramme().getDescription(),
                    MessageUtils.formatMontant(dsptationRepartie));
        }
    }

    /**
     * Valider departement avec sous programme.
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @param montantSaisiSousProgramme
     *            the montant saisi sous programme
     * @param typeDotationSaisi
     *            the type dotation saisi
     */
    /*
     * (non-Javadoc)
     *
     * CU_DOT_421
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerDepartementAvecSousProgramme(java.lang.Long,
     * java.math.BigDecimal)
     */
    @Override
    public void validerDepartementAvecSousProgramme(Long dotationDepartementId, BigDecimal montantSaisiSousProgramme,
            TypeDotationDepartementEnum typeDotationSaisi) {
        if (montantSaisiSousProgramme == null) {
            throw new TechniqueException(MONTANT_NULL);
        }

        DotationDepartement dde = this.dotationDepartementRepository.rechercherParId(dotationDepartementId);

        if (dde == null) {
            throw new TechniqueException("DotationDepartement introuvable " + dotationDepartementId);
        }

        BigDecimal dspDotationRepartie = dde.getDotationSousProgramme().getDotationRepartie();
        BigDecimal ddeMontantAncien = dde.getMontantTotal();
        BigDecimal dspMontant = dde.getDotationSousProgramme().getMontant();

        if (typeDotationSaisi.equals(TypeDotationDepartementEnum.EXCEPTIONNELLE)) {
            montantSaisiSousProgramme = montantSaisiSousProgramme.add(ddeMontantAncien);
        }

        if (dspDotationRepartie.subtract(ddeMontantAncien).add(montantSaisiSousProgramme).compareTo(dspMontant) > 0) {
            throw new RegleGestionException("dotation.departement.sur.dote", dde.getDotationSousProgramme().getSousProgramme().getDescription(),
                    MessageUtils.formatMontant(dspMontant.subtract(dspDotationRepartie).add(ddeMontantAncien)));
        }

    }

    /*
     * (non-Javadoc)
     *
     * CU_DOT_422
     *
     * Cette méthode de validation de l'import n'est pas utilisée La validation de l'import est réalisée dans la méthode
     * verifierCoherenceFichier(listeImportLigneDotation) dans ImportDotationServiceImpl
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerDepartementImportAvecSousProgramme(java.lang.Long,
     * java.math.BigDecimal)
     */
    @Override
    public void validerDepartementImportAvecSousProgramme(Long dotationDepartementId, BigDecimal montantImport) {
        if (montantImport == null) {
            throw new TechniqueException(MONTANT_NULL);
        }

        DotationDepartement dde = this.dotationDepartementRepository.rechercherParId(dotationDepartementId);

        if (dde == null) {
            throw new TechniqueException("DotationDepartement introuvable " + dotationDepartementId);
        }

        BigDecimal sommeLddExceptionnelle = BigDecimal.ZERO;

        for (LigneDotationDepartement ldd : dde.getLignesDotationDepartement()) {
            if (ldd.getTypeDotationDepartement().equals(TypeDotationDepartementEnum.EXCEPTIONNELLE)) {
                sommeLddExceptionnelle = sommeLddExceptionnelle.add(ldd.getMontant());
            }
        }

        BigDecimal dspMontant = dde.getDotationSousProgramme().getMontant();

        if (montantImport.add(sommeLddExceptionnelle).compareTo(dspMontant) > 0) {
            BigDecimal ddeDotationRepartie = dde.getDotationRepartie();
            throw new RegleGestionException("dotation.departement.sur.dote.import",
                    dde.getDotationSousProgramme().getSousProgramme().getDescription(),
                    MessageUtils.formatMontant(montantImport.add(ddeDotationRepartie).subtract(dspMontant)));
        }

    }

    /*
     * (non-Javadoc)
     *
     * CU_DOT_431
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerCollectiviteAvecDepartement(java.lang.Long,
     * java.math.BigDecimal)
     */
    @Override
    public void validerCollectiviteAvecDepartement(DotationCollectivite dotationCollective, BigDecimal dotationCollectiviteSaisi,
            List<MessageProperty> listeErreurs) {
        if (dotationCollectiviteSaisi == null) {
            throw new TechniqueException(MONTANT_NULL);
        }

        if (dotationCollective == null) {
            throw new TechniqueException(DOTATION_COLLECTIVITE_NULL);
        }

        BigDecimal ddeDotationRepartie = dotationCollective.getDotationDepartement().getDotationRepartie();
        BigDecimal dcoMontantAncien = dotationCollective.getMontant();
        BigDecimal ddeMontantNotifie = dotationCollective.getDotationDepartement().getMontantNotifie();

        if (ddeDotationRepartie.subtract(dcoMontantAncien).add(dotationCollectiviteSaisi).compareTo(ddeMontantNotifie) > 0) {
            listeErreurs.add(new MessageProperty("dotation.departement.sur.dote",
                    new String[] { dotationCollective.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                            MessageUtils.formatMontant(ddeMontantNotifie.subtract(ddeDotationRepartie).add(dcoMontantAncien)) }));
        }

    }

    /*
     * (non-Javadoc)
     *
     * CU_DOT_432
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService#validerCollectiviteAvecSubventions(java.lang.Long,
     * java.math.BigDecimal)
     */
    @Override
    public void validerCollectiviteAvecSubventions(DotationCollectivite dotationCollectivite, BigDecimal dotationCollectiviteSaisi,
            List<MessageProperty> listeErreurs) {
        if (dotationCollectiviteSaisi == null) {
            throw new TechniqueException(MONTANT_NULL);
        }

        if (dotationCollectivite == null) {
            throw new TechniqueException(DOTATION_COLLECTIVITE_NULL);
        }

        if ((dotationCollectiviteSaisi.compareTo(dotationCollectivite.getDotationRepartie()) < 0)
                && (dotationCollectiviteSaisi.compareTo(BigDecimal.ZERO) > 0)) {
            listeErreurs.add(new MessageProperty("dotation.collectivite.sous.dote",
                    new String[] { dotationCollectivite.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                            MessageUtils.formatMontant(dotationCollectivite.getDotationRepartie()) }));
        }
    }

}

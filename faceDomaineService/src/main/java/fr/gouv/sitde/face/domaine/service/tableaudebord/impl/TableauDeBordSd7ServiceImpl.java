/**
 *
 */
package fr.gouv.sitde.face.domaine.service.tableaudebord.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Class TableauDeBordSd7ServiceImpl.
 *
 * @author a453029
 */
@Named
public class TableauDeBordSd7ServiceImpl implements TableauDeBordSd7Service {

    /** The tableau de bord sd7 repository. */
    @Inject
    private TableauDeBordSd7Repository tableauDeBordSd7Repository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service#rechercherTableauDeBordSd7()
     */
    @Override
    public TableauDeBordSd7Dto rechercherTableauDeBordSd7() {
        return this.tableauDeBordSd7Repository.rechercherTableauDeBordSd7();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service#rechercherDemandesSubventionTdbSd7AcontrolerSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AcontrolerSubvention(PageDemande pageDemande) {
        return this.tableauDeBordSd7Repository.rechercherDemandesSubventionTdbSd7AcontrolerSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service#rechercherDemandesPaiementTdbSd7AcontrolerPaiement(fr.gouv.sitde.face.
     * transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AcontrolerPaiement(PageDemande pageDemande) {
        return this.tableauDeBordSd7Repository.rechercherDemandesPaiementTdbSd7AcontrolerPaiement(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service#rechercherDemandesSubventionTdbSd7AtransfererSubvention(fr.gouv.sitde.
     * face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AtransfererSubvention(PageDemande pageDemande) {
        return this.tableauDeBordSd7Repository.rechercherDemandesSubventionTdbSd7AtransfererSubvention(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordSd7Service#rechercherDemandesPaiementTdbSd7AtransfererPaiement(fr.gouv.sitde.face
     * .transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AtransfererPaiement(PageDemande pageDemande) {
        return this.tableauDeBordSd7Repository.rechercherDemandesPaiementTdbSd7AtransfererPaiement(pageDemande);
    }

}

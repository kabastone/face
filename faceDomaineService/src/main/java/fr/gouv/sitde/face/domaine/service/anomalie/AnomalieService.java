package fr.gouv.sitde.face.domaine.service.anomalie;

import java.util.List;
import java.util.Set;

import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Interface AnomalieService.
 */
public interface AnomalieService {

    /**
     * Rechercher anomalie par id et par id demande subvention.
     *
     * @param idAnomalie
     *            the id anomalie
     * @return the anomalie
     */
    Anomalie rechercherAnomalieParId(Long idAnomalie);

    /**
     * Enregistre l'anomalie en bdd.
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie
     */
    Anomalie creerEtValiderAnomalie(Anomalie anomalie);

    /**
     * Enregistre l'anomalie en bdd (sans flush).
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie
     */
    void creerAnomalie(Anomalie anomalie);

    /**
     * Modifier anomalie corrigee pour subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie
     */
    Anomalie majCorrectionAnomaliePourSubvention(Long idDemandeSubvention, Long idAnomalie, boolean corrigee);

    /**
     * Modifier anomalie corrigee pour paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @param idAnomalie
     *            the id anomalie
     * @param corrigee
     *            the corrigee
     * @return the anomalie
     */
    Anomalie majCorrectionAnomaliePourPaiement(Long idDemandePaiement, Long idAnomalie, boolean corrigee);

    /**
     * Contient anomalies A corriger.
     *
     * @param anomalies
     *            the anomalies
     * @return true, if successful
     */
    boolean contientAnomaliesEtACorriger(Set<Anomalie> anomalies);

    /**
     * Contient anomalies A corriger sans reponse.
     *
     * @param anomalies
     *            the anomalies
     * @return true, if successful
     */
    boolean contientAnomaliesSansReponse(Set<Anomalie> anomalies);

    /**
     * Contient anomalies A corriger par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return true, if successful
     */
    boolean contientAnomaliesEtACorrigerParIdDemandePaiement(Long idDemandePaiement);

    /**
     * Contient anomalies A corriger sans reponse par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return true, if successful
     */
    boolean contientAnomaliesSansReponseParIdDemandePaiement(Long idDemandePaiement);

    /**
     * Contient anomalies et A corriger par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return true, if successful
     */
    boolean contientAnomaliesEtACorrigerParIdDemandeSubvention(Long idDemandeSubvention);

    /**
     * Contient anomalies et A corriger sans reponse par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return true, if successful
     */
    boolean contientAnomaliesSansReponseParIdDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rechercher anomalies par id demande subvention.
     *
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return the list
     */
    List<Anomalie> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention);

    /**
     * Rechercher anomalies par id demande paiement.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the list
     */
    List<Anomalie> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement);

    /**
     * Modifier anomalie.
     *
     * @param anomalie
     *            the anomalie
     * @return the anomalie
     */
    Anomalie modifierAnomalie(Anomalie anomalie);

    /**
     * Rechercher anomalies par id demande subvention en enlevant celles qui ne doivent pas être visibles pour AODE.
     *
     * @param idDemandeSubvention
     * @return
     */
    List<Anomalie> rechercherAnomaliesParIdDemandeSubventionPourAODE(Long idDemandeSubvention);

    /**
     * Rends les anomalies visibles pour l'AODE sur la demande de subvention passée en paramètre.
     *
     * @param idDemandeSubvention
     *            la demande de subvention sur laquelle appliquer le changement.
     */
    void rendreAnomaliesDemandeSubventionVisiblesPourAODE(Long idDemandeSubvention);

    /**
     * Rendre anomalies demande paiement visibles pour AODE.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     */
    void rendreAnomaliesDemandePaiementVisiblesPourAODE(Long idDemandePaiement);

    /**
     * Rechercher anomalies par id demande paiement pour AODE.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @return the list
     */
    List<Anomalie> rechercherAnomaliesParIdDemandePaiementPourAODE(Long idDemandePaiement);
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.email;

/**
 * The Interface EmailProlongationService.
 */
public interface EmailProlongationService {

    /**
     * Envoyer email demande prolongation refusee.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     */
    void envoyerEmailDemandeProlongationRefusee(Long idDemandeProlongation);

    /**
     * Envoyer email demande prolongation refusee.
     *
     * @param idDemandeProlongation
     *            the id demande prolongation
     */
    void envoyerEmailDemandeProlongationAcceptee(Long idDemandeProlongation);

}

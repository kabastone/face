/**
 *
 */
package fr.gouv.sitde.face.domaine.service.anomalie.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.TypeAnomalieRepository;
import fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;

/**
 * The Class TypeAnomalieServiceImpl.
 *
 * @author Atos
 */
@Named
public class TypeAnomalieServiceImpl implements TypeAnomalieService {

    /** The type anomalie repository. */
    @Inject
    private TypeAnomalieRepository typeAnomalieRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService#rechercherParCode(java.lang.String)
     */
    @Override
    public TypeAnomalie rechercherParCode(String code) {
        return this.typeAnomalieRepository.rechercherParCode(code);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService#rechercherTousPourDemandePaiement()
     */
    @Override
    public List<TypeAnomalie> rechercherTousPourDemandePaiement() {
        return this.typeAnomalieRepository.rechercherTousPourDemandePaiement();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.anomalie.TypeAnomalieService#rechercherTousPourDemandePaiement()
     */
    @Override
    public List<TypeAnomalie> rechercherTousPourDemandeSubvention() {
        return this.typeAnomalieRepository.rechercherTousPourDemandeSubvention();
    }

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.tableaudebord.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository;
import fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;

/**
 * The Class TableauDeBordGeneServiceImpl.
 *
 * @author a453029
 */
@Named
public class TableauDeBordGeneServiceImpl implements TableauDeBordGeneService {

    /** The tableau de bord gene repository. */
    @Inject
    private TableauDeBordGeneRepository tableauDeBordGeneRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherTableauDeBordGenerique(java.lang.Long)
     */
    @Override
    public TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite) {
        return this.tableauDeBordGeneRepository.rechercherTableauDeBordGenerique(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationCollectivite> rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDossiersSubventionTdbGeneAdemanderPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAdemanderPaiement(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDossiersSubventionTdbGeneAdemanderPaiement(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDossiersSubventionTdbGeneAcompleterSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationCollectivite> rechercherDotationCollectiviteTdbGeneAcompleterSubvention(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDotationCollectiviteTdbAcompleterSubvention(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDemandesSubventionTdbGeneAcorrigerSubvention(java.lang.
     * Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbGeneAcorrigerSubvention(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDemandesSubventionTdbGeneAcorrigerSubvention(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDemandesPaiementTdbGeneAcorrigerPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbGeneAcorrigerPaiement(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDemandesPaiementTdbGeneAcorrigerPaiement(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDossiersSubventionTdbGeneAcommencerPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAcommencerPaiement(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDossiersSubventionTdbGeneAcommencerPaiement(idCollectivite, pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.tableaudebord.TableauDeBordGeneService#rechercherDossiersSubventionTdbGeneAsolderPaiement(java.lang.Long,
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAsolderPaiement(Long idCollectivite, PageDemande pageDemande) {
        return this.tableauDeBordGeneRepository.rechercherDossiersSubventionTdbGeneAsolderPaiement(idCollectivite, pageDemande);
    }

}

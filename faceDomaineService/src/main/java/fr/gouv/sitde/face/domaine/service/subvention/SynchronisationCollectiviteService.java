/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

/**
 * Classe métier de SynchronisationCollectivite.
 *
 * @author Atos
 */
public interface SynchronisationCollectiviteService {

    /**
     * Synchroniser une collectivite avec le WS Chorus.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idDemandeSubvention
     *            the id demande subvention
     * @return true if update the collectivite succeed
     */
    boolean synchroniserCollectivite(Long idCollectivite, Long idDemandeSubvention);

}

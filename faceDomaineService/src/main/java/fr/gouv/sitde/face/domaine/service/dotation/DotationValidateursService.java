/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Interface DotationValidateursService.
 *
 * @author Atos
 */
public interface DotationValidateursService {

    /**
     * Valider programe avec sous programmes.
     *
     * CU_DOT_401
     *
     * @param dotationProgramme
     *            the dotation programme
     * @param montantDotationProgrammeSaisi
     *            the montant dotation programme saisi
     */
    void validerProgrammeAvecSousProgrammes(DotationProgramme dotationProgramme, BigDecimal montantDotationProgrammeSaisi);

    /**
     * Valider sous programme avec programme.
     *
     * CU_DOT_411
     *
     * @param dotationSousProgramme
     *            the dotation sous programme
     * @param montantDotationSousProgrammeSaisi
     *            the montant dotation sous programme saisi
     */
    void validerSousProgrammeAvecProgrammeInitial(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi);

    /**
     * Valider sous programme avec programme.
     *
     * @param dotationSousProgramme
     *            the dotation sous programme
     * @param montantDotationSousProgrammeSaisi
     *            the montant dotation sous programme saisi
     */
    void validerSousProgrammeAvecProgrammeEffectif(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi);

    /**
     * Valider sous programme avec departements.
     *
     * CU_DOT_412
     *
     * @param dotationSousProgramme
     *            the dotation sous programme
     * @param montantDotationSousProgrammeSaisi
     *            the montant dotation sous programme saisi
     */
    void validerSousProgrammeAvecDepartements(DotationSousProgramme dotationSousProgramme, BigDecimal montantDotationSousProgrammeSaisi);

    /**
     * Valider departement avec sous programme.
     *
     * CU_DT_421
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @param montantSaisiSousProgramme
     *            the montant saisi sous programme
     * @param typeDotationSaisi
     *            the type dotation saisi
     */
    void validerDepartementAvecSousProgramme(Long dotationDepartementId, BigDecimal montantSaisiSousProgramme,
            TypeDotationDepartementEnum typeDotationSaisi);

    /**
     * Valider departement import avec sous programme.
     *
     * CU_DT_422
     *
     * @param dotationDepartementId
     *            the dotation departement id
     * @param montantImport
     *            the montant import
     */
    void validerDepartementImportAvecSousProgramme(Long dotationDepartementId, BigDecimal montantImport);

    /**
     * Valider collectivite avec departement.
     *
     * CU_DT_431
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @param dotationCollectiviteSaisi
     *            the dotation collectivite saisi
     * @param listeErreurs
     *
     */
    void validerCollectiviteAvecDepartement(DotationCollectivite dotationCollectivite, BigDecimal dotationCollectiviteSaisi,
            List<MessageProperty> listeErreurs);

    /**
     * Valider collectivite avec subventions.
     *
     * CU_DT_432
     *
     * @param dotationCollectivite
     *            the dotation collectivite
     * @param dotationCollectiviteSaisi
     *            the dotation collectivite saisi
     */
    void validerCollectiviteAvecSubventions(DotationCollectivite dotationCollectivite, BigDecimal dotationCollectiviteSaisi,
            List<MessageProperty> listeErreurs);

}

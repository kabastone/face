/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.transverse.dotation.CollectiviteRepartitionBo;
import fr.gouv.sitde.face.transverse.dotation.DotationCollectiviteRepartitionParSousProgrammeBo;
import fr.gouv.sitde.face.transverse.email.DotationCollectiviteCompletDto;
import fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.email.SousProgrammeNotificationEnum;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.referentiel.EtatCollectiviteEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class DotationCollectiviteServiceImpl.
 *
 * @author A754839
 */
@Named
public class DotationCollectiviteServiceImpl implements DotationCollectiviteService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DotationCollectiviteServiceImpl.class);

    /** The email service. */
    @Inject
    private EmailService emailService;

    /** The dotation collectivite repository. */
    @Inject
    private DotationCollectiviteRepository dotationCollectiviteRepository;

    /** The collectivite service. */
    @Inject
    private CollectiviteService collectiviteService;

    /** The dotation departement service. */
    @Inject
    private DotationDepartementService dotationDepartementService;

    /** The dotation validateurs service. */
    @Inject
    private DotationValidateursService dotationValidateursService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The Constant NB_ANNEE_TABLEAU. */
    private static final int NB_ANNEE_TABLEAU = 5;

    /** The Constant DOTATION_COLLECTIVITE_INTROUVABLE. */
    private static final String DOTATION_COLLECTIVITE_INTROUVABLE = "Impossible de trouver la DotationCollectivite : ";

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#emailInformationConsommation(fr.gouv.sitde.face.transverse.entities.
     * Collectivite)
     */
    @Override
    public void emailInformationConsommation(Collectivite collectivite) {

        Integer anneeEnCours = this.timeOperationTransverse.getAnneeCourante();

        List<DotationCollectiviteParSousProgrammeDto> listeDTO = this.dotationCollectiviteRepository
                .rechercherDTOparIdCollectivite(collectivite.getId(), anneeEnCours);

        DotationCollectiviteCompletDto dtoComplet = new DotationCollectiviteCompletDto(listeDTO);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Recuperation des donnees de la base : ");
            for (DotationCollectiviteParSousProgrammeDto dotDTO : listeDTO) {
                LOGGER.debug(dotDTO.toString());
            }

            LOGGER.debug("DTO de l'annee en cours pour extension-reseaux : {}",
                    dtoComplet.getDTOParAnneeEtSousProgramme(anneeEnCours, SousProgrammeNotificationEnum.EXTENSION_RESEAUX).getAideConsommee());
        }

        List<Integer> annees = new ArrayList<>(NB_ANNEE_TABLEAU);
        for (int i = NB_ANNEE_TABLEAU; i > 0; i--) {
            annees.add((anneeEnCours - i) + 1);
        }

        Map<String, Object> variablesContexte = new HashMap<>(100);
        variablesContexte.put("sprEnum", Arrays.asList(SousProgrammeNotificationEnum.values()));
        variablesContexte.put("listeAnnees", annees);
        variablesContexte.put("dtoComplet", dtoComplet);
        variablesContexte.put("nomCollectivite", collectivite.getNomLong());

        Boolean demandeSubvention = this.dotationCollectiviteRepository.verifierExistenceDemandeSubvention(collectivite.getId(), anneeEnCours);
        LOGGER.debug("Booleen : {}", demandeSubvention);

        variablesContexte.put("demandeSubvention", demandeSubvention);

        this.emailService.creerEtEnvoyerEmail(collectivite, TypeEmailEnum.COL_CONSOMMATION, TemplateEmailEnum.EMAIL_COL_CONSOMMATION,
                variablesContexte);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#rechercherDotationCollectiviteParId(java.lang.Long)
     */
    @Override
    public DotationCollectivite rechercherDotationCollectiviteParId(Long id) {
        return this.dotationCollectiviteRepository.rechercherDotationCollectiviteParId(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#rechercherDotationCollectiviteSimpleParId(java.lang.Long)
     */
    @Override
    public DotationCollectivite rechercherDotationCollectiviteSimpleParId(Long id) {
        return this.dotationCollectiviteRepository.rechercherDotationCollectiviteSimpleParId(id);
    }

    /*
     * (non-Javadoc)
     *
     */
    @Override
    public DotationCollectivite rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(Long idCollectivite, Integer idSousProgramme) {

        // Récupération de l'année en cours
        Integer anneeEnCours = this.timeOperationTransverse.getAnneeCourante();

        return this.dotationCollectiviteRepository.rechercherDotationCollectivite(idCollectivite, idSousProgramme, anneeEnCours);
    }

    @Override
    public List<DotationCollectivite> rechercherDotationCollectiviteParIdCollectivite(Long idCollectivite) {

        // Récupération de l'année en cours
        Integer anneeEnCours = this.timeOperationTransverse.getAnneeCourante();

        return this.dotationCollectiviteRepository.rechercherDotationCollectiviteParIdCollectivite(idCollectivite, anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#majDotationCollectivite(fr.gouv.sitde.face.transverse.entities.
     * DotationCollectivite)
     */
    @Override
    public DotationCollectivite majDotationCollectivite(DotationCollectivite dotationCollectivite) {
        return this.dotationCollectiviteRepository.majDotationCollectivite(dotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#
     * rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(java.lang.String, java.lang.String, java.lang.Integer)
     */
    @Override
    public List<DotationCollectivite> rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(String codeDepartement,
            String sprEnum, Integer anneeEnCours) {
        return this.dotationCollectiviteRepository.rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(codeDepartement,
                sprEnum, anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique
     * (java.lang.Long, java.lang.Integer, java.lang.String)
     */
    @Override
    public List<DotationCollectivite> rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(Long idCollectivite, Integer annee,
            String codeNumerique) {
        return this.dotationCollectiviteRepository.rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(idCollectivite, annee,
                codeNumerique);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#rechercherDotationsCollectivitesPourRepartition(java.lang.String)
     */
    @Override
    public List<CollectiviteRepartitionBo> rechercherDotationsCollectivitesPourRepartition(String codeDepartement) {

        List<CollectiviteRepartitionBo> collectivitesRepartitionBo = new ArrayList<>();
        List<Collectivite> collectivites = this.collectiviteService.rechercherCollectivitesParCodeDepartement(codeDepartement);

        List<SousProgramme> listeSousProgrammes = this.sousProgrammeService.rechercherPourDotationDepartement();

        for (Collectivite collectivite : collectivites) {

            // On ne prend pas les collectivités fermées
            if (!collectivite.getEtatCollectivite().equals(EtatCollectiviteEnum.FERMEE)) {

                CollectiviteRepartitionBo collectiviteRepartitionBo = new CollectiviteRepartitionBo();
                List<DotationCollectiviteRepartitionParSousProgrammeBo> dotationsCollectiviteBo = new ArrayList<>(listeSousProgrammes.size());

                collectiviteRepartitionBo.setId(collectivite.getId());
                collectiviteRepartitionBo.setNomCourt(collectivite.getNomCourt());
                collectiviteRepartitionBo.setCodeDepartement(codeDepartement);

                for (SousProgramme sousProgramme : listeSousProgrammes) {

                    DotationCollectiviteRepartitionParSousProgrammeBo dotationCollectiviteBo = new DotationCollectiviteRepartitionParSousProgrammeBo();

                    DotationDepartement dotationDepartement = this.dotationDepartementService
                            .rechercherDotationDepartementPourRepartition(codeDepartement, sousProgramme.getCodeDomaineFonctionnel());

                    // Si pas de dotation département ou de dotation collectivité, on initialise une dotation collectivité
                    if (dotationDepartement != null) {

                        dotationCollectiviteBo.setIdDotationDepartement(dotationDepartement.getId());

                        DotationCollectivite dotationCollectivite = this.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(
                                collectiviteRepartitionBo.getId(), dotationDepartement.getDotationSousProgramme().getSousProgramme().getId());

                        if (dotationCollectivite != null) {
                            dotationCollectiviteBo.setId(dotationCollectivite.getId());
                            dotationCollectiviteBo.setMontant(dotationCollectivite.getMontant());
                        } else {
                            dotationCollectiviteBo.setMontant(new BigDecimal(0));
                        }
                    } else {
                        dotationCollectiviteBo.setMontant(new BigDecimal(0));
                    }
                    dotationsCollectiviteBo.add(dotationCollectiviteBo);
                }
                collectiviteRepartitionBo.setDotationsCollectivite(dotationsCollectiviteBo);
                collectivitesRepartitionBo.add(collectiviteRepartitionBo);
            }
        }
        return collectivitesRepartitionBo;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#repartirDotationCollectivite(fr.gouv.sitde.face.transverse.dotation.
     * CollectiviteRepartitionBo)
     */
    @Override
    public void repartirDotationCollectivite(CollectiviteRepartitionBo collectiviteRepartitionBo) {
        List<MessageProperty> listeErreurs = new ArrayList<>();
        for (DotationCollectiviteRepartitionParSousProgrammeBo dotationCollectiviteBo : collectiviteRepartitionBo.getDotationsCollectivite()) {

            // Si pas de dotation département, on ne fait rien
            if (dotationCollectiviteBo.getIdDotationDepartement() != null) {

                DotationCollectivite dotationCollectivite = new DotationCollectivite();

                // Si la dotation collectivité existe on met à jour, sinon on la créee
                if (dotationCollectiviteBo.getId() != null) {
                    dotationCollectivite = this.rechercherDotationCollectiviteParId(dotationCollectiviteBo.getId());
                    if (dotationCollectivite == null) {
                        throw new TechniqueException(DOTATION_COLLECTIVITE_INTROUVABLE + dotationCollectiviteBo.getId());
                    }
                } else {
                    DotationDepartement dotationDepartement = this.dotationDepartementService
                            .rechercherParId(dotationCollectiviteBo.getIdDotationDepartement());
                    dotationCollectivite.setDotationDepartement(dotationDepartement);
                    dotationCollectivite.setCollectivite(new Collectivite());
                    dotationCollectivite.getCollectivite().setId(collectiviteRepartitionBo.getId());
                }

                BigDecimal montantSaisi;
                if (dotationCollectiviteBo.getMontant() != null) {
                    montantSaisi = new BigDecimal(dotationCollectiviteBo.getMontant().intValue());
                } else {
                    montantSaisi = new BigDecimal(0);
                }

                if (montantSaisi.compareTo(BigDecimal.ZERO) < 0) {
                    listeErreurs.add(new MessageProperty("dotation.departement.sur.dote",
                            new String[] {
                                    dotationCollectivite.getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription(),
                                    MessageUtils.formatMontant(dotationCollectivite.getDotationDepartement().getMontantNotifie()) }));
                }

                // Validation avant si mise à jour

                this.dotationValidateursService.validerCollectiviteAvecDepartement(dotationCollectivite, montantSaisi, listeErreurs);
                this.dotationValidateursService.validerCollectiviteAvecSubventions(dotationCollectivite, montantSaisi, listeErreurs);

                dotationCollectivite.setMontant(new BigDecimal(montantSaisi.intValue()));

                // Ajout ou modif
                DotationCollectivite dotationsCollectiviteAjoutModif = this.majDotationCollectivite(dotationCollectivite);

                // Validation après si ajout

                this.dotationValidateursService.validerCollectiviteAvecDepartement(dotationsCollectiviteAjoutModif, montantSaisi, listeErreurs);
                this.dotationValidateursService.validerCollectiviteAvecSubventions(dotationsCollectiviteAjoutModif, montantSaisi, listeErreurs);

            }
        }

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#recupererDossiersParDotationCollectivite(java.lang.Long)
     */
    @Override
    public List<DossierSubvention> recupererDossiersParDotationCollectivite(Long id) {
        return this.dotationCollectiviteRepository.recupererDossiersParDotationCollectivite(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#creerDotationCollectivite(java.lang.Long, java.lang.Integer)
     */
    @Override
    public DotationCollectivite creerDotationCollectivite(Long idCollectivite, Integer idSousProgramme) {
        DotationCollectivite dco = new DotationCollectivite();

        dco.setCollectivite(this.collectiviteService.rechercherCollectiviteParId(idCollectivite));

        DotationDepartement dotDep = this.dotationDepartementService.rechercherDotationDepartementParIdCollectivite(idCollectivite, idSousProgramme);

        // Si la dotation départementale n'existe pas, on fait la création.
        if (dotDep == null) {
            dotDep = this.dotationDepartementService.creerDotationDepartement(idCollectivite, idSousProgramme);
        }

        dco.setDotationDepartement(dotDep);

        return this.dotationCollectiviteRepository.creer(dco);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService#calculerDotationDisponible(fr.gouv.sitde.face.transverse.entities.
     * DotationCollectivite)
     */
    @Override
    public BigDecimal calculerDotationDisponible(DotationCollectivite dotationCollectivite) {
        return dotationCollectivite.getMontant().subtract(dotationCollectivite.getDotationRepartie());
    }

    @Override
    public boolean isEstUniqueSubventionEnCoursParSousProgramme(Long idDemandeSubvention, Long idDotationCollectivite,
            String descriptionSousProgramme) {
        return this.dotationCollectiviteRepository.isEstUniqueSubventionEnCoursParSousProgramme(idDemandeSubvention, idDotationCollectivite,
                descriptionSousProgramme);
    }
	/**
	 * Rechercher perte dotation collectivite pour batch.
	 *
	 * @return the list
	 */
	@Override
	public List<DotationCollectivite> rechercherPerteDotationCollectivitePourBatch() {
		Integer anneeEnCours = this.timeOperationTransverse.now().getYear();
		return this.dotationCollectiviteRepository.rechercherPerteDotationCollectivitePourBatch(anneeEnCours);
	}

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import java.util.List;

import fr.gouv.sitde.face.transverse.imports.ImportLigneDotation;

/**
 * Interface metier de calcul de regles metier et d'import de Dotation.
 *
 * @author Atos
 */
public interface ImportDotationService {

    /**
     * Calcul des regles metier et import des donnees.
     *
     * @param listeImportLigneDotation
     *            lignes de Dossier Dotation a importer
     * @return the boolean
     */
    Boolean importerDotation(List<ImportLigneDotation> listeImportLigneDotation);

    /**
     * RG_DOT_212-01.
     *
     * @return the boolean
     */
    Boolean verifierDotationAnnuelleNotifiee();

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel;

import fr.gouv.sitde.face.transverse.entities.MissionFace;

/**
 * The Interface MissionFaceService.
 */
public interface MissionFaceService {

    /**
     * Récuperation des données depuis le repository r_mission_face.
     *
     *
     * @return the MissionFace
     *
     */
    MissionFace rechercherMissionFace();
}

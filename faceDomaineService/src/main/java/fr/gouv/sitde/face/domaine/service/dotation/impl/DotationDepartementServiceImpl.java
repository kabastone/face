/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;

import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository;
import fr.gouv.sitde.face.domaine.service.dotation.CourrierAnnuelDepartementService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleBo;
import fr.gouv.sitde.face.transverse.dotation.DotationDepartementaleParSousProgrammeBo;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.Penalite;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class DotationDepartementServiceImpl.
 */
@Named
public class DotationDepartementServiceImpl implements DotationDepartementService {

    /** The Constant NOMBRE_DEPARTEMENTS_FRANCAIS. */
    private static final int NOMBRE_DEPARTEMENTS_FRANCAIS = 110;

    /** The ligne dotation departement repository. */
    @Inject
    private LigneDotationDepartementRepository ligneDotationDepartementRepository;

    /** The dotation nationale service. */
    @Inject
    private DotationNationaleService dotationNationaleService;

    /** The departement repository. */
    @Inject
    private DepartementRepository departementRepository;

    /** The dotation collectivite service. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The collectivite repository. */
    @Inject
    private CollectiviteRepository collectiviteRepository;

    /** The dotation departement repository. */
    @Inject
    private DotationDepartementRepository dotationDepartementRepository;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The courrier annuel departement service. */
    @Inject
    private CourrierAnnuelDepartementService courrierAnnuelDepartementService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#televerserEtNotifierDotationDepartement(java.lang.Integer,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public void televerserEtNotifierDotationDepartement(Integer idDepartement, FichierTransfert fichierCourrierDotation) {

        String codeDepartement = this.departementRepository.getCodeParId(idDepartement);
        this.courrierAnnuelDepartementService.televerserEtCreerDocumentCourrierRepartition(codeDepartement, fichierCourrierDotation);

        this.ligneDotationDepartementRepository.majDateEnvoiLigneDotationDepartement(idDepartement, this.now());

        if (this.autoriserRepartitionAutomatique(idDepartement)) {
            this.repartirAutomatique(idDepartement);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#rechercherDotationsDepartementalesParAnneeEnCours(fr.gouv.sitde.face.
     * transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationDepartementaleBo> rechercherDotationsDepartementalesParAnneeEnCours(PageDemande pageDemande) {

        // init.
        List<DotationDepartementaleBo> listeDotationsDepartementales = new ArrayList<>(NOMBRE_DEPARTEMENTS_FRANCAIS);
        List<SousProgramme> listeSousProgrammes = this.sousProgrammeService.rechercherPourDotationDepartement();
        Integer anneeEnCours = this.now().getYear();
        BigDecimal sommePourDepartement;

        // récupération des départements
        PageReponse<Departement> listeDepartements = this.departementRepository.rechercherDepartementsPagine(pageDemande);

        // liste des dotation pour la liste des départements
        List<DotationDepartement> listeDotaDep = this.dotationDepartementRepository
                .rechercherTousPourDepartements(listeDepartements.getListeResultats());

        // associe un id de département à une liste de dotations
        Map<Integer, List<DotationDepartement>> dotationsParDepartement = genererMapDotations(listeDotaDep);

        for (Departement departementCours : listeDepartements.getListeResultats()) {

            DotationDepartementaleBo dotationDepartementaleBo = new DotationDepartementaleBo(anneeEnCours, departementCours);
            Penalite penalite = departementCours.getPenalites().stream().filter(item -> anneeEnCours.equals(item.getAnnee())).findAny().orElse(null);
            dotationDepartementaleBo.setNonRegroupement(calculerPenaliteNonRegroupement(penalite));
            dotationDepartementaleBo.setStock(calculerPenaliteStock(penalite));
            dotationDepartementaleBo.setEnPreparation(
                    this.dotationDepartementRepository.ligneDotationDepartementEnPreparationExiste(anneeEnCours, departementCours.getCode()));
            dotationDepartementaleBo
                    .setNotifiee(this.dotationDepartementRepository.ligneDotationDepartementNotifieeExiste(anneeEnCours, departementCours.getCode()));

            // Instanciation nouvelle liste listeDotationsDepartementalesParSousProgramme
            // à partir de la liste de sous-programmes (tous les dspMontantsont donc à zero)
            // >> l'objectif est d'avoir ainsi toutes les colonnes du tableau à 0 par défaut
            List<DotationDepartementaleParSousProgrammeBo> listeDotationsDepartementalesParSousProgramme = new ArrayList<>(
                    listeSousProgrammes.size());
            for (SousProgramme sousProgramme : listeSousProgrammes) {
                if (this.isSousProgrammeValide(sousProgramme)) {
                    DotationDepartementaleParSousProgrammeBo dotaDepParSp = new DotationDepartementaleParSousProgrammeBo(sousProgramme,
                            BigDecimal.ZERO);
                    listeDotationsDepartementalesParSousProgramme.add(dotaDepParSp);
                }
            }

            // pour calcul de la somme par département
            sommePourDepartement = BigDecimal.ZERO;

            // pour le département en cours : récupération du total par sous programme
            // >> l'objectif est de mettre à jour les colonnes de sous-programmes du tableau
            for (DotationDepartementaleParSousProgrammeBo dotaDepParSp : listeDotationsDepartementalesParSousProgramme) {
                // on récupère les dotations du département courrant
                List<DotationDepartement> listeDotationsDepartement = dotationsParDepartement.get(departementCours.getId());
                if (CollectionUtils.isEmpty(listeDotationsDepartement)) {
                    continue;
                }
                // pour chaque dotation (trouvée sur ce département) : on récupère le montant total du sous-programme
                for (DotationDepartement dotaDep : listeDotationsDepartement) {
                    // on cherche le sous-programme en cours
                    if (dotaDep.getDotationSousProgramme().getSousProgramme().getId().equals(dotaDepParSp.getSousProgramme().getId())
                            && anneeEnCours.equals(dotaDep.getDotationSousProgramme().getDotationProgramme().getAnnee())) {
                        // mise à jour : montant dans la colonne (sous programme pour la ligne du département)
                        dotaDepParSp.setDspMontant(dotaDep.getMontantTotal());
                        // mise à jour : calcul somme
                        sommePourDepartement = sommePourDepartement.add(dotaDep.getMontantTotal());
                    }
                }
            }

            // affectation des données trouvées
            dotationDepartementaleBo.setSomme(sommePourDepartement);
            dotationDepartementaleBo.setListeDotationsDepartementalesParSousProgramme(listeDotationsDepartementalesParSousProgramme);
            listeDotationsDepartementales.add(dotationDepartementaleBo);
        }

        // construction de la réponse
        return new PageReponse<>(listeDotationsDepartementales, listeDepartements.getNbTotalResultats());
    }

    /**
     * Génère une map qui associe un ID de département à une liste de dotations. Permet ensuite d'obtenir rapidement la liste des dotations par
     * départements.
     *
     * @param listeDotaDep
     *            the liste dota dep
     * @return the map
     */
    private static Map<Integer, List<DotationDepartement>> genererMapDotations(List<DotationDepartement> listeDotaDep) {
        Map<Integer, List<DotationDepartement>> mapDepartementDotations = new HashMap<>(listeDotaDep.size());

        for (DotationDepartement dotationDepartement : listeDotaDep) {
            Integer idDepartement = dotationDepartement.getDepartement().getId();
            List<DotationDepartement> listeDotations = mapDepartementDotations.get(idDepartement);

            // pas de dotations pour ce département, on crée une nouvelle liste
            if (listeDotations == null) {
                listeDotations = new ArrayList<>(10);
            }
            // maj liste (1 seul item ou ajout)
            listeDotations.add(dotationDepartement);
            // mise à jour de la map
            mapDepartementDotations.put(idDepartement, listeDotations);
        }
        return mapDepartementDotations;
    }

    /**
     * Calculer penalite non regroupement.
     *
     * @param penalite
     *            the penalite
     * @return the big decimal
     */
    private static BigDecimal calculerPenaliteNonRegroupement(Penalite penalite) {
        if ((penalite == null) || (penalite.getNonRegroupement() == null)) {
            return BigDecimal.ZERO;
        } else {
            return penalite.getNonRegroupement();
        }
    }

    /**
     * Vérifie la validité du sous programme
     *
     * @param sousProgramme
     *            the sous programme
     * @return true, if is sous programme valide
     */
    private boolean isSousProgrammeValide(SousProgramme sousProgramme) {
        return (sousProgramme.getDateDebutValidite().getYear() <= this.now().getYear())
                && ((sousProgramme.getDateFinValidite() == null) || (this.now().getYear() <= sousProgramme.getDateFinValidite().getYear()));
    }

    /**
     * Calculer penalite stock.
     *
     * @param penalite
     *            the penalite
     * @return the big decimal
     */
    private static BigDecimal calculerPenaliteStock(Penalite penalite) {
        if ((penalite == null) || (penalite.getStock() == null)) {
            return BigDecimal.ZERO;
        } else {
            return penalite.getStock();
        }
    }

    /**
     * Now.
     *
     * @return the offset date time
     */
    protected LocalDateTime now() {
        return this.timeOperationTransverse.now();
    }

    /**
     * Repartir automatique.
     *
     * @param idDepartement
     *            the id departement
     */
    private void repartirAutomatique(Integer idDepartement) {
        List<SousProgramme> listeSousProgrammes = this.sousProgrammeService.rechercherPourDotationDepartement();
        Integer anneeEnCours = this.now().getYear();
        String codeDepartement = this.departementRepository.getCodeParId(idDepartement);

        for (SousProgramme sousProgramme : listeSousProgrammes) {

            List<DotationCollectivite> listeDotCol = this.dotationCollectiviteService
                    .rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(codeDepartement, sousProgramme.getAbreviation(),
                            anneeEnCours);

            DotationDepartement dotDep = this.dotationDepartementRepository.rechercherDotationDepartementTroisCriteres(anneeEnCours, codeDepartement,
                    sousProgramme.getCodeDomaineFonctionnel());

            // S'il y a une dotation collectivité elle est mise à jour.
            if (!listeDotCol.isEmpty()) {

                // Il n'y a qu'une collectivité dans le département, donc une seule dotation collectivité possible par sous programme.
                DotationCollectivite dotCol = listeDotCol.get(0);
                dotCol.setMontant(dotDep.getMontantNotifie());
                this.dotationCollectiviteService.majDotationCollectivite(dotCol);
                // Sinon elle est créée et remplie, si la dotation departement existe pour le sous programme.
            } else if (dotDep != null) {

                // Création d'une collectivité à remplir.
                DotationCollectivite dotCol = new DotationCollectivite();

                // Il n'y a qu'une collectivité dans le département (vérifié au préalable).
                List<Collectivite> listeCol = this.collectiviteRepository.rechercherCollectivitesParCodeDepartement(codeDepartement);
                dotCol.setCollectivite(listeCol.get(0));

                dotCol.setDotationDepartement(this.dotationDepartementRepository.rechercherDotationDepartementTroisCriteres(anneeEnCours,
                        codeDepartement, sousProgramme.getCodeDomaineFonctionnel()));
                dotCol.setMontant(dotDep.getMontantNotifie());

                this.dotationCollectiviteService.majDotationCollectivite(dotCol);
            }
        }
    }

    /**
     * Autoriser repartition automatique.
     *
     * @param idDepartement
     *            the id departement
     * @return true, if successful
     */
    private boolean autoriserRepartitionAutomatique(Integer idDepartement) {
        return this.departementRepository.compterCollectivitePourDepartement(idDepartement).equals(1);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#rechercherDotationDepartementPourRepartition(java.lang.String)
     */
    @Override
    public List<DotationDepartement> rechercherDotationDepartementPourRepartition(String codeDepartement) {

        List<SousProgramme> listeSousProgrammes = this.sousProgrammeService.rechercherPourDotationDepartement();

        List<DotationDepartement> dotationsDepartement = new ArrayList<>(listeSousProgrammes.size());

        for (SousProgramme sousProgramme : listeSousProgrammes) {

            DotationDepartement dotationDepartement = this.dotationDepartementRepository.rechercherDotationDepartementPourRepartition(codeDepartement,
                    sousProgramme.getCodeDomaineFonctionnel());

            if (dotationDepartement == null) {
                dotationDepartement = new DotationDepartement();
                dotationDepartement.setMontantNotifie(new BigDecimal(0));
                dotationDepartement.setDotationSousProgramme(new DotationSousProgramme());
                dotationDepartement.getDotationSousProgramme().setSousProgramme(sousProgramme);
            }
            dotationsDepartement.add(dotationDepartement);
        }

        return dotationsDepartement;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#rechercherDotationDepartementPourRepartition(java.lang.String,
     * java.lang.String)
     */
    @Override
    public DotationDepartement rechercherDotationDepartementPourRepartition(String codeDepartement, String codeDomaineFonctionnel) {
        return this.dotationDepartementRepository.rechercherDotationDepartementPourRepartition(codeDepartement, codeDomaineFonctionnel);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#rechercherParId(java.lang.Long)
     */
    @Override
    public DotationDepartement rechercherParId(Long dotationDepartementId) {
        return this.dotationDepartementRepository.rechercherParId(dotationDepartementId);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#rechercherDotationDepartementParIdCollectivite(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public DotationDepartement rechercherDotationDepartementParIdCollectivite(Long idCollectivite, Integer idSousProgramme) {
        return this.dotationDepartementRepository.rechercherDotationDepartementParIdCollectivite(idCollectivite, idSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationDepartementService#creerDotationDepartement(java.lang.Long, java.lang.Integer)
     */
    @Override
    public DotationDepartement creerDotationDepartement(Long idCollectivite, Integer idSousProgramme) {
        DotationDepartement dotationDepartement = new DotationDepartement();

        dotationDepartement.setDepartement(this.departementRepository.rechercherDepartementByIdCollectivite(idCollectivite));

        dotationDepartement.setDotationRepartie(BigDecimal.ZERO);
        dotationDepartement.setMontantNotifie(BigDecimal.ZERO);
        dotationDepartement.setMontantTotal(BigDecimal.ZERO);

        dotationDepartement.setDotationSousProgramme(this.dotationNationaleService
                .recupererDotationSousProgrammeParAnneeEtSousProgramme(this.timeOperationTransverse.getAnneeCourante(), idSousProgramme));

        return this.dotationDepartementRepository.creer(dotationDepartement);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.dto;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * @author A754839
 *
 */
public class RepartitionPaiementDto {

    private List<DemandeSubvention> listeDemandeSubvention;

    private List<BigDecimal> montantTransfertsAcompteRestant;

    private List<BigDecimal> montantTransfertsAvanceRestant;

    private List<BigDecimal> montantTransfertsSoldeRestant;

    private List<TransfertPaiement> listeTransfertPaiement;

    private List<TransfertPaiement> listeTransfertsGeneres;

    private BigDecimal aideDemandee;

    private DemandePaiement demandePaiement;

    /**
     * @return the listeDemandeSubvention
     */
    public List<DemandeSubvention> getListeDemandeSubvention() {
        return this.listeDemandeSubvention;
    }

    /**
     * @param listeDemandeSubvention
     *            the listeDemandeSubvention to set
     */
    public void setListeDemandeSubvention(List<DemandeSubvention> listeDemandeSubvention) {
        this.listeDemandeSubvention = listeDemandeSubvention;
    }

    /**
     * @return the montantTransfertsAcompteRestant
     */
    public List<BigDecimal> getMontantTransfertsAcompteRestant() {
        return this.montantTransfertsAcompteRestant;
    }

    /**
     * @param montantTransfertsAcompteRestant
     *            the montantTransfertsAcompteRestant to set
     */
    public void setMontantTransfertsAcompteRestant(List<BigDecimal> montantTransfertsAcompteRestant) {
        this.montantTransfertsAcompteRestant = montantTransfertsAcompteRestant;
    }

    /**
     * @return the montantTransfertsAvanceRestant
     */
    public List<BigDecimal> getMontantTransfertsAvanceRestant() {
        return this.montantTransfertsAvanceRestant;
    }

    /**
     * @param montantTransfertsAvanceRestant
     *            the montantTransfertsAvanceRestant to set
     */
    public void setMontantTransfertsAvanceRestant(List<BigDecimal> montantTransfertsAvanceRestant) {
        this.montantTransfertsAvanceRestant = montantTransfertsAvanceRestant;
    }

    /**
     * @return the montantTransfertsSoldeRestant
     */
    public List<BigDecimal> getMontantTransfertsSoldeRestant() {
        return this.montantTransfertsSoldeRestant;
    }

    /**
     * @param montantTransfertsSoldeRestant
     *            the montantTransfertsSoldeRestant to set
     */
    public void setMontantTransfertsSoldeRestant(List<BigDecimal> montantTransfertsSoldeRestant) {
        this.montantTransfertsSoldeRestant = montantTransfertsSoldeRestant;
    }

    /**
     * @return the listeTransfertPaiement
     */
    public List<TransfertPaiement> getListeTransfertPaiement() {
        return this.listeTransfertPaiement;
    }

    /**
     * @param listeTransfertPaiement
     *            the listeTransfertPaiement to set
     */
    public void setListeTransfertPaiement(List<TransfertPaiement> listeTransfertPaiement) {
        this.listeTransfertPaiement = listeTransfertPaiement;
    }

    /**
     * @return the listeTransfertsGeneres
     */
    public List<TransfertPaiement> getListeTransfertsGeneres() {
        return this.listeTransfertsGeneres;
    }

    /**
     * @param listeTransfertsGeneres
     *            the listeTransfertsGeneres to set
     */
    public void setListeTransfertsGeneres(List<TransfertPaiement> listeTransfertsGeneres) {
        this.listeTransfertsGeneres = listeTransfertsGeneres;
    }

    /**
     * @return the aideDemandee
     */
    public BigDecimal getAideDemandee() {
        return this.aideDemandee;
    }

    /**
     * @param aideDemandee
     *            the aideDemandee to set
     */
    public void setAideDemandee(BigDecimal aideDemandee) {
        this.aideDemandee = aideDemandee;
    }

    /**
     * @return the demandePaiement
     */
    public DemandePaiement getDemandePaiement() {
        return this.demandePaiement;
    }

    /**
     * @param demandePaiement
     *            the demandePaiement to set
     */
    public void setDemandePaiement(DemandePaiement demandePaiement) {
        this.demandePaiement = demandePaiement;
    }
}

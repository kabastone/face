/**
 *
 */
package fr.gouv.sitde.face.domaine.service.document;

import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface DonneesDocumentService.
 */
public interface DonneesDocumentService {

    /**
     * Génération du pdf pour demande de Subvention.
     *
     * @param idDemandeSubvention
     *            L'id de la demande désirée.
     *
     * @return Le FichierTransfert du pdf de la demande.
     *
     */
    FichierTransfert genererDemandeSubventionPdf(Long idDemandeSubvention);

    /**
     * Génération de la décision attributive d'une demande de paiement.
     *
     * @param idDemandePaiement
     *            L'id de la demande désirée.
     *
     * @return Le FichierTransfert du pdf de la demande.
     *
     */
    FichierTransfert genererDecisionAttributive(Long idDemandePaiement);

    /**
     * Génération du pdf d'annexe de dotations pour département.
     *
     * @param idDepartement
     *            L'id du département concerné.
     *
     * @return Le FichierTransfert du pdf de l'annexe.
     *
     */
    FichierTransfert genererAnnexeDotationPdf(Integer idDepartement);

    /**
     * Génération du pdf d'annexe de dotations pour tous les départements.
     *
     *
     * @return Le FichierTransfert du pdf de l'annexe.
     *
     */
    FichierTransfert genererAnnexeDotationPdf();
}

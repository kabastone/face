package fr.gouv.sitde.face.domaine.service.referentiel;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * The Interface DepartementService.
 */
public interface DepartementService {

    /**
     * Rechercher departements.
     *
     * @return the list
     */
    List<Departement> rechercherDepartements();

    /**
     * Rechercher departement par id.
     *
     * @param id
     *            the id
     * @return the departement
     */
    Departement rechercherDepartementParId(Integer id);

    /**
     * Maj departement.
     *
     * @param departement
     *            the departement
     * @return the departement
     */
    Departement majDepartement(Departement departement);

    /**
     * Enregistrer donnees fiscales departement.
     *
     * @param departement
     *            the departement
     * @return the departement
     */
    Departement enregistrerDonneesFiscalesDepartement(Departement departement);

    /**
     * Rechercher departement par code.
     *
     * @param code
     *            the code
     * @return the departement
     */
    Departement rechercherDepartementParCode(String code);
}

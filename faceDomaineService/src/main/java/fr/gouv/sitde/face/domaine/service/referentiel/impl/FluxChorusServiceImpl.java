/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.FluxChorusRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Class FluxChorusServiceImpl.
 */
@Named
public class FluxChorusServiceImpl implements FluxChorusService {

    /** The flux chorus repository. */
    @Inject
    private FluxChorusRepository fluxChorusRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.FluxChorusService#rechercherFluxChorus()
     */
    @Override
    public FluxChorus rechercherFluxChorus() {
        return this.fluxChorusRepository.rechercherFluxChorus();
    }

}

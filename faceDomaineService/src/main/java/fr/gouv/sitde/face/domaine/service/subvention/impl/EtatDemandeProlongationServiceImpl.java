/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeProlongationRepository;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;

/**
 * @author Atos
 *
 */
@Named
public class EtatDemandeProlongationServiceImpl extends BeanValidationService<EtatDemandeProlongation> implements EtatDemandeProlongationService {

    @Inject
    private EtatDemandeProlongationRepository etatDemandeProlongationRepository;

    @Override
    public EtatDemandeProlongation rechercherParCode(String codeEtatDemandeProlongation) {
        return this.etatDemandeProlongationRepository.findParCode(codeEtatDemandeProlongation);
    }
}

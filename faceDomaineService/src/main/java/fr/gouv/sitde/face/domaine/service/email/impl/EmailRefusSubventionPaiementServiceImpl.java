package fr.gouv.sitde.face.domaine.service.email.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.email.EmailRefusSubventionPaiementService;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * The Class EmailRefusSubventionPaiementServiceImpl.
 */
@Named
public class EmailRefusSubventionPaiementServiceImpl implements EmailRefusSubventionPaiementService {

    /** The email service. */
    @Inject
    private EmailService emailService;

    @Inject
    private CollectiviteService collectiviteService;

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE_DOCUMENTS);

    /**
     * Email refus demande subvention paiement.
     *
     * @param dossier
     *            the dossier
     * @param type
     *            the type
     * @param date
     *            the date
     * @param montant
     *            the montant
     * @param motifRefus
     *            the motif refus
     */
    @Override
    public void emailRefusDemandeSubventionPaiement(DossierSubvention dossier, String type, LocalDateTime date, BigDecimal montant,
            String motifRefus) {
        Map<String, Object> variablesContexte = new HashMap<>(100);
        variablesContexte.put("type", type);
        variablesContexte.put("dateDemande", date.format(formatter));
        variablesContexte.put("montantDemande", montant);
        variablesContexte.put("motifRefus", motifRefus);
        variablesContexte.put("sousProgrammeDemande",
                dossier.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme().getDescription());
        Collectivite collectivite = this.collectiviteService.rechercherCollectiviteParId(dossier.getDotationCollectivite().getCollectivite().getId());
        this.emailService.creerEtEnvoyerEmail(collectivite, TypeEmailEnum.DOS_REJET_SUBVENTION_PAIEMENT,
                TemplateEmailEnum.EMAIL_DOS_REJET_SUBVENTION_PAIEMENT, variablesContexte);

    }

}

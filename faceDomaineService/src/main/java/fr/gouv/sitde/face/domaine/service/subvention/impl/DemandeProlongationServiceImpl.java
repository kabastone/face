/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.email.EmailProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeProlongationService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service métier de DemandeProlongation.
 *
 * @author Atos
 */
@Named
public class DemandeProlongationServiceImpl extends BeanValidationService<DemandeProlongation> implements DemandeProlongationService {

    /** The demande paiement repository. */
    @Inject
    private DemandeProlongationRepository demandeProlongationRepository;

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The etat demande prolongation service. */
    @Inject
    private EtatDemandeProlongationService etatDemandeProlongationService;

    @Inject
    private EmailProlongationService emailProlongationService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#rechercherDemandesProlongation(java.lang.Long)
     */
    @Override
    public List<DemandeProlongation> rechercherDemandesProlongation(Long idDossierSubvention) {
        return this.demandeProlongationRepository.findByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#rechercherDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongation rechercherDemandeProlongation(Long idDemandeProlongation) {
        return this.demandeProlongationRepository.rechercherDemandeProlongation(idDemandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#rechercheParDateAchevementEtDossierSubvention(java.time.
     * LocalDateTime, java.lang.String)
     */
    @Override
    public DemandeProlongation rechercheParDateAchevementEtDossierSubvention(LocalDateTime dateLivraisonDuPoste, String numeroEJ) {
        return this.demandeProlongationRepository.rechercheParDateAchevementEtDossierSubvention(dateLivraisonDuPoste, numeroEJ);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#mettreAJourDemandeProlongation(fr.gouv.sitde.face.transverse.entities.
     * DemandeProlongation)
     */
    @Override
    public DemandeProlongation mettreAJourDemandeProlongation(DemandeProlongation demandeProlongation) {
        return this.demandeProlongationRepository.mettreAJourDemandeProlongation(demandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#initNouvelleDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongation initNouvelleDemandeProlongation(Long idDossier) {
        // Le dossier de subvention
        DossierSubvention dossierSubvention = this.dossierSubventionService.rechercherDossierSubventionParId(idDossier);

        if (dossierSubvention == null) {
            throw new TechniqueException("Le dossier n'a pas pu être récupéré.");
        }

        /* mappings */
        DemandeProlongation demandeProlongation = new DemandeProlongation();

        LocalDateTime now = this.timeOperationTransverse.now();
        demandeProlongation.setDateDemande(now);
        demandeProlongation.setDossierSubvention(dossierSubvention);

        return demandeProlongation;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#creerDemandeProlongation(fr.gouv.sitde.face.transverse.entities.
     * DemandeProlongation, java.util.List)
     */
    @Override
    public DemandeProlongation creerDemandeProlongation(DemandeProlongation demande, List<FichierTransfert> fichiersTransferts) {
        // Le dossier de subvention
        DossierSubvention dossierSubvention = this.dossierSubventionService.rechercherDossierSubventionParId(demande.getDossierSubvention().getId());

        if (dossierSubvention == null) {
            throw new TechniqueException("Le dossier n'a pas pu être trouvé.");
        }

        // Rattacher le dossier de subvention.
        demande.setDossierSubvention(dossierSubvention);

        this.validationReglesGestionDemandeProlongation(demande, fichiersTransferts);

        // Passer de l'état INITIAL à l'état DEMANDEE
        this.definirEtatDemandeProlongation(demande, EtatDemandeProlongationEnum.DEMANDEE);

        // Enregistrement de la demande de prolongation
        demande = this.demandeProlongationRepository.enregistrer(demande);

        // Gestion des fichiers
        this.gestionFichiersEnregistrer(fichiersTransferts, demande);

        return demande;
    }

    /**
     * Definir etat demande prolongation.
     *
     * @param demande
     *            the demande
     * @param etatDemande
     *            the etat demande
     */
    private void definirEtatDemandeProlongation(DemandeProlongation demande, EtatDemandeProlongationEnum etatDemande) {
        EtatDemandeProlongation etat = this.etatDemandeProlongationService.rechercherParCode(etatDemande.name());
        demande.setEtatDemandeProlongation(etat);
    }

    /**
     * Gestion fichiers enregistrer.
     *
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @param demande
     *            the demande
     */
    private void gestionFichiersEnregistrer(List<FichierTransfert> listeFichiersUploader, DemandeProlongation demande) {
        for (FichierTransfert fichierTransfere : listeFichiersUploader) {
            Document nouveauDocument = this.documentService.creerDocumentDemandeProlongation(demande, fichierTransfere);
            demande.getDocuments().add(nouveauDocument);
        }
    }

    /**
     * Validation regles gestion demande prolongation.
     *
     * @param demande
     *            the demande
     * @param fichiersTransferts
     *            the fichiers transferts
     */
    private void validationReglesGestionDemandeProlongation(DemandeProlongation demande, List<FichierTransfert> fichiersTransferts) {

        List<MessageProperty> listeErreurs = this.getErreursValidationBean(demande, Default.class);

        if (demande.getDateAchevementDemandee() == null) {
            listeErreurs.add(new MessageProperty("prolongation.demande.date.achevement.demandee.non.renseigne"));
        }

        // Affiche une erreur si la demande de prolongation est absente.
        if (!isTypeDocumentPresent(fichiersTransferts, TypeDocumentEnum.DEMANDE_PROLONGATION)) {
            listeErreurs.add(new MessageProperty("prolongation.demande.fichier.absent"));
        }

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }
    }

    /**
     * Checks if is type document present.
     *
     * @param documents
     *            the documents
     * @param typeDocument
     *            the type document
     * @return true, if is type document present
     */
    private static boolean isTypeDocumentPresent(List<FichierTransfert> documents, TypeDocumentEnum typeDocument) {
        return documents.stream().anyMatch(document -> document.getTypeDocument().getCode().equals(typeDocument.getCode()));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeProlongationService#accorderDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongation accorderDemandeProlongation(Long idDemandeProlongation) {
        DemandeProlongation demande = this.rechercherDemandeProlongation(idDemandeProlongation);

        this.definirEtatDemandeProlongation(demande, EtatDemandeProlongationEnum.ACCORDEE);

        return this.demandeProlongationRepository.enregistrer(demande);

    }

    @Override
    public DemandeProlongation refuserDemandeProlongation(DemandeProlongation demande) {

        // Vérifier la présence d'un motif de refus.
        if (StringUtils.isBlank(demande.getMotifRefus())) {
            throw new RegleGestionException("prolongation.demande.motif.refus.absent");
        }

        this.definirEtatDemandeProlongation(demande, EtatDemandeProlongationEnum.REFUSEE);

        // Envoi d'un email de refus.
        this.emailProlongationService.envoyerEmailDemandeProlongationRefusee(demande.getId());

        return this.demandeProlongationRepository.enregistrer(demande);
    }
}

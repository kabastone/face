/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService;
import fr.gouv.sitde.face.domaine.service.email.EmailRefusSubventionPaiementService;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.domaine.service.subvention.CadencementService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.EtatDemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * The Class DemandeSubventionServiceImpl.
 */
@Named
public class DemandeSubventionServiceImpl extends BeanValidationService<DemandeSubvention> implements DemandeSubventionService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DemandeSubventionServiceImpl.class);

    /** The demande subvention repository. */
    @Inject
    private DemandeSubventionRepository demandeSubventionRepository;

    /** The sous programme service. */
    @Inject
    private SousProgrammeService sousProgrammeService;

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The etat demande subvention service. */
    @Inject
    private EtatDemandeSubventionService etatDemandeSubventionService;

    /** The reglementaire service. */
    @Inject
    private ReglementaireService reglementaireService;

    /** The dotation collectivite. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The ligne dotation departement service. */
    @Inject
    private LigneDotationDepartementService ligneDotationDepartementService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** The email service. */
    @Inject
    private EmailService emailService;

    @Inject
    private CadencementService cadencementService;

    @Inject
    private EmailRefusSubventionPaiementService emailRefusSubventionPaiementService;

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantesFace.FORMAT_DATE_DOCUMENTS);

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherDemandesSubvention(java.lang.Long)
     */
    @Override
    public List<DemandeSubvention> rechercherDemandesSubvention(Long idDossierSubvention) {
        return this.demandeSubventionRepository.findByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherDemandeSubventionParId(java.lang.Long)
     */
    @Override
    public DemandeSubvention rechercherDemandeSubventionParId(Long idDemandeSubvention) {
        return this.demandeSubventionRepository.rechercheDemandeSubventionParId(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# recupererChorusNumLigneLegacy(java.lang.Long)
     */
    @Override
    public String recupererChorusNumLigneLegacy(Long idDossier) {
        // Récupération du numéro Chorus
        String numChorus = this.demandeSubventionRepository.recupererMaxChorusNumLigneLegacy(idDossier);

        Long lNumChorus = 1L;
        if (numChorus != null) {
            // Incrémentation du numéro Chorus
            lNumChorus = Long.parseLong(numChorus) + 1;
        }

        // Mise en forme
        return StringUtils.leftPad(lNumChorus.toString(), 10, '0');
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# initNouvelleDemandeSubvention()
     */
    @Override
    public DemandeSubvention initNouvelleDemandeSubvention(Long idDossierSubvention) {

        DossierSubvention dossierSubvention = null;
        if (idDossierSubvention == null) {
            // Le dossier de subvention
            dossierSubvention = this.dossierSubventionService.initNouveauDossierSubvention();
        } else {
            dossierSubvention = this.dossierSubventionService.rechercherDossierSubventionParId(idDossierSubvention);
        }

        if (dossierSubvention == null) {
            throw new TechniqueException("Le dossier n'a pas pu être créé ou récupéré.");
        }

        /* mappings */
        DemandeSubvention demandeSubvention = new DemandeSubvention();

        LocalDateTime now = this.timeOperationTransverse.now();
        demandeSubvention.setDateEtat(now);
        demandeSubvention.setDateDemande(now);
        demandeSubvention.setChorusNumLigneLegacy(this.recupererChorusNumLigneLegacy(dossierSubvention.getId()));
        demandeSubvention.setDossierSubvention(dossierSubvention);

        return demandeSubvention;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherDemandeSubventionPrincipaleParIdChorus(java.lang.Long)
     */
    @Override
    public DemandeSubvention rechercherDemandeSubventionPrincipaleParIdChorus(Long idDossierChorus) {
        return this.demandeSubventionRepository.findDemandePrincipaleByDossierChorus(idDossierChorus);
    }

    /**
     * Rechercher demande subvention principale par id dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the demande subvention
     */
    @Override
    public DemandeSubvention rechercherDemandeSubventionPrincipaleParIdDossier(Long idDossier) {
        return this.demandeSubventionRepository.findDemandePrincipaleByIdDossier(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#
     * mettreAJourDemandeSubvention(fr.gouv.sitde.face.transverse.entities. DemandeSubvention)
     */
    @Override
    public DemandeSubvention mettreAJourDemandeSubvention(DemandeSubvention demande) {
        return this.demandeSubventionRepository.mettreAJourDemandeSubvention(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# creerDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention creerDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts) {

        Integer idSousProgramme = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getId();

        boolean nouveauDossier = false;
        if (demande.getDossierSubvention().getId() == null) {
            nouveauDossier = this.creerOuRecupererDossierSubvention(demande);
        } else {
            demande.setDossierSubvention(this.dossierSubventionService.rechercherDossierSubventionParId(demande.getDossierSubvention().getId()));
        }

        if (!nouveauDossier) {
            demande.setChorusNumLigneLegacy(this.recupererChorusNumLigneLegacy(demande.getDossierSubvention().getId()));

            DemandeSubvention demandeEstComplementaire = this.demandeSubventionRepository
                    .findDemandePrincipaleByIdDossier(demande.getDossierSubvention().getId());
            demande.setDemandeComplementaire(demandeEstComplementaire != null);

        }

        // Valide et lève une exception si les RG ne sont pas respectés.
        this.validationReglesGestionDemandeSubvention(demande, fichiersTransferts, idSousProgramme);

        demande.getCadencement().setDemandeSubvention(demande);

        // Lors de la création, la demande est à l'état "DEMANDEE".
        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.DEMANDEE);

        demande = this.demandeSubventionRepository.enregistrer(demande);

        this.gestionFichiersEnregistrer(fichiersTransferts, demande);

        return demande;
    }

    /**
     * Creer ou recuperer dossier subvention.
     *
     * @param demande
     *            the demande
     * @return true, if successful
     * @throws TechniqueException
     *             the technique exception
     */
    private boolean creerOuRecupererDossierSubvention(DemandeSubvention demande) {
        // Gestion du dossier de subvention.
        DossierSubvention dossierSubvention = null;
        boolean nouveau = false;

        // Recherche du sous programme pour clarte du if.
        SousProgramme sousProgramme = this.sousProgrammeService.rechercherParId(demande.getDossierSubvention().getDotationCollectivite()
                .getDotationDepartement().getDotationSousProgramme().getSousProgramme().getId());

        // S'il existe un dossier en base correspondant a la demande et que la demande
        // est de travaux, on va le rechercher.
        List<DossierSubvention> listeDossiers = this.dossierSubventionService.rechercherDossierPourDemande(demande);
        if ((listeDossiers.size() == 1) && !sousProgramme.isDeProjet()) {
            dossierSubvention = listeDossiers.get(0);
        } else {
            nouveau = true;
            dossierSubvention = this.dossierSubventionService.creerDossierSubventionDepuisDemandeSubvention(demande.getDossierSubvention());
        }

        // Définition du dossier dans la demande.
        demande.setDossierSubvention(dossierSubvention);
        return nouveau;
    }

    /**
     * Récupère le document dans la demande de subvention via son code de type document.
     *
     * @param demandeSubvention
     *            the demande subvention
     * @param codeTypeDocument
     *            code du type du document à chercher
     * @return le document trouvé ou null.
     */
    private static Document getDocumentParCodeDocument(DemandeSubvention demandeSubvention, String codeTypeDocument) {

        for (Document document : demandeSubvention.getDocuments()) {
            if (document.getTypeDocument().getCode().equals(codeTypeDocument)) {
                return document;
            }
        }
        return null;
    }

    /**
     * Enregistre les fichiers en bdd et les relie à la demande de subvention.
     *
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @param demande
     *            the demande
     */
    private void gestionFichiersEnregistrer(List<FichierTransfert> listeFichiersUploader, DemandeSubvention demande) {
        for (FichierTransfert fichierTransfere : listeFichiersUploader) {
            // Gestion des docs complementaires subvention
            if (TypeDocumentEnum.DOC_COMPLEMENTAIRE_SUBVENTION.equals(fichierTransfere.getTypeDocument())) {
                Document nouveauDocument = this.documentService.creerDocumentDemandeSubvention(demande, fichierTransfere);
                demande.getDocuments().add(nouveauDocument);
            } else {
                Document documentExistant = getDocumentParCodeDocument(demande, fichierTransfere.getTypeDocument().getCode());
                if (documentExistant == null) {
                    // le document n'existe pas : on le crée et on l'ajoute à la liste
                    Document nouveauDocument = this.documentService.creerDocumentDemandeSubvention(demande, fichierTransfere);
                    demande.getDocuments().add(nouveauDocument);
                } else {
                    // le document existe : on doit le mettre à jour
                    Document documentModifie = this.documentService.modifierFichierDocument(documentExistant.getId(), fichierTransfere);
                    // suppression du document précédent
                    demande.getDocuments().remove(documentExistant);
                    // ajout du document modifié
                    demande.getDocuments().add(documentModifie);
                }
            }
        }
    }

    /**
     * Gestion fichiers supprimer.
     *
     * @param idsDocsComplSuppr
     *            the ids docs compl suppr
     * @param demande
     *            the demande
     */
    private void gestionFichiersComplementairesSupprimer(List<Long> idsDocsComplSuppr, DemandeSubvention demande) {
        // Suppression des fichiers complémentaires
        for (Long idDocument : idsDocsComplSuppr) {
            Document document = this.documentService.rechercherDocumentParIdDocument(idDocument);
            demande.getDocuments().remove(document);
            this.documentService.supprimerDocument(idDocument);
        }
    }

    /**
     * Validation RG demande subvention.
     *
     * @param demande
     *            the demande
     * @param fichiersTransferts
     *            the fichiers transferts
     * @param idSousProgramme
     *            the id sous programme
     */
    private void validationReglesGestionDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts,
            Integer idSousProgramme) {

        SousProgramme sousProgramme = this.sousProgrammeService.rechercherParId(idSousProgramme);

        List<MessageProperty> listeErreurs = this.getErreursValidationBean(demande, Default.class);

        // Valide et lève une exception si le montant total du cadencement est incorrect
        BigDecimal aideDemandee = demande.getMontantTravauxEligible().multiply(demande.getTauxAide()).divide(new BigDecimal(100));
        this.cadencementService.validerMontantCadencement(demande.getCadencement(), aideDemandee);

        DemandeSubventionServiceImpl.validerDotationCollectivite(demande);

        DotationCollectivite dotation = demande.getDossierSubvention().getDotationCollectivite();

        BigDecimal tauxAideMax = new BigDecimal(
                this.reglementaireService.rechercherReglementaire(demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                        .getDotationSousProgramme().getDotationProgramme().getAnnee()).getTauxAideSubvention());
        BigDecimal dotationRepartie = dotation.getDotationRepartie();
        BigDecimal montantDotation = dotation.getMontant();

        if (listeErreurs.isEmpty()) {

            // vérif - Montant aide
            if (demande.getMontantTravauxEligible().compareTo(BigDecimal.ZERO) <= 0) {
                listeErreurs.add(new MessageProperty("subvention.demande.montant.negatif"));
            }
            if (demande.getTauxAide().compareTo(BigDecimal.ZERO) <= 0) {
                listeErreurs.add(new MessageProperty("subvention.demande.taux.negatif"));
            }

            if (!this.dotationCollectiviteService.isEstUniqueSubventionEnCoursParSousProgramme(demande.getId(), dotation.getId(),
                    sousProgramme.getDescription())) {
                listeErreurs.add(new MessageProperty("subvention.demande.non.unique.par.programme"));
            }

            if (demande.getTauxAide().compareTo(tauxAideMax) > 0) {
                // Vérifier le taux d'aide
                String[] params = new String[] { tauxAideMax.toString() };
                listeErreurs.add(new MessageProperty("subvention.demande.taux.aide.incorrecte", params));
            }
            // Résultats calculés
            BigDecimal calculDotationDisponible = this.dotationCollectiviteService.calculerDotationDisponible(dotation);
            BigDecimal calculSommePlafondEtMontantTravaux = dotationRepartie
                    .add(demande.getMontantTravauxEligible().multiply(demande.getTauxAide()).divide(new BigDecimal(100)));

            // Si état Anomalie Signalée, on recommence de zéro
            boolean isEtatAnomalieSignalee = EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE.name().equals(demande.getEtatDemandeSubvention().getCode());
            if (isEtatAnomalieSignalee) {
                calculDotationDisponible = calculDotationDisponible.add(dotationRepartie);
                calculSommePlafondEtMontantTravaux = calculSommePlafondEtMontantTravaux.subtract(dotationRepartie);
            }

            // Vérifier la cohérence avec la dotation
            if (!sousProgramme.isDeProjet() && (montantDotation.compareTo(calculSommePlafondEtMontantTravaux) < 0)) {
                String[] params = new String[] { MessageUtils.formatMontant(calculSommePlafondEtMontantTravaux.subtract(montantDotation)),
                        sousProgramme.getDescription() };
                listeErreurs.add(new MessageProperty("subvention.demande.dotation.insuffisante", params));
            }

            // Vérifier le plafond d’aide demandé
            if (!sousProgramme.isDeProjet() && (demande.getPlafondAide().compareTo(calculDotationDisponible) > 0)) {
                listeErreurs.add(new MessageProperty("subvention.demande.plafond.aide.incorrecte"));
            }

            listeErreurs.addAll(this.validerPresenceFichiers(demande, fichiersTransferts));

        }

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }
    }

    /**
     * @param demande
     * @param fichiersTransferts
     * @param listeErreurs
     */
    private List<MessageProperty> validerPresenceFichiers(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts) {

        List<MessageProperty> listeErreurs = new ArrayList<>();
        boolean isEtatAnomalieSignalee = EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE.name().equals(demande.getEtatDemandeSubvention().getCode());
        if (!EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.name().equals(demande.getEtatDemandeSubvention().getCode())) {
            // Gestion des fichiers
            if (fichiersTransferts.isEmpty() && (demande.getId() == null) && !isEtatAnomalieSignalee) {
                listeErreurs.add(new MessageProperty("upload.fichier.aucun"));
            } else {

                // Récuperer la demande en base pour avoir les documents d'origine
                Set<Document> documents = new LinkedHashSet<>(0);
                if (isEtatAnomalieSignalee) {
                    DemandeSubvention demandeOriginale = this.demandeSubventionRepository.findById(demande.getId());
                    documents = demandeOriginale.getDocuments();
                }

                // Affiche une erreur si l'état prévisionnel PDF est absent.
                if (!isTypeDocumentPresent(isEtatAnomalieSignalee, fichiersTransferts, documents, TypeDocumentEnum.ETAT_PREVISIONNEL_PDF)) {
                    listeErreurs.add(new MessageProperty("subvention.demande.etat.previsionnel.pdf.absent"));
                }

                // Affiche une erreur si l'état prévisionnel Tableur est absent.
                if (!isTypeDocumentPresent(isEtatAnomalieSignalee, fichiersTransferts, documents, TypeDocumentEnum.ETAT_PREVISIONNEL_TABLEUR)) {
                    listeErreurs.add(new MessageProperty("subvention.demande.etat.previsionnel.tableur.absent"));
                }

            }
        }

        return listeErreurs;
    }

    /**
     * @param demande
     */
    private static void validerDotationCollectivite(DemandeSubvention demande) {
        if ((demande == null) || (demande.getDossierSubvention() == null) || (demande.getDossierSubvention().getDotationCollectivite() == null)) {
            throw new TechniqueException("La demande / le dossier de subvention ou la dotation de collectivité n'est pas renseigné.");
        }
    }

    /**
     * Checks if is type document present.
     *
     * @param documents
     *            the documents
     * @param typeDocument
     *            the etat previsionnel pdf
     * @return true, if is type document present
     */
    private static boolean isTypeDocumentPresent(List<FichierTransfert> documents, TypeDocumentEnum typeDocument) {
        return documents.stream().anyMatch(document -> document.getTypeDocument().getCode().equals(typeDocument.getCode()));
    }

    /**
     * Checks if is type document present.
     *
     * @param documents
     *            the documents
     * @param typeDocument
     *            the etat previsionnel pdf
     * @return true, if is type document present
     */
    private static boolean isTypeDocumentPresent(Set<Document> documents, TypeDocumentEnum typeDocument) {
        return documents.stream().anyMatch(document -> document.getTypeDocument().getCode().equals(typeDocument.getCode()));
    }

    /**
     * Checks if is type document present.
     *
     * @param isEtatAnomalieSignalee
     *            the is etat anomalie signalee
     * @param fichiersTransferts
     *            the fichiers transferts
     * @param documents
     *            the documents
     * @param typeDocument
     *            the type document
     * @return true, if is type document present
     */
    private static boolean isTypeDocumentPresent(boolean isEtatAnomalieSignalee, List<FichierTransfert> fichiersTransferts, Set<Document> documents,
            TypeDocumentEnum typeDocument) {
        return isTypeDocumentPresent(fichiersTransferts, typeDocument) || (isEtatAnomalieSignalee && isTypeDocumentPresent(documents, typeDocument));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# accorderDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention accorderDemandeSubvention(Long id) {

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);
        SousProgramme sousProgramme = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme();

        LocalDateTime dateAccordEtEnvoi = this.timeOperationTransverse.now();
        demande.setDateAccord(dateAccordEtEnvoi);

        // Vérification de la dotation sous programme si il est "de projet=true".
        // Création d'une Ligne de Dotation de Département
        // Mettre à jour une dotation de collectivité
        if (sousProgramme.isDeProjet()) {

            BigDecimal montant = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                    .getMontant();
            BigDecimal dotationRepartie = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                    .getDotationRepartie();
            BigDecimal plafondAide = demande.getPlafondAide();

            // montant < plafondAide => RGException
            if (montant.compareTo(plafondAide.add(dotationRepartie)) < 0) {
                throw new RegleGestionException("subvention.demande.dotation.sous.programme.insuffisante",
                        MessageUtils.formatMontant(plafondAide.add(dotationRepartie).subtract(montant)), sousProgramme.getDescription());
            }

            // creer LDD avec demande plafondAide
            LigneDotationDepartement ligneDotationDepartement = new LigneDotationDepartement();
            ligneDotationDepartement.setMontant(plafondAide);
            ligneDotationDepartement.setDotationDepartement(demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement());
            ligneDotationDepartement.setDateEnvoi(dateAccordEtEnvoi);
            this.ligneDotationDepartementService.ajouterLigneDotationDepartement(ligneDotationDepartement,
                    TypeDotationDepartementEnum.EXCEPTIONNELLE);

            // update dco avec existant+plafondAide
            DotationCollectivite dotationCollectivite = demande.getDossierSubvention().getDotationCollectivite();
            dotationCollectivite.setMontant(dotationCollectivite.getMontant().add(plafondAide));
            this.dotationCollectiviteService.majDotationCollectivite(dotationCollectivite);
        }

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.ACCORDEE);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# controlerDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention controlerDemandeSubvention(Long id) {

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.CONTROLEE);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# controlerDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention initierTransfertDemandeSubvention(Long id) {

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.EN_COURS_TRANSFERT);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#
     * transfererDemandeSubvention(fr.gouv.sitde.face.transverse.entities. DemandeSubvention)
     */
    @Override
    public DemandeSubvention transfererDemandeSubvention(Long id) {

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);

        if (demande.isDemandeComplementaire()) {
            this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.EN_ATTENTE_TRANSFERT_MAN);
        } else {
            this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.EN_ATTENTE_TRANSFERT);
        }

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# qualifierDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention qualifierDemandeSubvention(DemandeSubvention demandeCible, List<FichierTransfert> fichiersTransferts) {

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (this.anomalieService.contientAnomaliesEtACorrigerParIdDemandeSubvention(demandeCible.getId())) {
            throw new RegleGestionException("subvention.demande.repondre.anomalies");
        }

        // si pas dossier existant alors on valide le numero de dossier
        if (!demandeCible.isDemandeComplementaire()) {
            this.validerNumeroDossier(demandeCible);
        }

        DemandeSubvention demandeOrigine = this.rechercherDemandeSubventionParId(demandeCible.getId());
        demandeOrigine.setDemandeComplementaire(demandeCible.isDemandeComplementaire());
        DemandeSubventionServiceImpl.verifierPresenceFicheSiret(demandeOrigine, fichiersTransferts);

        // si flag dans l'ihm indiquant que c'est un dossier existant
        DossierSubvention dossierCible = this.dossierSubventionService.rechercherDossierSubventionParId(demandeCible.getDossierSubvention().getId());

        if (demandeCible.isDemandeComplementaire()) {
            return this.gererQualificationDemandeComplementaire(demandeCible, fichiersTransferts, demandeOrigine, dossierCible);
        }

        this.gererEtatEtFichierPourQualification(demandeCible, fichiersTransferts, demandeOrigine, dossierCible);

        return this.demandeSubventionRepository.mettreAJourDemandeSubvention(demandeOrigine);

    }

    /**
     * @param demandeCible
     * @param fichiersTransferts
     * @param demandeOrigine
     * @param dossierCible
     */
    private void gererEtatEtFichierPourQualification(DemandeSubvention demandeCible, List<FichierTransfert> fichiersTransferts,
            DemandeSubvention demandeOrigine, DossierSubvention dossierCible) {
        // Recuperation du dossier selectionne par l'agent
        dossierCible.setNumDossier(demandeCible.getDossierSubvention().getNumDossier());
        demandeOrigine.setDossierSubvention(dossierCible);

        this.gestionFichiersEnregistrer(fichiersTransferts, demandeOrigine);
        this.definirEtatDemandeSubvention(demandeOrigine, EtatDemandeSubventionEnum.QUALIFIEE);
    }

    /**
     * @param demandeCible
     * @param fichiersTransferts
     * @param demandeOrigine
     * @param dossierCible
     * @return
     */
    private DemandeSubvention gererQualificationDemandeComplementaire(DemandeSubvention demandeCible, List<FichierTransfert> fichiersTransferts,
            DemandeSubvention demandeOrigine, DossierSubvention dossierCible) {

        DemandeSubvention demandeReferenceDossier = dossierCible.getDemandesSubvention().stream()
                .filter(subvention -> !subvention.isDemandeComplementaire()).findFirst().orElse(null);
        DossierSubvention dossierOrigine = this.dossierSubventionService
                .rechercherDossierSubventionParId(demandeOrigine.getDossierSubvention().getId());

        if (demandeReferenceDossier == null) {
            throw new TechniqueException("il n'y a pas de demande initiale sur le dossier");
        }

        boolean demandeSubventionUnique = false;

        if (demandeCible.getTauxAide().compareTo(demandeReferenceDossier.getTauxAide()) != 0) {
            dossierCible = this.initialiserDossierPourDemandeComplementaireAvecTauxDifferent(demandeOrigine);
            demandeOrigine.setDemandeComplementaire(false);
        }

        if (!dossierOrigine.getId().equals(dossierCible.getId())) {
            demandeCible.setDossierSubvention(dossierCible);
            demandeSubventionUnique = (this.demandeSubventionRepository.compterNombreDemandeSubventionParDossier(dossierOrigine.getId()) == 1);
            this.documentService.deplacerDirectoryDemandeSubvention(demandeCible, demandeOrigine, demandeSubventionUnique);

        }

        // on utilise un dossier existant il faut modifier le chorusNumLegacy de la
        // demande par rapport au dossier
        demandeOrigine.setChorusNumLigneLegacy(this.recupererChorusNumLigneLegacy(dossierCible.getId()));

        this.gererEtatEtFichierPourQualification(demandeCible, fichiersTransferts, demandeOrigine, dossierCible);

        demandeOrigine = this.demandeSubventionRepository.mettreAJourDemandeSubvention(demandeOrigine);

        if (demandeSubventionUnique) {
            this.dossierSubventionService.supprimerDossierSubventionParId(dossierOrigine.getId());
        }

        return demandeOrigine;
    }

    /**
     * @param demandeOrigine
     * @return
     */
    private DossierSubvention initialiserDossierPourDemandeComplementaireAvecTauxDifferent(DemandeSubvention demandeOrigine) {
        DossierSubvention dossierTemporaire = this.dossierSubventionService.initNouveauDossierSubvention();
        dossierTemporaire.setDotationCollectivite(this.dotationCollectiviteService.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(
                demandeOrigine.getDossierSubvention().getDotationCollectivite().getCollectivite().getId(), demandeOrigine.getDossierSubvention()
                        .getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme().getId()));
        dossierTemporaire = this.dossierSubventionService.creerDossierSubventionDepuisDemandeSubvention(dossierTemporaire);
        return dossierTemporaire;
    }

    /**
     * @param demandeCible
     * @param fichiersTransferts
     * @throws RegleGestionException
     */
    private static void verifierPresenceFicheSiret(DemandeSubvention demandeOrigine, List<FichierTransfert> fichiersTransferts) {
        // Récuperer la demande en base pour avoir les documents d'origine
        Set<Document> documents = demandeOrigine.getDocuments();

        // Affiche une erreur si fiche siret est absent.
        if (!isTypeDocumentPresent(fichiersTransferts, TypeDocumentEnum.FICHE_SIRET)
                && !isTypeDocumentPresent(documents, TypeDocumentEnum.FICHE_SIRET)) {
            throw new RegleGestionException("subvention.demande.fiche.siret.absent");
        }
    }

    /**
     * Valider numero dossier.
     *
     * @param demande
     *            the demande
     */
    private void validerNumeroDossier(DemandeSubvention demande) {

        String numDossier = demande.getDossierSubvention().getNumDossier();

        if (numDossier == null) {
            numDossier = "";
        }

        List<MessageProperty> listeErreurs = this.getErreursValidationBean(demande, Default.class);

        // Validation des différentes contraintes pour le nom du dossier.
        String patternString = "^([0-9]{4})([A-Z]{2,3})(([0-9]{2}[AB])|([0-9]{3}))([0-9]{1,10})(-[0-9]{1,3})?$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(numDossier);

        // REGEX : Premier groupe entre paranthèse doit représenter l'année.
        final int groupYear = 1;
        // REGEX : Deuxième groupe entre paranthèse doit représenter le code de
        // département.
        final int groupCodeDepartement = 3;
        // REGEX : Deuxième groupe entre paranthèse doit représenter le numéro de la
        // collectivité.
        final int groupNumCollectivite = 6;

        // Récupération de la dotation de collectivité du dossier de la demande de
        // subvention.
        DotationCollectivite dotationCollectivite = this.dotationCollectiviteService.rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(
                demande.getDossierSubvention().getDotationCollectivite().getCollectivite().getId(), demande.getDossierSubvention()
                        .getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getSousProgramme().getId());

        // Création du nombre pour le code département.
        String codeDepartement = dotationCollectivite.getDotationDepartement().getDepartement().getCode();
        if (codeDepartement.length() == 2) {
            codeDepartement = "0".concat(codeDepartement);
        }

        // Mise en place des paramètres pour le message de format.
        String[] params = new String[] { String.valueOf(this.timeOperationTransverse.getAnneeCourante()), codeDepartement,
                dotationCollectivite.getCollectivite().getNumCollectivite() };
        listeErreurs.add(new MessageProperty("subvention.demande.num.dossier.pattern", params));
        boolean erreurDetectee = false;
        // Vérification de la correspondance entre le schéma et le numéro du dossier.
        if (matcher.find()) {

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(matcher.group(groupYear));
                LOGGER.debug(String.valueOf(this.timeOperationTransverse.getAnneeCourante()));

                LOGGER.debug(matcher.group(groupCodeDepartement));
                LOGGER.debug(dotationCollectivite.getDotationDepartement().getDepartement().getCode());

                LOGGER.debug(matcher.group(groupNumCollectivite));
                LOGGER.debug(dotationCollectivite.getCollectivite().getNumCollectivite());
            }

            // Vérification de l'année en début de nom du dossier.
            if (!String.valueOf(this.timeOperationTransverse.getAnneeCourante()).equals(matcher.group(groupYear))) {
                listeErreurs.add(new MessageProperty("subvention.demande.num.dossier.annee.invalide"));
                erreurDetectee = true;
            }

            // Vérification du code du département
            if (!matcher.group(groupCodeDepartement).equals(codeDepartement)) {
                listeErreurs.add(new MessageProperty("subvention.demande.num.dossier.departement.incorrect"));
                erreurDetectee = true;
            }

            if (!matcher.group(groupNumCollectivite).equals(dotationCollectivite.getCollectivite().getNumCollectivite())) {
                listeErreurs.add(new MessageProperty("subvention.demande.num.dossier.collectivite.incorrect"));
                erreurDetectee = true;
            }
        } else {
            erreurDetectee = true;
        }
        if (erreurDetectee) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }

        Long idDossier = demande.getDossierSubvention().getId();

        // Le numéro de dossier doit être unique.
        if (!this.dossierSubventionService.isNumeroDossierUnique(idDossier, numDossier)) {
            throw new RegleGestionException("subvention.demande.num.dossier.non.unique");
        }
    }

    /**
     * Refuser demande subvention.
     *
     * @param demande
     *            the demande
     * @return the demande subvention
     */
    @Override
    public DemandeSubvention refuserDemandeSubvention(Long idDemande, String motifRefus) {

        // Vérifier la présence d'un motif de refus.
        if (StringUtils.isBlank(motifRefus)) {
            throw new RegleGestionException("subvention.demande.motif.refus.absent");
        }
        DemandeSubvention demande = this.demandeSubventionRepository.rechercheDemandeSubventionParId(idDemande);
        demande.setMotifRefus(motifRefus);
        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.REFUSEE);
        LocalDateTime dateAccord = this.demandeSubventionRepository.rechercheDemandeSubventionParId(demande.getId()).getDateAccord();

        SousProgramme sousProgramme = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme();
        sousProgramme = this.sousProgrammeService.rechercherParId(sousProgramme.getId());

        if (sousProgramme.isDeProjet() && (dateAccord != null)) {
            Long idLigneDotationDepartementale = this.ligneDotationDepartementService.rechercherLignesDotationDepartementale(dateAccord);
            this.ligneDotationDepartementService.detruireLigneDotationDepartementale(idLigneDotationDepartementale);

            DotationCollectivite dotationCollectivite = demande.getDossierSubvention().getDotationCollectivite();
            dotationCollectivite.setMontant(dotationCollectivite.getMontant().subtract(demande.getPlafondAide()));
            this.dotationCollectiviteService.majDotationCollectivite(dotationCollectivite);
        }

        Cadencement cd = this.cadencementService.findCadencementById(demande.getId());
        cd.setDemandeSubvention(demande);
        demande.setCadencement(cd);

        // envoi mail refus
        this.emailRefusSubventionPaiementService.emailRefusDemandeSubventionPaiement(demande.getDossierSubvention(), "subvention",
                demande.getDateDemande(), demande.getPlafondAide(), demande.getMotifRefus());

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#
     * signalerAnomalieDemandeSubvention(fr.gouv.sitde.face.transverse.entities .DemandeSubvention)
     */
    @Override
    public DemandeSubvention signalerAnomalieDemandeSubvention(Long idDemandeSubvention) {

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (!this.anomalieService.contientAnomaliesEtACorrigerParIdDemandeSubvention(idDemandeSubvention)) {
            throw new RegleGestionException("subvention.demande.anomalies.a.corriger");
        }
        this.anomalieService.rendreAnomaliesDemandeSubventionVisiblesPourAODE(idDemandeSubvention);

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(idDemandeSubvention);
        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE);
        demande = this.demandeSubventionRepository.enregistrer(demande);

        return demande;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#
     * detecterAnomalieDemandeSubvention(fr.gouv.sitde.face.transverse.entities .DemandeSubvention)
     */
    @Override
    public DemandeSubvention detecterAnomalieDemandeSubvention(DemandeSubvention demande) {

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (!this.anomalieService.contientAnomaliesEtACorrigerParIdDemandeSubvention(demande.getId())) {
            throw new RegleGestionException("subvention.demande.anomalies.a.corriger");
        }

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.ANOMALIE_DETECTEE);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /**
     * Definir etat demande subvention.
     *
     * @param demande
     *            the demande
     * @param etatDemandeSubvention
     *            the etat demande subvention
     */
    private void definirEtatDemandeSubvention(DemandeSubvention demande, EtatDemandeSubventionEnum etatDemandeSubvention) {
        EtatDemandeSubvention etat = this.etatDemandeSubventionService.rechercherEtatDemandeSubventionParCode(etatDemandeSubvention.name());
        demande.setEtatDemandeSubvention(etat);
        demande.setDateEtat(this.timeOperationTransverse.now());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# corrigerDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention, java.util.List, java.util.List)
     */
    @Override
    public DemandeSubvention enregistrerPourCorrectionDemandeSubvention(DemandeSubvention demande, List<FichierTransfert> fichiersTransferts,
            List<Long> idsDocsComplSuppr) {

        Integer idSousProgramme = demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement().getDotationSousProgramme()
                .getSousProgramme().getId();

        // Valide et lève une exception si les RG ne sont pas respectés.
        this.validationReglesGestionDemandeSubvention(demande, fichiersTransferts, idSousProgramme);

        demande = this.demandeSubventionRepository.enregistrer(demande);

        // gestion des fichiers à enregistrer
        this.gestionFichiersEnregistrer(fichiersTransferts, demande);

        // gestion des fichiers à supprimer
        this.gestionFichiersComplementairesSupprimer(idsDocsComplSuppr, demande);

        return demande;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# attribuerDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention attribuerDemandeSubvention(Long idDemandeSubvention, List<FichierTransfert> fichiersTransferts) {

        // Affiche une erreur si la décision attributive est absente.
        if (!isTypeDocumentPresent(fichiersTransferts, TypeDocumentEnum.DECISION_ATTRIBUTIVE_SUBV)) {
            throw new RegleGestionException("subvention.demande.ajouter.decision.attributive");
        }

        DemandeSubvention demande = this.rechercherDemandeSubventionParId(idDemandeSubvention);

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.ATTRIBUEE);

        Map<String, Object> variablesContexte = new HashMap<>(100);
        variablesContexte.put("dateDemande", demande.getDateDemande().format(formatter));
        variablesContexte.put("montantDemande", demande.getPlafondAide());
        variablesContexte.put("sousProgrammeDemande", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getDescription());

        this.emailService.creerEtEnvoyerEmail(demande.getDossierSubvention(), TypeEmailEnum.DOS_ATTRIBUTION_SUBVENTION,
                TemplateEmailEnum.EMAIL_DOS_ATTRIBUTION_SUBVENTION, variablesContexte);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherParEJetPoste(java.lang.String, java.lang.String)
     */
    @Override
    public DemandeSubvention rechercherParEJetPoste(String numeroEJ, String numeroPoste) {
        return this.demandeSubventionRepository.rechercherParEJetPoste(numeroEJ, numeroPoste);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherParEJQuantityEtat(java.lang.String,
     * java.math.BigDecimal)
     */
    @Override
    public DemandeSubvention rechercherParEJQuantityEtat(String numeroEJ, BigDecimal plafond) {
        return this.demandeSubventionRepository.rechercherParEJQuantityEtat(numeroEJ, plafond);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# obtenirDtoPourBatchFen0111A()
     */
    @Override
    public List<DemandeSubvention> obtenirDtoPourBatchFen0111A() {
        return this.demandeSubventionRepository.obtenirDtoPourBatchFen0111A();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService#
     * recupererDocumentParDemandeEtType(fr.gouv.sitde.face.transverse.entities .DemandeSubvention,
     * fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum)
     */
    @Override
    public Document recupererDocumentParDemandeEtType(DemandeSubvention demande, TypeDocumentEnum typeDocument) {
        for (Document doc : demande.getDocuments()) {
            if (doc.getTypeDocument().getIdCodifie().equals(typeDocument.getIdCodifie())) {
                return doc;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rechercherDemandesSubventionOrdonneeParDateDemande(java.lang.Long)
     */
    @Override
    public List<DemandeSubvention> rechercherDemandesSubventionOrdonneeParDateDemande(Long idDossier) {
        return this.demandeSubventionRepository.findByDossierSubventionOrdonneeParDateDemande(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# corrigerDemandeSubvention(java.lang.Long)
     */
    @Override
    public DemandeSubvention corrigerDemandeSubvention(Long id) {
        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.CORRIGEE);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService# rejeterPourTauxDemandeSubvention(java.lang.Long, java.lang.Long)
     */
    @Override
    public DemandeSubvention rejeterPourTauxDemandeSubvention(Long id, BigDecimal tauxDemande) {
        DemandeSubvention demande = this.rechercherDemandeSubventionParId(id);

        if (demande.getTauxAide().compareTo(tauxDemande) == 0) {
            throw new RegleGestionException("subvention.demande.taux.rejet.differents");
        }

        demande.setMotifRefus(
                MessageUtils.getMsgValidationMetier("subvention.demande.motif.refus.rejet.taux", new String[] { tauxDemande.toString() }));

        this.definirEtatDemandeSubvention(demande, EtatDemandeSubventionEnum.REJET_TAUX);

        Map<String, Object> variablesContexte = new HashMap<>(100);
        variablesContexte.put("dateDemande", demande.getDateDemande().format(formatter));
        variablesContexte.put("montantDemande", demande.getPlafondAide());
        variablesContexte.put("sousProgrammeDemande", demande.getDossierSubvention().getDotationCollectivite().getDotationDepartement()
                .getDotationSousProgramme().getSousProgramme().getDescription());
        variablesContexte.put("tauxDemande", tauxDemande);
        variablesContexte.put("numeroDossier", demande.getDossierSubvention().getNumDossier());
        variablesContexte.put("tauxPrecedent", demande.getTauxAide());

        this.emailService.creerEtEnvoyerEmail(demande.getDossierSubvention(), TypeEmailEnum.SUB_REJET_TAUX, TemplateEmailEnum.EMAIL_SUB_REJET_TAUX,
                variablesContexte);

        return this.demandeSubventionRepository.enregistrer(demande);
    }

}

package fr.gouv.sitde.face.domaine.service.email.impl;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.email.MailSenderService;
import fr.gouv.sitde.face.domain.spi.repositories.EmailRepository;
import fr.gouv.sitde.face.domaine.service.email.EmailService;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.Email;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * The Class EmailServiceImpl.
 */
@Named
public class EmailServiceImpl implements EmailService {

    /** The email repository. */
    @Inject
    private EmailRepository emailRepository;

    /** The mail sender service. */
    @Inject
    private MailSenderService mailSenderService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.email.EmailService#creerEtEnvoyerEmail(fr.gouv.sitde.face.transverse.entities.Collectivite,
     * fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum, java.util.Map)
     */
    @Override
    public void creerEtEnvoyerEmail(Collectivite collectivite, TypeEmailEnum typeEmail, TemplateEmailEnum templateEmail,
            Map<String, Object> variablesContexte) {

        if (collectivite.getAdressesEmail().isEmpty()) {
            throw new TechniqueException("Envoi impossible : La collectivité n'a aucune adresse email");
        }

        Set<String> adressesDestinataires = collectivite.getAdressesEmail().stream().map(AdresseEmail::getAdresse).collect(Collectors.toSet());
        Email email = this.mailSenderService.construireEmail(adressesDestinataires, typeEmail, templateEmail, variablesContexte);
        email.setCollectivite(collectivite);
        this.mailSenderService.envoyerMail(email);
        this.emailRepository.creerEmail(email);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.email.EmailService#creerEtEnvoyerEmail(fr.gouv.sitde.face.transverse.entities.DossierSubvention,
     * fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum, java.util.Map)
     */
    @Override
    public void creerEtEnvoyerEmail(DossierSubvention dossierSubvention, TypeEmailEnum typeEmail, TemplateEmailEnum templateEmail,
            Map<String, Object> variablesContexte) {

        Collectivite collectivite = dossierSubvention.getDotationCollectivite().getCollectivite();
        if (collectivite.getAdressesEmail().isEmpty()) {
            throw new TechniqueException("Envoi impossible : La collectivité n'a aucune adresse email");
        }

        Set<String> adressesDestinataires = collectivite.getAdressesEmail().stream().map(AdresseEmail::getAdresse).collect(Collectors.toSet());
        Email email = this.mailSenderService.construireEmail(adressesDestinataires, typeEmail, templateEmail, variablesContexte);
        email.setCollectivite(collectivite);
        email.setDossierSubvention(dossierSubvention);
        this.mailSenderService.envoyerMail(email);
        this.emailRepository.creerEmail(email);
    }
}

package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domaine.service.administration.CollectiviteService;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationCollectiviteService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationRenoncementService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.domaine.service.dotation.LigneDotationDepartementService;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class DotationRenoncementServiceImpl.
 */
@Named
public class DotationRenoncementServiceImpl implements DotationRenoncementService {

    /** The dotation collectivite service. */
    @Inject
    private DotationCollectiviteService dotationCollectiviteService;

    @Inject
    private CollectiviteService collectiviteService;

    /** The dotation collectivite service. */
    @Inject
    private LigneDotationDepartementService ligneDotationDepartementService;

    /** The dotation validateurs service. */
    @Inject
    private DotationValidateursService dotationValidateursService;

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationRenoncementService#renoncerDotation(java.lang.Long, java.lang.Integer,
     * java.math.BigDecimal, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public void renoncerDotation(Long idCollectivite, Integer idSousProgramme, BigDecimal montant, FichierTransfert fichierEnvoyer) {

        // Validation du formulaire
        validationDonneesRenoncerDotation(montant, fichierEnvoyer);

        DotationCollectivite dotationCollectivite = this.dotationCollectiviteService
                .rechercherDotationCollectiviteParIdCollectiviteEtIdSousProgramme(idCollectivite, idSousProgramme);

        List<MessageProperty> listeErreurs = new ArrayList<>();
        // Validation
        this.dotationValidateursService.validerCollectiviteAvecSubventions(dotationCollectivite, dotationCollectivite.getMontant().subtract(montant),
                listeErreurs);

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }

        // Répercuter le renoncement au niveau du département
        LigneDotationDepartement ligneDotationDepartement = new LigneDotationDepartement();
        ligneDotationDepartement.setMontant(new BigDecimal(montant.negate().intValue()));
        ligneDotationDepartement.setDateEnvoi(this.timeOperationTransverse.now());
        ligneDotationDepartement.setDotationDepartement(dotationCollectivite.getDotationDepartement());

        Collectivite collectivite = this.collectiviteService.rechercherCollectiviteParId(idCollectivite);
        ligneDotationDepartement.setCollectivite(collectivite);
        ligneDotationDepartement = this.ligneDotationDepartementService.ajouterLigneDotationDepartement(ligneDotationDepartement,
                TypeDotationDepartementEnum.RENONCEMENT);

        // Mise à jour du renoncement au niveau de la collectivité
        dotationCollectivite.setMontant(dotationCollectivite.getMontant().subtract(montant));
        dotationCollectivite.getDotationDepartement().getDotationSousProgramme()
                .setMontant(dotationCollectivite.getDotationDepartement().getDotationSousProgramme().getMontant().subtract(montant));
        this.dotationCollectiviteService.majDotationCollectivite(dotationCollectivite);

        // Enregistrement en base et téléversement du document
        if (fichierEnvoyer != null) {
            this.documentService.creerDocumentLigneDotationDepartement(ligneDotationDepartement, fichierEnvoyer);
        }
    }

    /**
     * Validation donnees renoncer dotation.
     *
     * @param montant
     *            the montant
     * @param fichierEnvoyer
     *            the fichier envoyer
     */
    private static void validationDonneesRenoncerDotation(BigDecimal montant, FichierTransfert fichierEnvoyer) {

        List<MessageProperty> listeErreurs = new ArrayList<>();

        if ((montant == null) || (montant.intValue() <= 0)) {
            listeErreurs.add(new MessageProperty("dotation.renoncement.montant.positif"));
        }

        if (fichierEnvoyer == null) {
            listeErreurs.add(new MessageProperty("dotation.renoncement.justificatif.signe.obligatoire"));
        }

        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }
    }
}

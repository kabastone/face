package fr.gouv.sitde.face.domaine.service.dotation;

import java.math.BigDecimal;

import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;

/**
 * The Interface DotationRenoncementService.
 */
public interface DotationRenoncementService {

    /**
     * Renoncer dotation.
     *
     * @param idCollectivite
     *            the id collectivite
     * @param idSousProgramme
     *            the id sous programme
     * @param montant
     *            the montant
     * @param fichierEnvoyer
     *            the fichier envoyer
     */
    void renoncerDotation(Long idCollectivite, Integer idSousProgramme, BigDecimal montant, FichierTransfert fichierEnvoyer);
}

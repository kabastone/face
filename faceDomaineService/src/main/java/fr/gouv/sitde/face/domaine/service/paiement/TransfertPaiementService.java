/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * The Interface TransfertPaiementService.
 */
public interface TransfertPaiementService {

    /**
     * Recherche transfert by dossier subvention.
     *
     * @param idDossierSubvention
     *            the id dossier subvention
     * @return the list
     */
    List<TransfertPaiement> rechercheTransfertByDossierSubvention(Long idDossierSubvention);

    /**
     * Formatage chorus montant.
     *
     * @param montant
     *            the montant
     * @return the string
     */
    String formatageChorusMontant(BigDecimal montant);

    /**
     * Save transfert paiement.
     *
     * @param transfertPaiement
     *            the transfert paiement
     * @return the transfert paiement
     */
    TransfertPaiement saveTransfertPaiement(TransfertPaiement transfertPaiement);

    /**
     * Rechercher par num S fet legacyet num EJ.
     *
     * @param chorusNumSF
     *            the chorus num SF
     * @param chorusNumLigneLegacy
     *            the chorus num ligne legacy
     * @param chorusNumEJ
     *            the chorus num EJ
     * @return the list
     */
    List<TransfertPaiement> rechercherParNumSFetLegacyetNumEJ(String chorusNumSF, String chorusNumLigneLegacy, String chorusNumEJ);
}

package fr.gouv.sitde.face.domaine.service.email;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import fr.gouv.sitde.face.transverse.entities.DossierSubvention;

public interface EmailRefusSubventionPaiementService {

    void emailRefusDemandeSubventionPaiement(DossierSubvention dossier, String type, LocalDateTime date, BigDecimal montant, String motifRefus);

}

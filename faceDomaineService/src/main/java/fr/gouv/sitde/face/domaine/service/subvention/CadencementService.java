/**
 *
 */
package fr.gouv.sitde.face.domaine.service.subvention;

import java.math.BigDecimal;
import java.util.List;

import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;

/**
 * @author A754839
 *
 */
public interface CadencementService {

    /**
     * @param idCollectivite
     * @param deProjet
     * @return
     */
    List<Cadencement> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet);

    /**
     * @param cadencement
     * @return
     */
    void validerMontantCadencement(Cadencement cadencement, BigDecimal aideDemendee);

    /**
     * Rechercher cadencements pour maj.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<Cadencement> rechercherCadencementsPourMaj(Long idCollectivite);

    /**
     * @param cadencement
     */
    Cadencement enregistrerCadencement(Cadencement cadencement);

    /**
     * Find cadencement by id.
     *
     * @param id
     *            the id
     * @return the cadencement
     */
    Cadencement findCadencementById(Long id);

    /**
     * Modifier cadencement.
     *
     * @param cadencement
     *            the cadencement
     * @return the cadencement
     */
    Cadencement modifierCadencement(Cadencement cadencement);

    /**
     * @param idCollectivite
     * @return
     */
    Boolean estToutCadencementValidePourCollectivite(Long idCollectivite);

    /**
     * @param criteresRecherche
     * @param string
     * @return
     */
    List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche, String string);

    /**
     * @param cd
     * @param aideDemandee
     * @return
     */
    Boolean isMontantCadencementValide(Cadencement cd, BigDecimal aideDemandee);

}

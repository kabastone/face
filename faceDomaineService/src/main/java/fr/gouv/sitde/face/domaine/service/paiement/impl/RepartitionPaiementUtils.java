/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import fr.gouv.sitde.face.domaine.service.paiement.dto.MontantsRepartitionDto;
import fr.gouv.sitde.face.domaine.service.paiement.dto.RepartitionPaiementDto;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.util.ConstantesFace;

/**
 * @author A754839
 *
 */
public final class RepartitionPaiementUtils {

    private RepartitionPaiementUtils() {
    }

    /**
     * @param donneesRepartition
     * @param montants
     */
    public static void conserverMontantsCalculesDansDto(RepartitionPaiementDto donneesRepartition, MontantsRepartitionDto montants) {
        donneesRepartition.getMontantTransfertsAvanceRestant().add(montants.getTempAvance());
        donneesRepartition.getMontantTransfertsAcompteRestant().add(montants.getTempAcompte());
        donneesRepartition.getMontantTransfertsSoldeRestant().add(montants.getTempSolde());
    }

    /**
     * @param montants
     * @param regle
     * @param dsu
     */
    public static MontantsRepartitionDto initialiserMontantsPourRepartition(Reglementaire regle, DemandeSubvention dsu) {

        MontantsRepartitionDto montants = new MontantsRepartitionDto();

        // On conserve les montants totaux de chaque subvention pour calculer les proportions réservées.
        montants.setTempTotal(dsu.getPlafondAide());

        // Recuperation du taux d'aide en base, pourcentage du plafond qui est alloué en priorité aux avances et aux soldes.
        BigDecimal tauxAvance = new BigDecimal(regle.getTauxAidePaiementAvance()).divide(new BigDecimal("100"));
        montants.setTempAvance(montants.getTempTotal().multiply(tauxAvance).setScale(2, RoundingMode.HALF_UP));

        // Recuperation du taux d'aide en base, pourcentage du plafond est alloué aux acomptes.
        BigDecimal tauxAcompte = new BigDecimal(regle.getTauxAidePaiementAccompte()).divide(new BigDecimal("100"));
        montants.setTempAcompte(montants.getTempTotal().multiply(tauxAcompte).subtract(montants.getTempAvance()).setScale(2, RoundingMode.HALF_UP));
        montants.setTempSolde(
                montants.getTempTotal().subtract(montants.getTempAvance()).subtract(montants.getTempAcompte()).setScale(2, RoundingMode.HALF_UP));

        return montants;
    }

    /**
     * Gets the nombre ligne.
     *
     * @param nombreLigne
     *            the nombre ligne
     * @return the nombre ligne
     */
    public static String getNombreLigne(int nombreLigne) {
        StringBuilder builder = new StringBuilder();
        builder.append(nombreLigne);
        while (builder.length() < ConstantesFace.MAX_CHIFFRES_CHORUS_LIGNE) {
            builder.insert(0, "0");
        }
        return builder.toString();
    }

    /**
     * Repartir montant dans transferts.
     *
     * @param montantTransfertsRestant
     *            the montant transferts restant
     * @param demandePaiement
     *            the demande paiement
     * @param listeDemandeSubvention
     *            the liste demande subvention
     * @param listeTransfertsGeneres
     *            the liste transferts generes
     * @param aideDemandee
     *            the aide demandee
     * @return the big decimal
     */
    public static void repartirMontantDansTransferts(RepartitionPaiementDto donneesRepartition, TypeDemandePaiementEnum typeDemandePaiement) {

        if (donneesRepartition.getAideDemandee().compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        int nbSubventions = donneesRepartition.getListeDemandeSubvention().size();
        int compteur = 0;
        List<BigDecimal> montantTransfertsRestant = RepartitionPaiementUtils.recupererListePourRepartition(donneesRepartition, typeDemandePaiement);
        do {
            BigDecimal subvention = montantTransfertsRestant.get(compteur);
            DemandeSubvention demandeSubvention = donneesRepartition.getListeDemandeSubvention().get(compteur);

            if ((subvention.compareTo(BigDecimal.ZERO) <= 0)) {
                compteur++;
                continue;
            }

            // Si le montant de l'aide demandéee est inférieur à la subvention, il est consommé entièrement ici.
            if (donneesRepartition.getAideDemandee().compareTo(subvention) <= 0) {

                // S'il n'y a pas de transferts pour cette subvention on en crée un, sinon on le mets à jour.
                if (((donneesRepartition.getListeTransfertsGeneres().get(compteur).getMontant() == null)
                        || (donneesRepartition.getListeTransfertsGeneres().get(compteur).getMontant().compareTo(BigDecimal.ZERO) == 0))) {
                    donneesRepartition.getListeTransfertsGeneres().set(compteur, RepartitionPaiementUtils.creerTransfertPaiement(
                            donneesRepartition.getDemandePaiement(), demandeSubvention, donneesRepartition.getAideDemandee()));
                } else {

                    donneesRepartition.getListeTransfertsGeneres().get(compteur).setMontant(donneesRepartition.getListeTransfertsGeneres()
                            .get(compteur).getMontant().add(donneesRepartition.getAideDemandee()).setScale(2, RoundingMode.HALF_UP));
                }
                subvention = subvention.subtract(donneesRepartition.getAideDemandee()).setScale(2, RoundingMode.HALF_UP);
                donneesRepartition.setAideDemandee(BigDecimal.ZERO);

                // Sinon, on y soustrait la subvention, et on passe à la subvention suivante.
            } else {

                // S'il n'y a pas de transferts pour cette subvention on en crée un, sinon on le mets à jour.
                if (((donneesRepartition.getListeTransfertsGeneres().get(compteur).getMontant() == null)
                        || (donneesRepartition.getListeTransfertsGeneres().get(compteur).getMontant().compareTo(BigDecimal.ZERO) == 0))) {
                    donneesRepartition.getListeTransfertsGeneres().set(compteur,
                            RepartitionPaiementUtils.creerTransfertPaiement(donneesRepartition.getDemandePaiement(), demandeSubvention, subvention));
                } else {

                    donneesRepartition.getListeTransfertsGeneres().get(compteur).setMontant(donneesRepartition.getListeTransfertsGeneres()
                            .get(compteur).getMontant().add(subvention).setScale(2, RoundingMode.HALF_UP));
                }
                donneesRepartition.setAideDemandee(donneesRepartition.getAideDemandee().subtract(subvention).setScale(2, RoundingMode.HALF_UP));
                subvention = BigDecimal.ZERO;

            }

            // Mise à jour du montant acompte restant.
            montantTransfertsRestant.set(compteur, subvention);

            compteur++;
        } while ((donneesRepartition.getAideDemandee().compareTo(BigDecimal.ZERO) > 0) && (compteur != nbSubventions));
    }

    /**
     * @param donneesRepartition
     * @param typeDemandePaiement
     * @return
     */
    private static List<BigDecimal> recupererListePourRepartition(RepartitionPaiementDto donneesRepartition,
            TypeDemandePaiementEnum typeDemandePaiement) {
        switch (typeDemandePaiement) {
            case ACOMPTE:
                return donneesRepartition.getMontantTransfertsAcompteRestant();
            case AVANCE:
                return donneesRepartition.getMontantTransfertsAvanceRestant();
            case SOLDE:
                return donneesRepartition.getMontantTransfertsSoldeRestant();
            default:
                throw new TechniqueException("Aucun type de demande de paiement fourni");
        }
    }

    /**
     * Creer transfert paiement.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param demandeSubvention
     *            the demande subvention
     * @param montant
     *            the montant
     * @return the transfert paiement
     */
    private static TransfertPaiement creerTransfertPaiement(DemandePaiement demandePaiement, DemandeSubvention demandeSubvention,
            BigDecimal montant) {
        TransfertPaiement trp = new TransfertPaiement();
        trp.setDemandePaiement(demandePaiement);
        trp.setDemandeSubvention(demandeSubvention);
        trp.setMontant(montant);
        return trp;

    }

}

package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.DepartementService;
import fr.gouv.sitde.face.transverse.entities.Departement;

/**
 * The Class DepartementServiceImpl.
 */
@Named
public class DepartementServiceImpl implements DepartementService {

    /** The departement repository. */
    @Inject
    private DepartementRepository departementRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.DepartementService#rechercherDepartements()
     */
    @Override
    public List<Departement> rechercherDepartements() {

        return this.departementRepository.rechercherDepartements();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.DepartementService#rechercherDepartementParId(java.lang.Integer)
     */
    @Override
    public Departement rechercherDepartementParId(Integer idDepartement) {
        return this.departementRepository.rechercherDepartementById(idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.DepartementService#majDepartement(fr.gouv.sitde.face.transverse.entities.Departement)
     */
    @Override
    public Departement majDepartement(Departement departement) {
        return this.departementRepository.majDepartement(departement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.referentiel.DepartementService#enregistrerDonneesFiscalesDepartement(fr.gouv.sitde.face.transverse.entities.
     * Departement)
     */
    @Override
    public Departement enregistrerDonneesFiscalesDepartement(Departement departement) {
        Departement departementInterne = this.rechercherDepartementParId(departement.getId());
        departementInterne.setTpgNomCodique(departement.getTpgNomCodique());
        departementInterne.setTpgNumCodique(departement.getTpgNumCodique());
        return this.majDepartement(departementInterne);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.DepartementService#rechercherDepartementParCode(java.lang.String)
     */
    @Override
    public Departement rechercherDepartementParCode(String code) {
        return this.departementRepository.rechercherDepartementParCode(code);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.TransfertPaiementRepository;
import fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.DirecteurRepartitionPaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.dto.MontantsRepartitionDto;
import fr.gouv.sitde.face.domaine.service.paiement.dto.RepartitionPaiementDto;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * @author A754839
 *
 */
@Named
public class DirecteurRepartitionPaiementServiceImpl implements DirecteurRepartitionPaiementService {

    /** The reglementaire service. */
    @Inject
    private ReglementaireService reglementaireService;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande paiement service. */
    @Inject
    private DemandePaiementService demandePaiementService;

    /** The transfert paiement repository. */
    @Inject
    private TransfertPaiementRepository transfertPaiementRepository;

    @Inject
    private CalculateurRepartitionPaiementService calculateurRepartitionPaiementService;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.paiement.DirecteurRepartitionPaiementService#realiserRepartition(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement)
     */
    @Override
    public void realiserRepartition(Long idDemandePaiement) {
        DemandePaiement demandePaiement = this.demandePaiementService.rechercherDemandePaiement(idDemandePaiement);
        RepartitionPaiementDto donneesRepartition = this.initialiserDonneesRepartition(demandePaiement);
        // TODO: Ajouter le choix de calculateur ici, et le passer en argument. Injecter les deux versions. Si plus de 3, ajouter une factory.
        this.repartirAnciensTransfertPaiement(donneesRepartition);
        this.genererNouveauxTransferts(donneesRepartition);
        this.enregistrerNouveauxTransferts(donneesRepartition);
    }

    /**
     * Initialiser le DTO de repartition paiement contenant l'entierete des valeurs necessaires aux calculs.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the repartition paiement dto
     */
    private RepartitionPaiementDto initialiserDonneesRepartition(DemandePaiement demandePaiement) {
        RepartitionPaiementDto donneesRepartition = new RepartitionPaiementDto();

        List<DemandeSubvention> listeDemandeSubvention = this.demandeSubventionService
                .rechercherDemandesSubventionOrdonneeParDateDemande(demandePaiement.getDossierSubvention().getId());
        int nbSubventions = listeDemandeSubvention.size();

        donneesRepartition.setAideDemandee(demandePaiement.getAideDemandee());
        donneesRepartition.setDemandePaiement(demandePaiement);
        donneesRepartition.setListeDemandeSubvention(listeDemandeSubvention);
        donneesRepartition.setMontantTransfertsAcompteRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setMontantTransfertsAvanceRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setMontantTransfertsSoldeRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setListeTransfertPaiement(
                this.transfertPaiementRepository.findTransfertPaiementByDossierSubvention(demandePaiement.getDossierSubvention().getId()));

        List<TransfertPaiement> listeTransferts = new ArrayList<>(nbSubventions);
        for (int i = 0; i < nbSubventions; i++) {
            listeTransferts.add(new TransfertPaiement());
        }
        donneesRepartition.setListeTransfertsGeneres(listeTransferts);

        return donneesRepartition;
    }

    /**
     * Initialiser le DTO de repartition paiement contenant l'entierete des valeurs necessaires aux calculs.
     *
     * @param demandePaiement
     *            the demande paiement
     * @return the repartition paiement dto
     */
    private RepartitionPaiementDto initialiserDonneesRepartition(Long idDossier) {
        RepartitionPaiementDto donneesRepartition = new RepartitionPaiementDto();

        List<DemandeSubvention> listeDemandeSubvention = this.demandeSubventionService.rechercherDemandesSubventionOrdonneeParDateDemande(idDossier);
        int nbSubventions = listeDemandeSubvention.size();

        donneesRepartition.setListeDemandeSubvention(listeDemandeSubvention);
        donneesRepartition.setMontantTransfertsAcompteRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setMontantTransfertsAvanceRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setMontantTransfertsSoldeRestant(new ArrayList<>(nbSubventions));
        donneesRepartition.setListeTransfertPaiement(this.transfertPaiementRepository.findTransfertPaiementByDossierSubvention(idDossier));

        List<TransfertPaiement> listeTransferts = new ArrayList<>(nbSubventions);
        for (int i = 0; i < nbSubventions; i++) {
            listeTransferts.add(new TransfertPaiement());
        }
        donneesRepartition.setListeTransfertsGeneres(listeTransferts);

        return donneesRepartition;
    }

    /**
     * Repartir les anciens transfert paiement selon l'algorithme employe pour leur generation.
     *
     * @param donneesRepartition
     *            the donnees repartition
     */
    private void repartirAnciensTransfertPaiement(RepartitionPaiementDto donneesRepartition) {

        Reglementaire regle = this.reglementaireService.rechercherReglementaire();

        // Boucle sur les demandes de subventions afin de peupler les différentes listes.
        for (DemandeSubvention dsu : donneesRepartition.getListeDemandeSubvention()) {

            MontantsRepartitionDto montantsDto = RepartitionPaiementUtils.initialiserMontantsPourRepartition(regle, dsu);

            // Répartition des transferts passés selon leur type.
            for (TransfertPaiement trp : donneesRepartition.getListeTransfertPaiement()) {
                if (trp.getDemandeSubvention().getId().equals(dsu.getId())) {

                    // Si un transfert correspond à cette demande de subvention, le montant est enlevé du montant restant total.
                    montantsDto.setTempTotal(montantsDto.getTempTotal().subtract(trp.getMontant()).setScale(2, RoundingMode.HALF_UP));
                    montantsDto.setMontantTransfert(trp.getMontant());

                    // Si le transfert matche la demande de subvention on effectue une action selon le type de paiement.
                    switch (trp.getDemandePaiement().getTypeDemandePaiement()) {
                        case ACOMPTE:
                            this.calculateurRepartitionPaiementService.repartirAncienAcompte(montantsDto);
                            break;
                        case AVANCE:
                            this.calculateurRepartitionPaiementService.repartirAncienneAvance(montantsDto);
                            break;
                        case SOLDE:
                            this.calculateurRepartitionPaiementService.repartirAncienSolde(montantsDto);
                            break;
                    }
                }
            }

            RepartitionPaiementUtils.conserverMontantsCalculesDansDto(donneesRepartition, montantsDto);
        }
    }

    /**
     * Generer les nouveaux transferts en fonction du type de la demande de paiement.
     *
     * @param donneesRepartition
     *
     */
    private void genererNouveauxTransferts(RepartitionPaiementDto donneesRepartition) {
        switch (donneesRepartition.getDemandePaiement().getTypeDemandePaiement()) {
            case ACOMPTE:
                this.calculateurRepartitionPaiementService.repartirNouveauAcompte(donneesRepartition);
                return;

            case AVANCE:
                this.calculateurRepartitionPaiementService.repartirNouvelleAvance(donneesRepartition);
                return;
            case SOLDE:
                this.calculateurRepartitionPaiementService.repartirNouveauSolde(donneesRepartition);
                break;
        }
    }

    /**
     * @param donneesRepartition
     */
    private void enregistrerNouveauxTransferts(RepartitionPaiementDto donneesRepartition) {
        // Sauvegarde des transferts nécessaires.
        int nombreLigne = 1;
        for (TransfertPaiement trp : donneesRepartition.getListeTransfertsGeneres()) {
            if ((trp.getMontant() != null) && (trp.getMontant().compareTo(BigDecimal.ZERO) > 0)) {
                trp.setChorusNumLigneLegacy(RepartitionPaiementUtils.getNombreLigne(nombreLigne));
                this.transfertPaiementRepository.saveTransfertPaiement(trp);
                nombreLigne++;
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.RepartitionPaiementService#calculerResteAvancePaiementPourDossier(java.lang.Long)
     */
    @Override
    public BigDecimal calculerResteAvancePaiementPourDossier(Long idDossier) {

        RepartitionPaiementDto donneesRepartition = this.initialiserDonneesRepartition(idDossier);

        // Si l'une des demandes de paiement est un solde, plus d'avance disponible.
        for (TransfertPaiement trp : donneesRepartition.getListeTransfertPaiement()) {
            if (trp.getDemandePaiement().getTypeDemandePaiement().equals(TypeDemandePaiementEnum.SOLDE)) {
                return BigDecimal.ZERO;
            }
        }

        // Répartition des paiements sur les demandes de subvention.
        this.repartirAnciensTransfertPaiement(donneesRepartition);

        // Somme de tout les montants d'avance restants.
        BigDecimal resteAvance = BigDecimal.ZERO;
        for (BigDecimal montant : donneesRepartition.getMontantTransfertsAvanceRestant()) {
            resteAvance = resteAvance.add(montant);
        }

        return resteAvance.setScale(2);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.RepartitionPaiementService#calculerResteAcomptePaiementPourDossier(java.lang.Long)
     */
    @Override
    public BigDecimal calculerResteAcomptePaiementPourDossier(Long idDossier) {

        RepartitionPaiementDto donneesRepartition = this.initialiserDonneesRepartition(idDossier);

        // Si l'une des demandes de paiement est un solde, plus d'avance disponible.
        for (TransfertPaiement trp : donneesRepartition.getListeTransfertPaiement()) {
            if (trp.getDemandePaiement().getTypeDemandePaiement().equals(TypeDemandePaiementEnum.SOLDE)) {
                return BigDecimal.ZERO;
            }
        }

        // Répartition des paiements sur les demandes de subvention.
        this.repartirAnciensTransfertPaiement(donneesRepartition);

        // Somme de tout les montants d'avance restants.
        BigDecimal resteAcompte = BigDecimal.ZERO;
        for (BigDecimal montant : donneesRepartition.getMontantTransfertsAvanceRestant()) {
            resteAcompte = resteAcompte.add(montant);
        }
        for (BigDecimal montant : donneesRepartition.getMontantTransfertsAcompteRestant()) {
            resteAcompte = resteAcompte.add(montant);
        }

        return resteAcompte.setScale(2);
    }
}

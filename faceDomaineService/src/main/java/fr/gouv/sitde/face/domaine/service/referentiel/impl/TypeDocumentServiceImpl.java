/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.TypeDocumentRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.TypeDocumentService;
import fr.gouv.sitde.face.transverse.entities.TypeDocument;

/**
 * The Class TypeDocumentServiceImpl.
 *
 * @author a453029
 */
@Named
public class TypeDocumentServiceImpl implements TypeDocumentService {

    /** The type document repository. */
    @Inject
    private TypeDocumentRepository typeDocumentRepository;

    /**
     * Rechercher type document par id codifie.
     *
     * @param idCodifie
     *            the id codifie
     * @return the type document
     */
    @Override
    public TypeDocument rechercherTypeDocumentParIdCodifie(Integer idCodifie) {
        return this.typeDocumentRepository.rechercherTypeDocumentParIdCodifie(idCodifie);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.email;

import fr.gouv.sitde.face.transverse.email.EmailContactDto;

/**
 * The Interface EmailContactService.
 */
public interface EmailContactService {

    /**
     * Envoyer email contact.
     *
     * @param emailContactDto
     *            the email contact dto
     * @return the boolean
     */
    Boolean envoyerEmailContact(EmailContactDto emailContactDto);

}

package fr.gouv.sitde.face.domaine.service.document.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.document.FichierService;
import fr.gouv.sitde.face.domain.spi.document.GenerationDocService;
import fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.referentiel.TypeDocumentService;
import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class DocumentServiceImpl.
 */
@Named
public class DocumentServiceImpl implements DocumentService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceImpl.class);

    /** The generation doc service. */
    @Inject
    private GenerationDocService generationDocService;

    /** The fichier service. */
    @Inject
    private FichierService fichierService;

    /** The type document service. */
    @Inject
    private TypeDocumentService typeDocumentService;

    /** The document repository. */
    @Inject
    private DocumentRepository documentRepository;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /** Constante d'erreur : fichier ou type manquant. */
    private static final String ERREUR_FICHIER_TYPE_MANQUANT = "upload.fichier.ou.type.manquant";

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.document.DocumentService#genererDocumentPdf(fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum,
     * java.util.Map)
     */
    @Override
    public FichierTransfert genererDocumentPdf(TemplateDocumentEnum templateDocEnum, Map<String, Object> variablesContexte) {
        return this.generationDocService.genererFichierPdf(templateDocEnum, variablesContexte);
    }

    /*
     *
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.document.DocumentService#genererDocumentPdf(fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum,
     * java.util.List)
     */
    @Override
    public FichierTransfert genererDocumentPdf(TemplateDocumentEnum templateDocEnum, List<Map<String, Object>> listVariablesContexte) {
        return this.generationDocService.genererFichierPdf(templateDocEnum, listVariablesContexte);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentDemandeSubvention(DemandeSubvention demandeSubvention, FichierTransfert fichierTransfert) {

        if (demandeSubvention.getDossierSubvention() == null) {
            throw new TechniqueException("Impossible de creer un document de demande de subvention sans dossierSubvention");
        }

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_DEMANDE_SUBVENTION) {
            throw new TechniqueException("Le document à créer n'est pas de la famille demande de subvention");
        }

        Document document = new Document();
        document.setDemandeSubvention(demandeSubvention);
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentDemandeProlongation(fr.gouv.sitde.face.transverse.entities.
     * DemandeProlongation, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentDemandeProlongation(DemandeProlongation demandeProlongation, FichierTransfert fichierTransfert) {
        if (demandeProlongation.getDossierSubvention() == null) {
            throw new TechniqueException("Impossible de creer un document de demande de prolongation sans dossierSubvention");
        }

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_DEMANDE_PROLONGATION) {
            throw new TechniqueException("Le document à créer n'est pas de la famille demande de prolongation");
        }

        Document document = new Document();
        document.setDemandeProlongation(demandeProlongation);
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentDemandePaiement(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentDemandePaiement(DemandePaiement demandePaiement, FichierTransfert fichierTransfert) {
        if (demandePaiement.getDossierSubvention() == null) {
            throw new TechniqueException("Impossible de creer un document de demande de paiement sans dossierSubvention");
        }

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_DEMANDE_PAIEMENT) {
            throw new TechniqueException("Le document à créer n'est pas de la famille demande de paiement");
        }

        Document document = new Document();
        document.setDemandePaiement(demandePaiement);
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentDotationDepartement(fr.gouv.sitde.face.transverse.entities.
     * DotationDepartement, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentCourrierDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement, FichierTransfert fichierTransfert) {
        if (courrierAnnuelDepartement == null) {
            throw new TechniqueException("Impossible de creer un document de dotation de departement sans dotationDepartement");
        }

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_COURRIER_ANNUEL_DEPARTEMENT) {
            throw new TechniqueException("Le document à créer n'est pas de la famille demande de courrier departement");
        }

        Document document = new Document();
        document.setCourrierAnnuelDepartement(courrierAnnuelDepartement);
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentLigneDotationDepartement(fr.gouv.sitde.face.transverse.entities.
     * LigneDotationDepartement, fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement, FichierTransfert fichierTransfert) {
        if (ligneDotationDepartement.getDotationDepartement() == null) {
            throw new TechniqueException("Impossible de creer un document de ligne de dotation de departement sans dotationDepartement");
        }

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_LIGNE_DOTATION_DEPARTEMENT) {
            throw new TechniqueException("Le document à créer n'est pas de la famille demande de ligne de dotation departement");
        }

        Document document = new Document();
        document.setLigneDotationDepartement(ligneDotationDepartement);
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#creerDocumentModele(fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document creerDocumentModele(FichierTransfert fichierTransfert) {
        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        if (TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument()) != FamilleDocumentEnum.DOC_MODELE) {
            throw new TechniqueException("Le document à créer n'est pas de la famille doc_modele");
        }

        Document document = new Document();
        return this.enregistrerFichierEtDocument(document, fichierTransfert);
    }

    /**
     * Enregistrer fichier et document.
     *
     * @param document
     *            the document
     * @param fichierTransfert
     *            the fichier transfert
     * @return the document
     */
    private Document enregistrerFichierEtDocument(Document document, FichierTransfert fichierTransfert) {

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        Integer idCodifie = fichierTransfert.getTypeDocument().getIdCodifie();
        document.setTypeDocument(this.typeDocumentService.rechercherTypeDocumentParIdCodifie(idCodifie));
        document.setExtensionFichier(FilenameUtils.getExtension(fichierTransfert.getNomFichier()));
        document.setDateTeleversement(this.timeOperationTransverse.now());

        // Enregistrement du fichier
        File fichierCree = this.fichierService.enregistrerFichier(document, fichierTransfert);

        document.setNomFichierSansExtension(FilenameUtils.removeExtension(fichierCree.getName()));

        return this.documentRepository.creerDocument(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#rechercherDocumentParIdDocument(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentParIdDocument(Long idDocument) {
        FamilleDocument familleDoc = this.documentRepository.rechercherFamilleDocumentParIdDocument(idDocument);

        if (familleDoc == null) {
            return null;
        }

        FamilleDocumentEnum familleDocEnum = FamilleDocumentEnum.rechercherEnumParIdCodifie(familleDoc.getIdCodifie());

        return this.rechercherDocumentParIdDocumentEtFamille(idDocument, familleDocEnum);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#rechercherDocumentParIdDocumentEtFamille(java.lang.Long,
     * fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum)
     */
    @Override
    public Document rechercherDocumentParIdDocumentEtFamille(Long idDocument, FamilleDocumentEnum familleDocEnum) {

        Document doc = null;

        switch (familleDocEnum) {
            case DOC_DEMANDE_SUBVENTION:
                doc = this.documentRepository.rechercherDocumentDemandeSubventionParIdDoc(idDocument);
                break;
            case DOC_DEMANDE_PROLONGATION:
                doc = this.documentRepository.rechercherDocumentDemandeProlongationParIdDoc(idDocument);
                break;
            case DOC_DEMANDE_PAIEMENT:
                doc = this.documentRepository.rechercherDocumentDemandePaiementParIdDoc(idDocument);
                break;
            case DOC_COURRIER_ANNUEL_DEPARTEMENT:
                doc = this.documentRepository.rechercherDocumentCourrierAnnuelDepartementParIdDoc(idDocument);
                break;
            case DOC_LIGNE_DOTATION_DEPARTEMENT:
                doc = this.documentRepository.rechercherDocumentLigneDotationDepartementParIdDoc(idDocument);
                break;
            case DOC_MODELE:
                doc = this.documentRepository.rechercherDocumentModeleParIdDoc(idDocument);
                break;
        }

        return doc;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#chargerFichierDocument(java.lang.Long)
     */
    @Override
    public File chargerFichierDocument(Long idDocument) {
        // si aucun id spécifié
        if (idDocument == null) {
            throw new TechniqueException("L'id du document a télécharger est null");
        }

        // recherche du document
        Document document = this.rechercherDocumentParIdDocument(idDocument);

        // si document innexistant
        if (document == null) {
            LOGGER.warn("Le fichier avec l'id {} n'a pas été trouvé", idDocument);
            throw new TechniqueException("download.erreur.non.trouve");
        }

        // chargement
        return this.fichierService.chargerFichier(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#supprimerDocument(java.lang.Long)
     */
    @Override
    public void supprimerDocument(Long idDocument) {
        Document doc = this.rechercherDocumentParIdDocument(idDocument);

        try {
            this.fichierService.supprimerFichier(doc);
        } catch (TechniqueException e) {
            // L'erreur n'est pas bloquante.
            LOGGER.warn("Le fichier n'a pas pu être supprimé", e);
        }
        // Suppression en base
        this.documentRepository.supprimerDocument(doc);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#modifierFichierDocument(java.lang.Long,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public Document modifierFichierDocument(Long idDocument, FichierTransfert fichierTransfert) {

        if ((fichierTransfert == null) || (fichierTransfert.getTypeDocument() == null)) {
            throw new TechniqueException(ERREUR_FICHIER_TYPE_MANQUANT);
        }

        FamilleDocumentEnum familleDocumentEnum = TypeDocumentEnum.getFamilleFromTypeDoc(fichierTransfert.getTypeDocument());
        Document document = this.rechercherDocumentParIdDocumentEtFamille(idDocument, familleDocumentEnum);

        // Suppression du fichier actuellement enregistré.
        try {
            this.fichierService.supprimerFichier(document);
        } catch (TechniqueException e) {
            // L'erreur n'est pas bloquante.
            LOGGER.warn("Le fichier d'origine n'a pas pu être supprimé", e);
        }

        // Enregistrement du fichier
        document.setExtensionFichier(FilenameUtils.getExtension(fichierTransfert.getNomFichier()));
        File fichierCree = this.fichierService.enregistrerFichier(document, fichierTransfert);
        document.setNomFichierSansExtension(FilenameUtils.removeExtension(fichierCree.getName()));
        document.setDateTeleversement(this.timeOperationTransverse.now());

        return this.documentRepository.modifierDocument(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#rechercherFamilleDocumentParIdDocument(java.lang.Long)
     */
    @Override
    public FamilleDocument rechercherFamilleDocumentParIdDocument(Long idDocument) {
        return this.documentRepository.rechercherFamilleDocumentParIdDocument(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#zipperEtEncoderDocument(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public byte[] zipperEtEncoderDocument(Document document) {
        return this.fichierService.zipperEtEncoderDonneesBase64(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#rechercherDocumentsDotationParIdCollectiviteEtAnnee(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<Document> rechercherDocumentsDotationParIdCollectiviteEtAnnee(Long idCollectivite, Integer annee) {
        return this.documentRepository.rechercherDocumentsDotationParIdCollectiviteEtAnnee(idCollectivite, annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#deplacerDirectoryDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention, java.lang.Long, boolean)
     */
    @Override
    public void deplacerDirectoryDemandeSubvention(DemandeSubvention demandeCible, DemandeSubvention demandeSource,
            boolean supprimerDirectoryDossier) {
        Document cible = new Document();
        cible.setDemandeSubvention(demandeCible);

        Document source = new Document();
        source.setDemandeSubvention(demandeSource);

        this.fichierService.deplacerSousDossierDemandeSubvention(cible, source, supprimerDirectoryDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#rechercherDocumentsDotationParCodeDepartement(java.lang.String)
     */
    @Override
    public List<Document> rechercherDocumentsDotationParCodeDepartement(String codeDepartement) {
        return this.documentRepository.rechercherDocumentsDotationParCodeDepartement(codeDepartement);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domaine.service.document.DocumentService#encoderDocument(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public byte[] encoderDocument(Document doc) {
        return this.fichierService.encoderDonneesBase64(doc);
    }
}

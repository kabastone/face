/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.referentiel.ProgrammeRepository;
import fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService;
import fr.gouv.sitde.face.domaine.service.dotation.DotationValidateursService;
import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.entities.Programme;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class DotationNationaleServiceImpl.
 *
 * @author a453029
 */
@Named
public class DotationNationaleServiceImpl implements DotationNationaleService {

    /** The programme repository. */
    @Inject
    private ProgrammeRepository programmeRepository;

    /** The sous programme repository. */
    @Inject
    private SousProgrammeRepository sousProgrammeRepository;

    /** The dotation programme repository. */
    @Inject
    private DotationSousProgrammeRepository dotationSousProgrammeRepository;

    /** The dotation programme repository. */
    @Inject
    private DotationProgrammeRepository dotationProgrammeRepository;

    /** The Dotation validateurs service. */
    @Inject
    private DotationValidateursService dotationValidateursService;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService#rechercherDotationsSousProgrammesSur4Ans(java.lang.String)
     */
    @Override
    public List<DotationSousProgramme> rechercherDotationsSousProgrammesSur4Ans(String codeProgramme) {
        return this.dotationSousProgrammeRepository.rechercherDotationsSousProgrammesSur4Ans(codeProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService#initialiserDotationsInexistantesAnneeEnCours()
     */
    @Override
    public void initialiserDotationsInexistantesAnneeEnCours() {
        List<DotationProgramme> dotationsCreees = this.initialiserDotationsProgrammesAnneeEnCours();

        for (DotationProgramme dotaProgramme : dotationsCreees) {
            this.initialiserDotationsSousProgramme(dotaProgramme);
        }
    }

    /**
     * Initialiser les dotations programmes inexistantes pour l'année en cours.
     *
     * @return the list
     */
    private List<DotationProgramme> initialiserDotationsProgrammesAnneeEnCours() {

        List<Programme> listeProgrammesSansDotation = this.programmeRepository.rechercherProgrammesSansDotationPourAnneeEnCours();
        List<DotationProgramme> dotationsCreees = new ArrayList<>(listeProgrammesSansDotation.size());

        for (Programme programme : this.programmeRepository.rechercherProgrammesSansDotationPourAnneeEnCours()) {
            DotationProgramme dotaProgramme = new DotationProgramme();
            dotaProgramme.setProgramme(programme);
            dotaProgramme.setAnnee(this.timeOperationTransverse.getAnneeCourante());
            dotaProgramme.setMontant(BigDecimal.ZERO);
            dotationsCreees.add(this.dotationProgrammeRepository.creerDotationProgramme(dotaProgramme));
        }

        return dotationsCreees;
    }

    /**
     * Initialiser les dotations sous programme de la dotation de programme en parametre.
     *
     * @param dotationProgramme
     *            the dota programme
     */
    private void initialiserDotationsSousProgramme(DotationProgramme dotationProgramme) {

        List<SousProgramme> sousProgrammesSansDotation = this.sousProgrammeRepository
                .rechercherSousProgrammesParDotationProgramme(dotationProgramme.getId());

        for (SousProgramme spSansDotation : sousProgrammesSansDotation) {
            DotationSousProgramme dotaSousProgramme = new DotationSousProgramme();
            dotaSousProgramme.setDotationProgramme(dotationProgramme);
            dotaSousProgramme.setSousProgramme(spSansDotation);
            dotaSousProgramme.setMontant(BigDecimal.ZERO);
            this.dotationSousProgrammeRepository.creerDotationSousProgramme(dotaSousProgramme);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService#enregistrerMontantDotationProgramme(fr.gouv.sitde.face.transverse.entities
     * .DotationProgramme)
     */
    @Override
    public DotationProgramme enregistrerMontantDotationProgramme(DotationProgramme dotationProgramme) {
        DotationProgramme dotaProgrammeExistante = this.dotationProgrammeRepository.rechercherDotationProgrammeParId(dotationProgramme.getId());
        this.dotationValidateursService.validerProgrammeAvecSousProgrammes(dotaProgrammeExistante, dotationProgramme.getMontant());
        dotaProgrammeExistante.setMontant(dotationProgramme.getMontant());
        return this.dotationProgrammeRepository.modifierDotationProgramme(dotaProgrammeExistante);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.dotation.DotationNationaleService#enregistrerMontantDotationSousProgramme(fr.gouv.sitde.face.transverse.
     * entities.DotationSousProgramme)
     */
    @Override
    public DotationSousProgramme enregistrerMontantDotationSousProgramme(DotationSousProgrammeMontantBo dotationSousProgrammeMontantBo) {

        if (dotationSousProgrammeMontantBo.isConcerneInitial() == null) {
            throw new TechniqueException("Le montant indiqué doit concerner l'initial ou l'effectif mais ne peut être nul");
        }

        DotationSousProgramme dotationExistante = this.dotationSousProgrammeRepository
                .rechercherDotationSousProgrammeParId(dotationSousProgrammeMontantBo.getIdDotationSousProgramme());

        if (dotationSousProgrammeMontantBo.isConcerneInitial()) {
            this.ajouterMontantInitialEtEffectif(dotationSousProgrammeMontantBo.getMontant(), dotationExistante);
        } else {
            this.ajouterMontantEffectifUniquement(dotationSousProgrammeMontantBo.getMontant(), dotationExistante);
        }
        return this.dotationSousProgrammeRepository.modifierDotationSousProgramme(dotationExistante);
    }

    /**
     * @param dotationSousProgrammeMontantBo
     * @param dotationExistante
     */
    public void ajouterMontantInitialEtEffectif(BigDecimal montant, DotationSousProgramme dotationExistante) {
        BigDecimal nouveauMontantInitial = dotationExistante.getMontantInitial().add(montant);
        BigDecimal nouveauMontantEffectif = dotationExistante.getMontant().add(montant);

        this.dotationValidateursService.validerSousProgrammeAvecProgrammeInitial(dotationExistante, nouveauMontantInitial);
        this.dotationValidateursService.validerSousProgrammeAvecProgrammeEffectif(dotationExistante, nouveauMontantEffectif);
        this.dotationValidateursService.validerSousProgrammeAvecDepartements(dotationExistante, nouveauMontantEffectif);

        dotationExistante.setMontantInitial(nouveauMontantInitial);
        dotationExistante.setMontant(nouveauMontantEffectif);
    }

    /**
     * @param dotationSousProgrammeMontantBo
     * @param dotationExistante
     */
    public void ajouterMontantEffectifUniquement(BigDecimal montant, DotationSousProgramme dotationExistante) {
        BigDecimal nouveauMontantEffectif = dotationExistante.getMontant().add(montant);

        this.dotationValidateursService.validerSousProgrammeAvecProgrammeEffectif(dotationExistante, nouveauMontantEffectif);
        this.dotationValidateursService.validerSousProgrammeAvecDepartements(dotationExistante, nouveauMontantEffectif);
        dotationExistante.setMontant(nouveauMontantEffectif);
    }

    @Override
    public DotationSousProgramme recupererDotationSousProgrammeParAnneeEtSousProgramme(int annee, Integer idSousProgramme) {
        return this.dotationSousProgrammeRepository.recupererDotationSousProgrammeParAnneeEtSousProgramme(annee, idSousProgramme);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;

/**
 * Classe faite pour être étendue afin d'offrir des services de validation de bean via l'API Bean validation.
 *
 *
 * @author Atos
 *
 * @param <T>
 *            Classe du bean à valider
 */
public class BeanValidationService<T> {

    /**
     * Constructeur protected ne devant être appelé que par un super des classes filles.
     */
    protected BeanValidationService() {
        // Constructeur protected.
    }

    /**
     * Valide un bean en vérifiant ses contraintes pour les groupes passés en paramètre.<br/>
     * <b>N'envoie pas de RegleGestionException mais se contente de renvoyer une liste de MessageProperty.</b>
     * <p>
     * Toutes les violations de contraintes sont enregistrées dans la liste de MessageProperty renvoyée.
     *
     * @param bean
     *            le bean à valider
     * @param groupes
     *            les groupes pour lesquels il faut valider les contraintes.
     * @return la liste de MessageError correspondant au violations récupérées. La liste est vide si tout est OK.
     */
    protected List<MessageProperty> getErreursValidationBean(final T bean, final Class<?>... groupes) {

        Set<ConstraintViolation<T>> constraintViolations = this.validerBean(bean, groupes);
        List<MessageProperty> listeErrorMsg = new ArrayList<>();

        if (!constraintViolations.isEmpty()) {
            MessageProperty errorMsg = null;

            for (ConstraintViolation<T> constraint : constraintViolations) {
                // Pour chaque violation de contrainte, on ajoute un errorMessage à la liste de la rgException
                errorMsg = new MessageProperty(constraint.getMessage());
                listeErrorMsg.add(errorMsg);
            }
        }
        return listeErrorMsg;
    }

    /**
     * Valide un bean passé un paramètre pour les contraintes des groupes passés en paramètre et renvoie un set de ConstraintViolation.
     * <p>
     * S'il n'y a pas de violation, le set est renvoyé est vide.
     * </p>
     *
     * @param bean
     *            le bean à valider
     * @param groupes
     *            les groupes pour lesquels il faut valider les contraintes.
     * @return le set de violations de contraintes (peut être vide)
     */
    private Set<ConstraintViolation<T>> validerBean(final T bean, final Class<?>[] groupes) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(bean, groupes);
    }

}

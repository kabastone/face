/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation;

import java.util.List;

import fr.gouv.sitde.face.transverse.dotation.DotationSousProgrammeMontantBo;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;

/**
 * The Interface DotationNationaleService.
 *
 * @author a453029
 */
public interface DotationNationaleService {

    /**
     * Rechercher dotations sous programmes sur 4 ans.
     *
     * @param codeProgramme
     *            the code programme
     * @return the list
     */
    List<DotationSousProgramme> rechercherDotationsSousProgrammesSur4Ans(String codeProgramme);

    /**
     * Crée les dotations programme et dotations sous-programmes inexistantes pour l'année en cours.
     */
    void initialiserDotationsInexistantesAnneeEnCours();

    /**
     * Enregistrer montant dotation programme.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme
     */
    DotationProgramme enregistrerMontantDotationProgramme(DotationProgramme dotationProgramme);

    /**
     * Enregistrer montant dotation sous programme.
     *
     * @param dotationSousProgrammeMontantBo
     *            the dotation sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme enregistrerMontantDotationSousProgramme(DotationSousProgrammeMontantBo dotationSousProgrammeMontantBo);

    /**
     * Recuperer dotation sous programme par annee et sous programme.
     *
     * @param annee
     *            the annee
     * @param idSousProgramme
     *            the id sous programme
     * @return the dotation sous programme
     */
    DotationSousProgramme recupererDotationSousProgrammeParAnneeEtSousProgramme(int annee, Integer idSousProgramme);

}

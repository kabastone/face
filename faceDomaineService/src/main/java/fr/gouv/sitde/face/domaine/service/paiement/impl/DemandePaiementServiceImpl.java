/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository;
import fr.gouv.sitde.face.domaine.service.anomalie.AnomalieService;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.domaine.service.email.EmailRefusSubventionPaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.EtatDemandePaiementService;
import fr.gouv.sitde.face.domaine.service.referentiel.ReglementaireService;
import fr.gouv.sitde.face.domaine.service.subvention.DemandeSubventionService;
import fr.gouv.sitde.face.domaine.service.subvention.DossierSubventionService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.VersionAlgorithmeEnum;
import fr.gouv.sitde.face.transverse.util.MessageUtils;

/**
 * Service métier de DemandePaiement.
 *
 * @author Atos
 */
@Named
public class DemandePaiementServiceImpl extends BeanValidationService<DemandePaiement> implements DemandePaiementService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DemandePaiementServiceImpl.class);

    /** The Constant NB_FICHIERS_ENVOYER. */
    private static final int NB_FICHIERS_ENVOYER = 13;

    /** The demande subvention service. */
    @Inject
    private DemandeSubventionService demandeSubventionService;

    /** The demande paiement repository. */
    @Inject
    private DemandePaiementRepository demandePaiementRepository;

    /** The etat demande paiement. */
    @Inject
    private EtatDemandePaiementService etatDemandePaiementService;

    /** The dossier subvention service. */
    @Inject
    private DossierSubventionService dossierSubventionService;

    /** The reglementaire service. */
    @Inject
    private ReglementaireService reglementaireService;

    /** The document service. */
    @Inject
    private DocumentService documentService;

    /** The anomalie service. */
    @Inject
    private AnomalieService anomalieService;

    /** The email refus subvention paiement service. */
    @Inject
    private EmailRefusSubventionPaiementService emailRefusSubventionPaiementService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherDemandePaiement(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherDemandePaiement(Long idDemandePaiement) {
        // vérif
        if (idDemandePaiement == null) {
            throw new TechniqueException("Id demande de paiement null");
        }

        // recherche
        return this.demandePaiementRepository.rechercherParId(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherDemandePaiementAvecDocuments(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherDemandePaiementAvecDocuments(Long idDemandePaiement) {
        // vérif
        if (idDemandePaiement == null) {
            throw new TechniqueException("Id demande de paiement null");
        }

        // recherche
        DemandePaiement demandePaiement = this.demandePaiementRepository.rechercherParIdAvecDocs(idDemandePaiement);

        // récupération du dossier de subvention (avec calcul du reste à consommer, du plafond d'aide et du taux de consommation)
        DossierSubvention dossierSubvention = this.dossierSubventionService
                .rechercherDossierSubventionParId(demandePaiement.getDossierSubvention().getId());
        demandePaiement.setDossierSubvention(dossierSubvention);

        return demandePaiement;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.subvention.DemandePaiementService#rechercherDemandesPaiement(java.lang.Long)
     */
    @Override
    public List<DemandePaiement> rechercherDemandesPaiement(Long idDossierSubvention) {
        return this.demandePaiementRepository.rechercherParDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#realiserDemandePaiement(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement, java.util.List)
     */
    @Override
    public DemandePaiement realiserDemandePaiement(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader) {
        // vérif
        if (demandePaiement == null) {
            throw new TechniqueException("Demande de paiement null");
        }

        DossierSubvention dossierSubvention = this.dossierSubventionService
                .rechercherDossierSubventionParId(demandePaiement.getDossierSubvention().getId());

        if (TypeDemandePaiementEnum.SOLDE.equals(demandePaiement.getTypeDemandePaiement())) {
            this.calculerTauxPourSolde(demandePaiement, dossierSubvention);
        }

        // vérifie les champs et les documents obligatoires
        this.verifierChampsEtDocsObligatoires(demandePaiement, listeFichiersUploader);

        // On renseigne le numero ordre en cas de creation
        if (demandePaiement.getId() == null) {
            demandePaiement.setNumOrdre(this.demandePaiementRepository.rechercherNumeroOrdreSuivant(demandePaiement.getDossierSubvention().getId()));
        } else {
            demandePaiement.setNumOrdre(this.demandePaiementRepository.rechercherNumeroOrdre(demandePaiement.getId()));
        }

        // enregistrement de la demande de paiment (création ou maj)
        demandePaiement = this.demandePaiementRepository.enregistrer(demandePaiement);

        // on lui relie le dossier de subvention
        demandePaiement.setDossierSubvention(dossierSubvention);

        // gestion des fichiers à enregistrer
        this.gestionFichiersEnregistrer(listeFichiersUploader, demandePaiement);

        return demandePaiement;
    }

    /**
     * Calculer taux pour solde.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param dossierSubvention
     *            the dossier subvention
     */
    private void calculerTauxPourSolde(DemandePaiement demandePaiement, DossierSubvention dossierSubvention) {
        BigDecimal tauxSubvention = this.demandeSubventionService
                .rechercherDemandeSubventionPrincipaleParIdDossier(demandePaiement.getDossierSubvention().getId()).getTauxAide();
        BigDecimal tauxDemande = BigDecimal.ZERO;

        if (this.isDossierConditionsPourSoldeSurPlafondAide(demandePaiement, dossierSubvention)) {
            tauxDemande = dossierSubvention.getPlafondAideSansRefus().multiply(new BigDecimal("100"))
                    .divide(demandePaiement.getMontantTravauxRealises(), 4, RoundingMode.DOWN);
        } else if (demandePaiement.getMontantTravauxRealises().compareTo(BigDecimal.ZERO) > 0) {
            tauxDemande = dossierSubvention.getResteConsommerCalcule().multiply(new BigDecimal("100"))
                    .divide(demandePaiement.getMontantTravauxRealises(), 2, RoundingMode.DOWN);
        }

        demandePaiement.setTauxAide(tauxDemande.min(tauxSubvention));
    }

    /**
     * Checks if is dossier conditions pour solde sur plafond aide.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param dossierSubvention
     *            the dossier subvention
     * @return true, if is dossier conditions pour solde sur plafond aide
     */
    private boolean isDossierConditionsPourSoldeSurPlafondAide(DemandePaiement demandePaiement, DossierSubvention dossierSubvention) {
        Reglementaire regle = this.reglementaireService.rechercherReglementaireParIdDossier(dossierSubvention.getId());
        return VersionAlgorithmeEnum.V2.equals(regle.getVersionAlgorithme())
                && (demandePaiement.getMontantTravauxRealises().compareTo(BigDecimal.ZERO) > 0)
                && !DemandePaiementServiceImpl.asAcompteOuSolde(dossierSubvention.getDemandesPaiement())
                && (DemandePaiementServiceImpl.calculerSommeAvances(dossierSubvention.getDemandesPaiement()).compareTo(BigDecimal.ZERO) > 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherDemandePaiementPourPdf(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherDemandePaiementPourPdf(Long idDemandePaiement) {
        // récupération de la demande de paiement
        return this.demandePaiementRepository.rechercheDemandePaiementPourPdfParId(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#compteAccomptesPourDossierParIdDemande(java.lang.Long)
     */
    @Override
    public Integer compteAccomptesPourDossierParIdDemande(Long idDemandePaiement) {
        return this.demandePaiementRepository.compteAccomptesPourDossierParIdDemande(idDemandePaiement);
    }

    /**
     * Met à jour une demande de paiement vers l'état souhaité.
     *
     * @param idDemandePaiement
     *            the id demande paiement
     * @param etatDemandePaiementCible
     *            the etat demande paiement cible
     * @return the demande paiement
     */
    // vérif - id demande de paiement non présente
    private DemandePaiement majEtatDemandePaiement(Long idDemandePaiement, EtatDemandePaiementEnum etatDemandePaiementCible) {
        if (idDemandePaiement == null) {
            throw new TechniqueException("Demande de paiement null");
        }

        // vérif - etat cible non présent
        if (etatDemandePaiementCible == null) {
            throw new TechniqueException("Etat cible de la demande de paiement à null");
        }

        // recherche de la demande de paiement
        DemandePaiement demandePaiementBdd = this.rechercherDemandePaiementAvecDocuments(idDemandePaiement);

        // si l'état n'existe pas
        if (demandePaiementBdd == null) {
            throw new TechniqueException("Demande de paiement non trouvée en bdd");
        }

        // recherche de l'état cible en bdd
        EtatDemandePaiement nouvelEtatDemandePaiement = this.etatDemandePaiementService.rechercherParCode(etatDemandePaiementCible.toString());

        // si l'état n'existe pas
        if (nouvelEtatDemandePaiement == null) {
            throw new TechniqueException("Etat de demande de paiement non trouvé en bdd");
        }

        // maj de l'état
        demandePaiementBdd.setEtatDemandePaiement(nouvelEtatDemandePaiement);

        return demandePaiementBdd;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementAnomalieDetectee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementAnomalieDetectee(Long idDemandePaiement) {

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (!this.anomalieService.contientAnomaliesEtACorrigerParIdDemandePaiement(idDemandePaiement)) {
            throw new RegleGestionException("paiement.demande.anomalies.a.corriger");
        }

        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.ANOMALIE_DETECTEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementAnomalieSignalee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementAnomalieSignalee(Long idDemandePaiement) {

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (!this.anomalieService.contientAnomaliesEtACorrigerParIdDemandePaiement(idDemandePaiement)) {
            throw new RegleGestionException("paiement.demande.anomalies.a.corriger");
        }

        this.anomalieService.rendreAnomaliesDemandePaiementVisiblesPourAODE(idDemandePaiement);

        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.ANOMALIE_SIGNALEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementRefusee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementRefusee(Long idDemandePaiement, String motifRefus) {
        // vérification du motif de refus
        if (StringUtils.isBlank(motifRefus)) {
            throw new RegleGestionException("paiement.demande.motif.refus");
        }

        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.REFUSEE);

        // mise en place du motif de refus
        demandePaiement.setMotifRefus(motifRefus);

        // Envoi du mail de refus

        // envoi mail refus
        this.emailRefusSubventionPaiementService.emailRefusDemandeSubventionPaiement(demandePaiement.getDossierSubvention(), "paiement",
                demandePaiement.getDateDemande(), demandePaiement.getAideDemandee(), demandePaiement.getMotifRefus());

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementQualifiee(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement, java.util.List)
     */
    @Override
    public DemandePaiement majDemandePaiementQualifiee(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader) {
        // vérif - demande de paiement non présente
        if (demandePaiement == null) {
            throw new TechniqueException("Demande de paiement null");
        }

        // vérif - vérification du nombre fichiers envoyés : 1 seul doit être envoyé
        if (listeFichiersUploader.size() != 1) {
            throw new RegleGestionException("upload.fichier.aucun");
        }

        // La demande ne doit pas contenir d'anomalie non corrigée.
        if (this.anomalieService.contientAnomaliesEtACorrigerParIdDemandePaiement(demandePaiement.getId())) {
            throw new RegleGestionException("paiement.demande.repondre.anomalies");
        }

        // maj de la demande de paiement
        DemandePaiement demandePaiementBdd = this.majEtatDemandePaiement(demandePaiement.getId(), EtatDemandePaiementEnum.QUALIFIEE);

        // le fichier à uploader : doit être de type DECISION_ATTRIBUTIVE_PAIE ou bien un des documents déjà présents est de ce type.
        FichierTransfert fichierUploader = listeFichiersUploader.get(0);

        // Lambda vérifiant si le code du document correspond à une décision attributive de paiement
        Predicate<Document> predicate = o -> (TypeDocumentEnum.DECISION_ATTRIBUTIVE_PAIE.getCode().equals(o.getTypeDocument().getCode()));
        boolean decisionPresente = demandePaiementBdd.getDocuments().stream().anyMatch(predicate);
        if (!TypeDocumentEnum.DECISION_ATTRIBUTIVE_PAIE.equals(fichierUploader.getTypeDocument()) && !decisionPresente) {
            throw new TechniqueException("Le type du fichier à uploader est incorrect");
        }

        // persistence
        DemandePaiement demandePaiementMaj = this.demandePaiementRepository.enregistrer(demandePaiementBdd);

        // gestion des fichiers à enregistrer
        this.gestionFichiersEnregistrer(listeFichiersUploader, demandePaiementMaj);

        return demandePaiementMaj;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementAccordee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementAccordee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.ACCORDEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementControlee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementControlee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.CONTROLEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementEnAttenteTransfert(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementEnAttenteTransfert(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.EN_ATTENTE_TRANSFERT);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementCorrigee(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement, java.util.List, java.util.List)
     */
    @Override
    public DemandePaiement enregistrerDemandePaiementPourCorrection(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader,
            List<Long> idsDocsComplSuppr) {
        // vérif - demande de paiement non présente
        if (demandePaiement == null) {
            throw new TechniqueException("Demande de paiement null");
        }

        // vérif - vérification du nombre fichiers envoyés (13 maxi dans le cas de la correction)
        if (listeFichiersUploader.size() > NB_FICHIERS_ENVOYER) {
            LOGGER.info("Nb de fichiers envoyés : {}, attendu {}", listeFichiersUploader.size(), NB_FICHIERS_ENVOYER);
            throw new RegleGestionException("upload.fichier.nb.incorrect");
        }

        // persistence
        DemandePaiement demandePaiementMaj = this.demandePaiementRepository.enregistrer(demandePaiement);

        // gestion des fichiers à enregistrer
        this.gestionFichiersEnregistrer(listeFichiersUploader, demandePaiementMaj);

        // gestion des fichiers à supprimer
        this.gestionFichiersComplementairesSupprimer(idsDocsComplSuppr, demandePaiementMaj);

        return demandePaiementMaj;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementEnCoursTransfertEtapeDeux(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementEnCoursTransfertEtapeDeux(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.EN_COURS_TRANSFERT_2);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherDemandePaiementParIdChorus(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherDemandePaiementParIdChorus(Long idChorus) {
        return this.demandePaiementRepository.rechercherParIdChorus(idChorus);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementBdd(fr.gouv.sitde.face.transverse.entities.
     * DemandePaiement)
     */
    @Override
    public DemandePaiement majDemandePaiementBdd(DemandePaiement demandePaiement) {
        return this.demandePaiementRepository.mettreAJourDemandePaiement(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementTransferee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementTransferee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.TRANSFEREE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementCertifiee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementCertifiee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.CERTIFIEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /**
     * Enregistre les fichiers en bdd et les relie à la demande de paiement.
     *
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     * @param demandePaiement
     *            the demande paiement
     */
    private void gestionFichiersEnregistrer(List<FichierTransfert> listeFichiersUploader, DemandePaiement demandePaiement) {
        for (FichierTransfert fichierTransfere : listeFichiersUploader) {
            // Gestion des docs complementaires paiement
            if (TypeDocumentEnum.DOC_COMPLEMENTAIRE_PAIEMENT.equals(fichierTransfere.getTypeDocument())) {
                Document nouveauDocument = this.documentService.creerDocumentDemandePaiement(demandePaiement, fichierTransfere);
                demandePaiement.getDocuments().add(nouveauDocument);
            } else {
                Document documentExistant = getDocumentParCodeDocument(demandePaiement, fichierTransfere.getTypeDocument().getCode());
                if (documentExistant == null) {
                    // le document n'existe pas : on le crée et on l'ajoute à la liste
                    Document nouveauDocument = this.documentService.creerDocumentDemandePaiement(demandePaiement, fichierTransfere);
                    demandePaiement.getDocuments().add(nouveauDocument);
                } else {
                    // le document existe : on doit le mettre à jour
                    Document documentModifie = this.documentService.modifierFichierDocument(documentExistant.getId(), fichierTransfere);
                    // suppression du document précédent
                    demandePaiement.getDocuments().remove(documentExistant);
                    // ajout du document modifié
                    demandePaiement.getDocuments().add(documentModifie);
                }
            }
        }
    }

    /**
     * Gestion fichiers supprimer.
     *
     * @param idsDocsComplSuppr
     *            the ids docs compl suppr
     */
    private void gestionFichiersComplementairesSupprimer(List<Long> idsDocsComplSuppr, DemandePaiement demandePaiement) {
        // Suppression des fichiers complémentaires
        for (Long idDocument : idsDocsComplSuppr) {
            Document document = this.documentService.rechercherDocumentParIdDocument(idDocument);
            demandePaiement.getDocuments().remove(document);
            this.documentService.supprimerDocument(idDocument);
        }
    }

    /**
     * Récupère le document dans la demande de paiement via son code de type document.
     *
     * @param demandePaiement
     *            la demande de paiement
     * @param codeTypeDocument
     *            code du type du document à chercher
     * @return le document trouvé ou null.
     */
    private static Document getDocumentParCodeDocument(DemandePaiement demandePaiement, String codeTypeDocument) {
        for (Document document : demandePaiement.getDocuments()) {
            if (document.getTypeDocument().getCode().equals(codeTypeDocument)) {
                return document;
            }
        }
        return null;
    }

    /**
     * Verifie les champs obligatoires ainsi que les documents obligatoires.
     *
     * @param demandePaiement
     *            the demande paiement
     * @param listeFichiersUploader
     *            the liste fichiers uploader
     */
    private final void verifierChampsEtDocsObligatoires(DemandePaiement demandePaiement, List<FichierTransfert> listeFichiersUploader) {
        boolean isCreation = (demandePaiement.getId() == null);

        // vérif - Montant HT des travaux réalisé
        if (isCreation && (demandePaiement.getMontantTravauxRealises() == null)) {
            throw new RegleGestionException("Montant HT des travaux réalisés non renseigné");
        }
        // vérif - Taux de l'aide du FACE en %
        if (isCreation && (demandePaiement.getTauxAide() == null)) {
            throw new RegleGestionException("Taux de l'aide non renseigné");
        }

        // vérif - Montant aide
        if (demandePaiement.getMontantTravauxRealises().compareTo(BigDecimal.ZERO) <= 0) {
            throw new RegleGestionException("paiement.demande.montant.negatif");
        }
        if (demandePaiement.getTauxAide().compareTo(BigDecimal.ZERO) <= 0) {
            throw new RegleGestionException("paiement.demande.taux.negatif");
        }

        Reglementaire regle = this.reglementaireService.rechercherReglementaireParIdDossier(demandePaiement.getDossierSubvention().getId());

        if (VersionAlgorithmeEnum.V2.equals(regle.getVersionAlgorithme())) {
            this.verifierReglesV2(demandePaiement);
        }

        this.verifierMontantsEtTaux(demandePaiement, regle);

        // vérif - si aucun fichier n'a été envoyé
        if (listeFichiersUploader.isEmpty() && isCreation) {
            throw new RegleGestionException("upload.fichier.aucun");
        }
    }

    /**
     * @param demandePaiement
     */
    public void verifierMontantsEtTaux(DemandePaiement demandePaiement, Reglementaire regle) {
        // AVANCE
        DossierSubvention dossier = this.dossierSubventionService.rechercherDossierSubventionParId(demandePaiement.getDossierSubvention().getId());

        if (!this.demandePaiementRepository.isEstUniqueDemandePaiementEnCours(dossier.getNumDossier())) {
            throw new RegleGestionException("paiement.demande.non.unique");
        }

        BigDecimal avanceMaximumDemande = this.calculerAvanceMaximumDemande(dossier);

        BigDecimal aideDemandee;
        BigDecimal sommeAvances = BigDecimal.ZERO;
        List<DemandePaiement> demandesPrecedentes = this.rechercherDemandesPaiement(demandePaiement.getDossierSubvention().getId());

        if (VersionAlgorithmeEnum.V2.equals(regle.getVersionAlgorithme())
                && !demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE)
                && demandesPrecedentes.stream().map(DemandePaiement::getTypeDemandePaiement).noneMatch(TypeDemandePaiementEnum.ACOMPTE::equals)) {
            sommeAvances = DemandePaiementServiceImpl.calculerSommeAvances(demandesPrecedentes);
            aideDemandee = demandePaiement.getMontantTravauxRealises().multiply(demandePaiement.getTauxAide()).divide(new BigDecimal(100))
                    .subtract(sommeAvances).setScale(2, RoundingMode.HALF_UP);
        } else {
            aideDemandee = demandePaiement.getMontantTravauxRealises().multiply(demandePaiement.getTauxAide()).divide(new BigDecimal(100)).setScale(2,
                    RoundingMode.HALF_UP);
        }

        if (demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE) && (aideDemandee.compareTo(avanceMaximumDemande) > 0)) {
            throw new RegleGestionException("paiement.demande.montant.depasse", MessageUtils.formatMontant(avanceMaximumDemande));
        }

        BigDecimal acompteMaximumDemande = this.calculerAcompteMaximumDemande(dossier);

        // ACOMPTE
        if (demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.ACOMPTE) && (aideDemandee.compareTo(acompteMaximumDemande) > 0)) {
            throw new RegleGestionException("paiement.demande.montant.depasse", MessageUtils.formatMontant(acompteMaximumDemande.add(sommeAvances)));
        }

        BigDecimal soldeMaximumDemande = dossier.getPlafondAide().subtract(dossier.getAideDemandee());

        // SOLDE
        if (demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.SOLDE) && (aideDemandee.compareTo(soldeMaximumDemande) > 0)) {
            throw new RegleGestionException("paiement.demande.montant.depasse", MessageUtils.formatMontant(soldeMaximumDemande));
        }

        DemandeSubvention demandeSubventionPrincipale = this.demandeSubventionService
                .rechercherDemandeSubventionPrincipaleParIdDossier(dossier.getId());

        // Si la demande paiement n'est pas de type AVANCE
        // vérif - taux aide
        if (!demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE)
                && ((demandePaiement.getTauxAide().compareTo(demandeSubventionPrincipale.getTauxAide())) > 0)) {
            throw new RegleGestionException("paiement.demande.taux.depasse", String.valueOf(demandeSubventionPrincipale.getTauxAide()));
        }
    }

    /**
     * Verifier regles V 2.
     *
     * @param demandePaiement
     *            the demande paiement
     */
    private void verifierReglesV2(DemandePaiement demandePaiement) {
        List<DemandePaiement> demandesPrecedentes = this.rechercherDemandesPaiement(demandePaiement.getDossierSubvention().getId());
        if (demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE)
                && DemandePaiementServiceImpl.asAcompteOuSolde(demandesPrecedentes)) {
            throw new RegleGestionException("paiement.demande.acompte.solde.deja.realise");
        }
        if ((demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.ACOMPTE)
                || demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.SOLDE))
                && DemandePaiementServiceImpl.asAvanceNonVerse(demandesPrecedentes)) {
            throw new RegleGestionException("paiement.demande.avance.non.versee");
        }
        if (demandePaiement.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.ACOMPTE)
                && demandesPrecedentes.stream().map(DemandePaiement::getTypeDemandePaiement).noneMatch(TypeDemandePaiementEnum.ACOMPTE::equals)) {
            BigDecimal sommeAvances = DemandePaiementServiceImpl.calculerSommeAvances(demandesPrecedentes);
            if ((demandePaiement.getMontantTravauxRealises().multiply(demandePaiement.getTauxAide()).divide(new BigDecimal(100)))
                    .compareTo(sommeAvances) < 0) {
                throw new RegleGestionException("paiement.demande.acompte.inferieur.avances", MessageUtils.formatMontant(sommeAvances));
            }
        }
    }

    /**
     * @param demandesPrecedentes
     * @return
     */
    private static BigDecimal calculerSommeAvances(Collection<DemandePaiement> demandesPrecedentes) {
        return demandesPrecedentes.stream()
                .filter(demande -> (demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE))
                        && (EtatDemandePaiementEnum.VERSEE.name().equals(demande.getEtatDemandePaiement().getCode())))
                .map(DemandePaiement::getAideDemandee).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * @param demandesPrecedentes
     * @return
     */
    private static boolean asAcompteOuSolde(Collection<DemandePaiement> demandesPrecedentes) {
        return demandesPrecedentes.stream().anyMatch(demande -> (demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.ACOMPTE)
                || demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.SOLDE)));
    }

    /**
     * Vérifier pour un acompte ou un solde s'il n'existe pas dans le dossier de subvention une demande de paiement de type avance non versé .
     *
     * @param DemandePaiement
     *
     * @return Boolean
     */

    private static boolean asAvanceNonVerse(List<DemandePaiement> listeDemandePaiement) {
        return listeDemandePaiement.stream().anyMatch(demande -> (demande.getTypeDemandePaiement().equals(TypeDemandePaiementEnum.AVANCE))
                && !(EtatDemandePaiementEnum.VERSEE.name().equals(demande.getEtatDemandePaiement().getCode())));
    }

    /**
     * Calculer avance maximum demande.
     *
     * @param dossier
     *            the dossier
     * @return the big decimal
     */
    @Override
    public BigDecimal calculerAvanceMaximumDemande(DossierSubvention dossier) {
        Reglementaire reglementaire = this.reglementaireService.rechercherReglementaire(
                dossier.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
        BigDecimal avanceMaximumDossier = dossier.getPlafondAide().multiply(new BigDecimal(reglementaire.getTauxAidePaiementAvance()))
                .divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);

        BigDecimal montantAvancesTotal = this.demandePaiementRepository.rechercherMontantAvanceTotalDemandesPaiementDossier(dossier.getId())
                .setScale(2, RoundingMode.HALF_UP);

        return avanceMaximumDossier.subtract(montantAvancesTotal);
    }

    /**
     * Calculer acompte maximum demande.
     *
     * @param dossier
     *            the dossier
     * @return the big decimal
     */
    @Override
    public BigDecimal calculerAcompteMaximumDemande(DossierSubvention dossier) {
        Reglementaire reglementaire = this.reglementaireService.rechercherReglementaire(
                dossier.getDotationCollectivite().getDotationDepartement().getDotationSousProgramme().getDotationProgramme().getAnnee());
        return dossier.getPlafondAide().multiply(new BigDecimal(reglementaire.getTauxAidePaiementAccompte())).divide(new BigDecimal(100))
                .subtract(dossier.getAideDemandee()).setScale(2, RoundingMode.HALF_UP);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherToutesDemandesPaiementPourBatch()
     */
    @Override
    public List<DemandePaiement> rechercherToutesDemandesPaiementPourBatch() {
        return this.demandePaiementRepository.rechercherToutesDemandesPaiementPourBatch();

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementEnCoursTransfertEtapeUne(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementEnCoursTransfertEtapeUne(Long id) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(id, EtatDemandePaiementEnum.EN_COURS_TRANSFERT_1);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementVersee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementVersee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.VERSEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#getIdfromIdAe(java.lang.Long)
     */
    @Override
    public Long getIdfromIdAe(Long idAe) {
        return this.demandePaiementRepository.getIdfromIdAe(idAe);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherCertifNonNulParService(java.lang.Long)
     */
    @Override
    public List<DemandePaiement> rechercherCertifNonNulParService(String chorusNumSf) {
        return this.demandePaiementRepository.rechercherCertifNonNulParService(chorusNumSf);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherVersementNonNulParService(java.lang.String)
     */
    @Override
    public List<DemandePaiement> rechercherVersementNonNulParService(String chorusNumSf) {
        return this.demandePaiementRepository.rechercherVersementNonNulParService(chorusNumSf);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherToutesDemandesPaiementPourFen0159a()
     */
    @Override
    public List<DemandePaiement> rechercherToutesDemandesPaiementPourFen0159a() {
        return this.demandePaiementRepository.rechercherToutesDemandesPaiementPourFen0159a();

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementEnCoursTransfertEtapeFin(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementEnCoursTransfertEtapeFin(Long id) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(id, EtatDemandePaiementEnum.EN_COURS_TRANSFERT_FIN);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#rechercherToutesPourRepartition()
     */
    @Override
    public List<DemandePaiement> rechercherToutesPourRepartition() {
        return this.demandePaiementRepository.rechercherToutesPourRepartition();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.DemandePaiementService#majDemandePaiementCorrigee(java.lang.Long)
     */
    @Override
    public DemandePaiement majDemandePaiementCorrigee(Long idDemandePaiement) {
        // maj
        DemandePaiement demandePaiement = this.majEtatDemandePaiement(idDemandePaiement, EtatDemandePaiementEnum.CORRIGEE);

        // persistence
        return this.demandePaiementRepository.enregistrer(demandePaiement);
    }

    @Override
    public boolean isEstUniqueDemandePaiementEnCours(String numDossier) {
        return this.demandePaiementRepository.isEstUniqueDemandePaiementEnCours(numDossier);
    }
}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.anomalie;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;

/**
 * The Interface TypeAnomalieService.
 *
 * @author A754839
 */
public interface TypeAnomalieService {

    /**
     * Rechercher par code.
     *
     * @param code
     *            the code
     * @return the type anomalie
     */
    TypeAnomalie rechercherParCode(String code);

    /**
     * Rechercher tous les types d'anomalie : pour une demande de paiement.
     *
     * @return the list
     */
    List<TypeAnomalie> rechercherTousPourDemandePaiement();

    /**
     * Rechercher tous les types d'anomalie : pour une demande de subvention.
     *
     * @return the list
     */
    List<TypeAnomalie> rechercherTousPourDemandeSubvention();
}

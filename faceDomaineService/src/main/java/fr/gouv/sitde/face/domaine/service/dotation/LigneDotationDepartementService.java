package fr.gouv.sitde.face.domaine.service.dotation;

import java.time.LocalDateTime;
import java.util.List;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Interface LigneDotationDepartementService.
 */
public interface LigneDotationDepartementService {

    /**
     * Ajouter ligne dotation departement.
     *
     * @param ligneDotationDepartement
     *            the ligne dotation departement
     * @param typeDotationDepartement
     *            the type dotation departement
     * @return the ligne dotation departement
     */
    LigneDotationDepartement ajouterLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement,
            TypeDotationDepartementEnum typeDotationDepartement);

    /**
     * Lister les lignes de dotation départementale en préparation pour l'année en cours.
     *
     * @param idDepartement
     *            the id departement
     * @return La liste des lignes de dotation départementale en préparation pour l'année en cours
     */
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleEnPreparation(Integer idDepartement);

    /**
     * Lister les lignes de dotation départementale notifiées pour l'année en cours.
     *
     * @param idDepartement
     *            the id departement
     * @return La liste des lignes de dotation départementale notifiées pour l'année en cours
     */
    List<LigneDotationDepartement> rechercherLignesDotationDepartementaleNotifiees(Integer idDepartement);

    /**
     * Supprimer une ligne de dotation départementale.
     *
     * @param idLigneDotationDepartementale
     *            the id ligne dotation departementale
     */
    void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale);

    /**
     * Rechercher document notification pour ligne.
     *
     * @param idLigne
     *            the id ligne
     * @return the document
     */
    Document rechercherDocumentNotificationPourLigne(Long idLigne);

    /**
     * Rechercher lignes dotation departementale.
     *
     * @param plafondAide
     *            the plafond aide
     * @param id
     *            the id
     * @param string
     *            the string
     * @return the long
     */
    Long rechercherLignesDotationDepartementale(LocalDateTime dateAccord);

    /**
     * Rechercher lignes pertes et renonciations.
     *
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciations(String codeProgramme);

    /**
     * Rechercher lignes pertes et renonciations par id collectivite.
     *
     * @param anneeCourante
     *            the annee courante
     * @return the list
     */
    List<LigneDotationDepartement> rechercherLignesPertesEtRenonciationsParIdCollectivite(Long idCollectivite);

}

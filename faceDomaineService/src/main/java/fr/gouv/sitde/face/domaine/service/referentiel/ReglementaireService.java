/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel;

import fr.gouv.sitde.face.transverse.entities.Reglementaire;

/**
 * The Interface ReglementaireService.
 */
public interface ReglementaireService {

    /*
     * Récuperation des données depuis le repository r_reglementaire pour la date du jour.
     *
     *
     * @return the Reglementaire
     *
     */
    Reglementaire rechercherReglementaire();

    /*
     * Récuperation des données depuis le repository r_reglementaire pour la date desiree.
     *
     *
     * @return the Reglementaire
     *
     */
    Reglementaire rechercherReglementaire(Integer anneeMiseEnApplication);

    /**
     * Rechercher reglementaire par id dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the reglementaire
     */
    Reglementaire rechercherReglementaireParIdDossier(Long idDossier);

}

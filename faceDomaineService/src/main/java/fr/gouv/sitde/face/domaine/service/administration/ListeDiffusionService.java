package fr.gouv.sitde.face.domaine.service.administration;

import java.util.List;

import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * The Interface ListeDiffusionService.
 */
public interface ListeDiffusionService {

    /**
     * Pour la liste de diffusion : recherche toutes les adresses e-mail d'une collectivité.
     *
     * @param idCollectivite
     *            the id collectivite
     * @return the list
     */
    List<AdresseEmail> rechercherAdressesEmail(Long idCollectivite);

    /**
     * Suppression de l'adresse e-mail dans la liste de diffusion.
     *
     * @param idAdresseEmail the id adresse email
     */
    void supprimerAdresseEmail(Long idAdresseEmail);

    /**
     * Ajout de l'e-mail à la liste de diffusion de la collectivité.
     *
     * @param adresseEmail the adresse email
     * @param idCollectivite the id collectivite
     * @return the adresse email dto
     */
    AdresseEmail ajouterAdresseEmailCollectivite(String adresseEmail, Long idCollectivite);
}

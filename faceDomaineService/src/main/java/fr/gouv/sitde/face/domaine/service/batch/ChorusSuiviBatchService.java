/**
 *
 */
package fr.gouv.sitde.face.domaine.service.batch;

import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * @author Atos
 *
 */
public interface ChorusSuiviBatchService {
    /**
     * Lire suivi batch.
     *
     * @return the chorus suivi batch
     */
    ChorusSuiviBatch lireSuiviBatch();

    /**
     * Mettre A jour suivi batch.
     *
     * @param chorusSuiviBatch
     *            the chorus suivi batch
     * @return the chorus suivi batch
     */
    ChorusSuiviBatch mettreAJourSuiviBatch(ChorusSuiviBatch chorusSuiviBatch);

}

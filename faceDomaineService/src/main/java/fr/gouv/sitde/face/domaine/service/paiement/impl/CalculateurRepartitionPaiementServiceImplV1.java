/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.inject.Named;

import fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService;
import fr.gouv.sitde.face.domaine.service.paiement.dto.MontantsRepartitionDto;
import fr.gouv.sitde.face.domaine.service.paiement.dto.RepartitionPaiementDto;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;

/**
 * @author A754839
 *
 */
@Named
public class CalculateurRepartitionPaiementServiceImplV1 implements CalculateurRepartitionPaiementService {

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirAncienSolde(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirAncienSolde(MontantsRepartitionDto montantsDto) {
        // Si on a affaire à un solde, il est d'abord prélevé sur le solde, puis sur les avances et enfin sur les acomptes.
        if (montantsDto.getMontantTransfert().compareTo(montantsDto.getTempSolde().add(montantsDto.getTempAvance())) > 0) {

            montantsDto.setMontantTransfert(
                    montantsDto.getMontantTransfert().subtract(montantsDto.getTempSolde()).subtract(montantsDto.getTempAvance()));
            montantsDto.setTempSolde(montantsDto.getTempSolde().subtract(montantsDto.getTempSolde()).setScale(2, RoundingMode.HALF_UP));
            montantsDto.setTempAvance(montantsDto.getTempAvance().subtract(montantsDto.getTempAvance()).setScale(2, RoundingMode.HALF_UP));
            montantsDto.setTempAcompte(montantsDto.getTempAcompte().subtract(montantsDto.getMontantTransfert()));
        } else if (montantsDto.getMontantTransfert().compareTo(montantsDto.getTempSolde()) > 0) {
            montantsDto.setTempSolde(montantsDto.getTempSolde().subtract(montantsDto.getTempSolde()).setScale(2, RoundingMode.HALF_UP));
            montantsDto.setTempAvance(montantsDto.getTempAvance().subtract(montantsDto.getMontantTransfert().subtract(montantsDto.getTempSolde()))
                    .setScale(2, RoundingMode.HALF_UP));
        } else {
            montantsDto.setTempSolde(montantsDto.getTempSolde().subtract(montantsDto.getMontantTransfert()).setScale(2, RoundingMode.HALF_UP));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirAncienAcompte(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirAncienAcompte(MontantsRepartitionDto montantsDto) {
        // Les acomptes sont prélevés sur les acomptes en premier lieu, sur les avances ensuite.
        if (montantsDto.getMontantTransfert().compareTo(montantsDto.getTempAcompte()) > 0) {

            montantsDto.setMontantTransfert(montantsDto.getMontantTransfert().subtract(montantsDto.getTempAcompte()));
            montantsDto.setTempAcompte(BigDecimal.ZERO);
            montantsDto.setTempAvance(montantsDto.getTempAvance().subtract(montantsDto.getMontantTransfert()).setScale(2, RoundingMode.HALF_UP));
        } else {
            montantsDto.setTempAcompte(montantsDto.getTempAcompte().subtract(montantsDto.getMontantTransfert()).setScale(2, RoundingMode.HALF_UP));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirAncienneAvance(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirAncienneAvance(MontantsRepartitionDto montantsDto) {
        // Les avances sont forcément prélevées dans les avances.
        montantsDto.setTempAvance(montantsDto.getTempAvance().subtract(montantsDto.getMontantTransfert()).setScale(2, RoundingMode.HALF_UP));
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirNouveauSolde(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirNouveauSolde(RepartitionPaiementDto donneesRepartition) {
        // On répartit d'abord le montant dans le solde.
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.SOLDE);

        // Si le montant cible n'est toujours pas atteint, l'argent est prélevé sur le montant des avances.
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.AVANCE);

        // Si le montant cible n'est toujours pas atteint, l'argent est prélevé sur le montant des acomptes.
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.ACOMPTE);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirNouveauAcompte(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirNouveauAcompte(RepartitionPaiementDto donneesRepartition) {
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.ACOMPTE);

        // Si le montant cible n'est toujours pas atteint, l'argent est prélevé sur le montant des avances.
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.AVANCE);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.paiement.CalculateurRepartitionPaiementService#repartirNouvelleAvance(fr.gouv.sitde.face.domaine.service.
     * paiement.dto.MontantsRepartitionDto)
     */
    @Override
    public void repartirNouvelleAvance(RepartitionPaiementDto donneesRepartition) {
        RepartitionPaiementUtils.repartirMontantDansTransferts(donneesRepartition, TypeDemandePaiementEnum.AVANCE);
    }

}

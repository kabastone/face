/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.groups.Default;

import fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository;
import fr.gouv.sitde.face.domaine.service.administration.AdresseService;
import fr.gouv.sitde.face.domaine.service.referentiel.DepartementService;
import fr.gouv.sitde.face.domaine.service.validation.BeanValidationService;
import fr.gouv.sitde.face.transverse.entities.Adresse;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.messages.MessageProperty;

/**
 * The Class AdresseServiceImpl.
 *
 * @author Atos
 */
@Named
public class AdresseServiceImpl extends BeanValidationService<Adresse> implements AdresseService {

    /** The adresse repository. */
    @Inject
    private AdresseRepository adresseRepository;

    @Inject
    private DepartementService departementService;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#rechercherAdresseParId(java.lang.Long)
     */
    @Override
    public Adresse rechercherAdresseParId(Long idAdresse) {
        return this.adresseRepository.rechercherParId(idAdresse);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#modifierAdresse(fr.gouv.sitde.face.transverse.entities.Adresse)
     */
    @Override
    public Adresse modifierAdresse(Adresse adresse) {
        this.validerAdresse(adresse);
        return this.adresseRepository.modifierAdresse(adresse);
    }

    /**
     * Valider adresse.
     *
     * @param adresse
     *            the adresse
     */
    private void validerAdresse(Adresse adresse) {
        List<MessageProperty> listeErreurs = this.getErreursValidationBean(adresse, Default.class);

        // Envoi des exceptions
        if (!listeErreurs.isEmpty()) {
            throw new RegleGestionException(listeErreurs, listeErreurs.get(0).getCleMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#creerAdresse(fr.gouv.sitde.face.transverse.entities.Adresse)
     */
    @Override
    public Adresse creerAdresse(Adresse adresse) {
        this.validerAdresse(adresse);
        return this.adresseRepository.creerAdresse(adresse);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#rechercherAdresseParDepartement(java.lang.Integer)
     */
    @Override
    public Adresse rechercherAdresseParDepartement(Integer idDepartement) {
        return this.adresseRepository.rechercherParDepartement(idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#enregistrerAdressePourDepartement(java.lang.Integer,
     * fr.gouv.sitde.face.transverse.entities.Adresse)
     */
    @Override
    public Adresse enregistrerAdressePourDepartement(Integer idDepartement, Adresse adresse) {

        if (adresse.getId() != null) {
            adresse = this.modifierAdresse(adresse);
        } else {
            adresse = this.creerAdresse(adresse);
        }
        Departement departement = this.departementService.rechercherDepartementParId(idDepartement);
        departement.setAdresse(adresse);
        this.departementService.majDepartement(departement);

        return adresse;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.AdresseService#gererAdressePourCollectivite(fr.gouv.sitde.face.transverse.entities.
     * Collectivite)
     */
    @Override
    public void gererAdressePourCollectivite(Collectivite collectivite) {
        this.validerAdresse(collectivite.getAdresseMoa());

        // Si la collectivite a les deux adresses et que les adresses moa et moe existent bien
        if ((collectivite.getAdresseMoa() != null) && (collectivite.getAdresseMoe() != null) && !(collectivite.isAvecMoaMoeIdentiques())
                && !(collectivite.isAvecPlusieursMoe())) {
            this.validerAdresse(collectivite.getAdresseMoe());
            // On vérifie si l'adresse Moa a déjà un id.
            if (collectivite.getAdresseMoa().getId() != null) {
                // Si oui, on la modifie.
                collectivite.setAdresseMoa(this.modifierAdresse(collectivite.getAdresseMoa()));
            } else {
                // Sinon on la crée.
                collectivite.setAdresseMoa(this.creerAdresse(collectivite.getAdresseMoa()));
            }
            // On vérifie si l'adresse Moe a déjà un id.
            if (collectivite.getAdresseMoe().getId() != null) {
                // Si oui, on la modifie.
                collectivite.setAdresseMoe(this.modifierAdresse(collectivite.getAdresseMoe()));
            } else {
                // Sinon on la crée.
                collectivite.setAdresseMoe(this.creerAdresse(collectivite.getAdresseMoe()));
            }
        } else if (collectivite.getAdresseMoa() != null) {
            // Sinon seule l'adresse Moa est existante.
            // On vérifie si l'adresse Moa a déjà un id.
            if (collectivite.getAdresseMoa().getId() != null) {
                // Si oui, on la modifie.
                collectivite.setAdresseMoa(this.modifierAdresse(collectivite.getAdresseMoa()));
            } else {
                // Sinon on la crée.
                collectivite.setAdresseMoa(this.creerAdresse(collectivite.getAdresseMoa()));
            }
            // On assure l'adresse Moe comme étant nulle.
            collectivite.setAdresseMoe(null);
        }
    }
}

package fr.gouv.sitde.face.domaine.service.administration;

import fr.gouv.sitde.face.transverse.entities.Adresse;
import fr.gouv.sitde.face.transverse.entities.Collectivite;

public interface AdresseService {

    /**
     * Recherche d'une adresse par id.
     *
     * @param idAdresse
     *            the id adresse
     * @return the adresse
     */
    Adresse rechercherAdresseParId(Long idAdresse);

    /**
     * Modifier adresse.
     *
     * @param adresse
     *            the adresse
     * @return the adresse
     */
    Adresse modifierAdresse(Adresse adresse);

    /**
     * Creer adresse.
     *
     * @param adresse
     *            the adresse
     * @return the adresse
     */
    Adresse creerAdresse(Adresse adresse);

    /**
     * Rechercher adresse par departement.
     *
     * @param idDepartement
     *            the id departement
     * @return the adresse
     */
    Adresse rechercherAdresseParDepartement(Integer idDepartement);

    /**
     * Enregistrer adresse pour departement.
     *
     * @param idDepartement
     *            the id departement
     * @param adresse
     *            the adresse
     * @return the adresse
     */
    Adresse enregistrerAdressePourDepartement(Integer idDepartement, Adresse adresse);

    /**
     * Gerer adresse pour collectivite.
     *
     * @param collectivite
     *            the collectivite
     */
    void gererAdressePourCollectivite(Collectivite collectivite);

}

/**
 *
 */
package fr.gouv.sitde.face.domaine.service.administration.impl;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService;
import fr.gouv.sitde.face.domaine.service.document.DocumentService;
import fr.gouv.sitde.face.transverse.document.OrigineDocumentDto;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;

/**
 * The Class CollectiviteOrigineServiceImpl.
 *
 * @author a453029
 */
@Named
public class CollectiviteOrigineServiceImpl implements CollectiviteOrigineService {

    @Inject
    private CollectiviteRepository collectiviteRepository;

    @Inject
    private DocumentService documentService;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherIdCollectiviteParDotationCollectivite(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDotationCollectivite(Long idDotationCollectivite) {
        return this.collectiviteRepository.rechercherIdCollectiviteParDotationCollectivite(idDotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherIdCollectiviteParDossierSubvention(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDossierSubvention(Long idDossierSubvention) {
        return this.collectiviteRepository.rechercherIdCollectiviteParDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherIdCollectiviteParDemandeSubvention(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeSubvention(Long idDemandeSubvention) {
        return this.collectiviteRepository.rechercherIdCollectiviteParDemandeSubvention(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherIdCollectiviteParDemandeProlongation(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeProlongation(Long idDemandeProlongation) {
        return this.collectiviteRepository.rechercherIdCollectiviteParDemandeProlongation(idDemandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherIdCollectiviteParDemandePaiement(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandePaiement(Long idDemandePaiement) {
        return this.collectiviteRepository.rechercherIdCollectiviteParDemandePaiement(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.administration.CollectiviteOrigineService#rechercherCollectivitesOrigineParDocument(java.lang.Long)
     */
    @Override
    public OrigineDocumentDto rechercherCollectivitesOrigineParDocument(Long idDocument) {

        FamilleDocument familleDoc = this.documentService.rechercherFamilleDocumentParIdDocument(idDocument);
        OrigineDocumentDto origineDoc = new OrigineDocumentDto();

        if (familleDoc == null) {
            return origineDoc;
        }

        FamilleDocumentEnum familleDocEnum = FamilleDocumentEnum.rechercherEnumParIdCodifie(familleDoc.getIdCodifie());

        switch (familleDocEnum) {
            case DOC_DEMANDE_SUBVENTION:
                origineDoc.getListeIdCollectivitesOrigine()
                        .add(this.collectiviteRepository.rechercherIdCollectiviteParDocumentDemandeSubvention(idDocument));
                break;
            case DOC_DEMANDE_PROLONGATION:
                origineDoc.getListeIdCollectivitesOrigine()
                        .add(this.collectiviteRepository.rechercherIdCollectiviteParDocumentDemandeProlongation(idDocument));
                break;
            case DOC_DEMANDE_PAIEMENT:
                origineDoc.getListeIdCollectivitesOrigine()
                        .add(this.collectiviteRepository.rechercherIdCollectiviteParDocumentDemandePaiement(idDocument));
                break;

            case DOC_COURRIER_ANNUEL_DEPARTEMENT:
                origineDoc.setListeIdCollectivitesOrigine(
                        this.collectiviteRepository.rechercherIdCollectivitesParDocumentCourrierDepartement(idDocument));
                break;

            case DOC_LIGNE_DOTATION_DEPARTEMENT:
                origineDoc.setListeIdCollectivitesOrigine(
                        this.collectiviteRepository.rechercherIdCollectivitesParDocumentLigneDotationDepartement(idDocument));
                break;
            case DOC_MODELE:
                origineDoc.setDocSansLienCollectivite(true);
        }

        return origineDoc;
    }

}

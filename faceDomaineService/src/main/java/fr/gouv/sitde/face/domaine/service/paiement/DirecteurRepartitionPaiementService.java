/**
 *
 */
package fr.gouv.sitde.face.domaine.service.paiement;

import java.math.BigDecimal;

/**
 * @author A754839
 *
 */
public interface DirecteurRepartitionPaiementService {

    /**
     * @param demandePaiement
     */
    void realiserRepartition(Long idDemandePaiement);

    /**
     * Calculer reste avance paiement pour dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the big decimal
     */
    BigDecimal calculerResteAvancePaiementPourDossier(Long idDossier);

    /**
     * Calculer reste acompte paiement pour dossier.
     *
     * @param idDossier
     *            the id dossier
     * @return the big decimal
     */
    BigDecimal calculerResteAcomptePaiementPourDossier(Long idDossier);

}

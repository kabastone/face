/**
 *
 */
package fr.gouv.sitde.face.domaine.service.dotation;

import fr.gouv.sitde.face.transverse.entities.DotationProgramme;

/**
 * Classe métier de la dotation programme.
 *
 * @author a702709
 */
public interface DotationProgrammeService {

    /**
     * Rechercher dotation programme par annee en cours.
     *
     * @param codeProgramme
     *            the id programme
     * @return the dotation programme
     */
    DotationProgramme rechercherDotationProgrammeParAnneeEnCours(String codeProgramme);

    /**
     * Modifier dotation programme par annee en cours.
     *
     * @param dotationProgramme
     *            the dotation programme
     * @return the dotation programme
     */
    DotationProgramme modifierDotationProgrammeParAnneeEnCours(DotationProgramme dotationProgramme);

}

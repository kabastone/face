/**
 *
 */
package fr.gouv.sitde.face.domaine.service.referentiel.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository;
import fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class SousProgrammeServiceImpl.
 *
 * @author atos
 */
@Named
public class SousProgrammeServiceImpl implements SousProgrammeService {

    /** The sous programme repository. */
    @Inject
    private SousProgrammeRepository sousProgrammeRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherTousSousProgrammes()
     */
    @Override
    public List<SousProgramme> rechercherTousSousProgrammes() {
        return this.sousProgrammeRepository.rechercherTousSousProgrammes();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammesPourRenoncementDotation(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite) {
        return this.sousProgrammeRepository.rechercherSousProgrammesDeTravauxParCollectivite(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammesDeProjetParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite) {
        return this.sousProgrammeRepository.rechercherSousProgrammesDeProjetParCollectivite(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammesDeProjetParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeProjet() {
        return this.sousProgrammeRepository.rechercherSousProgrammesDeProjet();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammesPourRenoncementDotation()
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesPourLigneDotationDepartementale() {
        Integer anneeEnCours = this.timeOperationTransverse.getAnneeCourante();
        return this.sousProgrammeRepository.rechercherSousProgrammesPourLigneDotationDepartementale(anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherParId(java.lang.Integer)
     */
    @Override
    public SousProgramme rechercherParId(Integer idSousProgramme) {
        return this.sousProgrammeRepository.findById(idSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammeParAbreviation(java.lang.String)
     */
    @Override
    public SousProgramme rechercherSousProgrammeParAbreviation(String abreviation) {
        return this.sousProgrammeRepository.rechercherSousProgrammeParAbreviation(abreviation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammesParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesParCollectivite(Long idCollectivite) {
        List<SousProgramme> sousProgrammes = this.sousProgrammeRepository.rechercherSousProgrammesDeTravauxParCollectivite(idCollectivite);
        sousProgrammes.addAll(this.sousProgrammeRepository.rechercherSousProgrammesDeProjet());
        return sousProgrammes;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherSousProgrammeParId(java.lang.Integer)
     */
    @Override
    public SousProgramme rechercherSousProgrammeParId(Integer idSousProgramme) {
        return this.sousProgrammeRepository.findById(idSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#rechercherPourDotationDepartement()
     */
    @Override
    public List<SousProgramme> rechercherPourDotationDepartement() {
        return this.sousProgrammeRepository.rechercherPourDotationDepartement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domaine.service.referentiel.SousProgrammeService#estDeProjetParId(java.lang.Integer)
     */
    @Override
    public boolean estDeProjetParId(Integer id) {
        return this.rechercherParId(id).isDeProjet();
    }
}

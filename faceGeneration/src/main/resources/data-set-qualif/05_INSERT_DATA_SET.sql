-- Insertion de jeu de test

SET client_encoding = 'UTF8';



--------------------------------------------
----------- Anomalie cas de test ---------------------
--------------------------------------------
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_SUB_0001','Fausse ano1 de subvention', false);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_SUB_0002','Fausse ano2 de subvention', false);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_PAI_0001','Fausse ano1 de paiement', true);
insert into r_type_anomalie(tan_code, tan_libelle, tan_est_pour_demande_paiement) values ('ANO_PAI_0002','Fausse ano2 de paiement', true);
	
--------------------------------------------
----------- Utilisateur qualif1 ---------------------
--------------------------------------------

INSERT INTO utilisateur(
	uti_id, uti_cerbere_id, uti_nom, uti_prenom, uti_email)
	VALUES (nextval('utilisateur_uti_id_seq'), '0000', 'EN_ATTENTE', 'EN_ATTENTE',  'qualif1@exemple.fr');	
	

	
--------------------------------------------
----------- Collectivité ---------------------
--------------------------------------------
INSERT INTO collectivite(
	col_id, col_siret, col_nom_court, col_nom_long, 
	col_num_collectivite, col_receveur_num_codique, col_receveur_nom, col_dep_id)
	VALUES (nextval('collectivite_col_id_seq'), '78012998703591', 'AIN', 'SIE de l''AIN', 
	'1', '028011', 'receveur nom col 1', (SELECT dep_id FROM face.departement where dep_code = '01'));
	
	
--------------------------------------------
---------- Adresses email collectivite -----
--------------------------------------------
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.ain1@qualif.fr', (SELECT col_id FROM collectivite where col_siret='78012998703591'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.ain2@qualif.fr', (SELECT col_id FROM collectivite where col_siret='78012998703591'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.ain3@qualif.fr', (SELECT col_id FROM collectivite where col_siret='78012998703591'));


--------------------------------------------
----------- Droit Agent ---------------------
--------------------------------------------	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	

--------------------------------------------
----------- Collectivité --------------------
--------------------------------------------
INSERT INTO collectivite(
	col_id, col_siret, col_nom_court, col_nom_long, 
	col_num_collectivite, col_receveur_num_codique, col_receveur_nom, col_dep_id)
	VALUES (nextval('collectivite_col_id_seq'), '54053052655762', 'ARS LAQUENEXY', 'Commune d''ARS LAQUENEXY', 
	'2', '028012', 'receveur nom col 2', (SELECT dep_id FROM face.departement where dep_code = '57'));
	

--------------------------------------------
---------- Adresses email collectivite -----
--------------------------------------------
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.laquenexy1@qualif.fr', (SELECT col_id FROM collectivite where col_siret='54053052655762'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.laquenexy2@qualif.fr', (SELECT col_id FROM collectivite where col_siret='54053052655762'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.laquenexy3@qualif.fr', (SELECT col_id FROM collectivite where col_siret='54053052655762'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.laquenexy4@qualif.fr', (SELECT col_id FROM collectivite where col_siret='54053052655762'));
	

--------------------------------------------
----------- Droit Agent ---------------------
--------------------------------------------	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	
	
--------------------------------------------
----------- Collectivité --------------------
--------------------------------------------
INSERT INTO collectivite(
	col_id, col_siret, col_nom_court, col_nom_long, 
	col_num_collectivite, col_receveur_num_codique, col_receveur_nom, col_dep_id)
	VALUES (nextval('collectivite_col_id_seq'), '33981502848593', 'ANCERVILLE', 'Commune de ANCERVILLE', 
	'3', '028013', 'receveur nom col 3', (SELECT dep_id FROM face.departement where dep_code = '57'));
	
	
--------------------------------------------
---------- Adresses email collectivite -----
--------------------------------------------
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.ancerville1@qualif.fr', (SELECT col_id FROM collectivite where col_siret='33981502848593'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.ancerville2@qualif.fr', (SELECT col_id FROM collectivite where col_siret='33981502848593'));
	

--------------------------------------------
----------- Droit Agent ---------------------
--------------------------------------------	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	
--------------------------------------------
----------- Utilisateur qualif2 ---------------------
--------------------------------------------


INSERT INTO utilisateur(
	uti_id, uti_cerbere_id, uti_nom, uti_prenom, uti_email)
	VALUES (nextval('utilisateur_uti_id_seq'), '0000', 'EN_ATTENTE', 'EN_ATTENTE',  'qualif2@exemple.fr');	
	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	
	
INSERT INTO collectivite(
	col_id, col_siret, col_nom_court, col_nom_long, 
	col_num_collectivite, col_receveur_num_codique, col_receveur_nom, col_dep_id)
	VALUES (nextval('collectivite_col_id_seq'), '76239330787980', 'BIONVILLE SUR NIED', 'Commune de BIONVILLE SUR NIED', 
	'4', '028014', 'receveur nom col 4', (SELECT dep_id FROM face.departement where dep_code = '57'));
	

INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.bionville1@qualif.fr', (SELECT col_id FROM collectivite where col_siret='76239330787980'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.bionville2@qualif.fr', (SELECT col_id FROM collectivite where col_siret='76239330787980'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.bionville3@qualif.fr', (SELECT col_id FROM collectivite where col_siret='76239330787980'));
	
	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	

INSERT INTO collectivite(
	col_id, col_siret, col_nom_court, col_nom_long, 
	col_num_collectivite, col_receveur_num_codique, col_receveur_nom, col_dep_id, col_etat)
	VALUES (nextval('collectivite_col_id_seq'), '65639904890966', 'CHAILLY LES ENNERY', 'Commune de CHAILLY LES ENNERY', 
	'5', '028015', 'receveur nom col 5', (SELECT dep_id FROM face.departement where dep_code = '57'), 'FERMEE');
	
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.chailly1@qualif.fr', (SELECT col_id FROM collectivite where col_siret='65639904890966'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.chailly2@qualif.fr', (SELECT col_id FROM collectivite where col_siret='65639904890966'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.chailly3@qualif.fr', (SELECT col_id FROM collectivite where col_siret='65639904890966'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.chailly4@qualif.fr', (SELECT col_id FROM collectivite where col_siret='65639904890966'));
INSERT INTO adresse_email_collectivite(ade_adresse, ade_col_id)
	VALUES('col.chailly5@qualif.fr', (SELECT col_id FROM collectivite where col_siret='65639904890966'));
	
INSERT INTO droit_agent(
	dag_id, dag_est_admin, dag_uti_id, dag_col_id)
	VALUES (nextval('droit_agent_dag_id_seq'), true, currval('utilisateur_uti_id_seq'), currval('collectivite_col_id_seq'));
	

--Autre commune du 57 au besoin 	
--Commune de CHATEL SAINT GERMAIN
--Commune de CHEMINOT
--Commune de COIN SUR SEILLE
--Commune de Courcelles sur Nied
--Commune de FLETRANGE
--Commune de Fleury
--Commune de Guinglange
--Commune de HAYES
--Commune de LESSY
--Commune de Lorry les Metz
--Commune de LUPPY
--Commune de MANY
--Commune de MARSILLY
--Commune de MECLEUVES
--Commune de MONTOY FLANVILLE
--Commune de NORROY LE VENEUR
--Commune de OGY
--Commune de PANGE
--Commune de POMMERIEUX
--Commune de PONTPIERRE
--Commune de RETONFEY
--Commune de SAULNY
--Commune de ST PRIVAT LA MONTAGNE
--Commune de Thimonville
--Commune de Vany
--Commune de VITTONCOURT
--Commune de VULMONT

--------------------------------------------
----------- Année 2016 ---------------------
--------------------------------------------
----programme principal - 793
-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2016', '369600000', (select pro_id from r_programme where pro_code_numerique = '793'));

	
-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '183800000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '200000', 'EXCEPTIONNELLE', current_date  - interval '4 year' , currval('dotation_departement_dde_id_seq'));
	

---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));

	
---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2016AP001028011', '11111', 
	current_date - interval ' 3 year', current_date  - interval ' 1 month'  , current_date  - interval ' 2 year' + interval ' 3 month', 
	current_date - interval ' 3 year',
	currval('dotation_collectivite_dco_id_seq'));
	
INSERT INTO demande_prolongation(
	dpr_id, dpr_date_demande, dpr_date_achevement_demandee, dpr_motif_refus, 
	dpr_edpr_id, dpr_dos_id)
	VALUES (nextval('demande_prolongation_dpr_id_seq'), current_date  - interval ' 1 year', '2020-01-01 00:00:00' , 'je suis en retard', 
	(select edpr_ID from r_etat_dem_prolongation where edpr_code = 'DEMANDEE'), currval('dossier_subvention_dos_id_seq'));

	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, 
	dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '200000', 
	current_date - interval ' 3 year' + interval '2 month', current_date + interval '1 month',
	current_date - interval ' 3 year' + interval '6 month', '80', currval('dossier_subvention_dos_id_seq'), '1', false);


	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, 
	dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '100000', 
	current_date - interval ' 3 year' + interval '2 month', current_date + interval '1 month',
	current_date - interval ' 3 year' + interval '6 month', '80', currval('dossier_subvention_dos_id_seq'), '1', true);
		
INSERT INTO anomalie (ano_id, ano_est_corrigee, ano_problematique, ano_reponse, ano_tan_id, ano_dsu_id, ano_dpa_id) 
VALUES 
(nextval('anomalie_ano_id_seq'), false, 'Pas assez de finance', 'Seuil maximal atteint pour votre cas', (select tan_id from r_type_anomalie where TAN_CODE = 'ANO_CHO_SUB'), currval('demande_subvention_dsu_id_seq'), null), 
(nextval('anomalie_ano_id_seq'), true, 'Pas assez de finance', 'Seuil maximal atteint pour votre cas', (select tan_id from r_type_anomalie where TAN_CODE = 'ANO_SUB_0000'), currval('demande_subvention_dsu_id_seq'), null);

---département de Moselle	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '57'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '400000', 'ANNUELLE', current_date, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300000', 'EXCEPTIONNELLE', current_date, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '-100000', 'RENONCEMENT', current_date, currval('dotation_departement_dde_id_seq'));
	
--dep 57 - Commune d'ARS LAQUENEXY
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '2'), currval('dotation_departement_dde_id_seq'));


---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2016AP057028012', '11112', 
	current_date, current_date  - interval '3 year' , current_date  + interval '1 year', current_date - interval '3 year',
	currval('dotation_collectivite_dco_id_seq'));
	

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '100000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), false);
	
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '10000', 'AVANCE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),1);


INSERT INTO anomalie (ano_id, ano_est_corrigee, ano_problematique, ano_reponse, ano_tan_id, ano_dsu_id, ano_dpa_id) 
VALUES 
(nextval('anomalie_ano_id_seq'), false, 'Pas assez de finance', 'Seuil maximal atteint pour votre cas', (select tan_id from r_type_anomalie where TAN_CODE = 'ANO_CHO_PAI'), null, currval('demande_paiement_dpa_id_seq')), 
(nextval('anomalie_ano_id_seq'), false, 'Pas assez de finance', 'Seuil maximal atteint pour votre cas', (select tan_id from r_type_anomalie where TAN_CODE = 'ANO_PAI_0000'), null, currval('demande_paiement_dpa_id_seq'));

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '15000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),2);
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '50000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '30000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),3);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000003', '70000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'), 
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '85000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),4);	
	
	
--dep 57 - Commune de ANCERVILLE
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '200000', (select col_id from collectivite where col_num_collectivite = '3'), currval('dotation_departement_dde_id_seq'));

	
--dep 57 - Commune de BIONVILLE SUR NIED
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '100000', (select col_id from collectivite where col_num_collectivite = '4'), currval('dotation_departement_dde_id_seq'));
-----------
--******---
-----------
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '45900000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	

-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '200000', 'EXCEPTIONNELLE', current_date  - interval '4 year' , currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '400000', 'EXCEPTIONNELLE', current_date  - interval '4 year' , currval('dotation_departement_dde_id_seq'));
		

---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));


---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2016AP057028012-2', '11113', 
	current_date, current_date  + interval '2 month' , current_date  - interval '1 year', current_date - interval '1 year',
	currval('dotation_collectivite_dco_id_seq'));
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'DEMANDEE'), false);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'QUALIFIEE'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000003', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ACCORDEE'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000004', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'CONTROLEE'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000005', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'EN_ATTENTE_TRANSFERT_MAN'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000006', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'EN_ATTENTE_TRANSFERT'), true);	
	
INSERT INTO document(doc_id, doc_date_televersement, doc_nom_fichier_sans_ext, doc_extension_fichier, doc_tdo_id_codifie, doc_dsu_id)
values (nextval('document_doc_id_seq') , current_date, 'fichier_test_1','pdf',(select tdo_id_codifie from R_TYPE_DOCUMENT where tdo_code = 'ETAT_PREVISIONNEL_PDF'), currval('demande_subvention_dsu_id_seq'));
	
INSERT INTO document(doc_id, doc_date_televersement, doc_nom_fichier_sans_ext, doc_extension_fichier, doc_tdo_id_codifie, doc_dsu_id)
values (nextval('document_doc_id_seq'), current_date,'fichier_test_2','pdf',(select tdo_id_codifie from R_TYPE_DOCUMENT where tdo_code = 'FICHE_SIRET'), currval('demande_subvention_dsu_id_seq'));
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000007', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'EN_COURS_TRANSFERT'), true);	
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000008', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'TRANSFEREE'), true);	
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000009', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'APPROUVEE'), true);	
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000010', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'INCOHERENCE_CHORUS'), true);	
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000011', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ANOMALIE_DETECTEE'), true);	
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000012', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ANOMALIE_SIGNALEE'), true);	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000013', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'CORRIGEE'), true);	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000014', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'REFUSEE'), true);	
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000015', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);	
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000016', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'CLOTUREE'), true);	
	
		
		
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '54500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '05' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	

-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '500000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));


---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));

	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '39000000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '06' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	


-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '500000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));


---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '42000000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '07' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
	

-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '500000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));
	

---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));


---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2016AP057028012-3', '11114', 
	current_date, current_date  - interval '4 month' , current_date  - interval '2 year', current_date - interval '2 year',
	currval('dotation_collectivite_dco_id_seq'));
		
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_demande, dsu_date_attribution, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '1000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'DEMANDEE'), false);

INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1400000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '08' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '09' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '2500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '10' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

----programme principal - 794

-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2016', '7400000', (select pro_id from r_programme where pro_code_numerique = '794'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '200000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '02' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '400000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1400000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
--------------------------------------------
----------- Année 2017 ---------------------
--------------------------------------------
----programme principal - 793
-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2017', '369700000', (select pro_id from r_programme where pro_code_numerique = '793'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '171200000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
	

-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '500000', 'ANNUELLE', current_date - interval '4 year', currval('dotation_departement_dde_id_seq'));
	

---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '44500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '05' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '50900000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '06' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43000000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '07' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '08' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '490000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '09' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '2600000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '10' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

----programme principal - 794

-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2017', '7500000', (select pro_id from r_programme where pro_code_numerique = '794'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '180000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '02' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '410000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));	
	
	
--------------------------------------------
----------- Année 2018 ---------------------
--------------------------------------------
----programme principal - 793
-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2018', '369800000', (select pro_id from r_programme where pro_code_numerique = '793'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '171300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43400000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '44600000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '05' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '50800000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '06' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43100000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '07' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '08' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '480000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '09' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '2500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '10' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

----programme principal - 794

-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2018', '7900000', (select pro_id from r_programme where pro_code_numerique = '794'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '190000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '02' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '420000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '794')), 
	currval('dotation_programme_dpr_id_seq'));	
	
	
--------------------------------------------
----------- Année 2019 ---------------------
--------------------------------------------
----programme principal - 793
-- dotation_programme
INSERT INTO dotation_programme(
	dpr_id, dpr_annee, dpr_montant, dpr_pro_id)
	VALUES (nextval('dotation_programme_dpr_id_seq'), '2019', '369800000', (select pro_id from r_programme where pro_code_numerique = '793'));

-- dotation_sous_programme	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '171300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '03' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
-- dotation_departement pour ssp '03'	
---département de l'Ain
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100000', 'ANNUELLE', current_date, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '200000', 'EXCEPTIONNELLE', current_date, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '50000', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));

---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP001028011', '11115', 
	current_date, current_date  + interval '1 year' , current_date  + interval '3 year', current_date,
	currval('dotation_collectivite_dco_id_seq'));
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '200000', current_date, current_date - interval '1 month', current_date, '80', 
	currval('dossier_subvention_dos_id_seq'), '1', false);
	

INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP001028011-2', '11116', 
	current_date, current_date  + interval '3 year' , current_date  - interval '2 month', current_date,
	currval('dotation_collectivite_dco_id_seq'));
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '100000', current_date, current_date - interval '1 month', current_date, '80', 
	currval('dossier_subvention_dos_id_seq'), '1', false);
		
---département de Moselle	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '57'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '400000', 'ANNUELLE', current_date, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300000', 'EXCEPTIONNELLE', current_date, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '-100000', 'RENONCEMENT', current_date, currval('dotation_departement_dde_id_seq'));
	
--dep 57 - Commune d'ARS LAQUENEXY
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '2'), currval('dotation_departement_dde_id_seq'));


---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP057028012-2', '11117', 
	current_date, current_date  + interval '4 year' , current_date  + interval '1 year', current_date,
	currval('dotation_collectivite_dco_id_seq'));
	
INSERT INTO demande_prolongation(
	dpr_id, dpr_date_demande, dpr_date_achevement_demandee, dpr_motif_refus, 
	dpr_edpr_id, dpr_dos_id)
	VALUES (nextval('demande_prolongation_dpr_id_seq'), current_date, current_date  + interval '4 year' , 'je suis en retard', 
	(select edpr_ID from r_etat_dem_prolongation where edpr_code = 'VALIDEE'), currval('dossier_subvention_dos_id_seq'));


INSERT INTO demande_prolongation(
	dpr_id, dpr_date_demande, dpr_date_achevement_demandee, dpr_motif_refus, 
	dpr_edpr_id, dpr_dos_id)
	VALUES (nextval('demande_prolongation_dpr_id_seq'), current_date, current_date  + interval '5 year' , 'je suis en retard', 
	(select edpr_ID from r_etat_dem_prolongation where edpr_code = 'DEMANDEE'), currval('dossier_subvention_dos_id_seq'));
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '100000', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), false);
	
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '10000', 'AVANCE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),1);
	


INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '15000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),2);


INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '50000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '30000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),3);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000003', '70000', current_date, current_date, current_date, '80', currval('dossier_subvention_dos_id_seq'), 
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '85000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),4);	

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000004', '100000', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'DEMANDEE'), true);
	
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000005', '1300000', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'DEMANDEE'), true);
	
	
--dep 57 - Commune de ANCERVILLE
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '200000', (select col_id from collectivite where col_num_collectivite = '3'), currval('dotation_departement_dde_id_seq'));

	---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP057028013', '11118', 
	current_date, current_date  + interval '4 year' , current_date  + interval '1 year', current_date,
	currval('dotation_collectivite_dco_id_seq'));

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '62500', current_date - interval '1 month', current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), false);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '37500', current_date - interval '25 day', current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'REFUSEE'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000003', '25000', current_date - interval '5 day', current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);
	
INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000004', '70500', current_date - interval '25 day', current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '12500', 'AVANCE', current_date - interval '30 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'REFUSEE'), currval('dossier_subvention_dos_id_seq'),1);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '12500', 'AVANCE', current_date - interval '29 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'DEMANDEE'), currval('dossier_subvention_dos_id_seq'),2);
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '4000', 'ACOMPTE', current_date - interval '28 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'VERSEE'), currval('dossier_subvention_dos_id_seq'),3);
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '800', 'ACOMPTE', current_date - interval '27 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'QUALIFIEE'), currval('dossier_subvention_dos_id_seq'),4);
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '1000', 'ACOMPTE', current_date - interval '26 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'ACCORDEE'), currval('dossier_subvention_dos_id_seq'),5);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '500', 'ACOMPTE', current_date - interval '25 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'CONTROLEE'), currval('dossier_subvention_dos_id_seq'),6);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '500', 'ACOMPTE', current_date - interval '24 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'EN_ATTENTE_TRANSFERT'), currval('dossier_subvention_dos_id_seq'),7);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '2000', 'ACOMPTE', current_date - interval '23 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'EN_COURS_TRANSFERT_1'), currval('dossier_subvention_dos_id_seq'),8);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '4500', 'ACOMPTE', current_date - interval '22 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'EN_COURS_TRANSFERT_2'), currval('dossier_subvention_dos_id_seq'),9);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '20000', 'ACOMPTE', current_date - interval '21 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'EN_COURS_TRANSFERT_FIN'), currval('dossier_subvention_dos_id_seq'),10);
		
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '55000', 'ACOMPTE', current_date - interval '20 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'ANOMALIE_DETECTEE'), currval('dossier_subvention_dos_id_seq'),11);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '55000', 'ACOMPTE', current_date - interval '19 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'ANOMALIE_SIGNALEE'), currval('dossier_subvention_dos_id_seq'),12);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '1000', 'ACOMPTE', current_date - interval '18 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'CORRIGEE'), currval('dossier_subvention_dos_id_seq'),13);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '1000', 'SOLDE', current_date - interval '17 day', '80', 
	(select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),14);
	
--dep 57 - Commune de BIONVILLE SUR NIED
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '200000', (select col_id from collectivite where col_num_collectivite = '4'), currval('dotation_departement_dde_id_seq'));
	
---subvention 
INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP057028014', '11119', 
	current_date, current_date  + interval '4 year' , current_date  + interval '1 year', current_date,
	currval('dotation_collectivite_dco_id_seq'));

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '62500', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), false);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000002', '37500', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000003', '25000', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), true);
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '12500', 'AVANCE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),1);
	
INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '40000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),2);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '55000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),3);

INSERT INTO demande_paiement(
	dpa_id, dpa_montant_trx_realises, dpa_type, dpa_date_demande, dpa_taux_aide, dpa_epa_id, dpa_dos_id, dpa_num_ordre)
	VALUES (nextval('demande_paiement_dpa_id_seq'), '1000', 'ACOMPTE', current_date, '80', (select EDPA_ID from r_etat_dem_paiement where EDPA_CODE = 'TRANSFEREE'), currval('dossier_subvention_dos_id_seq'),4);

INSERT INTO dossier_subvention(
	dos_id, dos_num_dossier, dos_chorus_num_ej, 
	DOS_DATE_ETAT, dos_date_echeance_achevement, dos_date_echeance_lancement, dos_date_creation_dossier,
	dos_dco_id)
	VALUES (nextval('dossier_subvention_dos_id_seq'), '2019AP0570280142', '11120', 
	current_date, current_date  + interval '4 year' , current_date  + interval '1 year', current_date,
	currval('dotation_collectivite_dco_id_seq'));

INSERT INTO demande_subvention(
	dsu_id, dsu_chorus_num_ligne_legacy, dsu_montant_trx_eligible, dsu_date_etat, dsu_date_attribution, dsu_date_demande, dsu_taux_aide, dsu_dos_id, dsu_eds_id, dsu_est_demande_complementaire)
	VALUES (nextval('demande_subvention_dsu_id_seq'), '0000000001', '50000', current_date, current_date + interval '1 month', current_date, '80', currval('dossier_subvention_dos_id_seq'),
	(select EDS_ID from R_ETAT_DEM_SUBVENTION where eds_code = 'ATTRIBUEE'), false);
	
-----
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43400000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '04' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
		

-- dotation_departement	pour ssp '04'		
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100000', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300000', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500000', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));


---collectivite de l'ain
INSERT INTO dotation_collectivite(
	dco_id, dco_montant, dco_col_id, dco_dde_id)
	VALUES (nextval('dotation_collectivite_dco_id_seq'), '300000', (select col_id from collectivite where col_num_collectivite = '1'), currval('dotation_departement_dde_id_seq'));

	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '02'), currval('dotation_sous_programme_dsp_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '44600000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '05' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100005', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300005', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500005', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '50800000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '06' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	
	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100006', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300006', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500006', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '43100000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '07' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));
	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100007', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300007', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500007', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '1300000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '08' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));	

	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100008', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300008', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500008', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '480000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '09' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

	
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100009', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300009', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500009', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));
	
INSERT INTO dotation_sous_programme(
	dsp_id, dsp_montant, dsp_spr_id, dsp_dpr_id)
	VALUES (nextval('dotation_sous_programme_dsp_id_seq'), '2500000', 
	(select spr_id from r_sous_programme where spr_num_sous_action = '10' AND spr_pro_id = (select pro_id from r_programme where pro_code_numerique = '793')), 
	currval('dotation_programme_dpr_id_seq'));

		
INSERT INTO dotation_departement(
	dde_id, dde_dep_id, dde_dsp_id)
	VALUES (nextval('dotation_departement_dde_id_seq'), (select dep_id from departement where dep_code = '01'), currval('dotation_sous_programme_dsp_id_seq'));

	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 1, '100010', 'ANNUELLE', null, currval('dotation_departement_dde_id_seq'));

INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 2, '300010', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));	
	
INSERT INTO dotation_departement_ligne(
	ldd_id, ldd_num_ordre, ldd_montant, ldd_type, ldd_date_envoi, ldd_dde_id)
	VALUES (nextval('dotation_departement_ligne_ldd_id_seq'), 3, '500010', 'EXCEPTIONNELLE', null, currval('dotation_departement_dde_id_seq'));



-- Fin de transaction
COMMIT;



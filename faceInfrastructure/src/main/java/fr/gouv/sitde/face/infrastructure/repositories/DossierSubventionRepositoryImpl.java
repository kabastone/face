/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository;
import fr.gouv.sitde.face.persistance.repositories.DossierSubventionJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandePaiementQo;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheDossierPourDemandeSubventionQo;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service d'acces au repository JPA DossierSubvention.
 *
 * @author Atos
 *
 */
@Component
public class DossierSubventionRepositoryImpl implements DossierSubventionRepository {

    /** The dossier subvention jpa repository. */
    @Inject
    private DossierSubventionJpaRepository dossierSubventionJpaRepository;

    @Inject
    private TimeOperationTransverse timeUtils;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#findDossierSubventionById(java.lang.Long)
     */
    @Override
    public DossierSubvention findDossierSubventionById(Long idDossierSubvention) {
        return this.dossierSubventionJpaRepository.findDossierSubventionById(idDossierSubvention).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#rechercherDossiersParCriteres(fr.gouv.sitde.face.transverse.queryobject.
     * CritereRechercheDossierPourDemandeSubventionQo)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersParCriteres(CritereRechercheDossierPourDemandeSubventionQo criteresRecherche) {
        return this.dossierSubventionJpaRepository.rechercherDossiersParCriteres(criteresRecherche);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#findDossierSubventionParNumero(java.lang.String)
     */
    @Override
    public DossierSubvention findDossierSubventionParNumero(String numeroDossier) {
        return this.dossierSubventionJpaRepository.rechercherDossierParNumero(numeroDossier).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#findDossierSubventionPourMailRelance(java.time.LocalDateTime)
     */
    @Override
    public List<DossierSubvention> findDossierSubventionPourMailRelance(LocalDateTime dateRelanceMail) {
        return this.dossierSubventionJpaRepository.rechercherDossiersPourMailRelance(dateRelanceMail);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#findDossierSubventionPourMailExpiration(java.time.LocalDateTime)
     */
    @Override
    public List<DossierSubvention> findDossierSubventionPourMailExpiration(LocalDateTime dateExpirationMail) {
        return this.dossierSubventionJpaRepository.rechercherDossiersPourMailExpiration(dateExpirationMail);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#enregistrer(fr.gouv.sitde.face.transverse.entities.DossierSubvention)
     */
    @Override
    public DossierSubvention enregistrer(DossierSubvention dossierSubvention) {
        return this.dossierSubventionJpaRepository.saveAndFlush(dossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#isNumeroDossierUnique(java.lang.String)
     */
    @Override
    public boolean isNumeroDossierUnique(Long idDossier, String numDossier) {
        return this.dossierSubventionJpaRepository.countDossierPourNumeroDossier(idDossier, numDossier) == 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#findDossierSubventionByIdChorusAe(java.lang.Long)
     */
    @Override
    public DossierSubvention findDossierSubventionByIdChorusAe(Long idAe) {
        return this.dossierSubventionJpaRepository.rechercherDossierParIdChorusAe(idAe).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#supprimerDossierSubventionParId(java.lang.Long)
     */
    @Override
    public void supprimerDossierSubventionParId(Long idDossier) {
        this.dossierSubventionJpaRepository.supprimerDossierSubventionParId(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#rechercherDossierParAnneeCollectiviteEtSousProgramme(int,
     * java.lang.Long, java.lang.Integer)
     */
    @Override
    public List<DossierSubvention> rechercherDossierParAnneeCollectiviteEtSousProgramme(int annee, Long idCollectivite, Integer idSousProgramme) {
        return this.dossierSubventionJpaRepository.rechercherDossierParAnneeCollectiviteEtSousProgramme(annee, idCollectivite, idSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#rechercherDossiersParCriteresPaiement(fr.gouv.sitde.face.transverse.
     * queryobject.CritereRechercheDossierPourDemandePaiementQo)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersParCriteresPaiement(CritereRechercheDossierPourDemandePaiementQo criteresRecherche) {
        return this.dossierSubventionJpaRepository.rechercherDossiersParCriteresPaiement(criteresRecherche);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DossierSubventionRepository#recupererPlusHautNumeroOrdreDossier(fr.gouv.sitde.face.transverse.
     * entities.DossierSubvention)
     */
    @Override
    public List<String> recupererListeNumeroDossierParDotationCollectivite(DossierSubvention dossierSubvention) {
        return this.dossierSubventionJpaRepository
                .recupererToutNumeroDossierParDotationCollectivite(dossierSubvention.getDotationCollectivite().getId());
    }

    /**
     * Rechercher dossier subvention cadencement defaut.
     *
     * @return the list
     */
    @Override
    public List<DossierSubvention> rechercherDossierSubventionCadencementDefaut(String codeProgramme) {

        return this.dossierSubventionJpaRepository.recherDossierSubventionCadencementDefaut(codeProgramme);
    }

    @Override
    public List<DossierSubvention> rechercherDossierSubventionEnRetard(String codeProgramme) {

        return this.dossierSubventionJpaRepository.rechercherDossierSubventionEnRetard(codeProgramme);
    }

    @Override
    public List<DossierSubvention> rechercherDossierSubventionPourMailEngagementTravaux() {
        return this.dossierSubventionJpaRepository.rechercherDossierSubventionPourMailEngagementTravaux(this.timeUtils.getAnneeCourante());
    }

}

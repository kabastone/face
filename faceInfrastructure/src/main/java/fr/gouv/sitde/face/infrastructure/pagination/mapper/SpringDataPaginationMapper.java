/**
 *
 */
package fr.gouv.sitde.face.infrastructure.pagination.mapper;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * Mapper entre objets de pagination de la couche dommaine et objets de pagination Spring-data.
 *
 * @author a453029
 *
 */
public final class SpringDataPaginationMapper {

    /**
     * La classe SpringDataPaginationMapper est une classe utilitaire qui ne contient que des méthodes statiques. Elle n'a pas à être instanciée.
     */
    private SpringDataPaginationMapper() {
        // Constructeur privé.
    }

    /**
     * Récupérer l'objet pageRequest de Spring-data correspond à la pageDemande.
     *
     * @param pageDemande
     *            the page demande
     * @return the pageRequest from pageDemande
     */
    public static PageRequest getPageRequestFromPageDemande(PageDemande pageDemande) {

        if (pageDemande == null) {
            return null;
        }

        PageRequest pageRequest = null;

        if (StringUtils.isEmpty(pageDemande.getChampTri())) {
            // pageRequest sans tri
            pageRequest = PageRequest.of(pageDemande.getIndexPage(), pageDemande.getTaillePage());
        } else if (pageDemande.isDesc()) {
            // page request avec tri descendant
            pageRequest = PageRequest.of(pageDemande.getIndexPage(), pageDemande.getTaillePage(), Sort.Direction.DESC, pageDemande.getChampTri());
        } else {
            // page request avec tri ascendant
            pageRequest = PageRequest.of(pageDemande.getIndexPage(), pageDemande.getTaillePage(), Sort.Direction.ASC, pageDemande.getChampTri());
        }

        return pageRequest;
    }

    /**
     * Récupérer l'objet pageReponse correspondant à la page Spring-data fournie.
     *
     * @param <T>
     *            Le type de l'entité concernée.
     * @param page
     *            the page
     * @return the pageReponse from page
     */
    public static <T extends Serializable> PageReponse<T> getPageReponseFromPage(Page<T> page) {

        if (page == null) {
            return null;
        }
        return new PageReponse<>(page.getContent(), page.getTotalElements());
    }

}

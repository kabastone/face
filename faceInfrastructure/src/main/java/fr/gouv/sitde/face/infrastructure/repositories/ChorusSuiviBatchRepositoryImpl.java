/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.ChorusSuiviBatchRepository;
import fr.gouv.sitde.face.persistance.repositories.ChorusSuiviBatchJpaRepository;
import fr.gouv.sitde.face.transverse.entities.ChorusSuiviBatch;

/**
 * The Class ChorusSuiviBatchRepositoryImpl.
 *
 * @author Atos
 */
@Component
public class ChorusSuiviBatchRepositoryImpl implements ChorusSuiviBatchRepository {

    /** The chorus suivi batch jpa repository. */
    @Inject
    private ChorusSuiviBatchJpaRepository chorusSuiviBatchJpaRepository;

    /**
     * Lire suivi batch.
     *
     * @return the chorus suivi batch
     */
    @Override
    public ChorusSuiviBatch lireSuiviBatch() {
        return this.chorusSuiviBatchJpaRepository.findById(Boolean.TRUE).orElse(new ChorusSuiviBatch());
    }

    /**
     * Mettre A jour suivi batch.
     *
     * @param chorusSuiviBatch
     *            the chorus suivi batch
     * @return the chorus suivi batch
     */
    @Override
    public ChorusSuiviBatch mettreAJourSuiviBatch(ChorusSuiviBatch chorusSuiviBatch) {
        return this.chorusSuiviBatchJpaRepository.save(chorusSuiviBatch);
    }

}

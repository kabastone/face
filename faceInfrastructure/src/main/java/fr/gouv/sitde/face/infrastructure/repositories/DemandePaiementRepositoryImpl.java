/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository;
import fr.gouv.sitde.face.persistance.repositories.DemandePaiementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;

/**
 * Service d'acces au repository JPA DemandePaiement.
 *
 * @author Atos
 */
@Component
public class DemandePaiementRepositoryImpl implements DemandePaiementRepository {

    /** The demande paiement jpa repository. */
    @Inject
    private DemandePaiementJpaRepository demandePaiementJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherParId(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherParId(Long idDemandePaiement) {
        return this.demandePaiementJpaRepository.findById(idDemandePaiement).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherParIdAvecDocs(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercherParIdAvecDocs(Long idDemandePaiement) {
        return this.demandePaiementJpaRepository.findByIdAvecDocs(idDemandePaiement).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherParDossierSubvention(java.lang.Long)
     */
    @Override
    public List<DemandePaiement> rechercherParDossierSubvention(Long idDossierSubvention) {
        return this.demandePaiementJpaRepository.findByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#enregistrer(fr.gouv.sitde.face.transverse.entities. DemandePaiement)
     */
    @Override
    public DemandePaiement enregistrer(DemandePaiement demandePaiement) {
        return this.demandePaiementJpaRepository.saveAndFlush(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercheDemandePaiementPourPdfParId(java.lang.Long)
     */
    @Override
    public DemandePaiement rechercheDemandePaiementPourPdfParId(Long idDemandePaiement) {
        return this.demandePaiementJpaRepository.rechercheDemandePaiementPourPdfParId(idDemandePaiement).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#compteAccomptesPourDossierParIdDemande(java.lang.Long)
     */
    @Override
    public Integer compteAccomptesPourDossierParIdDemande(Long idDemandePaiement) {
        return this.demandePaiementJpaRepository.compteAccomptesPourDossierParIdDemande(idDemandePaiement);
    }

    @Override
    public DemandePaiement rechercherParIdChorus(Long idChorus) {
        return this.demandePaiementJpaRepository.rechercherParIdChorus(idChorus).orElse(null);
    }

    @Override
    public DemandePaiement mettreAJourDemandePaiement(DemandePaiement demandePaiement) {
        return this.demandePaiementJpaRepository.saveAndFlush(demandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherToutesDemandesPaiementPourBatch()
     */
    @Override
    public List<DemandePaiement> rechercherToutesDemandesPaiementPourBatch() {
        return this.demandePaiementJpaRepository.findAllPourBatch();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#getIdfromIdAe(java.lang.Long)
     */
    @Override
    public Long getIdfromIdAe(Long idAe) {
        return this.demandePaiementJpaRepository.getIdfromIdAe(idAe).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherToutesDemandesPaiementPourFen0159a()
     */
    @Override
    public List<DemandePaiement> rechercherToutesDemandesPaiementPourFen0159a() {
        return this.demandePaiementJpaRepository.findAllPourFen0159a();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherCertifNonNulParService(java.lang.Long)
     */
    @Override
    public List<DemandePaiement> rechercherCertifNonNulParService(String chorusNumSf) {
        return this.demandePaiementJpaRepository.rechercherCertifNonNulParService(chorusNumSf);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherVersementNonNulParService(java.lang.String)
     */
    @Override
    public List<DemandePaiement> rechercherVersementNonNulParService(String chorusNumSf) {
        return this.demandePaiementJpaRepository.rechercherVersementNonNulParService(chorusNumSf);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherToutesPourRepartition()
     */
    @Override
    public List<DemandePaiement> rechercherToutesPourRepartition() {
        return this.demandePaiementJpaRepository.findAllPourRepartition();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherMontantAvanceTotalDemandesPaiementDossier(java.lang.Long)
     */
    @Override
    public BigDecimal rechercherMontantAvanceTotalDemandesPaiementDossier(Long idDossier) {
        BigDecimal result = this.demandePaiementJpaRepository.rechercherMontantAvanceTotalDemandesPaiementDossier(idDossier);
        return result == null ? BigDecimal.ZERO : result;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherNumeroOrdreSuivant(java.lang.Long)
     */
    @Override
    public short rechercherNumeroOrdreSuivant(Long idDossier) {
        return this.demandePaiementJpaRepository.rechercherNumeroOrdreSuivant(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#rechercherNumeroOrdre(java.lang.Long)
     */
    @Override
    public short rechercherNumeroOrdre(Long idDemandePaiement) {
        return this.demandePaiementJpaRepository.rechercherNumeroOrdre(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandePaiementRepository#isEstUniqueDemandePaiementEnCours(java.lang.String)
     */
    @Override
    public boolean isEstUniqueDemandePaiementEnCours(String numDossier) {
        return this.demandePaiementJpaRepository.isEstUniqueDemandePaiementEnCours(numDossier);
    }

}

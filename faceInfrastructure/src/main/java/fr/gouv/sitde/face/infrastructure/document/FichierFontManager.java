/**
 *
 */
package fr.gouv.sitde.face.infrastructure.document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.infrastructure.configuration.UploadCheminRepertoireProperties;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;

/**
 * The Class FichierFontManager.
 *
 * @author a453029
 */
@Component
public class FichierFontManager {

    /** The Constant NOM_FICHIER_POLICE_BOLD. */
    private static final String NOM_FICHIER_POLICE_BOLD = "LiberationSerif-Bold.ttf";

    /** The Constant NOM_FICHIER_POLICE_BOLD_ITALIQUE. */
    private static final String NOM_FICHIER_POLICE_BOLD_ITALIQUE = "LiberationSerif-BoldItalic.ttf";

    /** The Constant NOM_FICHIER_POLICE_ITALIQUE. */
    private static final String NOM_FICHIER_POLICE_ITALIQUE = "LiberationSerif-Italic.ttf";

    /** The Constant NOM_FICHIER_POLICE_REGULAR. */
    private static final String NOM_FICHIER_POLICE_REGULAR = "LiberationSerif-Regular.ttf";

    /** The Constant NOM_REPERTOIRE_FONTS. */
    private static final String NOM_REPERTOIRE_FONTS = "fonts";

    /** The upload chemin repertoire properties. */
    @Inject
    private UploadCheminRepertoireProperties uploadCheminRepertoireProperties;

    /**
     * Intialise le repertoire des fonts en le nettoyant.
     */
    @PostConstruct
    public void init() {
        this.nettoyerRepertoireFonts();
    }

    /**
     * Nettoyer repertoire fonts.
     */
    @PreDestroy
    private void nettoyerRepertoireFonts() {
        FileUtils.deleteQuietly(new File(this.uploadCheminRepertoireProperties.getRacine() + File.separator + NOM_REPERTOIRE_FONTS));
    }

    /**
     * Recuperer fichier police bold.
     *
     * @return the file
     */
    public File recupererFichierPoliceBold() {
        return this.recupererFichierFont(NOM_FICHIER_POLICE_BOLD);
    }

    /**
     * Recuperer fichier police bold italic.
     *
     * @return the file
     */
    public File recupererFichierPoliceBoldItalic() {
        return this.recupererFichierFont(NOM_FICHIER_POLICE_BOLD_ITALIQUE);
    }

    /**
     * Recuperer fichier police italic.
     *
     * @return the file
     */
    public File recupererFichierPoliceItalic() {
        return this.recupererFichierFont(NOM_FICHIER_POLICE_ITALIQUE);
    }

    /**
     * Recuperer fichier police regular.
     *
     * @return the file
     */
    public File recupererFichierPoliceRegular() {
        return this.recupererFichierFont(NOM_FICHIER_POLICE_REGULAR);
    }

    /**
     * Recuperer fichier font.
     *
     * @param nomFichier
     *            the nom fichier
     * @return the file
     */
    private File recupererFichierFont(String nomFichier) {
        File repertoireFonts = new File(this.uploadCheminRepertoireProperties.getRacine() + File.separator + NOM_REPERTOIRE_FONTS);

        // Si le répertoire de fonts n'existe pas, on le crée
        if (!repertoireFonts.exists() && !repertoireFonts.mkdir()) {
            throw new TechniqueException("Impossible de creer le dossier des fonts  : " + repertoireFonts.getPath());
        }

        File fontFile = new File(repertoireFonts.getPath() + File.separator + nomFichier);
        if (!fontFile.exists()) {
            try {
                // On recupère le flux du fichier de polices dans le JAR faceInfrastructure
                InputStream inputStream = new ClassPathResource("document/fonts/" + nomFichier).getInputStream();
                // Copie du contenu du fichier.
                FileUtils.copyInputStreamToFile(inputStream, fontFile);

            } catch (IOException e) {
                throw new TechniqueException(e.getMessage(), e);
            }
        }
        return fontFile;
    }

}

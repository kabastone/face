/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.ReglementaireRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.ReglementaireJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Reglementaire;

/**
 * The Class ReglementaireRepositoryImpl.
 */
@Component
public class ReglementaireRepositoryImpl implements ReglementaireRepository {

    /** The reglementaire jpa repository. */
    @Inject
    private ReglementaireJpaRepository reglementaireJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.ReglementaireRepository#rechercherTousReglementaire()
     */
    @Override
    public Reglementaire rechercherReglementaire(Integer anneeMiseEnApplication) {
        return this.reglementaireJpaRepository.findReglementaire(anneeMiseEnApplication)
                .orElse(this.reglementaireJpaRepository.findMinReglementaire().orElse(null));
    }

}

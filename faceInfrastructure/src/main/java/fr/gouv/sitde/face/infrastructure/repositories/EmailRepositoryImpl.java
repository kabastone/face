/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.EmailRepository;
import fr.gouv.sitde.face.persistance.repositories.EmailJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Email;

/**
 * Service d'acces au repository JPA Email.
 *
 * @author a453029
 */
@Component
public class EmailRepositoryImpl implements EmailRepository {

    /** The email jpa repository. */
    @Inject
    private EmailJpaRepository emailJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.EmailRepository#creerEmail(fr.gouv.sitde.face.transverse.entities.Email)
     */
    @Override
    public Email creerEmail(Email email) {
        return this.emailJpaRepository.save(email);
    }

}

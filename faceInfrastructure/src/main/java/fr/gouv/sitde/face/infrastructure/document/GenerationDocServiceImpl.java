/**
 *
 */
package fr.gouv.sitde.face.infrastructure.document;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

import fr.gouv.sitde.face.domain.spi.document.GenerationDocService;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;

/**
 * The Class GenerationDocServiceImpl.
 *
 * @author a453029
 */
@Service
public class GenerationDocServiceImpl implements GenerationDocService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GenerationDocServiceImpl.class);

    /** The template engine. */
    @Inject
    @Qualifier("documentHtmlTemplateEngine")
    private ITemplateEngine docHtmlTemplateEngine;

    /** The fichier font manager. */
    @Inject
    private FichierFontManager fichierFontManager;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.document.GenerationDocService#genererFichierPdf(fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum,
     * java.util.Map)
     */
    @Override
    public FichierTransfert genererFichierPdf(final TemplateDocumentEnum templateDocEnum, final Map<String, Object> variablesContexte) {

        Context contexte = new Context(Locale.FRENCH, variablesContexte);

        String htmlContenu = this.docHtmlTemplateEngine.process(templateDocEnum.getNomTemplate(), contexte);
        ITextRenderer renderer = new ITextRenderer();

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            // Ajout des polices liberation serif dans l'iTextRenderer
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceBold().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceBoldItalic().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceItalic().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceRegular().getPath(), true);

            // Creation du pdf
            renderer.setDocumentFromString(htmlContenu);
            renderer.layout();
            renderer.createPDF(byteArrayOutputStream, false);
            renderer.finishPDF();
            byte[] bytes = byteArrayOutputStream.toByteArray();
            LOGGER.debug("Le PDF a été créé avec succès");
            return construireFichierTranfert(templateDocEnum, bytes);

        } catch (DocumentException | IOException e) {
            throw new TechniqueException(e);
        }
    }

    /*
     *
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.document.GenerationDocService#genererFichierPdf(fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum,
     * java.util.List)
     */
    @Override
    public FichierTransfert genererFichierPdf(final TemplateDocumentEnum templateDocEnum, final List<Map<String, Object>> listVariablesContexte) {

        // On fait un createPDF avec le premier fichier PDF
        // On enchaine ensuite les writeNextDocument pour les autres fichiers PDF
        // On finalise par finishPDF

        final String[] htmlContenus = new String[listVariablesContexte.size()];
        int cpt = 0;
        for (Map<String, Object> variablesContexte : listVariablesContexte) {
            Context contexte = new Context(Locale.FRENCH, variablesContexte);
            String htmlContenu = this.docHtmlTemplateEngine.process(templateDocEnum.getNomTemplate(), contexte);
            htmlContenus[cpt] = htmlContenu;
            cpt++;
        }

        ITextRenderer renderer = new ITextRenderer();

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            // Ajout des polices liberation serif dans l'iTextRenderer
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceBold().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceBoldItalic().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceItalic().getPath(), true);
            renderer.getFontResolver().addFont(this.fichierFontManager.recupererFichierPoliceRegular().getPath(), true);

            // Creation du pdf
            renderer.setDocumentFromString(htmlContenus[0]);
            renderer.layout();
            renderer.createPDF(byteArrayOutputStream, false);

            // Ensuite je boucle sur les autres contenus et j'utilise la méthode writeNextDocument
            // each page after the first we add using layout() followed by writeNextDocument()
            for (int i = 1; i < htmlContenus.length; i++) {
                renderer.setDocumentFromString(htmlContenus[i]);
                renderer.layout();
                renderer.writeNextDocument();
            }

            renderer.finishPDF();
            byte[] bytes = byteArrayOutputStream.toByteArray();
            LOGGER.debug("Le PDF a été créé avec succès");
            return construireFichierTranfert(templateDocEnum, bytes);

        } catch (DocumentException | IOException e) {
            throw new TechniqueException(e);
        }

    }

    /**
     * Construire fichier transfert.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @param bytes
     *            the bytes
     * @return the fichier transfert
     */
    private static FichierTransfert construireFichierTranfert(TemplateDocumentEnum templateDocEnum, byte[] bytes) {
        FichierTransfert fichierTransfert = new FichierTransfert();
        fichierTransfert.setBytes(bytes);
        fichierTransfert.setContentTypeTheorique(ExtensionFichierEnum.PDF.getContentType());
        fichierTransfert.setNomFichier(FichierServiceImpl.genererNomFichierPdfFromTemplate(templateDocEnum));
        return fichierTransfert;

    }
}

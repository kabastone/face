/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.TransfertPaiementRepository;
import fr.gouv.sitde.face.persistance.repositories.TransfertPaiementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.TransfertPaiement;

/**
 * Service d'acces au repository JPA TransfertPaiement.
 *
 * @author Atos
 */
@Component
public class TransfertPaiementRepositoryImpl implements TransfertPaiementRepository {

    /** The demande paiement jpa repository. */
    @Inject
    private TransfertPaiementJpaRepository transfertPaiementJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TransfertPaiementRepository#findTransfertPaiementByDemandePaiement(java.lang.Long)
     */
    @Override
    public List<TransfertPaiement> findTransfertPaiementByDossierSubvention(Long idDossierSubvention) {
        return this.transfertPaiementJpaRepository.findTransfertPaiementByDossierSubvention(idDossierSubvention);
    }

    @Override
    public TransfertPaiement saveTransfertPaiement(TransfertPaiement transfertPaiement) {
        return this.transfertPaiementJpaRepository.saveAndFlush(transfertPaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TransfertPaiementRepository#rechercherParNumSFetLegacyetNumEJ(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public List<TransfertPaiement> rechercherParNumSFetLegacyetNumEJ(String chorusNumSF, String chorusNumLigneLegacy, String chorusNumEJ) {
        return this.transfertPaiementJpaRepository.rechercherParNumSFetLegacyetNumEJ(chorusNumSF, chorusNumLigneLegacy, chorusNumEJ);
    }
}

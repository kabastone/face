/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository;
import fr.gouv.sitde.face.persistance.repositories.AdresseEmailJpaRepository;
import fr.gouv.sitde.face.transverse.entities.AdresseEmail;

/**
 * Implémentation du repository du service d'acces au repository des adresses e-mails pour la liste de diffusion.
 *
 * @author a453029
 */
@Component
public class AdresseEmailRepositoryImpl implements AdresseEmailRepository {

    /** The adresse email jpa repository. */
    @Inject
    private AdresseEmailJpaRepository adresseEmailJpaRepository;

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository#rechercherAdressesEmail(java.lang.Long)
     */
    @Override
    public List<AdresseEmail> rechercherAdressesEmail(Long idCollectivite) {
        return this.adresseEmailJpaRepository.rechercherAdressesEmail(idCollectivite);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository#rechercherAdresseEmail(java.lang.Long)
     */
    @Override
    public AdresseEmail rechercherAdresseEmail(Long id) {
        return this.adresseEmailJpaRepository.findById(id).orElse(null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository#supprimerAdresseEmail(java.lang.Long)
     */
    @Override
    public void supprimerAdresseEmail(Long id) {
        this.adresseEmailJpaRepository.deleteById(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseEmailRepository#ajouterAdresseEmail(fr.gouv.sitde.face.transverse.entities.AdresseEmail)
     */
    @Override
    public AdresseEmail ajouterAdresseEmail(AdresseEmail adresseEmail) {
        return this.adresseEmailJpaRepository.saveAndFlush(adresseEmail);
    }

}

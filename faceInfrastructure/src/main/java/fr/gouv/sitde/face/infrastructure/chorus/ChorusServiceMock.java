package fr.gouv.sitde.face.infrastructure.chorus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.chorus.ChorusService;
import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;
import fr.gouv.sitde.face.transverse.chorus.WsChorusCodesErreursEnum;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;

/**
 * Mock du service qui renvoie une Collectivite suite à l'appel au client WS Chorus.
 *
 */
@Component
@Profile({ "test", "testpic", "dev", "devang" })
public class ChorusServiceMock implements ChorusService {

    /*
     *
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.chorus.ChorusService#rechercherCollectivites(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public List<CollectiviteChorus> rechercherCollectivites(String codeSociete, String idFonctionnel, String idTechnique,
            String organisationAchatFournisseur, String domaineCommercialeClient) {

        List<CollectiviteChorus> collectivitesChorus = new ArrayList<>();
        CollectiviteChorus collectiviteChorus = new CollectiviteChorus();
        CollectiviteChorus collectiviteChorus2 = new CollectiviteChorus();

        // Différents cas de figure du ChorusServiceMock basé sur le couple idFonctionnel (siret) / idTechnique (id chorus)
        String key = StringUtils.defaultString(idFonctionnel).concat("-").concat(StringUtils.defaultString(idTechnique));

        switch (key) {

        case "-91":
        case "78012998703591-":
            collectiviteChorus.setIdTechnique("91");
            collectiviteChorus.setNom("AIN - chorus");
            collectiviteChorus.setDateSynchro(LocalDateTime.now());
            collectiviteChorus.setCodePostal("13000");
            collectiviteChorus.setVille("MARSEILLE");
            collectiviteChorus.setNumeroRue("1");
            collectiviteChorus.setNomRue("RUE");
            collectiviteChorus.setComplementAdresse("COMPLEMENT");
            collectivitesChorus.add(collectiviteChorus);
            break;

        case "54053052655762-":
            throw new RegleGestionException(WsChorusCodesErreursEnum.ECF_02.getCode());

        case "33981502848593-":
            collectiviteChorus.setIdTechnique("93");
            collectiviteChorus.setNom("ANCERVILLE");
            collectiviteChorus.setDateSynchro(LocalDateTime.now());
            collectiviteChorus.setCodePostal("13000");
            collectiviteChorus.setVille("MARSEILLE");
            collectiviteChorus.setNumeroRue("1");
            collectiviteChorus.setNomRue("RUE");
            collectiviteChorus.setComplementAdresse("COMPLEMENT");
            collectivitesChorus.add(collectiviteChorus);

            collectiviteChorus2.setIdTechnique("9393");
            collectiviteChorus2.setNom("Doublon");
            collectiviteChorus2.setDateSynchro(LocalDateTime.now());
            collectiviteChorus2.setCodePostal("13000");
            collectiviteChorus2.setVille("MARSEILLE");
            collectiviteChorus2.setNumeroRue("1");
            collectiviteChorus2.setNomRue("RUE");
            collectiviteChorus2.setComplementAdresse("COMPLEMENT");
            collectivitesChorus.add(collectiviteChorus2);
            break;

        case "-66":
        case "65639904890966-":
            collectiviteChorus.setIdTechnique("66");
            collectiviteChorus.setNom("CHAILLY LES ENNERY");
            collectiviteChorus.setDateSynchro(LocalDateTime.now());
            collectiviteChorus.setCodePostal("13000");
            collectiviteChorus.setVille("MARSEILLE");
            collectiviteChorus.setNumeroRue("1");
            collectiviteChorus.setNomRue("RUE");
            collectiviteChorus.setComplementAdresse("COMPLEMENT");
            collectivitesChorus.add(collectiviteChorus);
            break;

        case "-80":
        case "76239330787980-":
            collectiviteChorus.setIdTechnique("80");
            collectiviteChorus.setNom("BIONVILLE SUR NIED");
            collectiviteChorus.setDateSynchro(LocalDateTime.now());
            collectiviteChorus.setCodePostal("13000");
            collectiviteChorus.setVille("MARSEILLE");
            collectiviteChorus.setNumeroRue("1");
            collectiviteChorus.setNomRue("RUE");
            collectiviteChorus.setComplementAdresse("COMPLEMENT");
            collectivitesChorus.add(collectiviteChorus);
            break;

        default:
            throw new RegleGestionException(WsChorusCodesErreursEnum.ECF_02.getCode());
        }

        return collectivitesChorus;
    }
}

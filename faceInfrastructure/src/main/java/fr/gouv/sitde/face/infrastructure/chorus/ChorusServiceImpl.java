/**
 *
 */
package fr.gouv.sitde.face.infrastructure.chorus;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.clientchorus.service.ClientChorusWs;
import fr.gouv.sitde.face.domain.spi.chorus.ChorusService;
import fr.gouv.sitde.face.transverse.chorus.CollectiviteChorus;

/**
 * Classe implémentant le service de chorus.
 *
 * @author Atos
 *
 */
@Service
@Profile({ "prod" })
public class ChorusServiceImpl implements ChorusService {

    /** The client WS Chorus. */
    @Inject
    private ClientChorusWs clientChorusWs;

    @Override
    public List<CollectiviteChorus> rechercherCollectivites(String codeSociete, String idFonctionnel, String idTechnique,
            String organisationAchatFournisseur, String domaineCommercialeClient) {

        return this.clientChorusWs.rechercherCollectivites(codeSociete, idFonctionnel, idTechnique, organisationAchatFournisseur,
                domaineCommercialeClient);
    }

}

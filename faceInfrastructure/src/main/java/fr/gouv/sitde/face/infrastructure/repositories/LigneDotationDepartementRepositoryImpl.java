/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository;
import fr.gouv.sitde.face.persistance.repositories.LigneDotationDepartementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.LigneDotationDepartement;
import fr.gouv.sitde.face.transverse.referentiel.TypeDotationDepartementEnum;

/**
 * The Class LigneDotationDepartementRepositoryImpl.
 */
@Component
public class LigneDotationDepartementRepositoryImpl implements LigneDotationDepartementRepository {

    /** The ligne dotation departement jpa repository. */
    @Inject
    private LigneDotationDepartementJpaRepository ligneDotationDepartementJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherParDepartementEtTypePourAnnexe(java.lang.Long,
     * java.lang.String)
     */
    @Override
    public List<LigneDotationDepartement> rechercherParDepartementEtTypePourAnnexe(Integer idDepartement, TypeDotationDepartementEnum typeDotation,
            Integer annee) {
        return this.ligneDotationDepartementJpaRepository.rechercherParDepartementEtTypePourAnnexe(idDepartement, typeDotation, annee);
    }

    /*
     *
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherParTypePourAnnexe(fr.gouv.sitde.face.transverse.
     * referentiel.TypeDotationDepartementEnum, java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherParTypePourAnnexe(TypeDotationDepartementEnum typeDotation, Integer annee) {
        return this.ligneDotationDepartementJpaRepository.rechercherParTypePourAnnexe(typeDotation, annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#compterDotationAnnuelleNotifieeAnneeEnCours()
     */
    @Override
    public Integer compterDotationAnnuelleNotifieeAnneeEnCours(int anneeEnCours) {
        return this.ligneDotationDepartementJpaRepository.compteDotationAnnuelleNotifieeAnneeEnCours(anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#calculerDotation(java.util.List)
     */
    @Override
    public BigDecimal calculerDotationSansAnnuelle(int anneeEnCours, String sprAbreviation) {
        return this.ligneDotationDepartementJpaRepository.calculerDotationSansAnnuelle(anneeEnCours, sprAbreviation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#getDotationSousProgramme(int, java.lang.String)
     */
    @Override
    public BigDecimal getDotationSousProgramme(int anneeEnCours, String sprAbreviation) {
        return this.ligneDotationDepartementJpaRepository.compterDotationSousProgramme(anneeEnCours, sprAbreviation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#razDotationAnnuelles()
     */
    @Override
    public void razDotationAnnuelles(int anneeEnCours) {
        this.ligneDotationDepartementJpaRepository.effacerDotationsAnnuelles(anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#ajouterLigneDotationDepartement(fr.gouv.sitde.face.transverse.
     * entities.LigneDotationDepartement)
     */
    @Override
    public LigneDotationDepartement ajouterLigneDotationDepartement(LigneDotationDepartement ligneDotationDepartement) {
        return this.ligneDotationDepartementJpaRepository.save(ligneDotationDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#majLigneDotationDepartement(fr.gouv.sitde.face.transverse.
     * entities.LigneDotationDepartement)
     */
    @Override
    public LigneDotationDepartement majLigneDotationDepartement(LigneDotationDepartement ligne) {
        return this.ligneDotationDepartementJpaRepository.saveAndFlush(ligne);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#majDateEnvoiLigneDotationDepartement(java.lang.Integer,
     * java.time.LocalDateTime)
     */
    @Override
    public void majDateEnvoiLigneDotationDepartement(Integer idDepartement, LocalDateTime LocalDateTime) {
        this.ligneDotationDepartementJpaRepository.majDateEnvoiLigneDotationDepartement(idDepartement, LocalDateTime);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherNumeroOrdreSuivant(java.lang.Integer)
     */
    @Override
    public short rechercherNumeroOrdreSuivant(Integer idDepartement) {
        return this.ligneDotationDepartementJpaRepository.rechercherNumeroOrdreSuivant(idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLignesDotationDepartementaleEnPreparation(java.lang.
     * Integer, java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesDotationDepartementaleEnPreparation(Integer annee, Integer idDepartement) {
        return this.ligneDotationDepartementJpaRepository.rechercherLignesDotationDepartementaleEnPreparation(annee, idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLignesDotationDepartementaleNotifiees(java.lang.
     * Integer, java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesDotationDepartementaleNotifiees(Integer annee, Integer idDepartement) {
        return this.ligneDotationDepartementJpaRepository.rechercherLignesDotationDepartementaleNotifiees(annee, idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#detruireLigneDotationDepartementale(java.lang.Long)
     */
    @Override
    public void detruireLigneDotationDepartementale(Long idLigneDotationDepartementale) {
        this.ligneDotationDepartementJpaRepository.deleteById(idLigneDotationDepartementale);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLigneParId(java.lang.Long)
     */
    @Override
    public LigneDotationDepartement rechercherLigneParId(Long idLigne) {
        return this.ligneDotationDepartementJpaRepository.rechercherLigneParId(idLigne).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#recupereDocumentNotificationParDepartementEtDateEnvoi(java.lang.
     * Integer, java.time.LocalDateTime)
     */
    @Override
    public Document recupereDocumentNotificationParDepartementEtDateEnvoi(Integer id, LocalDateTime dateEnvoi) {
        List<Document> listeDocument = this.ligneDotationDepartementJpaRepository.recupereDocumentNotificationParDepartementEtDateEnvoi(id,
                dateEnvoi);
        if (listeDocument.isEmpty()) {
            return null;
        } else {
            return listeDocument.get(0);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLignesDotationDepartementale(java.math.BigDecimal,
     * java.lang.Integer, java.lang.String)
     */
    @Override
    public Long rechercherLignesDotationDepartementale(LocalDateTime dateAccord) {
        List<Long> ids = this.ligneDotationDepartementJpaRepository.rechercherLignesDotationDepartementale(dateAccord);

        return ids.get(0);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLignesPertesEtRenonciations(int)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesPertesEtRenonciations(String codeProgramme, Integer anneeCourante) {
        return this.ligneDotationDepartementJpaRepository.rechercherLignesPertesEtRenonciations(codeProgramme, anneeCourante);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.LigneDotationDepartementRepository#rechercherLignesPertesEtRenonciationsParIdCollectivite(java.lang.
     * Long, java.lang.Integer)
     */
    @Override
    public List<LigneDotationDepartement> rechercherLignesPertesEtRenonciationsParIdCollectivite(Long idCollectivite, Integer anneeCourante) {
        return this.ligneDotationDepartementJpaRepository.rechercherLignesPertesEtRenonciationsParIdCollectivite(idCollectivite, anneeCourante);

    }
}

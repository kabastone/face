package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.CourrierAnnuelDepartementRepository;
import fr.gouv.sitde.face.persistance.repositories.CourrierAnnuelDepartementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.CourrierAnnuelDepartement;

/**
 * The Class CourrierAnnuelDepartementRepositoryImpl.
 */
@Component
public class CourrierAnnuelDepartementRepositoryImpl implements CourrierAnnuelDepartementRepository {

    /** The courrier annuel departement jpa repository. */
    @Inject
    private CourrierAnnuelDepartementJpaRepository courrierAnnuelDepartementJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CourrierAnnuelDepartementRepository#creerCourrierAnnuelDepartement(fr.gouv.sitde.face.transverse.
     * entities.CourrierAnnuelDepartement)
     */
    @Override
    public CourrierAnnuelDepartement creerCourrierAnnuelDepartement(CourrierAnnuelDepartement courrierAnnuelDepartement) {
        return this.courrierAnnuelDepartementJpaRepository.save(courrierAnnuelDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CourrierAnnuelDepartementRepository#rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(
     * java.lang.String, java.lang.Integer)
     */
    @Override
    public CourrierAnnuelDepartement rechercherCourrierAnnuelDepartementParCodeDepartementEtAnnee(String codeDepartement, Integer annee) {
        return this.courrierAnnuelDepartementJpaRepository.findCourrierAnnuelDepartementByCodeDepartementEtAnnee(codeDepartement, annee).orElse(null);
    }
}

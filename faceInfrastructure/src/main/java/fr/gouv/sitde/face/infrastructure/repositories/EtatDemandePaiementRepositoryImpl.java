/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandePaiementRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.EtatDemandePaiementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.EtatDemandePaiement;

/**
 * Service d'acces au repository JPA EtatDemandePaiement.
 *
 * @author Atos
 */
@Component
public class EtatDemandePaiementRepositoryImpl implements EtatDemandePaiementRepository {

    /** The etat demande paiement jpa repository. */
    @Inject
    private EtatDemandePaiementJpaRepository etatDemandePaiementJpaRepository;

    /* (non-Javadoc)
     * @see fr.gouv.sitde.face.domain.spi.repositories.EtatDemandePaiementRepository#findByCode(java.lang.String)
     */
    @Override
    public EtatDemandePaiement findByCode(String codeEtatdemande) {
        return this.etatDemandePaiementJpaRepository.findByCode(codeEtatdemande);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository;
import fr.gouv.sitde.face.persistance.repositories.DotationProgrammeJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DotationProgramme;

/**
 * Service d'acces au repository JPA DotationProgramme.
 *
 * @author Atos
 */
@Component
public class DotationProgrammeRepositoryImpl implements DotationProgrammeRepository {

    /** The dotation programme repository. */
    @Inject
    private DotationProgrammeJpaRepository dotationProgrammeJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository#creerDotationProgramme(fr.gouv.sitde.face.transverse.entities.
     * DotationProgramme)
     */
    @Override
    public DotationProgramme creerDotationProgramme(DotationProgramme dotationProgramme) {
        return this.dotationProgrammeJpaRepository.saveAndFlush(dotationProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository#rechercherDotationProgrammeParId(java.lang.Long)
     */
    @Override
    public DotationProgramme rechercherDotationProgrammeParId(Long idDotationProgramme) {
        return this.dotationProgrammeJpaRepository.rechercherParId(idDotationProgramme).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationProgrammeRepository#modifierDotationProgramme(fr.gouv.sitde.face.transverse.entities.
     * DotationProgramme)
     */
    @Override
    public DotationProgramme modifierDotationProgramme(DotationProgramme dotationProgramme) {
        return this.dotationProgrammeJpaRepository.saveAndFlush(dotationProgramme);
    }

}

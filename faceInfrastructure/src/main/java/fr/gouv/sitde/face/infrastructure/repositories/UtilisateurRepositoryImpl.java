/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository;
import fr.gouv.sitde.face.infrastructure.pagination.mapper.SpringDataPaginationMapper;
import fr.gouv.sitde.face.persistance.repositories.UtilisateurJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Utilisateur;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * Service d'acces au repository JPA Utilisateur.
 *
 * @author Atos
 */
@Component
public class UtilisateurRepositoryImpl implements UtilisateurRepository {

    /** The utilisateur repository. */
    @Inject
    private UtilisateurJpaRepository utilisateurJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repoaccess.UtilisateurRepoSpi#rechercherParId(java.lang.Long)
     */
    @Override
    public Utilisateur rechercherParId(Long idUtilisateur) {
        return this.utilisateurJpaRepository.findById(idUtilisateur).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repoaccess.UtilisateurRepoSpi#rechercherTous(fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<Utilisateur> rechercherTous(PageDemande pageDemande) {
        Page<Utilisateur> page = this.utilisateurJpaRepository.findAll(SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande));
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repoaccess.UtilisateurRepoSpi#rechercherParEmail(java.lang.String)
     */
    @Override
    public Utilisateur rechercherParEmail(String email) {
        return this.utilisateurJpaRepository.findByEmail(email).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository#creerUtilisateur(fr.gouv.sitde.face.transverse.entities.Utilisateur)
     */
    @Override
    public Utilisateur creerUtilisateur(Utilisateur utilisateur) {
        return this.utilisateurJpaRepository.save(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository#modifierUtilisateur(fr.gouv.sitde.face.transverse.entities.Utilisateur)
     */
    @Override
    public Utilisateur modifierUtilisateur(Utilisateur utilisateur) {
        return this.utilisateurJpaRepository.saveAndFlush(utilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.UtilisateurRepository#supprimerUtilisateur(java.lang.Long)
     */
    @Override
    public void supprimerUtilisateur(Long idUtilisateur) {
        this.utilisateurJpaRepository.deleteById(idUtilisateur);
    }
}

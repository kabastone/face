/**
 *
 */
package fr.gouv.sitde.face.infrastructure.document;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.domain.spi.document.FichierService;
import fr.gouv.sitde.face.infrastructure.configuration.UploadCheminRepertoireProperties;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.exceptions.RegleGestionException;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.fichier.ExtensionFichierEnum;
import fr.gouv.sitde.face.transverse.fichier.FichierTransfert;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TemplateDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Classe implémentant le service de gestion des fichiers.
 *
 * @author Atos
 *
 */
@Service
public class FichierServiceImpl implements FichierService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FichierServiceImpl.class);

    /** Separateur entre le type de document et la date. */
    private static final char SEPARATEUR_NOM_FICHIER = '_';

    /** The pattern format date. */
    private static final String PATTERN_FORMAT_DATE_FICHIER = "yyyyMMddHHmmssSSS";

    /** The formatter. */
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FichierServiceImpl.PATTERN_FORMAT_DATE_FICHIER);

    /** The upload chemin repertoire properties. */
    @Inject
    private UploadCheminRepertoireProperties uploadCheminRepertoireProperties;

    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.infrastructure.fichier.FichierService#chargerFichier(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public File chargerFichier(Document document) {
        String cheminRepertoireFichier = ArborescenceFichiersCalculator.getCheminRepertoireDocument(document,
                this.uploadCheminRepertoireProperties.getRacine());

        File repertoire = new File(cheminRepertoireFichier);
        if (!repertoire.exists()) {
            throw new TechniqueException("Le repertoire à charger n'existe pas : " + repertoire.getPath());
        }

        File fichierDoc = new File(cheminRepertoireFichier + File.separator + document.getNomFichierSansExtension()
                + FilenameUtils.EXTENSION_SEPARATOR_STR + document.getExtensionFichier());
        if (!fichierDoc.exists()) {
            throw new TechniqueException("Le fichier n'existe pas : " + fichierDoc.getPath());
        }

        return fichierDoc;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.infrastructure.fichier.FichierService#supprimerFichier(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public void supprimerFichier(Document document) {

        File fichierDoc = this.chargerFichier(document);
        FileUtils.deleteQuietly(fichierDoc);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.document.FichierService#enregistrerFichier(fr.gouv.sitde.face.transverse.entities.Document,
     * fr.gouv.sitde.face.transverse.fichier.FichierTransfert)
     */
    @Override
    public File enregistrerFichier(Document document, FichierTransfert fichierTransfert) {

        if ((fichierTransfert == null) || (fichierTransfert.getBytes() == null)) {
            throw new TechniqueException("Le fichier à enregistrer n'existe pas");
        }

        if (document.getTypeDocument() == null) {
            throw new TechniqueException("Le document n'a pas de type de document");
        }

        validerTypeFichierParParsing(fichierTransfert);

        File fichierFinal = this.construireFichier(document);
        try {
            FileUtils.writeByteArrayToFile(fichierFinal, fichierTransfert.getBytes());
        } catch (IOException e) {
            throw new TechniqueException("Impossible d'ecrire le fichier " + fichierFinal.getAbsolutePath(), e);
        }

        LOGGER.debug("Enregistrement du fichier : {}", fichierFinal.getPath());

        return fichierFinal;
    }

    /**
     * Construire fichier.
     *
     * @param document
     *            the document
     * @return the file
     */
    private File construireFichier(Document document) {

        // Contrôle d'existence du répertoire racine
        File repertoireRacine = new File(this.uploadCheminRepertoireProperties.getRacine());

        if (!repertoireRacine.exists() || !repertoireRacine.isDirectory()) {
            throw new TechniqueException("Le répértoire racine " + this.uploadCheminRepertoireProperties.getRacine() + " n'est pas defini");
        }

        // Récupération du chemin du fichier à créer selon la famille du document
        String cheminRepertoire = ArborescenceFichiersCalculator.getCheminRepertoireDocument(document,
                this.uploadCheminRepertoireProperties.getRacine());

        // Calcul du nom de Fichier
        TypeDocumentEnum typeDocumentEnum = TypeDocumentEnum.rechercherEnumParIdCodifie(document.getTypeDocument().getIdCodifie());
        String nomFicher = FilenameUtils.getName(this.genererNomFichier(typeDocumentEnum, document.getExtensionFichier()));

        // Renvoi du file correspondant au chemin + nom fichier
        return new File(cheminRepertoire + File.separator + nomFicher);
    }

    /**
     * Generer un nom de fichier.
     *
     * @param typeDocumentEnum
     *            the type document enum
     * @param extension
     *            the extension
     * @return the string
     */
    private String genererNomFichier(TypeDocumentEnum typeDocumentEnum, final String extension) {
        StringBuilder nomFichier = new StringBuilder();
        nomFichier.append(typeDocumentEnum.getCode());

        // On ajoute la LocalDateTime au nom de fichier sauf pour les modèles.
        if (!FamilleDocumentEnum.DOC_MODELE.equals(TypeDocumentEnum.getFamilleFromTypeDoc(typeDocumentEnum))) {
            nomFichier.append(SEPARATEUR_NOM_FICHIER);
            nomFichier.append(formatter.format(this.timeOperationTransverse.now()));
        }
        nomFichier.append('.');
        nomFichier.append(extension);
        return nomFichier.toString();
    }

    /**
     * Generer nom fichier pdf from template.
     *
     * @param templateDocEnum
     *            the template doc enum
     * @return the string
     */
    public static String genererNomFichierPdfFromTemplate(TemplateDocumentEnum templateDocEnum) {
        StringBuilder nomFichier = new StringBuilder();
        nomFichier.append(templateDocEnum.toString());
        nomFichier.append(SEPARATEUR_NOM_FICHIER);
        SimpleDateFormat patternFormatDate = new SimpleDateFormat(PATTERN_FORMAT_DATE_FICHIER, Locale.FRENCH);
        nomFichier.append(patternFormatDate.format(new Date()));
        nomFichier.append('.');
        nomFichier.append(ExtensionFichierEnum.PDF.getExtension());
        return nomFichier.toString();
    }

    /**
     * Réalise une validation du type de fichier en parsant l'entête du fichier. <br/>
     * Une technique exception est envoyée en cas de type non-authorisé.
     *
     * @param fichierTransfert
     *            the fichier transfert
     */
    private static void validerTypeFichierParParsing(FichierTransfert fichierTransfert) {

        Metadata metadata = new Metadata();
        metadata.set(Metadata.RESOURCE_NAME_KEY, fichierTransfert.getNomFichier());
        metadata.set(Metadata.CONTENT_TYPE, fichierTransfert.getContentTypeTheorique());
        MediaType mimetype = null;

        try (TikaInputStream tikaInputStream = TikaInputStream.get(new ByteArrayInputStream(fichierTransfert.getBytes()))) {
            Tika tika = new Tika();
            mimetype = tika.getDetector().detect(tikaInputStream, metadata);
        } catch (IOException e) {
            throw new TechniqueException("Problème detection Tika : " + e.getMessage(), e);
        }

        if (isInvalidTikaType(mimetype.toString())) {
            throw new RegleGestionException("upload.fichier.type.non.gere", fichierTransfert.getNomFichier());
        }
    }

    /**
     * Vérifie si le mimetype passé en paramètre est un mimetype accepté par l'application.
     *
     * @param mimetype
     *            the mimetype
     * @return true, if is invalid tika type
     */
    private static boolean isInvalidTikaType(String mimetype) {
        return ExtensionFichierEnum.rechercherEnumParTikaType(mimetype) == null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.document.FichierService#zipperEtEncoderDonneesBase64(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public byte[] zipperEtEncoderDonneesBase64(Document doc) {
        File file = this.chargerFichier(doc);

        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(baos)) {
            try (FileInputStream fis = new FileInputStream(file)) {
                zos.putNextEntry(new ZipEntry(file.getName()));
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
        return baos.toByteArray();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.document.FichierService#deplacerSousDossier(fr.gouv.sitde.face.transverse.entities.Document,
     * fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public void deplacerSousDossierDemandeSubvention(Document cible, Document source, boolean supprimerParentSource) {

        // Contrôle d'existence du répertoire racine
        File repertoireRacine = new File(this.uploadCheminRepertoireProperties.getRacine());

        if (!repertoireRacine.exists() || !repertoireRacine.isDirectory()) {
            throw new TechniqueException("Le répértoire racine " + this.uploadCheminRepertoireProperties.getRacine() + " n'est pas defini");
        }

        // Récupération du chemin du fichier à créer selon la famille du document
        String cheminRepertoireSource = ArborescenceFichiersCalculator.getCheminRepertoireDemandeSubvention(source,
                this.uploadCheminRepertoireProperties.getRacine());

        String cheminRepertoireCible = ArborescenceFichiersCalculator.getCheminRepertoireDemandeSubvention(cible,
                this.uploadCheminRepertoireProperties.getRacine());

        File directorySource = new File(cheminRepertoireSource);
        File directoryCible = new File(cheminRepertoireCible);

        // si il existe un repertoire pour la demande de subvention
        if (directorySource.exists()) {

            File[] files = directorySource.listFiles();

            if (files != null) {

                try {
                    // on deplace le contenu du repertoire dans le nouveau repertoire
                    for (File file : files) {
                        FileUtils.moveFileToDirectory(file, directoryCible, true);
                    }
                    // on supprimer le repertoire de la demande de subvention d'origine
                    FileUtils.deleteDirectory(directorySource);

                    // si c'est demande on supprime
                    if (supprimerParentSource) {
                        // on remonte au repertoire parent (DEM-SUB)
                        String parentSource = StringUtils.substringBeforeLast(cheminRepertoireSource, File.separator);
                        // on remonte au repertoire parent (idDossier)
                        parentSource = StringUtils.substringBeforeLast(parentSource, File.separator);
                        File parentDirectory = new File(parentSource);
                        FileUtils.deleteDirectory(parentDirectory);
                    }

                } catch (IOException ex) {
                    throw new TechniqueException("Erreur de deplacement de dossier", ex);
                }
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.document.FichierService#encoderDonneesBase64(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public byte[] encoderDonneesBase64(Document doc) {
        File file = this.chargerFichier(doc);

        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (FileInputStream fis = new FileInputStream(file)) {
            int length;
            while ((length = fis.read(buffer)) > 0) {
                baos.write(buffer, 0, length);
            }
        } catch (IOException e) {
            throw new TechniqueException(e.getMessage(), e);
        }
        return baos.toByteArray();
    }

}

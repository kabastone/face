/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.FluxChorusRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.FluxChorusJpaRepository;
import fr.gouv.sitde.face.transverse.entities.FluxChorus;

/**
 * The Class FluxChorusRepositoryImpl.
 */
@Component
public class FluxChorusRepositoryImpl implements FluxChorusRepository {

    /** The flux chorus jpa repository. */
    @Inject
    private FluxChorusJpaRepository fluxChorusJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.FluxChorusRepository#rechercherFluxChorus()
     */
    @Override
    public FluxChorus rechercherFluxChorus() {
        return this.fluxChorusJpaRepository.rechercherFluxChorus().orElse(null);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.TypeAnomalieRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.TypeAnomalieJpaRepository;
import fr.gouv.sitde.face.transverse.entities.TypeAnomalie;

/**
 * The Class TypeAnomalieRepositoryImpl.
 *
 * @author A754839
 */
@Component
public class TypeAnomalieRepositoryImpl implements TypeAnomalieRepository {

    /** The type anomalie jpa repository. */
    @Inject
    private TypeAnomalieJpaRepository typeAnomalieJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TypeAnomalieRepository#rechercherParCode(java.lang.String)
     */
    @Override
    public TypeAnomalie rechercherParCode(String code) {
        return this.typeAnomalieJpaRepository.rechercherParCode(code).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TypeAnomalieRepository#rechercherTousPourDemandePaiement()
     */
    @Override
    public List<TypeAnomalie> rechercherTousPourDemandePaiement() {
        return this.typeAnomalieJpaRepository.rechercherTousPourDemandePaiement();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TypeAnomalieRepository#rechercherTousPourDemandePaiement()
     */
    @Override
    public List<TypeAnomalie> rechercherTousPourDemandeSubvention() {
        return this.typeAnomalieJpaRepository.rechercherTousPourDemandeSubvention();
    }

}

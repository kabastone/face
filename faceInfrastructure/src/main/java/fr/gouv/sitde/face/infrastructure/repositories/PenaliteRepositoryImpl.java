/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.PenaliteRepository;
import fr.gouv.sitde.face.persistance.repositories.PenaliteJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Penalite;

/**
 * @author Atos
 *
 */
@Component
public class PenaliteRepositoryImpl implements PenaliteRepository {

    @Inject
    private PenaliteJpaRepository penaliteJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.PenaliteRepository#ajouterPenalite(fr.gouv.sitde.face.transverse.entities.Penalite)
     */
    @Override
    public Penalite ajouterPenalite(Penalite penalite) {
        return this.penaliteJpaRepository.save(penalite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.PenaliteRepository#supprimerPenaliteParAnnee(int)
     */
    @Override
    public void supprimerPenaliteParAnnee(int annee) {
        this.penaliteJpaRepository.supprimerPenaliteParAnnee(annee);
    }

}

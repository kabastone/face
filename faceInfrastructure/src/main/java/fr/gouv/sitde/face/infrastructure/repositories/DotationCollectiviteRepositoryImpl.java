/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository;
import fr.gouv.sitde.face.persistance.repositories.DotationCollectiviteJpaRepository;
import fr.gouv.sitde.face.persistance.repositories.custom.DotationCollectiviteEmailDtoJpaRepository;
import fr.gouv.sitde.face.transverse.email.DotationCollectiviteParSousProgrammeDto;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;

/**
 * The Class DotationCollectiviteRepositoryImpl.
 */
@Component
public class DotationCollectiviteRepositoryImpl implements DotationCollectiviteRepository {

    /** The dotation collectivite jpa repository. */
    @Inject
    private DotationCollectiviteJpaRepository dotationCollectiviteJpaRepository;

    /** The dotation collectivite email Dto jpa repository. */
    @Inject
    private DotationCollectiviteEmailDtoJpaRepository dotationCollectiviteEmailDtoJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#majDotationCollectivite(fr.gouv.sitde.face.transverse.entities.
     * DotationCollectivite)
     */
    @Override
    public DotationCollectivite majDotationCollectivite(DotationCollectivite dotationCollectivite) {
        return this.dotationCollectiviteJpaRepository.saveAndFlush(dotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDotationsCollectivitesParIdDepartementEtAbreviationSousProgramme(
     * java.lang.Integer, fr.gouv.sitde.face.transverse.referentiel.SousProgrammeAbreviationEnum)
     */
    @Override
    public List<DotationCollectivite> rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(String codeDepartement,
            String sprEnum, Integer anneeEnCours) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationsCollectivitesParCodeDepartementEtAbreviationSousProgramme(codeDepartement,
                sprEnum, anneeEnCours);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#rechercherDotationCollectivite(java.lang.Long,
     * java.lang.Integer, java.lang.Integer)
     */
    @Override
    public DotationCollectivite rechercherDotationCollectivite(Long idCollectivite, Integer idSousProgramme, Integer annee) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationCollectivite(idCollectivite, idSousProgramme, annee).orElse(null);
    }

    @Override
    public List<DotationCollectivite> rechercherDotationCollectiviteParIdCollectivite(Long idCollectivite, Integer annee) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationCollectiviteParIdCollectivite(idCollectivite, annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#rechercherDTOparIdCollectivite(java.lang.Long)
     */
    @Override
    public List<DotationCollectiviteParSousProgrammeDto> rechercherDTOparIdCollectivite(Long idCollectivite, Integer annee) {
        return this.dotationCollectiviteEmailDtoJpaRepository.rechercherDonneesPourEmailDotation(idCollectivite, annee);

    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#verifierExistenceDemandeSubvention(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public boolean verifierExistenceDemandeSubvention(Long id, Integer anneeEnCours) {
        return this.dotationCollectiviteEmailDtoJpaRepository.verifierExistenceDemandeSubvention(id, anneeEnCours);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#rechercherDotationCollectiviteParId(java.lang.Long)
     */
    @Override
    public DotationCollectivite rechercherDotationCollectiviteParId(Long id) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationCollectiviteParId(id).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#rechercherDotationCollectiviteSimpleParId(java.lang.Long)
     */
    @Override
    public DotationCollectivite rechercherDotationCollectiviteSimpleParId(Long id) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationCollectiviteSimpleParId(id).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#
     * rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(java.lang.Long, java.lang.Integer, java.lang.String)
     */
    @Override
    public List<DotationCollectivite> rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(Long idCollectivite, Integer annee,
            String codeNumerique) {
        return this.dotationCollectiviteJpaRepository.rechercherDotationsCollectivitesParIdCollectiviteEtAnneeEtCodeNumerique(idCollectivite, annee,
                codeNumerique);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#recupererDossiersParDotationCollectivite(java.lang.Long)
     */
    @Override
    public List<DossierSubvention> recupererDossiersParDotationCollectivite(Long id) {
        return this.dotationCollectiviteJpaRepository.recupererDossiersParDotationCollectivite(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DotationCollectiviteRepository#creer(fr.gouv.sitde.face.transverse.entities.DotationCollectivite)
     */
    @Override
    public DotationCollectivite creer(DotationCollectivite dco) {
        return this.dotationCollectiviteJpaRepository.save(dco);
    }

    @Override
    public boolean isEstUniqueSubventionEnCoursParSousProgramme(Long idDemandeSubvention, Long idCollectivite, String descriptionProgramme) {
        if (idDemandeSubvention != null) {
            return this.dotationCollectiviteJpaRepository.isEstUniqueSubventionEnCoursParSousProgrammeAvecIdDemandeSubvention(idDemandeSubvention,
                    idCollectivite, descriptionProgramme);
        } else {
            return this.dotationCollectiviteJpaRepository.isEstUniqueSubventionEnCoursParSousProgramme(idCollectivite, descriptionProgramme);
        }

    }
	/**
	 * Rechercher perte dotation collectivite pour batch.
	 *
	 * @return the list
	 */
	@Override
	public List<DotationCollectivite> rechercherPerteDotationCollectivitePourBatch(Integer anneeEncours) {
		
		return this.dotationCollectiviteJpaRepository.rechercherPerteDotationCollectivitePourBatch(anneeEncours);
	}
}

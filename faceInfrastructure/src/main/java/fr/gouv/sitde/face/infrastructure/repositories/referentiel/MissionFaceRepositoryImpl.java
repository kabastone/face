/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.MissionFaceRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.MissionFaceJpaRepository;
import fr.gouv.sitde.face.transverse.entities.MissionFace;

/**
 * The Class MissionFaceRepositoryImpl.
 */
@Component
public class MissionFaceRepositoryImpl implements MissionFaceRepository {

    /** The mission face jpa repository. */
    @Inject
    private MissionFaceJpaRepository missionFaceJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.MissionFaceRepository#rechercherTousMissionFace()
     */
    @Override
    public MissionFace rechercherMissionFace() {
        return this.missionFaceJpaRepository.findMissionFace().orElse(null);
    }

}

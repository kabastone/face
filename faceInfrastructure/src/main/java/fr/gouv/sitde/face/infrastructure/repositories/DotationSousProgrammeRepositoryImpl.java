/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository;
import fr.gouv.sitde.face.persistance.repositories.DotationSousProgrammeJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DotationSousProgramme;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service d'acces au repository JPA DotationSousProgramme.
 *
 * @author Atos
 */
@Component
public class DotationSousProgrammeRepositoryImpl implements DotationSousProgrammeRepository {

    /** The dotation programme repository. */
    @Inject
    private DotationSousProgrammeJpaRepository dotationSousProgrammeJpaRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#rechercherDotationsSousProgrammesSur4Ans(java.lang.String)
     */
    @Override
    public List<DotationSousProgramme> rechercherDotationsSousProgrammesSur4Ans(String codeProgramme) {
        int anneeMinimale = this.timeOperationTransverse.getAnneeCourante() - 3;
        return this.dotationSousProgrammeJpaRepository.rechercherParAnneeMiniEtProgramme(codeProgramme, anneeMinimale);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#rechercherParSousProgrammeParAnneeEnCours(java.lang.String,
     * int)
     */
    @Override
    public DotationSousProgramme rechercherParSousProgrammeParAnneeEnCours(String sprCodeFonctionnel, int anneeEnCours) {
        return this.dotationSousProgrammeJpaRepository.rechercherParSousProgrammeParAnneeEnCours(sprCodeFonctionnel, anneeEnCours).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#creerDotationSousProgramme(fr.gouv.sitde.face.transverse.entities.
     * DotationSousProgramme)
     */
    @Override
    public DotationSousProgramme creerDotationSousProgramme(DotationSousProgramme dotationSousProgramme) {
        return this.dotationSousProgrammeJpaRepository.saveAndFlush(dotationSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#rechercherDotationSousProgrammeParId(java.lang.Long)
     */
    @Override
    public DotationSousProgramme rechercherDotationSousProgrammeParId(Long idDotationSousProgramme) {
        return this.dotationSousProgrammeJpaRepository.rechercherParId(idDotationSousProgramme).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#modifierDotationSousProgramme(fr.gouv.sitde.face.transverse.entities
     * .DotationSousProgramme)
     */
    @Override
    public DotationSousProgramme modifierDotationSousProgramme(DotationSousProgramme dotationSousProgramme) {
        return this.dotationSousProgrammeJpaRepository.saveAndFlush(dotationSousProgramme);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationSousProgrammeRepository#recupererDotationSousProgrammeParAnneeEtSousProgramme(int,
     * java.lang.Integer)
     */
    @Override
    public DotationSousProgramme recupererDotationSousProgrammeParAnneeEtSousProgramme(int annee, Integer idSousProgramme) {
        return this.dotationSousProgrammeJpaRepository.recupererDotationSousProgrammeParAnneeEtSousProgramme(annee, idSousProgramme);
    }

}

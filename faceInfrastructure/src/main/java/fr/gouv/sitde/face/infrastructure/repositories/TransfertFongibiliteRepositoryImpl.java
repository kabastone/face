package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.TransfertFongibiliteRepository;
import fr.gouv.sitde.face.persistance.repositories.TransfertFongibiliteJpaRepository;
import fr.gouv.sitde.face.transverse.entities.TransfertFongibilite;

@Component
public class TransfertFongibiliteRepositoryImpl implements TransfertFongibiliteRepository {
    @Inject
    private TransfertFongibiliteJpaRepository transfertFongibiliteJpaRepository;

    @Override
    public TransfertFongibilite enregistrer(TransfertFongibilite transfertFongibilite) {

        return this.transfertFongibiliteJpaRepository.saveAndFlush(transfertFongibilite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.TransfertFongibiliteRepository#rechercherTransfertsCollectiviteParAnnee(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<TransfertFongibilite> rechercherTransfertsCollectiviteParAnnee(Long idCollectivite, Integer annee) {
        return this.transfertFongibiliteJpaRepository.rechercherTransfertsCollectiviteParAnnee(idCollectivite, annee);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.TransfertFongibiliteRepository#rechercherTransfertsParAnnee(java.lang.Integer)
     */
    @Override
    public List<TransfertFongibilite> rechercherTransfertsParAnnee(Integer annee) {
        return this.transfertFongibiliteJpaRepository.rechercherTransfertsParAnnee(annee);
    }

}

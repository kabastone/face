/**
 *
 */
package fr.gouv.sitde.face.infrastructure.configuration;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class UploadCheminRepertoireProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("upload.chemin.repertoire")
@Validated
public class UploadCheminRepertoireProperties {

    /** The racine. */
    @NotEmpty
    private String racine;

    /**
     * Gets the racine.
     *
     * @return the racine
     */
    public String getRacine() {
        return this.racine;
    }

    /**
     * Sets the racine.
     *
     * @param racine
     *            the racine to set
     */
    public void setRacine(String racine) {
        this.racine = racine;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository;
import fr.gouv.sitde.face.persistance.repositories.DotationDepartementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.entities.DotationDepartement;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class DotationDepartementRepositoryImpl.
 *
 * @author Atos
 */
@Component
public class DotationDepartementRepositoryImpl implements DotationDepartementRepository {

    /** The dotation departement jpa repository. */
    @Inject
    private DotationDepartementJpaRepository dotationDepartementJpaRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#rechercherTousPourDepartements(java.util.List)
     */
    @Override
    public List<DotationDepartement> rechercherTousPourDepartements(List<Departement> listeDepartements) {
        return this.dotationDepartementJpaRepository.rechercherTousPourDepartements(listeDepartements);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#rechercherDotationDepartementTroisCriteres(int, java.lang.String,
     * java.lang.String)
     */
    @Override
    public DotationDepartement rechercherDotationDepartementTroisCriteres(Integer annee, String departementCode, String codeDomaineFonctionnel) {
        return this.dotationDepartementJpaRepository.rechercherDotationDepartementTroisCriteres(annee, departementCode, codeDomaineFonctionnel);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#ajouterDotationDepartement(fr.gouv.sitde.face.transverse.entities.
     * DotationDepartement)
     */
    @Override
    public DotationDepartement ajouterDotationDepartement(DotationDepartement dotationDepartement) {
        return this.dotationDepartementJpaRepository.save(dotationDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#rechercherParId(java.lang.Long)
     */
    @Override
    public DotationDepartement rechercherParId(Long dotationDepartementId) {
        return this.dotationDepartementJpaRepository.rechercherParId(dotationDepartementId).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#ligneDotationDepartementEnPreparationExiste(java.lang.Integer,
     * java.lang.String)
     */
    @Override
    public Boolean ligneDotationDepartementEnPreparationExiste(Integer annee, String departementCode) {
        return this.dotationDepartementJpaRepository.ligneDotationDepartementEnPreparationExiste(annee, departementCode);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#ligneDotationDepartementNotifieeExiste(java.lang.Integer,
     * java.lang.String)
     */
    @Override
    public Boolean ligneDotationDepartementNotifieeExiste(Integer annee, String departementCode) {
        return this.dotationDepartementJpaRepository.ligneDotationDepartementNotifieeExiste(annee, departementCode);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#rechercherDotationDepartementPourRepartition(java.lang.String,
     * java.lang.String)
     */
    @Override
    public DotationDepartement rechercherDotationDepartementPourRepartition(String codeDepartement, String codeDomaineFonctionnel) {
        return this.dotationDepartementJpaRepository.rechercherDotationDepartementPourRepartition(codeDepartement,
                this.timeOperationTransverse.getAnneeCourante(), codeDomaineFonctionnel);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#rechercherDotationDepartementParIdCollectivite(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public DotationDepartement rechercherDotationDepartementParIdCollectivite(Long idCollectivite, Integer idSousProgramme) {
        return this.dotationDepartementJpaRepository.rechercherDotationDepartementParIdCollectivite(idCollectivite, idSousProgramme,
                this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#creer(fr.gouv.sitde.face.transverse.entities.DotationDepartement)
     */
    @Override
    public DotationDepartement creer(DotationDepartement dotationDepartement) {
        return this.dotationDepartementJpaRepository.save(dotationDepartement);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.DotationDepartementRepository#enregistrer(fr.gouv.sitde.face.transverse.entities.
     * DotationDepartement)
     */
    @Override
    public DotationDepartement enregistrer(DotationDepartement dotationDepartement) {
        return this.dotationDepartementJpaRepository.saveAndFlush(dotationDepartement);
    }
}

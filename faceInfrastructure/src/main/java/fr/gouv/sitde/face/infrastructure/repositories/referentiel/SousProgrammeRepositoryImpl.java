/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.SousProgrammeJpaRepository;
import fr.gouv.sitde.face.transverse.entities.SousProgramme;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service d'acces au repository JPA SousProgramme .
 *
 * @author atos
 */
@Component
public class SousProgrammeRepositoryImpl implements SousProgrammeRepository {

    /** The sous programme jpa repository. */
    @Inject
    private SousProgrammeJpaRepository sousProgrammeJpaRepository;

    /** The time operation transverse. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherTousSousProgrammes()
     */
    @Override
    public List<SousProgramme> rechercherTousSousProgrammes() {
        return this.sousProgrammeJpaRepository.findAllSousProgrammes();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammesPourRenoncementDotation(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeTravauxParCollectivite(Long idCollectivite) {
        return this.sousProgrammeJpaRepository.findSousProgrammesDeTravauxParCollectivite(idCollectivite,
                this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammesDeProjetParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeProjetParCollectivite(Long idCollectivite) {
        return this.sousProgrammeJpaRepository.findSousProgrammesDeProjetParCollectivite(idCollectivite,
                this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammesDeTravauxParCollectivite(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesDeProjet() {
        return this.sousProgrammeJpaRepository.findSousProgrammesDeProjet(this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammeParAbreviation(java.lang.String)
     */
    @Override
    public SousProgramme rechercherSousProgrammeParAbreviation(String abreviation) {
        return this.sousProgrammeJpaRepository.findFirstByAbreviation(abreviation).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammesParDotationProgramme(java.lang.Long)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesParDotationProgramme(Long idDotationProgramme) {
        return this.sousProgrammeJpaRepository.rechercherSousProgrammesParDotationProgramme(idDotationProgramme,
                this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammesPourLigneDotationDepartementale(java.
     * lang.Integer)
     */
    @Override
    public List<SousProgramme> rechercherSousProgrammesPourLigneDotationDepartementale(Integer annee) {
        return this.sousProgrammeJpaRepository.rechercherSousProgrammesPourLigneDotationDepartementale(annee);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#findById(java.lang.Integer)
     */
    @Override
    public SousProgramme findById(Integer idSousProgramme) {
        return this.sousProgrammeJpaRepository.findById(idSousProgramme).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherPourDotationDepartement()
     */
    @Override
    public List<SousProgramme> rechercherPourDotationDepartement() {
        return this.sousProgrammeJpaRepository.rechercherPourDotationDepartement(this.timeOperationTransverse.getAnneeCourante());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.SousProgrammeRepository#rechercherSousProgrammeParCodeDomaineFonctionnel(java.lang.
     * String)
     */
    @Override
    public SousProgramme rechercherSousProgrammeParCodeDomaineFonctionnel(String codeDomaineFonctionnel) {
        return this.sousProgrammeJpaRepository.findSousProgrammesParCodeFonctionnel(codeDomaineFonctionnel);
    }
}

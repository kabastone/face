/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository;
import fr.gouv.sitde.face.persistance.repositories.DroitAgentJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DroitAgent;

/**
 * Service d'acces au repository JPA DroitAgent.
 *
 * @author a453029
 *
 */
@Component
public class DroitAgentRepositoryImpl implements DroitAgentRepository {

    /** The droit agent jpa repository. */
    @Inject
    private DroitAgentJpaRepository droitAgentJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository#rechercherDroitAgentParUtilisateurEtCollectivite(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public DroitAgent rechercherDroitAgentParUtilisateurEtCollectivite(Long idUtilisateur, Long idCollectivite) {

        return this.droitAgentJpaRepository.findByUtilisateurAndCollectivite(idUtilisateur, idCollectivite).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository#creerDroitAgent(fr.gouv.sitde.face.transverse.entities.DroitAgent)
     */
    @Override
    public DroitAgent creerDroitAgent(DroitAgent droitAgent) {
        return this.droitAgentJpaRepository.saveAndFlush(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository#modifierDroitAgent(fr.gouv.sitde.face.transverse.entities.DroitAgent)
     */
    @Override
    public DroitAgent modifierDroitAgent(DroitAgent droitAgent) {
        return this.droitAgentJpaRepository.saveAndFlush(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository#supprimerDroitAgent(fr.gouv.sitde.face.transverse.entities.DroitAgent)
     */
    @Override
    public void supprimerDroitAgent(DroitAgent droitAgent) {
        this.droitAgentJpaRepository.delete(droitAgent);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DroitAgentRepository#rechercherDroitsAgentsParCollectivite(java.lang.Long)
     */
    @Override
    public List<DroitAgent> rechercherDroitsAgentsParCollectivite(Long idCollectivite) {
        return this.droitAgentJpaRepository.findByCollectivite(idCollectivite);
    }

}

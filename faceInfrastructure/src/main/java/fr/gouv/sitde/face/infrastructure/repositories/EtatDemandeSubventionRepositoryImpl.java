/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeSubventionRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.EtatDemandeSubventionJpaRepository;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeSubvention;

/**
 * Service d'acces au repository JPA EtatDemandeSubvention.
 *
 * @author Atos
 */
@Component
public class EtatDemandeSubventionRepositoryImpl implements EtatDemandeSubventionRepository {

    /** The etat demande subvention jpa repository. */
    @Inject
    private EtatDemandeSubventionJpaRepository etatDemandeSubventionJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeSubventionRepository#findByCode(java.lang.String)
     */
    @Override
    public EtatDemandeSubvention findByCode(String codeEtatdemande) {
        return this.etatDemandeSubventionJpaRepository.findByCode(codeEtatdemande).orElse(null);
    }

}

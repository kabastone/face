/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeProlongationRepository;
import fr.gouv.sitde.face.persistance.repositories.EtatDemandeProlongationJpaRepository;
import fr.gouv.sitde.face.transverse.entities.EtatDemandeProlongation;

/**
 * The Class EtatDemandeProlongationRepositoryImpl.
 *
 * @author Atos
 */
@Component
public class EtatDemandeProlongationRepositoryImpl implements EtatDemandeProlongationRepository {

    /** The etat demande prolongation jpa repository. */
    @Inject
    private EtatDemandeProlongationJpaRepository etatDemandeProlongationJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.EtatDemandeProlongationRepository#findParCode(fr.gouv.sitde.face.transverse.entities.
     * EtatDemandeProlongation)
     */
    @Override
    public EtatDemandeProlongation findParCode(String codeEtatDemandeProlongation) {
        return this.etatDemandeProlongationJpaRepository.findByCode(codeEtatDemandeProlongation);
    }

}

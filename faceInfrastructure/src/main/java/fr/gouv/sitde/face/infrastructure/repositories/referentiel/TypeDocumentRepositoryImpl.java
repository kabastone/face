/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.TypeDocumentRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.TypeDocumentJpaRepository;
import fr.gouv.sitde.face.transverse.entities.TypeDocument;

/**
 * Service d'acces au repository JPA TypeDocument .
 *
 * @author a453029
 */
@Component
public class TypeDocumentRepositoryImpl implements TypeDocumentRepository {

    /** The typeDocument jpa repository. */
    @Inject
    private TypeDocumentJpaRepository typeDocumentJpaRepository;

    /**
     * Rechercher type document par id codifie.
     *
     * @param idCodifie
     *            the id codifie
     * @return the type document
     */
    @Override
    public TypeDocument rechercherTypeDocumentParIdCodifie(Integer idCodifie) {
        return this.typeDocumentJpaRepository.findByIdCodifie(idCodifie).orElse(null);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository;
import fr.gouv.sitde.face.persistance.repositories.DemandeSubventionJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;

/**
 * Service d'acces au repository JPA DemandeSubvention.
 *
 * @author Atos
 */
@Component
public class DemandeSubventionRepositoryImpl implements DemandeSubventionRepository {

    /** The demande subvention jpa repository. */
    @Inject
    private DemandeSubventionJpaRepository demandeSubventionJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#findByDossierSubvention(java.lang.Long)
     */
    @Override
    public List<DemandeSubvention> findByDossierSubvention(Long idDossierSubvention) {
        return this.demandeSubventionJpaRepository.findByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#rechercheDemandeSubventionPourPdfParId(java.lang.Long)
     */
    @Override
    public DemandeSubvention rechercheDemandeSubventionPourPdfParId(Long idDemandeSubvention) {
        return this.demandeSubventionJpaRepository.rechercheDemandeSubventionPourPdfParId(idDemandeSubvention).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#rechercheDemandeSubventionParId(java.lang.Long)
     */
    @Override
    public DemandeSubvention rechercheDemandeSubventionParId(Long idDemandeSubvention) {
        return this.demandeSubventionJpaRepository.rechercheDemandeSubventionParId(idDemandeSubvention).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#findById(java.lang.Long)
     */
    @Override
    public DemandeSubvention findById(Long idDemandeSubvention) {
        return this.demandeSubventionJpaRepository.findById(idDemandeSubvention).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#recupererMaxChorusNumLigneLegacy()
     */
    @Override
    public String recupererMaxChorusNumLigneLegacy(Long idDossier) {
        return this.demandeSubventionJpaRepository.findMaxChorusNumLigneLegacy(idDossier).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#findDemandePrincipaleByDossierChorus(java.lang.Long)
     */
    @Override
    public DemandeSubvention findDemandePrincipaleByDossierChorus(Long idDossierChorus) {
        return this.demandeSubventionJpaRepository.findPrincipaleByDossierChorus(idDossierChorus).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#findDemandePrincipaleByDossier(java.lang.Long)
     */
    @Override
    public DemandeSubvention findDemandePrincipaleByIdDossier(Long idDossier) {
        return this.demandeSubventionJpaRepository.findPrincipaleByDossier(idDossier).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#mettreAJourDemandeSubvention(fr.gouv.sitde.face.transverse.entities.
     * DemandeSubvention)
     */
    @Override
    public DemandeSubvention mettreAJourDemandeSubvention(DemandeSubvention demande) {
        return this.demandeSubventionJpaRepository.saveAndFlush(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#enregistrer(fr.gouv.sitde.face.transverse.entities.DemandeSubvention)
     */
    @Override
    public DemandeSubvention enregistrer(DemandeSubvention demande) {
        return this.demandeSubventionJpaRepository.saveAndFlush(demande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#rechercherParEJetPoste(java.lang.String, java.lang.String)
     */
    @Override
    public DemandeSubvention rechercherParEJetPoste(String numeroEJ, String numeroPoste) {
        return this.demandeSubventionJpaRepository.rechercherParEJetPoste(numeroEJ, numeroPoste);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#rechercherParEJQuantityEtat(java.lang.String, java.math.BigDecimal)
     */
    @Override
    public DemandeSubvention rechercherParEJQuantityEtat(String numeroEJ, BigDecimal plafond) {
        return this.demandeSubventionJpaRepository.rechercherParEJQuantityEtat(numeroEJ, plafond).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#obtenirDtoPourBatchFen0111A()
     */
    @Override
    public List<DemandeSubvention> obtenirDtoPourBatchFen0111A() {
        return this.demandeSubventionJpaRepository.obtenirDtoPourBatchFen0111A();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#findByDossierSubventionOrdonneeParDateDemande(java.lang.Long)
     */
    @Override
    public List<DemandeSubvention> findByDossierSubventionOrdonneeParDateDemande(Long idDossier) {
        return this.demandeSubventionJpaRepository.findByDossierSubventionOrdonneeParDateDemande(idDossier);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeSubventionRepository#compterNombreDemandeSubventionParDossier(java.lang.Long)
     */
    @Override
    public Integer compterNombreDemandeSubventionParDossier(Long idDossier) {
        return this.demandeSubventionJpaRepository.compterNombreDemandeSubventionParDossier(idDossier);
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.tableaudebord;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository;
import fr.gouv.sitde.face.infrastructure.pagination.mapper.SpringDataPaginationMapper;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.TableauDeBordMferJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeProlongationEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordMferDto;

/**
 * The Class TableauDeBordMferRepositoryImpl.
 *
 * @author a453029
 */
@Component
public class TableauDeBordMferRepositoryImpl implements TableauDeBordMferRepository {

    /** The tableau de bord mfer jpa repository. */
    @Inject
    private TableauDeBordMferJpaRepository tableauDeBordMferJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherTableauDeBordMfer()
     */
    @Override
    public TableauDeBordMferDto rechercherTableauDeBordMfer() {
        return this.tableauDeBordMferJpaRepository.rechercherTableauDeBordMfer();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesSubventionTdbMferAqualifierSubvention(fr
     * .gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAqualifierSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordMferJpaRepository.rechercherDemandesSubventionTdbAqualifierSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.DEMANDEE.toString(), EtatDemandeSubventionEnum.CORRIGEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesPaiementTdbMferAqualifierPaiement(fr.
     * gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAqualifierPaiement(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordMferJpaRepository.rechercherDemandesPaiementTdbAqualifierPaiement(EtatDossierEnum.OUVERT,
                EtatDemandePaiementEnum.DEMANDEE.toString(), EtatDemandePaiementEnum.CORRIGEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesSubventionTdbMferAaccorderSubvention(fr.
     * gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAaccorderSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordMferJpaRepository.rechercherDemandesSubventionTdbAaccorderSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.QUALIFIEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesPaiementTdbMferAaccorderPaiement(fr.gouv
     * .sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAaccorderPaiement(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordMferJpaRepository.rechercherDemandesPaiementTdbAaccorderPaiement(EtatDossierEnum.OUVERT,
                EtatDemandePaiementEnum.QUALIFIEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesSubventionTdbMferAattribuerSubvention(fr
     * .gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAattribuerSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordMferJpaRepository.rechercherDemandesSubventionTdbAattribuerSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.APPROUVEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesSubventionTdbMferAsignalerSubvention(fr.
     * gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbMferAsignalerSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordMferJpaRepository.rechercherDemandesSubventionTdbAsignalerSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.ANOMALIE_DETECTEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesPaiementTdbMferAsignalerPaiement(fr.gouv
     * .sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbMferAsignalerPaiement(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordMferJpaRepository.rechercherDemandesPaiementTdbAsignalerPaiement(EtatDossierEnum.OUVERT,
                EtatDemandePaiementEnum.ANOMALIE_DETECTEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordMferRepository#rechercherDemandesProlongationTdbMferAprolongerSubvention(
     * fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeProlongation> rechercherDemandesProlongationTdbMferAprolongerSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeProlongation> page = this.tableauDeBordMferJpaRepository
                .rechercherDemandesProlongationTdbAprolongerSubvention(EtatDemandeProlongationEnum.DEMANDEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

}

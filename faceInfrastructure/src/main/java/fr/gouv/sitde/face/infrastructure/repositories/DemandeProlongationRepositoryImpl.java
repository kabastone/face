/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository;
import fr.gouv.sitde.face.persistance.repositories.DemandeProlongationJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandeProlongation;

/**
 * Service d'acces au repository JPA DemandePaiement.
 *
 * @author Atos
 */
@Component
public class DemandeProlongationRepositoryImpl implements DemandeProlongationRepository {

    /** The demande prolongation jpa repository. */
    @Inject
    private DemandeProlongationJpaRepository demandeProlongationJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository#findByDossierSubvention(java.lang.Long)
     */
    @Override
    public List<DemandeProlongation> findByDossierSubvention(Long idDossierSubvention) {
        return this.demandeProlongationJpaRepository.findByDossierSubvention(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository#rechercherDemandeProlongation(java.lang.Long)
     */
    @Override
    public DemandeProlongation rechercherDemandeProlongation(Long idDemandeProlongation) {
        return this.demandeProlongationJpaRepository.rechercherDemandeProlongation(idDemandeProlongation).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository#rechercheParDateAchevementEtDossierSubvention(java.time.
     * LocalDateTime, java.lang.String)
     */
    @Override
    public DemandeProlongation rechercheParDateAchevementEtDossierSubvention(LocalDateTime dateLivraisonDuPoste, String numeroEJ) {
        return this.demandeProlongationJpaRepository.rechercheParDateAchevementEtDossierSubvention(dateLivraisonDuPoste, numeroEJ).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository#mettreAJourDemandeProlongation(fr.gouv.sitde.face.transverse.entities.
     * DemandeProlongation)
     */
    @Override
    public DemandeProlongation mettreAJourDemandeProlongation(DemandeProlongation demandeProlongation) {
        return this.demandeProlongationJpaRepository.saveAndFlush(demandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DemandeProlongationRepository#enregistrer(fr.gouv.sitde.face.transverse.entities.
     * DemandeProlongation)
     */
    @Override
    public DemandeProlongation enregistrer(DemandeProlongation demandeProlongation) {
        return this.demandeProlongationJpaRepository.saveAndFlush(demandeProlongation);
    }

}

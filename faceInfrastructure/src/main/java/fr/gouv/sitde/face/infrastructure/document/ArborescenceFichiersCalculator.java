/**
 *
 */
package fr.gouv.sitde.face.infrastructure.document;

import java.io.File;
import java.time.LocalDateTime;

import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.FamilleDocumentEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;

/**
 * The Class ArborescenceFichiersCalculator.
 *
 * @author a453029
 */
public final class ArborescenceFichiersCalculator {

    /** The Constant NOM_REPERTOIRE_ANNEE. */
    private static final String NOM_REPERTOIRE_ANNEE = "ANNEE";

    /** The Constant NOM_REPERTOIRE_MODELE. */
    private static final String NOM_REPERTOIRE_MODELE = "MODELE";

    /** The Constant NOM_REPERTOIRE_DOSSIER. */
    private static final String NOM_REPERTOIRE_DOSSIER = "DOSSIER";

    /** The Constant NOM_REPERTOIRE_DOTA_DEP. */
    private static final String NOM_REPERTOIRE_DOTA_DEP = "DOTA-DEP";

    /** The Constant NOM_REPERTOIRE_COU_DEP. */
    private static final String NOM_REPERTOIRE_COU_DEP = "COU-DEP";

    /** The Constant NOM_REPERTOIRE_LIGNE_DOTA_DEP. */
    private static final String NOM_REPERTOIRE_LIGNE_DOTA_DEP = "LI-DOTA-DEP";

    /** The Constant NOM_REPERTOIRE_DEM_SUBVENTION. */
    private static final String NOM_REPERTOIRE_DEM_SUBVENTION = "DEM-SUB";

    /** The Constant NOM_REPERTOIRE_DEM_PROLONGATION. */
    private static final String NOM_REPERTOIRE_DEM_PROLONGATION = "DEM-PRO";

    /** The Constant NOM_REPERTOIRE_DEM_PAIEMENT. */
    private static final String NOM_REPERTOIRE_DEM_PAIEMENT = "DEM-PAI";

    /**
     * Constructeur privé.
     */
    private ArborescenceFichiersCalculator() {
        // la classe ne possède que des membres et méthodes statiques.
    }

    /**
     * Gets the chemin repertoire document.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire document
     */
    public static String getCheminRepertoireDocument(Document document, String repertoireRacine) {

        if (document.getTypeDocument() == null) {
            throw new TechniqueException("Le document n'a pas de type de document");
        }

        TypeDocumentEnum typeDocumentEnum = TypeDocumentEnum.rechercherEnumParIdCodifie(document.getTypeDocument().getIdCodifie());
        FamilleDocumentEnum familleDocumentEnum = TypeDocumentEnum.getFamilleFromTypeDoc(typeDocumentEnum);

        // Récupération du chemin du fichier selon la famille du document
        String cheminRepertoireDoc = null;

        switch (familleDocumentEnum) {
            case DOC_COURRIER_ANNUEL_DEPARTEMENT:
                if (document.getCourrierAnnuelDepartement() == null) {
                    throw new TechniqueException("Le document du courrier annuel du département n'a pas de courrierAnnuelDepartement");
                }
                cheminRepertoireDoc = getCheminRepertoireDocCourriernDepartement(document, repertoireRacine);
                break;
            case DOC_LIGNE_DOTATION_DEPARTEMENT:
                if ((document.getLigneDotationDepartement() == null) || (document.getLigneDotationDepartement().getDotationDepartement() == null)) {
                    throw new TechniqueException(
                            "Le document de dotation de département n'a pas de ligneDotationDepartement ou de dotationDepartement");
                }
                cheminRepertoireDoc = getCheminRepertoireDocLigneDotationDepartement(document, repertoireRacine);
                break;
            case DOC_DEMANDE_SUBVENTION:
                if ((document.getDemandeSubvention() == null) || (document.getDemandeSubvention().getDossierSubvention() == null)) {
                    throw new TechniqueException("Le document de demande de subvention n'a pas de demandeSubvention ou de dossierSubvention");
                }
                cheminRepertoireDoc = getCheminRepertoireDocDemandeSubvention(document, repertoireRacine);
                break;
            case DOC_DEMANDE_PROLONGATION:
                if ((document.getDemandeProlongation() == null) || (document.getDemandeProlongation().getDossierSubvention() == null)) {
                    throw new TechniqueException("Le document de demande de prolongation n'a pas de demandeProlongation ou de dossierSubvention");
                }
                cheminRepertoireDoc = getCheminRepertoireDocDemandeProlongation(document, repertoireRacine);
                break;
            case DOC_DEMANDE_PAIEMENT:
                if ((document.getDemandePaiement() == null) || (document.getDemandePaiement().getDossierSubvention() == null)) {
                    throw new TechniqueException("Le document de demande de paiement n'a pas de demandePaiement ou de dossierSubvention");
                }
                cheminRepertoireDoc = getCheminRepertoireDocDemandePaiement(document, repertoireRacine);
                break;
            case DOC_MODELE:
                cheminRepertoireDoc = getCheminRepertoireDocModele(repertoireRacine);
                break;
        }

        return cheminRepertoireDoc;
    }

    /**
     * Methode permettant de calculer le dossier d'une demande de subvention.
     *
     * Indépendement d'un fichier réel
     *
     * N'est utilisé que pour déplacer le contenu de ce répertoire dans le répertoire du nouveau dossier de subvention
     *
     * @param document
     * @param repertoireRacine
     * @return
     */
    public static String getCheminRepertoireDemandeSubvention(Document document, String repertoireRacine) {

        // Récupération du chemin du fichier selon la famille du document
        return getCheminRepertoireDocDemandeSubvention(document, repertoireRacine);
    }

    /**
     * Gets the chemin repertoire doc modele.
     *
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc modele
     */
    private static String getCheminRepertoireDocModele(String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout du sous-pertoire des modeles
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_MODELE);

        return cheminRepertoire.toString();
    }

    /**
     * Gets the chemin repertoire doc demande subvention.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc demande subvention
     */
    private static String getCheminRepertoireDocDemandeSubvention(Document document, String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout des sous-repertoires annee et valeur de l'année au chemin
        LocalDateTime dateCreationDossier = document.getDemandeSubvention().getDossierSubvention().getDateCreationDossier();
        ajouterRepertoiresAnneeAChemin(cheminRepertoire, dateCreationDossier);

        // Ajout des sous-repertoires DOSSIER et id de dossier
        Long idDossier = document.getDemandeSubvention().getDossierSubvention().getId();
        ajouterRepertoiresDossierAChemin(cheminRepertoire, idDossier);

        // Ajout du sous-repertoire demande subvention
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_DEM_SUBVENTION);

        // Ajout du sous-repertoire d'id de demande subvention
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(document.getDemandeSubvention().getId());

        return cheminRepertoire.toString();
    }

    /**
     * Gets the chemin repertoire doc demande prolongation.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc demande prolongation
     */
    private static String getCheminRepertoireDocDemandeProlongation(Document document, String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout des sous-repertoires annee et valeur de l'année au chemin
        LocalDateTime dateCreationDossier = document.getDemandeProlongation().getDossierSubvention().getDateCreation();
        ajouterRepertoiresAnneeAChemin(cheminRepertoire, dateCreationDossier);

        // Ajout des sous-repertoires DOSSIER et id de dossier
        Long idDossier = document.getDemandeProlongation().getDossierSubvention().getId();
        ajouterRepertoiresDossierAChemin(cheminRepertoire, idDossier);

        // Ajout du sous-repertoire demande prolongation
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_DEM_PROLONGATION);

        // Ajout du sous-repertoire d'id de demande prolongation
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(document.getDemandeProlongation().getId());

        return cheminRepertoire.toString();
    }

    /**
     * Gets the chemin repertoire doc demande paiement.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc demande paiement
     */
    private static String getCheminRepertoireDocDemandePaiement(Document document, String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout des sous-repertoires annee et valeur de l'année au chemin
        LocalDateTime dateCreationDossier = document.getDemandePaiement().getDossierSubvention().getDateCreationDossier();
        ajouterRepertoiresAnneeAChemin(cheminRepertoire, dateCreationDossier);

        // Ajout des sous-repertoires DOSSIER et id de dossier
        Long idDossier = document.getDemandePaiement().getDossierSubvention().getId();
        ajouterRepertoiresDossierAChemin(cheminRepertoire, idDossier);

        // Ajout du sous-repertoire demande paiement
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_DEM_PAIEMENT);

        // Ajout du sous-repertoire d'id de demande paiement
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(document.getDemandePaiement().getId());

        return cheminRepertoire.toString();
    }

    /**
     * Gets the chemin repertoire doc courrier departement.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc courrier departement
     */
    private static String getCheminRepertoireDocCourriernDepartement(Document document, String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout des sous-repertoires annee et valeur de l'année au chemin
        LocalDateTime dateCreationDotationDepartement = document.getCourrierAnnuelDepartement().getDateCreation();
        ajouterRepertoiresAnneeAChemin(cheminRepertoire, dateCreationDotationDepartement);

        // Ajout des sous-repertoires cou-dep et id cou dep
        Long idCourrierDepartement = document.getCourrierAnnuelDepartement().getId();
        ajouterRepertoireCourrierDepartementAChemin(cheminRepertoire, idCourrierDepartement);

        return cheminRepertoire.toString();
    }

    /**
     * Gets the chemin repertoire doc ligne dotation departement.
     *
     * @param document
     *            the document
     * @param repertoireRacine
     *            the repertoire racine
     * @return the chemin repertoire doc ligne dotation departement
     */
    private static String getCheminRepertoireDocLigneDotationDepartement(Document document, String repertoireRacine) {
        StringBuilder cheminRepertoire = new StringBuilder(repertoireRacine);

        // Ajout des sous-repertoires annee et valeur de l'année au chemin
        LocalDateTime dateCreationDotationDepartement = document.getLigneDotationDepartement().getDotationDepartement().getDateCreation();
        ajouterRepertoiresAnneeAChemin(cheminRepertoire, dateCreationDotationDepartement);

        // Ajout du sous-repertoire ligne de dotation de departement
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_DOTA_DEP);

        // Ajout des sous-repertoires dota-dep et id dota dep
        Long idDotationDepartement = document.getLigneDotationDepartement().getDotationDepartement().getId();
        ajouterRepertoiresDossierAChemin(cheminRepertoire, idDotationDepartement);

        // Ajout du sous-repertoire ligne de dotation de departement
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(NOM_REPERTOIRE_LIGNE_DOTA_DEP);

        // Ajout du sous-repertoire d'id de ligne de dotation de departement
        cheminRepertoire.append(File.separator);
        cheminRepertoire.append(document.getLigneDotationDepartement().getId());

        return cheminRepertoire.toString();
    }

    /**
     * Ajouter repertoires annee A chemin.
     *
     * @param chemin
     *            the chemin
     * @param dateAnneeClassification
     *            the LocalDateTime annee classification
     */
    private static void ajouterRepertoiresAnneeAChemin(StringBuilder chemin, LocalDateTime dateAnneeClassification) {

        // Ajout du sous-repertoire Annee
        chemin.append(File.separator);
        chemin.append(NOM_REPERTOIRE_ANNEE);

        // Ajout du sous-repertoire de la valeur de l'année
        int annee = dateAnneeClassification.getYear();
        chemin.append(File.separator);
        chemin.append(annee);
    }

    /**
     * Ajouter repertoires dossier A chemin.
     *
     * @param chemin
     *            the chemin
     * @param idDossier
     *            the id dossier
     */
    private static void ajouterRepertoiresDossierAChemin(StringBuilder chemin, Long idDossier) {

        // Ajout du sous-repertoire DOSSIER
        chemin.append(File.separator);
        chemin.append(NOM_REPERTOIRE_DOSSIER);

        // Ajout du sous-repertoire d'idDossier
        chemin.append(File.separator);
        chemin.append(idDossier);
    }

    /**
     * Ajouter repertoires courrier departement A chemin.
     *
     * @param chemin
     *            the chemin
     * @param idCourrierDepartement
     *            the id courrier departement
     */
    private static void ajouterRepertoireCourrierDepartementAChemin(StringBuilder chemin, Long idCourrierDepartement) {

        // Ajout du sous-repertoire COU-DEP
        chemin.append(File.separator);
        chemin.append(NOM_REPERTOIRE_COU_DEP);

        // Ajout du sous-repertoire d'id de cou departement
        chemin.append(File.separator);
        chemin.append(idCourrierDepartement);
    }

}

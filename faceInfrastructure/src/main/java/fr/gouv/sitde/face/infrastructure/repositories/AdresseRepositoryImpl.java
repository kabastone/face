/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository;
import fr.gouv.sitde.face.persistance.repositories.AdresseJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Adresse;

/**
 * The Class AdresseRepositoryImpl.
 *
 * @author Atos
 */
@Component
public class AdresseRepositoryImpl implements AdresseRepository {

    /** The adresse jpa repository. */
    @Inject
    private AdresseJpaRepository adresseJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository#rechercherParId(java.lang.Long)
     */
    @Override
    public Adresse rechercherParId(Long idAdresse) {
        return this.adresseJpaRepository.findById(idAdresse).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository#modifierAdresse(fr.gouv.sitde.face.transverse.entities.Adresse)
     */
    @Override
    public Adresse modifierAdresse(Adresse adresse) {
        return this.adresseJpaRepository.saveAndFlush(adresse);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository#creerAdresse(fr.gouv.sitde.face.transverse.entities.Adresse)
     */
    @Override
    public Adresse creerAdresse(Adresse adresse) {
        return this.adresseJpaRepository.saveAndFlush(adresse);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AdresseRepository#rechercherParDepartement(java.lang.Integer)
     */
    @Override
    public Adresse rechercherParDepartement(Integer idDepartement) {
        return this.adresseJpaRepository.rechercherParDepartement(idDepartement).orElse(null);
    }

}

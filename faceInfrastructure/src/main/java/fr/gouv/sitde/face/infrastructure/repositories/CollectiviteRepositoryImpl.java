/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository;
import fr.gouv.sitde.face.persistance.repositories.CollectiviteJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Collectivite;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class CollectiviteRepositoryImpl.
 *
 * @author Atos
 */
@Component
public class CollectiviteRepositoryImpl implements CollectiviteRepository {

    /** The collectivite jpa repository. */
    @Inject
    private CollectiviteJpaRepository collectiviteJpaRepository;

    @Inject
    private TimeOperationTransverse timeUtils;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherCollectivitesParUtilisateurId(java.lang.Long)
     */
    @Override
    public List<Collectivite> rechercherCollectivites() {
        return this.collectiviteJpaRepository.findAll();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherParId(java.lang.Long)
     */
    @Override
    public Collectivite rechercherParId(Long idCollectivite) {
        return this.collectiviteJpaRepository.rechercherById(idCollectivite).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherCollectivitesParCodeDepartement(java.lang.String)
     */
    @Override
    public List<Collectivite> rechercherCollectivitesParCodeDepartement(String codeDepartement) {
        return this.collectiviteJpaRepository.findCollectivitesWithMailListByCodeDepartement(codeDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#modifierCollectivite(fr.gouv.sitde.face.transverse.entities.Collectivite)
     */
    @Override
    public Collectivite modifierCollectivite(Collectivite collectivite) {
        return this.collectiviteJpaRepository.saveAndFlush(collectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#creerCollectivite(fr.gouv.sitde.face.transverse.entities.Collectivite)
     */
    @Override
    public Collectivite creerCollectivite(Collectivite collectivite) {
        return this.collectiviteJpaRepository.save(collectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDotationCollectivite(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDotationCollectivite(Long idDotationCollectivite) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDotationCollectivite(idDotationCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDossierSubvention(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDossierSubvention(Long idDossierSubvention) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDossier(idDossierSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDemandeSubvention(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeSubvention(Long idDemandeSubvention) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDemandeSubvention(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDemandeProlongation(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandeProlongation(Long idDemandeProlongation) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDemandeProlongation(idDemandeProlongation);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDemandePaiement(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDemandePaiement(Long idDemandePaiement) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDemandePaiement(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDocumentDemandeSubvention(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDocumentDemandeSubvention(Long idDocument) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDocDemandeSubvention(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDocumentDemandeProlongation(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDocumentDemandeProlongation(Long idDocument) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDocDemandeProlongation(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectiviteParDocumentDemandePaiement(java.lang.Long)
     */
    @Override
    public Long rechercherIdCollectiviteParDocumentDemandePaiement(Long idDocument) {
        return this.collectiviteJpaRepository.findIdCollectiviteByIdDocDemandePaiement(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectivitesParDocumentCourrierDepartement(java.lang.Long)
     */
    @Override
    public List<Long> rechercherIdCollectivitesParDocumentCourrierDepartement(Long idDocument) {
        return this.collectiviteJpaRepository.findIdCollectivitesByIdDocCourrierDepartement(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherIdCollectivitesParDocumentLigneDotationDepartement(java.lang.Long)
     */
    @Override
    public List<Long> rechercherIdCollectivitesParDocumentLigneDotationDepartement(Long idDocument) {
        return this.collectiviteJpaRepository.findIdCollectivitesByIdDocLigneDotationDepartement(idDocument);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherToutesCollectivitesPourMail()
     */
    @Override
    public List<Collectivite> rechercherToutesCollectivitesPourMail() {
        return this.collectiviteJpaRepository.findAllCollectiviteAvecMail();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#verifierSiretUnicité(fr.gouv.sitde.face.transverse.entities.Collectivite)
     */
    @Override
    public boolean verifierSiretUnicite(Collectivite collectivite) {
        String siret = collectivite.getSiret();
        Long idCollectivite = collectivite.getId();
        if (collectivite.getId() != null) {
            return this.collectiviteJpaRepository.existsByCollectiviteAndSiret(idCollectivite, siret);
        } else {
            return this.collectiviteJpaRepository.existsBySiret(siret);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherCollectivitePourCadencementMaj()
     */
    @Override
    public List<Collectivite> rechercherCollectivitePourCadencementMaj() {
        return this.collectiviteJpaRepository.rechercherCollectivitePourCadencementMaj(this.timeUtils.getAnneeCourante() - 1);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CollectiviteRepository#rechercherCollectivitePourMailCadencementNonValide()
     */
    @Override
    public List<Collectivite> rechercherCollectivitePourMailCadencementNonValide() {
        return this.collectiviteJpaRepository.rechercherCollectivitePourMailCadencementNonValide();
    }

    @Override
    public List<Collectivite> rechercherCollectivitePourMailDotationNonConsomme() {
        return this.collectiviteJpaRepository.rechercherCollectivitePourMailDotationNonConsomme(this.timeUtils.getAnneeCourante());
    }

}

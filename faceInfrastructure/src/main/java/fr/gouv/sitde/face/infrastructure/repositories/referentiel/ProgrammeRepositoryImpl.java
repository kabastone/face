/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.referentiel;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.referentiel.ProgrammeRepository;
import fr.gouv.sitde.face.persistance.repositories.referentiel.ProgrammeJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Programme;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class ProgrammeRepositoryImpl.
 *
 * @author a453029
 */
@Component
public class ProgrammeRepositoryImpl implements ProgrammeRepository {

    /** The programme jpa repository. */
    @Inject
    private ProgrammeJpaRepository programmeJpaRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.referentiel.ProgrammeRepository#rechercherProgrammesSansDotationPourAnneeEnCours()
     */
    @Override
    public List<Programme> rechercherProgrammesSansDotationPourAnneeEnCours() {
        return this.programmeJpaRepository.rechercherProgrammesSansDotation(this.timeOperationTransverse.getAnneeCourante());
    }

}

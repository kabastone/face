/**
 *
 */
package fr.gouv.sitde.face.infrastructure.document;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import fr.gouv.sitde.face.domain.spi.document.ConversionNombreService;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import pl.allegro.finance.tradukisto.ValueConverters;

/**
 * The Class ConversionNombreServiceImpl.
 *
 * @author A754839
 */
@Service
public class ConversionNombreServiceImpl implements ConversionNombreService {

    private static final int TAILLE_MAX_MONTANT_CHORUS = 14;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.document.ConversionNombreService#conversionBigDecimalEnToutesLettres(java.math.BigDecimal)
     */
    @Override
    public String conversionBigDecimalEnToutesLettres(BigDecimal nombre) {
        String monnaie;
        final int million = 1_000_000;

        BigDecimal nombreScaled = nombre.setScale(2, BigDecimal.ROUND_HALF_UP);
        ValueConverters converter = ValueConverters.FRENCH_INTEGER;

        // Création d'un StringBuilder devant contenir le nombre en toutes lettres.
        StringBuilder texte = new StringBuilder();

        // Obtention des valeurs entières correspondant aux parties entières et décimales du nombre.
        int partieEntiere = nombreScaled.intValue();
        BigDecimal partieDecimale = nombreScaled.subtract(new BigDecimal(partieEntiere));
        BigDecimal partieDecimaleValeur = partieDecimale.multiply(new BigDecimal(100));
        int partieDecimaleInt = partieDecimaleValeur.intValue();

        // Tradukisto : traduction de la partie entière et accord du terme euro.
        texte.append(converter.asWords(partieEntiere));
        if ((partieEntiere % million) == 0) {
            monnaie = " d\'euros";
        } else {
            monnaie = (partieEntiere > 1) ? " euros" : " euro";
        }
        texte.append(monnaie);

        // Tradukisto : traduction de la partie décimale si existante, et accord du terme centime si besoin.
        if (partieDecimaleInt > 0) {
            texte.append(" et ");
            texte.append(converter.asWords(partieDecimaleInt));
            String cents = (partieDecimaleInt > 1) ? " centimes" : " centime";
            texte.append(cents);
        }

        return texte.toString();
    }

    @Override
    public BigDecimal conversionBigDecimalEnKiloEuros(BigDecimal nombre) {

        final BigDecimal mille = new BigDecimal(1_000);

        BigDecimal kiloEuros = nombre.divide(mille);
        return kiloEuros.setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.document.ConversionNombreService#formatageChorusMontant(java.math.BigDecimal)
     */
    @Override
    public String formatageChorusMontant(BigDecimal montant) {
        final BigDecimal montantMax = new BigDecimal(1_000_000_000);
        if (montant.compareTo(montantMax) >= 0) {
            throw new TechniqueException("Montant supérieur au maximum supporté par Chorus");
        }

        StringBuilder builder = new StringBuilder();
        builder.append(montant.setScale(3, RoundingMode.HALF_UP).toPlainString());
        while (builder.length() < TAILLE_MAX_MONTANT_CHORUS) {
            builder.insert(0, 0);
        }
        builder.insert(0, montant.signum() == -1 ? "-" : "+");
        return builder.toString();
    }

}

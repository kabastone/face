/**
 *
 */
package fr.gouv.sitde.face.infrastructure.configuration;

import java.nio.charset.StandardCharsets;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * The Class ThymeleafConfig.
 *
 * @author a453029
 */
@Configuration
public class ThymeleafConfig {

    /**
     * Mail html template engine.<br/>
     * Configuration mail HTML Templating ThymeLeaf (email content)
     *
     * @return the template engine
     */
    @Bean
    public TemplateEngine mailHtmlTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(mailHtmlTemplateResolver());
        return templateEngine;
    }

    /**
     * Mail text template engine.<br/>
     * Configuration mail Text Templating ThymeLeaf (email subject)
     *
     * @return the template engine
     */
    @Bean
    public TemplateEngine mailTextTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(mailTextTemplateResolver());
        return templateEngine;
    }

    /**
     * Document html template engine.<br/>
     * Configuration document HTML Templating ThymeLeaf
     *
     *
     * @return the template engine
     */
    @Bean
    public TemplateEngine documentHtmlTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(documentHtmlTemplateResolver());
        return templateEngine;
    }

    /**
     * Mail html template resolver.
     *
     * @return the i template resolver
     */
    private static ITemplateResolver mailHtmlTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/mail/template/corps/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

    /**
     * Mail text template resolver.
     *
     * @return the i template resolver
     */
    private static ITemplateResolver mailTextTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/mail/template/sujet/");
        templateResolver.setSuffix(".txt");
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

    /**
     * Document html template resolver.
     *
     * @return the i template resolver
     */
    private static ITemplateResolver documentHtmlTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/document/template/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

}

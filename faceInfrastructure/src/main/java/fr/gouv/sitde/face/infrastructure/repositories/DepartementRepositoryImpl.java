package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository;
import fr.gouv.sitde.face.persistance.repositories.DepartementJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Departement;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;

/**
 * The Class DepartementRepositoryImpl.
 */
@Component
public class DepartementRepositoryImpl implements DepartementRepository {

    /** The departement jpa repository. */
    @Inject
    private DepartementJpaRepository departementJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDepartements()
     */
    @Override
    public List<Departement> rechercherDepartements() {
        return this.departementJpaRepository.findAllDepartements();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDepartementsPagine(fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<Departement> rechercherDepartementsPagine(PageDemande pageDemande) {
        return this.departementJpaRepository.findAllDepartementsPagine(pageDemande);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDepartementById(java.lang.Integer)
     */
    @Override
    public Departement rechercherDepartementById(Integer id) {
        return this.departementJpaRepository.getDepartementParId(id).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#compterCollectivitePourDepartement(java.lang.Integer)
     */
    @Override
    public Integer compterCollectivitePourDepartement(Integer idDepartement) {
        return this.departementJpaRepository.compterCollectivite(idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#getCodeParId(java.lang.Integer)
     */
    @Override
    public String getCodeParId(Integer idDepartement) {
        return this.departementJpaRepository.getCodeParId(idDepartement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#majDepartement(fr.gouv.sitde.face.transverse.entities.Departement)
     */
    @Override
    public Departement majDepartement(Departement departement) {
        return this.departementJpaRepository.saveAndFlush(departement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDepartementParCode(java.lang.String)
     */
    @Override
    public Departement rechercherDepartementParCode(String code) {
        return this.departementJpaRepository.getDepartementParCode(code).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DepartementRepository#rechercherDepartementByIdCollectivite(java.lang.Long)
     */
    @Override
    public Departement rechercherDepartementByIdCollectivite(Long idCollectivite) {
        return this.departementJpaRepository.rechercherParIdCollectivite(idCollectivite);
    }
}

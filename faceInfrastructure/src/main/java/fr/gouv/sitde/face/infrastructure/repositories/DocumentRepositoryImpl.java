/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository;
import fr.gouv.sitde.face.persistance.repositories.DocumentJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Document;
import fr.gouv.sitde.face.transverse.entities.FamilleDocument;
import fr.gouv.sitde.face.transverse.referentiel.TypeDocumentEnum;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service d'acces au repository JPA Document.
 *
 * @author a453029
 */
@Component
public class DocumentRepositoryImpl implements DocumentRepository {

    /** The document jpa repository. */
    @Inject
    private DocumentJpaRepository documentJpaRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#creerDocument(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public Document creerDocument(Document document) {
        return this.documentJpaRepository.saveAndFlush(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#modifierDocument(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public Document modifierDocument(Document document) {
        return this.documentJpaRepository.saveAndFlush(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#supprimerDocument(fr.gouv.sitde.face.transverse.entities.Document)
     */
    @Override
    public void supprimerDocument(Document document) {
        this.documentJpaRepository.delete(document);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentDemandeSubventionParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentDemandeSubventionParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentDemandeSubventionById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentDemandeProlongationParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentDemandeProlongationParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentDemandeProlongationById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentDemandePaiementParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentDemandePaiementParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentDemandePaiementById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentDotationDepartementParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentCourrierAnnuelDepartementParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentCourrierAnnuelDepartementById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentLigneDotationDepartementParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentLigneDotationDepartementParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentLigneDotationDepartementById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentModeleParIdDoc(java.lang.Long)
     */
    @Override
    public Document rechercherDocumentModeleParIdDoc(Long idDocument) {
        return this.documentJpaRepository.findDocumentModeleById(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherFamilleDocumentParIdDocument(java.lang.Long)
     */
    @Override
    public FamilleDocument rechercherFamilleDocumentParIdDocument(Long idDocument) {
        return this.documentJpaRepository.findFamilleDocumentByIdDocument(idDocument).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentsDotationParIdCollectiviteEtAnnee(java.lang.Long,
     * java.lang.Integer)
     */
    @Override
    public List<Document> rechercherDocumentsDotationParIdCollectiviteEtAnnee(Long idCollectivite, Integer annee) {
        return this.documentJpaRepository.rechercherDocumentsDotationParIdCollectiviteEtAnnee(idCollectivite, annee,
                TypeDocumentEnum.COURRIER_REPARTITION.toString(), TypeDocumentEnum.COURRIER_DOTATION.toString(),
                TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION.toString());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.DocumentRepository#rechercherDocumentsDotationParCodeDepartement(java.lang.String)
     */
    @Override
    public List<Document> rechercherDocumentsDotationParCodeDepartement(String codeDepartement) {
        return this.documentJpaRepository.rechercherDocumentsDotationParCodeDepartementEtAnnee(codeDepartement, this.timeOperationTransverse.getAnneeCourante(),
                TypeDocumentEnum.COURRIER_REPARTITION.toString(), TypeDocumentEnum.COURRIER_DOTATION.toString(),
                TypeDocumentEnum.RENONCEMENT_MONTANT_DOTATION.toString());
    }
}

package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository;
import fr.gouv.sitde.face.persistance.repositories.AnomalieJpaRepository;
import fr.gouv.sitde.face.transverse.entities.Anomalie;

/**
 * The Class AnomalieRepositoryImpl.
 */
@Component
public class AnomalieRepositoryImpl implements AnomalieRepository {

    /** The anomalie jpa repository. */
    @Inject
    private AnomalieJpaRepository anomalieJpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#rechercherAnomalieParId(java.lang.Long)
     */
    @Override
    public Anomalie rechercherAnomalieParId(Long idAnomalie) {
        return this.anomalieJpaRepository.findById(idAnomalie).orElse(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#enregistrer(fr.gouv.sitde.face.transverse.entities.Anomalie)
     */
    @Override
    public Anomalie modifierAnomalie(Anomalie anomalie) {
        return this.anomalieJpaRepository.saveAndFlush(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#enregistrerSansFlush(fr.gouv.sitde.face.transverse.entities.Anomalie)
     */
    @Override
    public Anomalie creerAnomalie(Anomalie anomalie) {
        return this.anomalieJpaRepository.save(anomalie);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#rechercherAnomaliesParIdDemandePaiement(java.lang.Long)
     */
    @Override
    public Set<Anomalie> rechercherAnomaliesParIdDemandePaiement(Long idDemandePaiement) {
        return this.anomalieJpaRepository.rechercherAnomaliesParIdDemandePaiement(idDemandePaiement);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#rechercherAnomaliesParIdDemandeSubvention(java.lang.Long)
     */
    @Override
    public Set<Anomalie> rechercherAnomaliesParIdDemandeSubvention(Long idDemandeSubvention) {
        return this.anomalieJpaRepository.rechercherAnomaliesParIdDemandeSubvention(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#rendreAnomaliesDemandeSubventionVisiblesPourAODE(java.lang.Long)
     */
    @Override
    public void rendreAnomaliesDemandeSubventionVisiblesPourAODE(Long idDemandeSubvention) {
        this.anomalieJpaRepository.rendreAnomaliesDemandeSubventionVisiblesPourAODE(idDemandeSubvention);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.gouv.sitde.face.domain.spi.repositories.AnomalieRepository#rendreAnomaliesDemandePaiementVisiblesPourAODE(java.lang.Long)
     */
    @Override
    public void rendreAnomaliesDemandePaiementVisiblesPourAODE(Long idDemandePaiement) {
        this.anomalieJpaRepository.rendreAnomaliesDemandePaiementVisiblesPourAODE(idDemandePaiement);
    }
}

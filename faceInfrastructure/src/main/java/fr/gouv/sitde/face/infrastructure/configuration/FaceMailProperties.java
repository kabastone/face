/**
 *
 */
package fr.gouv.sitde.face.infrastructure.configuration;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * The Class FaceMailProperties.
 *
 * @author a453029
 */
@Component
@ConfigurationProperties("face.mail")
@Validated
public class FaceMailProperties {

    /** The from serveur adresse. */
    @NotEmpty
    private String fromServeurAdresse;

    /** The from serveur name. */
    @NotEmpty
    private String fromServeurName;

    /** The reply to adresse. */
    @NotEmpty
    private String replyToAdresse;

    /** The reply to name. */
    @NotEmpty
    private String replyToName;

    /** The adresse contact. */
    @NotEmpty
    private String adresseContact;

    /**
     * Gets the from serveur adresse.
     *
     * @return the fromServeurAdresse
     */
    public String getFromServeurAdresse() {
        return this.fromServeurAdresse;
    }

    /**
     * Sets the from serveur adresse.
     *
     * @param fromServeurAdresse
     *            the fromServeurAdresse to set
     */
    public void setFromServeurAdresse(String fromServeurAdresse) {
        this.fromServeurAdresse = fromServeurAdresse;
    }

    /**
     * Gets the from serveur name.
     *
     * @return the fromServeurName
     */
    public String getFromServeurName() {
        return this.fromServeurName;
    }

    /**
     * Sets the from serveur name.
     *
     * @param fromServeurName
     *            the fromServeurName to set
     */
    public void setFromServeurName(String fromServeurName) {
        this.fromServeurName = fromServeurName;
    }

    /**
     * Gets the reply to adresse.
     *
     * @return the replyToAdresse
     */
    public String getReplyToAdresse() {
        return this.replyToAdresse;
    }

    /**
     * Sets the reply to adresse.
     *
     * @param replyToAdresse
     *            the replyToAdresse to set
     */
    public void setReplyToAdresse(String replyToAdresse) {
        this.replyToAdresse = replyToAdresse;
    }

    /**
     * Gets the reply to name.
     *
     * @return the replyToName
     */
    public String getReplyToName() {
        return this.replyToName;
    }

    /**
     * Sets the reply to name.
     *
     * @param replyToName
     *            the replyToName to set
     */
    public void setReplyToName(String replyToName) {
        this.replyToName = replyToName;
    }

    /**
     * Gets the adresse contact.
     *
     * @return the adresseContact
     */
    public String getAdresseContact() {
        return this.adresseContact;
    }

    /**
     * Sets the adresse contact.
     *
     * @param adresseContact
     *            the adresseContact to set
     */
    public void setAdresseContact(String adresseContact) {
        this.adresseContact = adresseContact;
    }

}

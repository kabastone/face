/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository;
import fr.gouv.sitde.face.persistance.repositories.CadencementJpaRepository;
import fr.gouv.sitde.face.transverse.cadencement.ResultatRechercheCadencementDto;
import fr.gouv.sitde.face.transverse.entities.Cadencement;
import fr.gouv.sitde.face.transverse.queryobject.CritereRechercheCadencementQo;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * Service d'acces au repository JPA Email.
 *
 * @author a453029
 */
@Component
public class CadencementRepositoryImpl implements CadencementRepository {

    /** The email jpa repository. */
    @Inject
    private CadencementJpaRepository cadencementJpaRepository;

    @Inject
    private TimeOperationTransverse timeUtils;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository#recupererCadencementsPourCollectivite(java.lang.Long)
     */
    @Override
    public List<Cadencement> recupererCadencementsPourCollectivite(Long idCollectivite, Boolean deProjet) {
        return this.cadencementJpaRepository.recupererCadencementsPourCollectivite(idCollectivite, this.timeUtils.getAnneeCourante(), deProjet);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository#rechercherCadencementsPourMaj(java.lang.Long)
     */
    @Override
    public List<Cadencement> rechercherCadencementsPourMaj(Long idCollectivite) {
        return this.cadencementJpaRepository.rechercherCadencementsPourMaj(idCollectivite, this.timeUtils.getAnneeCourante() - 1);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository#enregistrerCadencement(fr.gouv.sitde.face.transverse.entities.Cadencement)
     */
    @Override
    public Cadencement enregistrerCadencement(Cadencement cadencement) {
        return this.cadencementJpaRepository.saveAndFlush(cadencement);
    }

    @Override
    public Cadencement findById(Long id) {
        return this.cadencementJpaRepository.getOne(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#estCadencementValide(java.lang.Long)
     */
    @Override
    public Boolean estToutCadencementValidePourCollectivite(Long idCollectivite) {
        return this.cadencementJpaRepository.estToutCadencementValidePourCollectivite(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.CadencementRepository#rechercherCadencementPaCodeNumerique(java.lang.String)
     */
    @Override
    public List<ResultatRechercheCadencementDto> rechercherCadencementParCodeNumerique(CritereRechercheCadencementQo criteresRecherche,
            String codeNumerique) {
        return this.cadencementJpaRepository.rechercherCadencementParCodeNumerique(criteresRecherche, codeNumerique);
    }

}

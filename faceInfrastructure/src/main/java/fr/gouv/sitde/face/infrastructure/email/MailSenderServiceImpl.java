package fr.gouv.sitde.face.infrastructure.email;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import fr.gouv.sitde.face.domain.spi.email.MailSenderService;
import fr.gouv.sitde.face.infrastructure.configuration.FaceMailProperties;
import fr.gouv.sitde.face.transverse.entities.Email;
import fr.gouv.sitde.face.transverse.exceptions.TechniqueException;
import fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum;

/**
 * Implementation du service qui assure la fabrication et l'envoi des emails dans l'application.
 *
 * @author a453029
 */
@Service
public class MailSenderServiceImpl implements MailSenderService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MailSenderServiceImpl.class);

    /** The Constant CONTENT_TYPE. */
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";

    /** The Constant SEPARATEUR_DESTINATAIRES. */
    private static final String SEPARATEUR_DESTINATAIRES = ",";

    /** The Constant PREFIXE_SUJET_MAIL. */
    private static final String PREFIXE_SUJET_MAIL = "[FACE] ";

    /**
     * Enumération pour les types de template.
     */
    private enum TypeTemplate {
        /** Désigne un template "objet" d'un email. */
        OBJ,
        /** Désigne un template "message" d'un email. */
        MSG
    }

    /** The face mail properties. */
    @Inject
    private FaceMailProperties faceMailProperties;

    /** The template engine. */
    @Inject
    @Qualifier("mailHtmlTemplateEngine")
    private ITemplateEngine mailHtmlTemplateEngine;

    /** The template engine. */
    @Inject
    @Qualifier("mailTextTemplateEngine")
    private ITemplateEngine mailTextTemplateEngine;

    /** The java mail sender. */
    @Inject
    private JavaMailSender javaMailSender;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.email.MailSenderService#construireEmail(java.util.Collection,
     * fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum, java.util.Map)
     */
    @Override
    public Email construireEmail(final Collection<String> adressesDestinataires, final TypeEmailEnum typeEmail, final TemplateEmailEnum templateEmail,
            final Map<String, Object> variablesContexte) {

        if ((adressesDestinataires == null) || adressesDestinataires.isEmpty()) {
            throw new TechniqueException("Impossible d'envoyer le mail sans adresse de destinataire.");
        }

        Context contexte = new Context(Locale.FRENCH, variablesContexte);

        String textObjet = null;
        String htmlContenu = null;

        textObjet = this.mailTextTemplateEngine.process(templateEmail.getNomTemplate() + TypeTemplate.OBJ, contexte);
        htmlContenu = this.mailHtmlTemplateEngine.process(templateEmail.getNomTemplate() + TypeTemplate.MSG, contexte);

        LOGGER.debug("textObjet = {}", textObjet);

        Email email = new Email();
        email.setCorps(htmlContenu);
        email.setAdressesDestinataires(String.join(SEPARATEUR_DESTINATAIRES, adressesDestinataires));
        email.setObjet(textObjet);
        email.setTypeEmail(typeEmail);

        return email;
    }

    /**
     * Envoyer mail.
     *
     * @param email
     *            the email
     * @param adresseEmetteur
     *            the adresse emetteur
     * @param nomEmetteur
     *            the nom emetteur
     */
    private void envoyerMail(final Email email, String adresseEmetteur, String nomEmetteur) {
        LOGGER.debug("=== envoyerMail(Objet = {}) ===", email.getObjet());
        try {
            MimeMessage message = this.javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message);

            InternetAddress internetAdressEmetteur = StringUtils.isEmpty(nomEmetteur) ? new InternetAddress(adresseEmetteur)
                    : new InternetAddress(adresseEmetteur, nomEmetteur);

            messageHelper.setFrom(internetAdressEmetteur);
            messageHelper.setReplyTo(new InternetAddress(this.faceMailProperties.getReplyToAdresse(), this.faceMailProperties.getReplyToName()));
            messageHelper.setSubject(PREFIXE_SUJET_MAIL + email.getObjet());

            // Type de contenu de l'email : HTML
            message.setContent(email.getCorps(), CONTENT_TYPE);

            for (String adresseDestinataire : StringUtils.delimitedListToStringArray(email.getAdressesDestinataires(), SEPARATEUR_DESTINATAIRES)) {
                messageHelper.addTo(new InternetAddress(adresseDestinataire));
            }

            message.addHeader("Auto-Submitted", "auto-generated");
            message.addHeader("Precedence", "auto-bulk");
            this.javaMailSender.send(message);

        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new TechniqueException("Erreur lors de l'envoi de l'email " + email.getObjet(), e);
        }

        LOGGER.debug("=== FIN envoyerMail(Objet = {}) ===", email.getObjet());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.email.MailSenderService#construireEmail(fr.gouv.sitde.face.transverse.referentiel.TypeEmailEnum,
     * fr.gouv.sitde.face.transverse.referentiel.TemplateEmailEnum, java.util.Map)
     */
    @Override
    public Email construireEmail(TypeEmailEnum typeEmail, TemplateEmailEnum templateEmail, Map<String, Object> variablesContexte) {

        Collection<String> adressesDestinataires = new ArrayList<>(1);
        adressesDestinataires.add(this.faceMailProperties.getAdresseContact());
        return this.construireEmail(adressesDestinataires, typeEmail, templateEmail, variablesContexte);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.infrastructure.email.MailSenderService#envoyerMail(fr.gouv.sitde.face.transverse.entities.Email)
     */
    @Override
    public void envoyerMail(final Email email) {
        this.envoyerMail(email, this.faceMailProperties.getFromServeurAdresse(), this.faceMailProperties.getFromServeurName());
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.email.MailSenderService#envoyerEmailContact(fr.gouv.sitde.face.transverse.entities.Email, java.lang.String)
     */
    @Override
    public Boolean envoyerEmailContact(final Email email, String adresseMailEmetteur) {
        this.envoyerMail(email, adresseMailEmetteur, null);
        return true;
    }

}

/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.tableaudebord;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository;
import fr.gouv.sitde.face.infrastructure.pagination.mapper.SpringDataPaginationMapper;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.TableauDeBordSd7JpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordSd7Dto;

/**
 * The Class TableauDeBordSd7RepositoryImpl.
 *
 * @author a453029
 */
@Component
public class TableauDeBordSd7RepositoryImpl implements TableauDeBordSd7Repository {

    /** The tableau de bord sd 7 jpa repository. */
    @Inject
    private TableauDeBordSd7JpaRepository tableauDeBordSd7JpaRepository;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository#rechercherTableauDeBordSd7()
     */
    @Override
    public TableauDeBordSd7Dto rechercherTableauDeBordSd7() {
        return this.tableauDeBordSd7JpaRepository.rechercherTableauDeBordSd7();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository#rechercherDemandesSubventionTdbSd7AcontrolerSubvention(fr.
     * gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AcontrolerSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordSd7JpaRepository.rechercherDemandesSubventionTdbAcontrolerSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.ACCORDEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository#rechercherDemandesPaiementTdbSd7AcontrolerPaiement(fr.gouv.
     * sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AcontrolerPaiement(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordSd7JpaRepository.rechercherDemandesPaiementTdbAcontrolerPaiement(EtatDossierEnum.OUVERT,
                EtatDemandePaiementEnum.ACCORDEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository#rechercherDemandesSubventionTdbSd7AtransfererSubvention(fr.
     * gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbSd7AtransfererSubvention(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordSd7JpaRepository.rechercherDemandesSubventionTdbAtransfererSubvention(EtatDossierEnum.OUVERT,
                EtatDemandeSubventionEnum.CONTROLEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordSd7Repository#rechercherDemandesPaiementTdbSd7AtransfererPaiement(fr.gouv
     * .sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbSd7AtransfererPaiement(PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordSd7JpaRepository.rechercherDemandesPaiementTdbAtransfererPaiement(EtatDossierEnum.OUVERT,
                EtatDemandePaiementEnum.CONTROLEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

}

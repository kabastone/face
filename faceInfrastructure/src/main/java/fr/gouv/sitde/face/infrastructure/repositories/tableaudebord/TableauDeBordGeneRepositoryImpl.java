/**
 *
 */
package fr.gouv.sitde.face.infrastructure.repositories.tableaudebord;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository;
import fr.gouv.sitde.face.infrastructure.pagination.mapper.SpringDataPaginationMapper;
import fr.gouv.sitde.face.persistance.repositories.tableaudebord.TableauDeBordGeneJpaRepository;
import fr.gouv.sitde.face.transverse.entities.DemandePaiement;
import fr.gouv.sitde.face.transverse.entities.DemandeSubvention;
import fr.gouv.sitde.face.transverse.entities.DossierSubvention;
import fr.gouv.sitde.face.transverse.entities.DotationCollectivite;
import fr.gouv.sitde.face.transverse.pagination.PageDemande;
import fr.gouv.sitde.face.transverse.pagination.PageReponse;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDemandeSubventionEnum;
import fr.gouv.sitde.face.transverse.referentiel.EtatDossierEnum;
import fr.gouv.sitde.face.transverse.referentiel.TypeDemandePaiementEnum;
import fr.gouv.sitde.face.transverse.tableaudebord.TableauDeBordGeneDto;
import fr.gouv.sitde.face.transverse.time.TimeOperationTransverse;

/**
 * The Class TableauDeBordGeneRepositoryImpl.
 *
 * @author a453029
 */
@Component
public class TableauDeBordGeneRepositoryImpl implements TableauDeBordGeneRepository {

    /** The tableau de bord gene jpa repository. */
    @Inject
    private TableauDeBordGeneJpaRepository tableauDeBordGeneJpaRepository;

    /** The time utils. */
    @Inject
    private TimeOperationTransverse timeOperationTransverse;

    /*
     * (non-Javadoc)
     *
     * @see fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherTableauDeBordGenerique(java.lang.Long)
     */
    @Override
    public TableauDeBordGeneDto rechercherTableauDeBordGenerique(Long idCollectivite) {
        return this.tableauDeBordGeneJpaRepository.rechercherTableauDeBordGenerique(idCollectivite);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(
     * java.lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationCollectivite> rechercherDotationsCollectiviteTdbGeneAdemanderSubvention(Long idCollectivite, PageDemande pageDemande) {
        int anneeCourante = this.timeOperationTransverse.getAnneeCourante();
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DotationCollectivite> page = this.tableauDeBordGeneJpaRepository.rechercherDotationsCollectiviteTdbAdemanderSubvention(idCollectivite,
                anneeCourante, pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDossiersSubventionTdbGeneAdemanderPaiement(java.
     * lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAdemanderPaiement(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DossierSubvention> page = this.tableauDeBordGeneJpaRepository.rechercherDossiersSubventionTdbAdemanderPaiement(idCollectivite,
                EtatDossierEnum.OUVERT, TypeDemandePaiementEnum.SOLDE, pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDossiersSubventionTdbGeneAcompleterSubvention(
     * java.lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DotationCollectivite> rechercherDotationCollectiviteTdbAcompleterSubvention(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DotationCollectivite> page = this.tableauDeBordGeneJpaRepository.rechercherDotationCollectiviteTdbAcompleterSubvention(idCollectivite,
                EtatDossierEnum.OUVERT, this.timeOperationTransverse.getAnneeCourante(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDemandesSubventionTdbGeneAcorrigerSubvention(
     * java.lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandeSubvention> rechercherDemandesSubventionTdbGeneAcorrigerSubvention(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandeSubvention> page = this.tableauDeBordGeneJpaRepository.rechercherDemandesSubventionTdbAcorrigerSubvention(idCollectivite,
                EtatDossierEnum.OUVERT, EtatDemandeSubventionEnum.ANOMALIE_SIGNALEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDemandesPaiementTdbGeneAcorrigerPaiement(java.
     * lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DemandePaiement> rechercherDemandesPaiementTdbGeneAcorrigerPaiement(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);

        Page<DemandePaiement> page = this.tableauDeBordGeneJpaRepository.rechercherDemandesPaiementTdbAcorrigerPaiement(idCollectivite,
                EtatDossierEnum.OUVERT, EtatDemandePaiementEnum.ANOMALIE_SIGNALEE.toString(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDossiersSubventionTdbGeneAcommencerPaiement(java
     * .lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAcommencerPaiement(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);
        LocalDateTime dateDuJourPlus3Mois = this.timeOperationTransverse.now().plusMonths(3).truncatedTo(ChronoUnit.DAYS);

        Page<DossierSubvention> page = this.tableauDeBordGeneJpaRepository.rechercherDossiersSubventionTdbAcommencerPaiement(idCollectivite,
                EtatDossierEnum.OUVERT, dateDuJourPlus3Mois, this.timeOperationTransverse.now(), pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.gouv.sitde.face.domain.spi.repositories.tableaudebord.TableauDeBordGeneRepository#rechercherDossiersSubventionTdbGeneAsolderPaiement(java.
     * lang.Long, fr.gouv.sitde.face.transverse.pagination.PageDemande)
     */
    @Override
    public PageReponse<DossierSubvention> rechercherDossiersSubventionTdbGeneAsolderPaiement(Long idCollectivite, PageDemande pageDemande) {
        Pageable pageable = SpringDataPaginationMapper.getPageRequestFromPageDemande(pageDemande);
        LocalDateTime dateDuJourPlus4Mois = this.timeOperationTransverse.now().plusMonths(4).truncatedTo(ChronoUnit.DAYS);

        Page<DossierSubvention> page = this.tableauDeBordGeneJpaRepository.rechercherDossiersSubventionTdbAsolderPaiement(idCollectivite,
                EtatDossierEnum.OUVERT, dateDuJourPlus4Mois, this.timeOperationTransverse.now(), TypeDemandePaiementEnum.SOLDE, pageable);
        return SpringDataPaginationMapper.getPageReponseFromPage(page);
    }
}

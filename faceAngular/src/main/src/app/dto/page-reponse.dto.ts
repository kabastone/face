export interface PageReponseDto {

  /** Liste de résultats. */
  listeResultats: any[];

  /** nb total de résultats. */
  nbTotalResultats: number;
}

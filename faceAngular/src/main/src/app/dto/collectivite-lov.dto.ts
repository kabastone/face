export interface CollectiviteLovDto {

  id: number;
  siret: string;
  nomCourt: string;
  etatCollectivite: string;
}

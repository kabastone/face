export interface EtatDemandeLovDto {
  code: string;
  libelle: string;
}

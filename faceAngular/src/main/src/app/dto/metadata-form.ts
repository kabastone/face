export interface MetadataForm {
  visible: boolean;
  editable: boolean;
}

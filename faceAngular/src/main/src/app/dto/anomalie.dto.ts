export interface AnomalieDto {
    id: string;
    dateCreation: string;
    libelleTypeAnomalie: string;
    codeTypeAnomalie: string;
    problematique: string;
    corrigee: boolean;
    visiblePourAODE: boolean;
    reponse: string;
    version: number;
    idDemandePaiement: number;
    idDemandeSubvention: number;
  }

export interface DepartementLovDto {

  id: number;
  code: string;
  nom: string;
  codeEtNom: string;
}

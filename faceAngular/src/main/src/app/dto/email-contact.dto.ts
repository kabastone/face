export interface EmailContactDto {
  nom: string;
  email: string;
  objet: string;
  message: string;
}

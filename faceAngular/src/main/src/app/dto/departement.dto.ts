export interface DepartementDto {

  id: number;
  code: string;
  nom: string;
  nomCodique: string;
  codique: string;
  version: string;
}

export interface PageDemandeDto {

   /** Index de la page voulue (attention, l'index commence à zero). */
  indexPage: number;

  /** Taille de la page voulue (nombre de résultats). */
  taillePage: number;

  /** Champ de tri de la clause orderBy. */
  champTri: string;

  /** Le tri doit être dans le sens descendant ou pas. */
  desc: boolean;

  /** The valeur filtre. */
  valeurFiltre: string;

}

export interface SousProgrammeLovDto {

  id: number;
  abreviation: string;
  description: string;
  codeDomaineFonctionnel: string;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { IAppConfig } from './app-config.model';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class
AppConfigService {
  static settings: IAppConfig;

  constructor(private httpClient: HttpClient) { }

  // chargement de la configuration au runtime
  chargementConfApp(): Promise<Object> {
    const jsonFile = `assets/config/config.${environment.name}.json`;
    return this.httpClient.get<IAppConfig>(jsonFile)
      .pipe(
        tap((response: IAppConfig) => {
          // chargement des données dev/prod dans 'AppConfigService.settings'
          AppConfigService.settings = <IAppConfig>response;

          // mise a jour de l'url de l'application
          this.majUrlApplication();

          // désactiver les logs console
          this.desactiverLogsConsole();

          console.log('environement chargé : ' + environment.name);
        }),
        catchError(this.gestionErreurHttp)
      ).toPromise();
  }

  // gestion des éventuelles erreurs http pour la récupération du fichier de config
  private gestionErreurHttp(erreur: HttpErrorResponse) {
    let messageErreur: string;
    if (erreur.error instanceof Error) {
      messageErreur = `chargement env: A network or client-side error occured: ${erreur.error.message}`;
    } else {
      messageErreur = `chargement env: API server returned error code ${erreur.status}, body of error was: ${erreur.error}`;
    }
    console.error(messageErreur);
    return throwError(erreur);
  }

  /**
   * Détection de l'url de l'application
   * et mise à jour dans 'AppConfigService.settings'.
   *
   */
  private majUrlApplication(): void {
    let urlApp = window.location.href;
    // suppression des données après l'ancre, si existante
    if (urlApp.indexOf('#') > 0) {
      urlApp = urlApp.substr(0, urlApp.indexOf('#'));
    }
    // suppression du /app, si existant (build prod uniquement)
    if (urlApp.indexOf('/app') > 0) {
      urlApp = urlApp.substr(0, urlApp.indexOf('/app'));
    }
    // suppression du slash de fin d'url si existant
    if (urlApp.endsWith('/')) {
      urlApp = urlApp.substr(0, urlApp.length - 1);
    }
    // mise à jour de la configuration
    AppConfigService.settings.urlApplication = urlApp;
  }

  /**
   * Désactiver les logs console si configuré comme tel
   * dans assets/config/config.*.json
   *
   */
  private desactiverLogsConsole(): void {
    if (!AppConfigService.settings.logging.console && window && window.console) {
      window.console.log = function () { };
      window.console.error = function () { };
      window.console.info = function () { };
      window.console.warn = function () { };
    }
  }
}

export interface IAppConfig {
  logging: {
      console: boolean;
  };
  paginationPageSize: string;
  urlApplication: string;
  versionApplication: string;
  dateBuild: string;
}

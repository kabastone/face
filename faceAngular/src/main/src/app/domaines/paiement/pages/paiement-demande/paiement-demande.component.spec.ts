import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementDemandeComponent } from './paiement-demande.component';

describe('PaiementDemandeComponent', () => {
  let component: PaiementDemandeComponent;
  let fixture: ComponentFixture<PaiementDemandeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementDemandeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementDemandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementRechercheComponent } from './paiement-recherche.component';

describe('PaiementRechercheComponent', () => {
  let component: PaiementRechercheComponent;
  let fixture: ComponentFixture<PaiementRechercheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementRechercheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementRechercheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnDestroy, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { EtatDemandeLovDto } from '@app/dto/etat-demande-lov.dto';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { ResultatRechercheDossierDto } from '@subvention/dto/resultat-recherche-dossier.dto';
import { Observable, pipe, Subject, Subscription } from 'rxjs';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { takeUntil, map, take } from 'rxjs/operators';
import { AppConfigService } from '@app/config/app-config.service';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { GestionAffichagePaiementService } from '@paiement/services/gestion-affichage-paiement.service';
import { CritereRecherchePaiementDto } from '../paiement-demande/dto/critere-recherche-paiement.dto';
import { RecherchePaiementService } from '@paiement/services/recherche-paiement.service';
import { State, Store } from '@ngrx/store';
import * as fromApp from '../../../../app_store/app.store';
import * as Actions from '../../../subvention/store/actions';
import * as Reducers from '../../../subvention/store/reducer'
import { ScrollHelper } from '@app/helper/scroll-helper';

@Component({
  selector: 'app-paiement-recherche',
  templateUrl: './paiement-recherche.component.html',
  styleUrls: ['./paiement-recherche.component.css']
})
export class PaiementRechercheComponent implements OnInit, OnDestroy, AfterViewChecked {
 // formulaire ihm
 public criteresForm: FormGroup;

 public sousProgrammes: SousProgrammeLovDto[];

 public departements: DepartementLovDto[];

 public etatsDemande: EtatDemandeLovDto[];

 private infoUtilisateur: UtilisateurConnecteDto;

 public collectivites: CollectiviteLovDto[];

 // objet CritereRecherchePaiementDto, reflet des données du formulaire
 public criteresDto: CritereRecherchePaiementDto;

 // liste des résulats chargés
 public resultatRecherchePaiementDtos: ResultatRechercheDossierDto[];

 // Nombre total de resultats trouvés
 public nbTotalResultats: number;

 public isUserUniquementGenerique: boolean;

 public pageSize: number;

 // pour le désabonnement auto
 public ngDestroyed$ = new Subject();

 private savedSearchTerms={} as Actions.SearchTerms;

 public searchTermsPayload;

 results$: Subscription;

 private scrollHelper = new ScrollHelper();



 constructor(
   private recherchePaiementService: RecherchePaiementService,
   private referentielService: ReferentielService,
   private messageService: MessageService,
   private messageManagerService: MessageManagerService,
   private droitService: DroitService,
   private formBuilder: FormBuilder,
   private gestionAffichage: GestionAffichagePaiementService,
   private store: Store<fromApp.AppState>,
 ) {
 }
  ngAfterViewChecked(): void {
    this.scrollHelper.doScroll();
  }

 // à la destruction
 public ngOnDestroy() {
   this.ngDestroyed$.next();
   this.results$.unsubscribe();
 }

 ngOnInit() {
   // Récupération du resultat de la recherche depuis le store 
  this.results$ = this.store.select('searchList')
  .subscribe(
    data => {
      
      if(data.resultsPaie.listeResultats.length > 0){
        this.savedSearchTerms = data.saveFormPaie;
        this.alimenterResultatFromPageReponse(data.resultsPaie); 
      }
    }
  );

   // Initialisation du profil utilisateur
   this.droitService
     .getInfosUtilisateur$()
     .pipe(takeUntil(this.ngDestroyed$))
     .subscribe(infoUtilisateur => {
       this.infoUtilisateur = infoUtilisateur;
       this.initialiserIsUserUniquementGenerique(infoUtilisateur);
     }
     );

   // Taille de la page
   this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;

   // Initialisation du formulaire
   this.initCriteresForm();

   // récupération des sousProgrammes
   this.referentielService
     .rechercherTousSousProgrammes$()
     .subscribe(data => (this.sousProgrammes = data));

   // récupération des états des demandes filtrant les demandes de
   this.referentielService
     .rechercherTousEtatsDemandePaiement$().pipe(
       map(listeEtat =>  {
           listeEtat.forEach(etat => {
             etat.libelle = this.gestionAffichage.recupererEtatLibelleString(etat.code)
           });
           return listeEtat;
         }
       )
     )
     .subscribe(data => (this.etatsDemande = data));

   if (!this.isUserUniquementGenerique) {
     // récupération des collectivites
     this.referentielService
       .rechercherToutesCollectivites$()
       .subscribe(data => (this.collectivites = data));

     // récupération des départements
     this.referentielService
       .rechercherTousDepartements$()
       .subscribe(data => (this.departements = data));
   }
 }

 /**
  * Initialise le flag de profil de l'utilisateur.
  */
 private initialiserIsUserUniquementGenerique(
   infosUtilisateur: UtilisateurConnecteDto
 ): void {
   this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
     infosUtilisateur
   );
 }

 /**
  * Initialise le criteresform.
  *
  */
 private initCriteresForm(): void {
   // init. du formulaire avec l'état initial du store 
   this.criteresForm = this.formBuilder.group({
    numDossier: new FormControl(this.savedSearchTerms.numDossier),
    anneePaiement: new FormControl(this.savedSearchTerms.anneePaiement, Validators.pattern('[0-9]*')),
    selectionSousProgramme: new FormControl(this.savedSearchTerms.selectionSousProgramme),
    selectionEtatDemande: new FormControl(this.savedSearchTerms.selectionEtatDemande),
    selectionCollectivite: new FormControl(this.savedSearchTerms.selectionCollectivite),
    selectionDepartement: new FormControl(this.savedSearchTerms.selectionDepartement)
   });
 }

 /**
  * Met à jour l'objet DTO à partir du formulaire.
  *
  */
 private majDtoFromFormulaire(): void {
   // Alimentation du DTO avec les propriétés du form portant le meme nom
   this.criteresDto = this.criteresForm.value as CritereRecherchePaiementDto;

   // On alimente l'id du sous-programme à partir de la liste déroulante.
   if (
     this.criteresForm.get('selectionSousProgramme').value !== null &&
     this.criteresForm.get('selectionSousProgramme').value !== '...'
   ) {
     const selectionSousProgramme: SousProgrammeLovDto = this.criteresForm.get(
       'selectionSousProgramme'
     ).value as SousProgrammeLovDto;
     this.criteresDto.idSousProgramme = selectionSousProgramme.id;
   }

   // On alimente le code de l'état de la demande à partir de la liste déroulante.
   if (
     this.criteresForm.get('selectionEtatDemande').value !== null &&
     this.criteresForm.get('selectionEtatDemande').value !== '...'
   ) {
     const selectionEtatDemande: EtatDemandeLovDto = this.criteresForm.get(
       'selectionEtatDemande'
     ).value as EtatDemandeLovDto;
     this.criteresDto.codeEtatDemande = selectionEtatDemande.code;
   }

   // On alimente l'id de la collectivite à partir de la liste déroulante.
   if (
     this.criteresForm.get('selectionCollectivite').value !== null &&
     this.criteresForm.get('selectionCollectivite').value !== '...'
   ) {
     const selectionCollectivite: CollectiviteLovDto = this.criteresForm.get(
       'selectionCollectivite'
     ).value as CollectiviteLovDto;
     this.criteresDto.idCollectivite = selectionCollectivite.id;
   }

    // On alimente l'id du departement à partir de la liste déroulante.
    if (
     this.criteresForm.get('selectionDepartement').value !== null &&
     this.criteresForm.get('selectionDepartement').value !== '...'
   ) {
     const selectionDepartement: DepartementLovDto = this.criteresForm.get(
       'selectionDepartement'
     ).value as DepartementLovDto;
     this.criteresDto.idDepartement = selectionDepartement.id;
   }
 }

 public rechercherPaiements(): void {
   this.messageService.clear();
   // Sauvegarde de l'état du formulaire
    this.store.dispatch(new Actions.SaveFormPaie(this.criteresForm.value))

   // mapping des champs du formulaire vers le DTO
   this.majDtoFromFormulaire();

   // On filtre la recherche sur la collectivite active d'un utilisateur generique.
   if (this.isUserUniquementGenerique) {
     this.criteresDto.idCollectivite = this.droitService.getIdCollectivite$().value;
   }

   this.criteresDto.pageDemande = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };
    // sauvegarde de l'état des citères de recherche
    this.searchTermsPayload = this.criteresForm.value;
    this.store.dispatch(new Actions.SearchPaiement(this.searchTermsPayload));

   // récupération des dossiers recherchés
   this.recherchePaiementService
     .getResultatRecherchePaiementDto$(this.criteresDto)
     .subscribe(data => this.alimenterResultatFromPageReponse(data));
 }

 public loadPaiementsLazy(event: LazyLoadEvent): void {
   this.messageService.clear();

   // Alimentation des données de pagination (pageDemande)
   this.criteresDto.pageDemande = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };
   this.criteresDto.pageDemande.indexPage = Math.trunc(event.first / this.pageSize);
   this.criteresDto.pageDemande.champTri = event.sortField;
   this.criteresDto.pageDemande.desc =
     event.sortOrder !== null && event.sortOrder === -1;

   // récupération des dossiers recherchés
   this.recherchePaiementService
     .getResultatRecherchePaiementDto$(this.criteresDto)
     .subscribe(data => this.alimenterResultatFromPageReponse(data));
 }

 private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
   if (pageReponse && pageReponse.listeResultats.length > 0) {
     this.resultatRecherchePaiementDtos = pageReponse.listeResultats;
     this.nbTotalResultats = pageReponse.nbTotalResultats;
     // scroll sur la liste des paiements
     this.scrollHelper.scrollToFirst('scroll');
   } else {
     this.resultatRecherchePaiementDtos = null;
     this.nbTotalResultats = 0;
     this.messageManagerService.aucuneDonneeTrouveeMessage();
   }
 }

 /**
  * Annuler : reset du formulaire et vidage du tableau de résultats.
  *
  */
 public annuler(): void {
   this.criteresForm.reset();
   this.resultatRecherchePaiementDtos = null;
   this.messageService.clear();
 }
}

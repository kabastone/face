// Angular
import { NgModule } from '@angular/core';

// interne
import { PaiementRechercheComponent } from './paiement-recherche.component';
import { SharedModule } from '@app/share/shared.module';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PaiementRechercheComponent],
  imports: [
    TooltipModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    PaiementRechercheComponent,
    RouterModule
  ]
})
export class PaiementRechercheModule { }

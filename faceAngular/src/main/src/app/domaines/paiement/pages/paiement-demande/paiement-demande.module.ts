import { NgModule } from '@angular/core';

// Interne
import { PaiementDemandeComponent } from './paiement-demande.component';
import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';

// PrimeNG
import {TooltipModule} from 'primeng/tooltip';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ToggleButtonModule} from 'primeng/togglebutton';
import { AnomalieModule } from '@component/anomalie/anomalie.module';
import {ProgressSpinnerModule} from 'primeng/progressspinner';

@NgModule({
  declarations: [
    PaiementDemandeComponent
  ],
  imports: [
    TooltipModule,
    InputTextareaModule,
    ToggleButtonModule,
    AnomalieModule,
    ProgressSpinnerModule,
    SharedModule,
    RouterModule,
  ],
  exports: [
    PaiementDemandeComponent
  ]
})
export class PaiementDemandeModule { }

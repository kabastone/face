import { FormGroup } from '@angular/forms';
import { TypeDemandeLovDto } from '@app/dto/type-demande-lov.dto';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { GestionDemandePaiementForm } from '@paiement/dto/gestion-demande-paiement-form';
import { AnomalieDto } from '@app/dto/anomalie.dto';
import { EtatDocUpload } from '@app/services/authentification/dto/etat-doc-upload';
import { Subject, Observable } from 'rxjs';

export class DemandePaiementData {

  // formulaire ihm
  public paiementFormCriteres: FormGroup;

  // état du formulaire
  public etatForm: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

  // formulaire principal de la page
  public formulairePrincipal: FormGroup;

  // listing des types de demande possibles
  public listeMenuTypeDemande: TypeDemandeLovDto[];

  public montantTotalHtTravaux: number;

  public montantTotalDemande: number;
  // si premiere demande
  public isPremiereDemande: boolean;

  //permet de ne pas pourvoir demander d'avance si true
  public hasUnAcompte: boolean;

  // maps des documents (associés la demande de paiement)
  public mapDocuments = new Map<string, number>();
  // array contenant le nom des documents
  public mapDocumentsName: Array<string>;

  // infos utilisateur
  public infosUtilisateur: UtilisateurConnecteDto = {} as UtilisateurConnecteDto;

  // permet de ne pas afficher le formulaire tant qu'il n'est pas alimenté
  public affichageFormulairePrincipal = false;

  // formulaire dédié à l'upload de fichiers (association : TypeDocument => fichier)
  public formulaireFichiers: FormData = new FormData();

  // état de la demande (au début = 'INITIAL')
  public codeEtatDemande = 'INITIAL';

  // libellé de l'état de la demande
  public libelleEtatDemande: string;

  // tableau des documents supplémentaires
  public documentsSupplementaires: DocumentDto[] = <DocumentDto[]>[];

  // tableau des documents supplémentaires rajoutés en cette session
  public documentsSupplementairesRajoutes: DocumentDto[] = <DocumentDto[]>[];

  // liste de fichiers qui correspondent aux docs complementaires
  public fichiersDocsSuppl: File[] = <File[]>[];

  // liste des id des docs complémentaires à supprimer
  public idsDocsComplSuppr: number[] = [];

  // version
  public version = 0;

  // date de demande
  public dateDemande: Date;

  // liste des anomalies
  public anomalies: AnomalieDto[] = <AnomalieDto[]>[];

  // le boutton 'Demander' ou 'Refuser' doit être disabled par défaut
  public demanderRefuserDisabled = true;

  // liste des états des documents à uploader
  public listeEtatsDocs: EtatDocUpload[] = <EtatDocUpload[]>[];

  /* Département et collectivité */
  public nomLongCollectivite: string;
  public codeDepartement: string;
}

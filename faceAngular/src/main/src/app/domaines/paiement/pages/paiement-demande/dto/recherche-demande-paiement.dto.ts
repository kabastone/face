export interface RechercheDemandePaiementDto {
  numDossier: string;
  resteConsommer: number;
}

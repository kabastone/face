import { Component, OnDestroy, OnInit, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DroitService } from '@app/services/authentification/droit.service';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { GestionAffichagePaiementService } from '@paiement/services/gestion-affichage-paiement.service';
import { MessageService } from 'primeng/api';
import { filter, takeUntil, tap } from 'rxjs/operators';
import { PaiementDemandeService } from './service/paiement-demande.service';
import { Subject } from 'rxjs';
import { DemandePaiementData } from './dto/demande-paiement.data';
import { DemandePaiementDto } from '@paiement/dto/demande-paiement.dto';
import { AnomalieDto } from '@app/dto/anomalie.dto';
import { EtatDocUpload } from '@app/services/authentification/dto/etat-doc-upload';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { Message } from 'primeng/api';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { CustomFileTransferComponent } from '@component/custom-file-transfer/custom-file-transfer.component';

import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'pmt-paiement-demande',
  templateUrl: './paiement-demande.component.html',
  styleUrls: ['./paiement-demande.component.css']
})
export class PaiementDemandeComponent implements OnInit, OnDestroy {
  // id du dossier de subvention (url)
  public idDossierSubvention = +this.route.snapshot.paramMap.get('idDossier');

  // id de la demande de paiement (url)
  public idDemandePaiement = +this.route.snapshot.paramMap.get('idDemandePaiement');

  // pour le désabonnement auto
  private ngDestroyed$ = new Subject();

  // données nécessaires à l'écran de la demande de paiement
  public demandePaiementData: DemandePaiementData = new DemandePaiementData();

  public anomalies: AnomalieDto[];

  // Référence à l'élément message pour le scroll into view.
  @ViewChild('messageAnomalieDemandePaiement') messageAnomalieElement: ElementRef;

  @ViewChildren(CustomFileTransferComponent) viewChildren !: QueryList<CustomFileTransferComponent>;

  private infosUtilisateur: UtilisateurConnecteDto;

  // Montants maximum pour input
  public maxAcompte: number;
  public maxAvance: number;
  public maxActuel: number;
  public maxTauxActuel: number;
  public montantSubvention: number;
  public maxTauxFace: number;
  public maxTauxAvanceFace: number;
  showButton: boolean;
  excelExtention: any;
  isPdfContent: boolean;
  isExcelContent: boolean;
  localUrl: any;
  displayModal: boolean;
  data: AOA;
  headData: any[];
  desactiverBoutonDemande = false;
  public motifRefusForm: FormGroup;

  public displayRefusModal: boolean;

  // constructeur
  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private paiementDemandeService: PaiementDemandeService,
    private gestionAffichageChampsService: GestionAffichagePaiementService,
    private referentielService: ReferentielService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private fileSaverService: FileSaverService) {

    this.demandePaiementData.mapDocumentsName = new Array<string>(9);
    // mise à jour des infos utilisateur
    this.droitService.getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        infosUser => {
          if (!infosUser.id) {
            return;
          }
          this.infosUtilisateur = infosUser;
        }
      );
  }

  retour = function () {
    this.router.navigateByUrl('/subvention/dossier/' + this.idDossierSubvention);
  };

  /**
  * A la destruction : on lance le désabonnement de tous les subscribes().
  */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  // init.
  ngOnInit() {
    // init. la première partie du formulaire (en haut)
    this.demandePaiementData.paiementFormCriteres = this.initFormulaireHaut();

    // création du formulaire principal
    this.demandePaiementData.formulairePrincipal = this.initFormulaireBas();


    // récupération des types des dossiers
    this.referentielService.rechercherTousTypesDemandePaiement$()
      .subscribe(typesDemande => {
        this.demandePaiementData.listeMenuTypeDemande = typesDemande;
      }
    );


    // mise à jour des infos utilisateur
    this.droitService.getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        infosUser => {
          if (!infosUser.id) {
            return;
          }
          this.demandePaiementData.infosUtilisateur = infosUser;

          // mise à jour de l'état du formulaire (via les roles)
          this.majEtatFormulaire('ACOMPTE');

          // le formulaire principal est prêt pour l'affichage
          this.demandePaiementData.affichageFormulairePrincipal = true;

          // chargement des données via la bdd
          this.chargementDonneesFormulaire();
        }
      );
    this.messageService.clear();
  }

  /**
   * TRUE : s'il s'agit du mode lecture (consultation d'une demande de paiement).
   * FALSE : s'il s'agit de la création (nouvelle demande de paiement)
   *
   * @returns true ou false.
   */
  public isDemandeExistante() {
    if (this.idDemandePaiement) {
      return true;
    }
    return false;
  }

  /**
   * Mise à jour du formulaire :
   * -> chargement de l'état du formulaire
   * puis :
   *   ajout des champs (actifs ou disabled)
   *   OU : activation/désactivation des champs existants
   */
  private majEtatFormulaire(typeDemande: string) {
    // init de l'état des champs du formulaire
    this.demandePaiementData.etatForm = this.gestionAffichageChampsService.recupererFormulaire(this.demandePaiementData.codeEtatDemande,
      this.demandePaiementData.infosUtilisateur.listeRoles, typeDemande, this.demandePaiementData.isPremiereDemande);

    // mise à jour de l'état
    Object.entries(this.demandePaiementData.etatForm).forEach(([key, itemForm]) => {
      // le champ existe : activation ou désactivation du champ
      if (this.demandePaiementData.formulairePrincipal.get(key)) {
        if (itemForm.editable) {
          this.demandePaiementData.formulairePrincipal.get(key).enable();
        } else {
          this.demandePaiementData.formulairePrincipal.get(key).disable();
        }
      } else {
        // le champ n'existe pas : on l'ajoute
        this.demandePaiementData.formulairePrincipal.addControl(key,
          new FormControl({ value: '', disabled: !itemForm.editable }));
      }
    });
    // mise à jour de l'objet des docs à uploader
    this.majEtatDocs();
  }

  // gestion du boutton 'corriger' + message d'avertissement
  private gererMessageAnomalie(demandePaiementDto: DemandePaiementDto) {
    if (demandePaiementDto.codeEtatDemande === 'ANOMALIE_SIGNALEE'
      && this.isAuMoinsUneAnomalieACorriger(demandePaiementDto.anomalies)
      && this.droitService.isUniquementGenerique(this.demandePaiementData.infosUtilisateur)) {
      this.messageManagerService.isAuMoinsUneAnomalieACorrigerPaiementMessage();
    }
  }

  /**
   * Une fois le formulaire construit (majEtatFormulaire()),
   * on peut charger les données dans le cas d'une nouvelle demande
   * ou d'une consultation de la demande de paiement existante.
   *
   */
  private chargementDonneesFormulaire() {
    if (this.idDemandePaiement) {
      // consultation d'une demande existante
      this.paiementDemandeService.rechercheDemandePaiement$(this.idDemandePaiement)
        .subscribe((demandePaiementDto: DemandePaiementDto) => {
          this.majFormulaireEtIds(demandePaiementDto, true);
          this.gererMessageAnomalie(demandePaiementDto);
        });
    } else if (this.idDossierSubvention) {
      // initialisation de la demande de paiement
      this.paiementDemandeService.initNouvelleDemandePaiement$(this.idDossierSubvention)
        .subscribe((demandePaiementDto: DemandePaiementDto) => {
          this.majFormulaireEtIds(demandePaiementDto, true);
          this.maxAvance = demandePaiementDto.montantRestantAvance;
          this.maxTauxAvanceFace = demandePaiementDto.tauxAvanceReglementaire;
          this.montantSubvention = demandePaiementDto.montantSubvention;
          this.maxTauxFace = demandePaiementDto.tauxAideFacePercent;
          this.maxTauxActuel = null;
          if (demandePaiementDto.typeDemande.code === 'ACOMPTE') {
            this.maxActuel = null;
          } else if (demandePaiementDto.typeDemande.code === 'AVANCE') {
            this.maxActuel = this.montantSubvention;
            this.maxTauxActuel = this.maxTauxAvanceFace;

          } else {
            this.maxActuel = 0;
          }
          this.demandePaiementData.formulairePrincipal.controls['montantTravauxHt'].setValue(this.maxActuel);
          this.demandePaiementData.formulairePrincipal.controls['tauxAideFacePercent'].setValue(this.maxTauxActuel);
          this.demandePaiementData.formulairePrincipal.controls['montantTotalDemande'].setValue(0);
          this.majCalculAideDemandee(demandePaiementDto.typeDemande.code);
        });
    }
  }

  /**
   * Mise à jour des formulaires et des IDs.
   * @param demandePaiementDto dto à utiliser pour les maj
   * @param [majFormulaireHaut] mise à jour (ou non) du formulaire du haut
   */
  private majFormulaireEtIds(demandePaiementDto: DemandePaiementDto, majFormulaireHaut?: boolean) {
    if (majFormulaireHaut) {
      // mise à jour du formulaire (haut)
      this.demandePaiementData.paiementFormCriteres.patchValue(demandePaiementDto);
    }

    // mise à jour du formulaire principal
    this.demandePaiementData.formulairePrincipal.patchValue(demandePaiementDto);

    // récupération de la liste des documents
    this.demandePaiementData.mapDocuments = new Map<string, number>();
    // initialisation d'une array qui contiendra les noms des documents à télécharger (titre à afficher). Par défaut 'introuvable'
    
    for (let i = 0; i < this.demandePaiementData.mapDocumentsName.length; i++) {
      this.demandePaiementData.mapDocumentsName[i] = 'Document introuvable';
    }
    this.demandePaiementData.documentsSupplementaires = <DocumentDto[]>[];
    this.demandePaiementData.documentsSupplementairesRajoutes = <DocumentDto[]>[];
    this.demandePaiementData.fichiersDocsSuppl = <File[]>[];
    this.demandePaiementData.idsDocsComplSuppr = [];
    this.demandePaiementData.formulaireFichiers = new FormData();
    Object.values(demandePaiementDto.documents).forEach(document => {
      if (document.codeTypeDocument === 'DOC_COMPLEMENTAIRE_PAIEMENT') {
        this.demandePaiementData.documentsSupplementaires.push(document);
      } else {
        this.demandePaiementData.mapDocuments.set(document.codeTypeDocument, document.id);
        // stockage des noms des fichiers dans une array pour affichage sur les titres des buttons de téléchargement
        switch (document.codeTypeDocument) {
          case 'ETAT_MARCHES_PASSES_PDF':
            this.demandePaiementData.mapDocumentsName[0] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_MARCHES_PASSES_TABLEUR':
            this.demandePaiementData.mapDocumentsName[1] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_REALISATION_TRAVAUX_PDF':
            this.demandePaiementData.mapDocumentsName[2] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_REALISATION_TRAVAUX_TABLEUR':
            this.demandePaiementData.mapDocumentsName[3] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_ACHEVEMENT_FINA_PDF':
            this.demandePaiementData.mapDocumentsName[4] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_ACHEVEMENT_FINANCIER_TABLEUR':
            this.demandePaiementData.mapDocumentsName[5] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_ACHEVEMENT_TECH_PDF':
            this.demandePaiementData.mapDocumentsName[6] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR':
            this.demandePaiementData.mapDocumentsName[7] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'DECISION_ATTRIBUTIVE_PAIE':
            this.demandePaiementData.mapDocumentsName[8] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'CERTIFICAT_PAIEMENT_PDF':
            this.demandePaiementData.mapDocumentsName[9] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
        }
      }
    });

    // récupération de la liste des anomalies
    this.demandePaiementData.anomalies = demandePaiementDto.anomalies;

    // maj des id techniques
    this.idDemandePaiement = demandePaiementDto.idDemande;
    if (this.idDemandePaiement) {
      this.idDossierSubvention = demandePaiementDto.idDossierSubvention;
    }
    this.demandePaiementData.version = demandePaiementDto.version;
    this.demandePaiementData.isPremiereDemande = demandePaiementDto.isPremiereDemande;
    this.demandePaiementData.hasUnAcompte = demandePaiementDto.hasUnAcompte;
    if (this.demandePaiementData.hasUnAcompte === true) {
      this.demandePaiementData.listeMenuTypeDemande = [{code: 'ACOMPTE', libelle: 'Acompte'}, {code: 'SOLDE', libelle: 'Solde'}];
    }
    this.demandePaiementData.codeEtatDemande = demandePaiementDto.codeEtatDemande;
    if (demandePaiementDto.codeEtatDemande) {
      this.demandePaiementData.libelleEtatDemande = this.gestionAffichageChampsService.recupererEtatLibelle(
        demandePaiementDto.codeEtatDemande,
        this.demandePaiementData.infosUtilisateur.listeRoles
      );
    }
    this.demandePaiementData.dateDemande = demandePaiementDto.dateDemande;
    this.demandePaiementData.codeDepartement = demandePaiementDto.codeDepartement;
    this.demandePaiementData.nomLongCollectivite = demandePaiementDto.nomLongCollectivite;
    this.demandePaiementData.montantTotalHtTravaux = demandePaiementDto.montantTotalHtTravaux;
    // maj des etats des champs
    this.majEtatFormulaire(demandePaiementDto.typeDemande.code);

    // maj du calcul de l'aide demandee
    this.majCalculAideDemandee(demandePaiementDto.typeDemande.code);
  }

  /**
   * Initialisation du haut du formulaire.
   *
   * @returns instance du formulaire
   */
  private initFormulaireHaut(): FormGroup {
    return this.formBuilder.group({
      numDossier: new FormControl({ value: '', disabled: true }),
      resteConsommer: new FormControl({ value: '', disabled: true })
    });
  }

  /**
   * Initialisation du bas du formulaire.
   *
   * @returns instance du formulaire
   */
  private initFormulaireBas(): FormGroup {
    return this.formBuilder.group({
      montantTravauxHt: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      tauxAideFacePercent: new FormControl(
        '',
        Validators.compose([Validators.required])
      )
    });
  }

  /**
  * La mise à jour du type de demande de paiement,
  * implique une mise à jour de l'état du formulaire.
  *
  * @param event sélection utilisateur
  */
  public majTypeDemande(event: any): void {
    // mise à jour de l'état du formulaire
    // en fonction de la nouvelle valeur sélectionnée
    this.majEtatFormulaire(event.value.code);

    // Remise au max possible des champs.
    if (event.value.code === 'AVANCE') {
      this.maxActuel = this.montantSubvention;
      this.maxTauxActuel = this.maxTauxAvanceFace;
      this.demandePaiementData.formulairePrincipal.controls['montantTotalDemande'].setValue(this.montantSubvention);
    } else {
      this.maxActuel = undefined;
      this.maxTauxActuel = undefined;
      this.demandePaiementData.formulairePrincipal.controls['montantTotalDemande'].setValue(0);
    }
    this.demandePaiementData.formulairePrincipal.controls['montantTravauxHt'].setValue(this.maxActuel);
    this.demandePaiementData.formulairePrincipal.controls['tauxAideFacePercent'].setValue(this.maxTauxActuel);
    this.majCalculAideDemandee(event.value.code);
  }

  /**
   * Vérifie la présence d'anomalies non corrigées pour la qualification de la demande en cours.
   */
  public contientAnomaliesPourQualification(): boolean {
    return this.isAuMoinsUneAnomalieACorriger(this.demandePaiementData.anomalies);
  }

  /**
   * Telecharge le certificat de demande de paiement.
   */
  public genererDecisionAttributive() {
    this.paiementDemandeService.genererDecisionAttributive(this.idDemandePaiement).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });

  }

  /**
   * Ajout d'un fichier à uploader.
   *
   * @param typeDocument type du document envoyé
   * @param event evenement contenant le fichier envoyé
   */
  public ajoutFichierUploader(typeDocument: string, event: any) {
    const fichier: File = event.target.files[0];

    // ajout du fichier
    if (event.target.files && fichier) {
      if(!this.demandePaiementData.formulaireFichiers.has(typeDocument)) {
        this.demandePaiementData.formulaireFichiers.append(typeDocument, fichier);
      } else {
        this.demandePaiementData.formulaireFichiers.set(typeDocument, fichier);
      }
    }

    // mise à jour de l'état
    Object.values(this.demandePaiementData.listeEtatsDocs).forEach(etatDoc => {
      if (etatDoc.codeDocument === typeDocument) {
        etatDoc.selectionne = true;
        return;
      }
    });

    // maj du flag pour l'activation/désactivatoin du bouton 'Demander' ou 'Refuser'
    this.majBoutonDemanderRefuser();
  }

  /**
   * Activation/désactivatoin du bouton 'Demander' et 'Refuser'.
   *
   */
  private majBoutonDemanderRefuser(): void {
    let isTousDocsTeleverses = true;

    // tous les docs doivent avoir 'selectionne' à true pour que le bouton soit activé
    Object.values(this.demandePaiementData.listeEtatsDocs).forEach(etatDoc => {
      if (etatDoc.selectionne === false) {
        isTousDocsTeleverses = false;
        return;
      }
    });

    // mise à jour
    this.demandePaiementData.demanderRefuserDisabled = !isTousDocsTeleverses;
  }

  /**
   * Ajout d'un document copmplémentaire à uploader.
   *
   * @param event evenement contenant le fichier envoyé
   */
  public ajoutDocComplementaire(event: any) {
    const fichier: File = event.target.files[0];
    this.showButton = true;
    this.excelExtention = this.isExcelExtention(fichier);
    const nouveauDocument: DocumentDto = {
      id: null,
      codeTypeDocument: 'DOC_COMPLEMENTAIRE_PAIEMENT',
      nomFichierSansExtension: fichier.name,
      extensionFichier: null,
      dateTeleversement: null
    };
    this.demandePaiementData.documentsSupplementaires.push(nouveauDocument);
    this.demandePaiementData.documentsSupplementairesRajoutes.push(nouveauDocument);
    this.demandePaiementData.fichiersDocsSuppl.push(fichier);
  }

  /**
   * Suppression d'un fichier à uploader.
   *
   * @param typeDocument - type du document à supprimer
   */
  public supprimerFichierUploader(typeDocument: string) {
    if (this.demandePaiementData.formulaireFichiers.has(typeDocument)) {
      this.demandePaiementData.formulaireFichiers.delete(typeDocument);
    }
  }

  /**
   * Téléchargement d'un fichier via son index.
   *
   * @param index - index du document à télécharger
   */
  public telechargerDocumentComplementaire(index: number) {

    const document = this.demandePaiementData.documentsSupplementaires[index];

    // téléchargement par id
    if (document.id) {
      this.paiementDemandeService.telechargerDocument(document.id).subscribe(response => {
        this.fileSaverService.telechargerFichier(response);
      });
    } else {
      // ou téléchargement de doc complémentaire (en méméoire)
      const fichier: File = this.demandePaiementData.fichiersDocsSuppl[index];
      const nomFichier: string = document.nomFichierSansExtension;
      const typeFichier: string = document.extensionFichier;
      this.fileSaverService.telechargerFichierFromFile(fichier, nomFichier, typeFichier);
    }
  }


  /**
   * Suppression d'un fichier via son index.
   *
   * @param index - index du document à supprimer
   */
  public supprimerDocComplementaire(index: number) {
    // alimentation de la liste des id docs à supprimer
    const idDocSuppr: number = this.demandePaiementData.documentsSupplementaires[index].id;
    if (idDocSuppr) {
      this.demandePaiementData.idsDocsComplSuppr.push(idDocSuppr);
    }
    const indexFichiersRajoutes = this.demandePaiementData.documentsSupplementairesRajoutes.findIndex(document => this.demandePaiementData.documentsSupplementaires[index].nomFichierSansExtension === document.nomFichierSansExtension);

    this.demandePaiementData.documentsSupplementaires.splice(index, 1);
    this.demandePaiementData.fichiersDocsSuppl.splice(indexFichiersRajoutes, 1);
    this.demandePaiementData.documentsSupplementairesRajoutes.slice(indexFichiersRajoutes, 1);
  }

  /**
   * Envoyer la demande de paiement.
   *
   */
  public envoyerDemandePaiement(): void {
    this.desactiverBoutonDemande = true;
    // init. du formulaire avec les fichiers uploades
    const formulaireAvecFichiers = this.initFormData(true);
    // appel du service de maj utilisateur
    this.paiementDemandeService.realiserDemandePaiement$(formulaireAvecFichiers)
    .pipe(
      tap(
        _ => this.activerOuDesactiverBoutonDemande()
      ),
        filter(demandePaiementDto => !!demandePaiementDto.numDossier)
    ).subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // suppression des fichiers à uploader
        this.demandePaiementData.formulaireFichiers = new FormData();

        // message de succès
        this.messageManagerService.realiserDemandePaiementMessage();

      });
  }

  public activerOuDesactiverBoutonDemande() {
    this.desactiverBoutonDemande = !this.desactiverBoutonDemande;
  }


  /**
   * Initialisation du formulaire avec fichiers.
   *
   * @param [avecFormPrincipal] mapper les données du formulaire principal
   */
  initFormData(avecFormPrincipal?: boolean) {

    // init.
    const formulaireAvecFichiers = new FormData();

    if (avecFormPrincipal) {
      // mapping des valeurs du formulaire
      Object.keys(this.demandePaiementData.formulairePrincipal.controls).forEach(key => {
        let value = this.demandePaiementData.formulairePrincipal.get(key).value;
        if (value && value.code) {
          // s'il s'agit d'un menu déroulant
          // on prend juste la value et non l'objet en entier
          value = value.code;
        }
        if (value !== null) {
          formulaireAvecFichiers.append(key, value);
        }
      });
      // mapping champs speciaux ou cachés
      formulaireAvecFichiers.append('version', String(this.demandePaiementData.version));
      formulaireAvecFichiers.append('codeEtatDemande', this.demandePaiementData.codeEtatDemande);
      formulaireAvecFichiers.append('dateDemande', String(this.demandePaiementData.dateDemande));
    }


    // ajout des fichiers supplémentaires au formulaire de fichiers
    this.demandePaiementData.documentsSupplementairesRajoutes.forEach((document, index) => {
      if (document.id === null) {
        this.demandePaiementData.formulaireFichiers.append('DOC_COMPLEMENTAIRE_PAIEMENT', this.demandePaiementData.fichiersDocsSuppl[index]);
      }
    });

    // mapping des fichiers uploadés
    for (const fichier of this.demandePaiementData.formulaireFichiers.entries()) {
      formulaireAvecFichiers.append(fichier[0], fichier[1]);
    }

    // mapping de la liste des id des docs complémentaires à supprimer
    formulaireAvecFichiers.append('idsDocsComplSuppr', String(this.demandePaiementData.idsDocsComplSuppr));

    // mapping des id
    formulaireAvecFichiers.append('idDossierSubvention', String(this.idDossierSubvention));
    if (this.idDemandePaiement) {
      // pour une demande existante
      formulaireAvecFichiers.append('idDemande', String(this.idDemandePaiement));
    }
    return formulaireAvecFichiers;
  }

  /**
   * Mise à jour du calcul : aide demandéee.
   */
  public majCalculAideDemandee(typeDemande: string): void {

    if(this.demandePaiementData.codeEtatDemande === 'INITIAL' && typeDemande !== 'AVANCE'){
      this.demandePaiementData.formulairePrincipal.get('montantTravauxHt').setValue(this.max(this.demandePaiementData.formulairePrincipal.get('montantTotalDemande').value - this.demandePaiementData.montantTotalHtTravaux, 0));
    }
    const montantTravauxHt: number = +this.demandePaiementData.formulairePrincipal.get('montantTravauxHt').value;
    const tauxAide: number = +this.demandePaiementData.formulairePrincipal.get('tauxAideFacePercent').value;
    let aideDemandee = montantTravauxHt * (tauxAide / 100);
    this.demandePaiementData.formulairePrincipal.get('aideDemandee').setValue(aideDemandee);
  }

  /**
   * Telecharger un document.
   */
  public telechargerDocumentPaiement(typeDocument: string) {
    const idDocument = this.demandePaiementData.mapDocuments.get(typeDocument);

    if (!idDocument) {
      // message de succès
      this.messageManagerService.documentIntrouvableMessage();

      return;
    }

    // téléchargement par id
    this.paiementDemandeService.telechargerDocument(idDocument).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });

  }

  /**
   * Passe la demande de paiement en ANOMALIE_DETECTEE.
   */
  public majDemandePaiementAnomalieDetectee() {
    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementAnomalieDetectee$(this.idDemandePaiement)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  /**
   * Passe la demande de paiement en ANOMALIE_SIGNALEE.
   */
  public majDemandePaiementAnomalieSignalee() {
    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementAnomalieSignalee$(this.idDemandePaiement)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

   /**
   * Passe la demande de paiement en REFUSEE.
   */
  public majDemandePaiementRefusee() {
    this.displayRefusModal = false;
    // supprimer les messages
    this.messageService.clear();
    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementRefusee$(this.idDemandePaiement, this.demandePaiementData.formulairePrincipal.value)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  /**
   * Passe la demande de paiement en QUALIFIEE.
   */
  public majDemandePaiementQualifiee() {
    // init. du formulaire avec les fichiers uploades
    const formulaireAvecFichiers = this.initFormData(false);

    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementQualifiee$(formulaireAvecFichiers)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  

  /**
   * Passe la demande de paiement en ACCORDEE.
   */
  public majDemandePaiementAccordee() {
    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementAccordee$(this.idDemandePaiement)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  /**
   * Passe la demande de paiement en CONTROLEE.
   */
  public majDemandePaiementControlee() {
    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementControlee$(this.idDemandePaiement)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  /**
   * Passe la demande de paiement en EN_ATTENTE_TRANSFERT.
   */
  public majDemandePaiementEnAttenteTransfert() {
    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementEnAttenteTransfert$(this.idDemandePaiement)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);

        // message de succès
        this.messageManagerService.MiseAJourMessage();
      });
  }

  /**
   * Enregistre la demande de paiement pour correction.
   */
  public majDemandePaiementPourCorrection() {
    // init. du formulaire avec les fichiers uploades
    const formulaireAvecFichiers = this.initFormData(true);

    // supprimer les messages
    this.messageService.clear();

    // envoyer la maj
    this.paiementDemandeService.majDemandePaiementPourCorrection$(formulaireAvecFichiers)
      .subscribe((demandePaiementDto: DemandePaiementDto) => {

        // maj formulaires et id techniques
        this.majFormulaireEtIds(demandePaiementDto);
        this.showMessageAnomalies();
      });
  }

  private showMessageAnomalies() {
    this.messageManagerService.penserRepondreAnomaliesMessage();
    this.messageAnomalieElement.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  public afficherOngletInstruction(): boolean {
    return this.isDemandeExistante() && (!this.droitService.isUniquementGenerique(this.infosUtilisateur));
  }

  /**
   * Lors de l'appui sur 'Annuler' : on remet à zéro les libelles des boutons puis on recharge le formulaire.
   *
   */
  public reload(): void {
    this.viewChildren.forEach(customFileTransfer => customFileTransfer.reset());
    this.ngOnInit();
  }

  /**
    * S'il existe au moins une anomalie "A corriger".
    *
    * @param anomalies - liste des anomalies
    * @returns true ou false
    */
  public isAuMoinsUneAnomalieACorriger(anomalies: AnomalieDto[]): boolean {
    return anomalies.some((anomalie) => anomalie.corrigee === false);
  }

  /**
   * Initialise la liste des états pour chaque document à téléverser.
   *
   */
  public majEtatDocs() {
    this.demandePaiementData.listeEtatsDocs = <EtatDocUpload[]>[];
    let etatDoc: EtatDocUpload;

    if (this.demandePaiementData.etatForm.etatMarchesPasses.visible
      && this.demandePaiementData.etatForm.etatMarchesPasses.editable) {
      etatDoc = { codeDocument: 'ETAT_MARCHES_PASSES_PDF', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
      etatDoc = { codeDocument: 'ETAT_MARCHES_PASSES_TABLEUR', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
    }
    if (this.demandePaiementData.etatForm.etatRealisationTravaux.visible
      && this.demandePaiementData.etatForm.etatRealisationTravaux.editable) {
      etatDoc = { codeDocument: 'ETAT_REALISATION_TRAVAUX_PDF', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
      etatDoc = { codeDocument: 'ETAT_REALISATION_TRAVAUX_TABLEUR', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
    }
    if (this.demandePaiementData.etatForm.etatAchevementFinancier.visible
      && this.demandePaiementData.etatForm.etatAchevementFinancier.editable) {
      etatDoc = { codeDocument: 'ETAT_ACHEVEMENT_FINA_PDF', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
      etatDoc = { codeDocument: 'ETAT_ACHEVEMENT_FINANCIER_TABLEUR', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
    }
    if (this.demandePaiementData.etatForm.etatAchevementTech.visible
      && this.demandePaiementData.etatForm.etatAchevementTech.editable) {
      etatDoc = { codeDocument: 'ETAT_ACHEVEMENT_TECH_PDF', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
      etatDoc = { codeDocument: 'ETAT_ACHEVEMENT_TECHNIQUE_TABLEUR', selectionne: false };
      this.demandePaiementData.listeEtatsDocs.push(etatDoc);
    }
  }

  max(num1: number, num2: number): number {
    return Math.max(num1, num2);
  }

  isExcelExtention(fichier: File) : boolean {
    // l'une des extensions correspond
    let extensionValide = false;
    const excelExtension = ['.ods','.xls','.xlsx'];


      Object.values(excelExtension).forEach(ext => {
        if (fichier.name.endsWith(ext)) {
          extensionValide = true;
          return;
        }
      });

    // l'extension à été trouvée ?
    if (extensionValide) {
      return true;
    }

    return false;

  }
  showModal(index) {
    const fichier: File = this.demandePaiementData.fichiersDocsSuppl[index];

    if(this.isExcelExtention(fichier)){
      this.previewExcelFile(fichier);
      this.isPdfContent = false;
      this.isExcelContent = true;
    }
    else {
      var reader = new FileReader();
      reader.onload = (event: any) => {
          this.localUrl = event.target.result;
          this.isPdfContent = true;
          this.isExcelContent = false;
      }
      reader.readAsDataURL(fichier);
    }
   this.displayModal = true;
  }

  private previewExcelFile(fichier) {

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* lire workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* récupérer première page */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* enregistrer données */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false, range: 10}));

      this.headData = this.data[0];
      this.data = this.data.slice(1); // supprimer le premier enregistrement

      const ws2: XLSX.WorkSheet = wb.Sheets[wb.SheetNames[1]];
      this.readDataSheet(ws2, 10);
    };

    reader.readAsBinaryString(fichier);
  }

  private readDataSheet(ws: XLSX.WorkSheet, startRow: number) {
    /* save data */
    let datas = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false, range: startRow}));
    let headDatas = datas[0];
    datas = datas.slice(1); // supprimer le premier enregistrement

    for (let i = 0; i < this.data.length; i++) {
      this.data[i][this.headData.length] = datas.filter(x => x[12] == this.data[i][0])
    }
  }
}

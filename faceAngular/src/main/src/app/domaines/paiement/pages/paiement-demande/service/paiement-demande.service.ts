import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { RechercheDemandePaiementDto } from '@paiement/pages/paiement-demande/dto/recherche-demande-paiement.dto';
import { Observable } from 'rxjs';
import { DemandePaiementDto } from '@paiement/dto/demande-paiement.dto';
import { AnomalieDto } from '@app/dto/anomalie.dto';

@Injectable({
  providedIn: 'root'
})
export class PaiementDemandeService {

  // constructeur
  constructor(
    private httpService: HttpService
  ) { }

  /**
   * Initialise une nouvelle demande de paiement pour un id de dossier de subvention.
   *
   * @param idDossier - id du dossier subvention
   * @returns formulaire de demande de paiement
   */
  initNouvelleDemandePaiement$(idDossier: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/dossier/${idDossier}/initialiser`);
  }

  /**
   * Permet de consulter une demande de paiement via son id.
   *
   * @param idDemande - id de la demande de paiement
   * @returns formulaire de demande de paiement
   */
  rechercheDemandePaiement$(idDemande: number): Observable<RechercheDemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemande}/details`);
  }

  /**
   * Réalise une nouvelle demande de paiement pour le dossier
   *
   * @param demandePaiementDto demande de paiement
   * @returns la demande de paiement réalisée
   */
  realiserDemandePaiement$(data: FormData): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/paiement/demander`, data, true, data);
  }

  /**
   * Génération de la décision attributive.
   * @param idDemandePaiement - id de la demannde de paiement
   */
  genererDecisionAttributive(idDemandePaiement: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/paiement/demande/${idDemandePaiement}/generation-decision-attributive`);
  }

  /**
   * Téléchargement d'un document.
   * @param typeDocument - type du document a télécharger
   */
  telechargerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/document/${idDocument}/telecharger`);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : ANOMALIE_DETECTEE.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementAnomalieDetectee$(idDemandePaiement: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemandePaiement}/maj-etat/anomalie-detectee`);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : ANOMALIE_SIGNALEE.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementAnomalieSignalee$(idDemandePaiement: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemandePaiement}/maj-etat/anomalie-signalee`);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : REFUSEE.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @param formulaireDto - données du formulaire
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementRefusee$(idDemandePaiement: number, formulaireDto: DemandePaiementDto): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequetePost(`/paiement/demande/${idDemandePaiement}/maj-etat/refusee`, formulaireDto);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : QUALIFIEE.
   *
   * @param data - données du formulaire
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementQualifiee$(data: FormData): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/paiement/demande/maj-etat/qualifiee`, data);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : ACCORDEE.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementAccordee$(idDemandePaiement: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemandePaiement}/maj-etat/accordee`);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : CONTROLEE.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementControlee$(idDemandePaiement: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemandePaiement}/maj-etat/controlee`);
  }

  /**
   * Met à jour l'état de la demande de paiement avec l'état : EN_COURS_TRANSFERT.
   *
   * @param idDemandePaiement - id de la demannde de paiement
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementEnAttenteTransfert$(idDemandePaiement: number): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequeteGet(`/paiement/demande/${idDemandePaiement}/maj-etat/en-attente-transfert`);
  }

  /**
   * Met à jour l'état de la demande de paiement pour la correction.
   *
   * @param data - données du formulaire
   * @returns la demande de paiement mise à jour
   */
  majDemandePaiementPourCorrection$(data: FormData): Observable<DemandePaiementDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/paiement/demande/maj-etat/corrigee`, data);
  }
}

import { TestBed } from '@angular/core/testing';
import { PaiementDemandeService } from './demande-paiement.service';

describe('PaiementDemandeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaiementDemandeService = TestBed.get(PaiementDemandeService);
    expect(service).toBeTruthy();
  });
});

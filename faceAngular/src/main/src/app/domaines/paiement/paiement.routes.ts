import { RouterModule, Routes } from '@angular/router';
// Interne
import { PaiementDemandeComponent } from './pages/paiement-demande/paiement-demande.component';
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { PaiementRechercheComponent } from './pages/paiement-recherche/paiement-recherche.component';
import { NgModule } from '@angular/core';

export const PAIEMENT_ROUTES: Routes = [
  // Demande de paiement
  {
    path: 'demande/dossier/:idDossier',
    component: PaiementDemandeComponent,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Demande de Paiement' }
  },
  // consulter une demande de paiement
  {
    path: 'demande/:idDemandePaiement/details',
    component: PaiementDemandeComponent,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Visualisation de la demande de paiement' }
  },
  // Recherche un dossier de subvention
  {
    path: 'recherche',
    component: PaiementRechercheComponent,
    data: { title: 'Recherche de paiements' },
    resolve: { componentMsg: ComponentResolverMsg }
  }
];
const routes: Routes = [
  {
    path: '',
    children: PAIEMENT_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
]

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class PaiementRouteModule {

}

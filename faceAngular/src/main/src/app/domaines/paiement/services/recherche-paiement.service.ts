import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { CritereRecherchePaiementDto } from '@paiement/pages/paiement-demande/dto/critere-recherche-paiement.dto';
import { Observable } from 'rxjs';
import { PageReponseDto } from '@app/dto/page-reponse.dto';

@Injectable({
  providedIn: 'root'
})
export class RecherchePaiementService {

  constructor( private httpService: HttpService) { }

   /**
   * Recherche les dossiers correspondant aux criteres donnés.
   *
   * @param criteresRecherche - les criteres de recherche
   * @returns La page reponse qui contient un tableau de resultats ResultatRechercheDossierDto
   */
  getResultatRecherchePaiementDto$(criteresRecherche: CritereRecherchePaiementDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/paiement/dossier/criteres/rechercher`, criteresRecherche);
  }
}

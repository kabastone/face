import { Injectable } from '@angular/core';
import { GestionDemandePaiementForm } from '@paiement/dto/gestion-demande-paiement-form';
import { MetadataForm } from '@app/dto/metadata-form';

@Injectable({
  providedIn: 'root'
})
export class GestionAffichagePaiementService {

  constructor() { }

  private readonly EDITABLE: MetadataForm = { visible: true, editable: true };
  private readonly VISIBLE: MetadataForm = { visible: true, editable: false };
  private readonly NONE: MetadataForm = { visible: false, editable: false };

  private typeDemandePaiement: string;
  private isOnlyGenerique = false;

  private isPremiereDemande: boolean;

  public recupererFormulaire(codeEtatDemande: string, roles: string[], typeDemandePaiement: string,
    isPremiereDemande: boolean): GestionDemandePaiementForm {
    this.isOnlyGenerique = false;
    const nouvelEtat: string = this.definirEtatSelonRole(codeEtatDemande, roles);
    this.typeDemandePaiement = typeDemandePaiement;
    this.isPremiereDemande = isPremiereDemande;
    return this.recupererFormulaireSelonEtat(nouvelEtat);
  }

  public recupererEtatLibelle(etat: string, roles: string[]) {
    const nouvelEtat: string = this.definirEtatSelonRole(etat, roles);
    return this.recupererEtatLibelleString(nouvelEtat);
  }


  public recupererEtatLibelleString(nouvelEtat: string) {
    switch (nouvelEtat) {
      case 'ANOMALIE_DETECTEE':
        return 'Détectée en anomalie';
      case 'DEMANDEE':
        return 'Demandée';
      case 'CORRIGEE':
        return 'Corrigée';
      case 'CERTIFIEE':
        return 'Certifiée';
      case 'APPROUVEE':
        return 'Approuvée';
      case 'QUALIFIEE':
        return 'Qualifiée';
      case 'CONTROLEE':
        return 'Contrôlée';
      case 'ACCORDEE':
        return 'Accordée';
      case 'INITIAL':
        return 'Initial';
      case 'ANOMALIE_SIGNALEE':
        return 'Signalée en anomalie';
      case 'REFUSEE':
        return 'Refusée';
      case 'ATTRIBUEE':
        return 'Attribuée';
      case 'VERSEE':
        return 'Versée';
      case 'TRANSFEREE':
        return 'Transférée';
      case 'EN_ATTENTE_TRANSFERT':
        return 'En attente de transfert initial';
      case 'EN_COURS_TRANSFERT_1':
        return 'En cours de transfert - étape 1';
      case 'EN_COURS_TRANSFERT_2':
        return 'En cours de transfert - étape 2';
      case 'EN_COURS_TRANSFERT_FIN':
        return 'En cours de transfert - fin';
      case 'EN_INSTRUCTION':
      case 'EN_INSTRUCTION_GENERIQUE':
      default:
        return 'En instruction';
    }
  }

  private definirEtatSelonRole(codeEtatDemande: string, roles: string[]) {
    let etatAccessible = false;
    let roleMinistere = true;

    if (roles.includes('MFER_INSTRUCTEUR')) {
      switch (codeEtatDemande) {
        case 'DEMANDEE':
        case 'CORRIGEE':
        case 'REFUSEE':
        case 'VERSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('MFER_RESPONSABLE')) {
      switch (codeEtatDemande) {
        case 'DEMANDEE':
        case 'CORRIGEE':
        case 'QUALIFIEE':
        case 'ANOMALIE_DETECTEE':
        case 'REFUSEE':
        case 'VERSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('SD7_INSTRUCTEUR')) {
      switch (codeEtatDemande) {
        case 'ACCORDEE':
        case 'REFUSEE':
        case 'VERSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('SD7_RESPONSABLE')) {
      switch (codeEtatDemande) {
        case 'ACCORDEE':
        case 'CONTROLEE':
        case 'TRANSFEREE':
        case 'REFUSEE':
        case 'VERSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.length === 1 && roles.includes('GENERIQUE')) {
      this.isOnlyGenerique = true;
      switch (codeEtatDemande) {
        case 'INITIAL':
        case 'ANOMALIE_SIGNALEE':
        case 'REFUSEE':
        case 'VERSEE':
          etatAccessible = true;
      }
      roleMinistere = false;
    }

    if (etatAccessible) {
      return codeEtatDemande;
    }

    if (roleMinistere) {
      return 'EN_INSTRUCTION';
    }

    // aucun cas spécifique : EN_INSTRUCTION_GENERIQUE
    return 'EN_INSTRUCTION_GENERIQUE';
  }

  private recupererFormulaireSelonEtat(codeEtatDemande: string): GestionDemandePaiementForm {
    switch (codeEtatDemande) {
      case 'INITIAL': { // création
        return this.getFormForInitiale();
      }
      case 'EN_INSTRUCTION': {
        return this.getFormForEnInstruction();
      }
      case 'EN_INSTRUCTION_GENERIQUE': {
        return this.getFormForEnInstructionGenerique();
      }
      case 'DEMANDEE': {
        return this.getFormForDemandee();
      }
      case 'QUALIFIEE': {
        return this.getFormForQualifiee();
      }
      case 'ACCORDEE': {
        return this.getFormForAccordee();
      }
      case 'CONTROLEE': {
        return this.getFormForControlee();
      }
      case 'EN_COURS_TRANSFERT': {
        return this.getFormForEnCoursTransfert();
      }
      case 'TRANSFEREE': {
        return this.getFormForTransferee();
      }
      case 'ANOMALIE_DETECTEE': {
        return this.getFormForDetecteeAnomalie();
      }
      case 'ANOMALIE_SIGNALEE': {
        return this.getFormForSignaleeAnomalie();
      }
      case 'CORRIGEE': {
        return this.getFormForCorrigee();
      }
      case 'REFUSEE': {
        return this.getFormForRefusee();
      }
      case 'VERSEE': {
        return this.getFormForVersee();
      }
    }

    return null;
  }

  // ETAT Initial
  public getFormForInitiale(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.EDITABLE;
    form.message = this.EDITABLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.EDITABLE;
      form.montantTotalDemande = this.EDITABLE;
      form.tauxAideAvance = this.NONE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.EDITABLE;
      form.tauxAideAvance = this.EDITABLE;
    }
    form.certificatPaiement = this.EDITABLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.EDITABLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.EDITABLE;
      form.tauxAideFacePercent = this.EDITABLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.aideDemandee = this.NONE;
      form.etatAchevementFinancier = this.EDITABLE;
      form.etatAchevementTech = this.EDITABLE;
      form.tauxAideFacePercent = this.NONE;
    } else {
      form.aideDemandee = this.VISIBLE;
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.EDITABLE;
    form.annuler = this.EDITABLE;
    form.demander = this.EDITABLE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.NONE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.NONE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Partie : anomalie */
    form.ongletAnomalie = this.NONE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;
    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForInitial

  // ETAT EnInstruction
  private getFormForEnInstruction(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
    }
    form.tauxAideFacePercent = this.VISIBLE;
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.NONE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    form.ongletAnomalie = this.NONE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;
    form.btnCorrectionAnomalie = this.NONE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForEnInstruction


  // ETAT EnInstructionGenerique
  private getFormForEnInstructionGenerique(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.NONE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    form.ongletAnomalie = this.NONE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;
    form.btnCorrectionAnomalie = this.NONE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForEnInstructionGenerique


  // Etat SignaleeAnomalie
  private getFormForSignaleeAnomalie(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.EDITABLE;
      form.montantTotalDemande = this.EDITABLE;
      form.tauxAideFacePercent = this.EDITABLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.EDITABLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.EDITABLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.EDITABLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.EDITABLE;
      form.etatAchevementTech = this.EDITABLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.EDITABLE;
    form.annuler = this.EDITABLE;
    form.demander = this.NONE;
    form.repondre = this.EDITABLE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.NONE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.NONE;
    form.tauxAideAvance = this.NONE;


    return form;
  } // getFormForSignaleeAnomalie

  // ETAT Refusee
  private getFormForRefusee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.NONE;
    form.motifRefus = this.VISIBLE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    if (this.isOnlyGenerique) {
      form.ongletAnomalie = this.NONE;
    } else {
      form.ongletAnomalie = this.VISIBLE;
    }
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.VISIBLE;
    form.btnCorrectionAnomalie = this.VISIBLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForRefusee


  // ETAT Versee
  private getFormForVersee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    form.ongletAnomalie = this.NONE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;
    form.btnCorrectionAnomalie = this.NONE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForVersee

  // ETAT Demandee
  private getFormForDemandee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.EDITABLE;
    form.decisionAttributiveSignee = this.EDITABLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.EDITABLE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.EDITABLE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.EDITABLE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.EDITABLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForDemandee


  // ETAT Corrigee
  private getFormForCorrigee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.EDITABLE;
    form.decisionAttributiveSignee = this.EDITABLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.EDITABLE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.EDITABLE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.EDITABLE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.EDITABLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForCorrigee

  // ETAT Qualifiee
  private getFormForQualifiee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.EDITABLE;
    form.decisionAttributiveSignee = this.EDITABLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.NONE;
    form.accorder = this.EDITABLE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.EDITABLE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.EDITABLE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.EDITABLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForQualifiee


  // ETAT DetecteeAnomalie
  private getFormForDetecteeAnomalie(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.EDITABLE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.EDITABLE;
    form.refuser = this.EDITABLE;
    form.corriger = this.EDITABLE;

    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.EDITABLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForDetecteeAnomalie


  // ETAT Accordee
  private getFormForAccordee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message =  this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.EDITABLE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.EDITABLE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.EDITABLE;



    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.VISIBLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForAccordee

  // ETAT Controlee
  private getFormForControlee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.EDITABLE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.EDITABLE;
    form.detecterAnomalie = this.EDITABLE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.EDITABLE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.EDITABLE;
    form.reponseAnomalie = this.EDITABLE;
    form.btnCorrectionAnomalie = this.VISIBLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForControlee

  // ETAT Transferee
  private getFormForTransferee(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;


    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.VISIBLE;
    form.btnCorrectionAnomalie = this.VISIBLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForTransferee

  // ETAT EnCoursTransfert
  private getFormForEnCoursTransfert(): GestionDemandePaiementForm {
    const form: GestionDemandePaiementForm = {} as GestionDemandePaiementForm;

    /* Partie recherche */
    form.numDossier = this.VISIBLE;
    form.resteConsommer = this.VISIBLE;

    /* Partie : demande de paiement */
    form.dateDemande = this.VISIBLE;
    form.typeDemande = this.VISIBLE;
    form.message = this.VISIBLE;
    if (this.isAccompteOuSolde()) {
      form.montantTravauxHt = this.VISIBLE;
      form.montantTotalDemande = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
    } else {
      form.montantTravauxHt = this.NONE;
      form.montantTotalDemande = this.NONE;
      form.tauxAideFacePercent = this.NONE;
    }
    form.aideDemandee = this.VISIBLE;
    form.certificatPaiement = this.VISIBLE;
    if (this.isAvance() || this.isPremiereDemande) {
      form.etatMarchesPasses = this.VISIBLE;
    } else {
      form.etatMarchesPasses = this.NONE;
    }

    if (this.isAccompte()) {
      form.etatRealisationTravaux = this.VISIBLE;
    } else {
      form.etatRealisationTravaux = this.NONE;

    }
    if (this.isSolde()) {
      form.etatAchevementFinancier = this.VISIBLE;
      form.etatAchevementTech = this.VISIBLE;
    } else {
      form.etatAchevementFinancier = this.NONE;
      form.etatAchevementTech = this.NONE;
    }

    form.docsComplementaires = this.VISIBLE;
    form.annuler = this.NONE;
    form.demander = this.NONE;
    form.repondre = this.NONE;

    /* Partie : instruction */
    form.etatInstruction = this.VISIBLE;
    form.genererDecisionAttributive = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;
    form.motifRefus = this.NONE;
    form.qualifier = this.NONE;
    form.accorder = this.NONE;
    form.controler = this.NONE;
    form.transferer = this.NONE;
    form.detecterAnomalie = this.NONE;
    form.signalerAnomalie = this.NONE;
    form.refuser = this.NONE;



    /* Anomalie */
    form.ongletAnomalie = this.VISIBLE;
    form.btnAjouterAnomalie = this.NONE;
    form.reponseAnomalie = this.VISIBLE;
    form.btnCorrectionAnomalie = this.VISIBLE;
    form.tauxAideAvance = this.NONE;

    return form;
  } // getFormForEnCoursTransfert

  /**
   * S'il s'agit d'une demande en 'Accompte' ou en 'Solde'.
   *
   * @returns true ou false
   */
  private isAccompteOuSolde(): boolean {
    if (this.typeDemandePaiement === 'ACOMPTE' || this.typeDemandePaiement === 'SOLDE') {
      return true;
    }
    return false;
  }

  /**
   * S'il s'agit d'une demande en 'Accompte'.
   *
   * @returns true ou false
   */
  private isAccompte(): boolean {
    if (this.typeDemandePaiement === 'ACOMPTE') {
      return true;
    }
    return false;
  }

  /**
   * S'il s'agit d'une demande en 'Solde'.
   *
   * @returns true ou false
   */
  private isSolde(): boolean {
    if (this.typeDemandePaiement === 'SOLDE') {
      return true;
    }
    return false;
  }

  /**
 * S'il s'agit d'une demande en 'Avance'.
 *
 * @returns true ou false
 */
  private isAvance(): boolean {
    if (this.typeDemandePaiement === 'AVANCE') {
      return true;
    }
    return false;
  }
}

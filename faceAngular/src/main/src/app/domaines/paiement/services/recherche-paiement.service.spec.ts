import { TestBed } from '@angular/core/testing';

import { RecherchePaiementService } from './recherche-paiement.service';

describe('RecherchePaiementService', () => {
  let service: RecherchePaiementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecherchePaiementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

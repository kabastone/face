import { TestBed } from '@angular/core/testing';

import { GestionAffichagePaiementService } from './gestion-affichage-paiement.service';

describe('GestionAffichagePaiementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestionAffichagePaiementService = TestBed.get(GestionAffichagePaiementService);
    expect(service).toBeTruthy();
  });
});

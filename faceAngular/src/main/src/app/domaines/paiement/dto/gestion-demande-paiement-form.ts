import { MetadataForm } from '@app/dto/metadata-form';

export interface GestionDemandePaiementForm {

  /* Partie recherche */
  numDossier: MetadataForm;
  resteConsommer: MetadataForm;

  /* Partie : demande de paiement */
  dateDemande: MetadataForm;
  typeDemande: MetadataForm;
  montantTravauxHt: MetadataForm;
  montantTotalDemande: MetadataForm;
  tauxAideFacePercent: MetadataForm;
  aideDemandee: MetadataForm;
  certificatPaiement: MetadataForm;
  etatMarchesPasses: MetadataForm;
  etatRealisationTravaux: MetadataForm;
  etatAchevementFinancier: MetadataForm;
  etatAchevementTech: MetadataForm;
  docsComplementaires: MetadataForm;
  annuler: MetadataForm;
  demander: MetadataForm;
  repondre: MetadataForm;
  tauxAideAvance: MetadataForm;
  message: MetadataForm;

  /* Partie : instruction */
  etatInstruction: MetadataForm;
  genererDecisionAttributive: MetadataForm;
  decisionAttributiveSignee: MetadataForm;
  motifRefus: MetadataForm;
  qualifier: MetadataForm;
  accorder: MetadataForm;
  controler: MetadataForm;
  transferer: MetadataForm;
  detecterAnomalie: MetadataForm;
  signalerAnomalie: MetadataForm;
  refuser: MetadataForm;
  corriger: MetadataForm;

  /* Anomalie */
  ongletAnomalie: MetadataForm;
  btnAjouterAnomalie: MetadataForm;
  reponseAnomalie: MetadataForm;
  btnCorrectionAnomalie: MetadataForm;
}

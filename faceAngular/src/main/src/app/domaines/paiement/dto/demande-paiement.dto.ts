import { TypeDemandeLovDto } from '@app/dto/type-demande-lov.dto';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { AnomalieDto } from '@app/dto/anomalie.dto';

export interface DemandePaiementDto {
  numDossier: string;
  resteConsommer: number;
  idDossierSubvention: number;
  idDemande: number;
  dateDemande: Date;
  typeDemande: TypeDemandeLovDto;
  montantTravauxHt: number;
  montantTotalHtTravaux: number;
  tauxAideFacePercent: number;
  montantRestantAvance: number;
  montantRestantAcompte: number;
  aideDemandee: string;
  etatRealisationTravaux: string;
  etatAchevementFinancier: string;
  etatAchevenementTech: string;
  etatInstruction: string;
  motifRefus: string;
  codeEtatDemande: string;
  libelleEtatDemande: string;
  version: number;
  isPremiereDemande: boolean;
  hasUnAcompte: boolean;
  documents: DocumentDto[];
  anomalies: AnomalieDto[];
  tauxAvanceReglementaire: number;
  montantSubvention: number;
  message: string;

  /* Département et collectivité */
  nomLongCollectivite: string;
  codeDepartement: string;
}

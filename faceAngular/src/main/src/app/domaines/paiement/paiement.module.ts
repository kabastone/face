import { NgModule } from '@angular/core';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';

// Interne
import { PaiementDemandeComponent } from './pages/paiement-demande/paiement-demande.component';
import { PaiementDemandeModule } from './pages/paiement-demande/paiement-demande.module';
import { PaiementRechercheModule } from './pages/paiement-recherche/paiement-recherche.module';
import { PaiementRouteModule } from './paiement.routes';

@NgModule({
  declarations: [],

  imports: [
    TooltipModule,
    PaiementDemandeModule,
    PaiementRechercheModule,
    PaiementRouteModule
  ],
  exports: [
    PaiementDemandeComponent
  ]
})
export class PaiementModule { }

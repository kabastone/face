import { DotationCollectiviteRepartitionParSousProgrammeDto } from './dotation-collectivite-repartition-par-sous-programme.dto';

export interface CollectiviteRepartitionDto {

  /** The id. */
  id: number;

  /** The nom court. */
  nomCourt: string;

  /** The code departement. */
  codeDepartement: string;

  /** The dotations collectivite. */
  dotationsCollectivite: DotationCollectiviteRepartitionParSousProgrammeDto[];

 }

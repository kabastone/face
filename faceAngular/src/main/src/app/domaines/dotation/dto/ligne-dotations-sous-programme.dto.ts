export interface LigneDotationsSousProgrammeDto {

  /** The id dotation sous programme. */
  idDotationSousProgramme: number;

  /** The versionDotationSousProgramme. */
  versionDotationSousProgramme: number;

  /** The id sous programme. */
  idSousProgramme: number;

  /** The num sous action. */
  numSousActionSp: string;

  /** The abreviationSp. */
  abreviationSp: string;

  /** The descriptionSp. */
  descriptionSp: string;

  /** The montant initial annee N. */
  montantInitialAnneeN: number;

  /** The montant annee N. */
  montantAnneeN: number;

  /** The montant annee moins 1. */
  montantAnneeMoins1: number;

  /** The montant annee moins 2. */
  montantAnneeMoins2: number;

  /** The montant annee moins 3. */
  montantAnneeMoins3: number;

  /** L'annee de fin de validite du sous programme. Null si pas d'annee. */
  anneeFinValidite: number;
}

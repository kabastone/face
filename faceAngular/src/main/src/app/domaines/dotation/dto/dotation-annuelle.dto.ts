import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { DotationAnnuelleDotationCollectiviteDto } from './dootation-annuelle-dotation-collectivite.dto';

export interface DotationAnnuelleDto {

  /** The id collectivite. */
  idCollectivite: number;

  /** The annee. */
  annee: number;

  /** The documents. */
  documents: DocumentDto[];

  /** The dotations collectivite programme principal */
  dotationsCollectiviteProgrammePrincipal: DotationAnnuelleDotationCollectiviteDto[];

  /** The dotations collectivite programme principal */
  dotationsCollectiviteProgrammeSpecial: DotationAnnuelleDotationCollectiviteDto[];

  /** The somme programme principal. */
  sommeProgrammePrincipal: number;

  /** The somme programme special. */
  sommeProgrammeSpecial: number;
 }

import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';

export interface TransfertFongibiliteLovDto {

  /** The id sous programme débité. */
  sousProgrammeDebiter: SousProgrammeLovDto;

  /** The id sous programme crédité. */
  sousProgrammeCrediter: SousProgrammeLovDto;

  /** The montant transfert. */
  montant: number;

  /** The departement. */
  departement: DepartementLovDto;

  /** The collectivite. */
  collectivite: CollectiviteLovDto;

  /** The date transfert. */
  dateTransfert: Date;
 }

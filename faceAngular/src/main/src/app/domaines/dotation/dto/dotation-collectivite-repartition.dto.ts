import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { DotationDepartementDto } from './dotation-departement.dto';
import { CollectiviteRepartitionDto } from './collectivite-repartition.dto';

export interface DotationCollectiviteRepartitionDto {

  /** The code departement. */
  codeDepartement: string;

  /** The dotations departement. */
  dotationsDepartement: DotationDepartementDto[];

  /** The documents. */
  documents: DocumentDto[];

  /** The collectivites repartition. */
  collectivitesRepartition: CollectiviteRepartitionDto[];

 }

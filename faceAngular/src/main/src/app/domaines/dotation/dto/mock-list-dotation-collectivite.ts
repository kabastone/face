import { DotationCollectivite } from './dotation-collectivite';

export const REPARTITION: DotationCollectivite[] = [
    {
        collectivite: "AODE 1",
      type: [
        { sousProgrammeAbreviation: 'ap', sousProgrammeDescription: 'ap desc', dspMontant: 180 },
        { sousProgrammeAbreviation: 'ae', sousProgrammeDescription: 'ae desc', dspMontant: 450 },
        { sousProgrammeAbreviation: 'ce', sousProgrammeDescription: 'ce desc', dspMontant: 929 },
        { sousProgrammeAbreviation: 'ss', sousProgrammeDescription: 'ss desc', dspMontant: 242 },
        { sousProgrammeAbreviation: 'sf', sousProgrammeDescription: 'sf desc', dspMontant: 394 }
      ],
        somme: 3816
    },
    {
        collectivite: "AODE 2",
      type: [
        { sousProgrammeAbreviation: 'ap', sousProgrammeDescription: 'ap desc', dspMontant: 180 },
        { sousProgrammeAbreviation: 'ae', sousProgrammeDescription: 'ae desc', dspMontant: 450 },
        { sousProgrammeAbreviation: 'ce', sousProgrammeDescription: 'ce desc', dspMontant: 929 },
        { sousProgrammeAbreviation: 'ss', sousProgrammeDescription: 'ss desc', dspMontant: 242 },
        { sousProgrammeAbreviation: 'sf', sousProgrammeDescription: 'sf desc', dspMontant: 394 }
      ],
        somme: 3816
    },
    {
        collectivite: "AODE 3",
      type: [
        { sousProgrammeAbreviation: 'ap', sousProgrammeDescription: 'ap desc', dspMontant: 180 },
        { sousProgrammeAbreviation: 'ae', sousProgrammeDescription: 'ae desc', dspMontant: 450 },
        { sousProgrammeAbreviation: 'ce', sousProgrammeDescription: 'ce desc', dspMontant: 929 },
        { sousProgrammeAbreviation: 'ss', sousProgrammeDescription: 'ss desc', dspMontant: 242 },
        { sousProgrammeAbreviation: 'sf', sousProgrammeDescription: 'sf desc', dspMontant: 394 }
      ],
        somme: 3816
    },

]
export const DEPARTEMENTCOLLECTIVITE: DotationCollectivite[] = [
    {
        collectivite: "AODE 1",
      type: [
        { sousProgrammeAbreviation: 'ap', sousProgrammeDescription: 'ap desc', dspMontant: 180 },
        { sousProgrammeAbreviation: 'ae', sousProgrammeDescription: 'ae desc', dspMontant: 450 },
        { sousProgrammeAbreviation: 'ce', sousProgrammeDescription: 'ce desc', dspMontant: 929 },
        { sousProgrammeAbreviation: 'ss', sousProgrammeDescription: 'ss desc', dspMontant: 242 },
        { sousProgrammeAbreviation: 'sf', sousProgrammeDescription: 'sf desc', dspMontant: 394 }
      ],
        somme: 3816
    }

]

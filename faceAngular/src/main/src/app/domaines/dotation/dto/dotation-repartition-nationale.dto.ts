import { DotationProgrammeDto } from './dotation-programme-dto';
import { LigneDotationsSousProgrammeDto } from './ligne-dotations-sous-programme.dto';

export interface DotationRepartitionNationaleDto {

  /** The dotation programme. */
  dotationProgramme: DotationProgrammeDto;

  /** The liste dotations sous programmes. */
  listeLignesDotationsSousProgrammes: LigneDotationsSousProgrammeDto[];

  /** The somme dotations sp annee moins 3. */
  sommeDotationsSpAnneeMoins3: number;

  /** The somme dotations sp annee moins 2. */
  sommeDotationsSpAnneeMoins2: number;

  /** The somme dotations sp annee moins 1. */
  sommeDotationsSpAnneeMoins1: number;

  /** The somme dotations sp annee N. */
  sommeDotationsSpAnneeN: number;

  /** The somme dotations sp initial annee N. */
  sommeDotationsSpInitialAnneeN: number;
}

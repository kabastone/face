export interface DotationCollectiviteFongibiliteDto {

  /** The description sous programme. */
  description: String;

  /** The id sous programme. */
  idSousProgramme: number;

  /** The dotation disponible. */
  dotationDisponible: number;

  /** The id collectivite. */
  idCollectivite: number;
 }

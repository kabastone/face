export interface DotationTransfertFongibiliteDto {

  /** The description sous programme. */
  idSousProgrammeDebiter: number;

  /** The id sous programme. */
  idSousProgrammeCrediter: number;

  /** The dotation disponible. */
  montantTransfert: number;

  /** The id collectivite. */
  idCollectivite: number;
 }

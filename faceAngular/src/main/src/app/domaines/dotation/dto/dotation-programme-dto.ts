export interface DotationProgrammeDto {

     /** The id dotation programme. */
     idDotationProgramme: number;

     /** The id programme. */
     idProgramme: number;

     /** The annee. */
     annee: number;

     /** The code numerique. */
     codeNumerique: string;

     /** The montant. */
     montant: number;

     /** The version. */
     version: number;
}

import { SousProgrammeDto } from './sous-programme-dto';

export class DotationDetails {
  idLigneDotationDepartementale: number;
  idDepartement: number;
  numOrdre: number;
  montant: number;
  typeDotationDepartement: string;
  dateEnvoi: string;
  sousProgramme: SousProgrammeDto;
  dateEnvoiFormate: Date;

  constructor(idDepartement: number, sousProgramme: SousProgrammeDto, montant: number) {
    this.idDepartement = idDepartement;
    this.sousProgramme = sousProgramme;
    this.montant = montant;
  }
}

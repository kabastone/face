import { DotationProgramme, DotationProgrammeAnnuel } from './dotation-programme';

//DOTATION PROGRAMME
export const DOTATIONSPROGPRINC: DotationProgramme[] = [
  { id: '3', code:'AP', libelle:'Renforcement des réseaux', annee2015:184.6, annee2016:184.6, annee2017:184.6, annee2018:184.6},
  { id: '4', code:'AE', libelle:'Extension des réseaux', annee2015:46, annee2016:46, annee2017:46, annee2018:46},
  { id: '5', code:'CE', libelle:'Enfouissement et pose en façade', annee2015:55, annee2016:55, annee2017:55, annee2018:55},
  { id: '6', code:'SS', libelle:'Sécurisation des fils nus', annee2015:39, annee2016:39, annee2017:39, annee2018:39},
  { id: '7', code:'SF', libelle:'Sécurisation des fils nus de faible section', annee2015:42, annee2016:42, annee2017:42, annee2018:42},
  { id: '8', code:'', libelle:'Fonctionnement', annee2015:1, annee2016:1, annee2017:1, annee2018:1},
  { id: '9', code:'AD', libelle:'Déclaration d\'utilité publique (DUP)', annee2015:5, annee2016:5, annee2017:5, annee2018:5},
  { id: '10', code:'AI', libelle:'Intempéries', annee2015:5, annee2016:5, annee2017:5, annee2018:5},
];
export const DOTATIONSPROGSPEC: DotationProgramme[] = [
    { id: '2', code:'AR', libelle:'Sites isolés', annee2015:2, annee2016:2, annee2017:2, annee2018:2.4},
    { id: '3', code:'AR', libelle:'Installations de proximité en zone non interconnectée', annee2015:4.4, annee2016:4.4, annee2017:4.4, annee2018:4},
    { id: '4', code:'AM', libelle:'Maîtrise de la demande d\'énergie', annee2015:1, annee2016:1, annee2017:1, annee2018:1},
  ];

  //DOTATION ANNUELLE
  export const DOTATIONSPROGPRINCANNUELLE: DotationProgrammeAnnuel[] = [
    { id: '03', code:'AP', libelle:'Renforcement des réseaux', annee2015:184.6, annee2016:184.6, annee2017:184.6, annee2018:184.6, annee2019:185},
    { id: '04', code:'AE', libelle:'Extension des réseaux', annee2015:46, annee2016:46, annee2017:46, annee2018:46, annee2019:55},
    { id: '05', code:'CE', libelle:'Enfouissement et pose en façade', annee2015:55, annee2016:55, annee2017:55, annee2018:55, annee2019:60},
    { id: '06', code:'SS', libelle:'Sécurisation des fils nus', annee2015:39, annee2016:39, annee2017:39, annee2018:39, annee2019:15},
    { id: '07', code:'SF', libelle:'Sécurisation des fils nus de faible section', annee2015:42, annee2016:42, annee2017:42, annee2018:42, annee2019:25},
    { id: '09', code:'AD', libelle:'Déclaration d\'utilité publique (DUP)', annee2015:5, annee2016:5, annee2017:5, annee2018:5, annee2019:35},
    { id: '10', code:'AI', libelle:'Intempéries', annee2015:5, annee2016:5, annee2017:5, annee2018:5, annee2019:1.6},
    ];
  export const DOTATIONSPROGSPECANNUELLE: DotationProgrammeAnnuel[] = [
    { id: '02', code:'AR', libelle:'Sites isolés', annee2015:2, annee2016:2, annee2017:2, annee2018:2.4, annee2019:2.4},
    { id: '03', code:'AR', libelle:'Installations de proximité en zone non interconnectée', annee2015:4.4, annee2016:4.4, annee2017:4.4, annee2018:4, annee2019:2},
    { id: '04', code:'AM', libelle:'Maîtrise de la demande d\'énergie', annee2015:1, annee2016:1, annee2017:1, annee2018:1, annee2019:3.2},
  
  ];

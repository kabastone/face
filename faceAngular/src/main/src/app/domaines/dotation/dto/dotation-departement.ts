import { TypeDotation } from './type-dotation';

export class DotationDepartement {
  idDepartement:number;
  codeDepartement:string;
  libelleDepartement:string;
  enPreparation:boolean;
  notifiee:boolean;
  somme: number;
  nonRegroupement:number;
  stock:number;
  listeDotationsDepartementalesParSousProgramme: TypeDotation[];
  sommeMontantsDotation = 0;
}

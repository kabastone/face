import { TypeDotation } from './type-dotation';

export class DotationCollectivite {
    collectivite:string;
    type: TypeDotation[];
    somme: number;
  }
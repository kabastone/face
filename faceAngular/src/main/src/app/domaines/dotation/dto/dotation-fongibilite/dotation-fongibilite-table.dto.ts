export interface DotationFongibiliteTableDto {

  /** The id sous programme débité. */
  sousProgrammeDebiter: String;

  /** The id sous programme crédité. */
  sousProgrammeCrediter: String;

  /** The montant transfert. */
  montant: number;

  /** The departement. */
  departement: String;

  /** The collectivite. */
  collectivite: String;

  /** The date transfert. */
  dateTransfert: Date;
 }

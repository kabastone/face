export interface DotationRenoncementDto {

  /** The id sous programme. */
  idSousProgramme: number;

  /** The num dossier. */
  dotationDisponible: number;

  /** The num dossier. */
  montant: number;

  /** The id collectivite. */
  idCollectivite: number;
 }

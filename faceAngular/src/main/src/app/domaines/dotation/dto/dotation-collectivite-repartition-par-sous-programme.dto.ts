export interface DotationCollectiviteRepartitionParSousProgrammeDto {

  /** The id. */
  id: number;

  /** The id dotation departement. */
  idDotationDepartement: number;

  /** The montant. */
  montant: number;

 }

export interface DotationSousProgrammeMontantBo {
  /** The id. */
  idDotationSousProgramme: number;

  /** The nom court. */
  montant: number;

  /** The code departement. */
  concerneInitial: boolean;
}

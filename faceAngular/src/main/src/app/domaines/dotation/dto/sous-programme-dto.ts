export class SousProgrammeDto {
  code: string;
  name: string;
  codeDomaineFonctionnel: string;
  deProjet: boolean;

  constructor(code: string, name: string, codeDomaineFonctionnel: string) {
    this.code = code;
    this.name = name;
    this.codeDomaineFonctionnel = codeDomaineFonctionnel;
  }
}

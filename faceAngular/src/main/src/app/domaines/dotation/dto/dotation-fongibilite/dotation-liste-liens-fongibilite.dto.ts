import { DotationCollectiviteFongibiliteDto } from './dotation-collectivite-fongibilite-dto';
import { DotationLienFongibiliteDto } from './dotation-lien-fongibilite.dto';

export interface DotationListeLiensFongibiliteDto {

  lienFongibilite: DotationLienFongibiliteDto[];

  dotationCollectiviteFongibiliteDto: DotationCollectiviteFongibiliteDto[];

}

export interface DotationDepartementDto {

  /** The id. */
  id: number;

  /** The abreviation sous programme. */
  abreviationSousProgramme: string;

  /** The description sous programme. */
  descriptionSousProgramme: string;

  /** The description sous programme. */
  numeroSousProgramme: string;

  /** The montant notifie. */
  montantNotifie: number;
}

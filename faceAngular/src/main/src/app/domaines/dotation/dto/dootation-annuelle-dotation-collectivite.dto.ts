export interface DotationAnnuelleDotationCollectiviteDto {

  /** The id dotation collectivite. */
  idDotationCollectivite: number;

  /** The num sous action. */
  numSousAction: String;

  /** The abreviation. */
  abreviation: String;

  /** The description. */
  description: String;

  /** The number. */
  montant: number;
}

export interface DotationLienFongibiliteDto {

  /** The id sous programme. */
  idDonneur: number;

  /** The num dossier. */
  idReceveur: number;
 }

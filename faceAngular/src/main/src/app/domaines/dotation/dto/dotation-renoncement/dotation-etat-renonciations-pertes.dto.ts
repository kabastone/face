export interface DotationEtatRenonciationsPertesDto {

  /** The date. */
  date: Date;

  /** The type. */
  type: String;

  /** The departement code. */
  departementCode: string;

  /** The departement nom. */
  departementNom: String;

  /** The collectivite. */
  collectivite: String;

  /** The sous programme. */
  sousProgramme: String;

  /** The montant. */
  montant: number;
 }

export class DotationProgramme {
    id:string;
    code:string;
    libelle:string;
    annee2015:number;
    annee2016:number;
    annee2017:number;
    annee2018:number;
    
  }
  export class DotationProgrammeAnnuel {
    id:string;
    code:string;
    libelle:string;
    annee2015:number;
    annee2016:number;
    annee2017:number;
    annee2018:number;
    annee2019:number;
    
  }
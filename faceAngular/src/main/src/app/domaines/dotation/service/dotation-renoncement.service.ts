import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { DotationEtatRenonciationsPertesDto } from '@dotation/dto/dotation-renoncement/dotation-etat-renonciations-pertes.dto';
import { DotationRenoncementDto } from '@dotation/dto/dotation-renoncement/dotation-renoncement-dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DotationRenoncementService {

  constructor(private httpService: HttpService) { }

  /**
   * Obtention de la
   *
   * @param codeNumerique dotation programme à lire
   * @returns la dotation
   */
  rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme$(
    idCollectivite: number, idSousProgramme: number): Observable<DotationRenoncementDto> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/renoncement/collectivite/${idCollectivite}/sous-programme/${idSousProgramme}/rechercher`);
  }

  /**
   * Renoncement d'une dotation (mise à jour dans dotation_departement_ligne et ajout dans dotation_collectivite)
   * Upload document
   *
   * @param dotationRenoncementDto - données du formulaire
   * @returns le dto
   */
  renoncerDotation$(data: FormData): Observable<DotationRenoncementDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/dotation/renoncement/renoncer`, data);
  }

  rechercherRenonciationsEtPertesParIdCollectivite$(
    idCollectivite: number): Observable<DotationEtatRenonciationsPertesDto[]> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/renoncement/collectivite/${idCollectivite}/rechercher`);
  }

  rechercherRenonciationsEtPertesParProgramme$(codeProgramme: string):
    Observable<DotationEtatRenonciationsPertesDto[]> {
    return this.httpService.envoyerRequeteGet(`/dotation/renoncement/code/${codeProgramme}/rechercher`);
  }
}

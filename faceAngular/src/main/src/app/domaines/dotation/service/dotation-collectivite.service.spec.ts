import { TestBed } from '@angular/core/testing';

import { DotationCollectiviteService } from './dotation-collectivite.service';

describe('DotationCollectiviteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DotationCollectiviteService = TestBed.get(DotationCollectiviteService);
    expect(service).toBeTruthy();
  });
});

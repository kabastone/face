import { TestBed } from '@angular/core/testing';

import { DotationFongibiliteService } from './dotation-fongibilite.service';

describe('DotationFongibiliteService', () => {
  let service: DotationFongibiliteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotationFongibiliteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

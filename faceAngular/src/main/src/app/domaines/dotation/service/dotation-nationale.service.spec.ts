import { TestBed } from '@angular/core/testing';

import { DotationNationaleService } from './dotation-nationale.service';

describe('DotationProgrammeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DotationNationaleService = TestBed.get(DotationNationaleService);
    expect(service).toBeTruthy();
  });
});

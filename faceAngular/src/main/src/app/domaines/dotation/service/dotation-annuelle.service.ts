import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DotationAnnuelleDto } from '@dotation/dto/dotation-annuelle.dto';

@Injectable({
  providedIn: 'root'
})
export class DotationAnnuelleService {

  constructor(private httpService: HttpService) { }

  /**
   * Rechercher dotation annuelle
   *
   * @param dotationAnnuelleDto - données du formulaire
   * @returns le dto
   */
  rechercherDotationAnnuelle$(dotationAnnuelleDto: DotationAnnuelleDto): Observable<DotationAnnuelleDto> {
    return this.httpService.envoyerRequetePost(`/dotation/annuelle/rechercher`, dotationAnnuelleDto);
  }

  /**
   * Téléchargement d'un document.
   * @param typeDocument - type du document a télécharger
   */
  telechargerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/document/${idDocument}/telecharger`);
  }
}

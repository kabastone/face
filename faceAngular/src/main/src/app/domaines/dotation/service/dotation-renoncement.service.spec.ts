import { TestBed } from '@angular/core/testing';

import { DotationRenoncementService } from './dotation-renoncement.service';

describe('DotationProgrammeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DotationRenoncementService = TestBed.get(DotationRenoncementService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DotationCollectiviteRepartitionDto } from '@dotation/dto/dotation-collectivite-repartition.dto';
import { CollectiviteRepartitionDto } from '@dotation/dto/collectivite-repartition.dto';

@Injectable({
  providedIn: 'root'
})
export class DotationCollectiviteService {

  constructor(private httpService: HttpService) { }

  /**
   * Rechercher dotation collectivite repartition
   *
   * @param dotationAnnuelleDto - données du formulaire
   * @returns le dto
   */
  rechercherDotationCollectiviteRepartition$(codeDepartement: string): Observable<DotationCollectiviteRepartitionDto> {
    return this.httpService.envoyerRequetePost(`/dotation/collectivite/repartition/rechercher`, codeDepartement);
  }

  /**
   * Téléchargement d'un document.
   * @param idDocument - id du document à télécharger
   */
  telechargerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/document/${idDocument}/telecharger`);
  }

  /* Suppression d'un document.
   * @param idDocument - id du document à supprimer
   */
  supprimerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteDelete(`/document/${idDocument}/supprimer`);
  }

  /**
   * Upload document
   *
   * @param DotationCollectiviteRepartitionDto - données du formulaire
   * @returns le dto
   */
  televerserDocumentCourrierRepartition$(data: FormData): Observable<DotationCollectiviteRepartitionDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/dotation/collectivite/repartition/document/televerser`, data);
  }

  /**
   * Répartir dotation collectivite
   *
   * @param collectiviteRepartitionDto - données du formulaire
   * @returns le dto
   */
  repartirDotationCollectivite$(collectiviteRepartitionDto: CollectiviteRepartitionDto): Observable<DotationCollectiviteRepartitionDto> {
    return this.httpService.envoyerRequetePost(`/dotation/collectivite/repartition/repartir`, collectiviteRepartitionDto);
  }
}

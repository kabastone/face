import { TestBed } from '@angular/core/testing';

import { DotationAnnuelleService } from './dotation-annuelle.service';

describe('DotationAnnuelleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DotationAnnuelleService = TestBed.get(DotationAnnuelleService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { DotationCollectiviteFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-collectivite-fongibilite-dto';
import { TransfertFongibiliteLovDto } from '@dotation/dto/dotation-fongibilite/transfert-fongibilite-lov.dto';
import { DotationListeLiensFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-liste-liens-fongibilite.dto';
import { DotationTransfertFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-transfert-fongibilite-dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DotationFongibiliteService {

  constructor(private httpService: HttpService) { }

  rechercherFongibiliteDotationParIdCollectivite$(idCollectivite: number): Observable<DotationListeLiensFongibiliteDto> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/fongibilite/collectivite/${idCollectivite}/rechercher`);
  }

  rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme$(
    idCollectivite: number, idSousProgramme: number): Observable<DotationCollectiviteFongibiliteDto> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/fongibilite/collectivite/${idCollectivite}/sous-programme/${idSousProgramme}/rechercher`);
  }

  transfererDotation$(data: FormData): Observable<DotationTransfertFongibiliteDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/dotation/fongibiliter/transferer`, data);
  }

  rechercherTransfertAode$(idCollectivite: number, annee: number): Observable<TransfertFongibiliteLovDto[]> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/transfert/collectivite/${idCollectivite}/annee/${annee}/rechercher`, true, [] as TransfertFongibiliteLovDto[]);
  }

  rechercherTransfertMfer$(annee: number): Observable<TransfertFongibiliteLovDto[]> {
    return this.httpService.envoyerRequeteGet(
      `/dotation/transfert/annee/${annee}/rechercher`, true, [] as TransfertFongibiliteLovDto[]);
  }
}

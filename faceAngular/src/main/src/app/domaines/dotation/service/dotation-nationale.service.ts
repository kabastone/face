import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DotationRepartitionNationaleDto } from '@dotation/dto/dotation-repartition-nationale.dto';
import { DotationProgrammeDto } from '@dotation/dto/dotation-programme-dto';
import { LigneDotationsSousProgrammeDto } from '@dotation/dto/ligne-dotations-sous-programme.dto';
import { DotationSousProgrammeMontantBo } from '@dotation/dto/dotation-sous-programme-montant.dto';

@Injectable({
  providedIn: 'root'
})
export class DotationNationaleService {

  constructor(private httpService: HttpService) { }

   /**
   * Rechercher les données de dotation repartition nationale
   *
   * @param codeProgramme code du programme
   * @returns la dotation repartition nationale DTO
   */
  rechercherDotationRepartitionNationaleDto$(codeProgramme: string): Observable<DotationRepartitionNationaleDto> {
    return this.httpService.envoyerRequeteGet(`/dotation/repartition-nationale/code/${codeProgramme}/rechercher`);
  }

  /**
   * Modifier le montant de la dotation de programme.
   *
   * @param dotationProgrammeDto dotation de programme
   * @returns la dotation de programme modifiée
   */
  modifierMontantDotationProgrammeRepartitionNationale$(dotationProgrammeDto: DotationProgrammeDto): Observable<DotationProgrammeDto> {
    return this.httpService.envoyerRequetePost(`/dotation/repartition-nationale/dotation-programme/montant/modifier`, dotationProgrammeDto);
  }

   /**
   * Modifier le montant de la dotation de sous-programme.
   *
   * @param ligneDotationsSousProgrammeDto la ligne de dotations sous-programme
   * @returns dotation repartition nationale DTO mise à jour
   */
  modifierMontantDotationSousProgrammeRepartitionNationale$(dotationSousProgrammeMontantBo: DotationSousProgrammeMontantBo):
    Observable<DotationRepartitionNationaleDto> {
    return this.httpService.envoyerRequetePost(`/dotation/repartition-nationale/dotation-sous-programme/montant/modifier`,
    dotationSousProgrammeMontantBo);
  }

}

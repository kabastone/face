import { RouterModule, Routes } from '@angular/router';

// Interne
import { DotationDetailComponent } from './pages/dotation-detail/dotation-detail.component';
import { DotationCollectiviteComponent } from './pages/dotation-collectivite/dotation-collectivite.component';
import { DotationRenoncementComponent } from './pages/dotation-renoncement/dotation-renoncement.component';
import { DotationAnnuelleComponent } from './pages/dotation-annuelle/dotation-annuelle.component';
import { DotationDepartementComponent } from './pages/dotation-departement/dotation-departement.component';
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { DotationRepartitionNationaleParentComponent } from './pages/dotation-repartition-nationale/dotation-repartition-nationale-parent/dotation-repartition-nationale-parent.component';
import { DotationEffectuerTransfertComponent } from './pages/dotation-effectuer-transfert/dotation-effectuer-transfert.component';
import { DotationEtatTransfertComponent } from './pages/dotation-etat-transfert/dotation-etat-transfert.component';
import { DotationRenonciationPerteParentComponent } from './pages/dotation-renonciation-perte/dotation-renonciation-perte-parent/dotation-renonciation-perte-parent.component';
import { NgModule } from '@angular/core';


export const DOTATION_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'departement',
    pathMatch: 'full',
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'departement',
    component: DotationDepartementComponent ,
    data: { title: 'Répartition par département en k€' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: ':idDepartement/details',
    component: DotationDetailComponent,
    data: { title: 'Détail de la dotation départementale' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'repartition-nationale',
    component: DotationRepartitionNationaleParentComponent,
    data: { title: 'Répartition nationale' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'collectivite',
    component: DotationCollectiviteComponent,
    data: { title: 'Répartition par collectivité' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'renoncement',
    component: DotationRenoncementComponent,
    data: { title: 'Renoncer à une partie d\'une dotation'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'renoncement/:idSousProgramme',
    component: DotationRenoncementComponent,
    data: { title: 'Renoncer à une partie d\'une dotation'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'annuelle',
    component: DotationAnnuelleComponent,
    data: { title: 'Dotation Annuelle'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'effectuer-transfert',
    component: DotationEffectuerTransfertComponent,
    data: { title: 'Effectuer un transfert de dotation'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'etat-transfert',
    component: DotationEtatTransfertComponent,
    data: { title: 'Etat de mes transferts de dotation'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'etat-transferts',
    component: DotationEtatTransfertComponent,
    data: { title: 'Etat des transferts de dotation'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'etat-renonciations-pertes',
    component: DotationRenonciationPerteParentComponent,
    data: { title: 'Renonciations et pertes'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'etat-renonciation-perte',
    component: DotationRenonciationPerteParentComponent,
    data: { title: 'Etat de mes renonciations et pertes'},
    resolve: { componentMsg: ComponentResolverMsg }
  },
];

const routes : Routes = [
  {
    path: '',
    children: DOTATION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  }
];

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class DotationRouteModule {}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRepartitionNationaleProgrammeComponent } from './dotation-repartition-nationale-programme.component';

describe('DotationRepartitionNationaleProgrammeComponent', () => {
  let component: DotationRepartitionNationaleProgrammeComponent;
  let fixture: ComponentFixture<DotationRepartitionNationaleProgrammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationRepartitionNationaleProgrammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRepartitionNationaleProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

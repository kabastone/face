import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRenoncementComponent } from './dotation-renoncement.component';

describe('DotationRenoncementComponent', () => {
  let component: DotationRenoncementComponent;
  let fixture: ComponentFixture<DotationRenoncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationRenoncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRenoncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { DotationDetails } from '@dotation/dto/dotation-details';
import { DetailSPService } from '../service/detail-sp.service';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dot-detail-envoyees',
  templateUrl: './detail-envoyees.component.html',
  styleUrls: ['./detail-envoyees.component.css']
})
export class DetailEnvoyeesComponent implements OnInit {

  dotations: DotationDetails[];

  @Input() idDepartement: number;

  rowGroupMetadata: any;

  constructor(
    private detailSPService: DetailSPService,
    private fileSaverService: FileSaverService
  ) { }

  ngOnInit() {

    this.loadDotationsNotifiees();
    this.updateRowGroupMetaData();
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  conversionDate(date: string): Date {
    return new Date(date.split('/', 3).reverse().join('-'));
  }

  loadDotationsNotifiees() {
    this.detailSPService.getDotationsNotifiees(this.idDepartement).pipe(
      map((dotations: DotationDetails[]) => {
        if(dotations){
          dotations.forEach((data, index, array) => {
            data.dateEnvoiFormate = this.conversionDate(data.dateEnvoi);
            array[index] = data;
          });
        }
        return dotations;
      }
      ))
    .subscribe((dotations: DotationDetails[]) => {this.dotations = dotations; this.updateRowGroupMetaData();} );
  }

  telechargerDocument(rowData: any): void {
    console.log(rowData);
    this.detailSPService.getDocumentNotification(rowData.idLigneDotationDepartementale).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.dotations) {
      for (let i = 0; i < this.dotations.length; i++) {
        const rowData = this.dotations[i];
        const dtEnvoi = rowData.dateEnvoi;
        const indexSomme = this.getIndexSomme(rowData);
        const mtn = rowData.montant;
        if (i === 0) {
          this.rowGroupMetadata[dtEnvoi] = { index: 0, size: 1 };
          this.rowGroupMetadata[indexSomme] = { index: 0, size: 1, somme: mtn };
        } else {
          const previousRowData = this.dotations[i - 1];
          // Pour la colonne 'Date de notification'
          const previousRowGroup = previousRowData.dateEnvoi;
          if (dtEnvoi === previousRowGroup) {
            this.rowGroupMetadata[dtEnvoi].size++;
          } else {
            this.rowGroupMetadata[dtEnvoi] = { index: i, size: 1 };
          }
          // Pour la colonne 'Somme'
          const previousRowGroupSomme = this.getIndexSomme(previousRowData);
          if (indexSomme === previousRowGroupSomme) {
            this.rowGroupMetadata[indexSomme].size++;
            this.rowGroupMetadata[indexSomme].somme += mtn;
          } else {
            this.rowGroupMetadata[indexSomme] = { index: i, size: 1, somme: mtn };
          }
        }
      }
    }
  }

  getIndexSomme(dotationDetails: DotationDetails): string {
    return dotationDetails.dateEnvoi + dotationDetails.typeDotationDepartement;
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Subject, Observable, of } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { DotationCollectiviteRepartitionDto } from '@dotation/dto/dotation-collectivite-repartition.dto';
import { DotationCollectiviteService } from '@dotation/service/dotation-collectivite.service';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { DotationCollectiviteRepartitionParSousProgrammeDto } from '@dotation/dto/dotation-collectivite-repartition-par-sous-programme.dto';
import { CollectiviteRepartitionDto } from '@dotation/dto/collectivite-repartition.dto';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-dotation-collectivite',
  templateUrl: './dotation-collectivite.component.html',
  styleUrls: ['./dotation-collectivite.component.css']
})
export class DotationCollectiviteComponent implements OnInit, OnDestroy {

  // Label pour le bouton de téléversement.
  public labelTeleverser = 'Téléverser un courrier de répartition au format pdf';

  // Formulaire de la page
  public dotationCollectiviteForm: FormGroup;

  // formulaire dédié à l'upload de fichiers
  private formulaireFichier: FormData = new FormData();

  // DTO reflet des données du formulaire
  public dotationCollectiviteRepartitionDto: DotationCollectiviteRepartitionDto;

  // id du document à supprimer
  public idDocumentSupprimer: number;

  // affichage (ou non) de la modal : confirmation de suppression d'un document
  public affichageModalConfirmerSupprimerDocument: Boolean = false;

  hide = true;

  public departements: DepartementLovDto[];

  public isUserUniquementGenerique: boolean;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  departement: string;
  filteredDepartements: any[];

  clonedCollectiviteRepartitionDto: { [s: string]: CollectiviteRepartitionDto; } = {};

  constructor(private fb: FormBuilder,
    private referentielService: ReferentielService,
    private dotationCollectiviteService: DotationCollectiviteService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private droitService: DroitService,
    private formBuilder: FormBuilder,
    private fileSaverService: FileSaverService
  ) { }

  /**
   * A la destruction : on lance le désabonnement de tous les subscribes
   */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  // Affiche les bons champs du formulaire en fonction du type sélectionné
  afficherBlock() {
    if (this.hide = true) {
      this.hide = false;
    }
  }

  ngOnInit() {

    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur =>
        this.initialiserIsUserUniquementGenerique(infoUtilisateur)
      );
    // récupération des départements
    if (!this.isUserUniquementGenerique) {
      this.referentielService
        .rechercherTousDepartements$()
        .subscribe(data => (this.departements = data));
    }

    this.initDotationCollectiviteForm();

  }

  /**
  * Initialise le flag de profil de l'utilisateur.
  */
  private initialiserIsUserUniquementGenerique(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
      infosUtilisateur
    );
  }

  private initDotationCollectiviteForm(): void {
    // init. du formulaire
    this.dotationCollectiviteForm = this.formBuilder.group({
      selectionDepartement: new FormControl()
    });
  }

  /**
   * Met à jour l'objet DTO à partir du formulaire.
   *
   */
  public majDtoFromFormulaire(): void {

    // Alimentation du DTO avec les propriétés du form portant le meme nom
    this.dotationCollectiviteRepartitionDto = this.dotationCollectiviteForm.value as DotationCollectiviteRepartitionDto;

    const selectionDepartement: DepartementLovDto = this.dotationCollectiviteForm.get('selectionDepartement').value as DepartementLovDto;
    const codeDepartement: string = selectionDepartement.code;

    // Récupération de la dotation annuelle
    this.dotationCollectiviteService
      .rechercherDotationCollectiviteRepartition$(codeDepartement)
      .subscribe(data => {
        this.dotationCollectiviteRepartitionDto = data;
        this.afficherBlock();
      });
  }

  /**
   * Telecharger un document.
   */
  public telechargerDocument(idDocument: number) {


    if (!idDocument) {
      // message de succès
      this.messageManagerService.documentIntrouvableMessage();

      return;
    }

    // téléchargement par id
    this.dotationCollectiviteService.telechargerDocument(idDocument).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });
  }

  /**
   * Televerser un document.
   */
  public televerserDocument(typeDocument: string, event: any) {

    const fichier: File = event.target.files[0];

    // ajout du fichier
    if (event.target.files && fichier) {
      this.formulaireFichier.append(typeDocument, fichier);
    }

    const formulaireAvecFichier = this.initFormData();

    // téléversement
    this.dotationCollectiviteService
      .televerserDocumentCourrierRepartition$(formulaireAvecFichier)
      .subscribe(data => {

        this.dotationCollectiviteRepartitionDto = data;

        // suppression du fichier à uploader
        this.formulaireFichier = new FormData();

        // message de succès
        this.messageManagerService.modificationEnregistree();
      });
  }

  /**
   * Supprimer un document.
   */
  public supprimerDocument() {

    // fermeture de la fenêtre modale de confirmation de suppression
    this.affichageModalConfirmerSupprimerDocument = false;

    // suppression par id
    this.dotationCollectiviteService.supprimerDocument(this.idDocumentSupprimer).subscribe(response => {
      const documentSuppr: DocumentDto = this.dotationCollectiviteRepartitionDto.documents.find(d => d.id === this.idDocumentSupprimer);
      const index = this.dotationCollectiviteRepartitionDto.documents.indexOf(documentSuppr);
      if (index > -1) {
        this.dotationCollectiviteRepartitionDto.documents.splice(index, 1);
      }
    });
  }

  /**
   * Affiche la modal de confirmation de suppression d'un document.
   *
   */
  confirmerSupprimerDocument(idDocument: number): void {
    this.idDocumentSupprimer = idDocument;
    this.affichageModalConfirmerSupprimerDocument = true;
  }

  /**
   * Ferme/cache la modal de confirmation de suppression d'un document.
   *
   */
  fermerConfirmerSupprimerDocument(): void {
    this.idDocumentSupprimer = undefined;
    this.affichageModalConfirmerSupprimerDocument = false;
  }

  /**
   * Afficher ou non le bouton supprimer document
   * codeTypeDocument
   */
  public isSupprimerDocumentVisible(codeTypeDocument: string): boolean {
    return codeTypeDocument === 'COURRIER_REPARTITION';
  }

  /**
   * Initialisation du formulaire avec fichiers.
   */
  private initFormData() {

    // init.
    const formulaireAvecFichiers = new FormData();

    const selectionDepartement: DepartementLovDto = this.dotationCollectiviteForm.get('selectionDepartement').value as DepartementLovDto;
    const codeDepartement: string = selectionDepartement.code;

    // mapping champs speciaux ou cachés
    formulaireAvecFichiers.append('codeDepartement', codeDepartement);

    // mapping des fichiers uploadés
    for (const fichier of this.formulaireFichier.entries()) {
      formulaireAvecFichiers.append(fichier[0], fichier[1]);
    }

    return formulaireAvecFichiers;
  }

  /**
   * Calcul la somme des montants dotation departement
   */
  public calculSommeDotationsDepartement(): number {
    let somme: number;
    somme = 0;
    if (this.dotationCollectiviteRepartitionDto && this.dotationCollectiviteRepartitionDto.dotationsDepartement) {
      for (let i = 0; i < this.dotationCollectiviteRepartitionDto.dotationsDepartement.length; i++) {
        let value: number = this.dotationCollectiviteRepartitionDto.dotationsDepartement[i].montantNotifie;
        if (isNaN(value)) {
          value = 0;
        }
        somme = somme + value;
      }
    }
    return somme;
  }

  public calculSommeDotationsCollectivite(dotationsCollectivite: DotationCollectiviteRepartitionParSousProgrammeDto[]): number {
    let somme: number;
    somme = 0;
    if (dotationsCollectivite) {
      for (let i = 0; i < dotationsCollectivite.length; i++) {
        let value: number = dotationsCollectivite[i].montant;
        if (isNaN(value)) {
          value = 0;
        }
        somme = somme + value;
      }
    }
    return somme;
  }

 /**
   * On Row Edit init.
   *
   * collectiviteRepartitionDto
   */
  onRowEditInit(collectiviteRepartitionDto: CollectiviteRepartitionDto) {

    this.clonedCollectiviteRepartitionDto[collectiviteRepartitionDto.id]
      = this.clonerCollectiviteRepartitionDto(collectiviteRepartitionDto);
  }

  /**
   * On Row Edit save.
   *
   * collectiviteRepartitionDto
   */
  onRowEditSave(collectiviteRepartitionDto: CollectiviteRepartitionDto, index: number) {

    // Nécessaire de rétablir la valeur avant, au cas où il y a une erreur du serveur
    const cloneCollRep: CollectiviteRepartitionDto = this.clonerCollectiviteRepartitionDto(collectiviteRepartitionDto);
    this.dotationCollectiviteRepartitionDto.collectivitesRepartition[index]
      = this.clonedCollectiviteRepartitionDto[collectiviteRepartitionDto.id];

    this.dotationCollectiviteService
      .repartirDotationCollectivite$(cloneCollRep)
      .subscribe(data => {

        this.dotationCollectiviteRepartitionDto = data;

        // message de succès
        this.messageManagerService.modificationEnregistree();
      });
  }

  /**
   * On Row Edit cancel
   *
   * anomalie
   * index
   */
  onRowEditCancel(collectiviteRepartitionDto: CollectiviteRepartitionDto, index: number) {

    this.dotationCollectiviteRepartitionDto.collectivitesRepartition[index]
      = this.clonedCollectiviteRepartitionDto[collectiviteRepartitionDto.id];

    delete this.clonedCollectiviteRepartitionDto[collectiviteRepartitionDto.id];
    this.messageService.clear();
  }

  /**
   * Cloner CollectiviteRepartitionDto
   * collectiviteRepartitionDto
   */
  private clonerCollectiviteRepartitionDto(collectiviteRepartitionDto: CollectiviteRepartitionDto): CollectiviteRepartitionDto {

    const copyDotationsCollectivite: DotationCollectiviteRepartitionParSousProgrammeDto[] = [];
    for (let i = 0; i < collectiviteRepartitionDto.dotationsCollectivite.length; i++) {
      copyDotationsCollectivite[i] = Object.assign({}, collectiviteRepartitionDto.dotationsCollectivite[i]);
    }

    const copyCollectiviteRepartitionDto: CollectiviteRepartitionDto
      = Object.assign({}, collectiviteRepartitionDto);

    copyCollectiviteRepartitionDto.dotationsCollectivite = copyDotationsCollectivite;

    return copyCollectiviteRepartitionDto;
  }
}

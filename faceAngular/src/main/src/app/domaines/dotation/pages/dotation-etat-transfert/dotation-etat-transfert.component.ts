import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { DroitService } from '@app/services/authentification/droit.service';
import { AnneeDto } from '@dotation/dto/annee.dto';
import { DotationFongibiliteTableDto } from '@dotation/dto/dotation-fongibilite/dotation-fongibilite-table.dto';
import { TransfertFongibiliteLovDto } from '@dotation/dto/dotation-fongibilite/transfert-fongibilite-lov.dto';
import { DotationFongibiliteService } from '@dotation/service/dotation-fongibilite.service';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dotation-etat-transfert-aode',
  templateUrl: './dotation-etat-transfert.component.html',
  styleUrls: ['./dotation-etat-transfert.component.css']
})
export class DotationEtatTransfertComponent implements OnInit, OnDestroy {

  public ngDestroyed$ = new Subject();

  public dotationEtatTransfertAodeForm: FormGroup;

  public transfertFongibiliteLovDto = [] as DotationFongibiliteTableDto[];


  // pour la liste des annees
  public annees: AnneeDto[] = [];

  public annee: number;
  public idCollectivite: number;

  public pageSize: number;
  public cols: any[];

  public nbTotalResultats = 0;

  constructor(
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private dotationFongibiliteService: DotationFongibiliteService,
    private messageService: MessageService,
    private datePipe: DatePipe
  ) { }

  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit(): void {
    // Initialisation du formulaire
    this.initDotationEtatTransfertAodeForm();

    // Initialisation des années >= 2016
    const anneeDebut = 2016;
    const anneeEnCours: number = (new Date()).getFullYear();
    for (let i = anneeDebut; i <= anneeEnCours; i++) {
      this.annees.push({ annee: i });
    }

    // Par défaut sur l'année en cours
    this.dotationEtatTransfertAodeForm.controls['selectionAnnee'].setValue({ annee: anneeEnCours });

    if (this.estMfer()) {
      this.cols = [
        { field: 'dateTransfert', header: 'Date', style: 'text-align: center;'},
        { field: 'departement', header: 'Département', style: 'text-align: center;'},
        { field: 'collectivite', header: 'AODE', style: 'text-align: center;'},
        { field: 'sousProgrammeDebiter', header: 'Sous-Programme débité', style: 'text-align: center;'},
        { field: 'sousProgrammeCrediter', header: 'Sous-Programme crédité', style: 'text-align: center;'},
        { field: 'montant', header: 'Montant du transfert', style: 'text-align: center;'}
      ];
    } else {
      this.cols = [
        { field: 'dateTransfert', header: 'Date', style: 'text-align: center;'},
        { field: 'sousProgrammeDebiter', header: 'Sous-Programme débité', style: 'text-align: center;'},
        { field: 'sousProgrammeCrediter', header: 'Sous-Programme crédité', style: 'text-align: center;'},
        { field: 'montant', header: 'Montant du transfert', style: 'text-align: center;'}
      ];
    }

    // Appel au web service quand l'id collectivité change
    this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.majDtoFromFormulaire();
        this.messageService.clear();
      });

  }

  public getDynamicDownloadName(): String {
    const d = new Date();
    const dateActuelle = new Date(d.getFullYear(), d.getMonth(), d.getDate());
    const dateActuelleString = this.datePipe.transform(dateActuelle, 'yyyy-MM-dd');
    if (this.estMfer()) {
      return 'Etat-des-transferts-de-dotation_' + dateActuelleString;
    } else {
      return 'Etat-de-mes-transferts-de-dotation_' + dateActuelleString;
    }
  }

  private initDotationEtatTransfertAodeForm(): void {
    // init. du formulaire
    this.dotationEtatTransfertAodeForm = this.formBuilder.group({
      selectionAnnee: new FormControl()
    });
  }

  public majDtoFromFormulaire(): void {

    this.transfertFongibiliteLovDto = [] as DotationFongibiliteTableDto[];
    this.nbTotalResultats = 0;

    // Alimentation du DTO avec les propriétés du form portant le meme nom

    this.annee = this.dotationEtatTransfertAodeForm.get('selectionAnnee').value.annee;

    this.idCollectivite = this.droitService.getIdCollectivite$().value;
    console.log(this.annee);

    if (this.estMfer()) {
      this.dotationFongibiliteService
      .rechercherTransfertMfer$(this.annee)
      .subscribe(data => {
        this.mapperList(data);
      });
    } else {
      this.dotationFongibiliteService
      .rechercherTransfertAode$(this.idCollectivite, this.annee)
      .subscribe(data => {
        this.mapperList(data);
      });
    }
  }

  public estMfer() {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMfer(utilisateurConnecte);
  }

  public transfertFongibiliteLovDtoToDotationFongibiliteTableDto(dto: TransfertFongibiliteLovDto): DotationFongibiliteTableDto {
    const tableDto = {} as DotationFongibiliteTableDto;
    this.nbTotalResultats += 1;
    tableDto.dateTransfert = dto.dateTransfert;
    tableDto.departement = dto.departement.codeEtNom;
    tableDto.collectivite = dto.collectivite.nomCourt;
    tableDto.sousProgrammeDebiter = dto.sousProgrammeDebiter.description;
    tableDto.sousProgrammeCrediter = dto.sousProgrammeCrediter.description;
    tableDto.montant = dto.montant;
    return tableDto;
  }

  public mapperList(data: TransfertFongibiliteLovDto[]) {
    data.forEach( lovDto =>
      this.transfertFongibiliteLovDto.push(this.transfertFongibiliteLovDtoToDotationFongibiliteTableDto(lovDto)));

  }

}

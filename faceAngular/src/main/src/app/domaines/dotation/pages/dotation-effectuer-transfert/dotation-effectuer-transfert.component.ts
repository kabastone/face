import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DroitService } from '@app/services/authentification/droit.service';
import { ConfirmationService } from 'primeng/api';
import { DotationCollectiviteFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-collectivite-fongibilite-dto';
import { DotationLienFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-lien-fongibilite.dto';
import { DotationListeLiensFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-liste-liens-fongibilite.dto';
import { DotationFongibiliteService } from '@dotation/service/dotation-fongibilite.service';
import { DotationTransfertFongibiliteDto } from '@dotation/dto/dotation-fongibilite/dotation-transfert-fongibilite-dto';
import { takeUntil, tap } from 'rxjs/operators';
import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-dotation-effectuer-transfert',
  templateUrl: './dotation-effectuer-transfert.component.html',
  styleUrls: ['./dotation-effectuer-transfert.component.css'],
  providers: [ConfirmationService]
})
export class DotationEffectuerTransfertComponent implements OnInit, OnDestroy {

  public ngDestroyed$ = new Subject();

  public idSousProgramme = +this.route.snapshot.paramMap.get('idSousProgramme');

  public dotationEffectuerTransfertForm: FormGroup;

  public ListeLiensFongibilite: DotationListeLiensFongibiliteDto = {} as DotationListeLiensFongibiliteDto;

  public sousProgrammes: DotationCollectiviteFongibiliteDto[];

  public dotationCollectiviteFongibiliteDtoDebiter: DotationCollectiviteFongibiliteDto = {} as DotationCollectiviteFongibiliteDto;

  public dotationCollectiviteFongibiliteDtoCrediter: DotationCollectiviteFongibiliteDto = {} as DotationCollectiviteFongibiliteDto;

  private formulaire: FormData = new FormData();

  hideDebiter = true;
  hideCrediter = true;
  displayDialog: boolean;

  private montantNum: number;

  public ListDonneur: DotationCollectiviteFongibiliteDto[] = [];
  public SetDonneur = new Set(this.ListDonneur);
  public ListReceveur: DotationCollectiviteFongibiliteDto[] = [];
  public SetReceveur = new Set(this.ListReceveur);

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private dotationFongibiliteService: DotationFongibiliteService,
    private confirmationService: ConfirmationService,
    private messageManagerService: MessageManagerService,
    private messageService: MessageService

  ) { }

  ngOnInit(): void {
    this.initDotationEffectuerTransfertForm();
    this.recupererLienSousProgrammes();

    this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.resetFormDebiter();
        this.resetFormCrediter();
        this.formulaire = new FormData();
        this.initDotationEffectuerTransfertForm();
        this.messageService.clear();
        this.recupererLienSousProgrammes();
      });
  }

  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  private initDotationEffectuerTransfertForm(): void {
    // init. du formulaire
    this.dotationEffectuerTransfertForm = this.formBuilder.group({
      selectionSousProgrammeDebiter: new FormControl(),
      selectionSousProgrammeCrediter: new FormControl(),
      montantDebiter: new FormControl(),
      montantTransfert: new FormControl(),
      montantCrediter: new FormControl()
    });
  }

  public recupererLienSousProgrammes(): void {
    if (!this.droitService.getIdCollectivite$().value) {
      return;
    }
    this.dotationFongibiliteService
      .rechercherFongibiliteDotationParIdCollectivite$(this.droitService.getIdCollectivite$().value)
      .subscribe(data => {
        (this.ListeLiensFongibilite = data);
        this.recupererDonneurReceveur(this.ListeLiensFongibilite.dotationCollectiviteFongibiliteDto,
          this.ListeLiensFongibilite.lienFongibilite);
          if (this.idSousProgramme) {
            this.dotationEffectuerTransfertForm.get('selectionSousProgrammeDebiter').
              setValue(data.dotationCollectiviteFongibiliteDto.find(
              ssProg => ssProg.idSousProgramme === this.idSousProgramme));
            this.dotationEffectuerTransfertForm.get('selectionSousProgrammeCrediter').
              setValue(data.dotationCollectiviteFongibiliteDto.find(
              ssProg => ssProg.idSousProgramme === this.idSousProgramme));
            this.majDtoFromFormulaireDebiter();
            this.majDtoFromFormulaireCrediter();
          }
        });
  }

  public majDtoFromFormulaireDebiter(): void {

    this.formulaire = new FormData();

    this.dotationCollectiviteFongibiliteDtoDebiter = this.dotationEffectuerTransfertForm.value as DotationCollectiviteFongibiliteDto;

    if (
      this.dotationEffectuerTransfertForm.get('selectionSousProgrammeDebiter').value !== null &&
      this.dotationEffectuerTransfertForm.get('selectionSousProgrammeDebiter').value !== 'Choisissez un sous programme'
    ) {
      const selectionSousProgramme: DotationCollectiviteFongibiliteDto = this.dotationEffectuerTransfertForm.get(
        'selectionSousProgrammeDebiter'
      ).value as DotationCollectiviteFongibiliteDto;

      this.dotationCollectiviteFongibiliteDtoDebiter.idSousProgramme = selectionSousProgramme.idSousProgramme;

      this.hideDebiter = true;

      this.dotationFongibiliteService
        .rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme$(
          this.droitService.getIdCollectivite$().value,
          this.dotationCollectiviteFongibiliteDtoDebiter.idSousProgramme)
        .subscribe(data => {
          this.dotationCollectiviteFongibiliteDtoDebiter = data;
          this.dotationCollectiviteFongibiliteDtoDebiter.idSousProgramme = selectionSousProgramme.idSousProgramme;
          this.hideDebiter = false;
        });

      this.dotationEffectuerTransfertForm.controls['montantDebiter'].setValue(0);
    } else {
      this.resetFormDebiter();
    }
}

  public majDtoFromFormulaireCrediter(): void {

    this.formulaire = new FormData();

    this.dotationCollectiviteFongibiliteDtoCrediter = this.dotationEffectuerTransfertForm.value as DotationCollectiviteFongibiliteDto;

    if (
      this.dotationEffectuerTransfertForm.get('selectionSousProgrammeCrediter').value !== null &&
      this.dotationEffectuerTransfertForm.get('selectionSousProgrammeCrediter').value !== 'Choisissez un sous programme'
    ) {
      const selectionSousProgramme: DotationCollectiviteFongibiliteDto = this.dotationEffectuerTransfertForm.get(
        'selectionSousProgrammeCrediter'
      ).value as DotationCollectiviteFongibiliteDto;

      this.dotationCollectiviteFongibiliteDtoCrediter.idSousProgramme = selectionSousProgramme.idSousProgramme;

      this.hideCrediter = true;

      this.dotationFongibiliteService
        .rechercherFongibiliteDotationParIdCollectiviteEtIdSousProgramme$(
          this.droitService.getIdCollectivite$().value,
          this.dotationCollectiviteFongibiliteDtoCrediter.idSousProgramme)
        .subscribe(data => {
          this.dotationCollectiviteFongibiliteDtoCrediter = data;
          this.dotationCollectiviteFongibiliteDtoCrediter.idSousProgramme = selectionSousProgramme.idSousProgramme;
          this.hideCrediter = false;
        });

      this.dotationEffectuerTransfertForm.controls['montantCrediter'].setValue(0);
    } else {
      this.resetFormCrediter();
      }
  }

  resetFormDebiter() {
    this.hideDebiter = true;
    this.dotationCollectiviteFongibiliteDtoDebiter = this.dotationEffectuerTransfertForm.value as DotationCollectiviteFongibiliteDto;
    this.dotationCollectiviteFongibiliteDtoDebiter.idSousProgramme = null;
    this.dotationCollectiviteFongibiliteDtoDebiter.dotationDisponible = null;
    this.dotationCollectiviteFongibiliteDtoDebiter.idCollectivite = null;
  }

  resetFormCrediter() {
    this.hideCrediter = true;
    this.dotationCollectiviteFongibiliteDtoCrediter = this.dotationEffectuerTransfertForm.value as DotationCollectiviteFongibiliteDto;

    this.dotationCollectiviteFongibiliteDtoCrediter.idSousProgramme = null;
    this.dotationCollectiviteFongibiliteDtoCrediter.dotationDisponible = null;
    this.dotationCollectiviteFongibiliteDtoCrediter.idCollectivite = null;
    this.dotationEffectuerTransfertForm.get('montantTransfert').setValue(0);
  }

  public getMontantTransfert() {
    if (!this.montantNum
      || this.montantNum === 0) {
      return this.dotationCollectiviteFongibiliteDtoCrediter.dotationDisponible;
    }

    return (+this.montantNum
      + (+this.dotationCollectiviteFongibiliteDtoCrediter.dotationDisponible));
  }

  public isMontantInferieurADotationDisponibleDebiter() {
    if (this.montantNum <= this.dotationCollectiviteFongibiliteDtoDebiter.dotationDisponible) {
      return true;
    } else {
      return false;
    }
  }

  public effectuerTransfert(): void {
    this.displayDialog = false;
    const formulaire = this.initFormData();
    this.dotationFongibiliteService
      .transfererDotation$(formulaire)
      .subscribe((dotationTransfertFongibiliteDto: DotationTransfertFongibiliteDto) => {
        this.resetFormDebiter();
        this.resetFormCrediter();
        this.formulaire = new FormData();
        this.initDotationEffectuerTransfertForm();
        this.messageService.clear();

        this.messageManagerService.modificationEnregistree();
      });
      this.montantNum = undefined;
  }

  initFormData()  {
    const formulaire = new FormData();
    console.log(this.dotationEffectuerTransfertForm.get('selectionSousProgrammeDebiter').value.idSousProgramme);
    formulaire.append('idSousProgrammeDebiter', this.dotationEffectuerTransfertForm.get('selectionSousProgrammeDebiter').value.idSousProgramme);
    formulaire.append('idSousProgrammeCrediter', this.dotationEffectuerTransfertForm.get('selectionSousProgrammeCrediter').value.idSousProgramme);
    formulaire.append('montantTransfert', String(this.montantNum));
    formulaire.append('idCollectivite', String(this.droitService.getIdCollectivite$().value));
    return formulaire;
  }

  public recupererDonneurReceveur(
    sousProgrammes: DotationCollectiviteFongibiliteDto[],
    ListeLiensFongibilite: DotationLienFongibiliteDto[]): void {
      this.ListDonneur = [];
      this.ListReceveur = [];
      this.SetDonneur.clear();
      this.SetReceveur.clear();
    sousProgrammes.forEach(spDonneur => {
      sousProgrammes.forEach( spReceveur => {
        ListeLiensFongibilite.some( lien => {
          if (spDonneur.idSousProgramme === lien.idDonneur && spReceveur.idSousProgramme === lien.idReceveur && spDonneur.dotationDisponible > 0) {
            this.SetDonneur.add(spDonneur);
            this.SetReceveur.add(spReceveur);
          }
        });
      });
    });
    this.ListDonneur = [...this.SetDonneur];
    this.ListReceveur = [...this.SetReceveur];
  }

  ouvrirModal(): void {
    this.displayDialog = true;
  }

  fermerModal(): void {
    this.displayDialog = false;
  }

  formatMontant(eventValue) {
    this.messageService.clear();
    // Si le champs est null ou vide, ne rien faire
    if (eventValue == null || eventValue === '') {
      return;
    }

    // récupère la valeur du champs et "enlève" ce qui n'est pas numerique
    eventValue = eventValue.replace(/[^0-9]/g, '');

    // parse la valeur récupérée en number et la met dans montantNum
    this.montantNum = parseInt(eventValue, 10);
    // renvois la valeur avec les espaces
    this.dotationEffectuerTransfertForm.get('montantTransfert').setValue(this.montantNum.toLocaleString());
  }
}

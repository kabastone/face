import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailEnvoyeesComponent } from './detail-envoyees.component';

describe('DetailEnvoyeesComponent', () => {
  let component: DetailEnvoyeesComponent;
  let fixture: ComponentFixture<DetailEnvoyeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailEnvoyeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailEnvoyeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

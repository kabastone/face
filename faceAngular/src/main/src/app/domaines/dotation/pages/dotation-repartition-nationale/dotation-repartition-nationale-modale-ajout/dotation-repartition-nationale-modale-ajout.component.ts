import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { DotationRepartitionNationaleDto } from '@dotation/dto/dotation-repartition-nationale.dto';
import { DotationSousProgrammeMontantBo } from '@dotation/dto/dotation-sous-programme-montant.dto';
import { LigneDotationsSousProgrammeDto } from '@dotation/dto/ligne-dotations-sous-programme.dto';
import { DotationNationaleService } from '@dotation/service/dotation-nationale.service';

@Component({
  selector: 'app-dotation-repartition-nationale-modale-ajout',
  templateUrl: './dotation-repartition-nationale-modale-ajout.component.html',
  styleUrls: ['./dotation-repartition-nationale-modale-ajout.component.css']
})
export class DotationRepartitionNationaleModaleAjoutComponent implements OnInit, OnChanges {

  public header = '';

  public visible = false;

  @Input()
  public codeProgramme = '';

  @Input()
  public dotationSsProgDto = {} as DotationSousProgrammeMontantBo;

  @Input()
  public dotationNationaleDto = {} as DotationRepartitionNationaleDto;

  public ligneSsProg = {} as LigneDotationsSousProgrammeDto;

  @Output()
  public dotationNationaleDtoEvent = new EventEmitter<DotationRepartitionNationaleDto>();

  constructor(
    private dotationNationaleService: DotationNationaleService
  ) { }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dotationSsProgDto && !changes.dotationSsProgDto.firstChange) {
      this.visible = true;
      this.ligneSsProg = this.dotationNationaleDto.listeLignesDotationsSousProgrammes.find(ligneDot => ligneDot.idDotationSousProgramme === this.dotationSsProgDto.idDotationSousProgramme)
      this.header = 'Ajout de dotation au budget ' + (this.dotationSsProgDto.concerneInitial ? 'initial' : 'effectif') + ' - ' + this.ligneSsProg.descriptionSp;
    }
  }

  ngOnInit(): void {
  }


  calculerNouveauMontant(): number {
    let montant = (
      (this.dotationSsProgDto.concerneInitial ? this.ligneSsProg.montantInitialAnneeN : this.ligneSsProg.montantAnneeN)
      + this.dotationSsProgDto.montant
    );

    return montant;
  }

  accepter(): void {

    this.dotationNationaleService.modifierMontantDotationSousProgrammeRepartitionNationale$(this.dotationSsProgDto).subscribe(
      data => {
        this.dotationNationaleDtoEvent.emit(data);
        this.dotationSsProgDto = {} as DotationSousProgrammeMontantBo;
        this.visible = false;
      }
    )

  }

  refuser(): void {
    this.dotationSsProgDto = {} as DotationSousProgrammeMontantBo;
    this.visible = false;
  }

}

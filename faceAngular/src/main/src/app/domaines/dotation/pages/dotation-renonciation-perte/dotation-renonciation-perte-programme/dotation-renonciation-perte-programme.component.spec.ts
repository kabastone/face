import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRenonciationPerteProgrammeComponent } from './dotation-renonciation-perte-programme.component';

describe('DotationRenonciationPerteProgrammeComponent', () => {
  let component: DotationRenonciationPerteProgrammeComponent;
  let fixture: ComponentFixture<DotationRenonciationPerteProgrammeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DotationRenonciationPerteProgrammeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRenonciationPerteProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

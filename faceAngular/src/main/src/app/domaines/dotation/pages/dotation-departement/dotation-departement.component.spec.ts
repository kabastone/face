import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationDepartementComponent } from './dotation-departement.component';

describe('DotationDepartementComponent', () => {
  let component: DotationDepartementComponent;
  let fixture: ComponentFixture<DotationDepartementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationDepartementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationDepartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '@app/config/app-config.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { DotationEtatRenonciationsPertesDto } from '@dotation/dto/dotation-renoncement/dotation-etat-renonciations-pertes.dto';
import { DotationRenoncementService } from '@dotation/service/dotation-renoncement.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dotation-renonciation-perte-programme',
  templateUrl: './dotation-renonciation-perte-programme.component.html',
  styleUrls: ['./dotation-renonciation-perte-programme.component.css']
})
export class DotationRenonciationPerteProgrammeComponent implements OnInit, OnDestroy {

  @Input()
  public codeProgramme = '';

  public dotationEtatRenonciationsPertesDto: DotationEtatRenonciationsPertesDto[];

  public nbTotalResultats: number;

  public pageSize: number;

  public cols: any[];

  public row: number;

  public rowGroupMetadata: any;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(
    private droitService: DroitService,
    private dotationRenoncementService: DotationRenoncementService,
    private datePipe: DatePipe
    ) { }

  ngOnDestroy(): void {
    this.ngDestroyed$.next();
  }

  ngOnInit(): void {

    if (this.estMfer()) {
      this.cols = [
        { field: 'date', header: 'Date', style: 'text-align: center;'},
        { field: 'type', header: 'Type', style: 'text-align: center;'},
        { field: 'departementCode', header: 'Département', style: 'text-align: center;'},
        { field: 'collectivite', header: 'AODE', style: 'text-align: center;'},
        { field: 'sousProgramme', header: 'Sous-Programme', style: 'text-align: center;'},
        { field: 'montant', header: 'Montant du transfert', style: 'text-align: center;'}
      ];
      this.dotationRenoncementService.rechercherRenonciationsEtPertesParProgramme$(this.codeProgramme)
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe( data => {
          this.preparerData(data);
        });
    } else {
      this.cols = [
        { field: 'date', header: 'Date', style: 'text-align: center;'},
        { field: 'type', header: 'Type', style: 'text-align: center;'},
        { field: 'sousProgramme', header: 'Sous-Programme', style: 'text-align: center;'},
        { field: 'montant', header: 'Montant du transfert', style: 'text-align: center;'}
      ];
      this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(idCollectivite => {
        this.dotationRenoncementService.rechercherRenonciationsEtPertesParIdCollectivite$(idCollectivite)
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe( data => {
          this.preparerData(data);
        });
      });
    }
  }

  public preparerData(data) {

    this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;

    this.nbTotalResultats = data.length;

    this.dotationEtatRenonciationsPertesDto = data;

    this.updateRowGroupMetaData();

  }

  public estMfer() {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMfer(utilisateurConnecte);
  }

  public getDynamicDownloadName(): String {
    const d = new Date();
    const dateActuelle = new Date(d.getFullYear(), d.getMonth(), d.getDate());
    const dateActuelleString = this.datePipe.transform(dateActuelle, 'yyyy-MM-dd');
    if (this.estMfer()) {
      return 'Renonciations-et-pertes_' + dateActuelleString;
    } else {
      return 'Etat-de-mes-renonciations-et-pertes_' + dateActuelleString;
    }
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};

    if (this.dotationEtatRenonciationsPertesDto) {
        for (let i = 0; i < this.dotationEtatRenonciationsPertesDto.length; i++) {
          let rowData = this.dotationEtatRenonciationsPertesDto[i];
          let departementCode = rowData.departementCode;
          if (i === 0) {
            this.rowGroupMetadata[departementCode] = { index: 0, size: 1 };
          } else {
            let previousRowData = this.dotationEtatRenonciationsPertesDto[i - 1];
            let previousRowGroup = previousRowData.departementCode;
            if (departementCode === previousRowGroup) {
              this.rowGroupMetadata[departementCode].size++;
            } else {
                this.rowGroupMetadata[departementCode] = { index: i, size: 1 };
            }
          }
      }
    }
  }
}

import { NgModule } from '@angular/core';

import { DotationEffectuerTransfertComponent } from './dotation-effectuer-transfert.component';

import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [DotationEffectuerTransfertComponent],
  imports: [
    SharedModule
  ]
})
export class DotationEffectuerTransfertModule { }

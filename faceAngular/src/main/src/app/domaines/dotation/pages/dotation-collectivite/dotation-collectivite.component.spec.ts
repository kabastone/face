import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationCollectiviteComponent } from './dotation-collectivite.component';

describe('DotationCollectiviteComponent', () => {
  let component: DotationCollectiviteComponent;
  let fixture: ComponentFixture<DotationCollectiviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationCollectiviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationCollectiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

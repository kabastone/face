import { NgModule } from '@angular/core';

// interne
import { DotationCollectiviteComponent } from './dotation-collectivite.component';

// primeng
import { KeyFilterModule } from 'primeng/keyfilter';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from '@app/share/shared.module';



@NgModule({
  declarations: [
    DotationCollectiviteComponent],
  imports: [
    KeyFilterModule,
    AutoCompleteModule,
    TooltipModule,
    SharedModule
  ]
})
export class DotationCollectiviteModule { }

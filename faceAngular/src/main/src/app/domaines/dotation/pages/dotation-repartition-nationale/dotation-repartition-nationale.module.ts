import { NgModule } from '@angular/core';

// PrimeNG
import {InputNumberModule} from 'primeng/inputnumber';
import { KeyFilterModule } from 'primeng/keyfilter';
import { DotationRepartitionNationaleParentComponent } from './dotation-repartition-nationale-parent/dotation-repartition-nationale-parent.component';
import { DotationRepartitionNationaleProgrammeComponent } from './dotation-repartition-nationale-programme/dotation-repartition-nationale-programme.component';
import { DotationRepartitionNationaleModaleAjoutComponent } from './dotation-repartition-nationale-modale-ajout/dotation-repartition-nationale-modale-ajout.component';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [
    DotationRepartitionNationaleParentComponent,
    DotationRepartitionNationaleProgrammeComponent,
    DotationRepartitionNationaleModaleAjoutComponent
  ],
  imports: [
    KeyFilterModule,
    InputNumberModule,
    SharedModule
  ]
})
export class DotationProgrammeModule { }

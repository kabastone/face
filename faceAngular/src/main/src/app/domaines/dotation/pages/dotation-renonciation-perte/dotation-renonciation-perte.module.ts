import { NgModule } from '@angular/core';

// interne
import { DotationRenonciationPerteParentComponent } from './dotation-renonciation-perte-parent/dotation-renonciation-perte-parent.component';
import { DotationRenonciationPerteProgrammeComponent } from './dotation-renonciation-perte-programme/dotation-renonciation-perte-programme.component';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from '@app/share/shared.module';



@NgModule({
  declarations: [
    DotationRenonciationPerteParentComponent,
    DotationRenonciationPerteProgrammeComponent
  ],
  imports: [
    TooltipModule,
    SharedModule
  ],
  exports: []
})
export class DotationRenonciationPerteModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnneeDto } from '@dotation/dto/annee.dto';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { DotationAnnuelleDto } from '@dotation/dto/dotation-annuelle.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { DotationAnnuelleService } from '@dotation/service/dotation-annuelle.service';
import { DotationAnnuelleDotationCollectiviteDto } from '@dotation/dto/dootation-annuelle-dotation-collectivite.dto';
import { takeUntil } from 'rxjs/operators';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'app-dotation-annuelle',
  templateUrl: './dotation-annuelle.component.html',
  styleUrls: ['./dotation-annuelle.component.css']
})
export class DotationAnnuelleComponent implements OnInit, OnDestroy {

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // Formulaire de la page
  public dotationAnnuelleForm: FormGroup;

  // DTO reflet des données du formulaire
  public dotationAnnuelleDto: DotationAnnuelleDto;

  // pour la liste des annees
  public annees: AnneeDto[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private dotationAnnuelleService: DotationAnnuelleService,
    private fileSaverService: FileSaverService,
    private messageManagerService: MessageManagerService) {
  }

  /**
   * A la destruction : on lance le désabonnement de tous les subscribes
   */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit() {

    // Initialisation du formulaire
    this.initDotationAnnuelleForm();

    // Initialisation des années >= 2016
    const anneeDebut = 2016;
    const anneeEnCours: number = (new Date()).getFullYear();
    for (let i = anneeDebut; i <= anneeEnCours; i++) {
      this.annees.push({ annee: i });
    }

    // Par défaut sur l'année en cours
    this.dotationAnnuelleForm.controls['selectionAnnee'].setValue({ annee: anneeEnCours });

    // Appel au web service quand l'id collectivité change
    this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => (this.majDtoFromFormulaire()));
  }

  private initDotationAnnuelleForm(): void {
    // init. du formulaire
    this.dotationAnnuelleForm = this.formBuilder.group({
      selectionAnnee: new FormControl()
    });
  }

  /**
   * Met à jour l'objet DTO à partir du formulaire.
   *
   */
  public majDtoFromFormulaire(): void {

    // Alimentation du DTO avec les propriétés du form portant le meme nom
    this.dotationAnnuelleDto = this.dotationAnnuelleForm.value as DotationAnnuelleDto;

    const selectionAnnee: AnneeDto = this.dotationAnnuelleForm.get('selectionAnnee').value as AnneeDto;
    this.dotationAnnuelleDto.annee = selectionAnnee.annee;

    this.dotationAnnuelleDto.idCollectivite = this.droitService.getIdCollectivite$().value;

    // Récupération de la dotation annuelle
    this.dotationAnnuelleService
      .rechercherDotationAnnuelle$(this.dotationAnnuelleDto)
      .subscribe(data => {
        this.dotationAnnuelleDto = data;
        this.dotationAnnuelleDto.sommeProgrammePrincipal = this.calculSommeProgramme(
            this.dotationAnnuelleDto.dotationsCollectiviteProgrammePrincipal
          );
      });
  }

  /**
   * Telecharger un document.
   */
  public telechargerDocument(idDocument: number) {

    if (!idDocument) {
      // message de succès
      this.messageManagerService.documentIntrouvableMessage();

      return;
    }

    // téléchargement par id
    this.dotationAnnuelleService.telechargerDocument(idDocument).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });
  }

  /**
   * Calcul la somme des montants
   */
  private calculSommeProgramme(dotationsCollectiviteProgramme: DotationAnnuelleDotationCollectiviteDto[]): number {
    let somme: number;
    somme = 0;
    if (dotationsCollectiviteProgramme) {
      for (let i = 0; i < dotationsCollectiviteProgramme.length; i++) {
        let value: number = dotationsCollectiviteProgramme[i].montant;
        if (isNaN(value)) {
          value = 0;
        }
        somme = somme + value;
      }
    }
    return somme;
  }
}

import { NgModule } from '@angular/core';
// interne
import { DotationAnnuelleComponent } from './dotation-annuelle.component';

// primeng
import { KeyFilterModule } from 'primeng/keyfilter';
import { SharedModule } from '@app/share/shared.module';



@NgModule({
  declarations: [
    DotationAnnuelleComponent],
  imports: [
    KeyFilterModule,
    SharedModule
    
  ]
})
export class DotationAnnuelleModule { }

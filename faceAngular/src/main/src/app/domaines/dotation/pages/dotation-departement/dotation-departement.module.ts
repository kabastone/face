import { NgModule } from '@angular/core';
import { KeyFilterModule } from 'primeng/keyfilter';
import { TooltipModule } from 'primeng/tooltip';
import { DotationDepartementComponent } from './dotation-departement.component';
import { RouterModule } from '@angular/router';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [DotationDepartementComponent],
  imports: [
    TooltipModule,
    KeyFilterModule,
    RouterModule,
    ProgressSpinnerModule,
    SharedModule
  ]
})
export class DotationDepartementModule { }

import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { DotationDetails } from '@dotation/dto/dotation-details';
import { DetailSPService } from '../service/detail-sp.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { SousProgrammeDto } from '@dotation/dto/sous-programme-dto';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { tap, finalize } from 'rxjs/operators';


@Component({
  selector: 'dot-detail-en-preparation',
  templateUrl: './detail-en-preparation.component.html',
  styleUrls: ['./detail-en-preparation.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class DetailEnPreparationComponent implements OnInit {

  angForm: FormGroup;
  dotations: DotationDetails[];

  private montantNum: number;

  // pour la liste des sous programmes
  public sousProgrammes: SousProgrammeLovDto[];

  displayDialog: boolean;

  @Input() idDepartement: number;

  @Output()
  public updateCallback: EventEmitter<any> = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private detailSPService: DetailSPService,
    private confirmationService: ConfirmationService,
    private messageManagerService: MessageManagerService,
    private referentielService: ReferentielService,
    private fileSaverService: FileSaverService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.loadDotationsEnPreparation();
    // Récupérer la liste des sous programmes
    this.recupererListeSousProgrammes();
  }

  private createForm() {
    this.angForm = this.formBuilder.group({
      sousProgramme: new FormControl (),
      montant: new FormControl ([Validators.required, Validators.pattern("([0-9]\s)*")])
    });
  }

  formatMontant(eventValue) {
    this.messageService.clear();
    // Si le champs est null ou vide, ne rien faire
    if (eventValue == null || eventValue === '') {
      return;
    }

    // récupère la valeur du champs et "enlève" ce qui n'est pas numerique
    eventValue = eventValue.replace(/[^0-9]/g, '');

    // parse la valeur récupérée en number et la met dans montantNum
    this.montantNum = parseInt(eventValue, 10);
    // renvois la valeur avec les espaces
    this.angForm.get('montant').setValue(this.montantNum.toLocaleString());
  }

  private recupererListeSousProgrammes(): void {
    this.referentielService.rechercherSousProgrammesPourLigneDotationDepartementale$()
      .subscribe(data => (this.sousProgrammes = data));
  }

  loadDotationsEnPreparation() {
    this.detailSPService.getDotationsEnPreparation(this.idDepartement).subscribe(
      (dotations: DotationDetails[]) => {
        if(dotations.constructor === Array) {
          this.dotations = dotations;
        } else {
          this.dotations = [];
        }
      }
    );
  }

  showDialogToAdd() {
    this.angForm.reset();
    this.displayDialog = true;
  }

  confirmerSuppression(idLigneDotationDepartementale: number) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer cette ligne ?',
      header: 'Suppression Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.detailSPService.deleteLigneDotationDepartementale(idLigneDotationDepartementale).subscribe(
          () => {
            this.loadDotationsEnPreparation();
            this.messageService.clear();
            this.messageManagerService.supprimerLigneDotationSuccess();
          });
      },
      reject: () => {
      }
    });
  }

  televerserEtNotifier(event: any) {
    const fichier: File = event.target.files[0];
    const formUpload = new FormData();
    formUpload.append('fichier', fichier);

    this.detailSPService.televerserEtNotifierDotations(this.idDepartement, formUpload).
      subscribe(_ => {
        this.traiterRetourNotification();
      });
  }

  private traiterRetourNotification() {
    this.loadDotationsEnPreparation();
    this.updateCallback.emit();

    // message de succes
    this.messageManagerService.traiterRetourNotificationMessage();
  }

  generer() {
    this.dotations.forEach(dotation => {
      this.detailSPService.addLigneDotationDepartementale(dotation);
    });
    this.loadDotationsEnPreparation();

    this.detailSPService.genererAnnexeDotationPdfParId(this.idDepartement).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });

  }

  save(sousProgrammeDto: SousProgrammeLovDto) {
    this.displayDialog = false;
    const sousProgramme = new SousProgrammeDto(sousProgrammeDto.abreviation,
      sousProgrammeDto.description, sousProgrammeDto.codeDomaineFonctionnel);
    this.detailSPService.addLigneDotationDepartementale(
      new DotationDetails(this.idDepartement, sousProgramme, this.montantNum))
        .subscribe(_ => {
          this.loadDotationsEnPreparation();
          this.messageManagerService.ajouterLigneDotationSuccess();
        });
    this.montantNum = undefined;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Interne
import { DotationDetailComponent } from './dotation-detail.component';
import { DetailEnPreparationComponent } from './detail-en-preparation/detail-en-preparation.component';
import { DetailEnvoyeesComponent } from './detail-envoyees/detail-envoyees.component';

// PrimeNG
import { KeyFilterModule } from 'primeng/keyfilter';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [DotationDetailComponent, DetailEnPreparationComponent, DetailEnvoyeesComponent],
  imports: [
    TooltipModule,
    KeyFilterModule,
    SharedModule
  ],
  exports: [DotationDetailComponent]
})
export class DotationDetailModule { }

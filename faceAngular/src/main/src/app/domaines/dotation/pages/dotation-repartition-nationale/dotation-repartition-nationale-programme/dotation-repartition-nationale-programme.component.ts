import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { DotationNationaleService } from '@dotation/service/dotation-nationale.service';
import { DotationRepartitionNationaleDto } from '@dotation/dto/dotation-repartition-nationale.dto';
import { DotationProgrammeDto } from '@dotation/dto/dotation-programme-dto';
import { LigneDotationsSousProgrammeDto } from '@dotation/dto/ligne-dotations-sous-programme.dto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MontantEurosPipe } from '@app/custom-pipes/montant-euros.pipe';
import { DotationSousProgrammeMontantBo } from '@dotation/dto/dotation-sous-programme-montant.dto';

@Component({
  selector: 'app-dotation-repartition-nationale-programme',
  templateUrl: './dotation-repartition-nationale-programme.component.html',
  styleUrls: ['./dotation-repartition-nationale-programme.component.css']
})
export class DotationRepartitionNationaleProgrammeComponent implements OnInit, OnDestroy {

  @Input()
  public codeProgramme = '';

  public dotationNationaleDto: DotationRepartitionNationaleDto;

  public isUserMferAdmin: boolean;

  public resteArepartir = 0;

  public modaleDto = {} as DotationSousProgrammeMontantBo;

  @Input()
  public montantStr: string;

  public montantNum: number;

  public montantAnneeNNum: number;

  public montantAnneeNStr: string;

  public ligneDotationsSousProgrammeClone: LigneDotationsSousProgrammeDto;

   // pour le désabonnement auto
   public ngDestroyed$ = new Subject();

  constructor( private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private droitService: DroitService,
    private dotationNationaleService: DotationNationaleService,
    private montantPipe: MontantEurosPipe) { }

  // à la destruction
  ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit() {
    this.dotationNationaleDto = {} as DotationRepartitionNationaleDto;
    this.dotationNationaleDto.dotationProgramme = {} as DotationProgrammeDto;

    this.messageService.clear();

    // Initialisation du profil utilisateur
    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur =>
        this.initialiserIsUserMferAdmin(infoUtilisateur)
      );

      this.dotationNationaleService.rechercherDotationRepartitionNationaleDto$(this.codeProgramme)
      .subscribe(data => this.alimenterDotationNationaleDto(data));

  }

  formatMontant(eventValue) {
    this.messageService.clear();
    // Si le champs est null ou vide, ne rien faire
    if (eventValue == null || eventValue === '') {
      return;
    }

    // récupère la valeur du champs et "enlève" les espaces
    eventValue = eventValue.replace(/\s/g, '');

    // Si il y a une ou des lettres dans le champ, renvois un message d'erreur
    if (eventValue.match(/^[0-9]+$/g) == null) {
      this.messageManagerService.onlyNumberAccepted();
      this.montantStr = eventValue;
      return;
    }

    // parse la valeur récupérée en number et la met dans montantNum
    this.montantNum = parseInt(eventValue, 10);
    // renvois la valeur avec les espaces
    this.montantStr = this.montantNum.toLocaleString();
  }

  formatMontantAnneeN(eventValue) {
    this.messageService.clear();
    // Si le champs est null ou vide, ne rien faire
    if (eventValue == null || eventValue === '') {
      return;
    }

    // récupère la valeur du champs et "enlève" les espaces
    eventValue = eventValue.replace(/\s/g, '');

    // Si il y a une ou des lettres dans le champ, renvois un message d'erreur
    if (eventValue.match(/^[0-9]+$/g) == null) {
      this.messageManagerService.onlyNumberAccepted();
      this.montantAnneeNStr = eventValue;
      return;
    }

    // parse la valeur récupérée en number et la met dans montantAnneeNNum
    this.montantAnneeNNum = parseInt(eventValue, 10);
    // renvois la valeur avec les espaces
    this.montantAnneeNStr = this.montantAnneeNNum.toLocaleString();
  }


   /**
   * Initialise le flag de profil de l'utilisateur.
   */
  private initialiserIsUserMferAdmin(
    infosUtilisateur: UtilisateurConnecteDto ): void {
    this.isUserMferAdmin = this.droitService.isMferResponsable(infosUtilisateur);
  }

  public alimenterDotationNationaleDto(dotationNationale: DotationRepartitionNationaleDto): void {
    this.dotationNationaleDto = dotationNationale;
    this.montantNum = dotationNationale.dotationProgramme.montant;
    this.montantStr = this.montantNum.toLocaleString();
    this.calculerResteArepartir();
  }

  formatLignedotationSelonValidite(ligneDotation: LigneDotationsSousProgrammeDto, annee: number) {

    if(ligneDotation.anneeFinValidite == null || this.dotationNationaleDto.dotationProgramme.annee - annee <= ligneDotation.anneeFinValidite) {
      switch(annee) {
        case 1: return this.montantPipe.transform(ligneDotation.montantAnneeMoins1);
        case 2: return this.montantPipe.transform(ligneDotation.montantAnneeMoins2);
        case 3: return this.montantPipe.transform(ligneDotation.montantAnneeMoins3);
        default: return "";
      }
    } else {
      return "";
    }
  }

  public definirCouleurSelonValidite(ligneDotation: LigneDotationsSousProgrammeDto): Object {
    if (ligneDotation.anneeFinValidite != null && ligneDotation.anneeFinValidite <= new Date().getFullYear()) {
      return 'grey';
    }
  }

  public estLigneDotationValide(ligneDotation: LigneDotationsSousProgrammeDto): Object {
    if (ligneDotation.anneeFinValidite != null && ligneDotation.anneeFinValidite <= new Date().getFullYear()) {
      return false;
    }
    return true;
  }

  private calculerResteArepartir() {
    if (this.isUserMferAdmin && this.dotationNationaleDto.dotationProgramme.montant > this.dotationNationaleDto.sommeDotationsSpAnneeN) {
      this.resteArepartir = this.dotationNationaleDto.dotationProgramme.montant - this.dotationNationaleDto.sommeDotationsSpAnneeN;
    } else {
      this.resteArepartir = 0;
    }
  }

  public modifierMontantDotationProgramme(): void {
    this.messageService.clear();

    // Si le champs contient des lettres : réinitialise le champ et envois un message d'erreur avec un retour en haut de la page
    if (this.montantStr.match(/^[0-9\s]+$/g) == null) {
      this.reinitialiserProgrammeForm();
      this.messageManagerService.onlyNumberAccepted();
      window.scrollTo(0, 0);
      return;
    } else {
    const dotationProgrammeClone: DotationProgrammeDto = Object.assign({}, this.dotationNationaleDto.dotationProgramme);
    dotationProgrammeClone.montant = this.montantNum;

    this.dotationNationaleService.modifierMontantDotationProgrammeRepartitionNationale$(dotationProgrammeClone)
      .subscribe(data => {this.alimenterDotationProgrammeDto(data);
      });
    }
  }

  private alimenterDotationProgrammeDto(dotationProgramme: DotationProgrammeDto) {
    this.dotationNationaleDto.dotationProgramme = dotationProgramme;
    this.montantNum = dotationProgramme.montant;
    this.montantStr = this.montantNum.toLocaleString();
    this.calculerResteArepartir();

    // message de succes
      this.messageManagerService.modificationEnregistree();
  }

   /**
   * reset du formulaire programme.
   *
   */
  public reinitialiserProgrammeForm(): void {
    this.messageService.clear();
    this.montantNum = this.dotationNationaleDto.dotationProgramme.montant;
    this.montantStr = this.montantNum.toLocaleString();
  }

  public alimenterDotationNationaleDtoApresModif(dotationNationale: DotationRepartitionNationaleDto): void {
    this.alimenterDotationNationaleDto(dotationNationale);

    // message de succes
    this.messageManagerService.modificationEnregistree();
  }

  initModale(ligneDotation: LigneDotationsSousProgrammeDto, concerneInitial: boolean){
    this.modaleDto = {
      concerneInitial: concerneInitial,
      idDotationSousProgramme: ligneDotation.idDotationSousProgramme,
      montant: 0
    }
  }

}

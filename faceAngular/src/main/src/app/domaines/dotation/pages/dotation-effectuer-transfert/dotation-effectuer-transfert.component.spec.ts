import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationEffectuerTransfertComponent } from './dotation-effectuer-transfert.component';

describe('DotationEffectuerTransfertComponent', () => {
  let component: DotationEffectuerTransfertComponent;
  let fixture: ComponentFixture<DotationEffectuerTransfertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationEffectuerTransfertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationEffectuerTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

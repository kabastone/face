import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationEtatTransfertComponent } from './dotation-etat-transfert.component';

describe('DotationEtatTransfertAodeComponent', () => {
  let component: DotationEtatTransfertComponent;
  let fixture: ComponentFixture<DotationEtatTransfertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationEtatTransfertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationEtatTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

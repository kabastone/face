import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRepartitionNationaleParentComponent } from './dotation-repartition-nationale-parent.component';

describe('DotationRepartitionNationaleParentComponent', () => {
  let component: DotationRepartitionNationaleParentComponent;
  let fixture: ComponentFixture<DotationRepartitionNationaleParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationRepartitionNationaleParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRepartitionNationaleParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

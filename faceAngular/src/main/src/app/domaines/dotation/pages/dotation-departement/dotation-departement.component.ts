import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { DotationDepartement } from '@dotation/dto/dotation-departement';
import { ImportDotationService } from '@subvention/services/import-dotation.service';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { DotationDepartementService } from './service/dotation-departement.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { switchMap } from 'rxjs/operators'

@Component({
  selector: 'dot-dotation-departement',
  templateUrl: './dotation-departement.component.html',
  styleUrls: ['./dotation-departement.component.css']
})
export class DotationDepartementComponent implements OnInit {
  hasntDotAnnuelle: any;
  dotationsDepartement: DotationDepartement[] = null;
  lancerImportDisabled = true;

  // formulaire dédié à l'upload de fichiers
  private formulaireFichiers: FormData = new FormData();

  // nombre d'éléments par page
  public pageSize: number;

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  constructor(
    private router: Router,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private importDotationService: ImportDotationService,
    private dotationDepartementService: DotationDepartementService,
    private fileSaverService: FileSaverService
  ) { }

  ngOnInit() {
    // Taille de la page
    this.pageSize = 10; // ou +`${AppConfigService.settings.paginationPageSize}`;

    this.importDotationService.verifierDotationAnnuelleNotifier$().subscribe(verif => this.hasntDotAnnuelle = verif);
  }

  /**
   * Chargement des dotations en lazy loading.
   *
   * @param event - critères de la recherche
   */
  public loadDotationsLazy(event: LazyLoadEvent): void {

    this.messageService.clear();

    // Alimentation des données de pagination
    const pageDemande = {
      indexPage: 0,
      taillePage: this.pageSize,
      champTri: '',
      desc: false,
      valeurFiltre: ''
    } as PageDemandeDto;
    pageDemande.indexPage = Math.trunc(event.first / this.pageSize);
    pageDemande.champTri = event.sortField;
    pageDemande.desc = event.sortOrder !== null && event.sortOrder === -1;

    // récupération des dotations par département
    this.dotationDepartementService.getDotationsDepartementales$(pageDemande)
      .subscribe(data => this.alimenterResultatFromPageReponse(data));
  }

  /**
   * Alimentation des résultats de recherche
   *
   * @param pageReponse - résultats de recherche & nb d'éléments trouvés
   */
  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.nbTotalResultats = pageReponse.nbTotalResultats;
      this.dotationsDepartement = pageReponse.listeResultats;
      this.dotationsDepartement.forEach(cur => {
        if (cur.notifiee) {
          this.lancerImportDisabled = false;
        }
      });

      this.dotationsDepartement.forEach(ligne => {
        ligne.sommeMontantsDotation = 0;
        ligne.listeDotationsDepartementalesParSousProgramme.forEach(col => {
          ligne.sommeMontantsDotation += col.dspMontant;
        });
      });


    } else {
      this.dotationsDepartement = null;
      this.nbTotalResultats = 0;
      // message
      this.messageManagerService.aucuneDonneeTrouveeMessage();
    }
  }

  /**
   * Ajout d'un fichier à uploader.
   *
   * @param typeDocument type du document envoyé
   * @param event evenement contenant le fichier envoyé
   */
  public ajoutFichierUploader(typeDocument: string, event: any) {
    const fichier: File = event.target.files[0];

    // ajout du fichier
    if (event.target.files && fichier) {
      this.formulaireFichiers.append(typeDocument, fichier);
      this.importDotationService.importerDotation$(this.formulaireFichiers)
        .pipe(
          switchMap(_ => {
          // Alimentation des données de pagination
          const pageDemande = {
            indexPage: 0,
            taillePage: this.pageSize,
            champTri: '',
            desc: false,
            valeurFiltre: ''
          } as PageDemandeDto;
          return this.dotationDepartementService.getDotationsDepartementales$(pageDemande)
        })).subscribe(data => this.alimenterResultatFromPageReponse(data));
    }
  }

  generer() {
    this.dotationDepartementService.genererAnnexeDotationPdf$().subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });
  }

  public supprimerFichierUploader(typeDocument: string) {
    if (this.formulaireFichiers.has(typeDocument)) {
      this.formulaireFichiers.delete(typeDocument);
    }
  }
}

import {Injectable} from '@angular/core';
import {DotationDetails} from '@dotation/dto/dotation-details';
import {Observable} from 'rxjs';
import {HttpService} from '@app/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class DetailSPService {

  dotations: DotationDetails[];

  constructor(
    private httpService: HttpService
  ) {}

  /**
   * Obtention des lignes de dotation départementale notifiées pour l'année en cours.
   *
   * @returns la liste des lignes dotations departementales.
   */
   getDotationsNotifiees (idDepartement: number): Observable<DotationDetails[]> {
    return this.httpService.envoyerRequeteGet(`/dotation/departement/rechercher/lignes/notifiees/${idDepartement}`, false);
  }

  getDocumentNotification (id: number): Observable<any> {
    return this.httpService.envoyerRequeteGetPourDownload(
      `/dotation/departement/rechercher/document/lignes/notifiees/${id}`
    );
  }

  /**
   * Obtention des lignes de dotation départementale en préparation pour l'année en cours.
   *
   * @returns la liste des lignes dotations departementales.
   */
  getDotationsEnPreparation (idDepartement: number): Observable<DotationDetails[]> {
     return this.httpService.envoyerRequeteGet(`/dotation/departement/rechercher/lignes/enPreparation/${idDepartement}`, false);
  }

  /**
   * Supprimer une ligne de dotation départementale
   *
   * @param idLigneDotationDepartementale - id de la ligne de dotation départementale
   * @returns
   */
  deleteLigneDotationDepartementale(idLigneDotationDepartementale: number): Observable<String> {
    return this.httpService.envoyerRequeteDelete(`/dotation/departement/lignes/${idLigneDotationDepartementale}`);
  }

  /**
   * Ajouter une ligne de dotation départementale
   *
   * @param dotation - ligne de dotation départementale
   * @returns
   */
  addLigneDotationDepartementale(dotation:DotationDetails):Observable<DotationDetails> {
    return this.httpService.envoyerRequetePost(`/dotation/departement/lignes`, dotation);
  }

  /**
   * Generation de la requete pour demande de paiement
   * @param id
   */
  genererAnnexeDotationPdfParId(id: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/dotation/${id}/generation`);
  }

  televerserEtNotifierDotations(idDepartement: number,  formUpload: FormData): Observable<String>{
    return this.httpService.envoyerRequetePostPourUpload(`/dotation/${idDepartement}/televerser`, formUpload);
  }

}

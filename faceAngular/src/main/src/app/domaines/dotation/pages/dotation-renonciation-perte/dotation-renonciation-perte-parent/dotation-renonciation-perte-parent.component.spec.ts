import {  async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRenonciationPerteParentComponent } from './dotation-renonciation-perte-parent.component';

describe('DotationRenonciationPerteComponent', () => {
  let component: DotationRenonciationPerteParentComponent;
  let fixture: ComponentFixture<DotationRenonciationPerteParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DotationRenonciationPerteParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRenonciationPerteParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';

@Component({
  selector: 'app-dotation-renonciation-perte',
  templateUrl: './dotation-renonciation-perte-parent.component.html',
  styleUrls: ['./dotation-renonciation-perte-parent.component.css']
})
export class DotationRenonciationPerteParentComponent implements OnInit {

  constructor(
    private droitService: DroitService
  ) { }

  ngOnInit(): void {
  }

  public estMfer() {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMfer(utilisateurConnecte);
  }

  public isAgent(): boolean {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isUniquementGenerique(utilisateurConnecte);
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DepartementDto } from '@app/dto/departement.dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { DetailEnvoyeesComponent } from './detail-envoyees/detail-envoyees.component';

@Component({
  selector: 'app-dotation-detail',
  templateUrl: './dotation-detail.component.html',
  styleUrls: ['./dotation-detail.component.css']
})
export class DotationDetailComponent implements OnInit {

  // id du département
  public idDepartement = +this.route.snapshot.paramMap.get('idDepartement');

  public departement: DepartementDto;

  @ViewChild(DetailEnvoyeesComponent) child: DetailEnvoyeesComponent;
  update() {
    this.child.loadDotationsNotifiees();
  }

  constructor(private route: ActivatedRoute,
    private router: Router,
    private referentielService: ReferentielService) { }

  dotDepartement = function () {
    this.router.navigateByUrl('/dotation/departement');
  };

  ngOnInit() {
    this.referentielService.rechercherDepartement$(this.idDepartement).subscribe(
      departement => this.departement = departement);
  }


}

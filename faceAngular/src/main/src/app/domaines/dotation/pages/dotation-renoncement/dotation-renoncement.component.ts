import { Component, OnInit, OnDestroy } from '@angular/core';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { DotationRenoncementDto } from '@dotation/dto/dotation-renoncement/dotation-renoncement-dto';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { DroitService } from '@app/services/authentification/droit.service';
import { DotationRenoncementService } from '@dotation/service/dotation-renoncement.service';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dotation-renoncement',
  templateUrl: './dotation-renoncement.component.html',
  styleUrls: ['./dotation-renoncement.component.css']
})
export class DotationRenoncementComponent implements OnInit, OnDestroy {

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // id de la dotation choisie si arrive du tableau de bord (url)
  public idSousProgramme = +this.route.snapshot.paramMap.get('idSousProgramme');

  hide = true;

  public dotationRenoncementForm: FormGroup;

  // objet CritereRechercheDossierDto, reflet des données du formulaire
  public dotationRenoncementDto: DotationRenoncementDto;

  // pour la liste des sous programmes
  public sousProgrammes: SousProgrammeLovDto[];

  // formulaire dédié à l'upload de fichiers
  private formulaireFichier: FormData = new FormData();

  constructor(
    private route: ActivatedRoute,
    private referentielService: ReferentielService,
    private droitService: DroitService,
    private dotationRenoncementService: DotationRenoncementService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private formBuilder: FormBuilder
  ) { }

  /**
   * A la destruction : on lance le désabonnement de tous les subscribes
   */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit() {

    // Initialisation du formulaire
    this.initDotationRenoncementForm();

    // Récupérer la liste des sous programmes
    this.recupererListeSousProgrammes();

    // On reset l'écran quand l'id collectivité change
    this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.resetEcran();
        this.recupererListeSousProgrammes();
      });
  }

  private initDotationRenoncementForm(): void {
    // init. du formulaire
    this.dotationRenoncementForm = this.formBuilder.group({
      selectionSousProgramme: new FormControl(),
      montant: new FormControl()
    });
  }

  /**
   * Récupérer la liste des sous programmes
   *
   */
  public recupererListeSousProgrammes(): void {
    if (this.droitService.getIdCollectivite$().value) {
      this.referentielService
        .rechercherSousProgrammesDeTravauxParCollectivite$(this.droitService.getIdCollectivite$().value)
        .subscribe(data => {
          (this.sousProgrammes = data);
          if (this.idSousProgramme) {
            this.dotationRenoncementForm.get('selectionSousProgramme').setValue(data.find(ssProg => ssProg.id === this.idSousProgramme));
            this.majDtoFromFormulaire();
          }
        });
    }
  }

  /**
   * Met à jour l'objet DTO à partir du formulaire.
   *
   */
  public majDtoFromFormulaire(): void {

    // suppression du fichier à uploader
    this.formulaireFichier = new FormData();

    // Alimentation du DTO avec les propriétés du form portant le meme nom
    this.dotationRenoncementDto = this.dotationRenoncementForm.value as DotationRenoncementDto;

    // On alimente l'id du sous-programme à partir de la liste déroulante.
    if (
      this.dotationRenoncementForm.get('selectionSousProgramme').value !== null &&
      this.dotationRenoncementForm.get('selectionSousProgramme').value !== 'Choisissez un sous programme'
    ) {
      const selectionSousProgramme: SousProgrammeLovDto = this.dotationRenoncementForm.get(
        'selectionSousProgramme'
      ).value as SousProgrammeLovDto;

      this.dotationRenoncementDto.idSousProgramme = selectionSousProgramme.id;

      this.hide = true;

      // Récupération de la dotation disponible
      this.dotationRenoncementService
        .rechercherRenoncementDotationParIdCollectiviteEtIdSousProgramme$(
          this.droitService.getIdCollectivite$().value,
          this.dotationRenoncementDto.idSousProgramme)
        .subscribe(data => {
          this.dotationRenoncementDto = data;
          this.dotationRenoncementDto.idSousProgramme = selectionSousProgramme.id;
          this.hide = false;
        });

      this.dotationRenoncementForm.controls['montant'].setValue(0);

    } else {
      // On reset l'écran
      this.resetEcran();
    }
  }

  /**
   * Initialisation du formulaire avec fichiers.
   */
  initFormData() {

    // init.
    const formulaireAvecFichiers = new FormData();

    // mapping champs speciaux ou cachés
    formulaireAvecFichiers.append('idSousProgramme', this.dotationRenoncementForm.get('selectionSousProgramme').value.id);
    formulaireAvecFichiers.append('montant', this.dotationRenoncementForm.get('montant').value);
    formulaireAvecFichiers.append('idCollectivite', String(this.droitService.getIdCollectivite$().value));
    formulaireAvecFichiers.append('dotationDisponible', String(this.dotationRenoncementDto.dotationDisponible));

    // mapping des fichiers uploadés
    for (const fichier of this.formulaireFichier.entries()) {
      formulaireAvecFichiers.append(fichier[0], fichier[1]);
    }

    return formulaireAvecFichiers;
  }

  /**
   * Met à jour l'objet DTO à partir du formulaire.
   *
   */
  public renoncerDotation(): void {

    // init. du formulaire avec les fichiers uploades
    const formulaireAvecFichier = this.initFormData();

    // Renoncer à la dotation
    this.dotationRenoncementService
      .renoncerDotation$(formulaireAvecFichier)
      .subscribe((dotationRenoncementDto: DotationRenoncementDto) => {

        this.resetEcran();

        // message de succès
        this.messageManagerService.modificationEnregistree();
      });
  }

  /**
   * On reset l'écran
   */
  private resetEcran(): void {
    this.hide = true;
    this.dotationRenoncementDto = this.dotationRenoncementForm.value as DotationRenoncementDto;
    this.dotationRenoncementDto.idSousProgramme = null;
    this.dotationRenoncementDto.dotationDisponible = null;
    this.dotationRenoncementDto.montant = null;
    this.dotationRenoncementDto.idCollectivite = null;
    this.initDotationRenoncementForm();
    // suppression du fichier à uploader
    this.formulaireFichier = new FormData();
    this.messageService.clear();
  }

  public ajoutFichierUploader(typeDocument: string, event: any) {

    // suppression si existe
    if (this.formulaireFichier.has(typeDocument)) {
      this.formulaireFichier.delete(typeDocument);
    }

    const fichier: File = event.target.files[0];

    // ajout du fichier
    if (event.target.files && fichier) {
      this.formulaireFichier.append(typeDocument, fichier);
    }
  }
}

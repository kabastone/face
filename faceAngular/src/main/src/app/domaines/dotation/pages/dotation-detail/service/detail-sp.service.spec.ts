import { TestBed } from '@angular/core/testing';

import { DetailSPService } from './detail-sp.service';

describe('DetailSPService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailSPService = TestBed.get(DetailSPService);
    expect(service).toBeTruthy();
  });
});

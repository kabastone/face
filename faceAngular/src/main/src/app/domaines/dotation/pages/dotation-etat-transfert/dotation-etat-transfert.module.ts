import { NgModule } from '@angular/core';

// interne
import { DotationEtatTransfertComponent } from './dotation-etat-transfert.component';

import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [
    DotationEtatTransfertComponent],
  imports: [
    SharedModule
  ]
})
export class DotationEtatTransfertModule { }

import { Injectable } from '@angular/core';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DotationDepartementService {
  constructor(
    private httpService: HttpService
  ) {
  }

  /**
   * Obtention des dotations departementales.
   *
   * @returns la liste des dotations departementales.
   */
  getDotationsDepartementales$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost('/dotation/departement/rechercher', pageDemande);
  }

  /**
   * Generation des annexes dotations
   * @returns pdf des annexes dotations
   */
  genererAnnexeDotationPdf$() {
    return this.httpService.envoyerRequeteGetPourDownload(`/dotation/departement/generation`);
  }

}

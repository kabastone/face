import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationRepartitionNationaleModaleAjoutComponent } from './dotation-repartition-nationale-modale-ajout.component';

describe('DotationRepartitionNationaleModaleAjoutComponent', () => {
  let component: DotationRepartitionNationaleModaleAjoutComponent;
  let fixture: ComponentFixture<DotationRepartitionNationaleModaleAjoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DotationRepartitionNationaleModaleAjoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationRepartitionNationaleModaleAjoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

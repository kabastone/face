import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationAnnuelleComponent } from './dotation-annuelle.component';

describe('DotationAnnuelleComponent', () => {
  let component: DotationAnnuelleComponent;
  let fixture: ComponentFixture<DotationAnnuelleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationAnnuelleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationAnnuelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

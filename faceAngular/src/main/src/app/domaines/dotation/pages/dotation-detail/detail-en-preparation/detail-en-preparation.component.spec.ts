import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailEnPreparationComponent } from './detail-en-preparation.component';

describe('DetailEnPreparationComponent', () => {
  let component: DetailEnPreparationComponent;
  let fixture: ComponentFixture<DetailEnPreparationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailEnPreparationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailEnPreparationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
// interne
import { DotationRenoncementComponent } from './dotation-renoncement.component';

import { SharedModule } from '@app/share/shared.module';


@NgModule({
  declarations: [
    DotationRenoncementComponent
  ],
  imports: [
    SharedModule
  ]
})
export class DotationRenoncementModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotationDetailComponent } from './dotation-detail.component';

describe('DotationDetailComponent', () => {
  let component: DotationDetailComponent;
  let fixture: ComponentFixture<DotationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Interne
import { DotationDetailComponent } from './pages/dotation-detail/dotation-detail.component';
import { DotationProgrammeModule } from './pages/dotation-repartition-nationale/dotation-repartition-nationale.module';
import { DotationCollectiviteModule } from './pages/dotation-collectivite/dotation-collectivite.module';
import { DotationRenoncementModule } from './pages/dotation-renoncement/dotation-renoncement.module';
import { DotationAnnuelleModule } from './pages/dotation-annuelle/dotation-annuelle.module';
import { DotationDetailModule } from './pages/dotation-detail/dotation-detail.module';
import { DotationEffectuerTransfertModule } from './pages/dotation-effectuer-transfert/dotation-effectuer-transfert.module';
import { DotationEtatTransfertModule } from './pages/dotation-etat-transfert/dotation-etat-transfert.module';
import { DotationRenonciationPerteModule } from './pages/dotation-renonciation-perte/dotation-renonciation-perte.module';

// PrimeNG
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import {KeyFilterModule} from 'primeng/keyfilter';
import {InputTextModule} from 'primeng/inputtext';
import { DotationDepartementModule } from './pages/dotation-departement/dotation-departement.module';
import { DotationRouteModule } from './dotation.routes';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DotationDetailModule,
    TabViewModule,
    TableModule,
    ButtonModule,
    KeyFilterModule,
    InputTextModule,
    FormsModule,
    DotationProgrammeModule,
    DotationCollectiviteModule,
    DotationRenoncementModule,
    DotationAnnuelleModule,
    DotationDepartementModule,
    DotationEffectuerTransfertModule,
    DotationEtatTransfertModule,
    DotationRenonciationPerteModule,
    DotationRouteModule

  ],
  exports: [
    DotationDetailComponent
  ]
})
export class DotationModule { }

import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DemandeProlongationTdbDto } from '../../dto/demande-prolongation-tdb.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { AppConfigService } from '@app/config/app-config.service';

@Component({
  selector: 'app-tdb-liste-demandes-prolongation',
  templateUrl: './tdb-liste-demandes-prolongation.component.html',
  styleUrls: ['./tdb-liste-demandes-prolongation.component.css']
})
export class TdbListeDemandesProlongationComponent implements OnInit, OnChanges {

  @Input()
  public caseTdb = '';

  // liste des résulats chargés
  public demandesProlongationTdbDto: DemandeProlongationTdbDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public pageSize: number;


  constructor(private tableauDeBordService: TableauDeBordService,
    private messageService: MessageService,
    private droitService: DroitService) {
       // Taille de la page
     this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.caseTdb = changes.caseTdb.currentValue;
    this.initListe();
  }

  private initListe() {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemande: PageDemandeDto = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };

   this.appelerServiceRechercheDemandesPaiement(pageDemande);
 }

  public loadDemandesLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemande: PageDemandeDto = {
      indexPage:  Math.trunc(event.first / this.pageSize),
      taillePage: this.pageSize,
      champTri: event.sortField,
      desc: event.sortOrder !== null && event.sortOrder === -1,
      valeurFiltre: ''
    };

    this.appelerServiceRechercheDemandesPaiement(pageDemande);
  }

  private appelerServiceRechercheDemandesPaiement(pageDemande: PageDemandeDto) {
    switch (this.caseTdb) {
      case 'mfer_sub_a_prol': {
        this.tableauDeBordService
          .rechercherTdbMferAprolongerSubvention$(pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
    }
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.demandesProlongationTdbDto = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
    } else {
      this.demandesProlongationTdbDto = null;
      this.nbTotalResultats = 0;
    }
  }

}

import { Component, OnInit, Input, AfterViewChecked } from '@angular/core';
import { ScrollHelper } from '@app/helper/scroll-helper';
import { TableauDeBordMferDto } from '../../dto/tabeau-de-bord-mfer.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';

@Component({
  selector: 'app-tableau-de-bord-mfer',
  templateUrl: './tableau-de-bord-mfer.component.html',
  styleUrls: ['./tableau-de-bord-mfer.component.css']
})
export class TableauDeBordMferComponent implements OnInit, AfterViewChecked {

  public caseTdb = '';

  public composantFils = '';

   public tdbDto: TableauDeBordMferDto;

   @Input() admin: boolean = false;

   scrollHelper = new ScrollHelper();

  constructor(private tableauDeBordService: TableauDeBordService) { }
  ngAfterViewChecked(): void {
    // initialiser la fonction scroll
   this.scrollHelper.doScroll();
  }

  ngOnInit() {

    this.tdbDto = {} as TableauDeBordMferDto;

    // récupération du tableau de bord dto
    this.recupererTableauDto();
  }

  private recupererTableauDto() {
    this.tableauDeBordService
      .rechercherTableauDeBordMferDto$()
      .subscribe(data => (this.tdbDto = data));
  }

  public choisirListe(choixCase: string): void {
    // Déclencher le scroll vers la liste
    this.scrollHelper.scrollToFirst('scroll');
    this.recupererTableauDto();
    this.caseTdb = choixCase;

    switch (choixCase) {
      case 'mfer_sub_a_qual': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'mfer_pai_a_qual': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
      case 'mfer_sub_a_acco': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'mfer_pai_a_acco': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
      case 'mfer_sub_a_attr': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'mfer_sub_a_sign': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'mfer_pai_a_sign': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
      case 'mfer_sub_a_prol': {
        this.composantFils = 'tdbListeDemandesProlongation';
        break;
      }
    }
  }

}

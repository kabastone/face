import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdbListeDotationsCollectiviteComponent } from './tdb-liste-dotations-collectivite.component';

describe('TdbListeDotationsCollectiviteComponent', () => {
  let component: TdbListeDotationsCollectiviteComponent;
  let fixture: ComponentFixture<TdbListeDotationsCollectiviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdbListeDotationsCollectiviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdbListeDotationsCollectiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

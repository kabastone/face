import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeBordSd7Component } from './tableau-de-bord-sd7.component';

describe('TableauDeBordSd7Component', () => {
  let component: TableauDeBordSd7Component;
  let fixture: ComponentFixture<TableauDeBordSd7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauDeBordSd7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauDeBordSd7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

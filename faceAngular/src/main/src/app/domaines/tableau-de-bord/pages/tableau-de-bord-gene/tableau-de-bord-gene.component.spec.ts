import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeBordGeneComponent } from './tableau-de-bord-gene.component';

describe('TableauDeBordGeneComponent', () => {
  let component: TableauDeBordGeneComponent;
  let fixture: ComponentFixture<TableauDeBordGeneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauDeBordGeneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauDeBordGeneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

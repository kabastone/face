import { AfterViewChecked, Component, OnDestroy, OnInit } from '@angular/core';
import { ScrollHelper } from '@app/helper/scroll-helper';
import { DroitService } from '@app/services/authentification/droit.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TableauDeBordGeneDto } from '../../dto/tabeau-de-bord-gene.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';

@Component({
  selector: 'app-tableau-de-bord-gene',
  templateUrl: './tableau-de-bord-gene.component.html',
  styleUrls: ['./tableau-de-bord-gene.component.css']
})
export class TableauDeBordGeneComponent implements OnInit, OnDestroy, AfterViewChecked {

  public tdbDto: TableauDeBordGeneDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  public caseTdb = '';

  public composantFils = '';

  private idCollectivite: number;
  scrollHelper = new ScrollHelper();

  public cadencementEstValide: boolean;

  public d = new Date();

  constructor(
    private tableauDeBordService: TableauDeBordService,
    private droitService: DroitService,
    private messageManagerService: MessageManagerService,
    private messageService: MessageService
    ) { }

  ngAfterViewChecked(): void {
   // initialise le scroll vers la liste
   this.scrollHelper.doScroll();
  }

   // à la destruction
   ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit() {
    this.tdbDto = {} as TableauDeBordGeneDto;

    // récupération de l'idCollectivite afin d'initialiser le tableau de bord
    this.droitService.getIdCollectivite$()
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(data => {
      this.messageService.clear();
      this.idCollectivite = data;
      this.initTdbDto();
    });


  }

  private gererMessageAlerteDateButoireDemandeSubvention(): void {
    const dateActuelle = new Date(this.d.getFullYear(), this.d.getMonth(), this.d.getDate());
    const dateDebutAlerteDateButoire = new Date(this.d.getFullYear(), 6, 31); // 31 juillet (janvier = 0)
    const dateFinAlerteDateButoire = new Date(this.d.getFullYear(), 8, 30); // 30 septembre
    if (((this.tdbDto.nbAdemanderSubvention !== 0) || (this.tdbDto.nbAcompleterSubvention !== 0)) &&
        (dateActuelle <= dateFinAlerteDateButoire) &&
        (dateActuelle >= dateDebutAlerteDateButoire)) {
          this.messageManagerService.alerteDateButoireDemandeSubvention();
      }
  }

  private gererMessageMiseAJourCadencement(boolean: boolean): void {
      this.cadencementEstValide = boolean;
      if (!this.cadencementEstValide) {
        this.messageManagerService.mettreAJourCadencementAODE();
      }
  }

  private initTdbDto(): void {
    this.caseTdb = '';
    this.composantFils = '';
    // récupération du tableau de bord dto
    this.tableauDeBordService
    .rechercherTableauDeBordGeneDto$(this.idCollectivite)
    .subscribe(data => {
        this.tdbDto = data;
        this.gererMessageAlerteDateButoireDemandeSubvention();
        this.tableauDeBordService.estCadencementValide$(this.idCollectivite)
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe(boolean => {
          this.gererMessageMiseAJourCadencement(boolean);
        });
      }
    );
  }

  public choisirListe(choixCase: string): void {
     // déclenche le scroll vers la liste
     this.scrollHelper.scrollToFirst('scroll');

    if (this.idCollectivite) {
      this.initTdbDto();

    }

    this.caseTdb = choixCase;

    switch (choixCase) {
      case 'gene_sub_a_dem': {
        this.composantFils = 'tdbListeDotationsCollectivite';
        break;
      }
      case 'gene_pai_a_dem': {
        this.composantFils = 'tdbListeDossiersSubvention';
        break;
      }
      case 'gene_sub_a_comp': {
        this.composantFils = 'tdbListeDotationsCollectivite';
        break;
      }
      case 'gene_sub_a_corr': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'gene_pai_a_corr': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
      case 'gene_pai_a_comm': {
        this.composantFils = 'tdbListeDossiersSubvention';
        break;
      }
      case 'gene_pai_a_sold': {
        this.composantFils = 'tdbListeDossiersSubvention';
        break;
      }
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdbListeDemandesSubventionComponent } from './tdb-liste-demandes-subvention.component';

describe('TdbListeDemandesSubventionComponent', () => {
  let component: TdbListeDemandesSubventionComponent;
  let fixture: ComponentFixture<TdbListeDemandesSubventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdbListeDemandesSubventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdbListeDemandesSubventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

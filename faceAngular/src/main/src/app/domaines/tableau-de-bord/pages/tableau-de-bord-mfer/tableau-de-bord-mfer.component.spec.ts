import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeBordMferComponent } from './tableau-de-bord-mfer.component';

describe('TableauDeBordMferComponent', () => {
  let component: TableauDeBordMferComponent;
  let fixture: ComponentFixture<TableauDeBordMferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauDeBordMferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauDeBordMferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdbListeDemandesPaiementComponent } from './tdb-liste-demandes-paiement.component';

describe('TdbListeDemandesPaiementComponent', () => {
  let component: TdbListeDemandesPaiementComponent;
  let fixture: ComponentFixture<TdbListeDemandesPaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdbListeDemandesPaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdbListeDemandesPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DemandePaiementTdbDto } from '../../dto/demande-paiement-tdb.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageDemandeWithCollectiviteDto } from '../../dto/page-demande-with-id-collectivite.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { AppConfigService } from '@app/config/app-config.service';

@Component({
  selector: 'app-tdb-liste-demandes-paiement',
  templateUrl: './tdb-liste-demandes-paiement.component.html',
  styleUrls: ['./tdb-liste-demandes-paiement.component.css']
})
export class TdbListeDemandesPaiementComponent implements OnInit, OnChanges {

  @Input()
  public caseTdb = '';

  // liste des résulats chargés
  public demandesPaiementTdbDto: DemandePaiementTdbDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public pageSize: number;

  constructor(private tableauDeBordService: TableauDeBordService,
    private messageService: MessageService,
    private droitService: DroitService) {
       // Taille de la page
     this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.caseTdb = changes.caseTdb.currentValue;
    this.initListe();
  }

  private initListe() {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };

   const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
     pageDemande: pageDemandeDto,
     idCollectivite: this.droitService.getIdCollectivite$().value
   };

   this.appelerServiceRechercheDemandesPaiement(pageDemandeWithCollectivite);
 }

  public loadDemandesLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
      indexPage:  Math.trunc(event.first / this.pageSize),
      taillePage: this.pageSize,
      champTri: event.sortField,
      desc: event.sortOrder !== null && event.sortOrder === -1,
      valeurFiltre: ''
    };

    const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
      pageDemande: pageDemandeDto,
      idCollectivite: this.droitService.getIdCollectivite$().value
    };

    this.appelerServiceRechercheDemandesPaiement(pageDemandeWithCollectivite);
  }

  private appelerServiceRechercheDemandesPaiement(pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto) {
    switch (this.caseTdb) {
      case 'gene_pai_a_corr': {
        this.tableauDeBordService
          .rechercherTdbGeneAcorrigerPaiement$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'sd7_pai_a_cont': {
        this.tableauDeBordService
          .rechercherTdbSd7AcontrolerPaiement$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'sd7_pai_a_tran': {
        this.tableauDeBordService
          .rechercherTdbSd7AtransfererPaiement$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_pai_a_qual': {
        this.tableauDeBordService
          .rechercherTdbMferAqualifierPaiement$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_pai_a_acco': {
        this.tableauDeBordService
          .rechercherTdbMferAaccorderPaiement$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_pai_a_sign': {
        this.tableauDeBordService
          .rechercherTdbMferAsignalerPaiement$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
    }
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.demandesPaiementTdbDto = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
    } else {
      this.demandesPaiementTdbDto = null;
      this.nbTotalResultats = 0;
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdbListeDemandesProlongationComponent } from './tdb-liste-demandes-prolongation.component';

describe('TdbListeDemandesProlongationComponent', () => {
  let component: TdbListeDemandesProlongationComponent;
  let fixture: ComponentFixture<TdbListeDemandesProlongationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdbListeDemandesProlongationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdbListeDemandesProlongationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

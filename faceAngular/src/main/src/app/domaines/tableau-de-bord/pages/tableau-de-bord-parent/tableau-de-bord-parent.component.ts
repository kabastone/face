import { Component, OnInit, OnDestroy } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-tableau-de-bord-parent',
  templateUrl: './tableau-de-bord-parent.component.html',
  styleUrls: ['./tableau-de-bord-parent.component.css']
})
export class TableauDeBordParentComponent implements OnInit,  OnDestroy {

  public isUserGenerique: boolean;

  public isUserMfer: boolean;

  public isUserMferAdmin: boolean;

  public isUserSd7: boolean;

  public isUserSd7Admin: boolean;



  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(private droitService: DroitService) {
  }

   // à la destruction
   public ngOnDestroy() {
    this.ngDestroyed$.next();
  }


ngOnInit() {

    this.isUserGenerique = false;
    this.isUserMfer = false;
    this.isUserMferAdmin = false;
    this.isUserSd7 = false;
    this.isUserSd7Admin = false;

    // Initialisation du profil utilisateur
    this.droitService
     .getInfosUtilisateur$()
     .pipe(takeUntil(this.ngDestroyed$))
     .subscribe(infoUtilisateur =>
       this.initialiserFlagsProfilUtilisateur(infoUtilisateur)
    );

  }


  /**
   * Initialise le flag de profil de l'utilisateur.
   */
  private initialiserFlagsProfilUtilisateur(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    if (this.droitService.isMferAdmin(infosUtilisateur)) {
      this.isUserMferAdmin = true;
      this.isUserMfer = true;
    } else if (this.droitService.isMfer(infosUtilisateur)) {
      this.isUserMfer = true;
    } else if (this.droitService.isSd7Admin(infosUtilisateur)) {
      this.isUserSd7Admin = true;
      this.isUserSd7 = true;
    } else if (this.droitService.isSd7(infosUtilisateur)) {
      this.isUserSd7 = true;
    } else {
      this.isUserGenerique = this.droitService.isUniquementGenerique(infosUtilisateur);
    }
  }

}

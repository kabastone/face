import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DemandeSubventionTdbDto } from '../../dto/demande-subvention-tdb.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageDemandeWithCollectiviteDto } from '../../dto/page-demande-with-id-collectivite.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { AppConfigService } from '@app/config/app-config.service';

@Component({
  selector: 'app-tdb-liste-demandes-subvention',
  templateUrl: './tdb-liste-demandes-subvention.component.html',
  styleUrls: ['./tdb-liste-demandes-subvention.component.css']
})
export class TdbListeDemandesSubventionComponent implements OnInit, OnChanges {

  @Input()
  public caseTdb = '';

  // liste des résulats chargés
  public demandesSubventionTdbDto: DemandeSubventionTdbDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public pageSize: number;

  constructor(private tableauDeBordService: TableauDeBordService,
    private messageService: MessageService,
    private droitService: DroitService) {
       // Taille de la page
     this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;
    }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.caseTdb = changes.caseTdb.currentValue;
    this.initListe();
  }

  private initListe() {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };

   const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
     pageDemande: pageDemandeDto,
     idCollectivite: this.droitService.getIdCollectivite$().value
   };

   this.appelerServiceRechercheDemandesSubvention(pageDemandeWithCollectivite);
 }

  public loadDemandesLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
      indexPage:  Math.trunc(event.first / this.pageSize),
      taillePage: this.pageSize,
      champTri: event.sortField,
      desc: event.sortOrder !== null && event.sortOrder === -1,
      valeurFiltre: ''
    };

    const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
      pageDemande: pageDemandeDto,
      idCollectivite: this.droitService.getIdCollectivite$().value
    };

    this.appelerServiceRechercheDemandesSubvention(pageDemandeWithCollectivite);
  }

  private appelerServiceRechercheDemandesSubvention(pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto) {
    switch (this.caseTdb) {
      case 'gene_sub_a_corr': {
        this.tableauDeBordService
          .rechercherTdbGeneAcorrigerSubvention$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'sd7_sub_a_cont': {
        this.tableauDeBordService
          .rechercherTdbSd7AcontrolerSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'sd7_sub_a_tran': {
        this.tableauDeBordService
          .rechercherTdbSd7AtransfererSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_sub_a_qual': {
        this.tableauDeBordService
          .rechercherTdbMferAqualifierSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_sub_a_acco': {
        this.tableauDeBordService
          .rechercherTdbMferAaccorderSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_sub_a_attr': {
        this.tableauDeBordService
          .rechercherTdbMferAattribuerSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'mfer_sub_a_sign': {
        this.tableauDeBordService
          .rechercherTdbMferAsignalerSubvention$(pageDemandeWithCollectivite.pageDemande)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
    }
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.demandesSubventionTdbDto = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
    } else {
      this.demandesSubventionTdbDto = null;
      this.nbTotalResultats = 0;
    }
  }

}

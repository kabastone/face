import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdbListeDossiersSubventionComponent } from './tdb-liste-dossiers-subvention.component';

describe('TdbListeDossiersSubventionComponent', () => {
  let component: TdbListeDossiersSubventionComponent;
  let fixture: ComponentFixture<TdbListeDossiersSubventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdbListeDossiersSubventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdbListeDossiersSubventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

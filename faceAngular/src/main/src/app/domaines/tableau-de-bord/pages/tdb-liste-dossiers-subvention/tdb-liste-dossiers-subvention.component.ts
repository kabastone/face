import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DossierSubventionTdbDto } from '../../dto/dossier-subvention-tdb.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageDemandeWithCollectiviteDto } from '../../dto/page-demande-with-id-collectivite.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { AppConfigService } from '@app/config/app-config.service';

@Component({
  selector: 'app-tdb-liste-dossiers-subvention',
  templateUrl: './tdb-liste-dossiers-subvention.component.html',
  styleUrls: ['./tdb-liste-dossiers-subvention.component.css']
})
export class TdbListeDossiersSubventionComponent implements OnInit, OnChanges {

  @Input()
  public caseTdb = '';

  // liste des résulats chargés
  public dossiersSubventionTdbDto: DossierSubventionTdbDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public pageSize: number;

  constructor(private tableauDeBordService: TableauDeBordService,
    private messageService: MessageService,
    private droitService: DroitService) {
      // Taille de la page
     this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;
     }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.caseTdb = changes.caseTdb.currentValue;
    this.initListe();
  }

  private initListe() {
     this.messageService.clear();

     // Alimentation des données de pagination (pageDemande)
     const pageDemandeDto: PageDemandeDto = {
      indexPage: 0,
      taillePage: this.pageSize,
      champTri: '',
      desc: false,
      valeurFiltre: ''
    };

    const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
      pageDemande: pageDemandeDto,
      idCollectivite: this.droitService.getIdCollectivite$().value
    };

    this.appelerServiceRechercheDossiersSubvention(pageDemandeWithCollectivite);
  }

  public loadDossiersLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
      indexPage:  Math.trunc(event.first / this.pageSize),
      taillePage: this.pageSize,
      champTri: event.sortField,
      desc: event.sortOrder !== null && event.sortOrder === -1,
      valeurFiltre: ''
    };

    const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
      pageDemande: pageDemandeDto,
      idCollectivite: this.droitService.getIdCollectivite$().value
    };

    this.appelerServiceRechercheDossiersSubvention(pageDemandeWithCollectivite);
  }

  private appelerServiceRechercheDossiersSubvention(pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto) {
    switch (this.caseTdb) {
      case 'gene_pai_a_dem': {
        this.tableauDeBordService
          .rechercherTdbGeneAdemanderPaiement$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'gene_pai_a_comm': {
        this.tableauDeBordService
          .rechercherTdbGeneAcommencerPaiement$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'gene_pai_a_sold': {
        this.tableauDeBordService
          .rechercherTdbGeneAsolderPaiement$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
    }
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.dossiersSubventionTdbDto = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
    } else {
      this.dossiersSubventionTdbDto = null;
      this.nbTotalResultats = 0;
    }
  }

}

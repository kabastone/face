import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeBordParentComponent } from './tableau-de-bord-parent.component';

describe('TableauDeBordParentComponent', () => {
  let component: TableauDeBordParentComponent;
  let fixture: ComponentFixture<TableauDeBordParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauDeBordParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauDeBordParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

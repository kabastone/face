import { Component, OnInit, Input,  OnChanges, SimpleChanges } from '@angular/core';
import { DotationCollectiviteTdbDto } from '../../dto/dotation-collectivite-tdb.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';
import { MessageService, LazyLoadEvent } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';
import { PageDemandeDto } from '@app/dto/page-demande.dto';
import { PageDemandeWithCollectiviteDto } from '../../dto/page-demande-with-id-collectivite.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { AppConfigService } from '@app/config/app-config.service';
import { DemandeSubventionService } from '@subvention/pages/subvention-demande/services/demande-subvention.service';

@Component({
  selector: 'app-tdb-liste-dotations-collectivite',
  templateUrl: './tdb-liste-dotations-collectivite.component.html',
  styleUrls: ['./tdb-liste-dotations-collectivite.component.css']
})
export class TdbListeDotationsCollectiviteComponent implements OnInit, OnChanges  {

  @Input()
  public caseTdb = '';

  // liste des résulats chargés
  public dotationsCollectiviteTdbDto: DotationCollectiviteTdbDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public pageSize: number;

  constructor(private tableauDeBordService: TableauDeBordService,
    private messageService: MessageService,
    private droitService: DroitService,
    private demandeSubventionService: DemandeSubventionService) {
      // Taille de la page
     this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;
     }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.caseTdb = changes.caseTdb.currentValue;
    this.initListe();
  }

  private initListe() {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
     indexPage: 0,
     taillePage: this.pageSize,
     champTri: '',
     desc: false,
     valeurFiltre: ''
   };

   const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
     pageDemande: pageDemandeDto,
     idCollectivite: this.droitService.getIdCollectivite$().value
   };

   this.appelerServiceRechercheDotationsCollectivite(pageDemandeWithCollectivite);
 }

 public passageDotation(dotationDto: DotationCollectiviteTdbDto): void {
  this.demandeSubventionService.dotationDto = dotationDto;
 }

  public loadDotationsLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    const pageDemandeDto: PageDemandeDto = {
      indexPage:  Math.trunc(event.first / this.pageSize),
      taillePage: this.pageSize,
      champTri: event.sortField,
      desc: event.sortOrder !== null && event.sortOrder === -1,
      valeurFiltre: ''
    };

    const pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto = {
      pageDemande: pageDemandeDto,
      idCollectivite: this.droitService.getIdCollectivite$().value
    };

    this.appelerServiceRechercheDotationsCollectivite(pageDemandeWithCollectivite);
  }

  private appelerServiceRechercheDotationsCollectivite(pageDemandeWithCollectivite: PageDemandeWithCollectiviteDto) {
    switch (this.caseTdb) {
      case 'gene_sub_a_dem': {
        this.tableauDeBordService
          .rechercherTdbGeneAdemanderSubvention$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
      case 'gene_sub_a_comp': {
        this.tableauDeBordService
          .rechercherTdbGeneAcompleterSubvention$(pageDemandeWithCollectivite)
          .subscribe(data => this.alimenterResultatFromPageReponse(data));
        break;
      }
    }
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.dotationsCollectiviteTdbDto = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
    } else {
      this.dotationsCollectiviteTdbDto = null;
      this.nbTotalResultats = 0;
    }
  }

}

import { Component, OnInit, Input, AfterViewChecked } from '@angular/core';
import { ScrollHelper } from '@app/helper/scroll-helper';
import { TableauDeBordSd7Dto } from '../../dto/tabeau-de-bord-sd7.dto';
import { TableauDeBordService } from '../../services/tableau-de-bord.service';

@Component({
  selector: 'app-tableau-de-bord-sd7',
  templateUrl: './tableau-de-bord-sd7.component.html',
  styleUrls: ['./tableau-de-bord-sd7.component.css']
})
export class TableauDeBordSd7Component implements OnInit, AfterViewChecked {

  public caseTdb = '';

  public composantFils = '';

  public tdbDto: TableauDeBordSd7Dto;

  @Input() admin: boolean = false;

  scrollHelper = new ScrollHelper();


  constructor(private tableauDeBordService: TableauDeBordService) { }

  ngAfterViewChecked(): void {
    // Initialise la fonction scroll si la liste est prête
    this.scrollHelper.doScroll();
  }

  ngOnInit() {

    this.tdbDto = {} as TableauDeBordSd7Dto;

    // récupération du tableau de bord dto
    this.recupererTableauDto();
  }

  private recupererTableauDto() {
    this.tableauDeBordService
      .rechercherTableauDeBordSd7Dto$()
      .subscribe(data => (this.tdbDto = data));
  }

  public choisirListe(choixCase: string): void {

    //déclenche le scroll vers la lsite
    this.scrollHelper.scrollToFirst('scroll');

    this.recupererTableauDto();
    this.caseTdb = choixCase;

    switch (choixCase) {
      case 'sd7_sub_a_cont': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'sd7_pai_a_cont': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
      case 'sd7_sub_a_tran': {
        this.composantFils = 'tdbListeDemandesSubvention';
        break;
      }
      case 'sd7_pai_a_tran': {
        this.composantFils = 'tdbListeDemandesPaiement';
        break;
      }
    }
  }

}

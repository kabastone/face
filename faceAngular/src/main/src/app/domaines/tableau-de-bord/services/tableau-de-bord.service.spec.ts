import { TestBed } from '@angular/core/testing';

import { TableauDeBordService } from './tableau-de-bord.service';

describe('TableauDeBordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableauDeBordService = TestBed.get(TableauDeBordService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { TableauDeBordMferDto } from '../dto/tabeau-de-bord-mfer.dto';
import { Observable } from 'rxjs';
import { TableauDeBordSd7Dto } from '../dto/tabeau-de-bord-sd7.dto';
import { TableauDeBordGeneDto } from '../dto/tabeau-de-bord-gene.dto';
import { PageDemandeWithCollectiviteDto } from '../dto/page-demande-with-id-collectivite.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { PageDemandeDto } from '@app/dto/page-demande.dto';

@Injectable({
  providedIn: 'root'
})
export class TableauDeBordService {

  constructor( private httpService: HttpService) { }

  /**
   * Rechercher tableau de bord MFER
   *
   * @returns le TableauDeBordMferDto
   */
  rechercherTableauDeBordMferDto$(): Observable<TableauDeBordMferDto> {
    return this.httpService.envoyerRequeteGet(`/tableaudebord/mfer/rechercher`, true);
  }

   /**
   * Rechercher tableau de bord SD7
   *
   * @returns le TableauDeBordSd7Dto
   */
  rechercherTableauDeBordSd7Dto$(): Observable<TableauDeBordSd7Dto> {
    return this.httpService.envoyerRequeteGet(`/tableaudebord/sd7/rechercher`, true);
  }

  /**
   * Rechercher tableau de bord generique
   *
   * @returns le TableauDeBordGeneDto
   */
  rechercherTableauDeBordGeneDto$(idCollectivite: number): Observable<TableauDeBordGeneDto> {
    return this.httpService.envoyerRequeteGet(`/tableaudebord/gene/${idCollectivite}/rechercher`, true);
  }

  /**
   * Rechercher les resultats de subvention à demander
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAdemanderSubvention$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/subademander/rechercher`, pageDemandeWithCollectiviteDto);
  }

  /**
   * Rechercher les resultats de paiement à demander
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAdemanderPaiement$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/paiademander/rechercher`, pageDemandeWithCollectiviteDto);
  }

   /**
   * Rechercher les resultats de subvention à completer
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAcompleterSubvention$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/subacompleter/rechercher`, pageDemandeWithCollectiviteDto);
  }

   /**
   * Rechercher les resultats de subvention à corriger
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAcorrigerSubvention$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/subacorriger/rechercher`, pageDemandeWithCollectiviteDto);
  }

   /**
   * Rechercher les resultats de paiement à corriger
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAcorrigerPaiement$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/paiacorriger/rechercher`, pageDemandeWithCollectiviteDto);
  }

   /**
   * Rechercher les resultats de paiement à commencer
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAcommencerPaiement$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/paiacommencer/rechercher`, pageDemandeWithCollectiviteDto);
  }

   /**
   * Rechercher les resultats de paiement à solder
   *
   * @returns le PageReponseDto
   */
  rechercherTdbGeneAsolderPaiement$(pageDemandeWithCollectiviteDto: PageDemandeWithCollectiviteDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/gene/paiasolder/rechercher`, pageDemandeWithCollectiviteDto);
  }

  /**
   * Rechercher les resultats de subvention à controler
   *
   * @returns le PageReponseDto
   */
  rechercherTdbSd7AcontrolerSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/sd7/subacontroler/rechercher`, pageDemande);
  }

  /**
   * Rechercher les resultats de paiement à controler
   *
   * @returns le PageReponseDto
   */
  rechercherTdbSd7AcontrolerPaiement$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/sd7/paiacontroler/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à transferer
   *
   * @returns le PageReponseDto
   */
  rechercherTdbSd7AtransfererSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/sd7/subatransferer/rechercher`, pageDemande);
  }

  /**
   * Rechercher les resultats de paiement à transferer
   *
   * @returns le PageReponseDto
   */
  rechercherTdbSd7AtransfererPaiement$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/sd7/paiatransferer/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à qualifier
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAqualifierSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/subaqualifier/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de paiement à qualifier
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAqualifierPaiement$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/paiaqualifier/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à accorder
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAaccorderSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/subaaccorder/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de paiement à accorder
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAaccorderPaiement$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/paiaaccorder/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à attribuer
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAattribuerSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/subaattribuer/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à signaler
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAsignalerSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/subasignaler/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de paiement à signaler
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAsignalerPaiement$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/paiasignaler/rechercher`, pageDemande);
  }

   /**
   * Rechercher les resultats de subvention à prolonger
   *
   * @returns le PageReponseDto
   */
  rechercherTdbMferAprolongerSubvention$(pageDemande: PageDemandeDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/tableaudebord/mfer/subaprolonger/rechercher`, pageDemande);
  }

  estCadencementValide$(idCollectivite: number): Observable<boolean> {
    return this.httpService.envoyerRequeteGet(`/subvention/cadencement/collectivite/${idCollectivite}/estValide`);
  }

}

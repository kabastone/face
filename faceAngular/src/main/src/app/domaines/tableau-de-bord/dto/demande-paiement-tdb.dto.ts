export interface DemandePaiementTdbDto {
  id: number;
  dateCreationDossier: Date;
  nomCourtCollectivite: string;
  numDossier: string;
  aideDemandeeDemande: number;
}

export interface TableauDeBordSd7Dto {

  nbAcontrolerSubvention: number;

  nbAcontrolerPaiement: number;

  nbAtransfererSubvention: number;

  nbAtransfererPaiement: number;

}

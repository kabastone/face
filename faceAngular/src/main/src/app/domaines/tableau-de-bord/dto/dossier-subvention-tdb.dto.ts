export interface DossierSubventionTdbDto {
  id: number;
  dateCreationDossier: Date;
  nomCourtCollectivite: string;
  numDossier: string;
  plafondAideDossier: number;
  plafondAideSansRefus: number;
}

export interface TableauDeBordMferDto {

  nbAqualifierSubvention: number;

  nbAqualifierPaiement: number;

  nbAaccorderSubvention: number;

  nbAaccorderPaiement: number;

  nbAattribuerSubvention: number;

  nbAsignalerSubvention: number;

  nbAsignalerPaiement: number;

  nbAprolongerSubvention: number;

}

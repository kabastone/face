export interface DemandeSubventionTdbDto {
  id: number;
  dateCreationDossier: Date;
  nomCourtCollectivite: string;
  numDossier: string;
  plafondAideDemande: number;
}

export interface TableauDeBordGeneDto {

  nbAdemanderSubvention: number;

  nbAdemanderPaiement: number;

  nbAcompleterSubvention: number;

  nbAcorrigerSubvention: number;

  nbAcorrigerPaiement: number;

  nbAcommencerPaiement: number;

  nbAsolderPaiement: number;

}

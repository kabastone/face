export interface DemandeProlongationTdbDto {
  id: number;
  dateCreationDossier: Date;
  nomCourtCollectivite: string;
  numDossier: string;
  dateAchevementDemandee: Date;
}

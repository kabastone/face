import { PageDemandeDto } from '@app/dto/page-demande.dto';

export interface PageDemandeWithCollectiviteDto {

  pageDemande: PageDemandeDto;
  idCollectivite: number;
}

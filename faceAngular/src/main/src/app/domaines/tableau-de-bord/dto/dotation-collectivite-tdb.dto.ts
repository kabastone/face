export interface DotationCollectiviteTdbDto {
  id: number;
  descriptionSousProgramme: number;
  montant: number;
  idSousProgramme: number;
}

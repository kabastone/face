import { Routes } from '@angular/router';
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { TableauDeBordParentComponent } from '@app/domaines/tableau-de-bord/pages/tableau-de-bord-parent/tableau-de-bord-parent.component';

export const TABLEAU_DE_BORD_ROUTES: Routes = [
  // Tableau de bord
  {
    path: 'afficher',
    component: TableauDeBordParentComponent,
    data: { title: 'Tableau de bord' },
    resolve: { componentMsg: ComponentResolverMsg },
  }
];

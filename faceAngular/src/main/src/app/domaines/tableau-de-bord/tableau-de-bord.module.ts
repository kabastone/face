import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { TableauDeBordParentComponent } from './pages/tableau-de-bord-parent/tableau-de-bord-parent.component';
import { TableauDeBordGeneComponent } from './pages/tableau-de-bord-gene/tableau-de-bord-gene.component';
import { TableauDeBordMferComponent } from './pages/tableau-de-bord-mfer/tableau-de-bord-mfer.component';
import { TableauDeBordSd7Component } from './pages/tableau-de-bord-sd7/tableau-de-bord-sd7.component';
import { CustomPipesModule } from '@app/custom-pipes/custom-pipes.module';
import { RouterModule } from '@angular/router';
import { TdbListeDotationsCollectiviteComponent } from './pages/tdb-liste-dotations-collectivite/tdb-liste-dotations-collectivite.component';
import { TdbListeDossiersSubventionComponent } from './pages/tdb-liste-dossiers-subvention/tdb-liste-dossiers-subvention.component';
import { TdbListeDemandesSubventionComponent } from './pages/tdb-liste-demandes-subvention/tdb-liste-demandes-subvention.component';
import { TdbListeDemandesPaiementComponent } from './pages/tdb-liste-demandes-paiement/tdb-liste-demandes-paiement.component';
import { TdbListeDemandesProlongationComponent } from './pages/tdb-liste-demandes-prolongation/tdb-liste-demandes-prolongation.component';

@NgModule({
  declarations: [
    TableauDeBordParentComponent, 
    TableauDeBordGeneComponent, 
    TableauDeBordMferComponent, 
    TableauDeBordSd7Component,
    TdbListeDotationsCollectiviteComponent, 
    TdbListeDossiersSubventionComponent, 
    TdbListeDemandesSubventionComponent, 
    TdbListeDemandesPaiementComponent, 
    TdbListeDemandesProlongationComponent],
  imports: [
    CommonModule,
    TabViewModule,
    TableModule,
    PanelModule,
    ButtonModule,
    RouterModule,
    CustomPipesModule
  ]
})
export class TableauDeBordModule { }

import { RouterModule, Routes } from '@angular/router';

// Interne
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { SubventionDossierComponent } from './pages/subvention-dossier/subvention-dossier.component';
import { SubventionDemandeComponent } from './pages/subvention-demande/subvention-demande.component';
import { SubventionRechercheDossierComponent } from './pages/subvention-recherche-dossier/subvention-recherche-dossier.component';
import { SubventionCadencementComponent } from './pages/subvention-cadencement/subvention-cadencement.component';
import { NgModule } from '@angular/core';
// import { INSTRUCTION_ROUTES } from './pages/subvention-demande/subvention-demande.routes';

export const SUBVENTION_ROUTES: Routes = [
  {
    path: 'dossier/:id',
    component: SubventionDossierComponent,
    data: { title: 'Dossier de subvention' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'demande',
    component: SubventionDemandeComponent,
    // children: INSTRUCTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Demande de subvention' }
  },
  {
    path: 'demande/projet',
    component: SubventionDemandeComponent,
    // children: INSTRUCTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Soumettre un projet' }
  },
  {
    path: 'demande/dossier/:idDossierSubvention',
    component: SubventionDemandeComponent,
    // children: INSTRUCTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Demande de subvention' }
  },
  {
    path: 'demande/:idDemandeSubvention',
    component: SubventionDemandeComponent,
    // children: INSTRUCTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg },
    data: { title: 'Demande de subvention' }
  },
  // Recherche un dossier de subvention
  {
    path: 'recherche',
    component: SubventionRechercheDossierComponent,
    data: { title: 'Recherche de subventions' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'cadencement/demande',
    component: SubventionCadencementComponent,
    data: { title: 'Subventions demandées' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'cadencement/projet',
    component: SubventionCadencementComponent,
    data: { title: 'Projets soumis' },
    resolve: { componentMsg: ComponentResolverMsg }
  }
];

const routes: Routes = [
  {
    path: '',
    children: SUBVENTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
];

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class SubventionRouteModule {
  
}
import { Component, OnInit } from '@angular/core';
import { DemandeSubventionSimpleDto } from '@subvention/dto/demande-subvention-simple.dto';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DossierSubventionService } from '../service/dossier-subvention.service';

@Component({
  selector: 'sub-dossier-subvention',
  templateUrl: './dossier-subvention.component.html',
  styleUrls: ['./dossier-subvention.component.css']
})
export class DossierSubventionComponent implements OnInit {

  // sujet à souscrire pour la maj de la liste de subvention
  private demandeSubventionSubject = new Subject<DemandeSubventionSimpleDto[]>();

  // observable : liste des demande de subvention
  public listeDemandeSubvention$: Observable<DemandeSubventionSimpleDto[]> = this.demandeSubventionSubject.asObservable();
  idDossier:number;

  // constructeur
  constructor(private route: ActivatedRoute,
    private dossierSubventionService: DossierSubventionService) { }

  // init
  ngOnInit() {
    // récupération id dossier
    this.route.params.subscribe(params => this.idDossier = +params.id);

    // obtention des demandes de subvention
    this.dossierSubventionService.getDemandesSubvention$(this.idDossier)
      .subscribe((listeDemandes: DemandeSubventionSimpleDto[]) => {
        if (listeDemandes && listeDemandes.length > 0) {
          this.demandeSubventionSubject.next(listeDemandes);
        }
      });
  }

  getLibelleEtatDemande(demande: DemandeSubventionSimpleDto): String {
    switch (demande.codeEtatDemande) {
      case 'ANOMALIE_DETECTEE':
        return 'Détectée en anomalie';
      case 'DEMANDEE':
        return 'Demandée';
      case 'CORRIGEE':
        return 'Corrigée';
      case 'APPROUVEE':
        return 'Approuvée';
      case 'QUALIFIEE':
        return 'Qualifiée';
      case 'CONTROLEE':
        return 'Contrôlée';
      case 'ACCORDEE':
        return 'Accordée';
      case 'INITIAL':
        return 'Initial';
      case 'ANOMALIE_SIGNALEE':
        return 'Signalée en anomalie';
      case 'REFUSEE':
        return 'Refusée';
      case 'REJET_TAUX':
        return 'Rejetée pour modification de taux';
      case 'ATTRIBUEE':
        return 'Attribuée';
      case 'EN_ATTENTE_TRANSFERT_MAN':
        return 'En attente de transfert manuel';
      case 'EN_ATTENTE_TRANSFERT':
        return 'En attente de transfert';
      case 'EN_COURS_TRANSFERT':
        return 'En cours de transfert';
      case 'INCOHERENCE_CHORUS':
        return 'Incohérence avec CHORUS';
      case 'CLOTUREE':
        return 'Cloturée';
      case 'EN_INSTRUCTION':
      default:
        return 'En instruction';
    }
  }

  /**
   * Retourne la classe utilisée pour l'affichage de l'icône d'état de la demande.
   *
   * @param demande de subvention
   * @returns classe de l'icône à utiliser
   */
  getClassIcone(demande: DemandeSubventionSimpleDto): String {
    // listing des etats possibles
    const sablierNoir: String[] = ['DEMANDEE', 'QUALIFIEE', 'ACCORDEE', 'CONTROLEE',
      'EN_ATTENTE_TRANSFERT_MAN', 'EN_ATTENTE_TRANSFERT', 'EN_COURS_TRANSFERT', 'TRANSFEREE',
      'APPROUVEE', 'INCOHERENCE_CHORUS', 'ANOMALIE_DETECTEE', 'CORRIGEE'];
    const alerteOrange: String[] = ['ANOMALIE_SIGNALEE'];
    const checkVert: String[] = ['ATTRIBUEE', 'CLOTUREE'];
    const croixRouge: String[] = ['REFUSEE', 'REJET_TAUX'];

    // affichage de la classe en fonction de l'état
    if (sablierNoir.includes(demande.codeEtatDemande)) {
      return 'fas fa-hourglass-half';
    }
    if (alerteOrange.includes(demande.codeEtatDemande)) {
      return 'fas fa-exclamation-triangle text-warning';
    }
    if (checkVert.includes(demande.codeEtatDemande)) {
      return 'fas fa-check text-success';
    }
    if (croixRouge.includes(demande.codeEtatDemande)) {
      return 'fas fa-times text-danger';
    }
  }
}

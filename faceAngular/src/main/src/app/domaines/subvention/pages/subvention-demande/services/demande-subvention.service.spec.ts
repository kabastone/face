import { TestBed } from '@angular/core/testing';

import { DemandeSubventionService } from './demande-subvention.service';

describe('DemandeSubventionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemandeSubventionService = TestBed.get(DemandeSubventionService);
    expect(service).toBeTruthy();
  });
});

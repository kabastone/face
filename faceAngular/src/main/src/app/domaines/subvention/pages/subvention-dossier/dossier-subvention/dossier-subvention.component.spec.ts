import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossierSubventionComponent } from './dossier-subvention.component';

describe('DossierSubventionComponent', () => {
  let component: DossierSubventionComponent;
  let fixture: ComponentFixture<DossierSubventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossierSubventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossierSubventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

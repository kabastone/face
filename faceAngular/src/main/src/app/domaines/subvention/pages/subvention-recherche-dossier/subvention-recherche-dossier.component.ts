import { AfterViewChecked, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppConfigService } from '@app/config/app-config.service';
import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { EtatDemandeLovDto } from '@app/dto/etat-demande-lov.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { CritereRechercheDossierDto } from '@subvention/dto/critere-recherche-dossier.dto';
import { ResultatRechercheDossierDto } from '@subvention/dto/resultat-recherche-dossier.dto';
import { RechercheDossierService } from '@subvention/services/recherche-dossier.service';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { Subject, Subscription } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { GestionAffichagesChampsService } from '@subvention/services/gestion-affichages-champs.service';
import { Store } from '@ngrx/store';
import * as Actions from '../../store/actions';
import * as fromApp from '../../../../app_store/app.store';
import { ScrollHelper } from '@app/helper/scroll-helper';

@Component({
  selector: 'app-subvention-recherche-dossier',
  templateUrl: './subvention-recherche-dossier.component.html',
  styleUrls: ['./subvention-recherche-dossier.component.css']
})
export class SubventionRechercheDossierComponent implements OnInit, OnDestroy,AfterViewChecked {
  // formulaire ihm
  public criteresForm: FormGroup;

  public sousProgrammes: SousProgrammeLovDto[];

  public departements: DepartementLovDto[];

  public etatsDemande: EtatDemandeLovDto[];

  private infoUtilisateur: UtilisateurConnecteDto;

  public collectivites: CollectiviteLovDto[];

  // objet CritereRechercheDossierDto, reflet des données du formulaire
  public criteresDto: CritereRechercheDossierDto;

  public searchTermsPayload;

  // liste des résulats chargés
  public resultatRechercheDossierDtos: ResultatRechercheDossierDto[];

  // Nombre total de resultats trouvés
  public nbTotalResultats: number;

  public isUserUniquementGenerique: boolean;

  public pageSize: number;
  private storeSubscription$: Subscription;
  private savedSearchTerms={} as Actions.SearchTerms;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();
  private scrollHelper : ScrollHelper = new ScrollHelper();

  constructor(
    private rechercheDossierService: RechercheDossierService,
    private referentielService: ReferentielService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private droitService: DroitService,
    private formBuilder: FormBuilder,
    private gestionAffichage: GestionAffichagesChampsService,
    private store: Store<fromApp.AppState>
  ) {}

  ngAfterViewChecked(): void {
    this.scrollHelper.doScroll();

  }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
    this.storeSubscription$.unsubscribe();
  }

  ngOnInit() {
    // récupération de la liste des dossiers subvention depuis le store
    this.storeSubscription$ = this.store.select('searchList').subscribe(
      data => {

        if(data.resultsSubV.listeResultats.length > 0){
          this.savedSearchTerms = data.saveFormSubv;
          this.alimenterResultatFromPageReponse(data.resultsSubV);
        }
          this.criteresDto = data.searchTermsSubv;
      }
    );

    // Initialisation du profil utilisateur
    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur => {
        this.infoUtilisateur = infoUtilisateur;
        this.initialiserIsUserUniquementGenerique(infoUtilisateur);
      }
      );

    // Taille de la page
    this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;

    // Initialisation du formulaire
    this.initCriteresForm();

    // récupération des sousProgrammes
    this.referentielService
      .rechercherTousSousProgrammes$()
      .subscribe(data => (this.sousProgrammes = data));

    // récupération des états des demandes filtrant les demandes de
    this.referentielService
      .rechercherTousEtatsDemandeSubvention$().pipe(
        map(listeEtat =>  {
            listeEtat.forEach(etat => {
              etat.libelle = this.gestionAffichage.recupererEtatLibelle(etat.code)
            });
            return listeEtat;
          }
        )
      )
      .subscribe(data => (this.etatsDemande = data));

    if (!this.isUserUniquementGenerique) {
      // récupération des collectivites
      this.referentielService
        .rechercherToutesCollectivites$()
        .subscribe(data => (this.collectivites = data));

      // récupération des départements
      this.referentielService
        .rechercherTousDepartements$()
        .subscribe(data => (this.departements = data));
    }
  }

  /**
   * Initialise le flag de profil de l'utilisateur.
   */
  private initialiserIsUserUniquementGenerique(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
      infosUtilisateur
    );
  }

  /**
   * Initialise le criteresform.
   *
   */
  private initCriteresForm(): void {
    // initialisation du formulaire
    this.criteresForm = this.formBuilder.group({
      numDossier: new FormControl(this.savedSearchTerms.numDossier),
      anneeProgrammationDossier: new FormControl(this.savedSearchTerms.anneeProgrammationDossier, Validators.pattern('[0-9]*')),
      selectionSousProgramme: new FormControl(this.savedSearchTerms.selectionSousProgramme),
      selectionEtatDemande: new FormControl(this.savedSearchTerms.selectionEtatDemande),
      selectionCollectivite: new FormControl(this.savedSearchTerms.selectionCollectivite),
      selectionDepartement: new FormControl(this.savedSearchTerms.selectionDepartement)
    });

  }

  /**
   * Met à jour l'objet DTO à partir du formulaire.
   *
   */
  private majDtoFromFormulaire(): void {
    // Alimentation du DTO avec les propriétés du form portant le meme nom
    this.criteresDto = this.criteresForm.value as CritereRechercheDossierDto;

    // On alimente l'id du sous-programme à partir de la liste déroulante.
    if (
      this.criteresForm.get('selectionSousProgramme').value !== null &&
      this.criteresForm.get('selectionSousProgramme').value !== '...'
    ) {
      const selectionSousProgramme: SousProgrammeLovDto = this.criteresForm.get(
        'selectionSousProgramme'
      ).value as SousProgrammeLovDto;
      this.criteresDto.idSousProgramme = selectionSousProgramme.id;
    }

    // On alimente le code de l'état de la demande à partir de la liste déroulante.
    if (
      this.criteresForm.get('selectionEtatDemande').value !== null &&
      this.criteresForm.get('selectionEtatDemande').value !== '...'
    ) {
      const selectionEtatDemande: EtatDemandeLovDto = this.criteresForm.get(
        'selectionEtatDemande'
      ).value as EtatDemandeLovDto;
      this.criteresDto.codeEtatDemande = selectionEtatDemande.code;
    }

    // On alimente l'id de la collectivite à partir de la liste déroulante.
    if (
      this.criteresForm.get('selectionCollectivite').value !== null &&
      this.criteresForm.get('selectionCollectivite').value !== '...'
    ) {
      const selectionCollectivite: CollectiviteLovDto = this.criteresForm.get(
        'selectionCollectivite'
      ).value as CollectiviteLovDto;
      this.criteresDto.idCollectivite = selectionCollectivite.id;
    }

     // On alimente l'id du departement à partir de la liste déroulante.
     if (
      this.criteresForm.get('selectionDepartement').value !== null &&
      this.criteresForm.get('selectionDepartement').value !== '...'
    ) {
      const selectionDepartement: DepartementLovDto = this.criteresForm.get(
        'selectionDepartement'
      ).value as DepartementLovDto;
      this.criteresDto.idDepartement = selectionDepartement.id;
    }
  }

  public rechercherDossiers(): void {
    this.messageService.clear();
    // sauvegarde de l'état du formulaire
    this.store.dispatch(new Actions.SaveFormSubv(this.criteresForm.value));

    // mapping des champs du formulaire vers le DTO
    this.majDtoFromFormulaire();

    // On filtre la recherche sur la collectivite active d'un utilisateur generique.
    if (this.isUserUniquementGenerique) {
      this.criteresDto.idCollectivite = this.droitService.getIdCollectivite$().value;
    }

    this.criteresDto.pageDemande = {
      indexPage: 0,
      taillePage: this.pageSize,
      champTri: '',
      desc: false,
      valeurFiltre: ''
    };
    // Sauvegarde des citères de la recherche dans le store
    this.searchTermsPayload = this.criteresForm.value;
    this.store.dispatch(new Actions.SearchSubvention(this.searchTermsPayload));

    // récupération des dossiers recherchés
     this.rechercheDossierService
      .getResultatRechercheDossierDto$(this.criteresDto)
      .subscribe(data => this.alimenterResultatFromPageReponse(data));

  }

  public loadDossiersLazy(event: LazyLoadEvent): void {
    this.messageService.clear();

    // Alimentation des données de pagination (pageDemande)
    this.criteresDto.pageDemande = {
      indexPage: 0,
      taillePage: this.pageSize,
      champTri: '',
      desc: false,
      valeurFiltre: ''
    };
    this.criteresDto.pageDemande.indexPage = Math.trunc(event.first / this.pageSize);
    this.criteresDto.pageDemande.champTri = event.sortField;
    this.criteresDto.pageDemande.desc =
      event.sortOrder !== null && event.sortOrder === -1;

    // récupération des dossiers recherchés

     this.rechercheDossierService
      .getResultatRechercheDossierDto$(this.criteresDto)
      .subscribe(data => this.alimenterResultatFromPageReponse(data));
  }

  private alimenterResultatFromPageReponse(pageReponse: PageReponseDto): void {
    if (pageReponse && pageReponse.listeResultats.length > 0) {
      this.resultatRechercheDossierDtos = pageReponse.listeResultats;
      this.nbTotalResultats = pageReponse.nbTotalResultats;
      // scroll sur la liste des demandes de subvention
      this.scrollHelper.scrollToFirst("scroll");
    } else {
      this.resultatRechercheDossierDtos = null;
      this.nbTotalResultats = 0;
      this.messageManagerService.aucuneDonneeTrouveeMessage();
    }
  }

  /**
   * Annuler : reset du formulaire et vidage du tableau de résultats.
   *
   */
  public annuler(): void {
    this.criteresForm.reset();
    this.resultatRechercheDossierDtos = null;
    this.messageService.clear();
  }

}

import { NgModule } from '@angular/core';


// PrimeNG
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TooltipModule } from 'primeng/tooltip';
import { ResultatDemandeTableauComponent } from './demande-tableau/demande-tableau.component';
import { SubventionDemandeComponent } from './subvention-demande.component';
import { AnomalieModule } from '@component/anomalie/anomalie.module';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SubventionDemandeComponent,
    ResultatDemandeTableauComponent,
  ],
  imports: [
    TooltipModule,
    InputTextareaModule,
    ToggleButtonModule,
    AnomalieModule,
    ProgressSpinnerModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    SubventionDemandeComponent
  ]
})
export class SubventionDemandeModule { }

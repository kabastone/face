import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DemandePaiementSimpleDto } from '@subvention/dto/demande-paiement-simple.dto';
import { DossierSubventionService } from '../service/dossier-subvention.service';

@Component({
  selector: 'sub-dossier-paiement',
  templateUrl: './dossier-paiement.component.html',
  styleUrls: ['./dossier-paiement.component.css']
})
export class DossierPaiementComponent implements OnInit {

  // sujet à souscrire pour la maj de la liste de demande paiement
  private demandePaiementSubject = new Subject<DemandePaiementSimpleDto[]>();

  // Emitter renvoyant un booléen indiquant si une des demandes est un solde.
  @Output() demandePaiementEvent = new EventEmitter<boolean>();

  // observable : liste des demande de paiement
  public listeDemandePaiement$: Observable<DemandePaiementSimpleDto[]> = this.demandePaiementSubject.asObservable();

  // constructeur
  constructor(private route: ActivatedRoute,
    private dossierSubventionService: DossierSubventionService) { }

  // init
  ngOnInit() {
    // récupération id dossier
    const idDossier = +this.route.snapshot.paramMap.get('id');

    // obtention des demandes de paiement
    this.dossierSubventionService.getDemandesPaiement$(idDossier)
    .subscribe((listeDemandes: DemandePaiementSimpleDto[]) => {
      if (listeDemandes && listeDemandes.length > 0) {
        this.demandePaiementSubject.next(listeDemandes);
        if (listeDemandes.every(function(demande: DemandePaiementSimpleDto) {
          return demande.typeDemandePaiement !== 'SOLDE';
        })) {
          this.demandePaiementEvent.emit(true);
        } else {
          this.demandePaiementEvent.emit(false);
        }
      }
    });
  }

  /**
   * Retourne la classe utilisée pour l'affichage de l'icône d'état de la demande.
   *
   * @param demande de subvention
   * @returns classe de l'icône à utiliser
   */
  getClassIcone(demande: DemandePaiementSimpleDto): String {
    // listing des etats possibles
    const sablierNoir: String[] = ['DEMANDEE', 'QUALIFIEE', 'ACCORDEE', 'CONTROLEE',
      'EN_ATTENTE_TRANSFERT', 'EN_COURS_TRANSFERT_1', 'EN_COURS_TRANSFERT_2', 'EN_COURS_TRANSFERT_FIN',
      'TRANSFEREE', 'ANOMALIE_DETECTEE', 'CORRIGEE', 'CERTIFIEE'];
    const alerteOrange: String[] = ['ANOMALIE_SIGNALEE'];
    const checkVert: String[] = ['VERSEE'];
    const croixRouge: String[] = ['REFUSEE'];

    // affichage de la classe en fonction de l'état
    if (sablierNoir.includes(demande.codeEtatDemande)) {
      return 'fas fa-hourglass-half';
    }
    if (alerteOrange.includes(demande.codeEtatDemande)) {
      return 'fas fa-exclamation-triangle text-warning';
    }
    if (checkVert.includes(demande.codeEtatDemande)) {
      return 'fas fa-check text-success';
    }
    if (croixRouge.includes(demande.codeEtatDemande)) {
      return 'fas fa-times text-danger';
    }
  }

}

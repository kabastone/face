import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossierProlongationComponent } from './dossier-prolongation.component';

describe('DossierProlongationComponent', () => {
  let component: DossierProlongationComponent;
  let fixture: ComponentFixture<DossierProlongationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossierProlongationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossierProlongationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

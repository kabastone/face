import { Component, OnDestroy, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { DemandeSubventionDto } from '@subvention/dto/demande-subvention';
import { DemandeSubventionForm } from '@subvention/dto/demande-subvention-form';
import { DossierSubventionLovDto } from '@subvention/dto/dossier-subvention-lov.dto';
import { GestionAffichagesChampsService } from '@subvention/services/gestion-affichages-champs.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil, map, filter, tap } from 'rxjs/operators';
import { DemandeSubventionService } from './services/demande-subvention.service';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { SousProgrammeDto } from '@dotation/dto/sous-programme-dto';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { AnomalieDto } from '@app/dto/anomalie.dto';
import { CustomFileTransferComponent } from '@component/custom-file-transfer/custom-file-transfer.component';



import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'sub-subvention-demande',
  templateUrl: './subvention-demande.component.html',
  styleUrls: ['./subvention-demande.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class SubventionDemandeComponent implements OnInit, OnDestroy {

  showButton: boolean;
  excelExtention: boolean;
  isPdfContent: boolean;
  isExcelContent: boolean;
  headData: any;
  data: any;
  displayModal: boolean;
  desactiverBoutonDemande = false;
  displayRefus: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private droitService: DroitService,
    private formBuilder: FormBuilder,
    private gestionAffichagesChampsService: GestionAffichagesChampsService,
    private demandeSubventionService: DemandeSubventionService,
    private referentielService: ReferentielService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private confirmationService: ConfirmationService,
    private fileSaverService: FileSaverService
  ) {

    // mise à jour des infos utilisateur
    this.droitService.getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        infosUser => {
          if (!infosUser.id) {
            return;
          }
          this.infosUtilisateur = infosUser;
        }
      );

    this.form = gestionAffichagesChampsService.default();
    this.createFormBuilder();
    this.createForm();
  }

  // Mon mock pour le select
  sousProgrammes: SousProgrammeLovDto[];

  // Mon formulaire de critères
  demandeSubventionForm: FormGroup;

  demandeSubvention: DemandeSubventionDto = {} as DemandeSubventionDto;

  displayDemandeSubvention = 'hide-class';

  selectedSousProgramme: SousProgrammeLovDto;

  form: DemandeSubventionForm;
  angForm: FormGroup;

  // infos utilisateur
  public infosUtilisateur: UtilisateurConnecteDto = {} as UtilisateurConnecteDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // id du dossier de subvention (url)
  public idDossier: number = +this.route.snapshot.paramMap.get('idDossierSubvention');

  // id du dossier en cours
  public idDemande: number = +this.route.snapshot.paramMap.get('idDemandeSubvention');

  // formulaire dédié à l'upload de fichiers (association : TypeDocument => fichier)
  public formulaireFichiers: FormData = new FormData();

  // Référence à l'élément message pour le scroll into view.
  @ViewChild('messageAnomalieDemandePaiement') messageAnomalieElement: ElementRef;

  @ViewChildren(CustomFileTransferComponent) viewChildren !: QueryList<CustomFileTransferComponent>;

  // Booléen servant à indiquer si les noms de dossier ont déjà été cherchés.
  private rechercherListeDossiers = true;
  public listeDossier: DossierSubventionLovDto[];
  public mapDocuments: Map<string, number>;
  public mapDocumentsName: Array<string>;

  // tableau des documents supplémentaires
  public documentsSupplementaires: DocumentDto[] = <DocumentDto[]>[];
  // tableau des documents supplémentaires rajoutés en cette session
  public documentsSupplementairesRajoutes: DocumentDto[] = <DocumentDto[]>[];
  // liste de fichiers qui correspondent aux docs complementaires
  public fichiersDocsSuppl: File[] = <File[]>[];
  // liste des id des docs complémentaires à supprimer
  public idsDocsComplSuppr: number[] = [];
  // Enregistre le message selon le message prioritaire à affiché
  public message: string;
  // Enregistre l'état du message pour gérer l'affichage de celui-ci selon le switch
  public etatMessage: String;

  private listeEtatsAvantBatch: string[] = ['DEMANDEE', 'QUALIFIEE', 'CORRIGEE', 'ACCORDEE', 'CONTROLEE', 'EN_ATTENTE_TRANSFERT_MAN', 'EN_COURS_TRANSFERT'];

  displayDialog: boolean;
  yearLabel: number;
  localUrl: any;



  /**
  * A la destruction : on lance le désabonnement de tous les subscribes()
  *
  */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }


  ngOnInit() {
    this.desactiverBoutonDemande = false;
    // récupération de l'année courante pour le cadencement
    this.yearLabel = new Date().getFullYear();
    this.route.params.subscribe(params => {
      this.idDossier = params['idDossierSubvention'];
      this.idDemande = params['idDemandeSubvention'];
    });

    if (this.idDemande) {
      // récupération des infos de la collectivité sélectionnée
      this.recupererDemandeSubvention(true);
    } else if (this.deSoumettreUnProjet()) {
      
      this.droitService.getIdCollectivite$()
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe(
          idCollectivite => {
            this.referentielService
              .rechercherSousProgrammesDeProjet$()
              .pipe(takeUntil(this.ngDestroyed$))
              .subscribe(sousProgrammeDto => {
                this.sousProgrammes = sousProgrammeDto;
                this.majDotationDisponible(idCollectivite);
                this.demandeSubventionForm.controls['sousProgramme'].reset();
              });
          });

        // Création
        this.creerDemandeSubvention();

    } else {

      if (this.idDossier) {
        this.droitService.getIdCollectivite$()
          .pipe(takeUntil(this.ngDestroyed$))
          .subscribe(
            idCollectivite => {
              this.referentielService
                .rechercherSousProgrammesParCollectivite$(+idCollectivite)
                .pipe(takeUntil(this.ngDestroyed$))
                .subscribe(sousProgrammeDto => {
                  this.sousProgrammes = sousProgrammeDto
                  this.majDotationDisponible(idCollectivite);
                });
            });
      } else {
        this.droitService.getIdCollectivite$()
          .pipe(takeUntil(this.ngDestroyed$))
          .subscribe(
            idCollectivite => {
              this.referentielService
                .rechercherSousProgrammesParCollectivite$(+idCollectivite)
                .pipe(takeUntil(this.ngDestroyed$))
                .subscribe(sousProgrammeDto => {
                  this.sousProgrammes = sousProgrammeDto;
                  this.demandeSubventionForm.controls['sousProgramme'].setValue(
                    this.findBySSProgrammeParDescription('' + this.demandeSubventionService.dotationDto.descriptionSousProgramme)
                  );
                  this.majDotationDisponible(idCollectivite);
                });
            });

        this.demandeSubventionForm.controls['sousProgramme'].valueChanges.subscribe(data => {
          if (data) {
            this.selectedSousProgramme = data;
            this.majDotationDisponible();
          }
        });
      }
      
      // Création
      this.creerDemandeSubvention();
    }
  }

  private recupererDemandeSubvention(gererMessage: boolean) {
    this.demandeSubventionService.getDemandeSubventionDto$(this.idDemande)
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.demandeSubvention = data;
        this.idDossier = +data.idDossier;
        if (this.demandeSubvention.idCollectivite) {
          // récupération des sousProgrammes
          this.referentielService
            .rechercherSousProgrammesParCollectivite$(+this.demandeSubvention.idCollectivite)
            .pipe(takeUntil(this.ngDestroyed$))
            .subscribe(sousProgrammesRecu => (this.sousProgrammes = sousProgrammesRecu), err => (this.messageService.clear()));
        }
        this.chargementMetadataForm();
        this.createFormBuilder();
        this.chargementDonneesEcran(data);
        if (gererMessage) {
          this.gererMessages();
          this.hasCadencementValide(+this.demandeSubvention.idCollectivite);
        }
      });
  }

  hasCadencementValide(idCollectivite: number) {
    if ((this.droitService.isMfer(this.infosUtilisateur))) {
      this.demandeSubventionService.estCadencementValide$(idCollectivite)
        .pipe(filter(estValide => !estValide))
        .subscribe(_ => this.lancerMessageCadencementInvalideCollectivite());
    }
  }

  lancerMessageCadencementInvalideCollectivite() {
    if (this.droitService.isMfer(this.infosUtilisateur)) {
      this.messageManagerService.mettreAJourCadencementMFER();
    }
  }

  public modificationStatusDossier(event: any): void {
    if (event) {
      this.demandeSubventionForm.controls['numDossier'].disable();
    } else {
      this.demandeSubventionForm.controls['numDossier'].enable();
      this.demandeSubventionForm.controls['idDossier'].setValue(this.demandeSubvention.idDossier);
      this.demandeSubventionForm.controls['numDossier'].setValue(this.findDossierParId(this.demandeSubvention.idDossier));
    }
  }


  findDossierParId(idDossier: string): string {
    return this.listeDossier.find(x => +x.id === +idDossier).numDossier;
  }

  public onChangeDropdown(event: any): void {
    this.demandeSubventionForm.controls['numDossier'].setValue(event.value.numDossier);
    this.demandeSubventionForm.controls['idDossier'].setValue(event.value.id);
  }

  private chargementMetadataForm(): void {
    this.form = this.gestionAffichagesChampsService.recupererFormulaire(this.demandeSubvention.etat, this.infosUtilisateur.listeRoles);
  }

  public afficherOngletDemandeSubvention(): boolean {
    return this.demandeSubventionForm && this.demandeSubventionForm.get('sousProgramme').value;
  }

  public afficherOngletInstruction(): boolean {
    return this.demandeSubvention && this.demandeSubvention.etat !== 'INITIAL'
      && this.demandeSubventionForm && this.demandeSubventionForm.get('sousProgramme').value
      && (!this.droitService.isUniquementGenerique(this.infosUtilisateur));
  }

  public afficherOngletAnomalie(): boolean {
    return this.demandeSubvention && this.demandeSubvention.etat !== 'INITIAL'
      && this.demandeSubventionForm && this.demandeSubventionForm.get('sousProgramme').value
      && (this.droitService.isMfer(this.infosUtilisateur)
        || this.droitService.isSd7(this.infosUtilisateur)
        || (this.droitService.isAuMoinsGenerique(this.infosUtilisateur) && this.demandeSubvention.etat === 'ANOMALIE_SIGNALEE'));
  }


  public majDotationDisponible(idCollectivite?: number): void {
    idCollectivite = +this.demandeSubvention['idCollectivite'] ? +this.demandeSubvention['idCollectivite'] : this.droitService.getIdCollectivite$().value;
    if (this.selectedSousProgramme && idCollectivite) {
      this.demandeSubventionService.recupererSousProgramme$(this.selectedSousProgramme.id).subscribe((data: SousProgrammeDto) => {
        if (!data.deProjet) {
          this.form.dotationDisponible = this.gestionAffichagesChampsService.VISIBLE;
          this.demandeSubventionService.recupererDotationDisponible$(this.selectedSousProgramme.id, idCollectivite)
            .pipe(takeUntil(this.ngDestroyed$))
            .subscribe(
              dotationDisponible => {
                this.demandeSubventionForm.controls['dotationDisponible'].setValue(dotationDisponible);
              }
            );
        } else {
          this.demandeSubventionForm.controls['dotationDisponible'].setValue(0);
          this.form.dotationDisponible = this.gestionAffichagesChampsService.VISIBLE;
        }
      });
    }
  }

  /**
   * Créer une demande de subvention.
   *
   */
  private creerDemandeSubvention(): void {


    if (this.idDossier) {
      // Lors de la création
      this.demandeSubventionService.initDemandeSubventionDtoAvecDossier$(this.idDossier)
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe(
          data => {
            this.demandeSubvention = data;
            this.chargementMetadataForm();
            this.chargementDonneesEcran(data);
            this.selectedSousProgramme = this.findBySSProgrammeParId(+this.demandeSubvention.idSousProgramme);
            this.majDotationDisponible();
          }
        );
    } else {
      // Lors de la création
      this.demandeSubventionService.initDemandeSubventionDto$()
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe(
          data => {
            this.demandeSubvention = data;
            this.chargementMetadataForm();
            this.chargementDonneesEcran(data);
          }
        );
    }
  }

  private chargementDonneesEcran(demandeSubvention: DemandeSubventionDto): void {
    if (demandeSubvention.idSousProgramme && !demandeSubvention.sousProgramme && this.sousProgrammes) {
      demandeSubvention.sousProgramme = this.findBySSProgrammeParId(+demandeSubvention.idSousProgramme).description;
    }
    if (demandeSubvention.etat) {
      demandeSubvention.etatLibelle = this.gestionAffichagesChampsService.recupererEtatLibelleRole(
        demandeSubvention.etat,
        this.infosUtilisateur.listeRoles
      );
    }
    Object.entries(demandeSubvention).forEach(([key, value]) => {
      if (value != null && this.demandeSubventionForm.get(key) != null) {
        if (key === 'cadencementDto') {
          this.demandeSubventionForm.get(key).patchValue(value)
        } else {
          this.demandeSubventionForm.get(key).setValue(value);
        }

      }
    });
    // récupération de la liste des documents
    this.mapDocuments = new Map<string, number>();
    // initialisation d'une array qui contiendra les noms des documents à télécharger (titre à afficher). Par défaut 'introuvable'
    this.mapDocumentsName = new Array<string>(5);
    for (let i = 0; i < this.mapDocumentsName.length; i++) {
      this.mapDocumentsName[i] = 'Document introuvable';
    }
    this.documentsSupplementaires = <DocumentDto[]>[];
    this.documentsSupplementairesRajoutes = <DocumentDto[]>[];
    this.fichiersDocsSuppl = <File[]>[];
    this.idsDocsComplSuppr = [];
    this.formulaireFichiers = new FormData();
    Object.values(demandeSubvention.documents).forEach(document => {
      if (document.codeTypeDocument === 'DOC_COMPLEMENTAIRE_SUBVENTION') {
        this.documentsSupplementaires.push(document);
      } else {
        this.mapDocuments.set(document.codeTypeDocument, document.id);
        // stockage des noms des fichiers dans une array pour affichage sur les titres des buttons de téléchargement
        switch (document.codeTypeDocument) {
          case 'ETAT_PREVISIONNEL_PDF':
            this.mapDocumentsName[0] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'ETAT_PREVISIONNEL_TABLEUR':
            this.mapDocumentsName[1] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'FICHE_SIRET':
            this.mapDocumentsName[3] = document.nomFichierSansExtension + '.' + document.extensionFichier;
            break;
          case 'DECISION_ATTRIBUTIVE_SUBV':
            this.mapDocumentsName[4] = document.nomFichierSansExtension + '.' + document.extensionFichier;
        }
      }
    });
    if (this.sousProgrammes && +this.demandeSubvention.idSousProgramme) {
      this.selectedSousProgramme = this.findBySSProgrammeParId(+this.demandeSubvention.idSousProgramme);
    }

    if (this.rechercherListeDossiers && this.demandeSubvention.idCollectivite && this.droitService.isMfer(this.infosUtilisateur)) {
      this.demandeSubventionService.recupererListeDossiers(+this.demandeSubvention.idDotationCollectivite).
        pipe(map(listeDossier => listeDossier.filter((dossier: DossierSubventionLovDto) => (+dossier.id !== +this.demandeSubvention.idDossier)))).
        subscribe(
          data => {
            this.listeDossier = data;
            this.rechercherListeDossiers = false;
          });
    }
    this.majDotationDisponible();
  }

  // Recherche un sous-programme par sa description venant du serveur.
  private findBySSProgrammeParDescription(description: string): SousProgrammeLovDto {
    return this.sousProgrammes.find(x => x.description === description);
  }

  // Recherche un sous-programme par son id.
  private findBySSProgrammeParId(idSSP: number): SousProgrammeLovDto {
    return this.sousProgrammes.find(x => x.id === idSSP);
  }

  public showSousProgrammeDropDown() {
    return (!this.idDemande && !this.idDossier)
      || (this.idDemande && this.demandeSubvention && !this.demandeSubvention.sousProgramme);
  }

  private getEtatMessage(): String {
    this.message = '';
    let estComplementaireEtAvantBatch = this.estDemandeComplementaireEtAvantBatch();

    if (estComplementaireEtAvantBatch && (this.droitService.isMferInstructeur(this.infosUtilisateur))) {
      this.message = 'MFER-INSTRUCTEUR';
    }
    if (estComplementaireEtAvantBatch && (this.droitService.isMferAdmin(this.infosUtilisateur))) {
      this.message = 'MFER-ADMIN';
    }
    if (estComplementaireEtAvantBatch && (this.droitService.isSd7Admin(this.infosUtilisateur))) {
      this.message = 'SD7-ADMIN';
    }
    if (estComplementaireEtAvantBatch && (this.droitService.isSd7Instructeur(this.infosUtilisateur))) {
      this.message = 'SD7-INSTRUCTEUR';
    }
    if (this.demandeSubvention.etat === 'ANOMALIE_SIGNALEE' && this.aAuMoinsUneAnomalieACorriger(this.demandeSubvention.anomalies) && (this.droitService.isUniquementGenerique(this.infosUtilisateur))) {
      this.message = 'ANOMALIE';
    }
    return this.message;
  }

  private estDemandeComplementaireEtAvantBatch() {
    return (this.demandeSubvention.demandeComplementaire)
      && (this.listeEtatsAvantBatch.includes(this.demandeSubvention.etat));
  }

  public estMfer() {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMfer(utilisateurConnecte);
  }

  private gererMessages() {
    this.etatMessage = this.getEtatMessage();
    switch (this.etatMessage) {
      case 'ANOMALIE':
        this.gererAffichageMessageAnomalies();
        break;
      case 'MFER-INSTRUCTEUR':
        this.gererAffichageMessageDemandeComplementaireCorrigeeOuDemandeeMferInstructeur();
        break;
      case 'MFER-ADMIN':
      case 'SD7-ADMIN':
      case 'SD7-INSTRUCTEUR':
        this.gererAffichageMessageDemandeComplementaireCorrigeeOuDemandeeMferAdminOuSD7AdminOuSD7Instructeur();
        break;
    }
  }

  private gererAffichageMessageAnomalies() {
    this.messageService.clear();
    this.messageManagerService.isAuMoinsUneAnomalieACorrigerMessage();
  }

  private gererAffichageMessageDemandeComplementaireCorrigeeOuDemandeeMferInstructeur() {
    this.messageService.clear();
    this.messageManagerService.isDemandeComplementaireCorrigeeOuDemandeeMessageMferInstructeur();
  }

  private gererAffichageMessageDemandeComplementaireCorrigeeOuDemandeeMferAdminOuSD7AdminOuSD7Instructeur() {
    this.messageService.clear();
    this.messageManagerService.isDemandeComplementaireCorrigeeOuDemandeeMessageMferAdminOuSD7AdminOuSD7Instructeur();
  }

  public genererDemandeSubventionPdf() {
    this.demandeSubventionService.genererDemandeSubventionPdfParId(this.idDemande)
      .subscribe(response => {
        this.fileSaverService.telechargerFichier(response);
      });
  }

  /**
  * S'il existe au moins une anomalie "A corriger".
  *
  * @param anomalies - liste des anomalies
  * @returns true ou false
  */
  public aAuMoinsUneAnomalieACorriger(anomalies: AnomalieDto[]): boolean {
    return anomalies.some((anomalie) => anomalie.corrigee === false);
  }

  private createFormBuilder(): void {
    this.demandeSubventionForm = this.formBuilder.group({
      /* DOTATION */
      sousProgramme: new FormControl(''),
      dotationDisponible: new FormControl(''),
      /* Demande de Subvention */
      dateJour: new FormControl(''),
      montantHTTravauxEligibles: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      tauxAideFacePercent: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      plafondAideDemandee: new FormControl(''),
      etatPrevisionnelPDF: new FormControl(''),
      etatPrevisionnelTableur: new FormControl(''),
      documentComplementaire: new FormControl(''),
      message: new FormControl(''),

      //Cadencement
      cadencementDto: this.formBuilder.group({
        montantAnnee1: new FormControl(
          0, Validators.compose([Validators.required])),
        montantAnnee2: new FormControl(
          0, Validators.compose([Validators.required])),
        montantAnnee3: new FormControl(
          0, Validators.compose([Validators.required])),
        montantAnnee4: new FormControl(
          0, Validators.compose([Validators.required])),
        montantAnneeProl: new FormControl('0'),
      }),

      /* Instruction */
      etat: new FormControl(''),
      idDossier: new FormControl(''),
      numDossier: new FormControl(''),
      demandeComplementaire: new FormControl(''),
      listeDossierExistant: new FormControl(''),
      motifRefus: new FormControl(''),
      decisionAttributive: new FormControl(''),
      ficheSiret: new FormControl(''),
      decisionAttributiveSignee: new FormControl(''),
      /* Anomalie */
      dateAnomalie: new FormControl(''),
      typeAnomalie: new FormControl(''),
      problematiqueAnomalie: new FormControl(''),
      reponseAnomalie: new FormControl(''),
    });
  }

  private majObjetFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      this.demandeSubvention[key] = value;
    });
  }
  /**
   * Ajout d'un fichier à uploader.
   *
   * @param typeDocument type du document envoyé
   * @param event evenement contenant le fichier envoyé
   */
  public ajoutFichierUploader(typeDocument: string, event: any) {
    const fichier: File = event.target.files[0];
    // ajout du fichier
    if (event.target.files && fichier) {
      this.formulaireFichiers.append(typeDocument, fichier);
    }
  }
  private sendSuccessMessage(): void {
    // message de succès
    this.messageManagerService.successMessage();
  }


  initFormData(avecFormPrincipal?: boolean) {

    // init.
    const formulaireAvecFichiers = new FormData();

    if (this.demandeSubvention.id) {
      formulaireAvecFichiers.append('id', String(this.demandeSubvention.id));
    }
    if (this.demandeSubvention.chorusNumLigneLegacy) {
      formulaireAvecFichiers.append('chorusNumLigneLegacy', this.demandeSubvention.chorusNumLigneLegacy);
    }
    if (this.demandeSubvention.dateAccord) {
      formulaireAvecFichiers.append('dateAccord', this.demandeSubvention.dateAccord);
    }

    if (this.demandeSubvention.etat == 'INITIAL') {
      formulaireAvecFichiers.append('estValide', "false");
    }

    if (avecFormPrincipal) {
      // mapping des valeurs du formulaire
      Object.keys(this.demandeSubventionForm.value).forEach(key => {
        let value = this.demandeSubventionForm.get(key).value;
        if (value && value.code) {
          // s'il s'agit d'un menu déroulant
          // on prend juste la value et non l'objet en entier
          value = value.code;
        }
        if (value !== null) {
          if (key === 'cadencementDto') {
            formulaireAvecFichiers.append('montantAnnee1', value.montantAnnee1)
            formulaireAvecFichiers.append('montantAnnee2', value.montantAnnee2)
            formulaireAvecFichiers.append('montantAnnee3', value.montantAnnee3)
            formulaireAvecFichiers.append('montantAnnee4', value.montantAnnee4)
            formulaireAvecFichiers.append('montantAnneeProl', value.montantAnneeProl)
          }

          formulaireAvecFichiers.append(key, value);
        }
      });
      // mapping champs speciaux ou cachés
      formulaireAvecFichiers.append('numDossier', String(this.demandeSubventionForm.get('numDossier').value));
      formulaireAvecFichiers.append('idDossier', String(this.demandeSubventionForm.controls['idDossier'].value));
      formulaireAvecFichiers.append('version', String(this.demandeSubvention.version));
      formulaireAvecFichiers.append('etat', this.demandeSubvention.etat);
      formulaireAvecFichiers.append('dateJour', String(this.demandeSubvention.dateJour));
    } else {
      formulaireAvecFichiers.append('idDossier', String(this.demandeSubvention.idDossier));
    }

    // ajout des fichiers supplémentaires au formulaire de fichiers
    this.documentsSupplementairesRajoutes.forEach((document, index) => {
      if (document.id === null) {
        this.formulaireFichiers.append('DOC_COMPLEMENTAIRE_SUBVENTION', this.fichiersDocsSuppl[index]);
      }
    });

    // mapping des fichiers uploadés
    for (const fichier of this.formulaireFichiers.entries()) {
      formulaireAvecFichiers.append(fichier[0], fichier[1]);
    }

    // mapping de la liste des id des docs complémentaires à supprimer
    formulaireAvecFichiers.append('idsDocsComplSuppr', String(this.idsDocsComplSuppr));

    // mapping des id
    if (!this.demandeSubvention.idCollectivite) {
      this.demandeSubvention.idCollectivite = '' + this.droitService.getIdCollectivite$().value;
    }
    if (this.demandeSubvention.idCollectivite === 'undefined') {
      this.demandeSubvention.idCollectivite = null;
    }
    if (this.demandeSubvention.idCollectivite) {
      formulaireAvecFichiers.append('idCollectivite', String(this.demandeSubvention.idCollectivite));
    }
    if (!this.demandeSubvention.idSousProgramme) {
      formulaireAvecFichiers.delete('sousProgramme');
      formulaireAvecFichiers.append('idSousProgramme', String(this.demandeSubventionForm.get('sousProgramme').value.id));
    } else {
      formulaireAvecFichiers.append('idSousProgramme', this.demandeSubvention.idSousProgramme);
    }

    return formulaireAvecFichiers;
  }


  /* BUTTONS / ACTIONS */

  existeAutreDossier() {
    return this.listeDossier?.length > 0;
  }

  private preparerPourRequete(): void {

    const version = this.demandeSubvention.version;
    this.majObjetFormulaire(this.demandeSubventionForm.value);
    if (!this.demandeSubvention.idCollectivite) {
      this.demandeSubvention.idCollectivite = '' + this.droitService.getIdCollectivite$().value;
    }
    if (this.demandeSubvention.idCollectivite === 'undefined') {
      this.demandeSubvention.idCollectivite = null;
    }
    this.demandeSubvention.version = version;
  }

  public demander(): void {
    this.desactiverBoutonDemande = true;
    if (this.demandeSubvention.id) {
      console.log('[TODO] CAS NON FONCTIONNEL');
    } else {
      if (!this.controlMontantCadencement(
        this.demandeSubventionForm.get('cadencementDto.montantAnnee1').value,
        this.demandeSubventionForm.get('cadencementDto.montantAnnee2').value,
        this.demandeSubventionForm.get('cadencementDto.montantAnnee3').value,
        this.demandeSubventionForm.get('cadencementDto.montantAnnee4').value,
        this.getPlafondDemande()
      )) {

        // envoyer un message d'erreur

        this.messageManagerService.erreurMontantCadencement();
        this.activerOuDesactiverBoutonDemande()
      } else {
        // enregistrement d'une nouvelle demande de subvention
        this.enregistrerCreationDemandeSubvention();
      }
    }
  }

  controlMontantCadencement(mnt1: number,
    mnt2: number,
    mnt3: number,
    mnt4: number,
    mntDisponible: number): boolean {
    if (((+mnt1) + (+mnt2) + (+mnt3) + (+mnt4)) !== +mntDisponible) {
      return false;
    }
    return true;
  }

  private enregistrerCreationDemandeSubvention() {
    const formulaireAvecFichiers = this.initFormData(true);
    this.demandeSubventionService.creerDemandeSubvention(formulaireAvecFichiers)
      .pipe(
        tap(
          _ => this.activerOuDesactiverBoutonDemande()
        ),
        filter(data => !!data.id)
      ).subscribe(
        data => {
          this.router.navigateByUrl('/subvention/demande/' + +data.id).then(
            _ => this.messageManagerService.enregistrerCreationDemandeSubventionMessage()
          );
        }
      );
  }

  public activerOuDesactiverBoutonDemande() {
    this.desactiverBoutonDemande = !this.desactiverBoutonDemande;
  }

  public getPlafondDemande(): number {
    if (!this.demandeSubventionForm.get('tauxAideFacePercent').value
      || this.demandeSubventionForm.get('tauxAideFacePercent').value === 0) {
      return 0;
    }

    return (this.demandeSubventionForm.get('montantHTTravauxEligibles').value
      * this.demandeSubventionForm.get('tauxAideFacePercent').value)
      / 100;
  }

  public qualifier(): void {
    const formulaireAvecFichiers = this.initFormData(true);
    this.demandeSubventionService.qualifierDemandeSubvention(String(this.idDemande), formulaireAvecFichiers).subscribe(
      data => {
        if (data.id) {
          this.reload(+this.demandeSubvention.id);
          this.sendSuccessMessage();
        } else {
          this.recupererDemandeSubvention(false);
        }
      });
  }

  public accorder(): void {
    this.demandeSubventionService.accorderDemandeSubvention(this.demandeSubvention.id).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public controler(): void {
    this.demandeSubventionService.controlerDemandeSubvention(this.demandeSubvention.id).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public transferer(): void {
    this.demandeSubventionService.transfererDemandeSubvention(this.demandeSubvention.id).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public attribuer(): void {
    const formulaireAvecFichiers = this.initFormData(true);
    this.demandeSubventionService.attribuerDemandeSubvention(String(this.idDemande), formulaireAvecFichiers).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public detecterAnomalie(): void {
    this.preparerPourRequete();
    this.demandeSubventionService.detecterAnomalieDemandeSubvention(this.demandeSubvention).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public signalerAnomalie(): void {
    this.preparerPourRequete();
    this.demandeSubventionService.signalerAnomalieDemandeSubvention(this.demandeSubvention).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public showMotifRefusModal(): void {
    this.displayRefus = true;
  }

  public refuser(): void {
    this.displayRefus = false;
    this.preparerPourRequete();
    this.demandeSubventionService.refuserDemandeSubvention(this.demandeSubvention).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      });
  }

  public enregistrer(): void {

    const formulaireAvecFichiers = this.initFormData(true);
    if (!this.controlMontantCadencement(
      +formulaireAvecFichiers.get('montantAnnee1'),
      +formulaireAvecFichiers.get('montantAnnee2'),
      +formulaireAvecFichiers.get('montantAnnee3'),
      +formulaireAvecFichiers.get('montantAnnee4'),
      this.getPlafondDemande()
    )) {
      this.messageManagerService.erreurMontantCadencement();
      return;
    }

    this.demandeSubventionService.corrigerDemandeSubvention(formulaireAvecFichiers).subscribe(
      data => {
        this.demandeSubvention = data;
        this.idDossier = +data.idDossier;
        this.chargementDonneesEcran(data);
        this.showMessageAnomalies();
      });
  }

  public annuler(): void {
    if (this.demandeSubvention.id) {
      this.reload(+this.demandeSubvention.id);
    } else {
      this.demandeSubventionForm.reset();
      this.viewChildren.forEach(customFileTransfer => customFileTransfer.reset());
      this.createFormBuilder();
      this.chargementMetadataForm();
      this.demandeSubventionForm.controls['sousProgramme'].setValue(this.selectedSousProgramme);
      this.chargementDonneesEcran(this.demandeSubvention);
    }
  }

  public reload(id: number): void {
    this.messageService.clear();
    this.idDemande = id;
    this.recupererDemandeSubvention(false);
  }

  public estEtatAnomalieDetectee(): boolean {
    return this.demandeSubvention.etat === 'ANOMALIE_DETECTEE';
  }

  public estEtatRefuseOuRejetTaux(): boolean {
    return this.demandeSubvention.etat === 'REFUSEE' || this.demandeSubvention.etat === 'REJET_TAUX';
  }

  public estEtatDemandeOuQualifie(): boolean {
    return this.demandeSubvention.etat === 'DEMANDEE' || this.demandeSubvention.etat === 'QUALIFIEE';
  }

  private showMessageAnomalies() {
    this.messageManagerService.penserRepondreAnomaliesMessage();
    this.messageAnomalieElement.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  /**
 * Telecharger un document.
 */
  public telechargerDocumentSubvention(typeDocument: string) {
    const idDocument = this.mapDocuments.get(typeDocument);

    if (!idDocument) {
      // message d'avertissement
      this.messageManagerService.documentIntrouvableMessage();

      return;
    }

    // téléchargement par id
    this.demandeSubventionService.telechargerDocument(idDocument).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });

  }

  public isDemandeExistante() {
    if (this.idDemande) {
      return true;
    }
    return false;
  }

  public hasIdDossier(): boolean {
    if (this.idDossier === null || !this.idDossier || this.idDossier === 0) {
      return false;
    }
    return true;
  }

  retour = function () {
    this.router.navigateByUrl('/subvention/dossier/' + this.idDossier);
  };

  /**
 * Ajout d'un document complémentaire à uploader.
 *
 * @param event evenement contenant le fichier envoyé
 */
  public ajoutDocComplementaire(event: any) {
    const fichier: File = event.target.files[0];
    this.showButton = true;
    const nouveauDocument: DocumentDto = {
      id: null,
      codeTypeDocument: 'DOC_COMPLEMENTAIRE_SUBVENTION',
      nomFichierSansExtension: fichier.name,
      extensionFichier: null,
      dateTeleversement: null
    };
    this.documentsSupplementaires.push(nouveauDocument);
    this.documentsSupplementairesRajoutes.push(nouveauDocument);
    this.fichiersDocsSuppl.push(fichier);
  }

  /**
   * Téléchargement d'un fichier via son index.
   *
   * @param index - index du document à télécharger
   */
  public telechargerDocumentComplementaire(index: number) {

    const document = this.documentsSupplementaires[index];

    // téléchargement par id
    this.demandeSubventionService.telechargerDocument(document.id).subscribe(response => {
      let isFileSaverSupported = false;
      try {
        isFileSaverSupported = !!new Blob;
      } catch (e) { }
      if (isFileSaverSupported) {
        const filename = response.headers.get('Content-Disposition').split('=')[1];
        const blob = new Blob([response.body], { type: response.headers.get('content-type') });
        saveAs(blob, filename);
      }
    });
  }

  /**
   * Suppression d'un fichier via son index.
   *
   * @param index - index du document à supprimer
   */
  public supprimerDocComplementaire(index: number) {
    // alimentation de la liste des id docs à supprimer
    const idDocSuppr: number = this.documentsSupplementaires[index].id;
    if (idDocSuppr) {
      this.idsDocsComplSuppr.push(idDocSuppr);
    }
    const indexFichiersRajoutes = this.documentsSupplementairesRajoutes.findIndex(document => this.documentsSupplementaires[index].nomFichierSansExtension === document.nomFichierSansExtension);

    this.documentsSupplementaires.splice(index, 1);
    this.fichiersDocsSuppl.splice(indexFichiersRajoutes, 1);
    this.documentsSupplementairesRajoutes.slice(indexFichiersRajoutes, 1);
  }

  public deSoumettreUnProjet(): boolean {
    if (this.router.url.includes('/projet')) {
      return true;
    } else {
      return false;
    }
  }

  private createForm() {
    this.angForm = this.formBuilder.group({
      tauxDemandeModif: new FormControl(),
    });
  }

  modifierTaux(): void {
    this.displayDialog = true;
  }

  fermerModal(): void {
    this.displayDialog = false;
  }

  save() {
    this.displayDialog = false;
    let taux = this.angForm.get('tauxDemandeModif').value;

    this.demandeSubventionService.envoyerMailTauxDifferent(this.demandeSubvention, taux).subscribe(
      data => {
        this.reload(+this.demandeSubvention.id);
        this.sendSuccessMessage();
      }
    );
  }

  isExcelExtention(fichier: File): boolean {
    // l'une des extensions correspond
    let extensionValide = false;
    const excelExtension = ['.ods', '.xls', '.xlsx'];


    Object.values(excelExtension).forEach(ext => {
      if (fichier.name.endsWith(ext)) {
        extensionValide = true;
        return;
      }
    });

    // l'extension à été trouvée ?
    if (extensionValide) {
      return true;
    }

    return false;

  }
  showModal(index) {
    const fichier: File = this.fichiersDocsSuppl[index];

    if (this.isExcelExtention(fichier)) {
      this.previewExcelFile(fichier);
      this.isPdfContent = false;
      this.isExcelContent = true;
    }
    else {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
        this.isPdfContent = true;
        this.isExcelContent = false;
      }
      reader.readAsDataURL(fichier);
    }
    this.displayModal = true;
  }

  private previewExcelFile(fichier) {

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* lire workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* récupérer première page */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* enregistrer données */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1, raw: false, range: 10 }));

      this.headData = this.data[0];
      this.data = this.data.slice(1); // supprimer le premier enregistrement

      const ws2: XLSX.WorkSheet = wb.Sheets[wb.SheetNames[1]];
      this.readDataSheet(ws2, 10);
    };

    reader.readAsBinaryString(fichier);
  }

  private readDataSheet(ws: XLSX.WorkSheet, startRow: number) {
    /* save data */
    let datas = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1, raw: false, range: startRow }));
    let headDatas = datas[0];
    datas = datas.slice(1); // supprimer le premier enregistrement

    for (let i = 0; i < this.data.length; i++) {
      this.data[i][this.headData.length] = datas.filter(x => x[12] == this.data[i][0])
    }
  }
}

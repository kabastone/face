import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionRechercheDossierComponent } from './subvention-recherche-dossier.component';

describe('SubventionRechercheDossierComponent', () => {
  let component: SubventionRechercheDossierComponent;
  let fixture: ComponentFixture<SubventionRechercheDossierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubventionRechercheDossierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionRechercheDossierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

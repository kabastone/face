import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { CadencementTableDto } from '@subvention/dto/cadencement-table.dto';
import { CadencementDto } from '@subvention/dto/cadencement.dto';
import { CadencementTableMapDto } from '@subvention/dto/cadencement-table-map.dto';
import { CadencencementService } from '@subvention/services/cadencencement.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'app-subvention-cadencement',
  templateUrl: './subvention-cadencement.component.html',
  styleUrls: ['./subvention-cadencement.component.css']
})
export class SubventionCadencementComponent implements OnInit, OnDestroy {

  annees = [] as number[];

  public cadencementTableDtoArray  = [] as CadencementTableDto[];

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  clonedCadencements: { [s: number]: CadencementDto; } = {};

  cadencement1: CadencementDto[];
  cadencements: CadencementDto[];

  public idCollectivite: number;

  public isUserUniquementGenerique: boolean;

  public cadencementMap = {} as Record<string, CadencementTableMapDto>;

  public anneesSet = new Set<number>();
  public coursEdition = null;
  public rowGroupMetadata = {};
  public rowGroupMetadata2 = {};

  private deProjet: boolean;

  constructor(
    private droitService: DroitService,
    private cadencementService: CadencencementService,
    private router: Router,
    private messageManagerService: MessageManagerService,
    private messageService: MessageService
  ) {
      if (this.router.url.includes('/projet')) {
        this.deProjet = true;
      } else {
        this.deProjet = false;
      }
    }

  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit(): void {

    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur =>
        this.initialiserIsUserUniquementGenerique(infoUtilisateur)
      );

      this.droitService.getIdCollectivite$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.majDtoFromFormulaire();
      });
  }

  private majDtoFromFormulaire() {

    this.messageService.clear();
    this.cadencementMap = {} as Record<string, CadencementTableMapDto>;
    this.cadencementTableDtoArray = [] as CadencementTableDto[];
    this.annees = [] as number[];
    this.idCollectivite = this.droitService.getIdCollectivite$().value;

    this.cadencementService
    .recupererCadencementsPourCollectivite$(this.idCollectivite, this.deProjet)
    .subscribe(data => {
      this.cadencementTableDtoArray = data;
      this.cadencementTableDtoArray.sort(
        (a, b) => a.sousProgramme.codeDomaineFonctionnel.localeCompare(b.sousProgramme.codeDomaineFonctionnel)
        ).forEach(
        cadencementTable => {
          this.annees.push(cadencementTable.anneeProgrammation);
          this.remplirMapAvecData(cadencementTable);
        }
      );

      this.anneesSet = new Set<number> (this.annees);
      this.annees = Array.from(this.anneesSet);
      this.annees.sort((a,b) => a-b);
      this.updateRowGroupMetaData(Math.max(...this.annees));
      let anneeMin = (Math.max(...this.annees)-4);
      if(this.cadencementTableDtoArray.filter(cad => cad.anneeProgrammation === anneeMin && cad.hasProlongation).length === 0
        && this.annees.find(annee => annee == anneeMin)) {
        this.annees.shift();
      }

    });
  }

  public maxAnneeSet(){
    return Math.max(...this.anneesSet.values())
  }

  private remplirMapAvecData(cadencementTable: CadencementTableDto) {
    if (!this.cadencementMap[cadencementTable.anneeProgrammation]) {
      this.cadencementMap[cadencementTable.anneeProgrammation] = {} as CadencementTableMapDto;
      this.cadencementMap[cadencementTable.anneeProgrammation].hasProlongation = cadencementTable.hasProlongation;
      this.cadencementMap[cadencementTable.anneeProgrammation].cadencements = [cadencementTable, ];
    } else {
      this.cadencementMap[cadencementTable.anneeProgrammation].hasProlongation =
        this.cadencementMap[cadencementTable.anneeProgrammation].hasProlongation || cadencementTable.hasProlongation;
        this.cadencementMap[cadencementTable.anneeProgrammation].cadencements.push(cadencementTable);
    }
  }

  private initialiserIsUserUniquementGenerique(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
      infosUtilisateur
    );
  }

  onRowEditInit(cadencementDto: CadencementTableDto) {
    this.coursEdition = true;
    this.clonedCadencements[cadencementDto.cadencement.id] = {...cadencementDto.cadencement};
  }
  onRowEditCancel(cadencementDto: CadencementTableDto, index: number, annee: number) {
    this.coursEdition = null;
    this.cadencementMap[annee].cadencements[index].cadencement = this.clonedCadencements[cadencementDto.cadencement.id];
    delete this.clonedCadencements[cadencementDto.cadencement.id];
    this.messageService.clear();
  }
  onRowEditSave(cadencementDto: CadencementTableDto, index: number, annee: number) {
    this.coursEdition = null;
    this.cadencementService.updateCadencement(cadencementDto.cadencement,
       this.clonedCadencements[this.cadencementMap[annee].cadencements[index].cadencement.id])
    .subscribe( cadencement => {
      if (cadencement !== this.clonedCadencements[cadencementDto.cadencement.id]) {
        this.messageManagerService.modificationEnregistree();
        this.cadencementMap[annee].cadencements[index].cadencement = cadencement;
      } else {
        this.messageManagerService.erreurMontantCadencement();
        this.cadencementMap[annee].cadencements[index].cadencement = this.clonedCadencements[cadencementDto.cadencement.id];
      }
    });
  }

  onSort(annee: number) {
    this.updateRowGroupMetaData(annee);
  }

  updateRowGroupMetaData(annee: number) {
    this.rowGroupMetadata = {};
    this.rowGroupMetadata2 = {};
    if (this.cadencementMap[annee]) {
        for (let i = 0; i < this.cadencementMap[annee].cadencements.length; i++) {
            let cadencementTableDto = this.cadencementMap[annee].cadencements[i];
            let brand = cadencementTableDto.sousProgramme.abreviation;
            if (i === 0) {
                this.rowGroupMetadata[brand] = { index: 0, size: 1 };
            } else {
                let previousRowData = this.cadencementMap[annee].cadencements[i - 1];
                let previousRowGroup = previousRowData.sousProgramme.abreviation;
                if (brand === previousRowGroup) {
                    this.rowGroupMetadata[brand].size++;
                } else {
                    this.rowGroupMetadata[brand] = { index: i, size: 1 };
                }
            }
        }
        for (let i = 0; i < this.cadencementMap[annee].cadencements.length; i++) {
          let cadencementTableDto = this.cadencementMap[annee].cadencements[i];
          let brand = cadencementTableDto.numDossier.toString();
          if (i === 0) {
              this.rowGroupMetadata2[brand] = { index: 0, size: 1 };
          } else {
              let previousRowData = this.cadencementMap[annee].cadencements[i - 1];
              let previousRowGroup = previousRowData.numDossier.toString();
              if (brand === previousRowGroup) {
                  this.rowGroupMetadata2[brand].size++;
              } else {
                  this.rowGroupMetadata2[brand] = { index: i, size: 1 };
              }
          }
      }
    }
  }
  hasCadencementValide(annee: number): boolean {
    for (let i = 0; i < this.cadencementMap[annee].cadencements.length; i++) {
      const cadencementDto = this.cadencementMap[annee].cadencements[i];
      if (!cadencementDto.cadencement.estValide) {
        return false;
      }
    }
    return true;
  }

  celluleNonModifiable(annee: number) {
    const dateActuelle = new Date().getFullYear();
    if (annee < dateActuelle) {
       return 'silver';
    }
  }

  celluleEstNonModifiable(annee: number) {
    const dateActuelle = new Date().getFullYear();
    if (annee < dateActuelle) {
       return true;
    }
  }
}

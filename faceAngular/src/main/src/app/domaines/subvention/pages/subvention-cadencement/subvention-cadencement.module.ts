import { NgModule } from '@angular/core';

// interne
import { SubventionCadencementComponent } from './subvention-cadencement.component';

// PrimeNG

import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from '@app/share/shared.module';
import { SubventionRouteModule } from '@subvention/subvention.routes';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    SubventionCadencementComponent
  ],
  imports: [
    TooltipModule,
    SharedModule,
    RouterModule
  ],
  exports: []
})
export class SubventionCadencementModule { }

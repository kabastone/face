import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionResultatDemandeTableauComponent } from './subvention-demande-tableau.component';

describe('SubventionResultatDemandeTableauComponent', () => {
  let component: SubventionResultatDemandeTableauComponent;
  let fixture: ComponentFixture<SubventionResultatDemandeTableauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubventionResultatDemandeTableauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionResultatDemandeTableauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

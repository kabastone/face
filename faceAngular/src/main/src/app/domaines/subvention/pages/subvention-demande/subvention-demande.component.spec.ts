import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionDemandeComponent } from './subvention-demande.component';

describe('SubventionDemandeComponent', () => {
  let component: SubventionDemandeComponent;
  let fixture: ComponentFixture<SubventionDemandeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubventionDemandeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionDemandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

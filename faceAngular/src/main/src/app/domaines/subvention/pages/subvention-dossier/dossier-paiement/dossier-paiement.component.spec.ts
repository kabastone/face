import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossierPaiementComponent } from './dossier-paiement.component';

describe('DossierPaiementComponent', () => {
  let component: DossierPaiementComponent;
  let fixture: ComponentFixture<DossierPaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossierPaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossierPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

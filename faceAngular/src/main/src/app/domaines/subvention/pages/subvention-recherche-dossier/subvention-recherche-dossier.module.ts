import { NgModule } from '@angular/core';

// interne
import { SubventionRechercheDossierComponent } from './subvention-recherche-dossier.component';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [
    SubventionRechercheDossierComponent
  ],
  imports: [
    TooltipModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    SubventionRechercheDossierComponent
  ]
})
export class SubventionRechercheDossierModule { }

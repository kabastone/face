import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DossierSubventionDto } from '@subvention/dto/dossier-subvention.dto';
import { DemandeSubventionSimpleDto } from '@subvention/dto/demande-subvention-simple.dto';
import { DemandePaiementSimpleDto } from '@subvention/dto/demande-paiement-simple.dto';
import { DemandeProlongationSimpleDto } from '@subvention/dto/demande-prolongation-simple.dto';

@Injectable({
  providedIn: 'root'
})
export class DossierSubventionService {

  // onglet prolongation affiche ou pas
  public ongletProlongationAffiche = true;

  // constructeur
  constructor(
    private httpService: HttpService
  ) { }

  /**
   * Recherche d'un dossier subvention par id.
   *
   * @param id du dossier à obtenir
   * @returns le dossier de subvention
   */
  getDossierSubventionParId$(id: number): Observable<DossierSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/${id}/rechercher`);
  }

  /**
   * Recherche des demandes de subvention via l'id du dossier.
   *
   * @param idDossier id du dossier en cours
   * @returns la liste des demandes de subvention
   */
  getDemandesSubvention$(idDossier: number): Observable<DemandeSubventionSimpleDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/${idDossier}/demandes-subvention/rechercher`, false);
  }

  /**
   * Recherche des demandes de paiement via l'id du dossier.
   *
   * @param idDossier id du dossier en cours
   * @returns la liste des demandes de paiement
   */
  getDemandesPaiement$(idDossier: number): Observable<DemandePaiementSimpleDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/${idDossier}/demandes-paiement/rechercher`, false);
  }

    /**
   * Recherche des demandes de prolongation via l'id du dossier.
   *
   * @param idDossier id du dossier en cours
   * @returns la liste des demandes de paiement
   */
  getDemandesProlongation$(idDossier: number): Observable<DemandeProlongationSimpleDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/${idDossier}/demandes-prolongation/rechercher`, false);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionDossierComponent } from './subvention-dossier.component';

describe('SubventionDossierComponent', () => {
  let component: SubventionDossierComponent;
  let fixture: ComponentFixture<SubventionDossierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubventionDossierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionDossierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DemandeProlongationSimpleDto } from '@subvention/dto/demande-prolongation-simple.dto';
import { Observable, Subject } from 'rxjs';
import { DossierSubventionService } from '../service/dossier-subvention.service';

@Component({
  selector: 'sub-dossier-prolongation',
  templateUrl: './dossier-prolongation.component.html',
  styleUrls: ['./dossier-prolongation.component.css']
})
export class DossierProlongationComponent implements OnInit {

  // sujet à souscrire pour la maj de la liste de demande prolongation
  private demandeProlongationSubject = new Subject<DemandeProlongationSimpleDto[]>();

  // observable : liste des demande de prolongation
  public listeDemandeProlongation$: Observable<DemandeProlongationSimpleDto[]> = this.demandeProlongationSubject.asObservable();

  // constructeur
  constructor(private route: ActivatedRoute,
    private dossierSubventionService: DossierSubventionService) { }

  // init
  ngOnInit() {
    // récupération id dossier
    const idDossier = +this.route.snapshot.paramMap.get('id');

    // obtention des demandes de prolongation
    this.dossierSubventionService.getDemandesProlongation$(idDossier)
    .subscribe((listeDemandes: DemandeProlongationSimpleDto[]) => {
      if (listeDemandes && listeDemandes.length > 0) {
        this.dossierSubventionService.ongletProlongationAffiche = true;
        this.demandeProlongationSubject.next(listeDemandes);
      } else {
        this.dossierSubventionService.ongletProlongationAffiche = false;
      }
    });
  }

  /**
   * Retourne la classe utilisée pour l'affichage de l'icône d'état de la demande.
   *
   * @param demande de subvention
   * @returns classe de l'icône à utiliser
   */
  getClassIcone(demande: DemandeProlongationSimpleDto): String {
    // listing des etats possibles
    const sablierNoir: String[] = ['DEMANDEE', 'ACCORDEE', 'INCOHERENCE_CHORUS'];
    const alerteOrange: String[] = [];
    const checkVert: String[] = ['VALIDEE'];
    const croixRouge: String[] = ['REFUSEE'];

    // affichage de la classe en fonction de l'état
    if (sablierNoir.includes(demande.codeEtatDemande)) {
      return 'fas fa-hourglass-half';
    }
    if (alerteOrange.includes(demande.codeEtatDemande)) {
      return 'fas fa-exclamation-triangle text-warning';
    }
    if (checkVert.includes(demande.codeEtatDemande)) {
      return 'fas fa-check text-success';
    }
    if (croixRouge.includes(demande.codeEtatDemande)) {
      return 'fas fa-times text-danger';
    }
  }
}

import { TestBed } from '@angular/core/testing';
import { DossierSubventionService } from './dossier-subvention.service';

describe('DossierSubventionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DossierSubventionService = TestBed.get(DossierSubventionService);
    expect(service).toBeTruthy();
  });
});

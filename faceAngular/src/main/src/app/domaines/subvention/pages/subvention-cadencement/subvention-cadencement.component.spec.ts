import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionCadencementComponent } from './subvention-cadencement.component';

describe('SubventionCadencementComponent', () => {
  let component: SubventionCadencementComponent;
  let fixture: ComponentFixture<SubventionCadencementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubventionCadencementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionCadencementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

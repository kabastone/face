import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { Location } from '@angular/common';
import { DroitService } from '@app/services/authentification/droit.service';
import { DossierSubventionDto } from '@subvention/dto/dossier-subvention.dto';
import { Observable, pipe, Subject } from 'rxjs';
import { DossierSubventionService } from './service/dossier-subvention.service';
import { RouteStoreService } from '@subvention/services/route-store.service';
import { MontantEurosPipe } from '@app/custom-pipes/montant-euros.pipe';

@Component({
  selector: 'app-subvention-dossier',
  templateUrl: './subvention-dossier.component.html',
  styleUrls: ['./subvention-dossier.component.css']
})
export class SubventionDossierComponent implements OnInit, OnDestroy {
  // dossier à afficher
  public objetFormulaire: DossierSubventionDto = {} as DossierSubventionDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // id du dossier en cours
  public idDossier: number = +this.route.snapshot.paramMap.get('id');

  private aucunSolde = true;
  public subventionDossier: Observable<DossierSubventionDto>;

  public dataChart: any;
  public options = {
    scales: {
      xAxes: [{
        stacked: true,
        ticks: {}
      }],
      yAxes: [{
        stacked: true,
        gridLines: {
          offsetGridLines: true
        },
        ticks: {}
      }]
    },
    tooltips: {}
  };

  previousRoute: string;


  // constructeur
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private dossierSubventionService: DossierSubventionService,
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private routeStoreService: RouteStoreService,
    private pipeEuros: MontantEurosPipe,
  ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  // init.
  ngOnInit() {

    this.route.params.subscribe(params => this.idDossier = +params.id);
    // chargement du dossier
    this.dossierSubventionService
      .getDossierSubventionParId$(this.idDossier)
      .subscribe((dossier: DossierSubventionDto) => {
        // mise a jour de l'objet du formulaire
        this.majObjetFormulaire(dossier);
        this.majDataChart(dossier);
      });
    /* this.store.select('subventionList').subscribe(data => {
      this.store.dispatch(new Actions.SearchSubvention(data.searchTerms));
    }) */
  }

  /**
   * Initialisation du formulaire.
   *
   * @returns instance du formulaire
   */
  initFormulaire(): FormGroup {
    return this.formBuilder.group({
      numDossier: new FormControl(''),
      etatDossier: new FormControl(''),
      annee: new FormControl(''),
      descriptionSousProgramme: new FormControl(''),
      engagementJuridique: new FormControl(''),
      dateEcheanceLancement: new FormControl(''),
      dateEcheanceAchevement: new FormControl('')
    });
  }

  /**
   * Met à jour l'objet du formulaire via un DTO.
   *
   * @param dataDto - dto en entrée
   */
  private majObjetFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      this.objetFormulaire[key] = value;
    });
  }

  private majDataChart(dossier: DossierSubventionDto): void {
    this.dataChart = {
      labels: [""],
      datasets: [
        {
          label: 'Aide versée',
          data: [dossier.aideVerseeCalcule],
          backgroundColor: ["#1b81d1"],
        },

        {
          label: 'Aide demandée',
          data: [(dossier.plafondAide - dossier.aideVerseeCalcule - dossier.resteDisponible)],
          backgroundColor: ["#8dcdff"],
        },

        {
          label: 'Reste disponible',
          data: [dossier.resteDisponible]
        }],
      backgroundColor: ["#6c757d"],
    };

    let pipeEuros = this.pipeEuros.transform;

    let callbackTooltip = function (tooltipItem, data) {
      var label = ' ' + data.datasets[tooltipItem.datasetIndex].label;

      if (label) {
        label += ': ';
      }

      label += pipeEuros(tooltipItem.xLabel);
      return label;
    }

    let callbackTauxConsommation = function () {
      return 'Taux de consommation: ' + dossier.tauxConsommationCalcule + ' %';
    }

    let callbackLabelTick = function (value, index) {

      var label = pipeEuros(value);
      return label + ' (' + (((index) / 4) * 100) + ' %)';

    }

    this.options.scales.xAxes[0].ticks = {
      callback: callbackLabelTick,
      suggestedMax: dossier.plafondAide,
      max: dossier.plafondAide,
      stepSize: dossier.plafondAide / 4,
    };

    this.options.tooltips = {
      titleFontSize: 14,
      titleMarginBottom: 12,
      bodyFontSize: 14,
      bodySpacing: 10,
      position: 'nearest',
      callbacks: {
        title: callbackTauxConsommation,
        label: callbackTooltip
      }
    }
  }

  retour = function () {

    this.router.navigateByUrl(this.routeStoreService.getPreviousRoute());
  };


  /**
   * Indique si l'utilisateur possède uniquement le rôle générique.
   *
   * @returns true ou false
   */
  public isUniquementGenerique(): boolean {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isUniquementGenerique(utilisateurConnecte);
  }

  /**
   * Affichage du bouton de subvention ou non.
   *
   * @returns true ou false
   */
  private hasResteAconsommer(): boolean {
    // pas de reste à consommer, pas de paiement
    if (!this.objetFormulaire.resteDisponible) {
      return false;
    }
    const resteDisponible = this.objetFormulaire.resteDisponible;
    // invisible : lorsque le plafond d'aide est égale à 0
    if (resteDisponible <= 0) {
      return false;
    }

    return true;
  }

  /**
   * Affichage du bouton de prolongation ou non.
   *
   * @returns true ou false
   */
  public affichageBoutonProlongation(): boolean {
    // pas de dossier, pas de bouton
    if (!this.objetFormulaire) {
      return false;
    }

    // pas de date d'échéance, pas de prolongation
    if (!this.objetFormulaire.dateEcheanceAchevement) {
      return false;
    }

    // Test état du dossier
    if (this.objetFormulaire.etatDossier !== 'OUVERT') {
      return false;
    }

    // Test période
    // init.
    const dateActuelle = new Date();
    // this.objetFormulaire.dateEcheanceAchevement n'est au format date mais string
    const dateForm = this.objetFormulaire.dateEcheanceAchevement;
    const dateEcheance = new Date(parseInt(dateForm.toString().substr(6, 4), 10),
      parseInt(dateForm.toString().substr(3, 2), 10) - 1,
      parseInt(dateForm.toString().substr(0, 2), 10));

    // date échéance : 6 mois avant
    const echeanceSixMoisAvant = new Date(
      new Date(dateEcheance).setMonth(dateEcheance.getMonth() - 6)
    );

    // déjà, le dossier doit être 'ouvert' +
    // la date actuelle doit être entre : -6 mois avant la date d'échéance
    // et ne pas avoir non plus avoir dépassé cette date d'échéance
    if (dateActuelle < echeanceSixMoisAvant || dateActuelle > dateEcheance) {
      return false;
    }

    return this.hasResteAconsommer();
  }

  /**
 * Change le status de l'attribut "aucunSolde" selon qu'il y ait une demande de paiement avec solde ou non.
 *
 * @returns true ou false
 */
  public demandePaiementSoldeExiste($event: boolean) {
    this.aucunSolde = $event;
  }

  /**
   * Affichage du bouton de subvention ou non.
   *
   * @returns true ou false
   */
  public isDemandeSubventionVisible(): boolean {
    // pas de dossier, pas de bouton
    if (!this.objetFormulaire) {
      return false;
    }

    // init.
    const anneeActuelle: number = new Date().getFullYear();
    const anneeDossier: number = this.objetFormulaire.annee;

    // si l'on est sur la même année
    if (anneeActuelle === anneeDossier) {
      return true;
    }

    // pas de boutton visible
    return false;
  }

  /**
   * Affichage du bouton de paiement ou non.
   *
   * @returns true ou false
   */
  public isDemandePaiementVisible(): boolean {
    // pas de dossier, pas de bouton
    if (!this.objetFormulaire) {
      return false;
    }


    // 1- Test de l'échéance
    // pas de date d'échéance, pas de paiement
    if (!this.objetFormulaire.dateEcheanceAchevement) {
      return false;
    }
    // init.
    const anneeActuelle: Date = new Date();
    const dateForm = this.objetFormulaire.dateEcheanceAchevement;
    const dateEcheance = new Date(parseInt(dateForm.toString().substr(6, 4), 10),
      parseInt(dateForm.toString().substr(3, 2), 10) - 1,
      parseInt(dateForm.toString().substr(0, 2), 10));
    // invisible : lorsque l'on a pas atteint la date d'échéance
    if (anneeActuelle > dateEcheance) {
      return false;
    }

    // Si une demande de solde, pas de demande supplémentaire possible
    return this.hasResteAconsommer() && this.aucunSolde;
  }

  /**
   * Indique si l'onglet doit être affiche ou non.
   *
   * @returns true ou false
   */
  public isOngletProlongationAffiche(): boolean {
    return this.dossierSubventionService.ongletProlongationAffiche;
  }

  isCollectiviteOuvert(): boolean {
    return this.droitService.isCollectiviteOuvert();
  }


}

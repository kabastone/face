
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';
import { ChartModule } from 'primeng/chart';

// Interne
import { SubventionDossierComponent } from './subvention-dossier.component';
import { DossierSubventionComponent } from './dossier-subvention/dossier-subvention.component';
import { DossierPaiementComponent } from './dossier-paiement/dossier-paiement.component';
import { DossierProlongationComponent } from './dossier-prolongation/dossier-prolongation.component';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [
    SubventionDossierComponent,
    DossierSubventionComponent,
    DossierPaiementComponent,
    DossierProlongationComponent],
  imports: [
    ChartModule,
    TooltipModule,
    SharedModule,
    RouterModule,
  ]
})
export class SubventionDossierModule { }

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DemandeSubventionDto } from '@subvention/dto/demande-subvention';
import { DossierSubventionLovDto } from '@subvention/dto/dossier-subvention-lov.dto';
import { DotationCollectiviteTdbDto } from '@app/domaines/tableau-de-bord/dto/dotation-collectivite-tdb.dto';

/**
 * Service de Demande de Subvention
 *
 * @export
 * @class DemandeSubventionService
 */
@Injectable({
  providedIn: 'root'
})
export class DemandeSubventionService {
  // Nécessaire pour passer la dotation collectivité depuis le TdB
  // Vers la page de création d'une demande de subvention.
  public dotationDto: DotationCollectiviteTdbDto;

  constructor(
    private httpService: HttpService
  ) { }

  /**
   * Récupérer une DemandeSubventionDto$
   *
   * @param id
   * @returns DemandeSubventionDto
   */
  getDemandeSubventionDto$(id: number): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/${id}/rechercher`);
  }

  recupererListeDossiers(id: number): Observable<DossierSubventionLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/recuperer-dossiers/${id}`);
  }

  recupererDotationDisponible$(idSousProgramme: number, idCollectivite: number): Observable<number> {
    return this.httpService.envoyerRequeteGet(
      `/subvention/demande/sous-programme/${idSousProgramme}/collectivite/${idCollectivite}/rechercher`
      );
  }

  recupererSousProgramme$(id: number) {
    return this.httpService.envoyerRequeteGet(`/referentiel/sous-programme/${id}/rechercher`);
  }

  /**
   * Récupérer une DemandeSubventionDto initialisée
   *
   * @returns DemandeSubventionDto
   */
  initDemandeSubventionDto$(): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/initialiser`);
  }

  /**
   * Récupérer une DemandeSubventionDto initialisée avec un dossier déjà créé
   *
   * @returns DemandeSubventionDto
   */
  initDemandeSubventionDtoAvecDossier$(idDossier: number): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/dossier/${idDossier}/initialiser`);
  }

  /**
   * Génére une demande de subvention pdf par id
   *
   * @param id
   * @returns
   */
  genererDemandeSubventionPdfParId(id: number) {
        return this.httpService.envoyerRequeteGetPourDownload(`/subvention/demande/${id}/generation`);
  }


  /**
   * Initialise une nouvelle demande de subvention.
   *
   * @param idDossier - id du dossier subvention
   * @returns formulaire de demande de paiement
   */
  initNouvelleDemandeSubvention$(): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/initialiser`);
  }

  /**
   * Créer une demande de subvention.
   *
   * @param demandeSubvention
   * @returns
   */
  creerDemandeSubvention(data: FormData): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/subvention/demande/creer`, data, true, data);
  }

  /**
   * Qualifier une demande de subvention.
   *
   * @param id
   * @returns
   */
  qualifierDemandeSubvention(id: string, data: FormData): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/subvention/demande/${id}/qualifier`, data, true, {} as DemandeSubventionDto);
  }

  /**
   * Accorder une demande de subvention.
   *
   * @param id
   * @returns
   */
  accorderDemandeSubvention(id: string): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/${id}/accorder`);
  }

  /**
   * Controler une demande de subvention.
   *
   * @param id
   * @returns
   */
  controlerDemandeSubvention(id: string): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/${id}/controler`);
  }

  /**
   * Transférer une demande de subvention.
   *
   * @param id
   * @returns
   */
  transfererDemandeSubvention(id: string): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequeteGet(`/subvention/demande/${id}/transferer`);
  }

  /**
   * Refuser une demande de subvention.
   *
   * @param demandeSubvention
   * @returns
   */
  refuserDemandeSubvention(demandeSubvention: DemandeSubventionDto): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePost(`/subvention/demande/${demandeSubvention.id}/refuser`, demandeSubvention);
  }

  /**
   * Signaler une anomalie dans une demande de subvention.
   *
   * @param demandeSubvention
   * @returns
   */
  signalerAnomalieDemandeSubvention(demandeSubvention: DemandeSubventionDto): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePost(`/subvention/demande/${demandeSubvention.id}/anomalies/signaler`, demandeSubvention);
  }

  /**
   * Détecter une anomalie dans une demande de subvention.
   *
   * @param demandeSubvention
   * @returns
   */
  detecterAnomalieDemandeSubvention(demandeSubvention: DemandeSubventionDto): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePost(`/subvention/demande/${demandeSubvention.id}/anomalies/detecter`, demandeSubvention);
  }

  /**
   * Attribuer une demande de subvention.
   *
   * @param id
   * @returns
   */
  attribuerDemandeSubvention(id: string, data: FormData): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/subvention/demande/${id}/attribuer`, data);
  }

  /**
   * Corriger une demande de subvention.
   *
   * @param demandeSubvention
   */
  corrigerDemandeSubvention(data: FormData): Observable<DemandeSubventionDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/subvention/demande/corriger`, data);
  }

    /**
   * Téléchargement d'un document.
   * @param typeDocument - type du document a télécharger
   */
  telechargerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/document/${idDocument}/telecharger`);
  }

  envoyerMailTauxDifferent(demandeSubvention: DemandeSubventionDto, tauxDemande: number) {
    return this.httpService.envoyerRequetePost(`/subvention/demande/${demandeSubvention.id}/rejet-taux`, tauxDemande)
  }
  /**
   * Vérifie si la collectivite a tout ses cadencements valides ou non
   * @param idCollectivite
   */
  estCadencementValide$(idCollectivite: number): Observable<boolean> {
    return this.httpService.envoyerRequeteGet(`/subvention/cadencement/collectivite/${idCollectivite}/estValide`);
  }
}

import { Component, OnInit } from '@angular/core';
import { DOCUMENTCOMPLEMENTAIRE } from '@subvention/dto/mock-list-subvention-document-complementaire';

@Component({
  selector: 'sub-demande-tableau',
  templateUrl: './demande-tableau.component.html',
  styleUrls: ['./demande-tableau.component.css']
})
export class ResultatDemandeTableauComponent implements OnInit {
// Mon mock
  docSups= DOCUMENTCOMPLEMENTAIRE;

// Mon fichier upload
  uploadedFiles: any[] = [];

  constructor() { }

  onUpload(event) {
    for(let file of event.files) {
        this.uploadedFiles.push(file);
    }
  }
  ngOnInit() {
  }

}

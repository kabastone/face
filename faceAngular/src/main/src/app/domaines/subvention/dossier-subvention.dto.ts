export interface DossierSubventionDto {
  numDossier: string;
  etatDossier: string;
  annee: number;
  descriptionSousProgramme: string;
  engagementJuridique: string;
  dateEcheanceLancement: Date;
  dateEcheanceAchevement: Date;

  /* Département et collectivité */
  nomLongCollectivite: string;
  codeDepartement: string;
}

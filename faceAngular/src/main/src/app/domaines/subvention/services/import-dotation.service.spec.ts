import { TestBed } from '@angular/core/testing';

import { ImportDotationService } from './import-dotation.service';

describe('ImportDotationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImportDotationService = TestBed.get(ImportDotationService);
    expect(service).toBeTruthy();
  });
});

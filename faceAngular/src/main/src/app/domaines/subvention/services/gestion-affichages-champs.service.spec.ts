import { TestBed } from '@angular/core/testing';

import { GestionAffichagesChampsService } from './gestion-affichages-champs.service';

describe('GestionAffichagesChampsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestionAffichagesChampsService = TestBed.get(GestionAffichagesChampsService);
    expect(service).toBeTruthy();
  });
});

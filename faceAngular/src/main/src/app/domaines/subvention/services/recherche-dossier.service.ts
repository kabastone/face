import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { CritereRechercheDossierDto } from '@subvention/dto/critere-recherche-dossier.dto';
import { PageReponseDto } from '@app/dto/page-reponse.dto';


@Injectable({
  providedIn: 'root'
})
export class RechercheDossierService {

  constructor( private httpService: HttpService) { }

   /**
   * Recherche les dossiers correspondant aux criteres donnés.
   *
   * @param criteresRecherche - les criteres de recherche
   * @returns La page reponse qui contient un tableau de resultats ResultatRechercheDossierDto
   */
  getResultatRechercheDossierDto$(criteresRecherche: CritereRechercheDossierDto): Observable<PageReponseDto> {
    return this.httpService.envoyerRequetePost(`/subvention/dossier/criteres/rechercher`, criteresRecherche);
  }
}

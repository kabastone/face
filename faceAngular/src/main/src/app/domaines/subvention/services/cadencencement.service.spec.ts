import { TestBed } from '@angular/core/testing';

import { CadencencementService } from './cadencencement.service';

describe('CadencencementService', () => {
  let service: CadencencementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CadencencementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

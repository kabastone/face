import { Injectable } from '@angular/core';
import { DemandeSubventionForm } from '@subvention/dto/demande-subvention-form';
import { MetadataForm } from '@app/dto/metadata-form';

@Injectable({
  providedIn: 'root'
})
export class GestionAffichagesChampsService {

  constructor() { }

  public readonly EDITABLE: MetadataForm = { visible: true, editable: true };
  public readonly VISIBLE: MetadataForm = { visible: true, editable: false };
  public readonly NONE: MetadataForm = { visible: false, editable: false };

  public default(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.EDITABLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.EDITABLE;
    form.tauxAideFacePercent = this.EDITABLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.EDITABLE;
    form.documentComplementaire = this.EDITABLE;
    form.message = this.EDITABLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;
    // Cadencement
    form.montantAnnee1 = this.EDITABLE;
    form.montantAnnee2 = this.EDITABLE;
    form.montantAnnee3 = this.EDITABLE;
    form.montantAnnee4 = this.EDITABLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.EDITABLE;
    form.btnDemander = this.EDITABLE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;

    /* Instruction */
    form.etat = this.NONE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnEnregistrer = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.NONE;
    form.typeAnomalie = this.NONE;
    form.problematiqueAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;
    form.btnCorrectionAnomalie = this.NONE;

    return form;
  }

  public recupererFormulaire(etat: string, roles: string[]): DemandeSubventionForm {
    const nouvelEtat: string = this.definirEtatSelonRole(etat, roles);
    return this.recupererFormulaireSelonEtat(nouvelEtat);
  }

  public recupererEtatLibelle(etat: string) {
    switch (etat) {
      case 'EN_ATTENTE_TRANSFERT_MAN':
        return 'En attente de transfert manuel';
      case 'EN_ATTENTE_TRANSFERT':
        return 'En attente de transfert';
      case 'EN_COURS_TRANSFERT':
        return 'En cours de transfert';
      case 'TRANSFEREE':
        return 'Transférée';
      case 'INCOHERENCE_CHORUS':
        return 'En incohérence avec CHORUS';
      case 'CLOTUREE':
        return 'Cloturée';
      case 'ANOMALIE_DETECTEE':
        return 'Détectée en anomalie';
      case 'DEMANDEE':
        return 'Demandée';
      case 'CORRIGEE':
        return 'Corrigée';
      case 'APPROUVEE':
        return 'Approuvée';
      case 'QUALIFIEE':
        return 'Qualifiée';
      case 'CONTROLEE':
        return 'Contrôlée';
      case 'ACCORDEE':
        return 'Accordée';
      case 'INITIAL':
        return 'Initial';
      case 'ANOMALIE_SIGNALEE':
        return 'Signalée en anomalie';
      case 'REFUSEE':
        return 'Refusée';
      case 'REJET_TAUX':
          return 'Rejetée pour modification de taux';
      case 'ATTRIBUEE':
        return 'Attribuée';
      case 'EN_INSTRUCTION':
      default:
        return 'En instruction';
    }
  }

  public recupererEtatLibelleRole(etat: string, roles: string[]) {
    const nouvelEtat: string = this.definirEtatSelonRole(etat, roles);
    return this.recupererEtatLibelle(nouvelEtat);
  }

  private definirEtatSelonRole(etat: string, roles: string[]) {
    let etatAccessible = false;

    if (roles.includes('MFER_INSTRUCTEUR')) {
      switch (etat) {
        case 'ANOMALIE_DETECTEE':
        case 'DEMANDEE':
        case 'CORRIGEE':
        case 'REJET_TAUX':
        case 'REFUSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('MFER_RESPONSABLE')) {
      switch (etat) {
        case 'ANOMALIE_DETECTEE':
        case 'APPROUVEE':
        case 'QUALIFIEE':
        case 'REJET_TAUX':
        case 'REFUSEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('SD7_INSTRUCTEUR')) {
      switch (etat) {
        case 'ACCORDEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('SD7_RESPONSABLE')) {
      switch (etat) {
        case 'ACCORDEE':
        case 'CONTROLEE':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.length === 1 && roles.includes('GENERIQUE')) {
      switch (etat) {
        case 'INITIAL':
        case 'ANOMALIE_SIGNALEE':
        case 'REFUSEE':
        case 'REJET_TAUX':
        case 'ATTRIBUEE':
        case 'REFUSEE':
          etatAccessible = true;
      }
    }

    if (etatAccessible) {
      return etat;
    }

    return 'EN_INSTRUCTION';
  }

  private recupererFormulaireSelonEtat(etat: string): DemandeSubventionForm {
    switch (etat) {
      case 'ANOMALIE_SIGNALEE': {
        return this.getFormForAnomalieSignalee();
      }
      case 'INITIAL': {
        return this.getFormForInitial();
      }
      case 'DEMANDEE': {
        return this.getFormForDemandee();
      }
      case 'QUALIFIEE': {
        return this.getFormForQualifiee();
      }
      case 'ACCORDEE': {
        return this.getFormForAccordee();
      }
      case 'CONTROLEE': {
        return this.getFormForControlee();
      }
      case 'EN_ATTENTE_TRANSFERT_MAN': {
          return this.getFormForEnAttenteTransfertMan();
      }
      case 'EN_ATTENTE_TRANSFERT': {
          return this.getFormForEnAttenteTransfert();
      }
      case 'EN_COURS_TRANSFERT': {
        return this.getFormForEnCoursTransfert();
      }
      case 'TRANSFEREE': {
        return this.getFormForTransferee();
      }
      case 'APPROUVEE': {
        return this.getFormForApprouvee();
      }
      case 'INCOHERENCE_CHORUS': {
        return this.getFormForIncoherenceChorus();
      }
      case 'ANOMALIE_DETECTEE': {
        return this.getFormForAnomalieDetectee();
      }
      case 'CORRIGEE': {
        return this.getFormForCorrigee();
      }
      case 'REFUSEE': {
        return this.getFormForRefusee();
      }
      case 'REJET_TAUX': {
        return this.getFormForRejetTaux();
      }
      case 'ATTRIBUEE': {
        return this.getFormForAttribuee();
      }
      case 'EN_INSTRUCTION': {
        return this.getFormForEnInstruction();
      }
    }

    return null;
  } // recupererFormulaireSelonEtat

  // Etat Initial
  private getFormForInitial(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.EDITABLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.EDITABLE;
    form.tauxAideFacePercent = this.EDITABLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.EDITABLE;
    form.documentComplementaire = this.EDITABLE;
    form.message = this.EDITABLE;
    form.ficheSiretOngletDemandeSubvention = this.EDITABLE;

    // Cadencement
    form.montantAnnee1 = this.EDITABLE;
    form.montantAnnee2 = this.EDITABLE;
    form.montantAnnee3 = this.EDITABLE;
    form.montantAnnee4 = this.EDITABLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.EDITABLE;
    form.btnDemander = this.EDITABLE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.NONE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.NONE;
    form.typeAnomalie = this.NONE;
    form.problematiqueAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;

    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForInitial

   // Etat AnomalieSignalee
   private getFormForAnomalieSignalee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.EDITABLE;
    form.tauxAideFacePercent = this.EDITABLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.EDITABLE;
    form.documentComplementaire = this.EDITABLE;
    form.message = this.EDITABLE;
    form.ficheSiretOngletDemandeSubvention = this.EDITABLE;
    // cadencement
    form.montantAnnee1 = this.EDITABLE;
    form.montantAnnee2 = this.EDITABLE;
    form.montantAnnee3 = this.EDITABLE;
    form.montantAnnee4 = this.EDITABLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.EDITABLE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.EDITABLE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.EDITABLE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForAnomalieSignalee

  // ETAT Demandee
  private getFormForDemandee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    // cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.EDITABLE;
    form.demandeComplementaire = this.EDITABLE;
    form.listeDossierExistant = this.EDITABLE;
    form.motifRefus = this.EDITABLE;
    form.ficheSiret = this.EDITABLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.EDITABLE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.EDITABLE;
    form.btnRefuser = this.EDITABLE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.EDITABLE;

    return form;
  } // getFormForDemandee

  // ETAT Qualifiee
  private getFormForQualifiee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    // Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.EDITABLE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.EDITABLE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.EDITABLE;
    form.btnRefuser = this.EDITABLE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.EDITABLE;

    return form;
  } // getFormForQualifiee

  // ETAT Accordee
  private getFormForAccordee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    // Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.EDITABLE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.EDITABLE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForAccordee

  // ETAT Controlee
  private getFormForControlee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    // Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.EDITABLE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.EDITABLE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.EDITABLE;
    form.btnRefuser = this.EDITABLE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForControlee

  // Etat EnAttenteTransfertMan
  private getFormForEnAttenteTransfertMan():  DemandeSubventionForm {
      const form: DemandeSubventionForm = {} as DemandeSubventionForm;

      /* DOTATION */
      form.sousProgramme = this.VISIBLE;
      form.dotationDisponible = this.VISIBLE;

      /* Demande de Subvention */
      form.dateJour = this.VISIBLE;
      form.montantHTTravauxEligibles = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
      form.plafondAideDemandee = this.VISIBLE;
      form.etatPrevisionnel = this.VISIBLE;

      form.documentComplementaire = this.VISIBLE;
      form.message = this.VISIBLE;
      form.ficheSiretOngletDemandeSubvention = this.NONE;

      //Cadencement
      form.montantAnnee1 = this.VISIBLE;
      form.montantAnnee2 = this.VISIBLE;
      form.montantAnnee3 = this.VISIBLE;
      form.montantAnnee4 = this.VISIBLE;
      form.montantAnneeProl = this.NONE;

      form.btnAnnuler = this.NONE;
      form.btnDemander = this.NONE;
      form.btnRepondre = this.NONE;
      form.btnRenoncer = this.NONE;
      form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

      /* Instruction */
      form.etat = this.VISIBLE;
      form.numDossier = this.VISIBLE;
      form.demandeComplementaire = this.NONE;
      form.listeDossierExistant = this.NONE;
      form.motifRefus = this.NONE;
      form.ficheSiret = this.VISIBLE;
      form.decisionAttributiveSignee = this.NONE;

      form.btnQualifier = this.NONE;
      form.btnAccorder = this.NONE;
      form.btnControler = this.NONE;
      form.btnTransferer = this.NONE;
      form.btnAttribuer = this.NONE;
      form.btnDetecterAnomalie = this.NONE;
      form.btnRefuser = this.NONE;
      form.btnSignalerAnomalie = this.NONE;
      form.btnGenererDecisionAttributive = this.NONE;

      /* Anomalie */
      form.btnAjouterAnomalie = this.NONE;
      form.dateAnomalie = this.VISIBLE;
      form.typeAnomalie = this.VISIBLE;
      form.problematiqueAnomalie = this.VISIBLE;
      form.reponseAnomalie = this.VISIBLE;

      form.btnCorrectionAnomalie = this.VISIBLE;

      return form;
  } // getFormForEnAttenteTransfertMan

  // Etat EnAttenteTransfert
  private getFormForEnAttenteTransfert(): DemandeSubventionForm {
      const form: DemandeSubventionForm = {} as DemandeSubventionForm;

      /* DOTATION */
      form.sousProgramme = this.VISIBLE;
      form.dotationDisponible = this.VISIBLE;

      /* Demande de Subvention */
      form.dateJour = this.VISIBLE;
      form.montantHTTravauxEligibles = this.VISIBLE;
      form.tauxAideFacePercent = this.VISIBLE;
      form.plafondAideDemandee = this.VISIBLE;
      form.etatPrevisionnel = this.VISIBLE;

      form.documentComplementaire = this.VISIBLE;
      form.message = this.VISIBLE;
      form.ficheSiretOngletDemandeSubvention = this.NONE;

      //Cadencement
      form.montantAnnee1 = this.VISIBLE;
      form.montantAnnee2 = this.VISIBLE;
      form.montantAnnee3 = this.VISIBLE;
      form.montantAnnee4 = this.VISIBLE;
      form.montantAnneeProl = this.NONE;

      form.btnAnnuler = this.NONE;
      form.btnDemander = this.NONE;
      form.btnRepondre = this.NONE;
      form.btnRenoncer = this.NONE;
      form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

      /* Instruction */
      form.etat = this.VISIBLE;
      form.numDossier = this.VISIBLE;
      form.demandeComplementaire = this.NONE;
      form.listeDossierExistant = this.NONE;
      form.motifRefus = this.NONE;
      form.ficheSiret = this.VISIBLE;
      form.decisionAttributiveSignee = this.NONE;

      form.btnQualifier = this.NONE;
      form.btnAccorder = this.NONE;
      form.btnControler = this.NONE;
      form.btnTransferer = this.NONE;
      form.btnAttribuer = this.NONE;
      form.btnDetecterAnomalie = this.NONE;
      form.btnRefuser = this.NONE;
      form.btnSignalerAnomalie = this.NONE;
      form.btnGenererDecisionAttributive = this.NONE;

      /* Anomalie */
      form.btnAjouterAnomalie = this.NONE;
      form.dateAnomalie = this.VISIBLE;
      form.typeAnomalie = this.VISIBLE;
      form.problematiqueAnomalie = this.VISIBLE;
      form.reponseAnomalie = this.VISIBLE;

      form.btnCorrectionAnomalie = this.VISIBLE;

      return form;
  } // getFormForEnAttenteTransfertMan

  // ETAT EnCoursTransfert
  private getFormForEnCoursTransfert(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.VISIBLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForEnCoursTransfert

  // Etat Transferee
  private getFormForTransferee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.VISIBLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForTransferee

  // ETAT Approuvee
  private getFormForApprouvee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.EDITABLE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.EDITABLE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.EDITABLE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.NONE;
    form.typeAnomalie = this.NONE;
    form.problematiqueAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;

    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForApprouvee

  // ETAT IncoherenceChorus
  private getFormForIncoherenceChorus(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.VISIBLE;
    form.decisionAttributiveSignee = this.VISIBLE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.VISIBLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForIncoherenceChorus

  // ETAT AnomalieDetectee
  private getFormForAnomalieDetectee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.NONE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.VISIBLE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.EDITABLE;
    form.ficheSiret = this.EDITABLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.EDITABLE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.EDITABLE;
    form.btnSignalerAnomalie = this.EDITABLE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.EDITABLE;

    return form;
  } // getFormForAnomalieDetectee

  // ETAT Corrigee
  private getFormForCorrigee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.VISIBLE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.EDITABLE;
    form.demandeComplementaire = this.EDITABLE;
    form.listeDossierExistant = this.EDITABLE;
    form.motifRefus = this.EDITABLE;
    form.ficheSiret = this.EDITABLE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.EDITABLE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.EDITABLE;
    form.btnRefuser = this.EDITABLE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.EDITABLE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.EDITABLE;

    form.btnCorrectionAnomalie = this.EDITABLE;

    return form;
  } // getFormForCorrigee

  // ETAT Refusee
  private getFormForRefusee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.VISIBLE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.VISIBLE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.VISIBLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForRefusee

  // ETAT Rejetée pour modification de taux
  private getFormForRejetTaux(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.VISIBLE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.VISIBLE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.VISIBLE;
    form.typeAnomalie = this.VISIBLE;
    form.problematiqueAnomalie = this.VISIBLE;
    form.reponseAnomalie = this.VISIBLE;

    form.btnCorrectionAnomalie = this.VISIBLE;

    return form;
  } // getFormForRejetTaux

  // ETAT Attribuee
  private getFormForAttribuee(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.VISIBLE;

    //Cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.VISIBLE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.NONE;
    form.typeAnomalie = this.NONE;
    form.problematiqueAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;

    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForAttribuee

  // ETAT EnInstruction
  private getFormForEnInstruction(): DemandeSubventionForm {
    const form: DemandeSubventionForm = {} as DemandeSubventionForm;

    /* DOTATION */
    form.sousProgramme = this.VISIBLE;
    form.dotationDisponible = this.VISIBLE;

    /* Demande de Subvention */
    form.dateJour = this.VISIBLE;
    form.montantHTTravauxEligibles = this.VISIBLE;
    form.tauxAideFacePercent = this.VISIBLE;
    form.plafondAideDemandee = this.VISIBLE;
    form.etatPrevisionnel = this.VISIBLE;
    form.documentComplementaire = this.VISIBLE;
    form.message = this.VISIBLE;
    form.ficheSiretOngletDemandeSubvention = this.VISIBLE;

    // cadencement
    form.montantAnnee1 = this.VISIBLE;
    form.montantAnnee2 = this.VISIBLE;
    form.montantAnnee3 = this.VISIBLE;
    form.montantAnnee4 = this.VISIBLE;
    form.montantAnneeProl = this.NONE;

    form.btnAnnuler = this.NONE;
    form.btnDemander = this.NONE;
    form.btnRepondre = this.NONE;
    form.btnRenoncer = this.NONE;
    form.btnAccepter = this.NONE;
    form.btnEnregistrer = this.NONE;

    /* Instruction */
    form.etat = this.VISIBLE;
    form.numDossier = this.NONE;
    form.demandeComplementaire = this.NONE;
    form.listeDossierExistant = this.NONE;
    form.motifRefus = this.NONE;
    form.ficheSiret = this.NONE;
    form.decisionAttributiveSignee = this.NONE;

    form.btnQualifier = this.NONE;
    form.btnAccorder = this.NONE;
    form.btnControler = this.NONE;
    form.btnTransferer = this.NONE;
    form.btnAttribuer = this.NONE;
    form.btnDetecterAnomalie = this.NONE;
    form.btnRefuser = this.NONE;
    form.btnSignalerAnomalie = this.NONE;
    form.btnGenererDecisionAttributive = this.NONE;

    /* Anomalie */
    form.btnAjouterAnomalie = this.NONE;
    form.dateAnomalie = this.NONE;
    form.typeAnomalie = this.NONE;
    form.problematiqueAnomalie = this.NONE;
    form.reponseAnomalie = this.NONE;

    form.btnCorrectionAnomalie = this.NONE;

    return form;
  } // getFormForEnInstruction

}

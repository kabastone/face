import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { CadencementTableDto } from '@subvention/dto/cadencement-table.dto';
import { CadencementDto } from '@subvention/dto/cadencement.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadencencementService {

  constructor(private httpService: HttpService) { }

  recupererCadencementsPourCollectivite$(idCollectivite: number, deProjet: boolean): Observable<CadencementTableDto[]> {
    return this.httpService.envoyerRequeteGet(
      `/subvention/cadencement/recuperer/collectivite/${idCollectivite}?deProjet=${deProjet}`);
  }

  updateCadencement(cadencementDto: CadencementDto, clonedCadencement: CadencementDto): Observable<CadencementDto> {
    return this.httpService.envoyerRequetePost(`/subvention/cadencement/${cadencementDto.id}/modifier`,
       cadencementDto, true, clonedCadencement, '');
  }
}

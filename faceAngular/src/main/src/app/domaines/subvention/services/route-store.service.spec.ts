import { TestBed } from '@angular/core/testing';

import { RouteStoreService } from './route-store.service';

describe('RouteStoreService', () => {
  let service: RouteStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouteStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

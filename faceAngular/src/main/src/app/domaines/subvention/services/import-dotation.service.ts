import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImportDotationService {

  constructor(private httpService: HttpService) { }

  importerDotation$(data: FormData): Observable<any> {
    return this.httpService.envoyerRequetePostPourUpload(`/subvention/dossier/importer`, data);
  }

  verifierDotationAnnuelleNotifier$(): Observable<any> {
    return this.httpService.envoyerRequetePost(`/subvention/dossier/verifier`, null);
  }
}

import { Injectable } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';
import * as Paths from '../../../helper/path-to-redirect';

@Injectable({
  providedIn: 'root'
})
export class RouteStoreService {
  urlToRedirect: string;

  constructor(private router: Router) {
   this.router.events
     .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
     .subscribe((events: RoutesRecognized[]) => {
         const url = events[0].urlAfterRedirects;
       if(Paths.pathTList.find(ele => ele === url)) {
        
          this.urlToRedirect = url;
       }
       
    });
   }

   getPreviousRoute() {
     return this.urlToRedirect;
   }
}

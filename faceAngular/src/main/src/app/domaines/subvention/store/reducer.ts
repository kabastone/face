import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { CritereRecherchePaiementDto } from '@paiement/pages/paiement-demande/dto/critere-recherche-paiement.dto';
import { CritereRechercheDossierDto } from '@subvention/dto/critere-recherche-dossier.dto';
import * as Actions from  './actions';



export interface State {
    searchTermsSubv : CritereRechercheDossierDto;
    searchTermsPaie: CritereRecherchePaiementDto;
    resultsSubV: PageReponseDto;
    resultsPaie: PageReponseDto;
    saveFormSubv: Actions.SearchTerms;
    saveFormPaie: Actions.SearchTerms;
}


const initialState: State =  {
    searchTermsSubv: {} as CritereRechercheDossierDto,
    searchTermsPaie: {} as  CritereRecherchePaiementDto,
    resultsSubV: {listeResultats: [], nbTotalResultats: 0},
    resultsPaie: {listeResultats: [], nbTotalResultats: 0},
    saveFormSubv: {} as Actions.SearchTerms,
    saveFormPaie: {} as Actions.SearchTerms
}

export function dossierSubvReducer(
    state:State = initialState, 
    action: Actions.Actions): State {

    switch(action.type) {
        case Actions.SEARCH_SUBVENTION: {
            return {
                ...state,
                searchTermsSubv: action.payload
            }
        }
            
        case Actions.SEARCH_SUBVENTION_DONE: {
            return {
                ...state,
                resultsSubV: action.payload
            }
        }

        case Actions.SAVE_FORM_SUBVENTION: {
            return {
                ...state,
                saveFormSubv: action.payload
            }    

        }

        case Actions.SEARCH_PAIEMENT_DONE: {
            return {
                ...state,
                resultsPaie: action.payload
            }
        }
        case Actions.SAVE_FORM_SUBVENTION: {
            return {
                ...state,
                saveFormSubv: action.payload
            }    

        }
        case Actions.SAVE_FORM_PAIE: {
            return {
                ...state,
                saveFormPaie: action.payload
            }    

        }
        default: {
            return state;
        }    
    }
}
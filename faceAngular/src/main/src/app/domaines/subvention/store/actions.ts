import { PageReponseDto } from '@app/dto/page-reponse.dto';
import { Action } from '@ngrx/store';
import { CritereRecherchePaiementDto } from '@paiement/pages/paiement-demande/dto/critere-recherche-paiement.dto';
import { CritereRechercheDossierDto } from '@subvention/dto/critere-recherche-dossier.dto';

export interface SearchTerms {
      numDossier: string,
      anneeProgrammationDossier: string,
      anneePaiement: string,
      selectionSousProgramme: string,
      selectionEtatDemande: string,
      selectionCollectivite: string,
      selectionDepartement: string
}


// search dossier subvention actions
export const SEARCH_SUBVENTION = 'SEARCH_SUBVENTION';
export const SEARCH_SUBVENTION_DONE = 'SEARCH_SUBVENTION_DONE';
export const SAVE_FORM_SUBVENTION = 'SAVE_FORM_SUBVENTION';

// search dossier paiement actions
export const SEARCH_PAIEMENT = 'SEARCH_PAIEMENT';
export const SEARCH_PAIEMENT_DONE = 'SEARCH_PAIEMENT_DONE';
export const SAVE_FORM_PAIE = 'SAVE_FORM_PAIE';

export class SearchSubvention implements Action {
    readonly type = SEARCH_SUBVENTION;
    constructor(public payload: CritereRechercheDossierDto) {}
}

export class SearchSubventionDone implements Action {
    readonly type = SEARCH_SUBVENTION_DONE;
    constructor(public payload:PageReponseDto) {}

}

export class SaveFormSubvention implements Action {
    readonly type = SAVE_FORM_SUBVENTION;
    constructor(public payload: SearchTerms) {}

}

export class SearchPaiement implements Action {
    readonly type = SEARCH_PAIEMENT;
    constructor(public payload: CritereRecherchePaiementDto) {}
}

export class SearchPaiementDone implements Action {
    readonly type = SEARCH_PAIEMENT_DONE;
    constructor(public payload:PageReponseDto) {}

}

export class SaveFormSubv implements Action {
    readonly type = SAVE_FORM_SUBVENTION;
    constructor(public payload: SearchTerms) {}

}
export class SaveFormPaie implements Action {
    readonly type = SAVE_FORM_PAIE;
    constructor(public payload: SearchTerms) {}

}
export type Actions = SearchSubvention
                      | SearchSubventionDone
                      | SearchPaiement
                      | SearchPaiementDone
                      | SaveFormSubv
                      | SaveFormPaie;

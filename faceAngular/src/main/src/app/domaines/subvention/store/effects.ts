import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { RecherchePaiementService } from '@paiement/services/recherche-paiement.service';
import { DemandeSubventionService } from '@subvention/pages/subvention-demande/services/demande-subvention.service';
import { DossierSubventionService } from '@subvention/pages/subvention-dossier/service/dossier-subvention.service';
import { RechercheDossierService } from '@subvention/services/recherche-dossier.service';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import * as SearchDossier from './actions' 

@Injectable()

export class SearchDossierSubvEffect {
    constructor(
        private actions$: Actions<SearchDossier.Actions>, 
        private rechercheDossierService: RechercheDossierService, 
        private recherchePaiementService: RecherchePaiementService) {}

    @Effect()
    searchSubvention$: Observable<Action> = this.actions$
    .pipe(ofType(SearchDossier.SEARCH_SUBVENTION),
        
        switchMap((searchData: SearchDossier.SearchSubvention) =>{
            return this.rechercheDossierService.getResultatRechercheDossierDto$(searchData.payload)
            .pipe(
                map( results => {
                    return new SearchDossier.SearchSubventionDone(results)
                }))
            //catchError(() => of({ type: 'Search Dossier subvention loaded Error' }))
        } )
    );
    @Effect()
    searchPaiement$: Observable<Action> = this.actions$
    .pipe(ofType(SearchDossier.SEARCH_PAIEMENT),
        
        switchMap((searchData: SearchDossier.SearchPaiement) =>{
            return this.recherchePaiementService.getResultatRecherchePaiementDto$(searchData.payload)
            .pipe(
                map( results => {
                    return new SearchDossier.SearchPaiementDone(results)
                }))
            //catchError(() => of({ type: 'Search Dossier subvention loaded Error' }))
        } )
    );
}
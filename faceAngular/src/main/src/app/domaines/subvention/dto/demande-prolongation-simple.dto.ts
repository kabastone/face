export interface DemandeProlongationSimpleDto {
  id: number;
  codeEtatDemande: string;
  libelleEtatDemande: string;
  dateDemande: Date;
  dateAchevementDemandee: Date;
}

export interface ResultatRechercheDossierDto {
    idDossier: number;
    numDossier: string;
    numEJ: string;
    nomCourtCollectivite: string;
    dateCreationDossier: Date;
    plafondAide: number;
    plafondAideSansRefus: number;
  }

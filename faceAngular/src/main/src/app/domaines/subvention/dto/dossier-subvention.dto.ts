export interface DossierSubventionDto {
  numDossier: string;
  etatDossier: string;
  annee: number;
  descriptionSousProgramme: string;
  engagementJuridique: string;
  dateEcheanceLancement: Date;
  dateEcheanceAchevement: Date;
  nomLongCollectivite: string;
  montantPrevisionelTravaux: number;
  montantTravauxVerses: number;
  resteDisponible: number;
  plafondAide: number;
  tauxConsommationCalcule: number;
  aideVerseeCalcule: number;
  codeDepartement: string;
  estUniqueSubventionEnCoursParSousProgramme: boolean;
  estUniquePaiementEnCours: boolean;
}

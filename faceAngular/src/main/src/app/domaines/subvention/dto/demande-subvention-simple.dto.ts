export interface DemandeSubventionSimpleDto {
  id: number;
  codeEtatDemande: string;
  libelleEtatDemande: string;
  dateDemande: Date;
  aideDemandee: number;
  typeDemande: string;
}

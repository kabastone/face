export interface DemandePaiementSimpleDto {
  id: number;
  codeEtatDemande: string;
  libelleEtatDemande: string;
  dateDemande: Date;
  aideDemandee: number;
  typeDemandePaiement: string;
}

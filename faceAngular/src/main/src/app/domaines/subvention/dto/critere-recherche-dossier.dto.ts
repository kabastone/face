import { PageDemandeDto } from '@app/dto/page-demande.dto';

export interface CritereRechercheDossierDto {

 /** The num dossier. */
 numDossier: string;

 /** The annee creation dossier. */
 anneeProgrammationDossier: number;

 /** The id sous programme. */
 idSousProgramme: number;

 /** The code etat demande. */
 codeEtatDemande: string;

 /** The id collectivite. */
 idCollectivite: number;

 /** The id departement. */
 idDepartement: number;

 /** The page demande. */
 pageDemande: PageDemandeDto;

}

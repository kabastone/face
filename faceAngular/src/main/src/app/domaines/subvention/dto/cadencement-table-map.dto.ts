import { CadencementTableDto } from './cadencement-table.dto';

export interface CadencementTableMapDto {

  hasProlongation: boolean;

  cadencements: CadencementTableDto[];

}

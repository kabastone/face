import { DemandeSubventionTdbDto } from '@app/domaines/tableau-de-bord/dto/demande-subvention-tdb.dto';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { CadencementDto } from './cadencement.dto';

export interface CadencementTableDto {

  sousProgramme: SousProgrammeLovDto;

  plafondAideSubvention: number;

  idDossier: number;

  numDossier: number;

  cadencement: CadencementDto;

  anneeProgrammation: number;

  hasProlongation: boolean;

  soldee: boolean;
}

export interface CadencementDto {
  montantAnnee1: number;
  montantAnnee2: number;
  montantAnnee3: number;
  montantAnnee4: number;
  montantAnneeProl: number;
  estValide: boolean;
}
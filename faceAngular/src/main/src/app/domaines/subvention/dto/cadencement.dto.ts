export interface CadencementDto {

  id: number;

  montantAnnee1: number;

  montantAnnee2: number;

  montantAnnee3: number;

  montantAnnee4: number;

  montantAnneeProlongation: number;

  estValide: boolean;

}

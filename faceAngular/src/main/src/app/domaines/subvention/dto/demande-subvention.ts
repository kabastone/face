import { AnomalieDto } from '@app/dto/anomalie.dto';
import { DocumentDto } from '@app/services/authentification/dto/document.dto';
import { CadencementDto } from './cadencement';

export interface DemandeSubventionDto {

  id: string;
  idDossier: string;
  idCollectivite: string;
  idDotationCollectivite: string;
  idSousProgramme: string;
  version: number;
  chorusNumLigneLegacy: string;
  dateAccord: string;

  /* Département et collectivité */
  nomLongCollectivite: string;
  codeDepartement: string;

  /* DOTATION */
  sousProgramme: string;
  dotationDisponible: string;

  /* Demande de Subvention */
  dateJour: Date;
  montantHTTravauxEligibles: string;
  tauxAideFacePercent: string;
  plafondAideDemandee: string;
  etatPrevisionnelPDF: string;
  etatPrevisionnelTableur: string;
  documentComplementaire: string;
  message: string;
  documents: DocumentDto[];

  /* Instruction */
  etat: string;
  etatLibelle: string;
  numDossier: string;
  demandeComplementaire: string;
  listeDossierExistant: string;
  plafondAideAccordee: string;
  courrier: string;
  motifRefus: string;
  decisionAttributive: string;
  ficheSiret: string;
  decisionAttributiveSignee: string;
  cadencementDto: CadencementDto;

  /* Anomalie */
  anomalies: AnomalieDto[];
}

import { MetadataForm } from '@app/dto/metadata-form';
import { Meta } from '@angular/platform-browser';

export interface DemandeSubventionForm {
  /* DOTATION */
  sousProgramme: MetadataForm;
  dotationDisponible: MetadataForm;

  /* Demande de Subvention */
  dateJour: MetadataForm;
  montantHTTravauxEligibles: MetadataForm;
  tauxAideFacePercent: MetadataForm;
  plafondAideDemandee: MetadataForm;
  etatPrevisionnel: MetadataForm;
  documentComplementaire: MetadataForm;
  message: MetadataForm;
  ficheSiretOngletDemandeSubvention: MetadataForm;
  //cadencement
  montantAnnee1: MetadataForm;
  montantAnnee2: MetadataForm;
  montantAnnee3: MetadataForm;
  montantAnnee4: MetadataForm;
  montantAnneeProl: MetadataForm;

  btnAnnuler: MetadataForm;
  btnDemander: MetadataForm;
  btnRepondre: MetadataForm;
  btnRenoncer: MetadataForm;
  btnAccepter: MetadataForm;

  /* Instruction */
  etat: MetadataForm;
  numDossier: MetadataForm;
  demandeComplementaire: MetadataForm;
  listeDossierExistant: MetadataForm;
  plafondAideAccordee: MetadataForm;
  courrier: MetadataForm;
  motifRefus: MetadataForm;
  decisionAttributive: MetadataForm;
  ficheSiret: MetadataForm;
  decisionAttributiveSignee: MetadataForm;

  btnQualifier: MetadataForm;
  btnAccorder: MetadataForm;
  btnControler: MetadataForm;
  btnTransferer: MetadataForm;
  btnAttribuer: MetadataForm;
  btnDetecterAnomalie: MetadataForm;
  btnRefuser: MetadataForm;
  btnEnregistrer: MetadataForm;
  btnSignalerAnomalie: MetadataForm;
  btnGenererDecisionAttributive: MetadataForm;

  /* Anomalie */
  btnAjouterAnomalie: MetadataForm;
  dateAnomalie: MetadataForm;
  typeAnomalie: MetadataForm;
  problematiqueAnomalie: MetadataForm;
  reponseAnomalie: MetadataForm;

  btnCorrectionAnomalie: MetadataForm;
}

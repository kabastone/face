import { NgModule } from '@angular/core';
// PrimeNG
import { TooltipModule } from 'primeng/tooltip';

// Interne
import { SubventionDossierModule } from './pages/subvention-dossier/subvention-dossier.module';
import { SubventionDemandeModule } from './pages/subvention-demande/subvention-demande.module';
import { SubventionRechercheDossierModule } from './pages/subvention-recherche-dossier/subvention-recherche-dossier.module';
import { SubventionCadencementModule } from './pages/subvention-cadencement/subvention-cadencement.module';
import { SubventionRouteModule } from './subvention.routes';
import { SharedModule } from '@app/share/shared.module';


@NgModule({
  declarations: [],
  imports: [
    TooltipModule,
    SubventionDemandeModule,
    SubventionRechercheDossierModule,
    SubventionDossierModule,
    SubventionCadencementModule,
    SharedModule,
    SubventionRouteModule,
  ],
  exports: [
  ]
})
export class SubventionModule { }

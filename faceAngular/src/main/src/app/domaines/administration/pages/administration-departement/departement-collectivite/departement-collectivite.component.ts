import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CollectiviteSimpleDto } from '@app/services/authentification/dto/collectivite-simple-dto';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { DepartementDto } from '@app/dto/departement.dto';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'adm-departement-collectivite',
  templateUrl: './departement-collectivite.component.html',
  styleUrls: ['./departement-collectivite.component.css']
})
export class DepartementCollectiviteComponent implements OnInit, OnChanges {

  public collectivites: CollectiviteSimpleDto[];

  @Input() departement: DepartementLovDto;

  public isUserUniquementGenerique: boolean;

  // Formulaire IHM
  public collectiviteForm: FormGroup;

  public ngDestroyed$ = new Subject();

  // Collectivite
  public collectivite: CollectiviteDetailDto = {} as CollectiviteDetailDto;

  public affichageModalCreationCollectivite: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private collectiviteService: CollectiviteService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private droitService: DroitService) {
      this.collectiviteForm = this.formBuilder.group({
        departement: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        nomLong: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        nomCourt: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        numCollectivite: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        siret: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        receveurNom: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        receveurNumCodique: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        avecMoaMoeIdentiques: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        avecPlusieursMoe: new FormControl(
          '',
          Validators.compose([Validators.required])
        ),
        etatCollectivite: new FormControl(
          { value: '', disabled: true }
        )
      });
  }

  ngOnInit() {
    this.droitService
    .getInfosUtilisateur$()
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(infoUtilisateur =>
      this.initialiserIsUserUniquementGenerique(infoUtilisateur)
    );
  // récupération des départements
  if (!this.isUserUniquementGenerique) {
    this.collectiviteService.listerCollectivitesParDepartement(this.departement.code)
      .subscribe(data => (this.collectivites = data));
  }
    this.collectivite.etatCollectivite = 'CREEE';
    this.collectivite.departement = this.departement;



    this.chargementDonneesEcran(this.collectivite);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.departement.currentValue && !changes.departement.firstChange) {
      this.collectivites = [];
      if (!this.isUserUniquementGenerique) {
        this.collectiviteService.listerCollectivitesParDepartement(this.departement.code)
          .subscribe(data => (this.collectivites = data));
      }
      this.collectivite.departement = this.departement;
      this.chargementDonneesEcran(this.collectivite);
    }
  }

  private chargementDonneesEcran(collectivite: CollectiviteDetailDto): void {
    this.collectiviteForm.reset();
    Object.entries(collectivite).forEach(([key, value]) => {
      if (value != null && this.collectiviteForm.get(key) != null) {
        this.collectiviteForm.get(key).setValue(value);
      }
    });

    // Cas particulier
    this.collectiviteForm.get('departement').setValue(collectivite.departement.nom);
  }

  afficherModal() {
    this.collectiviteForm.reset();
    this.affichageModalCreationCollectivite = true;
    this.alimenterFormAvecInformationsDejaRemplies();
  }

  fermerModal() {
    this.collectiviteForm.reset();
    this.affichageModalCreationCollectivite = false;
    this.messageService.clear();
    this.alimenterFormAvecInformationsDejaRemplies();
  }

  enregistrer() {
    // mapping des champs du formulaire vers l'objet utilisateur à envoyer au WS
    this.majObjetFormulaire(this.collectiviteForm.value);

    this.collectiviteService.creerCollectivite(this.collectivite).subscribe(dataMaj => {
      // message de succès
      this.messageManagerService.enregistrerCollectiviteMessage();

      if (!this.isUserUniquementGenerique) {
        this.collectiviteService.listerCollectivitesParDepartement(this.departement.code)
          .subscribe(data => (this.collectivites = data));
      }
      this.affichageModalCreationCollectivite = false;
      this.collectiviteForm.reset();
      this.alimenterFormAvecInformationsDejaRemplies();
    });
  }

  private majObjetFormulaire(dataDto: any): void {
    const departement = this.departement;
    Object.entries(dataDto).forEach(([key, value]) => {
      this.collectivite[key] = value;
    });
    this.collectivite.departement = departement;
  }

  annuler() {
    this.collectiviteForm.reset();
    this.affichageModalCreationCollectivite = false;
    this.messageService.clear();
    this.alimenterFormAvecInformationsDejaRemplies();
  }

    /**
  * Initialise le flag de profil de l'utilisateur.
  */
 private initialiserIsUserUniquementGenerique(
  infosUtilisateur: UtilisateurConnecteDto
): void {
  this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
    infosUtilisateur
  );
}

alimenterFormAvecInformationsDejaRemplies() {
  this.collectiviteForm.get('etatCollectivite').setValue('CREEE');
  this.collectiviteForm.get('departement').setValue(this.departement.nom);
  }
}

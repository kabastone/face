import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementDonneesFiscalesComponent } from './departement-donnees-fiscales.component';

describe('AdministrationDonneesFiscalesComponent', () => {
  let component: DepartementDonneesFiscalesComponent;
  let fixture: ComponentFixture<DepartementDonneesFiscalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartementDonneesFiscalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementDonneesFiscalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

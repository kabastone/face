import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationDepartementComponent } from './administration-departement.component';

describe('AdministrationDepartementComponent', () => {
  let component: AdministrationDepartementComponent;
  let fixture: ComponentFixture<AdministrationDepartementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationDepartementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationDepartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild  } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';

import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DroitAgentCollectiviteDto } from '@app/services/authentification/dto/droit-agent-collectivite-dto';
import { CollectiviteSimpleDto } from '@app/services/authentification/dto/collectivite-simple-dto';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { CollectiviteListeDiffusionComponent } from '../collectivite-liste-diffusion/collectivite-liste-diffusion.component';

@Component({
  selector: 'adm-collectivite-resultat',
  templateUrl: './collectivite-resultat.component.html',
  styleUrls: ['./collectivite-resultat.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class CollectiviteResultatComponent implements OnInit, OnDestroy, OnChanges {
  // Formulaire IHM
  public collectiviteForm: FormGroup;

  public modificationEnregistree = false;

  // Collectivite
  @Input() collectivite: CollectiviteDetailDto;
  @Output() collectiviteChange = new EventEmitter<boolean>();

  modificationDetectee: boolean;
  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  @ViewChild(CollectiviteListeDiffusionComponent) listeDiffusion: CollectiviteListeDiffusionComponent;

  public droitAgentCollectivite: DroitAgentCollectiviteDto;

  // Collectivites
  public collectivites: CollectiviteSimpleDto[];

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // constructeur
  constructor(
    private formBuilder: FormBuilder,
    private collectiviteService: CollectiviteService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private confirmationService: ConfirmationService,
    private droitService: DroitService
  ) {
    this.collectiviteForm = this.initialiserForm();
 }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.collectivite) {
      this.collectiviteForm.reset();
      this.chargementDonneesEcran(this.collectivite);
      this.modificationDetectee = false;
      this.formCollectiviteEstDesactive();
      this.messageService.clear();
    }
  }

  formCollectiviteEstDesactive() {
    if (this.isCollectiviteOuverte()) {
      this.collectiviteForm.enable({emitEvent: false});
    } else {
      this.collectiviteForm.disable({emitEvent: false});
    }
  }

  emettreModification(event: any) {
    if (event) {
      this.modificationDetectee = true;
    } else {
      this.modificationDetectee = false;
    }
    this.modificationDetecteeChange.emit(this.modificationDetectee);
  }

  isCollectiviteOuverte() {
    return this.collectivite?.etatCollectivite !== 'FERMEE';
  }

  notifierListeDiffusion(event: any) {
    if(event) {
      this.listeDiffusion.chargementListe();
    }
  }

  private initialiserForm() {
    return this.formBuilder.group({
      departement: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      nomLong: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.maxLength(300)])
        ),
      nomCourt: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.maxLength(75)])
        ),
      numCollectivite: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ),
      siret: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(14), Validators.maxLength(14)])
        ),
      receveurNom: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.maxLength(150)])
      ),
      receveurNumCodique: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ),
      avecMoaMoeIdentiques: new FormControl(
        ''
      ),
      avecPlusieursMoe: new FormControl(
        ''
      ),
      etatCollectivite: new FormControl(
        { value: '', disabled: true }
      )
    });
  }
  ngOnInit() {

    // this.collectiviteService.getCollectivite().subscribe(collectivite=> this.chargementDonneesEcran(collectivite));
    this.chargementDonneesEcran(this.collectivite);

    this.collectiviteForm.valueChanges.subscribe( data => {
      if (this.collectiviteForm.dirty && !this.modificationDetectee) {
        this.modificationDetectee = true;
        this.modificationDetecteeChange.emit(true);
      } else if (this.modificationEnregistree) {
        // message de succès enregistement modification collectivité
        this.messageManagerService.modificationEnregistree();
      }
    });

    this.droitService.getInfosUtilisateur$().subscribe(utilisateur => {
      if (this.droitService.isSd7(utilisateur)) {
        this.collectiviteForm.disable({emitEvent: false});
      }
    });
  }

  private chargementDonneesEcran(collectivite: CollectiviteDetailDto): void {
    Object.entries(collectivite).forEach(([key, value]) => {
      if (value != null && this.collectiviteForm.get(key) != null) {
        this.collectiviteForm.get(key).setValue(value);
      }
    });

    // Cas particulier
    this.collectiviteForm.get('departement').setValue(collectivite.departement.nom);
  }

  private majObjetFormulaire(dataDto: any): void {
    const departement = this.collectivite.departement;
    Object.entries(dataDto).forEach(([key, value]) => {
      this.collectivite[key] = value;
    });
    this.collectivite.departement = departement;
  }

  fermer(): void {
    this.confirmationService.confirm({
      message: 'Êtes-vous certain de vouloir fermer cette collectivité ?',
      header: 'Confirmation de fermeture',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.collectivite.etatCollectivite = 'FERMEE';
        this.collectiviteForm.get('etatCollectivite').setValue(this.collectivite.etatCollectivite);

        this.majObjetFormulaire(this.collectiviteForm.value);

        this.collectiviteService.modifierCollectivite(this.collectivite).subscribe(dataMaj => {
          this.modificationDetectee = false;
          this.modificationEnregistree = true;
          this.formCollectiviteEstDesactive();
          this.messageManagerService.modificationEnregistree();
        });
      },
      reject: () => {
      }
    });
  }


  annuler(): void {
    this.collectiviteForm.reset();
    this.chargementDonneesEcran(this.collectivite);
    this.modificationDetectee = false;
    this.modificationDetecteeChange.emit(false);
    this.messageService.clear();
  }

  enregistrer(): void {
    // mapping des champs du formulaire vers l'objet utilisateur à envoyer au WS
    this.majObjetFormulaire(this.collectiviteForm.value);

    this.collectiviteService.modifierCollectivite(this.collectivite).subscribe(data => {
        this.collectivite = data;
        this.collectiviteForm.reset();
        this.modificationDetectee = false;
        this.modificationDetecteeChange.emit(false);
        // Message de confirmation
        this.messageManagerService.modificationEnregistree();
        this.chargementDonneesEcran(this.collectivite);
    });
  }
  public isMferAdmin(): boolean {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMferAdmin(utilisateurConnecte);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationDonneesFiscalesComponent } from './administration-donnees-fiscales.component';

describe('AdministrationDonneesFiscalesComponent', () => {
  let component: AdministrationDonneesFiscalesComponent;
  let fixture: ComponentFixture<AdministrationDonneesFiscalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationDonneesFiscalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationDonneesFiscalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {CollectiviteResultatComponent } from './collectivite-resultat.component';

describe('CollectiviteResultatComponent', () => {
  let component: CollectiviteResultatComponent;
  let fixture: ComponentFixture<CollectiviteResultatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectiviteResultatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectiviteResultatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationOngletAdresseComponent } from './administration-onglet-adresse.component';

describe('AdministrationAdresseComponent', () => {
  let component: AdministrationOngletAdresseComponent;
  let fixture: ComponentFixture<AdministrationOngletAdresseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationOngletAdresseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationOngletAdresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

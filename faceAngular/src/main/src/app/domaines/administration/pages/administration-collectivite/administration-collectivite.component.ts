import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

import { CollectiviteDetailDto } from "@administration/dto/collectivite-detail-dto";
import { CollectiviteSimpleDto } from '@app/services/authentification/dto/collectivite-simple-dto';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { subscribeOn, takeUntil } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { MessageManagerService } from '@app/services/commun/message-manager.service';


@Component({
  selector: 'adm-administration-collectivite',
  templateUrl: './administration-collectivite.component.html',
  styleUrls: ['./administration-collectivite.component.css']
})
export class AdministrationCollectiviteComponent implements OnInit, OnDestroy {
  // Collectivite
  public collectivite: CollectiviteDetailDto;

  public utilisateurGenerique: boolean;

  // collectivité sélectionnée dans le menu
  public collectiviteSelection: CollectiviteSimpleDto;

  // liste des collectivites chargées
  public listeCollectivites: CollectiviteSimpleDto[] = [];

  // Liste filtrée des collectivites chargées
  public filteredCollectivites: CollectiviteSimpleDto[] = [];

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  public dropdownDisabled = false;

  // constructeur
  constructor(
    private collectiviteService: CollectiviteService,
    private droitService: DroitService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService
  ) {
    this.collectiviteService.listerToutesCollectivites$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        this.listeCollectivites = data;
        this.droitService.getIdCollectivite$().subscribe(idCollectivite => {
          this.onChangeCollectivite(this.listeCollectivites.find(x => x.id === idCollectivite));
          this.messageService.clear();
        });
      });
  }

  modificationDetectee(event: any) {
    if (event) {
      this.messageManagerService.modificationDetecteMessage();
      this.dropdownDisabled = true;
    } else {
      this.messageService.clear();
      this.dropdownDisabled = false;
    }
  }

  // init
  ngOnInit() {
    this.droitService.getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur => {
        this.utilisateurGenerique = this.droitService.isUniquementGenerique(infoUtilisateur);
        if (this.utilisateurGenerique) {
          // Message d'erreur
          this.messageManagerService.getInfosUtilisateurMessage();
        }
      });
  }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  /**
   * OnChange du menu déroulant de sélection de collectivité
   *
   * @param selection - la collectivité sélectionnée
   */
  public onChangeCollectivite(selection: CollectiviteSimpleDto): void {
    if (selection == null || !(typeof selection === 'object')) { return; }
    this.collectiviteSelection = selection;

    // récupération des infos de la collectivité sélectionnée
    this.collectiviteService.getCollectiviteDetailDto$(this.collectiviteSelection.id)
      .subscribe(data => {
        this.collectivite = data;
        this.modificationDetectee(false);
      });
  }

  /**
   * Filtre des collectivités selon l'event reçu.
   *
   * @param event Saisie de l'utilisateur
   */
  filterCollectivites(event: any) {
    this.filteredCollectivites = [];
    this.listeCollectivites.forEach(collectivite => {
      if (
        collectivite.nomCourt
          .toLowerCase()
          .indexOf(event.query.toLowerCase()) === 0
      ) {
        this.filteredCollectivites.push(collectivite);
      }
    });
  }
}

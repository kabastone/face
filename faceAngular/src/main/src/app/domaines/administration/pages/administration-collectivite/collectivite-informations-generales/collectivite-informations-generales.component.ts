import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DroitService } from '@app/services/authentification/droit.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'adm-collectivite-informations-generales',
  templateUrl: './collectivite-informations-generales.component.html',
  styleUrls: ['./collectivite-informations-generales.component.css']
})
export class CollectiviteInformationsGeneralesComponent implements OnInit {

  @Input() collectiviteForm: FormGroup;

  // Collectivite
  @Input() collectivite: CollectiviteDetailDto;

  modificationDetectee: boolean;

  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  constructor(
    private droitService: DroitService,
    private messageManagerService: MessageManagerService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.collectiviteForm.valueChanges.subscribe( data => {
      if (this.collectiviteForm.dirty && this.modificationDetectee === false) {
        this.modificationDetectee = true;
        this.modificationDetecteeChange.emit(true);
      }
    });
  }

  isCollectiviteOuvert(): boolean {
    return this.collectivite.etatCollectivite !== 'FERMEE';
  }

  synchroniser() {
    this.collectivite.etatCollectivite = 'CREEE';
    this.collectiviteForm.get('etatCollectivite').setValue(this.collectivite.etatCollectivite);
  }

}

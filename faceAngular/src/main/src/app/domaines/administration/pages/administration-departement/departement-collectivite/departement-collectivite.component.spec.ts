import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementCollectiviteComponent } from './departement-collectivite.component';

describe('DepartementCollectiviteComponent', () => {
  let component: DepartementCollectiviteComponent;
  let fixture: ComponentFixture<DepartementCollectiviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartementCollectiviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementCollectiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { CollectiviteListeDiffusionService } from './collectivite-liste-diffusion.service';

describe('CollectiviteListeDiffusionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollectiviteListeDiffusionService = TestBed.get(CollectiviteListeDiffusionService);
    expect(service).toBeTruthy();
  });
});

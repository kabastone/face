import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementAdresseComponent } from './departement-adresse.component';

describe('DepartementAdresseComponent', () => {
  let component: DepartementAdresseComponent;
  let fixture: ComponentFixture<DepartementAdresseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartementAdresseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementAdresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdresseEmailDto } from '@administration/dto/adresse-email.dto';
import { HttpService } from '@app/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class CollectiviteListeDiffusionService {
  // constructeur
  constructor(private httpService: HttpService) { }

  /**
   * Obtention de la liste de diffusion d'une collectivité :
   * recherche toutes les adresses e-mail d'une collectivité.
   *
   * @param idCollectivite - id de la collectivité en cours
   * @returns la liste de diffusion
   */
  getListeDiffusionCollectivite(idCollectivite: number): Observable<AdresseEmailDto[]> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/collectivite/${idCollectivite}/liste-diffusion/rechercher`,
      false, [] as AdresseEmailDto[]);
  }

  /**
   * Suppression de l'adresse e-mail dans la liste de diffusion.
   *
   * @param idAdresseMailSupprimer - id de l'adresse mail
   * @param idCollectivite - id de la collectivité en cours
   * @returns OK si tout va bien
   */
  supprimerAdresseEmail(idAdresseMailSupprimer: string, idCollectivite: number): Observable<String> {
    return this.httpService.envoyerRequeteDelete(`/collectivite/${idCollectivite}/liste-diffusion/${idAdresseMailSupprimer}/supprimer`);
  }

  /**
   * Ajout de l'adresse e-mail dans la liste de diffusion.
   *
   * @param adresseEmail - adresse e-mail à ajouter
   * @param idCollectivite - id de la collectivité en cours
   * @returns nouvel objet ajouté en bdd
   */
  ajouterAdresseEmailCollectivite(adresseEmail: string, idCollectivite: number): Observable<AdresseEmailDto> {
    return this.httpService.envoyerRequetePost(`/collectivite/${idCollectivite}/liste-diffusion/ajouter`, adresseEmail);
  }
}

// Angular
import { NgModule } from '@angular/core';

// interne
import { AdministrationCollectiviteComponent } from './administration-collectivite.component';
import { CollectiviteDonneesFiscalesComponent } from './collectivite-donnees-fiscales/collectivite-donnees-fiscales.component';
import { CollectiviteInformationsGeneralesComponent } from './collectivite-informations-generales/collectivite-informations-generales.component';
import { CollectiviteListeDiffusionComponent } from './collectivite-liste-diffusion/collectivite-liste-diffusion.component';
import { CollectiviteAgentsComponent } from './collectivite-agents/collectivite-agents.component';
import { CollectiviteOngletAdresseComponent } from './collectivite-onglet-adresse/collectivite-onglet-adresse.component';
import { CollectiviteAdresseComponent } from './collectivite-adresse/collectivite-adresse.component';
import { CollectiviteResultatComponent } from './collectivite-resultat/collectivite-resultat.component';

// primeng
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {InputMaskModule} from 'primeng/inputmask';
import { SharedModule } from '@app/share/shared.module';
import { KeyFilterModule } from 'primeng/keyfilter';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AdministrationCollectiviteComponent,
    CollectiviteInformationsGeneralesComponent,
    CollectiviteDonneesFiscalesComponent,
    CollectiviteListeDiffusionComponent,
    CollectiviteAgentsComponent,
    CollectiviteOngletAdresseComponent,
    CollectiviteAdresseComponent,
    CollectiviteResultatComponent
  ],
  imports: [
    AccordionModule,
    AutoCompleteModule,
    InputMaskModule,
    KeyFilterModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    CollectiviteInformationsGeneralesComponent,
    CollectiviteDonneesFiscalesComponent
  ]
})
export class AdministrationCollectiviteModule { }

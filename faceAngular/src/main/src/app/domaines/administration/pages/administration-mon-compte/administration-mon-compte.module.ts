import { NgModule } from '@angular/core';
import { AdministrationMonCompteComponent } from './administration-mon-compte.component';
import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AdministrationMonCompteComponent
  ],
  imports: [
   SharedModule,
   RouterModule
  ]
})
export class AdministrationMonCompteModule { }

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';

@Component({
  selector: 'adm-collectivite-donnees-fiscales',
  templateUrl: './collectivite-donnees-fiscales.component.html',
  styleUrls: ['./collectivite-donnees-fiscales.component.css']
})
export class CollectiviteDonneesFiscalesComponent implements OnInit {

  @Input() collectiviteForm: FormGroup  ;

  // Collectivite
  @Input() collectivite: CollectiviteDetailDto;

  modificationDetectee: boolean;

  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
    this.collectiviteForm.valueChanges.subscribe( data => {
      if (this.collectiviteForm.dirty && this.modificationDetectee === false) {
        this.modificationDetectee = true;
        this.modificationDetecteeChange.emit(true);
      }
    });
  }

  isCollectiviteOuvert(): boolean {
    return this.collectivite && this.collectivite.etatCollectivite !== 'FERMEE';
  }

}

import { AdresseEmailDto } from '@administration/dto/adresse-email.dto';
import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Observable, of, Subject } from 'rxjs';
import { switchMap, takeUntil, filter, tap } from 'rxjs/operators';
import { CollectiviteListeDiffusionService } from './service/collectivite-liste-diffusion.service';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';

@Component({
  selector: 'adm-collectivite-liste-diffusion',
  templateUrl: './collectivite-liste-diffusion.component.html',
  styleUrls: ['./collectivite-liste-diffusion.component.css']
})
export class CollectiviteListeDiffusionComponent implements OnInit, OnDestroy, OnChanges {

  // sujet à souscrire pour la maj de la liste de diffusion
  private listeDiffusionSubject = new Subject<AdresseEmailDto[]>();

  // observable : liste de diffusion
  public listeDiffusion$: Observable<AdresseEmailDto[]> = this.listeDiffusionSubject.asObservable();

  // infos utilisateur connecté
  private utilisateurConnecte: UtilisateurConnecteDto;

  // id de l'adresse mail à supprimer
  private idAdresseMailSupprimer: string;

  // affichage (ou non) de la modal : confirmation de suppression de l'adresse email
  public affichageModalConfirmSupprEmail: Boolean = false;

  // affichage (ou non) de la modal : ajout d'une adresse email
  public affichageModalAjoutEmail: Boolean = false;

  // adresse e-mail à ajouter à la liste de diffusion
  public emailAjouter: string = undefined;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // pour la désactivation du bouton de suppression de l'adresse email.
  private uneSeuleAdresse: boolean = false;

  // Collectivite
  @Input() collectivite: CollectiviteDetailDto;

  // constructeur
  constructor(
    private droitService: DroitService,
    private collectiviteListeDiffusionService: CollectiviteListeDiffusionService
  ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  // Lors de changements
  ngOnChanges(changes: SimpleChanges): void {
    this.listeDiffusionSubject.next([] as AdresseEmailDto[]);
    this.chargerListeDiffusion()
    .subscribe(listeDiffusion =>
        this.listeDiffusionSubject.next(listeDiffusion)
    );
  }

  // init.
  ngOnInit() {

    // subscribe sur maj de la liste de diffusion
    this.droitService.getInfosUtilisateur$()
      .pipe(
        switchMap((infoUser: UtilisateurConnecteDto) => {
          if (!infoUser.id) {
            return of([] as AdresseEmailDto[]);
          }
          this.utilisateurConnecte = infoUser;
          return this.chargerListeDiffusion();
        }),
        takeUntil(this.ngDestroyed$)
      )
      .subscribe(listeDiffusion =>
        this.listeDiffusionSubject.next(listeDiffusion)
      );
  }

  /**
   * Indique si l'utilisateur est administrateur sur la collectivite, ou non.
   *
   * @returns true ou false
   */
  isAdminCollectivite(): boolean {
    if (!this.utilisateurConnecte) {
      return false;
    }
    return this.droitService.isAdminCollectivite(
      this.utilisateurConnecte,
      this.collectivite.id
    );
  }

  /**
   * Affiche la modal de confirmation de suppression d'une adresse mail dans la liste.
   *
   */
  confirmerSupprimerEmail(idAdresseMail: string): void {
    this.idAdresseMailSupprimer = idAdresseMail;
    this.affichageModalConfirmSupprEmail = true;
  }

  isCollectiviteOuvert(): boolean {
    return this.collectivite.etatCollectivite !== 'FERMEE';
  }

  /**
   * Ferme/cache la modal de confirmation de suppression d'une adresse email.
   *
   */
  fermerConfirmSupprEmail(): void {
    this.idAdresseMailSupprimer = undefined;
    this.affichageModalConfirmSupprEmail = false;
  }

  /**
   * Suppression d'une adresse email dans la liste de diffusion
   *
   */
  supprimerAdresseEmail(): void {
    // fermeture de la fenêtre modale de confirmation de suppression
    this.affichageModalConfirmSupprEmail = false;

    // suppression
    this.collectiviteListeDiffusionService.supprimerAdresseEmail(this.idAdresseMailSupprimer, this.collectivite.id)
      .pipe(
        switchMap(_ => this.chargerListeDiffusion())
      )
      .subscribe(listeDiffusion => this.listeDiffusionSubject.next(listeDiffusion));
  }

  /**
   * Affiche la modal de confirmation d'ajout d'une adresse mail dans la liste.
   *
   */
  afficherModalAjoutEmail(): void {
    this.emailAjouter = undefined;
    this.affichageModalAjoutEmail = true;
  }

  estDerniereAdresse(): boolean {
    return (this.uneSeuleAdresse && this.collectivite.etatCollectivite ==="CREEE");
  }

  /**
   * Ajout de l'adresse e-mail spécifiée dans la liste de diffusion de la collectivité.
   *
   */
  ajouterAdresseEmail(): void {
    // fermeture de la fenêtre modale de confirmation de suppression
    this.affichageModalAjoutEmail = false;

    // l'e-mail à ajouter = en lower case + trim
    this.emailAjouter = this.emailAjouter.trim().toLowerCase();

    // ajout
    this.collectiviteListeDiffusionService.ajouterAdresseEmailCollectivite(this.emailAjouter, this.collectivite.id)
      .pipe(
        switchMap(_ => this.chargerListeDiffusion())
      )
      .subscribe(listeDiffusion => this.listeDiffusionSubject.next(listeDiffusion));
  }

  /**
   * Chargement de la liste de diffusion : Hook externe.
   *
  */
  public chargementListe() {
    this.chargerListeDiffusion().subscribe(listeDiffusion => this.listeDiffusionSubject.next(listeDiffusion));
  }

  /**
   * Chargement de la liste de diffusion.
   *
   * @returns les données observables de la liste de diffusion
   */
  private chargerListeDiffusion(): Observable<AdresseEmailDto[]> {
    if (!this.collectivite.id) {
      return of([] as AdresseEmailDto[]);
    }
    return this.collectiviteListeDiffusionService.getListeDiffusionCollectivite(this.collectivite.id)
    .pipe(tap( listeDiffusion => {
      this.uneSeuleAdresse = (listeDiffusion.length === 1);
    }));
  }
}

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { DroitAgentUtilisateurDto } from '@administration/dto/droit-agent-utilisateur.dto';
import { Observable } from 'rxjs';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';

@Injectable({
  providedIn: 'root'
})
export class CollectiviteAgentsService {

  constructor(private httpService: HttpService) { }

  /**
   * Obtention des agents d'une collectivite.
   *
   * @param id de la collectivité
   * @returns la liste des agents de la collectivité
   */
  getAgentsCollectivite(idCollectivite: number): Observable<DroitAgentUtilisateurDto[]> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/collectivite/${idCollectivite}/agents/rechercher`,
      true, [] as DroitAgentUtilisateurDto[]);
  }

  /**
   * Recherche d'un utilisateur via son adresse e-mail.
   *
   * @param email à utiliser pour la recherche
   * @returns l'utilisateur trouvé (ou 'DataNotFoundException' si rien n'est trouvé)
   */
  rechercherUtilisateurParMail(email: string): Observable<UtilisateurConnecteDto> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/utilisateur/email/${email}/rechercher`, false, {} as UtilisateurConnecteDto);
  }

  /**
   * Création d'un utilisateur.
   *
   * @param utilisateur - utilisateur à créer
   * @returns l'utilisateur créé
   */
  creerUtilisateur(utilisateur: UtilisateurConnecteDto): Observable<UtilisateurConnecteDto> {
    return this.httpService.envoyerRequetePost('/utilisateur/creer', utilisateur);
  }

  /**
   * Ajoute l'agent sur la collectivité spécifiée.
   *
   * @param idUtilisateur - id de l'agent à supprimer
   * @param idCollectivite - id de la collecitivité en cours
   * @returns l'agent mis à jour
   */
  ajouterAgent(idUtilisateur: number, idCollectivite: number): Observable<UtilisateurConnecteDto> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/collectivite/${idCollectivite}/agent/${idUtilisateur}/ajouter`,
      true, {} as UtilisateurConnecteDto);
  }

  /**
   * Change l'état d'un agent : ajoute ou supprime le statut 'admin'.
   *
   * @param idUtilisateur - id de l'agent à supprimer
   * @param idCollectivite - id de la collecitivité en cours
   * @returns l'agent mis à jour
   */
  changerEtatAdminDroitAgent(idUtilisateur: number, idCollectivite: number): Observable<UtilisateurConnecteDto> {
    return this.httpService.envoyerRequeteGet(`/collectivite/${idCollectivite}/agent/${idUtilisateur}/changer-etat`);
  }

  /**
   * Supprime un agent d'une collectivité.
   *
   * @param idUtilisateur - id de l'agent à supprimer
   * @param idCollectivite - id de la collecitivité en cours
   * @returns l'agent mis à jour
   */
  supprimerAgentCollectivite(idUtilisateur: number, idCollectivite: number): Observable<String> {
    return this.httpService.envoyerRequeteDelete(`/collectivite/${idCollectivite}/agent/${idUtilisateur}/supprimer`);
  }
}

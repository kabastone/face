import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SelectItem} from 'primeng/api';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { DroitService } from '@app/services/authentification/droit.service';


@Component({
  selector: 'adm-collectivite-adresse',
  templateUrl: './collectivite-adresse.component.html',
  styleUrls: ['./collectivite-adresse.component.css']
})
export class CollectiviteAdresseComponent implements OnInit {

  // Formulaire IHM
  @Input() adresseForm: FormGroup;
  @Input() fermee: boolean;

  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  modificationDetectee = false;

  civilites: SelectItem[];

  constructor(
    private formBuilder: FormBuilder,
    private droitService: DroitService
  ) {
    this.civilites = [
      {label: 'Mme', value: 'Madame'},
      {label: 'M.', value: 'Monsieur'}]
  }

  ngOnInit() {
    this.adresseForm.valueChanges.subscribe( data => {
      if (this.adresseForm.dirty && this.modificationDetectee === false) {
        this.modificationDetectee = true;
        this.modificationDetecteeChange.emit(true);
      }
    });
  }

  isCollectiviteOuvert(): boolean {
    return this.fermee;
  }
}

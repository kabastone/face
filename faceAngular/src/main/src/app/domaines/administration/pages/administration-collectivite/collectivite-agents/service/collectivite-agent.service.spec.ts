import { TestBed } from '@angular/core/testing';

import { AdministrationAgentsService } from './administration-agent.service';

describe('AdministrationAgentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdministrationAgentsService = TestBed.get(AdministrationAgentsService);
    expect(service).toBeTruthy();
  });
});

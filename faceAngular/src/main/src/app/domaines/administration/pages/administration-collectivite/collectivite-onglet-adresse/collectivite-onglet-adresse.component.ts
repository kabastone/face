import { AdresseDto } from '@administration/dto/adresse.dto';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { Component, Input, OnDestroy, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { MessageService } from 'primeng/api';
import { DroitService } from '@app/services/authentification/droit.service';

@Component({
  selector: 'adm-collectivite-onglet-adresse',
  templateUrl: './collectivite-onglet-adresse.component.html',
  styleUrls: ['./collectivite-onglet-adresse.component.css']
})
export class CollectiviteOngletAdresseComponent implements OnInit, OnDestroy, OnChanges {

  @Input() collectivite: CollectiviteDetailDto;
  @Input() collectiviteForm: FormGroup;

  @Output() collectiviteChange = new EventEmitter<boolean>();

  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  // Boolean indiquant si le form est modifié par les données de la base ou l'utilisateur.
  public enregistrementPossible = false;

  public adresseFormMoa: FormGroup;
  public adresseFormMoe: FormGroup;

  public modificationDetectee = false;
  public modificationEnregistree = false;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private collectiviteService: CollectiviteService,
    private messageManagerService: MessageManagerService,
    private messageService: MessageService,
    private droitService: DroitService
  ) {
    this.adresseFormMoa = this.createAdresseFormGroup();
    this.adresseFormMoe = this.createAdresseFormGroup();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEmpty(this.collectivite.adresseMoa)) {
      this.chargementDonneesEcran(this.collectivite.adresseMoa, this.adresseFormMoa);
    } else {
      this.adresseFormMoa.reset();
    }
    if (!this.isEmpty(this.collectivite.adresseMoe)) {
      this.chargementDonneesEcran(this.collectivite.adresseMoe, this.adresseFormMoe);
    } else {
      this.adresseFormMoe.reset();
    }
    this.desactiverChampsSiCollectiviteFermee();
    this.desactiverChampsSiUtilisateurEstSD7();
  }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  changementEtatCheckbox(event: any, origine: string) {
    if (event && origine === "MoaMoeIdentiques") {
      this.collectiviteForm.get('avecPlusieursMoe').setValue(false);
    } else if (event && origine === "PlusieursMoe") {
      this.collectiviteForm.get('avecMoaMoeIdentiques').setValue(false);
    }
  }

  isCollectiviteOuvert(): boolean {
    return this.collectivite?.etatCollectivite !== 'FERMEE';
  }

  ngOnInit() {

    if (this.collectivite.adresseMoa) {
      this.chargementDonneesEcran(this.collectivite.adresseMoa, this.adresseFormMoa);
    }
    if (this.collectivite.adresseMoe) {
      this.chargementDonneesEcran(this.collectivite.adresseMoe, this.adresseFormMoe);
    }

    this.onFormGroupValuesChange();
  }

  public showMoe(): boolean {
    return !(this.collectiviteForm.get('avecPlusieursMoe').value || this.collectiviteForm.get('avecMoaMoeIdentiques').value);
  }

  private onFormGroupValuesChange() {

    this.collectiviteForm.valueChanges
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      _ => {
        this.desactiverChampsSiCollectiviteFermee();
        this.calculerEnregistrementPossible();
        if (!this.modificationDetectee) {
          this.modificationDetectee = true;
        }
      }
    );
    this.adresseFormMoa.valueChanges
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      _ => {
        if (this.adresseFormMoa.touched && this.adresseFormMoa.dirty) {
          this.calculerEnregistrementPossible();

          if (!this.modificationDetectee) {
            this.modificationDetectee = true;
          }
        }
    });
    this.adresseFormMoe.valueChanges
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      _ => {
        if (this.adresseFormMoe.touched && this.adresseFormMoe.dirty) {
          this.calculerEnregistrementPossible();
          if (!this.modificationDetectee) {
            this.modificationDetectee = true;
          }
        }
    });
  }


  private desactiverChampsSiCollectiviteFermee() {
    if (this.isCollectiviteOuvert()) {
      this.adresseFormMoa.enable({ emitEvent: false });
      this.adresseFormMoe.enable({ emitEvent: false });
    } else {
      this.adresseFormMoa.disable({ emitEvent: false });
      this.adresseFormMoe.disable({ emitEvent: false });
    }
  }

  desactiverChampsSiUtilisateurEstSD7() {
    this.droitService.getInfosUtilisateur$().subscribe(utilisateur => {
      if(this.droitService.isSd7(utilisateur)) {
        this.adresseFormMoa.disable({ emitEvent: false });
        this.adresseFormMoe.disable({ emitEvent: false });
      }
    });
  }

  calculerEnregistrementPossible() {
    this.enregistrementPossible = (!(
      this.collectiviteForm.controls['avecMoaMoeIdentiques'].value
      || this.collectiviteForm.controls['avecPlusieursMoe'].value)
      && this.adresseFormMoa.valid && this.adresseFormMoe.valid
    ) || (
      (this.collectiviteForm.controls['avecMoaMoeIdentiques'].value
      || this.collectiviteForm.controls['avecPlusieursMoe'].value)
      && this.adresseFormMoa.valid
    );
  }

  private chargementDonneesEcran(adresse: AdresseDto, adresseForm: FormGroup): void {
    adresseForm.reset();
    Object.entries(adresse).forEach(([key, value]) => {
      if (value != null && adresseForm.get(key) != null) {
        adresseForm.controls[key].setValue(value);
      }
    });
  }

  enregistrer(): void {
    // mapping des champs du formulaire vers l'objet utilisateur à envoyer au WS
    if (this.collectivite.adresseMoa) {
      this.majObjetFormulaire(this.collectivite.adresseMoa, this.adresseFormMoa.value);
    }
    if (this.collectivite.adresseMoe) {
      this.majObjetFormulaire(this.collectivite.adresseMoe, this.adresseFormMoe.value);
    }

    this.collectivite.avecMoaMoeIdentiques = this.collectiviteForm.controls['avecMoaMoeIdentiques'].value;
    this.collectivite.avecPlusieursMoe = this.collectiviteForm.controls['avecPlusieursMoe'].value;

    if(this.collectivite.avecMoaMoeIdentiques || this.collectivite.avecPlusieursMoe) {
      this.collectivite.adresseMoa = this.collectivite.adresseMoa ? this.collectivite.adresseMoa : new AdresseDto();
      this.majObjetFormulaire(this.collectivite.adresseMoa, this.adresseFormMoa.value);
    } else {
      this.collectivite.adresseMoa = this.collectivite.adresseMoa ? this.collectivite.adresseMoa : new AdresseDto();
      this.collectivite.adresseMoe = this.collectivite.adresseMoe ? this.collectivite.adresseMoe : new AdresseDto();

      this.majObjetFormulaire(this.collectivite.adresseMoa, this.adresseFormMoa.value);
      this.majObjetFormulaire(this.collectivite.adresseMoe, this.adresseFormMoe.value);

    }

    this.collectiviteService.modifierCollectivite(this.collectivite)
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(dataMaj => {

      this.collectiviteChange.emit(dataMaj);
      this.collectivite = dataMaj;

      this.adresseFormMoa.reset();
      this.adresseFormMoe.reset();

      if (this.collectivite.adresseMoa) {
        this.chargementDonneesEcran(this.collectivite.adresseMoa, this.adresseFormMoa);
      }
      if (this.collectivite.adresseMoe) {
        this.chargementDonneesEcran(this.collectivite.adresseMoe, this.adresseFormMoe);
      }

      this.desactiverChampsSiCollectiviteFermee();

      this.modificationDetectee = false;
      this.modificationDetecteeChange.emit(false);


      // message de succès
      this.messageManagerService.enregistrerAdresseMessage();
    });
  }

  annuler(): void {
    if (!this.isEmpty(this.collectivite.adresseMoa)) {
      this.chargementDonneesEcran(this.collectivite.adresseMoa, this.adresseFormMoa);
    } else {
      this.adresseFormMoa.reset();
    }
    if (!this.isEmpty(this.collectivite.adresseMoe)) {
      this.chargementDonneesEcran(this.collectivite.adresseMoe, this.adresseFormMoe);
    } else {
      this.adresseFormMoe.reset();
    }
    this.collectiviteForm.controls['avecMoaMoeIdentiques'].setValue(this.collectivite.avecMoaMoeIdentiques);
    this.collectiviteForm.controls['avecPlusieursMoe'].setValue(this.collectivite.avecPlusieursMoe);

    this.modificationDetectee = false;
    this.modificationDetecteeChange.emit(false);
    this.messageService.clear();
  }

  private majObjetFormulaire(data: any, dataForm: FormGroup): void {
    Object.entries(dataForm).forEach(([key, value]) => {
      data[key] = value;
    });
  }

  onAdresseMoaChanged(adresse: AdresseDto) {
    // do what you want with new value
  }

  onAdresseMoeChanged(adresse: AdresseDto) {
    // do what you want with new value
  }

  createAdresseFormGroup(): FormGroup {
    return this.formBuilder.group({
      qualiteDestinataire: new FormControl(''),
      civilite: new FormControl(''),
      nom: new FormControl(
      '',
        Validators.compose([Validators.required])
      ),
      prenom: new FormControl(''),
      numeroVoie: new FormControl(''),
      nomVoie: new FormControl(
      '',
        Validators.compose([Validators.required])
      ),
      complement: new FormControl(''),
      codePostal: new FormControl(
       '',
        Validators.compose([Validators.required])
      ),
      commune: new FormControl(
      '',
        Validators.compose([Validators.required])
      )
    });
  }

  private isEmpty(obj): boolean {
    for(const key in obj) {
        if (obj.hasOwnProperty(key)) {
          return false;
        }
    }
    return true;
  }

  emettreModification(event: any) {
    if (event === this.modificationDetectee) {
      return ;
    }
    if (event) {
      this.modificationDetectee = true;
    } else {
      this.modificationDetectee = false;
    }
      this.modificationDetecteeChange.emit(this.modificationDetectee);
    }
}

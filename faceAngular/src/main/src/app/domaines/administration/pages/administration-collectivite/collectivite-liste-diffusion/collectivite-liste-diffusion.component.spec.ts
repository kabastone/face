import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationListeDiffusionComponent } from './administration-liste-diffusion.component';

describe('AdministrationListeDiffusionComponent', () => {
  let component: AdministrationListeDiffusionComponent;
  let fixture: ComponentFixture<AdministrationListeDiffusionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationListeDiffusionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationListeDiffusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

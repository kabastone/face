import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DepartementDto } from '@app/dto/departement.dto';
import { MessageService } from 'primeng/api';
import { DepartementService } from '../service/departement.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'adm-departement-donnees-fiscales',
  templateUrl: './departement-donnees-fiscales.component.html',
  styleUrls: ['./departement-donnees-fiscales.component.css']
})
export class DepartementDonneesFiscalesComponent implements OnInit, OnChanges {


  @Input() public departement: DepartementDto;
  @Output() public departementChange = new EventEmitter<boolean>();

  @Input() public modificationDetectee: boolean;
  @Output() public modificationDetecteeChange = new EventEmitter<boolean>();

  public donneesFiscalesForm: FormGroup;

  // bouton 'enregistrer' en disabled
  public enregistrerDisabled = true;

  constructor(private formBuilder: FormBuilder,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private departementService: DepartementService, ) {
    this.initialiserForm();
  }

  ngOnInit() {

    this.donneesFiscalesForm.valueChanges.subscribe(
      _ => {
        if (this.donneesFiscalesForm.dirty && this.modificationDetectee === false) {
          this.enregistrerDisabled = false;
          this.modificationDetectee = true;
          this.modificationDetecteeChange.emit(true);
          // Message de modification
          this.messageManagerService.modificationDetecteMessage();
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.departement && this.departement) {
      this.chargementDonneesEcran(this.departement);
    }
  }

  private initialiserForm(): void {
    this.donneesFiscalesForm = this.formBuilder.group({
      nomCodique: new FormControl('', Validators.compose([Validators.required])),
      codique: new FormControl('', Validators.compose([Validators.required]))

    });
  }

  private chargementDonneesEcran(departement: DepartementDto): void {
    Object.entries(departement).forEach(([key, value]) => {
      if (this.donneesFiscalesForm.get(key) != null) {
        this.donneesFiscalesForm.get(key).setValue(value);
      }
    });
  }

  private majObjetFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      this.departement[key] = value;
    });
  }

  enregistrer() {
    this.majObjetFormulaire(this.donneesFiscalesForm.value);
    this.departementService.envoyerDepartementDonneesFiscalesEnregistrement(this.departement.id, this.departement)
      .subscribe(data => {
        this.departement = data;
        this.donneesFiscalesForm.reset();
        this.modificationDetectee = false;
        this.modificationDetecteeChange.emit(false);
        // message de succes
        this.messageManagerService.modificationEnregistree();
        this.chargementDonneesEcran(this.departement);
      })
  }

  annuler(): void {
    this.donneesFiscalesForm.reset();
    this.modificationDetectee = false;
    this.modificationDetecteeChange.emit(false);
    this.messageService.clear();
    this.chargementDonneesEcran(this.departement);
  }
}

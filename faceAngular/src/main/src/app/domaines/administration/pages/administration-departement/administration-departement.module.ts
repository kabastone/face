import { NgModule } from '@angular/core';

import { AdministrationDepartementComponent } from './administration-departement.component';
import { DepartementCollectiviteComponent } from './departement-collectivite/departement-collectivite.component';
import { DepartementDonneesFiscalesComponent } from './departement-donnees-fiscales/departement-donnees-fiscales.component';

import { AdministrationCollectiviteModule } from '../administration-collectivite/administration-collectivite.module';
import { DepartementAdresseComponent } from './departement-adresse/departement-adresse.component';
import { LayoutModule } from '@layout/layout.module';
import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AdministrationDepartementComponent,
    DepartementCollectiviteComponent,
    DepartementDonneesFiscalesComponent,
    DepartementAdresseComponent,
    ],
  imports: [
    LayoutModule,
    AdministrationCollectiviteModule,
    SharedModule,
    RouterModule
  ]
})
export class AdministrationDepartementModule { }

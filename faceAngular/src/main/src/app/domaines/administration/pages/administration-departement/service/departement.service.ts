import { Injectable } from '@angular/core';
import { AdresseDto } from '@administration/dto/adresse.dto';
import { Observable } from 'rxjs';
import { HttpService } from '@app/services/http/http.service';
import { DepartementDto } from '@app/dto/departement.dto';

@Injectable({
  providedIn: 'root'
})

export class DepartementService {

  constructor( private httpService: HttpService) { }

  public rechercherAdresseDepartement(id: number): Observable<AdresseDto> {
        return this.httpService.envoyerRequeteGet(`/administration/departement/${id}/adresse/recherche`);
  }

  public envoyerAdresseEnregistrement(id: number, adresse: AdresseDto): Observable<AdresseDto> {
    return this.httpService.envoyerRequetePost(`/administration/departement/${id}/adresse/enregistrer`, adresse);
  }

  public envoyerDepartementDonneesFiscalesEnregistrement(id: number, departement: DepartementDto){
    return this.httpService.envoyerRequetePost(`/administration/departement/${id}/donnees-fiscales/enregistrer`, departement);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationCollectiviteComponent } from './administration-collectivite.component';

describe('AdministrationCollectiviteComponent', () => {
  let component: AdministrationCollectiviteComponent;
  let fixture: ComponentFixture<AdministrationCollectiviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationCollectiviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationCollectiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

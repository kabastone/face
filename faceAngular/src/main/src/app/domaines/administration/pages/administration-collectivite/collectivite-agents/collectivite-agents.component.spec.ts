import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectiviteAgentsComponent } from './collectivite-agents.component';

describe('AdministrationAgentComponent', () => {
  let component: CollectiviteAgentsComponent;
  let fixture: ComponentFixture<CollectiviteAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectiviteAgentsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectiviteAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

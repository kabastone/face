import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectiviteAdresseComponent } from './collectivite-adresse.component';

describe('CollectiviteAdresseComponent', () => {
  let component: CollectiviteAdresseComponent;
  let fixture: ComponentFixture<CollectiviteAdresseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectiviteAdresseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectiviteAdresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

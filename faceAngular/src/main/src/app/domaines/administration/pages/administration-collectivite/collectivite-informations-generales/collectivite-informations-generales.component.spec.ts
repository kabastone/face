import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationInformationsGeneralesComponent } from './administration-informations-generales.component';

describe('AdministrationInformationsGeneralesComponent', () => {
  let component: AdministrationInformationsGeneralesComponent;
  let fixture: ComponentFixture<AdministrationInformationsGeneralesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationInformationsGeneralesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationInformationsGeneralesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

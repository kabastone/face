import { DroitAgentUtilisateurDto } from '@administration/dto/droit-agent-utilisateur.dto';
import { Component, OnDestroy, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';
import { DroitAgentCollectiviteDto } from '@app/services/authentification/dto/droit-agent-collectivite-dto';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Observable, of, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { CollectiviteAgentsService } from './service/collectivite-agent.service';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'adm-collectivite-agents',
  templateUrl: './collectivite-agents.component.html',
  styleUrls: ['./collectivite-agents.component.css']
})
export class CollectiviteAgentsComponent implements OnInit, OnDestroy, OnChanges {

  // La collectivite selectionnee par l'utilisateur.
  @Input() collectivite: CollectiviteDetailDto;

  @Output() ajoutPremierAgent = new EventEmitter<boolean>();

  // sujet à souscrire pour la maj de la liste des agents de la collectivité
  private agentsSubject = new Subject<DroitAgentUtilisateurDto[]>();

  // observable : liste des agents de la collectivité
  public listeAgentsCollectivite$:  Observable<DroitAgentUtilisateurDto[]> = this.agentsSubject.asObservable();

  // affichage (ou non) de la modal d'ajout de recherche d'un agent
  public affichageModalRecherche: Boolean = false;

  // adresse mail de l'agent à ajouter
  public emailRechercheAgent: string;

  // affichage (ou non) de la modal qui permet d'accepter (ou non) l'agent trouvé
  public affichageModalAgentTrouve: Boolean = false;

  // affichage (ou non) de la modal d'agent non trouvé
  public affichageModalAgentNonTrouve: Boolean = false;

  // affichage (ou non) de la modal : confirmation de suppression d'un agent
  public affichageModalConfirmSupprAgent: Boolean = false;

  // agent trouvé suite à une recherche en bdd
  public agentTrouveBdd: UtilisateurConnecteDto = {} as UtilisateurConnecteDto;

  // id de l'agent à supprimer
  public idAgentSupprimer: number;

  // infos agent connecté
  public agentConnecte: UtilisateurConnecteDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  private unSeulAdmin: boolean = false;

  // constructeur
  constructor(
    private router: Router,
    private droitService: DroitService,
    private collectiviteAgentsService: CollectiviteAgentsService
    ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  //Lorsque la collectivite change.
  ngOnChanges(changes: SimpleChanges): void {
    this.agentsSubject.next([] as DroitAgentUtilisateurDto[]);
    this.getAgentsCollectivite().subscribe( listeUsers => {
          this.agentsSubject.next(listeUsers);
    });
  }

  isCollectiviteOuvert(): boolean {
    return this.collectivite?.etatCollectivite !== 'FERMEE';
  }

  getTailleAdmin(): string{
    return this.isCollectiviteOuvert()?'ui-md-1':'ui-md-2';
  }

  // init.
  ngOnInit(): void {

    // chargement des données de l'écran
    this.droitService.getInfosUtilisateur$()
    .pipe(
      switchMap((infoUser: UtilisateurConnecteDto) => {
        if (!infoUser.id) {
          return of([] as DroitAgentUtilisateurDto[]);
        }
        this.agentConnecte = infoUser;
        return this.getAgentsCollectivite();
      }),
      takeUntil(this.ngDestroyed$)
    )
    .subscribe(listeUsers => this.agentsSubject.next(listeUsers));
  }

  /**
   * Affiche la fenêtre modale de recherche.
   *
   */
  afficherModalRecherche(): void {
    // on vide une éventuelle recherche précédente
    this.emailRechercheAgent = undefined;

    // affichage
    this.affichageModalRecherche = true;
  }

  /**
   * Ferme/cache la fenêtre modale de recherche.
   *
   */
  fermerModalRecherche(): void {
    this.affichageModalRecherche = false;
  }

  /**
   * Effectue la recherche d'un utilisateur via son mail.
   *
   */
  rechercherUtilisateur(): void {
    // on vide un éventuel résultat précédent
    this.agentTrouveBdd = {} as UtilisateurConnecteDto;

    // recherche de l'utilisateur via son mail
    this.collectiviteAgentsService.rechercherUtilisateurParMail(this.emailRechercheAgent)
      .subscribe(agentTrouve => {
        if (agentTrouve.id) {
          this.gestionAgentTrouve(agentTrouve);
        } else {
          this.gestionAgentNonTrouve();
        }
      });
  }

  /**
   * Gestion de l'agent trouvé.
   *
   * @param agentTrouve - l'agent trouvé en bdd
   */
  gestionAgentTrouve(agentTrouve: UtilisateurConnecteDto): void {
    // on le save dans la classe
    this.agentTrouveBdd = agentTrouve;

    // on ferme la modal
    this.fermerModalRecherche();

    // on affiche une autre modal : pour savoir si l'on ajoute bien celui-ci
    this.affichageModalAgentTrouve = true;
  }

  /**
   * Gestion de l'agent non trouvé.
   *
   */
  gestionAgentNonTrouve(): void {
    // fermeture de la fenêtre existante
    this.affichageModalRecherche = false;

    // affichage de la fenêtre modale suivante
    this.affichageModalAgentNonTrouve = true;
  }

  /**
   * Accepte l'agent trouvé en bdd.
   *
   */
  accepteAgentTrouve(): void {
    // fermeture de la fenêtre modale
    this.affichageModalAgentTrouve = false;

    // ajout de l'agent trouvé, dans la collectivité en cours
    this.collectiviteAgentsService.ajouterAgent(this.agentTrouveBdd.id, this.collectivite.id)
    .pipe(
      switchMap(_ => this.getAgentsCollectivite()),
      tap(listeUsers => {
        this.ajoutPremierAgent.emit(listeUsers.length === 1);
        console.log("boolean", listeUsers.length === 1, listeUsers);
      })
    )
    .subscribe(listeUsers => this.agentsSubject.next(listeUsers));
  }

  /**
   * Ferme/cache la fenêtre modale d'acceptation (ou non) de l'agent trouvé.
   *
   */
  fermerModalAgentTrouve(): void {
    this.affichageModalAgentTrouve = false;
  }

  /**
   * Crée l'utilisateur.
   *
   */
  creerUtilisateur(): void {
    // cache la fenêtre modale
    this.affichageModalAgentNonTrouve = false;

    // création de l'utilisateur avec des droits sur cette collectivité
    const nouvelUtilisateur: UtilisateurConnecteDto = {
      email: this.emailRechercheAgent,
      droitsAgentCollectivites: [{idCollectivite: this.collectivite.id} as DroitAgentCollectiviteDto]
    } as UtilisateurConnecteDto;

    // création
    this.collectiviteAgentsService.creerUtilisateur(nouvelUtilisateur)
    .pipe(
      switchMap(_ => this.getAgentsCollectivite()),
      tap(listeUsers => this.ajoutPremierAgent.emit(listeUsers.length === 1))
    )
    .subscribe(listeUsers => this.agentsSubject.next(listeUsers));
  }

  /**
   * Change l'état d'un agent : ajoute ou supprime le statut 'admin'.
   *
   * @param idAgent - id de l'agent à modifier
   */
  changerEtatAgent(idAgent: number): void {
    // mise à jour de l'état
    this.collectiviteAgentsService.changerEtatAdminDroitAgent(idAgent, this.collectivite.id)
    .pipe(
      switchMap(_ => this.getAgentsCollectivite())
    )
    .subscribe(listeUsers => {
        this.agentsSubject.next(listeUsers);
      if (idAgent === this.agentConnecte.id) {
        this.logOut();
      }
      });
  }

  /**
   * Ferme/cache la fenêtre modale d'acceptation (ou non) de l'agent NON trouvé.
   *
   */
  fermerModalAgentNonTrouve(): void {
    this.affichageModalAgentNonTrouve = false;
  }

  /**
   * Affiche la modal de confirmation de suppression d'un agent.
   *
   */
  confirmerSupprimerAgentCollectivite(idAgent: number): void {
    this.idAgentSupprimer = idAgent;
    this.affichageModalConfirmSupprAgent = true;
  }

  /**
   * Ferme/cache la modal de confirmation de suppression d'un agent.
   *
   */
  fermerConfirmSupprAgent(): void {
    this.idAgentSupprimer = undefined;
    this.affichageModalConfirmSupprAgent = false;
  }

  /**
   * Supprime l'association d'un agent à une collectivité.
   *
   */
  supprimerAgentCollectivite(): void {
    // fermeture de la fenêtre modale de confirmation de suppression
    this.affichageModalConfirmSupprAgent = false;

    this.collectiviteAgentsService.supprimerAgentCollectivite(this.idAgentSupprimer, this.collectivite.id)
    .pipe(
      switchMap(_ => this.getAgentsCollectivite())
    )
    .subscribe(listeUsers => this.agentsSubject.next(listeUsers));
  }

  /**
   * Indique si l'agent est administrateur sur la collectivite, ou non.
   *
   * @returns true ou false
   */
  isAdminCollectivite(): boolean {
    if (!this.agentConnecte) {
      return false;
    }
    return this.droitService.isAdminCollectivite(this.agentConnecte, this.collectivite.id);
  }

  public bloquerActionSiDernierAdministrateur(utilisateur: DroitAgentCollectiviteDto): boolean {
    return (!this.isAdminCollectivite() || !this.isCollectiviteOuvert() || (this.unSeulAdmin && (utilisateur.admin)));
  }

  /**
   * Obtention des agents de la collectivité en cours.
   * Sinon liste vide.
   *
   * @returns la liste des agents de la collectivité
   */
  private getAgentsCollectivite(): Observable<DroitAgentUtilisateurDto[]> {
    if (!this.collectivite.id) {
      return of([] as DroitAgentUtilisateurDto[]);
    }
    return this.collectiviteAgentsService.getAgentsCollectivite(this.collectivite.id).pipe(
      tap( listeAgents => {
          let count = 0;
          listeAgents?.forEach(agent => {
            if(agent.admin) {
              count++;
            }
          this.unSeulAdmin = (count === 1);
          });
        }
      ));
  }

  logOut = function () {
    this.router.navigateByUrl('/logout');
  };

}

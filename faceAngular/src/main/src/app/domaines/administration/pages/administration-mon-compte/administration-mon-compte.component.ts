// Interne
import { DroitUtilisateur } from '@administration/dto/droit.utilisateur.dto';
import { UtilisateurDto } from '@administration/dto/utilisateur.dto';
import { MonCompteService } from '@administration/service/mon-compte.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurAdminDto } from '@app/services/authentification/dto/utilisateur-admin-dto';
// PrimeNG
import { MessageService } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'app-administration-mon-compte',
  templateUrl: './administration-mon-compte.component.html',
  styleUrls: ['./administration-mon-compte.component.css']
})
export class AdministrationMonCompteComponent implements OnInit, OnDestroy {
  // formulaire ihm
  public userform: FormGroup;

  // objet utilisateur, reflet des données du formulaire
  private objetUtilisateur: UtilisateurDto = {} as UtilisateurDto;

  // sujet à souscrire pour : la maj de la liste des droits
  private droitSubject = new Subject<DroitUtilisateur[]>();

  // observable : liste des droits utilisateur
  public listeDroits$: Observable<DroitUtilisateur[]> = this.droitSubject.asObservable();

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  public userConnecte: UtilisateurConnecteDto;

  /**
   * Creates an instance of MonCompteComponent.
   *
   * @param monCompteService service métier
   * @param messageService service des messages IHM
   * @param formBuilder objet du formulaire
   */
  constructor(
    private monCompteService: MonCompteService,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private formBuilder: FormBuilder,
    private droitService: DroitService
  ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  /**
   * Chargement des données de la page.
   *
   */
  public ngOnInit(): void {
    // init. du formulaire
    this.userform = this.formBuilder.group({
      nom: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      prenom: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      telephone: new FormControl(''),
      email: new FormControl('')
    });

    // re-chargement des données de l'écran à chaque maj utilisateur
    this.droitService.getInfosUtilisateur$()
      .pipe(
        tap(userConnecte => this.userConnecte = userConnecte),
        switchMap(userConnecte => this.monCompteService.getInformationsUtilisateur(userConnecte.id)),
        takeUntil(this.ngDestroyed$)
      ).subscribe((infosUtilisateur: UtilisateurAdminDto) => {
        // mise a jour de l'objet 'utilisateur'
        this.majObjetFormulaire(infosUtilisateur);
        // mise à jour du formulaire
        this.majFormulaire(infosUtilisateur);
        // mise à jour de la liste des collectivités
        this.majListeCollectivites(infosUtilisateur);
      });
  }

  /**
   * Chargement des collectivités.
   *
   */
  private majListeCollectivites(user: UtilisateurAdminDto): void {
    const listeDroits: DroitUtilisateur[] = [];

    Object.values(user.droitsAgentCollectivites).forEach(value => {
      listeDroits.push({
        collectivite: value.nomCourt,
        etat: value.etatCollectivite
      } as DroitUtilisateur);
    });

    this.droitSubject.next(listeDroits);
  }

  /**
   * Met à jour l'objet du formulaire via un DTO.
   *
   * @param dataDto - dto en entrée
   */
  private majObjetFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      this.objetUtilisateur[key] = value;
    });
  }

  /**
   * Met à jour le formulaire via un DTO.
   *
   * @param dataDto - dto en entrée
   */
  private majFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      if (this.userform.get(key) != null) {
        this.userform.get(key).setValue(value);
      }
    });
  }

  /**
   * Modification de l'utilisateur
   *
   */
  public modifierUtilisateur(): void {
    // mapping des champs du formulaire vers l'objet utilisateur à envoyer au WS
    this.majObjetFormulaire(this.userform.value);

    // appel du service de maj utilisateur
    this.monCompteService.modifierUtilisateur(this.objetUtilisateur)
      .subscribe((utilisateurMaj: UtilisateurDto) => {
        // mise à jour de l'objet formulaire via les données reçues (pour maj version)
        this.majObjetFormulaire(utilisateurMaj);

        // message de succès
        this.messageManagerService.modificationEnregistree();


        // mise à jour du store applicatif
        this.droitService.updateUtilisateurConnecteViaUtilisateurDto(utilisateurMaj);
      });
  }

  /**
   * Annuler : rechargement des dernières données.
   *
   */
  public annuler(): void {
    this.messageService.clear();
    this.majFormulaire(this.objetUtilisateur);
  }

  public isUniquementGenerique(): boolean {
    return this.droitService.isUniquementGenerique(this.userConnecte);
  }
}

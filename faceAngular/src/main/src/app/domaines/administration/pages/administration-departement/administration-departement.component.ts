import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { MessageService } from 'primeng/api';
import { DepartementDto } from '@app/dto/departement.dto';

@Component({
  selector: 'adm-departement-departement',
  templateUrl: './administration-departement.component.html',
  styleUrls: ['./administration-departement.component.css']
})
export class AdministrationDepartementComponent implements OnInit {


  hide = true;

  public modificationDetecteeAdresse = false;
  public modificationDetecteeDonneesFiscales = false;

  public departements: DepartementLovDto[];

  public selectedDepartement: DepartementLovDto;

  public isUserUniquementGenerique: boolean;

  public ngDestroyed$ = new Subject();

  departement: DepartementDto;
  filteredDepartements: any[];

  constructor(private referentielService: ReferentielService,
    private droitService: DroitService,
    private messageService: MessageService, ) { }

    onChangeDepartement(departement: DepartementLovDto) {
    if (this.hide === true) {
      this.hide = false;
    }

    this.referentielService.rechercherDepartement$(departement.id).subscribe( data => {
      this.departement = data;
    })
  }

  ngOnInit() {

    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur =>
        this.initialiserIsUserUniquementGenerique(infoUtilisateur)
      );
    // récupération des départements
    if (!this.isUserUniquementGenerique) {
      this.referentielService
        .rechercherTousDepartements$()
        .subscribe(data => (this.departements = data));
    }

  }

  /**
  * Initialise le flag de profil de l'utilisateur.
  */
  private initialiserIsUserUniquementGenerique(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    this.isUserUniquementGenerique = this.droitService.isUniquementGenerique(
      infosUtilisateur
    );
  }
}


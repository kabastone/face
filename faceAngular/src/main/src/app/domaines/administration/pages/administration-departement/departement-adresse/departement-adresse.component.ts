import { Component, OnInit, Input, OnChanges, OnDestroy, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { AdresseDto } from '@administration/dto/adresse.dto';
import { DepartementService } from '../service/departement.service';
import { DepartementDto } from '@app/dto/departement.dto';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'adm-departement-adresse',
  templateUrl: './departement-adresse.component.html',
  styleUrls: ['./departement-adresse.component.css']
})
export class DepartementAdresseComponent implements OnInit, OnChanges, OnDestroy {

  civilites: SelectItem[];

  selectedCivilite: SelectItem;

  @Input()
  modificationDetectee: boolean;
  @Output()
  modificationDetecteeChange = new EventEmitter<boolean>();

  @Input()
  departement: DepartementDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  // Containers pour la gestion de l'adresse.
  adresseForm: FormGroup;
  adresse = new AdresseDto();


  constructor(
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private formBuilder: FormBuilder,
    private departementService: DepartementService,
    ) {
    this.civilites = [
      {label: 'Mme', value: 'Madame'},
      {label: 'M.', value: 'Monsieur'}];
  }

  ngOnInit() {
    this.adresseForm = this.initialiserForm();

    this.adresseForm.valueChanges.subscribe( data => {
      if (this.adresseForm.dirty && this.modificationDetectee === false) {
        this.modificationDetectee = true;
        this.modificationDetecteeChange.emit(true);
        // Message d'avertissement
        this.messageManagerService.modificationDetecteMessage();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.departement) {
      this.departementService.rechercherAdresseDepartement(this.departement.id)
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe( data => {
        if (data != null) {
          this.recuperationAdresse(data);
          this.adresse = data;
        } else {
          this.adresseForm.reset();
        }
        this.messageService.clear();
      });
    }
  }

  ngOnDestroy(): void {
    this.ngDestroyed$.next();
  }

  public annuler(): void {
    this.adresseForm.reset();
    this.recuperationAdresse(this.adresse);
    this.modificationDetectee = false;
    this.modificationDetecteeChange.emit(false);
    this.messageService.clear();
  }

  public enregistrerAdresse(): void {
    this.obtenirAdresseParForm();
    this.departementService.envoyerAdresseEnregistrement(this.departement.id, this.adresse)
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe( data => {
      if (data != null) {
        this.modificationDetectee = false;
        this.modificationDetecteeChange.emit(false);
        // Message de confirmation
        this.messageManagerService.enregistrerAdresseMessage();
      }
    });
  }

  private obtenirAdresseParForm() {
    Object.entries(this.adresseForm.value).forEach(([key, value]) => {
      if ( key === 'civilite') {
        this.adresse[key] = (value as SelectItem).value;
      } else {
        this.adresse[key] = value;
      }
    });
  }

  private recuperationAdresse(adresseDto: AdresseDto){
    this.adresseForm.reset();
    Object.entries(adresseDto).forEach(([key, value]) => {
      if (value != null && this.adresseForm.get(key) != null) {
        if ( key === 'civilite') {
          this.adresseForm.get(key).setValue(this.civilites.find(x => x.value === value));
        } else {
          this.adresseForm.get(key).setValue(value);
        }
      }
    });
  }

  private initialiserForm(): FormGroup {
    return this.formBuilder.group({
      prenom: new FormControl(''),
      civilite: new FormControl(''),
      qualiteDestinataire: new FormControl(''),
      numeroVoie: new FormControl(''),
      complement: new FormControl(''),
      nom: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      nomVoie: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      codePostal: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      commune: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
    }, {
      validators: (formGroup: FormGroup) => {
        return this.validateGroup(formGroup);
      }
    });
  }

  private validateGroup(formGroup: FormGroup): any {
    Object.entries(formGroup.value).forEach(([key, value]) => {
      if (formGroup.controls[key].invalid) {
        return { validateGroup: { valid: false } };
      }
    });

    return null;
  }

}

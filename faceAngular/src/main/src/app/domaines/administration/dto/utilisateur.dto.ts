export interface UtilisateurDto {
  id: string;
  nom: string;
  prenom: string;
  telephone: string;
  email: string;
  cerbereId: string;
  version: number;
}

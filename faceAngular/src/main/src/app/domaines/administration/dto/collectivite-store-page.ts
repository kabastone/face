import { StorePage } from '@app/services/store-page/store-page';
import { CollectiviteSimpleDto } from '@app/services/authentification/dto/collectivite-simple-dto';

export interface CollectiviteStorePage extends StorePage {
  collectiviteSelectionee: CollectiviteSimpleDto;
  modificationDetectee: boolean;
  modificationEnregistree: boolean;
}

export class AdresseDto {
  id: string;
  civilite: string;
  nom: string;
  prenom: string;
  qualiteDestinataire: string;
  numeroVoie: string;
  nomVoie: string;
  complement: string;
  codePostal: string;
  commune: string;
  version: string;
}

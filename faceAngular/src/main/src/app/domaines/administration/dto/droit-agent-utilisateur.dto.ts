export interface DroitAgentUtilisateurDto {
  idUtilisateur: string;
  email: string;
  nom: string;
  prenom: string;
  telephone: string;
  admin: boolean;
  idCollectivite: string;
}

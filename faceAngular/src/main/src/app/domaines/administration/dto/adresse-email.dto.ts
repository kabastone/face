export interface AdresseEmailDto {
  id: string;
  email: string;
  idCollectivite: string;
}

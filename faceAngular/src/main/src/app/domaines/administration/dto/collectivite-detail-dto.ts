import { AdresseDto } from './adresse.dto';
import { DroitAgentUtilisateurDto } from './droit-agent-utilisateur.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';

export class CollectiviteDetailDto {
  id: number;
  nomLong: string;
  nomCourt: string;
  numCollectivite: string;
  siret: string;

  etatCollectivite: string;

  receveurNom: string;
  receveurNumCodique: string;

  avecMoaMoeIdentiques = false;
  avecPlusieursMoe = false;

  listeDiffusion: String[] = [];

  adresseMoa: AdresseDto;
  adresseMoe: AdresseDto;

  departement: DepartementLovDto;

  listeAgents: DroitAgentUtilisateurDto[];

  version: number;
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UtilisateurDto } from '../dto/utilisateur.dto';
import { DroitUtilisateur } from '../dto/droit.utilisateur.dto';
import { HttpService } from '@app/services/http/http.service';
import { UtilisateurAdminDto } from '@app/services/authentification/dto/utilisateur-admin-dto';

@Injectable({
  providedIn: 'root'
})
export class MonCompteService {
  constructor(
    private httpService: HttpService
  ) { }

  /**
   * Obtention des informations utilisateur
   *
   * @param id utilisateur à lire
   * @returns l'utilisateur
   */
  getInformationsUtilisateur(id: number): Observable<UtilisateurAdminDto> {
    return this.httpService.envoyerRequeteGet(`/utilisateur/id/${id}/rechercher`);
  }

  /**
   * Obtention des droits utilisateur
   *
   * @param id utilisateur à lire
   * @returns les droits utilisateur
   */
  getDroitsUtilisateur(id: string): Observable<DroitUtilisateur[]> {
    return this.httpService.envoyerRequeteGet(`/droits-utilisateur/${id}/lire`);
  }

  /**
   * Modification de l'utilisateur
   *
   * @param utilisateur à modifier
   * @returns utilisateur modifié
   */
  modifierUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
    return this.httpService.envoyerRequetePost(`/utilisateur/${utilisateur.id}/modifier`, utilisateur);
  }

  /**
   * Ajout d'un l'utilisateur
   *
   * @param utilisateur à modifier
   * @returns utilisateur modifié
   */
  ajouterUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
    return this.httpService.envoyerRequetePost('/utilisateur/creer', utilisateur);
  }

  /**
   * Suppression d'un l'utilisateur
   *
   * @param utilisateur à modifier
   * @returns utilisateur modifié
   */
  supprimerUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
    return this.httpService.envoyerRequeteDelete(`/utilisateur/${utilisateur.id}/supprimer`);
  }
}

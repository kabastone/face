import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppConfigService } from '@app/config/app-config.service';

// interne
import { AdministrationCollectiviteModule } from './pages/administration-collectivite/administration-collectivite.module';
import { AdministrationDepartementModule } from './pages/administration-departement/administration-departement.module';
import { AdministrationMonCompteModule } from './pages/administration-mon-compte/administration-mon-compte.module';
import { AdministrationRouteModule } from './administration.routes';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdministrationCollectiviteModule,
    AdministrationDepartementModule,
    AdministrationMonCompteModule,
    AdministrationRouteModule
  ],
  providers: [AppConfigService]
})
export class AdministrationModule { }

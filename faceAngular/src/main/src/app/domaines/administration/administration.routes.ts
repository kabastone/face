import { RouterModule, Routes } from '@angular/router';

// Interne
import { AdministrationMonCompteComponent } from './pages/administration-mon-compte/administration-mon-compte.component';
import { AdministrationCollectiviteComponent } from './pages/administration-collectivite/administration-collectivite.component';
import { AdministrationDepartementComponent } from './pages/administration-departement/administration-departement.component';
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { NgModule } from '@angular/core';

export const ADMINISTRATION_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'mon-compte',
    pathMatch: 'full',
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'mon-compte',
    component: AdministrationMonCompteComponent,
    data: { title: 'Mon compte' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'collectivite',
    component: AdministrationCollectiviteComponent,
    data: { title: 'Collectivité' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'departement',
    component: AdministrationDepartementComponent,
    data: { title: 'Département' },
    resolve: { componentMsg: ComponentResolverMsg }
  }

];

const routes: Routes = [
  {
    path: '',
    children: ADMINISTRATION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
  
]

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class AdministrationRouteModule {}

import { NgModule } from '@angular/core';
import { DemandeProlongationComponent } from './pages/demande-prolongation/demande-prolongation.component';
import { CalendarModule } from 'primeng/calendar';
import { ProlongationRouteModule } from './prolongation.routes';
import { SharedModule } from '@app/share/shared.module';

@NgModule({
  declarations: [
    DemandeProlongationComponent
  ],
  imports: [
    CalendarModule,
    ProlongationRouteModule,
    SharedModule
  ]
})
export class ProlongationModule { }

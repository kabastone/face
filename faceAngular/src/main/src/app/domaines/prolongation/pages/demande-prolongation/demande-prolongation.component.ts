import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DemandeProlongationDto } from '@app/domaines/prolongation/dto/demande-prolongation-dto';
import { DemandeProlongationForm } from '@app/domaines/prolongation/dto/demande-prolongation-form';
import { GestionAffichagesChampsProlongationService } from '@app/domaines/prolongation/services/gestion-affichages-champs-prolongation.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DemandeProlongationService } from '../../services/demande-prolongation.service';
import { DatePipe } from '@angular/common';
import { FileSaverService } from '@app/services/commun/file-saver.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CustomFileTransferComponent } from '@component/custom-file-transfer/custom-file-transfer.component';

@Component({
  selector: 'app-demande-prolongation',
  templateUrl: './demande-prolongation.component.html',
  styleUrls: ['./demande-prolongation.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class DemandeProlongationComponent implements OnInit, OnDestroy {

  // mon formulaire
  dossierForm: FormGroup;

  // infos utilisateur
  public infosUtilisateur: UtilisateurConnecteDto = {} as UtilisateurConnecteDto;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  form: DemandeProlongationForm;

  prolongation: DemandeProlongationDto;

  // id du dossier de subvention (url)
  public idDossierSubvention = +this.route.snapshot.paramMap.get('idDossier');

  // id du dossier en cours
  public idProlongation: number = +this.route.snapshot.paramMap.get('idProlongation');

  // formulaire dédié à l'upload de fichiers (association : TypeDocument => fichier)
  public formulaireFichiers: FormData = new FormData();

  public mapDocuments: Map<string, number>;

  // nom du fichier
  public documentName = 'Document introuvable';

  @ViewChild('fileTransfer')
  customFileTransferComponent: CustomFileTransferComponent;
  achevementEcheanceDemande: Date;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private droitService: DroitService,
    private formBuilder: FormBuilder,
    private gestionChampsProlongation: GestionAffichagesChampsProlongationService,
    private demandeProlongationService: DemandeProlongationService,
    private datepipe: DatePipe,
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private fileSaverService: FileSaverService,
    private confirmationService: ConfirmationService,
  ) {

    // mise à jour des infos utilisateur
    this.droitService.getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        infosUser => {
          if (!infosUser.id) {
            return;
          }
          this.infosUtilisateur = infosUser;
        }
      );

    this.form = gestionChampsProlongation.default();
    this.createFormBuilder();
  }

  ngOnInit() {
    if (this.idProlongation) {
      // Prolongation déjà créée
      this.rechercherDemandeProlongation();
    } else if (this.idDossierSubvention) {
      // Création de la prolongation
      this.creerDemandeProlongation();
    } else {
      console.log('[TODO] CAS NON FONCTIONNEL');
    }
  }

  public isDemandeProlongationExistante() {
    if (this.idProlongation) {
      return true;
    }
    return false;
  }

  private rechercherDemandeProlongation(): void {
    // récupération des infos de la collectivité sélectionnée
    this.demandeProlongationService.getDemandeProlongationDto$(this.idProlongation)
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      data => {
        this.prolongation = data;
        this.chargementMetadataForm();
        this.createFormBuilder();
        this.chargementDonneesEcran(data);
        if (this.prolongation.documents[0] != null) {
          this.documentName = this.prolongation.documents[0].nomFichierSansExtension + '.'
            + this.prolongation.documents[0].extensionFichier;
        }
      }
    );
  }

  private creerDemandeProlongation(): void {
    // Lors de la création
    this.demandeProlongationService.initDemandeProlongationDto$(String(this.idDossierSubvention))
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        data => {
          this.prolongation = data;
          this.chargementMetadataForm();
          this.createFormBuilder();
          this.chargementDonneesEcran(data);
        }
      );
  }

  private chargementMetadataForm(): void {
    this.form = this.gestionChampsProlongation.recupererFormulaire(this.prolongation.etat, this.infosUtilisateur.listeRoles);
  }

  private chargementDonneesEcran(demandeProlongation: DemandeProlongationDto): void {
    if (demandeProlongation.etat) {
      demandeProlongation.etatLibelle = this.gestionChampsProlongation.recupererEtatLibelle(
        demandeProlongation.etat,
        this.infosUtilisateur.listeRoles
      );
    }
    Object.entries(demandeProlongation).forEach(([key, value]) => {
      if (value != null && this.dossierForm.get(key) != null) {
        this.dossierForm.get(key).setValue(value);
      }
    });
    // récupération de la liste des documents
    this.mapDocuments = new Map<string, number>();
    Object.values(demandeProlongation.documents).forEach(document => {
        this.mapDocuments.set(document.codeTypeDocument, document.id);
    });
  }

  public afficherInstruction(): boolean {
    return this.prolongation && this.prolongation.etat !== 'INITIAL';
  }

  private createFormBuilder(): void {
    this.dossierForm = this.formBuilder.group({
      numDossier: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      dateEcheanceAchevement: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      dateDemande: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      dateEcheanceAchevementDemande: new FormControl(
        null,
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      etat: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      motifRefus: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      )
    });
  }

  /**
   * Ajout d'un fichier à uploader.
   *
   * @param typeDocument type du document envoyé
   * @param event evenement contenant le fichier envoyé
   */
  public ajoutFichierUploader(typeDocument: string, event: any) {
    const fichier: File = event.target.files[0];

    // ajout du fichier
    if (event.target.files && fichier) {
      this.formulaireFichiers.append(typeDocument, fichier);
    }
  }


  initFormData(avecFormPrincipal?: boolean) {

    // init.
    const formulaireAvecFichiers = new FormData();
    if (avecFormPrincipal) {
      // mapping des valeurs du formulaire
      Object.keys(this.dossierForm.value).forEach(key => {
        let value = this.dossierForm.get(key).value;
        if (value && value.code) {
          // s'il s'agit d'un menu déroulant
          // on prend juste la value et non l'objet en entier
          value = value.code;
        }
        if (value !== null) {
          formulaireAvecFichiers.append(key, value);
        }
      });
      // mapping champs speciaux ou cachés
      formulaireAvecFichiers.append('version', String(this.prolongation.version));
      formulaireAvecFichiers.append('etat', this.prolongation.etat);
      formulaireAvecFichiers.append('dateDemande', String(this.prolongation.dateDemande));
    }
    const value = String(this.datepipe.transform(this.dossierForm.get('dateEcheanceAchevementDemande').value, 'dd/MM/yyyy'));
    formulaireAvecFichiers.delete('dateEcheanceAchevementDemande');
    formulaireAvecFichiers.append('dateEcheanceAchevementDemande', value);

    // mapping des fichiers uploadés
    for (const fichier of this.formulaireFichiers.entries()) {
      formulaireAvecFichiers.append(fichier[0], fichier[1]);
    }

    // mapping des id

    if (this.prolongation.idDossierSubvention) {
      // pour une demande existante
      formulaireAvecFichiers.append('idDossierSubvention', String(this.prolongation.idDossierSubvention));
    }
    return formulaireAvecFichiers;
  }

  /**
  * A la destruction : on lance le désabonnement de tous les subscribes()
  *
  */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  public annuler() {
    // Chargement des données de la page
    this.chargementMetadataForm();
    this.createFormBuilder();
    this.chargementDonneesEcran(this.prolongation);

    // Suppression du fichier téléchargé
    for (const fichier of this.formulaireFichiers.entries()) {
      this.formulaireFichiers.delete(fichier[0]);
    }
    // Reset du label du champ télécharger
    this.customFileTransferComponent.reset();

    this.messageService.clear();
  }

  public accorder(): void {
    this.demandeProlongationService.accorderDemandeSubvention(this.prolongation.id).subscribe(
      _ => {
        this.reload(+this.prolongation.id);
        this.sendSuccessMessage();
      });
  }

  public refuser(): void {
    const version = this.prolongation.version;
    this.majObjetFormulaire(this.dossierForm.value);
    this.demandeProlongationService.refuserDemandeProlongation(this.prolongation).subscribe(
      data => {
        this.reload(+this.prolongation.id);
        this.sendSuccessMessage();
      }
    );
  }

  public demander() {

      this.confirmationService.confirm({
        message: 'La date d\'échéance d\'achèvement demandée implique une plolongation \
                  sur l\' année ' + this.achevementEcheanceDemande.getFullYear() + ', par conséquent , vous devez mettre\
                   à jour votre cadencement dans l\'onglet " subvention " une fois votre demande de prolongation acceptée.',
        header: 'Confirmation de prolongation',
        icon: 'pi pi-info-circle',
        accept : () => {
          const formulaireAvecFichiers = this.initFormData(true);
          this.demandeProlongationService.creerDemandeProlongation(formulaireAvecFichiers).subscribe(
            data => {
              this.router.navigateByUrl('/prolongation/demande/' + data.id);
              this.sendSuccessMessage();
            });
        },
        reject: () => {

        }
      })
    
  }

  public reload(id: number): void {
    this.ngOnInit();
  }

  /**
   * Telecharger un document.
   */
  public telechargerDocumentProlongation(typeDocument: string) {
    const idDocument = this.mapDocuments.get(typeDocument);

    if (!idDocument) {
      // message d'avertissement
      this.messageManagerService.documentIntrouvableMessage();

      return;
    }

    // téléchargement par id
    this.demandeProlongationService.telechargerDocument(idDocument).subscribe(response => {
      this.fileSaverService.telechargerFichier(response);
    });

  }

  private majObjetFormulaire(dataDto: any): void {
    Object.entries(dataDto).forEach(([key, value]) => {
      this.prolongation[key] = value;
    });
  }

  private sendSuccessMessage(): void {
    // message de succès
      this.messageManagerService.successMessage();
    }

  public isDateProlongationValid():  boolean {
    const dateAchevementdemandeForm = String(this.datepipe.transform(this.dossierForm.get('dateEcheanceAchevementDemande').value, 'dd/MM/yyyy'));
    const dateAchevementForm = this.dossierForm.get('dateEcheanceAchevement').value;

    this.achevementEcheanceDemande = new Date(parseInt(dateAchevementdemandeForm.substr(6, 4), 10),
                                                          parseInt(dateAchevementdemandeForm.substr(3, 2), 10) - 1,
                                                          parseInt(dateAchevementdemandeForm.substr(0, 2), 10));
                          
     const echeanceAchevement = new Date(parseInt(dateAchevementForm.substr(6, 4), 10),
                                                          parseInt(dateAchevementForm.substr(3, 2), 10) - 1,
                                                          parseInt(dateAchevementForm.substr(0, 2), 10));
    
    return this.achevementEcheanceDemande.getTime() >= new Date(
      new Date(echeanceAchevement).setMonth(echeanceAchevement.getMonth() + 12)
      ).getTime();
  }
}

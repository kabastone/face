import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubventionDemandeProlongationComponent } from './subvention-demande-prolongation.component';

describe('SubventionDemandeProlongationComponent', () => {
  let component: SubventionDemandeProlongationComponent;
  let fixture: ComponentFixture<SubventionDemandeProlongationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubventionDemandeProlongationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubventionDemandeProlongationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

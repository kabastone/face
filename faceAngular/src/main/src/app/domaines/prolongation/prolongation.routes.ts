import { RouterModule, Routes } from '@angular/router';
import { DemandeProlongationComponent } from './pages/demande-prolongation/demande-prolongation.component';
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { NgModule } from '@angular/core';

export const PROLONGATION_ROUTES: Routes = [
  {
    path: 'demande/:idProlongation',
    component: DemandeProlongationComponent,
    data: { title: 'Demande de prolongation' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'demande/dossier/:idDossier',
    component: DemandeProlongationComponent,
    data: { title: 'Demande de prolongation' },
    resolve: { componentMsg: ComponentResolverMsg }
  }
];
const routes: Routes = [
  {
    path: '',
    children: PROLONGATION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },

]

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class ProlongationRouteModule {}

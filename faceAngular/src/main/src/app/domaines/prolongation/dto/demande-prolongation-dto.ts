
import { DocumentDto } from '@app/services/authentification/dto/document.dto';

export interface DemandeProlongationDto {

  id: string;
  idDossierSubvention: string;
  version: number;
  numDossier: string;
  dateEcheanceAchevement: Date;
  dateDemande: Date;
  dateEcheanceAchevementDemande: Date;
  etat: string;
  etatLibelle: string;
  motifRefus: string;
  documents: DocumentDto[];

  /* Département et collectivité */
  nomLongCollectivite: string;
  codeDepartement: string;
}

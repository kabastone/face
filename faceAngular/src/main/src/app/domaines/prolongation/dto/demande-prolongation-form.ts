import { MetadataForm } from '@app/dto/metadata-form';

export interface DemandeProlongationForm {
  /* Regroupement Dossier */
  numDossier: MetadataForm;
  dateEcheanceAchevement: MetadataForm;

  /* Regroupement Prolongation */
  dateDemande: MetadataForm;
  dateEcheanceAchevementDemande: MetadataForm;
  demandeSignee: MetadataForm;

  btnDemander: MetadataForm;
  btnAnnuler: MetadataForm;

  /* Regroupement Instruction */
  etat: MetadataForm;
  motifRefus: MetadataForm;

  btnAccorder: MetadataForm;
  btnRefuser: MetadataForm;

}

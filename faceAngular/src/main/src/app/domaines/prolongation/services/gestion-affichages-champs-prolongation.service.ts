import { Injectable } from '@angular/core';
import { DemandeProlongationForm } from '@app/domaines/prolongation/dto/demande-prolongation-form';
import { MetadataForm } from '@app/dto/metadata-form';

@Injectable({
  providedIn: 'root'
})
export class GestionAffichagesChampsProlongationService {

  constructor() { }

  private readonly EDITABLE: MetadataForm = { visible: true, editable: true };
  private readonly VISIBLE: MetadataForm = { visible: true, editable: false };
  private readonly NONE: MetadataForm = { visible: false, editable: false };

  public default(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.NONE;
    form.dateEcheanceAchevement = this.NONE;

    /* Regroupement Prolongation */
    form.dateDemande = this.NONE;
    form.dateEcheanceAchevementDemande = this.NONE;
    form.demandeSignee = this.NONE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.NONE;
    form.motifRefus = this.NONE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  }

  public recupererFormulaire(etat: string, roles: string[]): DemandeProlongationForm {
    const nouvelEtat: string = this.definirEtatSelonRole(etat, roles);
    return this.recupererFormulaireSelonEtat(nouvelEtat);
  }

  private definirEtatSelonRole(etat: string, roles: string[]) {
    let etatAccessible = false;

    if (roles.includes('MFER_INSTRUCTEUR') || roles.includes('MFER_RESPONSABLE')) {
      switch (etat) {
        case 'DEMANDEE':
        case 'ACCORDEE':
        case 'VALIDEE':
        case 'REFUSEE':
        case 'INCOHERENCE_CHORUS':
          etatAccessible = true;
      }
    }

    if (!etatAccessible && roles.includes('GENERIQUE')) {
      switch (etat) {
        case 'INITIAL':
        case 'ACCORDEE':
        case 'VALIDEE':
          etatAccessible = true;
      }
    }

    if (etatAccessible) {
      return etat;
    }

    return 'EN_INSTRUCTION';
  }


  public recupererEtatLibelle(etat: string, roles: string[]) {
    const nouvelEtat: string = this.definirEtatSelonRole(etat, roles);
    switch (nouvelEtat) {
      case 'DEMANDEE':
        return 'Demandée';
      case 'ACCORDEE':
        return 'Accordée';
      case 'INITIAL':
        return 'Initial';
      case 'REFUSEE':
        return 'Refusée';
      case 'INCOHERENCE_CHORUS':
        return 'Incohérence avec CHORUS';
      case 'EN_INSTRUCTION':
      default:
        return 'En instruction';
    }
  }

  private recupererFormulaireSelonEtat(etat: string): DemandeProlongationForm {
    switch (etat) {
      case 'INITIAL': {
        return this.getFormForInitial();
      }
      case 'DEMANDEE': {
        return this.getFormForDemandee();
      }
      case 'ACCORDEE': {
        return this.getFormForAccordee();
      }
      case 'INCOHERENCE_CHORUS': {
        return this.getFormForIncoherenceChorus();
      }
      case 'VALIDEE': {
        return this.getFormForValidee();
      }
      case 'REFUSEE': {
        return this.getFormForRefusee();
      }
      case 'EN_INSTRUCTION': {
        return this.getFormForEnInstruction();
      }
    }

    return null;
  } // recupererFormulaireSelonEtat

  // Etat Initial
  private getFormForInitial(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.EDITABLE;
    form.demandeSignee = this.EDITABLE;

    form.btnDemander = this.EDITABLE;
    form.btnAnnuler = this.EDITABLE;

    /* Regroupement Instruction */
    form.etat = this.NONE;
    form.motifRefus = this.NONE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForInitial

  // Etat Demandee
  private getFormForDemandee(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.EDITABLE;

    form.btnAccorder = this.EDITABLE;
    form.btnRefuser = this.EDITABLE;

    return form;
  } // getFormForDemandee

  // Etat Accordee
  private getFormForAccordee(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.NONE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForAccordee

  // Etat Validee
  private getFormForValidee(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.NONE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForValidee

  // Etat IncoherenceChorus
  private getFormForIncoherenceChorus(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.VISIBLE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForIncoherenceChorus

  // Etat Refusee
  private getFormForRefusee(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.VISIBLE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForRefusee

  // Etat EnInstruction
  private getFormForEnInstruction(): DemandeProlongationForm {
    const form: DemandeProlongationForm = {} as DemandeProlongationForm;

    /* Regroupement Dossier */
    form.numDossier = this.VISIBLE;
    form.dateEcheanceAchevement = this.VISIBLE;

    /* Regroupement Prolongation */
    form.dateDemande = this.VISIBLE;
    form.dateEcheanceAchevementDemande = this.VISIBLE;
    form.demandeSignee = this.VISIBLE;

    form.btnDemander = this.NONE;
    form.btnAnnuler = this.NONE;

    /* Regroupement Instruction */
    form.etat = this.VISIBLE;
    form.motifRefus = this.NONE;

    form.btnAccorder = this.NONE;
    form.btnRefuser = this.NONE;

    return form;
  } // getFormForEnInstruction


}

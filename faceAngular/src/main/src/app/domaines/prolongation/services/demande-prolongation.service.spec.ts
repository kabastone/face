import { TestBed } from '@angular/core/testing';

import { DemandeProlongationService } from './demande-prolongation.service';

describe('DemandeProlongationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemandeProlongationService = TestBed.get(DemandeProlongationService);
    expect(service).toBeTruthy();
  });
});

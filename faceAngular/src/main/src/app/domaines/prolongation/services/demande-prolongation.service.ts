import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { DemandeProlongationDto } from '../dto/demande-prolongation-dto';

@Injectable({
  providedIn: 'root'
})
export class DemandeProlongationService {

  constructor(
    private httpService: HttpService
  ) { }


  /**
   * initialiser une Demande de Prolongation.
   *
   * @param idDossier
   * @returns
   */
  initDemandeProlongationDto$(idDossier: string): Observable<DemandeProlongationDto> {
    return this.httpService.envoyerRequeteGet(`/prolongation/demande/dossier/${idDossier}/initialiser`);
  }

  /**
   * Créer une demande de prolongation.
   *
   * @param data
   * @returns
   */
  creerDemandeProlongation(data: FormData): Observable<DemandeProlongationDto> {
    return this.httpService.envoyerRequetePostPourUpload(`/prolongation/demande/creer`, data);
  }

  /**
   * Télécharger un document.
   *
   * @param idDocument
   * @returns
   */
  telechargerDocument(idDocument: number) {
    return this.httpService.envoyerRequeteGetPourDownload(`/document/${idDocument}/telecharger`);
  }

  /**
   * Rechercher une demande de prolongation.
   *
   * @param idProlongation
   * @returns
   */
  getDemandeProlongationDto$(idProlongation: number): Observable<DemandeProlongationDto>  {
    return this.httpService.envoyerRequeteGet(`/prolongation/demande/${idProlongation}/rechercher`);
  }

  /**
   * Accorder une demande de prolongation.
   *
   * @param id
   * @returns
   */
  accorderDemandeSubvention(id: string): Observable<DemandeProlongationDto> {
    return this.httpService.envoyerRequeteGet(`/prolongation/demande/${id}/accorder`);
  }

  /**
   * Refuser une demande de prolongation.
   *
   * @param prolongation
   * @returns
   */
  refuserDemandeProlongation(prolongation: DemandeProlongationDto): Observable<DemandeProlongationDto> {
    return this.httpService.envoyerRequetePost(`/prolongation/demande/${prolongation.id}/refuser`, prolongation);
  }
}

export interface DossierSubventionTableMferDto {
    idDossier: number;
    
    departementCode: string;

    departementNom: string;

    collectiviteNomCourt: string;

    numDossier: string;

    dossierNumEJ: string;


}
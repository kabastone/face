export interface CritereRechercheCadencementDto {

  /** annee programmtion. */
  anneeProgrammation: number;

  /** The id departement. */
  idDepartement: number;

  /** The id collectivite. */
  idCollectivite: number;
}

export interface ResultatRechercheCadencementDto {

  sousProgrammeCode: String;

  sousProgrammeAbreviation: String;

  sousProgrammeDescription: String;

  montantSubventions: number;

  cadencementAnnee1: number;

  cadencementAnnee2: number;

  cadencementAnnee3: number;

  cadencementAnnee4: number;

  cadencementAnneeProlongation: number;

  cadencementEstValide: boolean;
}

import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable } from 'rxjs';
import { CritereRechercheCadencementDto } from '../dto/critere-recherche-cadencement.dto';
import { ResultatRechercheCadencementDto } from '../dto/resultat-recherche-cadencement.dto';

@Injectable({
  providedIn: 'root'
})
export class GestionEtatGeneralService {

  constructor(private httpService: HttpService) { }

  getResultatRechercheCadencementDto$(criteresRecherche: CritereRechercheCadencementDto, codeProgramme: string):
    Observable<ResultatRechercheCadencementDto[]> {
    return this.httpService.envoyerRequetePost(`/subvention/cadencement/criteres/code/${codeProgramme}/rechercher`, criteresRecherche);
  }
}

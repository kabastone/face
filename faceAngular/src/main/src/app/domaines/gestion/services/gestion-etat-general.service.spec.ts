import { TestBed } from '@angular/core/testing';

import { GestionEtatGeneralService } from './gestion-etat-general.service';

describe('GestionEtatGeneralService', () => {
  let service: GestionEtatGeneralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionEtatGeneralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';

// PrimeNG
import { GestionEtatGeneralModule } from './pages/gestion-etat-general/gestion-etat-general.module';
import { GestionAodeDefautModule } from './pages/gestion-aode-defaut/gestion-aode-defaut.module';
import { GestionTravauxRetardParentComponent } from './pages/gestion-travaux-retard/gestion-travaux-retard-parent/gestion-travaux-retard-parent.component';
import { GestionTravauxRetardProgrammeComponent } from './pages/gestion-travaux-retard/gestion-travaux-retard-programme/gestion-travaux-retard-programme.component';
import { GestionRouteModule } from './gestion.routes';
import { SharedModule } from '@app/share/shared.module';
import { DatePipe } from '@angular/common';



@NgModule({
  declarations: [
    GestionTravauxRetardParentComponent, 
    GestionTravauxRetardProgrammeComponent],
  imports: [
    GestionEtatGeneralModule,
    GestionAodeDefautModule,
    GestionRouteModule,
    SharedModule
  ]
})
export class GestionModule { }

import { RouterModule, Routes } from '@angular/router';

// Interne
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { GestionAodeDefautParentComponent } from './pages/gestion-aode-defaut/gestion-aode-defaut-parent/gestion-aode-defaut-parent.component';
import { GestionTravauxRetardParentComponent } from './pages/gestion-travaux-retard/gestion-travaux-retard-parent/gestion-travaux-retard-parent.component';
import { GestionEtatGeneralParentComponent } from './pages/gestion-etat-general/gestion-etat-general-parent/gestion-etat-general-parent.component';
import { NgModule } from '@angular/core';


export const GESTION_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'cadencement/etat-general',
    pathMatch: 'full',
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'cadencement/etat-general',
    component: GestionEtatGeneralParentComponent,
    data: { title: 'Etat général du cadencement' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'cadencement/aode-defaut',
    component: GestionAodeDefautParentComponent,
    data: { title: 'Cadencement - AODE en défaut' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'engagement-travaux-retard',
    component: GestionTravauxRetardParentComponent,
    data: { title: 'Engagement des travaux - AODE en défaut' },
    resolve: { componentMsg: ComponentResolverMsg }
  }
];

const routes: Routes = [
  {
    path: '',
    children: GESTION_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
]

@NgModule({
  exports:[RouterModule],
  imports:[RouterModule.forChild(routes)],
  providers: [ComponentResolverMsg]
})
export class GestionRouteModule {}
import { TestBed } from '@angular/core/testing';

import { GestionAodeDefautService } from './gestion-aode-defaut.service';

describe('GestionAodeDefautService', () => {
  let service: GestionAodeDefautService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionAodeDefautService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

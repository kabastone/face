import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpService } from '@app/services/http/http.service';
import { Observable, Subject } from 'rxjs';
import { DossierSubventionTableMferDto } from '../dto/dossier-subvention-table-mfer-dto';

@Injectable({
  providedIn: 'root'
})
export class GestionAodeDefautService {

  constructor(private httpService: HttpService) { }

  rechercherAodeDefautDto$(codeProgramme:string) : Observable<DossierSubventionTableMferDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/defaut/${codeProgramme}/rechercher`);
  }
  rechercherTravauxRetardDto$(codeProgramme:string) : Observable<DossierSubventionTableMferDto[]> {
    return this.httpService.envoyerRequeteGet(`/subvention/dossier/retard/${codeProgramme}/rechercher`);
  }
}

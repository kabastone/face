import { NgModule } from '@angular/core';

// interne
import { GestionEtatGeneralComponent } from './gestion-etat-general/gestion-etat-general.component';
import { GestionEtatGeneralParentComponent } from './gestion-etat-general-parent/gestion-etat-general-parent.component';

import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';

// PrimeNG
import { TooltipModule } from 'primeng/tooltip';



@NgModule({
  declarations: [
    GestionEtatGeneralComponent,
    GestionEtatGeneralParentComponent
  ],
  imports: [
    TooltipModule,
    SharedModule,
    RouterModule
  ]
})
export class GestionEtatGeneralModule { }

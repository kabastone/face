import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEtatGeneralComponent } from './gestion-etat-general.component';

describe('GestionEtatGeneralComponent', () => {
  let component: GestionEtatGeneralComponent;
  let fixture: ComponentFixture<GestionEtatGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionEtatGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEtatGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

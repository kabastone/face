import { Input } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { GestionEtatGeneralService } from '@app/domaines/gestion/services/gestion-etat-general.service';
import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { AnneeDto } from '@dotation/dto/annee.dto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-gestion-etat-general-parent',
  templateUrl: './gestion-etat-general-parent.component.html',
  styleUrls: ['./gestion-etat-general-parent.component.css']
})
export class GestionEtatGeneralParentComponent implements OnInit, OnDestroy {

  private infoUtilisateur: UtilisateurConnecteDto;
  public isUserUniquementMfer: boolean;

  public collectivites: CollectiviteLovDto[];
  public departements: DepartementLovDto[];

  public gestionEtatGeneralForm: FormGroup;

  public anneeDebut = 2016;
  public anneeEnCours: number = (new Date()).getFullYear();
  public annees: AnneeDto[] = [];

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private referentielService: ReferentielService,
    private gestionEtatGeneralService: GestionEtatGeneralService,
    private collectiviteService: CollectiviteService
  ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit(): void {

    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur => {
        this.infoUtilisateur = infoUtilisateur;
        this.initialiserIsUserUniquementMfer(infoUtilisateur);
      }
    );

    this.initGestionEtatGeneralForm();

    // Initialisation des années >= 2016
    for (let i = this.anneeDebut; i <= this.anneeEnCours; i++) {
      this.annees.push({ annee: i });
    }
    this.rechercherDepartementsEtCollectivites();
  }

  public rechercherDepartementsEtCollectivitesEtClear() {
    this.gestionEtatGeneralForm.get('selectionCollectivite').setValue(null);
    this.rechercherDepartementsEtCollectivites();
  }

  public rechercherDepartementsEtCollectivites() {
    // récupération des départements
    this.referentielService
    .rechercherTousDepartements$()
    .subscribe(data => (this.departements = data));

    if (
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== null &&
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== '...'
    ) {
      this.collectiviteService.listerCollectivitesParDepartement(this.gestionEtatGeneralForm.get('selectionDepartement').value.code)
      .subscribe(data => (this.collectivites = data));
    } else {
    // récupération des collectivites
    this.referentielService
    .rechercherToutesCollectivites$()
    .subscribe(data => (this.collectivites = data));
    }
  }

  private initialiserIsUserUniquementMfer(
    infosUtilisateur: UtilisateurConnecteDto
    ): void {
      this.isUserUniquementMfer = this.droitService.isMfer(
        infosUtilisateur
      );
  }

  private initGestionEtatGeneralForm(): void {
    // init. du formulaire
    this.gestionEtatGeneralForm = this.formBuilder.group({
      selectionAnnee: new FormControl(),
      selectionDepartement: new FormControl(),
      selectionCollectivite: new FormControl()
    });
  }

}

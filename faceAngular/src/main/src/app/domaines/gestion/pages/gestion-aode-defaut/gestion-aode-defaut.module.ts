import { NgModule } from '@angular/core';

import { SharedModule } from '@app/share/shared.module';
import { RouterModule } from '@angular/router';


// PrimeNG
import { TooltipModule } from 'primeng/tooltip';
import { GestionAodeDefautParentComponent } from './gestion-aode-defaut-parent/gestion-aode-defaut-parent.component';
import { GestionAodeDefautProgrammeComponent } from './gestion-aode-defaut-programme/gestion-aode-defaut-programme.component';




@NgModule({
  declarations: [
    GestionAodeDefautParentComponent,
    GestionAodeDefautProgrammeComponent
  ],
  imports: [
    TooltipModule,
    SharedModule,
    RouterModule
  ]
})
export class GestionAodeDefautModule { }

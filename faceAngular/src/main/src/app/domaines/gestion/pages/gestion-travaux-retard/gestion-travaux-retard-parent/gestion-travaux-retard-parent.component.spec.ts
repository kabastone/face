import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionTravauxRetardParentComponent } from './gestion-travaux-retard-parent.component';

describe('GestionTravauxRetardParentComponent', () => {
  let component: GestionTravauxRetardParentComponent;
  let fixture: ComponentFixture<GestionTravauxRetardParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionTravauxRetardParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionTravauxRetardParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

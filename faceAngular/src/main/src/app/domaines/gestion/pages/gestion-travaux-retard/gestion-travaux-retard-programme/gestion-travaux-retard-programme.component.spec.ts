import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionTravauxRetardProgrammeComponent } from './gestion-travaux-retard-programme.component';

describe('GestionTravauxRetardProgrammeComponent', () => {
  let component: GestionTravauxRetardProgrammeComponent;
  let fixture: ComponentFixture<GestionTravauxRetardProgrammeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionTravauxRetardProgrammeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionTravauxRetardProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

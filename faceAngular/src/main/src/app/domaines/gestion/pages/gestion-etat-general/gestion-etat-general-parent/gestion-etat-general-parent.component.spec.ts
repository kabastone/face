import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEtatGeneralParentComponent } from './gestion-etat-general-parent.component';

describe('GestionEtatGeneralParentComponent', () => {
  let component: GestionEtatGeneralParentComponent;
  let fixture: ComponentFixture<GestionEtatGeneralParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionEtatGeneralParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEtatGeneralParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

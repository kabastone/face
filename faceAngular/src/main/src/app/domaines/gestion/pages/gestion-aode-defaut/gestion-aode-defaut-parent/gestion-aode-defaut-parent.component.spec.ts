import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionAodeDefautParentComponent } from './gestion-aode-defaut-parent.component';

describe('GestionAodeDefautParentComponent', () => {
  let component: GestionAodeDefautParentComponent;
  let fixture: ComponentFixture<GestionAodeDefautParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionAodeDefautParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionAodeDefautParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionAodeDefautProgrammeComponent } from './gestion-aode-defaut-programme.component';

describe('GestionAodeDefautProgrammeComponent', () => {
  let component: GestionAodeDefautProgrammeComponent;
  let fixture: ComponentFixture<GestionAodeDefautProgrammeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionAodeDefautProgrammeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionAodeDefautProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

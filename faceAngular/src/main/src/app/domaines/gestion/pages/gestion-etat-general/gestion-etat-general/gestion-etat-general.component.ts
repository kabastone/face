import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { AnneeDto } from '@dotation/dto/annee.dto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CritereRechercheCadencementDto } from '../../../dto/critere-recherche-cadencement.dto';
import { ResultatRechercheCadencementDto } from '../../../dto/resultat-recherche-cadencement.dto';
import { GestionEtatGeneralService } from '../../../services/gestion-etat-general.service';

@Component({
  selector: 'app-gestion-etat-general',
  templateUrl: './gestion-etat-general.component.html',
  styleUrls: ['./gestion-etat-general.component.css']
})
export class GestionEtatGeneralComponent implements OnInit, OnDestroy {

  @Input()
  public codeProgramme = '';

  @Input()
  public gestionEtatGeneralForm: FormGroup;

  public criteresDto: CritereRechercheCadencementDto;

  private infoUtilisateur: UtilisateurConnecteDto;
  public collectivites: CollectiviteLovDto[];
  public departements: DepartementLovDto[];

  public cadencementProgramme: ResultatRechercheCadencementDto[];

  public isUserUniquementMfer: boolean;

  public hasProlongation = false;

  public annees: AnneeDto[] = [];

  public cols: any[];

  public anneeDebut = 2016;
  public anneeEnCours: number = (new Date()).getFullYear();

  public totalAnnees = [];

  public exportColumns: any[];

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private droitService: DroitService,
    private referentielService: ReferentielService,
    private gestionEtatGeneralService: GestionEtatGeneralService,
    private datePipe: DatePipe
  ) { }

  // à la destruction
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  ngOnInit(): void {

    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(infoUtilisateur => {
        this.infoUtilisateur = infoUtilisateur;
        this.initialiserIsUserUniquementMfer(infoUtilisateur);
      }
    );
    this.gestionEtatGeneralForm.valueChanges.subscribe(_ =>
      this.rechercherDossiers()
    );



    // Initialisation des années >= 2016
    for (let i = this.anneeDebut; i <= this.anneeEnCours; i++) {
      this.annees.push({ annee: i });
    }

    this.majChampsTableau();

    // récupération des collectivites
    this.referentielService
      .rechercherToutesCollectivites$()
      .subscribe(data => (this.collectivites = data));

    // récupération des départements
    this.referentielService
      .rechercherTousDepartements$()
      .subscribe(data => (this.departements = data));

    this.rechercherDossiers();


  }

  public getDynamicDownloadName() {
    const d = new Date();
    const dateActuelle = new Date(d.getFullYear(), d.getMonth(), d.getDate());
    const dateActuelleString = this.datePipe.transform(dateActuelle, 'yyyy-MM-dd');
    let anneeCritere = 'Cumul-des-annees_';
    let departementCritere = '';
    let collectiviteCritere = '';
    if (
      this.gestionEtatGeneralForm.get('selectionAnnee').value !== null &&
      this.gestionEtatGeneralForm.get('selectionAnnee').value !== '...'
    ) {
      anneeCritere = this.gestionEtatGeneralForm.get('selectionAnnee').value.annee + '_';
    }
    if (
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== null &&
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== '...'
    ) {
      departementCritere = this.gestionEtatGeneralForm.get('selectionDepartement').value.nom + '_';
    }
    if (
      this.gestionEtatGeneralForm.get('selectionCollectivite').value !== null &&
      this.gestionEtatGeneralForm.get('selectionCollectivite').value !== '...'
    ) {
      collectiviteCritere = this.gestionEtatGeneralForm.get('selectionCollectivite').value.nomCourt + '_';
    }

    return 'Etat-general_' + 'Programme-' + this.codeProgramme + '_'
      + anneeCritere + departementCritere + collectiviteCritere + dateActuelleString;
  }

  private initialiserIsUserUniquementMfer(
    infosUtilisateur: UtilisateurConnecteDto
    ): void {
      this.isUserUniquementMfer = this.droitService.isMfer(
        infosUtilisateur
      );
  }


  public majChampsTableau() {



    this.cols = [
      { field: 'sousProgrammeCode', header: 'Code', euros: false, style: 'text-align: center;'},
      { field: 'sousProgrammeAbreviation', header: 'Abréviation', euros: false, style: 'text-align: center;'},
      { field: 'sousProgrammeDescription', header: 'Libellé', euros: false, style: 'text-align: center;'},
      { field: 'montantSubventions', header: 'Montant Subventions', euros: true, style: 'text-align: center;'},
      { field: 'cadencementAnnee1', header: this.gestionEtatGeneralForm.get('selectionAnnee').value ? this.gestionEtatGeneralForm.get('selectionAnnee').value.annee : this.anneeEnCours, euros: true, style: 'text-align: center;'},
      { field: 'cadencementAnnee2', header: this.gestionEtatGeneralForm.get('selectionAnnee').value ? this.gestionEtatGeneralForm.get('selectionAnnee').value.annee + 1 : this.anneeEnCours + 1, euros: true, style: 'text-align: center;'},
      { field: 'cadencementAnnee3', header: this.gestionEtatGeneralForm.get('selectionAnnee').value ? this.gestionEtatGeneralForm.get('selectionAnnee').value.annee + 2 : this.anneeEnCours + 2, euros: true, style: 'text-align: center;'},
      { field: 'cadencementAnnee4', header: this.gestionEtatGeneralForm.get('selectionAnnee').value ? this.gestionEtatGeneralForm.get('selectionAnnee').value.annee + 3 : this.anneeEnCours + 3, euros: true, style: 'text-align: center;'}
    ];
    if (this.hasProlongation) {
      this.cols.push({ field: 'cadencementAnneeProlongation', header: this.gestionEtatGeneralForm.get(
        'selectionAnnee').value ? this.gestionEtatGeneralForm.get('selectionAnnee').value.annee + 4 :
        this.anneeEnCours + 4, euros: true, style: 'text-align: center;'});
    }
    this.cols.push({field: 'cadencementEstValide', header: 'Etat', euros: false, style: 'text-align: center;'});

    this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));
  }

  public rechercherDossiers(): void {

    this.majDtoFromFormulaire();

    this.majChampsTableau();

    this.gestionEtatGeneralService
      .getResultatRechercheCadencementDto$(this.criteresDto, this.codeProgramme)
      .subscribe(data => {
        this.cadencementProgramme = data;
        this.hasProlongation = data.map(_ => _.cadencementAnneeProlongation).reduce((add, num) => add + num) > 0;
        this.majChampsTableau();
        this.getMontantAnnees();
      });
  }

  public majDtoFromFormulaire(): void {
    this.criteresDto = this.gestionEtatGeneralForm.value as CritereRechercheCadencementDto;

    // On alimente l'année à partir de la liste déroulante.
    if (
      this.gestionEtatGeneralForm.get('selectionAnnee').value !== null &&
      this.gestionEtatGeneralForm.get('selectionAnnee').value !== 'Cumul des années'
    ) {
      const selectionAnnee: AnneeDto = this.gestionEtatGeneralForm.get(
        'selectionAnnee'
        ).value as AnneeDto;
      this.criteresDto.anneeProgrammation = selectionAnnee.annee;
    }
    // On alimente l'id de la collectivite à partir de la liste déroulante.
    if (
      this.gestionEtatGeneralForm.get('selectionCollectivite').value !== null &&
      this.gestionEtatGeneralForm.get('selectionCollectivite').value !== '...'
    ) {
      const selectionCollectivite: CollectiviteLovDto = this.gestionEtatGeneralForm.get(
        'selectionCollectivite'
      ).value as CollectiviteLovDto;
      this.criteresDto.idCollectivite = selectionCollectivite.id;
    }

    // On alimente l'id du departement à partir de la liste déroulante.
    if (
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== null &&
      this.gestionEtatGeneralForm.get('selectionDepartement').value !== '...'
    ) {
      const selectionDepartement: DepartementLovDto = this.gestionEtatGeneralForm.get(
        'selectionDepartement'
      ).value as DepartementLovDto;
      this.criteresDto.idDepartement = selectionDepartement.id;
    }
  }

  getMontantAnnees() {
    this.totalAnnees = [];
    let totalSubventions = 0;
    this.cadencementProgramme.forEach ( cadencement => {
      totalSubventions += cadencement.montantSubventions;
    });
    let totalAnnee1 = 0;
    this.cadencementProgramme.forEach ( cadencement => {
      totalAnnee1 += cadencement.cadencementAnnee1;
    });
    let totalAnnee2 = 0;
    this.cadencementProgramme.forEach ( cadencement => {
      totalAnnee2 += cadencement.cadencementAnnee2;
    });
    let totalAnnee3 = 0;
    this.cadencementProgramme.forEach ( cadencement => {
      totalAnnee3 += cadencement.cadencementAnnee3;
    });
    let totalAnnee4 = 0;
    this.cadencementProgramme.forEach ( cadencement => {
      totalAnnee4 += cadencement.cadencementAnnee4;
    });
    this.totalAnnees.push(totalSubventions, totalAnnee1, totalAnnee2, totalAnnee3, totalAnnee4);
    if (this.hasProlongation) {
      let totalAnneeProlongation = 0;
      this.cadencementProgramme.forEach ( cadencement => {
        totalAnneeProlongation += cadencement.cadencementAnneeProlongation;
      });
      this.totalAnnees.push(totalAnneeProlongation);
    }
  }
}

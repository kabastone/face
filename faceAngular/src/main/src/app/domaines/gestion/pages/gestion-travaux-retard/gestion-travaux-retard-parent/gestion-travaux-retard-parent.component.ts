import { Component, OnInit } from '@angular/core';
import { DroitService } from '@app/services/authentification/droit.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-gestion-travaux-retard-parent',
  templateUrl: './gestion-travaux-retard-parent.component.html',
  styleUrls: ['./gestion-travaux-retard-parent.component.css']
})
export class GestionTravauxRetardParentComponent implements OnInit {

  isMfer: boolean;
  ngDestroy$ = new Subject();

  constructor(private droitService: DroitService) { }

  ngOnInit(): void {
    this.droitService.getInfosUtilisateur$()
    .pipe(takeUntil(this.ngDestroy$))
    .subscribe(utilisateurinfos => {
      this.isMfer = this.droitService.isMfer(utilisateurinfos);
    });
  }

}

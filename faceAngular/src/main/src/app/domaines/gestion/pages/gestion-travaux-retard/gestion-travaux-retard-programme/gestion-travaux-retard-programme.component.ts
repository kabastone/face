import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfigService } from '@app/config/app-config.service';
import { DossierSubventionTableMferDto } from '@app/domaines/gestion/dto/dossier-subvention-table-mfer-dto';
import { GestionAodeDefautService } from '@app/domaines/gestion/service/gestion-aode-defaut.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-gestion-travaux-retard-programme',
  templateUrl: './gestion-travaux-retard-programme.component.html',
  styleUrls: ['./gestion-travaux-retard-programme.component.css']
})
export class GestionTravauxRetardProgrammeComponent implements OnInit {

  @Input()
  codeProgramme: string;

  dossierDto:DossierSubventionTableMferDto[];

  public nbTotalResultats: number;

  public pageSize: number;

  public cols: any[];

  public row: number;

  public rowGroupMetadata: any;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();
  constructor(
    private droitService: DroitService,
    private gestionaodeDefautService: GestionAodeDefautService,
    private router: Router,
    private datePipe: DatePipe
    ) { }

  ngOnInit(): void {
    if(this.isUserMferAdmin()) {
      this.cols = [
        { field: 'departementCode', header: 'Departement', style: 'width: 5%; text-align: center;' },
        { field: 'collectiviteNomCourt', header: 'AODE', style: 'width: 5%; text-align: center;' },
        { field: 'numDossier', header: 'Numero de dossier', style: 'width: 20%; text-align: center;'  },
        { field: 'dossierNumEJ', header: 'EJ', style: 'width: 13%; text-align: center;'}
      ];
        this.gestionaodeDefautService.rechercherTravauxRetardDto$(this.codeProgramme)
        .pipe(takeUntil(this.ngDestroyed$))
        .subscribe( data => {

          this.pageSize = +`${AppConfigService.settings.paginationPageSize}`;

          this.nbTotalResultats = data.length;

          this.dossierDto = data;

          this.updateRowGroupMetaData();

        });
     }
  }

  public getDynamicDownloadName() {
    const d = new Date();
    const dateActuelle = new Date(d.getFullYear(), d.getMonth(), d.getDate());
    const dateActuelleString = this.datePipe.transform(dateActuelle, 'yyyy-MM-dd');

    return 'Engagement-des-travaux_' + 'Programme-' + this.codeProgramme + '_' + dateActuelleString;
  }

  /**
   * Initialise le flag de profil de l'utilisateur.
   */
  private isUserMferAdmin()  {
    const utilisateurConnecte = this.droitService.getInfosUtilisateur$().value;
    return this.droitService.isMfer(utilisateurConnecte);
  }

  onSort() {
    this.updateRowGroupMetaData();
}

updateRowGroupMetaData() {
    this.rowGroupMetadata = {};

    if (this.dossierDto) {
        for (let i = 0; i < this.dossierDto.length; i++) {
            let rowData = this.dossierDto[i];
            let departementCode = rowData.departementCode;

            if (i == 0) {
                this.rowGroupMetadata[departementCode] = { index: 0, size: 1 };
            }
            else {
                let previousRowData = this.dossierDto[i - 1];
                let previousRowGroup = previousRowData.departementCode;
                if (departementCode === previousRowGroup) {
                  this.rowGroupMetadata[departementCode].size++;

                }

                else
                    this.rowGroupMetadata[departementCode] = { index: i, size: 1 };
            }
        }
    }
}

navigateDetails(idDossier) {
  this.router.navigateByUrl('/subvention/dossier/' + idDossier);
}
}

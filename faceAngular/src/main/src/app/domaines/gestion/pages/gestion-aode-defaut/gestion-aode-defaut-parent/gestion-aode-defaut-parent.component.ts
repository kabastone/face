import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GestionAodeDefautService } from '@app/domaines/gestion/service/gestion-aode-defaut.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-gestion-aode-defaut-parent',
  templateUrl: './gestion-aode-defaut-parent.component.html',
  styleUrls: ['./gestion-aode-defaut-parent.component.css']
})
export class GestionAodeDefautParentComponent implements OnInit {

  isMfer: boolean;
  ngDestroy$ = new Subject();
  
  constructor(private droitService: DroitService) { }

  ngOnInit(): void {
    this.droitService.getInfosUtilisateur$()
    .pipe(takeUntil(this.ngDestroy$))
    .subscribe(utilisateurinfos => {
      this.isMfer = this.droitService.isMfer(utilisateurinfos);
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { ListeAnomaliesComponent } from './liste-anomalies/liste-anomalies.component';
import { AjoutAnomalieComponent } from './ajout-anomalie/ajout-anomalie.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import localeFr from '@angular/common/locales/fr';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DialogModule } from 'primeng/dialog';
import { LayoutModule } from '@layout/layout.module';
import { MessageModule } from 'primeng/message';

@NgModule({
  declarations: [
    ListeAnomaliesComponent,
    AjoutAnomalieComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextareaModule,
    MessageModule,
    DialogModule,
    LayoutModule
  ],
  exports: [
    ListeAnomaliesComponent
  ]
})
export class AnomalieModule { }

registerLocaleData(localeFr);

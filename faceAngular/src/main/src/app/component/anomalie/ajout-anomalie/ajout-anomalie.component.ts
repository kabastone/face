import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AnomalieDto } from '@app/dto/anomalie.dto';
import { TypeAnomalieLovDto } from '@app/dto/type-anomalie-lov.dto';
import { AnomalieService } from '@app/services/commun/anomalie.service';
import { ReferentielService } from '@app/services/commun/referentiel.service';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'app-ajout-anomalie',
  templateUrl: './ajout-anomalie.component.html',
  styleUrls: ['./ajout-anomalie.component.css']
})
export class AjoutAnomalieComponent implements OnInit {
  // formulaire ihm
  public formulaireIhm: FormGroup;

  // liste des types d'anomalies (menu déroulant)
  public listeTypesAnomaliePaiement: TypeAnomalieLovDto[];

  // liste des types d'anomalies (menu déroulant)
  public listeTypesAnomalieSubvention: TypeAnomalieLovDto[];

  public selectedAnoSubv: TypeAnomalieLovDto;
  public selectedAnoPaie: TypeAnomalieLovDto;
  public propertyArea: string;

  // anomalie
  public anomalieDto: AnomalieDto;

  // id du dossier de subvention (url)
  public idDemandeSubvention = +this.route.snapshot.paramMap.get('idDemandeSubvention');

  // id de la demande de paiement (url)
  public idDemandePaiement = +this.route.snapshot.paramMap.get('idDemandePaiement');

  @Output()
  public updateCallback: EventEmitter<any> = new EventEmitter();

  @Input() set displayed(displayed: boolean) {
    if (displayed) {
      this.selectedAnoSubv = undefined;
      this.selectedAnoPaie = undefined;
      this.propertyArea = '';
    }
 }

  // constructeur
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private referentielService: ReferentielService,
    private anomalieService: AnomalieService,
    private messageManagerService: MessageManagerService,
  ) { }

  // init.
  ngOnInit() {
    // init. du formulaire
    this.formulaireIhm = this.formBuilder.group({
      date: new FormControl({ value: this.getCurrentDate(), disabled: true }),
      type: new FormControl('', Validators.compose([Validators.required])),
      selectionTypeAnomalie: new FormControl(),
      problematique: new FormControl('', Validators.compose([Validators.required]))
    });

    // chargement du menu déroulant : listing des types d'anomalies
    this.referentielService
      .listerTypesAnomaliePourDemandePaiement$()
      .subscribe(liste => (this.listeTypesAnomaliePaiement = liste));

    this.referentielService
      .listerTypesAnomaliePourDemandeSubvention$()
      .subscribe(liste => (this.listeTypesAnomalieSubvention = liste));
  }

  load() {
    location.reload();
  }

  /**
   * Ajout de l'anomalie.
   *
   */
  public ajouterAnomalie(): void {
    // transformation en dto
    this.anomalieDto = this.formulaireIhm.value;
    if (this.idDemandePaiement) {
      this.anomalieDto.idDemandePaiement = this.idDemandePaiement;
    } else if (this.idDemandeSubvention) {
      this.anomalieDto.idDemandeSubvention = this.idDemandeSubvention;
    }

    this.anomalieDto.codeTypeAnomalie = this.formulaireIhm.get('selectionTypeAnomalie').value.code;

    // debug :

    // appel du service
    this.anomalieService.ajouterAnomalie(this.anomalieDto)
      .subscribe(_ => {
        // message de succès
        this.messageManagerService.ajouterAnomalieMessage();

        if (this.updateCallback) {
                    this.updateCallback.emit();
        }
      });
  }

  /**
   * Récupération de la date.
   *
   * @returns la date actuelle
   */
  private getCurrentDate() {
    const datePipe = new DatePipe('fr');
    return datePipe.transform(new Date(), 'dd/MM/yyyy');
  }
}

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AnomalieDto } from '@app/dto/anomalie.dto';
import { AnomalieService } from '@app/services/commun/anomalie.service';
import { MetadataForm } from '@app/dto/metadata-form';
import { Observable } from 'rxjs';
import { MessageService } from 'primeng/api';
// import { resetCompiledComponents } from '@angular/core/src/render3/jit/module';

@Component({
  selector: 'app-liste-anomalies',
  templateUrl: './liste-anomalies.component.html',
  styleUrls: ['./liste-anomalies.component.css']
})
export class ListeAnomaliesComponent implements OnInit {

  public anomalies: AnomalieDto[] = [];

  @Input()
  public idDemandeSubvention: string;

  @Input()
  public idDemandePaiement: string;

  @Input()
  public btnAjouterAnomalie: MetadataForm;

  @Input()
  public btnCorrectionAnomalie: MetadataForm;

  @Input()
  public reponseAnomalie: MetadataForm;

  clonedAnomalies: { [s: string]: AnomalieDto; } = {};


  @Output() public updateCallback: EventEmitter<any> = new EventEmitter();


  public affichageModalCreationAnomalie: Boolean = false;

  constructor(
    private anomalieService: AnomalieService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.updateListe();
  }

  afficherModal() {
    this.affichageModalCreationAnomalie = true;

  }

  /**
   * Correction de l'anomalie.
   *
   * @param anomalie - l'anomalie à modifier
   * @param corrigee - le flag de correction (true/false)
   */
  public majCorrectionAnomalie(anomalie: AnomalieDto, corrigee: boolean) {
    anomalie.corrigee = corrigee;
    if (this.idDemandeSubvention != null) {
      this.anomalieService.majCorrectionAnomalie(+anomalie.id, +this.idDemandeSubvention, 'subvention', corrigee).subscribe();
    } else if (this.idDemandePaiement != null) {
      this.anomalieService.majCorrectionAnomalie(+anomalie.id, +this.idDemandePaiement, 'paiement', corrigee).subscribe();
    }
  }

  public updateListe(): void {
    this.affichageModalCreationAnomalie = false;
    let anomaliesObs = null;
    if (this.idDemandeSubvention) {
      anomaliesObs = this.anomalieService.rechercherAnomaliesParId('subvention', +this.idDemandeSubvention);
    } else if (this.idDemandePaiement) {
      anomaliesObs = this.anomalieService.rechercherAnomaliesParId('paiement', +this.idDemandePaiement);
    }

    if (anomaliesObs) {
      anomaliesObs.subscribe(data => {
        this.anomalies = data;
      });
    }
  }

  /**
   * Génération de l'url d'ajout : pour une demande de paiement ou de subvention.
   *
   * @returns l'url générée
   */
  public genererUrlPourAjout() {
    if (this.idDemandePaiement) {
      return `/anomalie/paiement/${this.idDemandePaiement}/init-ajouter`;
    } if (this.idDemandeSubvention) {
      return `/anomalie/subvention/${this.idDemandeSubvention}/init-ajouter`;
    }
    return '#';
  }

  /**
   * On Row Edit init.
   *
   * @param anomalie
   */
  onRowEditInit(anomalie: AnomalieDto) {
    this.clonedAnomalies[anomalie.id] = { ...anomalie };
  }

  /**
   * On Row Edit save.
   *
   * @param anomalie
   */
  onRowEditSave(anomalie: AnomalieDto) {
    this.anomalieService.updateAnomalie(anomalie).subscribe( _ => {
      this.updateListe();
      this.updateCallback.emit();
    });
  }

  /**
   * On Row Edit cancel
   *
   * @param anomalie
   * @param index
   */
  onRowEditCancel(anomalie: AnomalieDto, index: number) {
    this.anomalies[index] = this.clonedAnomalies[anomalie.id];
    delete this.clonedAnomalies[anomalie.id];
    this.messageService.clear();
  }

  public definirStyleSelonAnomalie(anomalie: AnomalieDto): Object {
    if (anomalie.corrigee) {
      return 'grey';
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '@app/config/app-config.service';
import { DroitService } from '@app/services/authentification/droit.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private droitService: DroitService
  ) { }

  ngOnInit() {
    // déconnection
    this.droitService.logout();

    // on redirige sur la déconnection de l'application
    window.location.href =  `${AppConfigService.settings.urlApplication}/deconnecter`;
  }
}

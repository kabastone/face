import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { Subject } from 'rxjs';
import { DroitService } from '@app/services/authentification/droit.service';
import { takeUntil } from 'rxjs/operators';
import { DroitAgentCollectiviteDto } from '@app/services/authentification/dto/droit-agent-collectivite-dto';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  public listeCollectivites: DroitAgentCollectiviteDto[] = [];

  public isUserGeneriqueEtCollectivite: boolean;
  public isUserMfer: boolean;
  public isUserSd7: boolean;

  public ngDestroyed$ = new Subject();

  // constructeur
  constructor(private router: Router,
    private droitService: DroitService,

  ) { }

  // init. du composant
  ngOnInit() {
    this.droitService
      .getInfosUtilisateur$()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(
        infoUtilisateur => {
          if (!infoUtilisateur.id) {
            return;
          }
          this.listeCollectivites = infoUtilisateur.droitsAgentCollectivites || [];
          this.initialiserFlagsProfilUtilisateur(infoUtilisateur);
          if (this.isUserMfer || this.isUserSd7 || this.isUserGeneriqueEtCollectivite) {
            this.router.navigateByUrl('/tableaudebord/afficher');
          } else { this.router.navigateByUrl('/accueil'); }
        });
  }

  /**
     * Initialise le flag de profil de l'utilisateur.
     */
  private initialiserFlagsProfilUtilisateur(
    infosUtilisateur: UtilisateurConnecteDto
  ): void {
    if (this.droitService.isMfer(infosUtilisateur)) {
      this.isUserMfer = true;
    } else if (this.droitService.isSd7(infosUtilisateur)) {
      this.isUserSd7 = true;
    } else if (this.droitService.isGeneriqueEtUneCollectivite(infosUtilisateur, this.listeCollectivites) ||
      this.droitService.isGeneriqueEtMultiCollectivites(infosUtilisateur, this.listeCollectivites)) {
      this.isUserGeneriqueEtCollectivite = true;
    }
  }

}

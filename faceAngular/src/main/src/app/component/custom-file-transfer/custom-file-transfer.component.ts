import { Component, Input, OnInit, Output, EventEmitter, ViewChildren, ElementRef, QueryList } from '@angular/core';

import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-custom-file-transfer',
  templateUrl: './custom-file-transfer.component.html',
  styleUrls: ['./custom-file-transfer.component.css']
})
export class CustomFileTransferComponent implements OnInit {
  /**
   * Label du champ du file upload (mode 'default').
   *
   */
  
  @Input() public label = 'Téléverser un document';

  private labelOriginal: string;

  /**
   * Title du champ du file upload (mode 'button').
   *
   */
  @Input() public title = 'Téléverser un document';

  @Input() public titleDl = 'Télécharger le document';

  /**
   * Libelle du champ du file download (mode 'default').
   *
   */
  @Input() public libelle = '';

  /**
   * Icône du champ du file upload (mode 'button').
   *
   */
  @Input() public icon = 'fas fa-plus';

  /**
   * Mode d'apparence par 'default' valeur possible: 'button', 'button-text'.
   *
   */
  @Input() public mode = 'default';

  /**
   * Type de fichiers acceptés.
   *
   */
  @Input() public accept = '';

  /**
   * Taille maximale du fichier à envoyer.
   *
   */
  @Input() public maxSize = 2097152;

  /**
   * Affichage du boutton de download (mode 'default') ou non.
   *
   */
  @Input() public affichageDownload = false;

 /**
   * Affichage du boutton d'upload (mode 'default') ou non.
   *
   */
  @Input() public affichageUpload = false;

  /**
   * Boutton d'upload : affichage en mode désactivé (grisé) ou non.
   *
   */
  @Input() public disabledUpload = false;

  /**
   * Boutton de download : affichage en mode désactivé (grisé) ou non.
   *
   */
  @Input() public disabledDownload = false;

  /**
   * Fonction à appeller lors de la sélection d'un fichier.
   *
   */
  @Output() public uploadCallback: EventEmitter<any> = new EventEmitter();

  /**
   * Fonction à appeller lors du téléchargement d'un fichier.
   *
   */
  @Output() public downloadCallback: EventEmitter<any> = new EventEmitter();

  @ViewChildren('customFile') inputList !: QueryList<ElementRef>;

  /**
   * Message d'erreur.
   *
   */
  public errorLibelle = '';
  
  displayModal: boolean;
  displayShowButton: boolean;
  event : any;
  excelExtention: boolean=false;
  fileEventUploaded: any;

  /**
   * Creates an instance of CustomFileTransferComponent.
   */
  constructor() {
  }

  /**
   * A l'init. du component.
   *
   */
  ngOnInit() {
    this.labelOriginal = this.label;
  }

  /**
   * Lors de l'ajout d'un fichier,
   * appel de la méthode via l'EventEmitter.
   *
   * @param event - évènement d'upload
   */
  onUploadCallback(event: any) {
    // init.
    this.event = event;
    const fichier: File = event.target.files[0];
    this.errorLibelle = '';
    // vérifie la taille du fichier
    if (!this.isTailleFichierValide(fichier)) {
      const tailleMax = this.formatSize(this.maxSize);
      this.errorLibelle = 'Fichier trop gros (' + tailleMax + ' maxi)';
      return;
    }

    // vérifie le type du fichier
    if (!this.isExtensionValide(fichier)) {
      this.errorLibelle = 'Extension de fichier non valide';
      return;
    }

    // tout est ok : mise à jour du label avec le nom de fichier
    this.label = fichier.name;
    this.displayShowButton = true;
    this.excelExtention = this.isExcelExtension(fichier);
    
    this.fileEventUploaded = event;

    // on transmet cet évènement vers la fonction utilisateur
    if (this.uploadCallback) {
      this.uploadCallback.emit(event);
    }
  }

  initTitleDl(nomFichierSansExtension: string): string {
    return this.titleDl = nomFichierSansExtension;
  }

  /**
   * Remets à zéro les documents et fichiers des inputs.
   */
  reset() {
    if(this.inputList){
      this.inputList.forEach(input => input.nativeElement.value = "");
    }
    this.label = this.labelOriginal;
  }

  /**
   * Vérifie si la taille du fichier correspond.
   *
   * @param fichier - fichier à vérifier
   * @returns true ou false
   */
  private isTailleFichierValide(fichier: File): boolean {
    if (fichier.size > this.maxSize) {
      return false;
    }
    return true;
  }

  /**
   * Vérifie si l'extension du fichier correspond.
   *
   * @param fichier - fichier à vérifier
   * @returns true ou false
   */
  private isExtensionValide(fichier: File): boolean {
    // pas de configuration, pas de vérification
    if (!this.accept) {
      return true;
    }

    // seulement une extension de fichier
    if (!this.accept.includes(',') && fichier.name.endsWith(this.accept)) {
      return true;
    }

    // l'une des extensions correspond
    let extensionValide = false;
    if (this.accept.includes(',')) {
      const extensionsFichiers = this.accept.split(',');
      Object.values(extensionsFichiers).forEach(ext => {
        if (fichier.name.endsWith(ext)) {
          extensionValide = true;
          return;
        }
      });
    }
    // l'extension à été trouvée ?
    if (extensionValide) {
      return true;
    }

    return false;
  }

  private isExcelExtension(fichier): boolean {
    let extensionValide = false;
    const extensionsFichiers = ['.ods','.xls','.xlsx']
      Object.values(extensionsFichiers).forEach(ext => {
        if (fichier.name.endsWith(ext)) {
          extensionValide = true;
          return;
        }
      });
    
    // l'extension à été trouvée ?
    if (extensionValide) {
      return true;
    }

    return false;
  }

  /**
   * Lors du téléchargement d'un fichier,
   * appel de la méthode via l'EventEmitter.
   *
   * @param event - évènement de download
   */
  onDownloadCallback(event: any) {
    // on transmet cet évènement vers la fonction utilisateur
    if (this.downloadCallback) {
      this.downloadCallback.emit(event);
    }
  }

  /**
   * Formattage d'un nombre.
   *
   * @param bytes - nombre à formatter
   * @param unitIdx - index pour l'unité
   * @returns nombre formatté
   */
  private formatSize(bytes: number, unitIdx?: number) {
    if (!bytes || !isFinite(bytes)) {
      return '0';
    }
    if (typeof unitIdx === 'undefined') {
      unitIdx = 0;
    }
    if (bytes > 1024) {
      return this.formatSize(bytes / 1024, unitIdx + 1);
    }
    const units = ['octets', 'ko', 'Mo', 'Go', 'To', 'Po'];
    return bytes.toFixed(2) + ' ' + units[unitIdx];
  }

}

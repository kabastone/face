import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-file-previewer',
  templateUrl: './file-previewer.component.html',
  styleUrls: ['./file-previewer.component.css']
})
export class FilePreviewerComponent implements OnInit {
  @Input()
  excelExtention: boolean;
  @Input()
  event: any;
  @Input()
  showButton: boolean;
  data: AOA;
  headData: any[];
  localUrl: any;
  isPdfContent: boolean;
  isExcelContent: boolean;
  displayModal: boolean;
  constructor() { }

  ngOnInit(): void {
  }

  showModal(){
    if(this.excelExtention){
      this.previewExcelFile(this.event);
      this.isPdfContent = false;
      this.isExcelContent = true;
    }
    else {
      var reader = new FileReader();
      reader.onload = (event: any) => {
          this.localUrl = event.target.result;
          this.isPdfContent = true;
          this.isExcelContent = false;
      }
      reader.readAsDataURL(this.event.target.files[0]);
    }
   this.displayModal = true;  
  }

  private previewExcelFile(evt) {

    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* lire workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* récuperer 1ere page */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* enregistrer donnees */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false, range: 10}));

      this.headData = this.data[0];
      this.data = this.data.slice(1); // supprimer l'entete

      const ws2: XLSX.WorkSheet = wb.Sheets[wb.SheetNames[1]];
      this.readDataSheet(ws2, 10);
    };
    reader.readAsBinaryString(target.files[0]);
  }

  private readDataSheet(ws: XLSX.WorkSheet, startRow: number) {
    /* save data */
    let datas = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false, range: startRow}));
    let headDatas = datas[0];
    datas = datas.slice(1); // supprimer l'entete

    for (let i = 0; i < this.data.length; i++) {
      this.data[i][this.headData.length] = datas.filter(x => x[12] == this.data[i][0])
    }
  }

}

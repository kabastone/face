import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomFileTransferComponent } from './custom-file-transfer.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { CustomPipesModule } from '@app/custom-pipes/custom-pipes.module';
import { FilePreviewerComponent } from './file-previewer/file-previewer.component';

@NgModule({
  declarations: [
    CustomFileTransferComponent,
    FilePreviewerComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    CustomPipesModule

  ],
  exports: [
    CustomFileTransferComponent,
    FilePreviewerComponent
  ]
})
export class CustomFileTransferModule { }

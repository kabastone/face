import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomFileTransferComponent } from './custom-file-transfer.component';


describe('CustomFileTransferComponent', () => {
  let component: CustomFileTransferComponent;
  let fixture: ComponentFixture<CustomFileTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomFileTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFileTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

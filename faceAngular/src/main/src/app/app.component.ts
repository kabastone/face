import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

const APP_TITLE = 'FACE - ';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'face-angular';
  titre_page$: Observable<string> = of('Titre');

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  getLastChild(route : ActivatedRoute): ActivatedRoute {
    if(route.firstChild) {
      return this.getLastChild(route.firstChild);
    }
    return route;
  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => this.getLastChild(route)),
        switchMap(route => route.data ),
        map(data => {
          if (data.title) {
            // If a route has a title set (e.g. data: {title: "Foo"}) then we use it
            return data.title;
          } else {
            return 'No title';
          }
        })
      )
      .subscribe(
        // pathString => (this.titre_page$ = of(`${APP_TITLE} ${pathString}`))
        pathString => (this.titre_page$ = of(`${pathString}`))
      );
  }
}

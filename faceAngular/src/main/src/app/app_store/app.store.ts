import { ActionReducerMap } from '@ngrx/store';
import * as fromSubv from '../domaines/subvention/store/reducer';

export interface AppState {
    searchList : fromSubv.State;

}

export const appReducer: ActionReducerMap<AppState> = {
    searchList: fromSubv.dossierSubvReducer
};
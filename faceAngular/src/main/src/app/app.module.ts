import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccueilComponent } from '@component/accueil/accueil.component';
import { LogoutComponent } from '@component/logout/logout.component';
import { LayoutModule } from '@layout/layout.module';
import { CookieService } from 'ngx-cookie-service';
import { MessageService } from 'primeng/api';
import { AppComponent } from './app.component';
import { AppConfigService } from './config/app-config.service';
import { TableauDeBordModule } from './domaines/tableau-de-bord/tableau-de-bord.module';
import { AppRoutingModule } from './routing/app-routing.module';
import { AuthInterceptor } from './services/authentification/auth.interceptor';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { CustomPipesModule } from './custom-pipes/custom-pipes.module';
import { CustomFileTransferModule } from '@component/custom-file-transfer/custom-file-transfer.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { SearchDossierSubvEffect } from './domaines/subvention/store/effects';
import * as fromApp from './app_store/app.store';
import { RouteStoreService } from '@subvention/services/route-store.service';
import { MontantEurosPipe } from './custom-pipes/montant-euros.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    LogoutComponent
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    // http
    HttpClientModule,
    // déclaration du store de l'application
    StoreModule.forRoot(fromApp.appReducer, {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([SearchDossierSubvEffect]),
    LayoutModule,
    // module tableau de bord
    TableauDeBordModule,
    CustomPipesModule,
    // module de file transfer (upload & download)
    CustomFileTransferModule,
  ],
  providers: [
    // service des messages primeNG
    MessageService,
    MontantEurosPipe,
    DatePipe,
    // config au runtime
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      multi: true,
      deps: [AppConfigService]
    },
    // gestion des cookies
    CookieService,
    // intercepteur http (authentification via token)
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private routeStoreService: RouteStoreService) {}
}

// fonction de chargement de la config' au runtime
export function initializeApp(appConfigService: AppConfigService) {
  return () => appConfigService.chargementConfApp();
}

registerLocaleData(localeFr, 'fr-FR');

import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({name: 'montantKeuros'})
export class MontantKeurosPipe implements PipeTransform {
  transform(value: number): string {
    const decPipe = new DecimalPipe('fr-FR');
    if (!value) {
      value = 0;
    }
    value = value / 1000;
    return decPipe.transform(value, '1.0');
  }
}

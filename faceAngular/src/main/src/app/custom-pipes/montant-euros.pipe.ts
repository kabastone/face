import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

/*
 * Formate la valeur numérique donnée en String représentant un montant en euros.
*/
@Pipe({name: 'montantEuros'})
export class MontantEurosPipe implements PipeTransform {
  transform(value: number): string {
    const decPipe = new DecimalPipe('fr-FR');
    if (!value) {
      value = 0;
    }
    const result = decPipe.transform(value, '1.2-2') + '€';
    return result.replace(/,/gi, '.');
  }
}

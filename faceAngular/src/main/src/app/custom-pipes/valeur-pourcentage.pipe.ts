import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

/*
 * Formate la valeur numérique donnée en String représentant une valeur de pourcentage.
*/
@Pipe({name: 'valeurPourcentage'})
export class ValeurPourcentagePipe implements PipeTransform {
  transform(value: number): string {
    const decPipe = new DecimalPipe('fr-FR');
    if (!value) {
      value = 0;
    }
    return decPipe.transform(value, '1.2-2') + ' %';
  }
}

import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'ajoutAccentEtatCollectivite'})
export class AjoutAccentEtatCollectivitePipe implements PipeTransform {
    transform(value: string): string {
        switch (value) {
            case 'CREEE': {
                return value = 'Créée';
                break;
            }
            case 'FERMEE':  {
                return value = 'Fermée';
                break;
            }
            default: {
                return value = 'Erreur';
                break;
            }
        }
    }
}

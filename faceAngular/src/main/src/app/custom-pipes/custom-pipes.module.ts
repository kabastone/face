import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MontantEurosPipe } from './montant-euros.pipe';
import { ValeurPourcentagePipe } from './valeur-pourcentage.pipe';
import { MontantKeurosPipe } from './montant-keuros.pipe';
import { MontantKeurosArrondiPipe } from './montant-keuros-arrondi.pipe';
import {AjoutAccentEtatCollectivitePipe} from './ajout-accent-etat-collectivite.pipe';
import { SafePipe } from './safe.pipe';

@NgModule({
  declarations: [
    MontantEurosPipe,
    ValeurPourcentagePipe,
    MontantKeurosPipe,
    MontantKeurosArrondiPipe,
    AjoutAccentEtatCollectivitePipe,
    SafePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MontantEurosPipe,
    ValeurPourcentagePipe,
    MontantKeurosPipe,
    MontantKeurosArrondiPipe,
    AjoutAccentEtatCollectivitePipe,
    SafePipe
  ]
})
export class CustomPipesModule { }

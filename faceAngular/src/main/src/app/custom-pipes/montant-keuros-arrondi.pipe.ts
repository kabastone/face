import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({name: 'montantKeurosArrondi'})
export class MontantKeurosArrondiPipe implements PipeTransform {
  transform(value: number): string {
    const decPipe = new DecimalPipe('fr-FR');
    value = value ? value / 1000 : 0;
    return decPipe.transform(value, '1.0-0');
  }
}

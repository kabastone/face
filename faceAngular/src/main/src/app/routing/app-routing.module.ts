import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TABLEAU_DE_BORD_ROUTES } from '@app/domaines/tableau-de-bord/tableau-de-bord.routes';
// Interne
import { AccueilComponent } from '@component/accueil/accueil.component';
import { LogoutComponent } from '@component/logout/logout.component';
// Import des Routes des modules
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { PIED_ROUTES } from '@layout/pied/pied.routes';

// routes : attention, l'ordre est important!
const routes: Routes = [
  // par défaut : redirection sur l'accueil
  {
    path: '',
    redirectTo: '/accueil',
    pathMatch: 'full',
    resolve: { componentMsg: ComponentResolverMsg }
  },
  // accueil
  {
    path: 'accueil',
    component: AccueilComponent,
    data: { title: 'Accueil' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  // logout
  {
    path: 'logout',
    component: LogoutComponent,
    data: { title: 'Déconnexion en cours...' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'dotation',
    loadChildren: () => import('@dotation/dotation.module').then(m => m.DotationModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'gestion',
    loadChildren: () => import('@app/domaines/gestion/gestion.module').then(m => m.GestionModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'administration',
    loadChildren: ()=> import('@administration/administration.module').then(m => m.AdministrationModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'subvention',
    loadChildren: () => import('@subvention/subvention.module').then(m => m.SubventionModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'prolongation',
    loadChildren: () => import('@app/domaines/prolongation/prolongation.module').then(m => m.ProlongationModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'paiement',
    loadChildren: () => import('@paiement/paiement.module').then(m => m.PaiementModule),
    resolve: { componentMsg: ComponentResolverMsg }
  },
  // Tableau de bord
  {
    path: 'tableaudebord',
    children: TABLEAU_DE_BORD_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
  // Footer de l'appli
  {
    path: 'pied',
    children: PIED_ROUTES,
    resolve: { componentMsg: ComponentResolverMsg }
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })
  ],
  // ComponentResolverMsg : effacement des messages prime ng (https://angular.io/guide/router#resolve-guard)
  providers: [ComponentResolverMsg]
})
export class AppRoutingModule { }

// export de toutes les routes + ajout du ComponentResolverMsg
// a decommenter + utiliser : quand bug angular 7 résolu (727, 1097, 23609)
// export function exportRoutesWithResolveGuard(): Route[] {
//   const resolverMsg = { componentMsg: ComponentResolverMsg };
//   for (const route of routes) {
//     if (route.resolve) {
//       // ajout du composant ComponentResolverMsg
//       route.resolve = { ...resolverMsg, ...route.resolve };
//     } else {
//       // uniquement le ComponentResolverMsg
//       route['resolve'] = resolverMsg;
//     }
//   }
//   return routes;
// }

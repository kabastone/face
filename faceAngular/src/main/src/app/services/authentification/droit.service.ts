import { UtilisateurDto } from '@administration/dto/utilisateur.dto';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';
import { RxStoreService } from '../store-appli/rx-store.service';
import { StoreAppli } from '../store-appli/store-appli';
import { DroitAgentCollectiviteDto } from './dto/droit-agent-collectivite-dto';
import { UtilisateurConnecteDto } from './dto/utilisateur-connecte-dto';

@Injectable({
  providedIn: 'root'
})
export class DroitService {

  // objet du store applicatif
  private storeApplicatif = this.rxStoreService.get<StoreAppli>('storeAppli', {} as StoreAppli);

  // constructeur
  constructor(
    private cookieService: CookieService,
    private rxStoreService: RxStoreService
  ) {
    this.storeApplicatif.value.infosUtilisateur$ = new BehaviorSubject<UtilisateurConnecteDto>({} as UtilisateurConnecteDto);
    this.storeApplicatif.value.idCollectivite$ = new BehaviorSubject<number>(undefined);
  }

  /**
   * S'il y a un cookie d'authentification
   * ET qu'il est différent de celui qui est dans le store :
   * on met à jour ce token avec la valeur du cookie d'authentification.
   *
   * Ceci est réalisé sur la récupération de la 'config.dev.json' (ou config.prod.json) au chargement de l'application
   * ou dans le cas d'un envoi d'une requête classique (auth.interceptor.ts),
   * on fait potentiellement une mise à jour du token d'authentification.
   */
  verifierEtMiseAjourTokenAuth(): void {
    // init
    const cookieTokenRecu = this.cookieService.get('temptoken');
    const tokenActuel = this.getTokenAuth();

    // si l'on a un nouveau cookie d'authentification : on le met dans le store applicatif
    if (cookieTokenRecu && cookieTokenRecu !== tokenActuel) {
      this.updateTokenAuth(cookieTokenRecu);
    }
  }

  /**
   * Retourne 'true' si un token d'authentification existe,
   * sinon 'false'
   *
   * @returns true ou false
   */
  isTokenExistant(): boolean {
    if (this.getTokenAuth()) {
      return true;
    }
    return false;
  }

  /**
   * Récupère la valeur du token d'authentification.
   *
   * @return le token stocké dans le store
   */
  getTokenAuth(): string {
    return this.storeApplicatif.value.tokenAuthentification;
  }

  /**
   * Met à jour le token d'authentification.
   *
   * @param nouveauToken - le nouveau token à mettre dans le store
   */
  updateTokenAuth(nouveauToken: string): void {
    // mise a jour du token dans la structure
    this.storeApplicatif.value.tokenAuthentification = nouveauToken;

    // mise à jour dans le store
    this.storeApplicatif.update(this.storeApplicatif.value);
  }

  /**
   * Déconnexion utilisateur.
   * Supprime les données utilisateur du store appli :
   *   o token d'authentification
   *   o données de l'utilisateur connecté
   */
  logout() {
    // suppression du token d'authentification
    this.updateTokenAuth(undefined);

    // suppression des données utilisateur dans le store appli
    this.updateUtilisateurConnecte({} as UtilisateurConnecteDto);
  }

  /**
   * Récupère la valeur des données de l'utilisateur connecté.
   *
   * @return les données de l'utilisateur connecté
   */
  getInfosUtilisateur$(): BehaviorSubject<UtilisateurConnecteDto> {
    return this.storeApplicatif.value.infosUtilisateur$;
  }

  /**
   * Met à jour les données de l'utilisateur connecté : à partir d'un 'UtilisateurConnecteDto'.
   *
   * @param infosUtilisateur - les nouvelles informations utilisateur à mettre dans le store
   */
  public updateUtilisateurConnecte(infosUtilisateur: UtilisateurConnecteDto): void {
    // mise à jour des infos de l'utilisateur connecté
    this.storeApplicatif.value.infosUtilisateur$.next(infosUtilisateur);

    // mise à jour de l'idCollectivite active s'il s'agit d'un utilisateur générique ne pouvant accéder qu'à 1 seule collectivité.
    if (this.isUniquementGenerique(infosUtilisateur) &&
      infosUtilisateur.droitsAgentCollectivites.length === 1) {
      this.storeApplicatif.value.idCollectivite$.next(infosUtilisateur.droitsAgentCollectivites[0].idCollectivite);
    }

    // mise à jour du store appli global
    this.storeApplicatif.update(this.storeApplicatif.value);
  }

  /**
   * Met à jour les données de l'utilisateur connecté : à partir d'un 'UtilisateurDto'.
   * Via un mapping des principaux champs de 'UtilisateurDto' vers 'UtilisateurConnecteDto'.
   *
   * @param infosUtilisateur - les nouvelles informations utilisateur à mettre dans le store
   */
  public updateUtilisateurConnecteViaUtilisateurDto(utilisateurDto: UtilisateurDto): void {
    const utilisateurConnecteDto: UtilisateurConnecteDto = this.storeApplicatif.value.infosUtilisateur$.value;
    // maj des champs
    utilisateurConnecteDto.nom = utilisateurDto.nom;
    utilisateurConnecteDto.prenom = utilisateurDto.prenom;
    utilisateurConnecteDto.telephone = utilisateurDto.telephone;
    // maj du store applicatif
    this.updateUtilisateurConnecte(utilisateurConnecteDto);
  }

  /**
   * Récupère la valeur de la collectivité sélectionnée.
   *
   * @return l'id de la collectivité sélectionnée
   */
  getIdCollectivite$(): BehaviorSubject<number> {
    return this.storeApplicatif.value.idCollectivite$;
  }

  /**
   * Met à jour la valeur de la collectivité sélectionnée.
   *
   * @param infosUtilisateur - les nouvelles informations utilisateur à mettre dans le store
   */
  public updateIdCollectivite(idCollectivite: number): void {
    // mise à jour l'id de la collectivité
    this.storeApplicatif.value.idCollectivite$.next(idCollectivite);
    // mise à jour du store appli global
    this.storeApplicatif.update(this.storeApplicatif.value);
  }

  /**
   * Indique si l'utilisateur a une collectivité.
   *
   * @returns true ou false
   */
  isGeneriqueEtUneCollectivite(infosUtilisateur: UtilisateurConnecteDto,
    listeDroitsAgent: DroitAgentCollectiviteDto[]): boolean {
    return infosUtilisateur.id && listeDroitsAgent.length === 1 && this.isAuMoinsGenerique(infosUtilisateur);
  }

  /**
   * Indique si l'utilisateur a plusieurs collectivités.
   *
   * @returns true ou false
   */
  isGeneriqueEtMultiCollectivites(infosUtilisateur: UtilisateurConnecteDto,
    listeDroitsAgent: DroitAgentCollectiviteDto[]):  boolean {
    return infosUtilisateur.id && listeDroitsAgent.length > 1 && this.isAuMoinsGenerique(infosUtilisateur);
  }

  /**
   * Indique si l'utilisateur possède au moins le rôle générique.
   *
   * @returns true ou false
   */
  isAuMoinsGenerique(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles && infosUtilisateur.listeRoles.includes('GENERIQUE');
  }

  /**
   * Indique si l'utilisateur possède uniquement le rôle générique.
   *
   * @returns true ou false
   */
  isUniquementGenerique(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && infosUtilisateur.listeRoles.length === 1
    && infosUtilisateur.listeRoles.includes('GENERIQUE');
  }

   /**
   * Indique si l'utilisateur possède un rôle MFER.
   *
   * @returns true ou false
   */
  isMfer(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && (infosUtilisateur.listeRoles.includes('MFER_INSTRUCTEUR') || infosUtilisateur.listeRoles.includes('MFER_RESPONSABLE'));
  }

  /**
   * Indique si l'utilisateur possède un rôle MFER ADMINISTRATEUR.
   *
   * @returns true ou false
   */
  isMferAdmin(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && (infosUtilisateur.listeRoles.includes('MFER_INSTRUCTEUR') && infosUtilisateur.listeRoles.includes('MFER_RESPONSABLE'));
  }

   /**
   * Indique si l'utilisateur possède un rôle MFER RESPONSABLE.
   *
   * @returns true ou false
   */
  isMferResponsable(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && infosUtilisateur.listeRoles.includes('MFER_RESPONSABLE');
  }

   /**
   * Indique si l'utilisateur possède un rôle MFER RESPONSABLE.
   *
   * @returns true ou false
   */
  isMferInstructeur(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && infosUtilisateur.listeRoles.includes('MFER_INSTRUCTEUR');
  }

   /**
   * Indique si l'utilisateur possède un rôle SD7.
   *
   * @returns true ou false
   */
  isSd7(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && (infosUtilisateur.listeRoles.includes('SD7_INSTRUCTEUR') || infosUtilisateur.listeRoles.includes('SD7_RESPONSABLE'));
  }

  /**
   * Indique si l'utilisateur possède un rôle SD7 INSTRUCTEUR.
   *
   * @returns true ou false
   */
  isSd7Instructeur(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && (infosUtilisateur.listeRoles.includes('SD7_INSTRUCTEUR'));
  }

  /**
   * Indique si l'utilisateur possède un rôle SD7 ADMINISTRATEUR.
   *
   * @returns true ou false
   */
  isSd7Admin(infosUtilisateur: UtilisateurConnecteDto): boolean {
    return infosUtilisateur.listeRoles
    && (infosUtilisateur.listeRoles.includes('SD7_INSTRUCTEUR') && infosUtilisateur.listeRoles.includes('SD7_RESPONSABLE'));
  }

  /**
   * Indique si l'utilisateur est administrateur sur la collectivite, ou non.
   * L'utilisateur doit :
   *   o soit avoir un rôle MFER
   *   o soit avoir un rôle GENERIQUE avec des droits d'administration sur la collectivité voulue.
   * @returns true ou false
   */
  isAdminCollectivite(infosUtilisateur: UtilisateurConnecteDto, idCollectivite: number): boolean {
    if (infosUtilisateur.listeRoles
      && (infosUtilisateur.listeRoles.includes('MFER_INSTRUCTEUR') || infosUtilisateur.listeRoles.includes('MFER_RESPONSABLE'))) {
      return true;
    }
    if (infosUtilisateur.listeRoles.includes('GENERIQUE')) {
      // si l'utilisateur est GENERIQUE
      for (const droitAgentCollectivite of infosUtilisateur.droitsAgentCollectivites) {
        if (droitAgentCollectivite.idCollectivite === idCollectivite && droitAgentCollectivite.admin === true) {
          return true;
        }
      }
    }
    return false;
  }

  isCollectiviteOuvert(): boolean {

    const droit: DroitAgentCollectiviteDto = this.storeApplicatif.value.infosUtilisateur$.getValue().droitsAgentCollectivites.find(
      (x: DroitAgentCollectiviteDto) => x.idCollectivite === this.storeApplicatif.value.idCollectivite$.getValue());

    return droit.etatCollectivite !== 'FERMEE';

  }
}

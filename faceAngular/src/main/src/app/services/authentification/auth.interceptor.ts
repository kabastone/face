import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DroitService } from './droit.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private droitService: DroitService) {
  }

  /**
   * Ajoute l'entête d'authentification à chaque requête HTTP.
   *
   * @param req - la requête HTTP
   * @returns la requête modifiée
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // mise à jour du token
    this.droitService.verifierEtMiseAjourTokenAuth();

    // récupération du token stocké
    const tokenAuth = this.droitService.getTokenAuth();

    if (tokenAuth) {
      // si présent, ajout du token dans la requête HTTP
      const cloned = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + tokenAuth)
      });
      return next.handle(cloned);
    } else {
      // sinon la requête n'est pas modifiée
      return next.handle(req);
    }
  }
}

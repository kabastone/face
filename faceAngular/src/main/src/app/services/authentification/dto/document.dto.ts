export interface DocumentDto {
  id: number;
  codeTypeDocument: string;
  nomFichierSansExtension: string;
  extensionFichier: string;
  dateTeleversement: string;
}

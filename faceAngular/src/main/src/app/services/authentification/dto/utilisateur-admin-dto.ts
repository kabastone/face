import { DroitAgentCollectiviteDto } from './droit-agent-collectivite-dto';

export interface UtilisateurAdminDto {
  id: number;
  nom: string;
  prenom: string;
  telephone: string;
  email: string;
  cerbereId: string;
  droitsAgentCollectivites: DroitAgentCollectiviteDto[];
}

import { UtilisateurAdminDto } from './utilisateur-admin-dto';

export interface UtilisateurConnecteDto extends UtilisateurAdminDto {
  listeRoles: string[];
}

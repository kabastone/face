export interface DroitAgentCollectiviteDto {
  idCollectivite: number;
  siret: string;
  nomCourt: string;
  nomLong: string;
  etatCollectivite: string;
  admin: boolean;
  idUtilisateur: number;
}

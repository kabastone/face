export interface CollectiviteSimpleDto {
  id: number;
  siret: string;
  nomCourt: string;
  nomLong: string;
  etatCollectivite: string;
}

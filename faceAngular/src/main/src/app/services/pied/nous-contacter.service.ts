import { Injectable } from '@angular/core';
import { EmailContactDto } from '@app/dto/email-contact.dto';
import { HttpService } from '../http/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NousContacterService {

  constructor(private httpService: HttpService) { }

  public envoyerEmailContact(Dto: EmailContactDto): Observable<boolean> {
    return this.httpService.envoyerRequetePost(`/nous-contacter/email/envoi`, Dto);
  }
}

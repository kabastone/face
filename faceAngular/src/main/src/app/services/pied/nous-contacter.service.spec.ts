import { TestBed } from '@angular/core/testing';

import { NousContacterService } from './nous-contacter.service';

describe('NousContacterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NousContacterService = TestBed.get(NousContacterService);
    expect(service).toBeTruthy();
  });
});

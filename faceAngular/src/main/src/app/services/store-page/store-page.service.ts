import { Injectable } from '@angular/core';
import { StorePage } from '@app/services/store-page/store-page';
import { RxStoreService } from '../store-appli/rx-store.service';

@Injectable({
  providedIn: 'root'
})
export class StorePageService {

  // gestion du store page
  private storePage = this.rxStoreService.get<StorePage>('storePage');

  // constructeur
  constructor(private rxStoreService: RxStoreService) { }

  /**
   * Obtention la donnée du store
   *
   * @returns la donnée du store
   */
  public getStorePage() {
    return this.storePage.value;
  }

  /**
   * Obtention la donnée du store en observable.
   *
   * @returns la donnée du store en observable
   */
  public getStorePage$() {
    return this.storePage.value$;
  }

  /**
   * Mise à jour du store page.
   *
   * @param objectStore - objet a mettre dans le store page
   */
  public majStorePage(objectStore: StorePage): void {
    this.storePage.update(objectStore);
    // si clone : this.storePage.update(JSON.parse(JSON.stringify(objectStore)));
  }

  /**
   * Effacement complet : on vide le store page.
   *
   */
  public effacerStorePage() {
    this.storePage.update(null);
  }
}

import { TestBed } from '@angular/core/testing';

import { StorePageService } from './store-page.service';

describe('StorePageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorePageService = TestBed.get(StorePageService);
    expect(service).toBeTruthy();
  });
});

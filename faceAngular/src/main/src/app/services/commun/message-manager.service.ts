import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class MessageManagerService {

  constructor(private messageService: MessageService) { }

  // Message générique
  message(typeSeverity: string, detailMessage: string, scroll?: Boolean, key: string = 'standard', clear: Boolean = true) {
    if(clear) {
      this.messageService.clear();
    }
    this.messageService.add({
      severity: typeSeverity,
      detail: detailMessage,
      key: key
    });
    if (scroll !== false) {
      // TODO Code à revoir
      setTimeout(function () { window.scrollTo({top:0, behavior:'smooth'}); }, 150);
    }
  }

  // messages récurents
  successMessage() {
    this.message('success', 'Enregistré avec succès');
  }

  modificationEnregistree() {
    this.message('success', 'Modifications enregistrées');
  }

  modificationDetecteMessage() {
    this.message('warn', 'Modifications Détectées');
  }

  documentIntrouvableMessage() {
    this.message('warn', 'Le document est introuvable');
  }

  MiseAJourMessage() {
    this.message('success', 'Mise à jour effectuée');
  }

  aucuneDonneeTrouveeMessage() {
    this.message('info', 'Aucune donnée n\'a été trouvée');
  }

  // messages uniques
  ajouterAnomalieMessage() {
    this.message('success', 'Anomalie ajoutée');
  }

  getInfosUtilisateurMessage() {
    this.message('warn', 'Veuillez sélectionner une collectivite');
  }

  getStorePageMessage() {
    this.message('warn', 'Modifications en attente d\'enregistrement');
  }

  enregistrerAdresseMessage() {
    this.message('success', 'Adresse enregistrée');
  }

  enregistrerCollectiviteMessage() {
    this.message('success', 'Collectivité créée');
  }

  traiterRetourNotificationMessage() {
    this.message('success', 'Les notifications ont été effectuées');
  }

  isAuMoinsUneAnomalieACorrigerMessage() {
    this.message('warn', 'Ce dossier contient une anomalie décrite sous votre demande de subvention. Veuillez apporter les modifications nécessaires au dossier, puis saisir votre réponse afin de valider le nouvel envoi.');
  }

  isDemandeComplementaireCorrigeeOuDemandeeMessageMferInstructeur() {
    this.message('warn', 'Ce dossier est une demande complémentaire, pour CHORUS, il sera traité de manière manuelle et son dossier doit donc être transmis au format papier.');
  }

  isDemandeComplementaireCorrigeeOuDemandeeMessageMferAdminOuSD7AdminOuSD7Instructeur() {
    this.message('warn', 'Ce dossier est une demande complémentaire, le format papier doit être joint pour saisie dans CHORUS.');
  }

    isAuMoinsUneAnomalieACorrigerPaiementMessage() {
    this.message('warn', 'Ce dossier contient une anomalie décrite sous votre demande de paiement. Veuillez apporter les modifications nécessaires au dossier, puis saisir votre réponse afin de valider le nouvel envoi.');
  }

  penserRepondreAnomaliesMessage() {
    this.message('success', 'Enregistrement effectué. Veuillez penser à répondre à toutes les anomalies.', false, 'anomalies');
  }

  realiserDemandePaiementMessage() {
    this.message('success', 'Demande envoyée');
  }

  enregistrerCreationDemandeSubventionMessage() {
    this.message('success', 'Votre dossier a bien été soumis à nos services. Nous allons le traiter dans les plus brefs délais.');
  }

  envoiEmailContactMessage() {
    this.message('success', 'Email envoyé !');
  }

  erreurEnvoiEmailContactMessage() {
    this.message('error', 'Erreur lors de l\'envoi.');
  }

  onlyNumberAccepted() {
    this.message('error', 'Le montant ne  doit contenir que des chiffres.', false);
  }

  supprimerLigneDotationSuccess() {
    this.message('success', 'La ligne a été supprimée.');
  }

  ajouterLigneDotationSuccess() {
    this.message('success', 'La ligne a été ajoutée.');
  }

  erreurMontantCadencement() {
    this.message('error', 'Le total du cadencement doit être égal au total de l\'aide demandée.');
  }

  mettreAJourCadencementAODE() {
    this.message('error', 'Vous avez des cadencements à mettre à jour, reportez-vous à l\'onglet subvention.', false, 'standard', false);
  }

  mettreAJourCadencementMFER() {
    this.message('warn',
    'L\'AODE qui a effectué la demande de subvention ci-dessous n\'a pas mis la totalité de ses cadencements à jour.', true, 'standard', false);
  }

  alerteDateButoireDemandeSubvention() {
    this.message('warn', 'Attention, il vous reste des subventions à demander, veillez au respect de la date limite.', false, 'standard', false);
  }
}

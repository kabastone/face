import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { DepartementLovDto } from '@app/dto/departement-lov.dto';
import { Observable } from 'rxjs';
import { SousProgrammeLovDto } from '@app/dto/sous-programme-lov.dto';
import { EtatDemandeLovDto } from '@app/dto/etat-demande-lov.dto';
import { CollectiviteLovDto } from '@app/dto/collectivite-lov.dto';
import { TypeDemandeLovDto } from '@app/dto/type-demande-lov.dto';
import { DepartementDto } from '@app/dto/departement.dto';

@Injectable({
  providedIn: 'root'
})
export class ReferentielService {

  constructor( private httpService: HttpService) { }

  /**
   * Rechercher tous les departements.
   *
   * @returns Les departements
   */
  rechercherTousDepartements$(): Observable<DepartementLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/departement/lister`);
  }

  rechercherDepartement$(id: number): Observable<DepartementDto> {
    return this.httpService.envoyerRequeteGet(`/referentiel/departement/${id}/rechercher`);
  }

  /**
   * Rechercher tous les sous-programmes.
   *
   * @returns Les sous-programmes
   */
  rechercherTousSousProgrammes$(): Observable<SousProgrammeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/sous-programme/lister`);
  }

  /**
   * Rechercher les sous-programmes pour un renoncement dotation.
   *
   * @returns Les sous-programmes
   */
  rechercherSousProgrammesDeTravauxParCollectivite$(idCollectivite: number): Observable<SousProgrammeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/collectivite/${idCollectivite}/sous-programme-travaux/rechercher`);
  }

  /**
   * Rechercher les sous-programmes de projet.
   *
   * @returns Les sous-programmes
   */
  rechercherSousProgrammesDeProjetParCollectivite$(idCollectivite: number): Observable<SousProgrammeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/collectivite/${idCollectivite}/sous-programme-projet/rechercher`);
  }

  /**
  * Rechercher les sous-programmes.
  *
  * @returns Les sous-programmes
  */
 rechercherSousProgrammesParCollectivite$(idCollectivite: number): Observable<SousProgrammeLovDto[]> {
   return this.httpService.envoyerRequeteGetGestionNotFound(
     `/referentiel/collectivite/${idCollectivite}/sous-programme/rechercher`,
     false,
     []
     );
 }

 /**
  * Rechercher les sous-programmes.
  *
  * @returns Les sous-programmes
  */
 rechercherSousProgrammesDeProjet$(): Observable<SousProgrammeLovDto[]> {
  return this.httpService.envoyerRequeteGetGestionNotFound(`/referentiel/sous-programme-projet/rechercher`,
  false,
  []
  );
}

  /**
   * Rechercher tous les états de demandes de subvention.
   *
   * @returns Les etats de demandes de subvention
   */
  rechercherTousEtatsDemandeSubvention$(): Observable<EtatDemandeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/etat-demande-subvention/lister`);
  }

    /**
   * Rechercher tous les états de demandes de paiement.
   *
   * @returns Les etats de demandes de subvention
   */
  rechercherTousEtatsDemandePaiement$(): Observable<EtatDemandeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/etat-demande-paiement/lister`);
  }

   /**
   * Rechercher toutes les collectivites.
   *
   * @returns Les collectivites
   */
  rechercherToutesCollectivites$(): Observable<CollectiviteLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/collectivite/lister`);
  }

  /**
   * Rechercher tous les types de demande de paiement.
   *
   * @returns les types de demande de paiement.
   */
  rechercherTousTypesDemandePaiement$(): Observable<TypeDemandeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/type-demande-paiement/lister`);
  }

 /**
   * Rechercher tous les types d'anomalies.
   *
   * @returns les types d'anomalies.
   */
  listerTypesAnomaliePourDemandePaiement$(): Observable<TypeDemandeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/type-anomalie/paiement/lister`);
  }

 /**
   * Rechercher tous les types d'anomalies.
   *
   * @returns les types d'anomalies.
   */
  listerTypesAnomaliePourDemandeSubvention$(): Observable<TypeDemandeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/type-anomalie/subvention/lister`);
  }

  /**
   * Rechercher les sous-programmes pour creation de ligne dotation departementale.
   *
   * @returns Les sous-programmes
   */
  rechercherSousProgrammesPourLigneDotationDepartementale$(): Observable<SousProgrammeLovDto[]> {
    return this.httpService.envoyerRequeteGet(`/referentiel/dotation/departement/lignes/sous-programme/rechercher`);
  }

}

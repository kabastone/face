import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class FileSaverService {

  constructor() { }

  /**
   * Read the text contents of a File or Blob using the FileReader interface.
   * This is an async interface so it makes sense to handle it with Rx.
   * @param File | Blob
   * @return Observable<string | ArrayBuffer>
   */
  public blobReader = (blob: Blob): Observable<string | ArrayBuffer> => {
    if (!(blob instanceof Blob)) {
      return throwError(new Error('`blob` must be an instance of File or Blob.'));
    }

    return new Observable(obs => {
      const reader = new FileReader();

      reader.onerror = err => obs.error(err);
      reader.onabort = err => obs.error(err);
      reader.onload = () => obs.next(reader.result);
      reader.onloadend = () => obs.complete();

      return reader.readAsText(blob);
    });
  }

  /**
   * Permet de télécharger un fichier - via les données fournies.
   *
   * @param response les données reçues
   */
  public telechargerFichierFromFile(fichier: File, nomFichier: string, typeFichier: string): void {
    let isFileSaverSupported = true;

    // on vérifie si l'utilisation d'un blob est possible
    try {
      isFileSaverSupported = !!new Blob;
    } catch (e) {
      console.log('FileSaver non supporté - impossible de créer un blob');
    }

    // gestion du fichier à télécharger
    const blobCustom = new Blob([fichier], { type: typeFichier });
    saveAs(blobCustom, nomFichier);
  }

  /**
   * Permet de télécharger un fichier - via un WS.
   *
   * @param response les données reçues
   */
  public telechargerFichier(response: any): void {
    let isFileSaverSupported = true;

    // on vérifie si l'utilisation d'un blob est possible
    try {
      isFileSaverSupported = !!new Blob;
    } catch (e) {
      console.log('FileSaver non supporté - impossible de créer un blob');
    }

    // pas de fichier : pas de headers
    if (response.headers.get('content-disposition') == null
      || response.headers.get('content-type') == null) {
      // pas de téléchargement
      return;
    }

    // gestion du téléchargement via WS
    const filename = response.headers.get('content-disposition').split('=')[1];
    const blob = new Blob([response.body], { type: response.headers.get('content-type') });
    saveAs(blob, filename);
  }

}

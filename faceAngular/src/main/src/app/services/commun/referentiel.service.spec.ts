import { TestBed } from '@angular/core/testing';

import { ReferentielService } from './referentiel.service';

describe('ReferentielService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReferentielService = TestBed.get(ReferentielService);
    expect(service).toBeTruthy();
  });
});

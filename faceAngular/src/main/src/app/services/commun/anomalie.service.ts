import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

import { Observable } from 'rxjs';
import { AnomalieDto } from '@app/dto/anomalie.dto';

@Injectable({
  providedIn: 'root'
})
export class AnomalieService {

  constructor( private httpService: HttpService) { }

  /**
   * Ajout de l'anomalie.
   *
   * @param anomalie - anomalie à ajouter
   * @returns anomalie créee
   */
  ajouterAnomalie(anomalieDto: AnomalieDto): Observable<AnomalieDto> {
    return this.httpService.envoyerRequetePost('/anomalie/ajouter', anomalieDto);
  }
  /**
   * Update l'anomalie.
   *
   * @param anomalieDto
   * @returns
   */
  updateAnomalie(anomalieDto: AnomalieDto): Observable<AnomalieDto> {
    return this.httpService.envoyerRequetePost(`/anomalie/${anomalieDto.id}/modifier`, anomalieDto);
  }

  /**
   * Pour une demande de subvention ou paiement : mettre à jour la correction d'une anomalie.
   *
   * @param idAnomalie - id de l'anomalie concernée
   * @param idDemande - id de la demande (paiement ou subvention)
   * @param corrigee - le nouvel état (true/false)
   * @returns l'anomalie mise à jour
   */
  majCorrectionAnomalie(idAnomalie: number, idDemande: number, domaine: string, corrigee: boolean): Observable<AnomalieDto> {
    return this.httpService.envoyerRequetePost(`/anomalie/${idAnomalie}/${domaine}/${idDemande}/maj-correction`, corrigee);
  }

  /**
   * Rechercher la liste des anomalies liées à une demande de subvention/paiement.
   *
   * @param path
   * @param id
   * @returns
   */
  rechercherAnomaliesParId(path: string, id: number): Observable<AnomalieDto[]> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/anomalie/${path}/${id}/rechercher`, false, [] as AnomalieDto[]);
  }
}

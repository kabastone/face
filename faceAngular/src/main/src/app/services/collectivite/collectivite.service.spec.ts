import { TestBed } from '@angular/core/testing';

import { CollectiviteService } from './collectivite.service';

describe('CollectiviteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollectiviteService = TestBed.get(CollectiviteService);
    expect(service).toBeTruthy();
  });
});

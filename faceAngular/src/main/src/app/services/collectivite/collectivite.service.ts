import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../http/http.service';
import { CollectiviteSimpleDto } from '../authentification/dto/collectivite-simple-dto';
import { CollectiviteDetailDto } from '@administration/dto/collectivite-detail-dto';

@Injectable({
  providedIn: 'root'
})
export class CollectiviteService {

  constructor(
    private httpService: HttpService
  ) { }

  /**
   * Récupère la liste de toutes les collectivitées.
   */
  listerToutesCollectivites$(): Observable<CollectiviteSimpleDto[]> {
    return this.httpService.envoyerRequeteGet('/collectivite/lister');
  }

  listerCollectivitesParDepartement(code: string): Observable<CollectiviteSimpleDto[]> {
    return this.httpService.envoyerRequeteGetGestionNotFound(`/referentiel/collectivite/departement/${code}/lister`,
      true, [] as CollectiviteSimpleDto[]);
  }

  /**
   * Récupèré les informations détailées de la collectivité spécifiée.
   *
   * @param id - id de la collectivité
   * @returns détail de la collectivité
   */
  getCollectiviteDetailDto$(id: number): Observable<CollectiviteDetailDto> {
    return this.httpService.envoyerRequeteGet(`/collectivite/${id}/rechercher`);
  }

  /**
  * Modifie la collectivité.
  *
   * @param collectivite
   * @returns
   */
  modifierCollectivite(collectivite: CollectiviteDetailDto): Observable<any> {
    return this.httpService.envoyerRequetePost(`/collectivite/${collectivite.id}/modifier`, collectivite);
  }

    /**
  * Ferme la collectivité.
  *
   * @param collectivite
   * @returns
   */
  fermerCollectivite(collectivite: CollectiviteDetailDto): Observable<any> {
    return this.httpService.envoyerRequetePost(`/collectivite/${collectivite.id}/fermer`, collectivite);
  }


  /**
   * Créer la collectivité.
   *
   * @param collectivite
   * @returns
   */
  creerCollectivite(collectivite: CollectiviteDetailDto): Observable<any> {
    return this.httpService.envoyerRequetePost(`/collectivite/creer`, collectivite, true, null, 'modale-collectivite');
  }
}

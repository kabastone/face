import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigService } from '@app/config/app-config.service';
import { MessageService } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DroitService } from '../authentification/droit.service';
import { FileSaverService } from '../commun/file-saver.service';

@Injectable({
  providedIn: 'root'
})

/**
 * Service dédié aux requêtes POST/GET/DELETE, etc.
 *
 * @class HttpService
 */
export class HttpService {
  /**
   * Création d'une instance de HttpService.
   * @param httpClient pour les requêtes HTTP
   * @param messageService service primenng, pour message IHM
   */
  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private droitService: DroitService,
    private fileSaverService: FileSaverService
  ) { }

  /**
   * Retourne le préfixe de l'url du ws.
   *
   * @returns préfixe url du ws
   */
  private getPrefixeUrlWs(): string {
    return `${AppConfigService.settings.urlApplication}/api`;
  }

  /**
   * Envoie une requête GET.
   *
   * @param cheminUrlWs chemin de l'url du ws
   * @param [affichageErreurs] affichage ou non des erreurs sur l'IHM
   * @param [resultatCasErreur] objet dont l'observable doit être retourné en cas d'erreur
   * @returns donnée observable reçue
   */
  public envoyerRequeteGet(cheminUrlWs: string, affichageErreurs?: boolean, resultatCasErreur?): Observable<any> {
    // envoi de la requête
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);
    const data = this.httpClient.get<any>(urlWs).pipe(
      tap(donneesRecues => {
        console.log(`requête GET envoyée [${urlWs}] avec succès`);
        // si on a un message spécifique du WS : on part en erreur!
        if (donneesRecues != null && donneesRecues['message'] !== undefined && donneesRecues['type'] !== undefined) {
          throw { error: donneesRecues };
        }
      }),
      // catch des erreurs envetuelles
      catchError(this.handleError<any>('GET', affichageErreurs, resultatCasErreur))
    );
    return data;
  }

  /**
   * Envoie une requête GET.
   *
   * @param cheminUrlWs chemin de l'url du ws
   * @param affichageErreurs affichage ou non des erreurs sur l'IHM
   * @param resultatCasErreur objet dont l'observable doit être retourné en cas d'erreur
   * @returns donnée observable reçue
   */
  public envoyerRequeteGetGestionNotFound(cheminUrlWs: string, affichageErreurs: boolean, resultatCasErreur): Observable<any> {
    // envoi de la requête
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);
    const data = this.httpClient.get<any>(urlWs).pipe(
      tap(donneesRecues => {
        console.log(`requête GET envoyée [${urlWs}] avec succès (gestion not found)`);
        // si on a un message spécifique du WS : on part en erreur!
        if (donneesRecues != null && donneesRecues['message'] !== undefined && donneesRecues['type'] !== undefined) {
          throw { error: donneesRecues };
        }
      }),
      // catch des erreurs envetuelles
      catchError(error => {
        if (error.status === 404) {
          return of(resultatCasErreur);
        } else {
          this.handleError<any>('GET (gestion not found)', affichageErreurs, resultatCasErreur);
        }
      })
    );
    return data;
  }

  /**
    * Envoie une requête GET pour download.
    *
    * @param cheminUrlWs chemin de l'url du ws
    * @param [affichageErreurs] affichage ou non des erreurs sur l'IHM
    * @returns donnée observable reçue
    */
  public envoyerRequeteGetPourDownload(cheminUrlWs: string, affichageErreurs?: boolean, resultatCasErreur?) {
    // envoi de la requête
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);

    const data = this.httpClient.get(urlWs, { observe: 'response', responseType: 'blob' }).pipe(
      tap(donneesRecues => {
        // infos dev
        console.log(`requête GET envoyée [${urlWs}] avec succès (pour download)`);

        // lecture du blob obtenu
        // si l'on a du binaire, on continue (=téléchargement du fichier)
        // si l'on a un objet d'erreur, on part en erreur
        this.fileSaverService.blobReader(donneesRecues.body).pipe(
          tap(blobData => {
            let objectLisible = null;
            try {
              objectLisible = JSON.parse(String(blobData));
            } catch (e) {
              // impossible de sérialiser un PDF = cas usuel du téléchargement d'un binaire
            }
            // cas usuel : on a du binaire (pas d'erreur json)
            if (!objectLisible) {
              return;
            }
            // recherche de l'erreur, si existante
            if (objectLisible != null && objectLisible['message'] !== undefined && objectLisible['type'] !== undefined) {
              throw { error: objectLisible };
            }
          }),
          catchError(this.handleError<any>('GET (pour download)', affichageErreurs, resultatCasErreur))
        ).subscribe();
      }),
      // catch des erreurs envetuelles
      catchError(this.handleError<any>('GET (pour download)', affichageErreurs, resultatCasErreur))
    );
    return data;
  }

  /**
   * Envoie une requête DELETE.
   *
   * @param cheminUrlWs chemin de l'url du ws
   * @returns donnée observable reçue
   */
  public envoyerRequeteDelete(cheminUrlWs: string, affichageErreurs?: boolean, resultatCasErreur?): Observable<any> {

    // envoi de la requête
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);
    const data = this.httpClient.delete<any>(urlWs).pipe(
      tap(donneesRecues => {
        console.log(`requête DELETE envoyée [${urlWs}] avec succès`);
        // si on a un message spécifique du WS : on part en erreur!
        if (donneesRecues != null && donneesRecues['message'] !== undefined && donneesRecues['type'] !== undefined) {
          throw { error: donneesRecues };
        }
      }),
      // catch des erreurs envetuelles
      catchError(this.handleError<any>('DELETE', affichageErreurs, resultatCasErreur))
    );
    return data;
  }

  /**
   * Envoie une requête POST.
   *
   * @param cheminUrlWs chemin de l'url du ws
   * @param objetEnvoyer objet a envoyer en POST
   * @returns donnée observable reçue
   */
  public envoyerRequetePost(cheminUrlWs: string, objetEnvoyer: any,
         affichageErreurs?: boolean, resultatCasErreur?, key?: string): Observable<any> {
    // options POST
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    // envoi de la requête POST
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);
    const observable = this.httpClient.post(urlWs, objetEnvoyer, httpOptions).pipe(
      tap(donneesRecues => {
        console.log(`requête POST envoyée [${urlWs}] avec succès`);
        // si on a un message spécifique du WS : on part en erreur!
        if (donneesRecues != null && donneesRecues['message'] !== undefined && donneesRecues['type'] !== undefined) {
          throw { error: donneesRecues };
        }
      }),
      // catch des erreurs envetuelles
      catchError(this.handleError<any>('POST', affichageErreurs, resultatCasErreur, key))
    );

    // cas nominal
    return observable;
  }

  /**
   * Envoie une requête POST avec upload de fichiers.
   *
   * @param cheminUrlWs chemin de l'url du ws
   * @param objetEnvoyer objet a envoyer en POST
   * @returns donnée observable reçue
   */
  public envoyerRequetePostPourUpload(cheminUrlWs: string, objetEnvoyer: any,
    affichageErreurs?: boolean, resultatCasErreur?): Observable<any> {
    // envoi de la requête POST
    const urlWs = this.getPrefixeUrlWs().concat(cheminUrlWs);
    const observable = this.httpClient.post(urlWs, objetEnvoyer).pipe(
      tap(donneesRecues => {
        console.log(`requête POST envoyée [${urlWs}] avec succès (pour upload)`);
        // si on a un message spécifique du WS : on part en erreur!
        if (donneesRecues != null && donneesRecues['message'] !== undefined && donneesRecues['type'] !== undefined) {
          throw { error: donneesRecues };
        }
      }),
      // catch des erreurs envetuelles
      catchError(this.handleError<any>('POST (pour upload)', affichageErreurs, resultatCasErreur))
    );
    // cas nominal
    return observable;
  }
  /**
   * Gestion d'une opération HTTP échouée et ne bloque pas l'application.
   * Affiche un message d'erreur en console (mode dev uniquepment).
   * Affiche un message d'erreur PrimeNG sur l'IHM
   *
   * @param operation - name of the operation that failed
   * @param [affichageErreurs] affichage ou non des erreurs sur l'IHM
   * @param resultatCasErreur - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', affichageErreurs?: boolean, resultatCasErreur?: T, key: string = 'standard') {

    return (error: any): Observable<T> => {
      const apiError = error.error;

      // init. erreur par défaut
      let titreErreur = 'Erreur technique : ';
      let messageErreur = 'Une erreur vient de se produire dans l\'application';

      // log console (en dev uniquement)
      // a noter que error.url n'est pas alimenté si tomcat est coupé en prod
      if (error.url) {
        console.warn(`Erreur de requête ${operation} sur ${error.url}`);
        console.log('reponse du ws :');
        console.log(apiError);
      } else if (error.message) {
        console.error(error.message);
      }

      // messages d'erreurs 'custom' issus de la réponse du WS
      if (apiError.type === 'RegleGestionException') {
        // cas des erreurs multiples
        titreErreur = '';
        messageErreur = apiError.erreursMultiples.join('\n');
      } else if (apiError['message'] !== undefined) {
        // cas d'une erreur unique
        titreErreur = '';
        messageErreur = apiError['message'];
      }

      // gestion du token expiré
      if (error.status === 401) {
        // affichage du message d'information
        titreErreur = '';
        messageErreur = 'Session expirée, veuillez vous reconnecter';
        this.envoyerMessageIhm(messageErreur, 'info', '', affichageErreurs, key);
        // on force la déconnection l'utilisateur
        this.droitService.logout();
        return of();
      } else if (error.status === 504 || error.status === 0) {
        // gestion du tomcat innaccessible
        // 504 = dev (retour proxy)
        // 0 = prod (tomcat coupé, cas vraiment à la marge)
        titreErreur = '';
        messageErreur = 'Le serveur est injoignable, veuillez réessayer plus tard';
        this.envoyerMessageIhm(messageErreur, 'error', titreErreur, affichageErreurs, key);
      }

      // envoi message ihm
      if (apiError['status'] === 'NOT_FOUND') {
        // 404 not found = message info
        this.envoyerMessageIhm(messageErreur, 'info', '', affichageErreurs, key);
      } else {
        // sinon : message d'erreur
        this.envoyerMessageIhm(messageErreur, 'error', titreErreur, affichageErreurs, key);
      }

      // si l'on désactivé l'affichage des erreurs : objet vide retourné
      if (affichageErreurs === false) {
        return of({} as T);
      }

      // si un résultat spécifique est souhaité en cas d'erreur
      if (resultatCasErreur) {
        return of(resultatCasErreur);
      }

      // observable vide pour ne pas bloquer l'appli
      return of();
    };
  }

  /**
   * Envoi d'un message sur l'IHM
   *
   * @param message contenu du message
   * @param type type d'erreur
   * @param titre titre du message
   * @param [affichageErreurs] affichage ou non des erreurs sur l'IHM
   */
  private envoyerMessageIhm(message: string, type: string, titre: string, affichageErreurs?: boolean, key?: string) {
    // lancement de la fonction en cas d'erreur, si spécifiée
    if (affichageErreurs === false) {
      // pas d'affichage des messages d'erreurs
      return;
    }
    window.scrollTo(0, 0);
    // uniquement le dernier message
    this.messageService.clear();
    this.messageService.add({
      severity: type,
      summary: titre,
      detail: message,
      key: key
    });
  }
}

import { TestBed } from '@angular/core/testing';

import { RxStoreService } from './rx-store.service';

describe('RxStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RxStoreService = TestBed.get(RxStoreService);
    expect(service).toBeTruthy();
  });
});

import { Observable, BehaviorSubject } from 'rxjs';

export class RxItem<T> {
  /**
   * Stockage de la valeur via BehaviorSubject,
   * car un simple Subject ne la conserve pas.
   * Initialisé par le constructeur.
   */
  private observables: { instance$: BehaviorSubject<T>; } = {
      instance$: undefined
    };

  // init. avec une valeur par defaut si spécifiée
  constructor(initValue?: any) {
    this.observables.instance$ = new BehaviorSubject<T>(initValue);
  }

  /**
   * Récupère la valeur de cet item en tant qu'observable.
   *
   */
  get value$(): Observable<T> {
    return this.observables.instance$;
  }

  /**
   * Récupère la valeur de cet item.
   *
   */
  get value(): T {
    return this.observables.instance$.getValue();
  }

  /**
   * Met à jour la valeur de cet item.
   *
   * @param value - nouvelle valeur
   */
  update(value: T) {
    this.observables.instance$.next(value);
  }

  /**
   * Stoppe l'observable pour qu'il arrête d'envoyer des données.
   *
   */
  stop() {
    this.observables.instance$.complete();
  }
}

import { BehaviorSubject } from 'rxjs';
import { UtilisateurConnecteDto } from '../authentification/dto/utilisateur-connecte-dto';

export interface StoreAppli {
  // token d'authentification
  tokenAuthentification: string;

  // informations sur l'utilisateur connecté
  infosUtilisateur$: BehaviorSubject<UtilisateurConnecteDto>;

  // collectivité en cours
  idCollectivite$: BehaviorSubject<number>;
}

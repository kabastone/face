import { Injectable } from '@angular/core';
import { RxItem } from './rx-item';
@Injectable({
  providedIn: 'root'
})
export class RxStoreService {
  private storeItems: { [id: string]: RxItem<any> } = {};

  /**
   * Initialise un nouveau membre dans le store ou alors retourne l'élement
   * représenté par sa clef (id).
   *
   * @template T - type
   * @param id - clef
   * @param [initValue] - valeur par défaut
   * @returns le membre du store associé à la clef (id)
   */
  get<T>(id: string, initValue?: any): RxItem<T> {
    let itemStore = this.storeItems[id];
    if (!itemStore) {
      itemStore = new RxItem<T>(initValue);
      this.storeItems[id] = itemStore;
    }
    return itemStore;
  }

  /**
   * Indique si l'élément 'id' existe dans le store.
   *
   * @param id - clef
   * @returns true ou false
   */
  exists(id: string): boolean {
    return id in this.storeItems;
  }

  /**
   * Supprime un élément du store.
   *
   * @template T - type
   * @param id - clef
   */
  delete<T>(id: string) {
    const itemExiste = this.storeItems[id];
    if (itemExiste) {
      delete this.storeItems[id];
    }
  }

  /**
   * Vide le store.
   *
   */
  clear() {
    Object.entries(this.storeItems).forEach(([index]) => {
      delete this.storeItems[index];
    });
    this.storeItems = {};
  }
}

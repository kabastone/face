import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppConfigService } from '@app/config/app-config.service';
import { DroitService } from '@app/services/authentification/droit.service';
import { UtilisateurAdminDto } from '@app/services/authentification/dto/utilisateur-admin-dto';
import { UtilisateurConnecteDto } from '@app/services/authentification/dto/utilisateur-connecte-dto';
import { CollectiviteService } from '@app/services/collectivite/collectivite.service';
import { HttpService } from '@app/services/http/http.service';
import { Observable, Subject } from 'rxjs';
import { DroitAgentCollectiviteDto } from '@app/services/authentification/dto/droit-agent-collectivite-dto';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'lay-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.css']
})
export class MenuPrincipalComponent implements OnInit, OnDestroy {

  // le chemin vers la marianne
  public marianne: any = 'assets/img/marianne.png';

  // infos utilisateur
  public infosUtilisateur: UtilisateurConnecteDto = {} as UtilisateurConnecteDto;

  // liste des collectivitées chargées pour le menu déroulant
  public listeCollectivitesMenu: DroitAgentCollectiviteDto[] = [];

  // collectivité - sélection utilisateur (menu déroulant)
  public collectiviteSelection: DroitAgentCollectiviteDto;

  // subject de collectivité (pour maj)
  public collectiviteSubject = new Subject<DroitAgentCollectiviteDto>();

  // observable de la collectivité sélectionnée
  public collectiviteSelection$: Observable<DroitAgentCollectiviteDto> = this.collectiviteSubject.asObservable();

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  /**
   * A la destruction : on lance le désabonnement de tous les subscribes()
   *
   */
  public ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  // constructeur
  constructor(
    private droitService: DroitService,
    private collectiviteService: CollectiviteService,
    private httpService: HttpService,
    private router: Router
  ) {
    // mise à jour des infos utilisateur
    this.droitService.getInfosUtilisateur$()
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      infosUser => {
        if (!infosUser.id) {
          return;
        }
        this.infosUtilisateur = infosUser;
        this.chargementMenuCollectivites();
      }
    );
    // on subscribe sur la sélection de la collectivité sélectionnée
    this.collectiviteSelection$
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe(droitAgent => this.droitService.updateIdCollectivite(droitAgent.idCollectivite)
    );
  }

  // init composant
  ngOnInit() {
    // chargement des informations sur l'utilisateur connecté
    this.majStoreInformationsUtilisateur();
  }


  /**
   * Chargement menu IHM des collectivités.
   *
   */
  chargementMenuCollectivites(): void {
    if (this.isAuMoinsGenerique()) {
      this.listeCollectivitesMenu = this.infosUtilisateur.droitsAgentCollectivites || [];

      if (this.listeCollectivitesMenu.length === 1) {
        this.collectiviteSubject.next(this.listeCollectivitesMenu[0]);
      } else if (this.listeCollectivitesMenu.length > 1) {
        // pré-sélection de la première valeur du menu déroulant
        this.collectiviteSelection = this.infosUtilisateur.droitsAgentCollectivites[0];
        this.collectiviteSubject.next(this.collectiviteSelection);
      }
    }
  }

  /**
   * OnChange du menu déroulant de sélection de collectivité
   *
   * @param selection - la collectivité sélectionnée
   */
  public onChangeCollectivite(selection: DroitAgentCollectiviteDto): void {
    this.collectiviteSubject.next(selection);
  }

  /**
   * Indique si l'utilisateur est au moins générique + a une seule collectivité.
   *
   * @returns true ou false
   */
  isGeneriqueEtUneCollectivite(): boolean {
    return this.droitService.isGeneriqueEtUneCollectivite(this.infosUtilisateur, this.listeCollectivitesMenu);
  }

  /**
   * Indique si l'utilisateur est au moins générique + a plus d'une seule collectivité.
   *
   * @returns true ou false
   */
  isGeneriqueEtMultiCollectivites(): boolean {
    return this.droitService.isGeneriqueEtMultiCollectivites(this.infosUtilisateur, this.listeCollectivitesMenu);
  }

  /**
   * Indique si l'utilisateur possède au moins le rôle générique.
   *
   * @returns true ou false
   */
  isAuMoinsGenerique(): boolean {
    return this.droitService.isAuMoinsGenerique(this.infosUtilisateur);
  }

  /**
   * Génère l'url de login à partir de la configuration chargée au runtime.
   *
   */
  getUrlConnection(): string {
    return `${AppConfigService.settings.urlApplication}/connecter`;
  }

  /**
   * Met à jour le store applicatif avec les informations de l'utilisateur connecté.
   *
   */
  private majStoreInformationsUtilisateur(): void {
    // pas de récupération, si non connecté
    if (!this.droitService.isTokenExistant()) {
      return;
    }

    // récupération des informations sur l'utilisateur connecte
    this.httpService.envoyerRequeteGet('/utilisateur-connecte/rechercher')
      .subscribe((infosUtilisateur: UtilisateurConnecteDto) => {
        // mise à jour du store applicatif
        this.droitService.updateUtilisateurConnecte(infosUtilisateur);
      });
  }

  /**
   * Récupération des informations utilisateur à partir du store applicatif.
   *
   * @returns informations utilisateur
   */
  getInfosUtilisateur$(): Observable<UtilisateurAdminDto> {
    const data = this.droitService.getInfosUtilisateur$();
    return data;
  }

  isMfer(): boolean {
    return typeof this.infosUtilisateur.listeRoles !== 'undefined' && (this.infosUtilisateur.listeRoles.includes('MFER_INSTRUCTEUR')
      || this.infosUtilisateur.listeRoles.includes('MFER_RESPONSABLE'));
  }

  isSd7(): boolean {
    return typeof this.infosUtilisateur.listeRoles !== 'undefined' && (this.infosUtilisateur.listeRoles.includes('SD7_INSTRUCTEUR')
      || this.infosUtilisateur.listeRoles.includes('SD7_RESPONSABLE'));
  }

  isAgent(): boolean {
    return typeof this.infosUtilisateur.listeRoles !== 'undefined' && this.droitService.isUniquementGenerique(this.infosUtilisateur)
      && this.infosUtilisateur.droitsAgentCollectivites.length > 0;
  }

  isCollectiviteOuvert(): boolean {
    return this.droitService.isCollectiviteOuvert();
  }
  public isActive(base: string): boolean {
    return this.router.url.includes(`/${base}`);
  }
}

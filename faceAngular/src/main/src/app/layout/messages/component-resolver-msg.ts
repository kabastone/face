import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';

@Injectable()
export class ComponentResolverMsg implements Resolve<any> {

  constructor(private messageService: MessageService) { }

  resolve(route: ActivatedRouteSnapshot, rstate:  RouterStateSnapshot): Observable<any>|Promise<any>|any {
    // effacement des messages prime ng
    this.messageService.clear();
  }
}

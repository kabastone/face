import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '@app/config/app-config.service';

@Component({
  selector: 'lay-pied',
  templateUrl: './pied.component.html',
  styleUrls: ['./pied.component.css']
})
export class PiedComponent implements OnInit {
  bloc_ministere_1: any = 'assets/img/bloc_ministere_1.png';
  bloc_ministere_2: any = 'assets/img/bloc_ministere_2.png';

  constructor() {}

  ngOnInit() {}
  
  /**
   * Renvoie la version de l'application  à partir de la configuration chargée au runtime.
   *
   */
  getVersionApplication(): string {
    return `${AppConfigService.settings.versionApplication}`;
  }
  
  /**
   * Renvoie la date de build de l'application à partir de la configuration chargée au runtime.
   *
   */
  getDateBuild(): string {
    return `${AppConfigService.settings.dateBuild}`;
  }
}

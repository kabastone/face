import { Routes } from '@angular/router';

// Interne
import { ComponentResolverMsg } from '@layout/messages/component-resolver-msg';
import { MentionLegaleComponent } from './mention-legale/mention-legale.component';
import { ContactComponent } from './contact/contact.component';
import { NousContacterComponent } from './nous-contacter/nous-contacter.component';

export const PIED_ROUTES: Routes = [
  {
    path: 'mentions-legales',
    component: MentionLegaleComponent,
    data: { title: 'Mentions Légales' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'nous-contacter',
    component: NousContacterComponent,
    data: { title: 'Nous contacter' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'Contact' },
    resolve: { componentMsg: ComponentResolverMsg }
  },
];

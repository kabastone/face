import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {EmailContactDto} from '@app/dto/email-contact.dto';
import { NousContacterService } from '@app/services/pied/nous-contacter.service';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageManagerService } from '@app/services/commun/message-manager.service';

@Component({
  selector: 'lay-nous-contacter',
  templateUrl: './nous-contacter.component.html',
  styleUrls: ['./nous-contacter.component.css']
})
export class NousContacterComponent implements OnInit, OnDestroy {


  emailDto: EmailContactDto;

  contactFormGroup: FormGroup;

  emailEnvoye: boolean;

  // pour le désabonnement auto
  public ngDestroyed$ = new Subject();

  constructor(
    private messageService: MessageService,
    private messageManagerService: MessageManagerService,
    private formBuilder: FormBuilder,
    private nousContacterService: NousContacterService) { }

  ngOnInit() {

    this.initialisationForm();
  }

  ngOnDestroy(): void {
    this.ngDestroyed$.next();
  }

  public envoiEmailContact(): void {
    this.emailDto = {
      nom: this.contactFormGroup.get('nom').value,
      message: this.contactFormGroup.get('message').value,
      email: this.contactFormGroup.get('email').value,
      objet: this.contactFormGroup.get('objet').value
    };

    this.nousContacterService.envoyerEmailContact(this.emailDto)
    .pipe(takeUntil(this.ngDestroyed$))
    .subscribe(
      data => {
        if (!!data && typeof(data) === 'boolean') {
          this.messageManagerService.envoiEmailContactMessage();
          this.initialisationForm();
        } else {
          this.messageManagerService.erreurEnvoiEmailContactMessage();
        }
    });
  }

  private initialisationForm(): void {
    this.contactFormGroup = this.formBuilder.group({
      nom: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      ),
      email: new FormControl(
        '', {
          validators: Validators.compose([
            Validators.required,
            Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+?\.[a-zA-Z]{1,4}$')
          ]),
          updateOn: 'blur'
        }),
        objet: new FormControl(
          '', {
            validators: Validators.compose([
              Validators.required,
              Validators.minLength(1)
            ])
          }
        ),
        message: new FormControl(
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(1),
          ])
        )
    });
  }

}

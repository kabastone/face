import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MentionLegaleComponent } from './mention-legale/mention-legale.component';
import { ContactComponent } from './contact/contact.component';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { NousContacterComponent } from './nous-contacter/nous-contacter.component';
import {DialogModule} from 'primeng/dialog';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    MentionLegaleComponent,
    ContactComponent,
    NousContacterComponent],
  imports: [
    CommonModule,
    InputTextModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextareaModule,
    ButtonModule,
    RouterModule,
    DialogModule

  ]
})
export class PiedModule { }

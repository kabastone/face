import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { PiedComponent } from './pied/pied.component';
import { EnteteComponent } from './entete/entete.component';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { MessagesComponent } from './messages/messages.component';


// PrimeNG
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PiedModule } from './pied/pied.module';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    PiedComponent,
    EnteteComponent,
    MenuPrincipalComponent,
    MessagesComponent,
  ],
  imports: [
    CommonModule,
    // PrimeNG : gestion des messages
    MessagesModule,
    MessageModule,
    RouterModule,
    FormsModule,
    PiedModule,
    DropdownModule
  ],
  exports: [
    PiedComponent,
    EnteteComponent,
    MenuPrincipalComponent,
    MessagesComponent,
    RouterModule
  ]
})
export class LayoutModule { }

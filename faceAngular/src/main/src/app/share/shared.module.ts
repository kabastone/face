import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomPipesModule } from '@app/custom-pipes/custom-pipes.module';
import { CustomFileTransferModule } from '@component/custom-file-transfer/custom-file-transfer.module';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
    declarations:[],
    imports:[ 
        CommonModule,
        TabViewModule,
        TableModule,
        ButtonModule,
        InputTextModule,
        FormsModule,
        DropdownModule,
        CheckboxModule,
        DialogModule,
        MessageModule,
        PanelModule,
        ReactiveFormsModule,
        CustomPipesModule,
        ConfirmDialogModule,
        MessagesModule,

        CustomFileTransferModule,
    ],
    exports: [
        CommonModule,
        TabViewModule,
        TableModule,
        ButtonModule,
        InputTextModule,
        FormsModule,
        DropdownModule,
        CheckboxModule,
        DialogModule,
        MessageModule,
        PanelModule,
        ReactiveFormsModule,
        CustomPipesModule,
        ConfirmDialogModule,
        MessagesModule,

        CustomFileTransferModule,
    ]
})
export class SharedModule {

}